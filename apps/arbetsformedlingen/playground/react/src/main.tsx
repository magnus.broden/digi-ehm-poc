import { StrictMode } from 'react';
import ReactDOMClient from 'react-dom/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import App from './app/app';
import Article from './app/components/pages/article/Article';
import Article2 from './app/components/pages/article-2/Article2';
import Article3 from './app/components/pages/article-3/Article3';
import Home from './app/components/pages/home/Home';
import ButtonTest from './app/components/pages/button-test/ButtonTest';

const root = ReactDOMClient.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
	<StrictMode>
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<App />}>
					<Route path="/" element={<Home />} />
					<Route path="/article" element={<Article />} />
					<Route path="/article2" element={<Article2 />} />
					<Route path="/article3" element={<Article3 />} />
					<Route path="/buttontest" element={<ButtonTest />} />
				</Route>
			</Routes>
		</BrowserRouter>
	</StrictMode>
);
