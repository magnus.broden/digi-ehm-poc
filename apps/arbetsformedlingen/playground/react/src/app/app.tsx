// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Outlet } from 'react-router-dom';

import './app.scss';

import { LayoutBlockContainer } from 'core-dist';
import { DigiLayoutBlock, DigiTypography } from 'arbetsformedlingen-react-dist';

import Header from './components/header/Header';
import Navigation from './components/navigation/Navigation';
import Footer from './components/footer/Footer';

export function App() {
	return (
		<DigiTypography>
		<div className="app">
			<Header />
			<DigiLayoutBlock
				className="app__wrapper"
				af-container={LayoutBlockContainer.NONE}
			>
				<div className='app__navigation-wrapper'>
					<Navigation />
				</div>
				<main className='app__main'>
					<Outlet />
				</main>
			</DigiLayoutBlock>
			<Footer />
		</div>
	</DigiTypography>
	);
}

export default App;
