import { DigiLayoutBlock, DigiTypography } from 'arbetsformedlingen-react-dist';
import { LayoutBlockVariation } from 'core-dist';
import './Footer.scss';

const Footer = () => {

  return (
    <DigiLayoutBlock
      className='footer'
      afVerticalPadding
      afVariation={LayoutBlockVariation.PROFILE}
    >
      <DigiTypography>
        <p>Footer</p>
      </DigiTypography>
    </DigiLayoutBlock>
  )
}

export default Footer;