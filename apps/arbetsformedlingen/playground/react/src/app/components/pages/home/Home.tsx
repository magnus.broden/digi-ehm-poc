import {
  ButtonVariation,
  CodeLanguage,
  CodeVariation,
  ExpandableFaqVariation,
  FormCheckboxVariation,
  FormFileUploadValidation,
  FormFileUploadVariation,
  FormRadiobuttonVariation,
  InfoCardHeadingLevel,
  InfoCardType,
  InfoCardVariation,
  LayoutBlockContainer,
  LayoutBlockVariation,
  LayoutColumnsElement,
  LinkButtonSize,
  LinkButtonVariation,
  LoaderSpinnerSize,
  LogoColor,
  LogoVariation,
  NavigationContextMenuItemType,
  NavigationSidebarPosition,
  NavigationSidebarVariation,
  ProgressbarVariation,
  ProgressStepsHeadingLevel,
  ProgressStepsVariation,
  TagSize,
  TypographyTimeVariation
} from 'arbetsformedlingen-dist';
import {
  DigiCalendarWeekView,
  DigiFormInput,
  DigiLayoutBlock,
  DigiTypographyHeadingJumbo,
  DigiLink,
  DigiLinkInternal,
  DigiLinkExternal,
  DigiLinkButton,
  DigiButton,
  DigiTag,
  DigiIconArrowDown,
  DigiCalendar,
  DigiCode,
  DigiExpandableAccordion,
  DigiExpandableFaq,
  DigiExpandableFaqItem,
  DigiFormCheckbox,
  DigiFormErrorList,
  DigiFormFieldset,
  DigiFormFileUpload,
  DigiFormFilter,
  DigiFormLabel,
  DigiFormRadiobutton,
  DigiFormRadiogroup,
  DigiInfoCard,
  DigiTypography,
  DigiInfoCardMultiContainer,
  DigiLayoutColumns,
  DigiMediaImage,
  DigiIconArrowRight,
  DigiLoaderSpinner,
  DigiLogo,
  DigiNavigationBreadcrumbs,
  DigiNavigationContextMenu,
  DigiNavigationContextMenuItem,
  DigiNavigationPagination,
  DigiNavigationSidebarButton,
  DigiNavigationSidebar,
  DigiNavigationVerticalMenu,
  DigiNavigationTabs,
  DigiNavigationTab,
  DigiProgressStep,
  DigiProgressSteps,
  DigiProgressbar,
  DigiTypographyTime,
  DigiDialog,
} from 'arbetsformedlingen-react-dist';
import { DialogSize } from 'libs/arbetsformedlingen/package/src';
import { useNavigate } from 'react-router-dom';
import './Home.module.scss';

const Home = () => {
  const navigate = useNavigate();
  const dates = ['2022-07-04', '2022-07-11', '2022-07-12', '2022-07-18'];

  function clickHandler(e: any) {
    navigate(e.target.afHref)
  }

  return (
    <DigiLayoutBlock afVerticalPadding>
      <DigiTypographyHeadingJumbo afText="Designsystemet - react"></DigiTypographyHeadingJumbo>
      <br />
      <DigiLayoutBlock afContainer={LayoutBlockContainer.NONE}>
        <DigiFormInput
          afLabel="Namn"
          onAfOnChange={(e) => console.log('change', e.detail.target.value)}
          onAfOnInput={(e) => console.log('input', e.detail.data)}
        ></DigiFormInput>

        <DigiCalendar
          afActive={true}
          afShowWeekNumber={true}
          afMultipleDates={true}>
          <DigiButton slot="calendar-footer">Välj</DigiButton>
        </DigiCalendar>

        <DigiCalendarWeekView
          afDates={JSON.stringify(dates)}
          onAfOnDateChange={(e) => console.log('dateChange', e.detail)}
        ></DigiCalendarWeekView>

        <ul>
          <li>
            <a href="/">startsidan (a-tagg) </a>
          </li>

          <li>
            <br></br>
            <DigiLink
              afHref="/article"
              afOverrideLink
              onAfOnClick={clickHandler}
            >startsidan (digi-link)</DigiLink>
          </li>

          <li>
            <br></br>
            <DigiLinkInternal
              afHref="/article"
              afOverrideLink
              onAfOnClick={clickHandler}
            >startsidan (digi-link-internal)</DigiLinkInternal>
          </li>

          <li>
            <br></br>
            <DigiLinkExternal
              afHref="/article"
              afOverrideLink
              onAfOnClick={clickHandler}
            >startsidan (digi-link-external)</DigiLinkExternal>
          </li>

          <li>
            <br></br>
            <DigiLinkButton
              afHref="/article"
              afOverrideLink
              onAfOnClick={clickHandler}
            >startsidan (digi-link-button)</DigiLinkButton>
          </li>

          <li>
            <br></br>
            <DigiButton afSize="large" afVariation={ButtonVariation.PRIMARY} afFullWidth={false}>
              startsidan (digi-link-external)
              <DigiIconArrowDown slot="icon" />
            </DigiButton>
          </li>

          <li>
            <br></br>
            <DigiTag
              afText="Jag är en tag"
              afSize={TagSize.SMALL}
              afNoIcon={false}>
            </DigiTag>
          </li>

          <li>
            <br></br>
            <DigiCode
              afLanguage={CodeLanguage.JAVASCRIPT}
              afVariation={CodeVariation.DARK}
              afCode="function foo(bar) {
                if (foo + 2 === 3) {
                  return true // This is true if foo is 1;
                } else {
                  return 'Wrong!';
                }" />

          </li>
          <li>
          {/*<DigiDialog 
          afSize={DialogSize.LARGE}
          afShowDialog={false}
          afHeading="Rubrik" 
          afPrimaryButtonText="Skicka" 
          afSecondaryButtonText="Avbryt">
          </DigiDialog>*/}
          </li>

          <li>
            <DigiExpandableAccordion afHeading="Utfällbar rubrik">
              <p>Ea nulla enim enim voluptate mollit proident.</p>
            </DigiExpandableAccordion>
          </li>
          <li>
            <br></br>
            <DigiExpandableFaq
              afHeading="Vanliga frågor"
              afVariation={ExpandableFaqVariation.PRIMARY}
            >
              <DigiExpandableFaqItem afHeading="Fråga 1">
                <p>Svar 1 - Reprehenderit ea nulla dolore non fugiat aute. Reprehenderit velit velit non eiusmod. Voluptate commodo consectetur ullamco velit minim quis. Reprehenderit consectetur nisi officia est fugiat id anim incididunt qui eiusmod.</p>
              </DigiExpandableFaqItem>
              <DigiExpandableFaqItem afHeading="Fråga 2">
                <p>Svar 2 - In veniam nostrud esse velit velit incididunt ullamco non adipisicing. Dolore sint quis aute nostrud. Pariatur commodo ullamco dolor nulla ut. Consequat amet velit veniam ipsum reprehenderit tempor. Eu labore enim officia anim laboris magna eu eu ad. Pariatur voluptate sint nisi ut nulla.</p>
              </DigiExpandableFaqItem>
              <DigiExpandableFaqItem afHeading="Fråga 3">
                <p>Svar 3 - Officia laboris commodo quis ex nisi cupidatat officia dolore est. Ad dolore exercitation sunt deserunt Lorem do esse reprehenderit ex non. Proident occaecat elit enim ullamco.</p>
              </DigiExpandableFaqItem>
            </DigiExpandableFaq>
          </li>

          <li>
            <br></br>
            <DigiFormCheckbox afLabel="Kryssruta" afVariation={FormCheckboxVariation.PRIMARY} />
          </li>
          <li>
            <br></br>
            <DigiFormErrorList
              afHeading="Felmeddelandelista">
              <a href="#input_1_id">Felmeddelandelänk 1</a>
              <a href="#input_2_id">Felmeddelandelänk 2</a>
            </DigiFormErrorList>
          </li>
          <li>
            <br></br>
            <DigiFormFieldset
              afForm="Formulärnamnet"
              afLegend="Det här är en legend"
              afName="Fältgruppnamn"
            >
              <DigiFormCheckbox afLabel="Alternativ 1"></DigiFormCheckbox>
              <DigiFormCheckbox afLabel="Alternativ 2"></DigiFormCheckbox>
              <DigiFormCheckbox afLabel="Alternativ 3"></DigiFormCheckbox>
              <DigiButton>
                Skicka
              </DigiButton>
            </DigiFormFieldset>
          </li>
          <li>
            <br></br>
            <DigiFormFileUpload
              afVariation={FormFileUploadVariation.PRIMARY}
              afValidation={FormFileUploadValidation.DISABLED}
              afFileTypes="*">
            </DigiFormFileUpload>
          </li>

          <li>
            <br></br>
            <DigiFormFilter
              afFilterButtonText="Yrkesområde"
              afSubmitButtonText="Filtrera"
            >
              <DigiFormCheckbox
                afLabel="Val 1" />
              <DigiFormCheckbox
                afLabel="Val 2" />
              <DigiFormCheckbox
                afLabel="Val 3" />
            </DigiFormFilter>
          </li>

          <li>
            <br></br>
            <DigiFormLabel afLabel="Etikett" afFor={"<DigiIconInfoCircleSolid slot='actions' />"} />
          </li>

          <li>
            <br></br>
            <DigiFormRadiobutton
              afLabel="Kryssruta"
              afVariation={FormRadiobuttonVariation.PRIMARY}>
            </DigiFormRadiobutton>
          </li>
          <li>
            <br></br>
            <DigiFormRadiogroup
              afName="myRadiogroupName"
            >
              <DigiFormRadiobutton
                afLabel="Kryssruta 1"
                afValue="val-1"
              ></DigiFormRadiobutton>
              <DigiFormRadiobutton
                afLabel="Kryssruta 2"
                afValue="val-2"
              ></DigiFormRadiobutton>
              <DigiFormRadiobutton
                afLabel="Kryssruta 3"
                afValue="val-3"
              ></DigiFormRadiobutton>
            </DigiFormRadiogroup>
          </li>
          <li>
            <br></br>
            <DigiInfoCard afHeading="Jag är ett infokort" afType={InfoCardType.SINGLE} afHeadingLevel={InfoCardHeadingLevel.H1}>
              <p>
                Det här är bara ord för att illustrera hur det ser ut med text inuti. Lorem ipsum dolor sit amet,
                consectetur adipiscing elit. Suspendisse commodo egestas elit in consequat. Proin in ex consectetur,
                laoreet augue sit amet, malesuada tellus.
              </p>
            </DigiInfoCard>
          </li>
          <li>
            <br></br>
            <DigiLayoutBlock afVariation={LayoutBlockVariation.PRIMARY}>
              <DigiTypography>
                <h2>Det här är en primär blockvariant</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna neque, interdum vel massa eget,
                  condimentum rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque.
                  Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est.
                </p>
              </DigiTypography>
            </DigiLayoutBlock>
          </li>
          <li>
            <br></br>
            <DigiInfoCardMultiContainer afHeading="Rubrik till infokortbehållare">
              <DigiInfoCard
                afHeading="Jag är ett infokort av typen multi"
                afVariation={InfoCardVariation.PRIMARY}
                afType={InfoCardType.RELATED}
                afLinkText="Jag är en obligatorisk länktext"
                afLinkHref="/"
                afHeadingLevel={InfoCardHeadingLevel.H1}>
                <p>
                  Det här är bara ord för att illustrera hur det ser ut med text
                  inuti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Suspendisse commodo egestas elit in consequat. Proin in ex
                  consectetur, laoreet augue sit amet, malesuada tellus.
                </p>
              </DigiInfoCard>
            </DigiInfoCardMultiContainer>
          </li>

          <li>
            <br></br>
            <DigiLayoutColumns
              afElement={LayoutColumnsElement.DIV}>
              <DigiMediaImage afUnlazy
                afSrc='assets/images/employment-service-sign.jpg'
                afAlt="Fasadskylt med Arbetsförmedlingens logotyp.">
              </DigiMediaImage>
              <p>...</p>
            </DigiLayoutColumns>
          </li>

          <li>
            <br></br>
            <DigiLinkButton
              afHref="'#'"
              afSize={LinkButtonSize.MEDIUM}
              afVariation={LinkButtonVariation.SECONDARY}>
              Jag är en länk
            </DigiLinkButton>
          </li>
          <li>
            <br></br>
            <DigiLoaderSpinner
              afSize={LoaderSpinnerSize.MEDIUM}
            ></DigiLoaderSpinner>
          </li>
          <li>
            <br></br>
            <DigiLogo afSystemName="System One"
              afVariation={LogoVariation.LARGE}
              afColor={LogoColor.PRIMARY}>
            </DigiLogo>`
          </li>
          <li>
            <br></br>
            <DigiMediaImage
              afUnlazy
              afHeight="300"
              afWidth="300"
              afSrc='assets/images/employment-service-sign.jpg'
              afAlt="Arbetsförmedlingens logotyp som en fasadskyld"
            >
            </DigiMediaImage>
          </li>
          <li>
            <br></br>
            <DigiNavigationBreadcrumbs
              afCurrentPage="Nuvarande sida"
            >
              <a href="/">Start</a>
              <a href="/contact-us">Undersida 1</a>
              <a href="#">Undersida 2</a>
            </DigiNavigationBreadcrumbs>
          </li>
          <li>
            <DigiNavigationContextMenu
              afText="Rullgardinsmeny"
              afStartSelected={0}
            >
              <DigiNavigationContextMenuItem
                afText="Menyval 1"
                afType={NavigationContextMenuItemType.BUTTON}
              >
              </DigiNavigationContextMenuItem>
              <DigiNavigationContextMenuItem
                afText="Menyval 2"
                afType={NavigationContextMenuItemType.BUTTON}
              >
              </DigiNavigationContextMenuItem>
            </DigiNavigationContextMenu>
          </li>

          <li>
            <br></br>
            <DigiNavigationPagination
              afTotalPages={6}
              afInitActive-page="1"
              afCurrentResultStart={1}
              afCurrentResultEnd={25}
              afTotalResults={10}
              afResultName="annonser"
              af-total-pages={8 ? 10 : 6}
              afInitActivePage={1}>
            </DigiNavigationPagination>
          </li>
          <li>
            <br></br>
            <DigiNavigationSidebarButton
              afText="Meny">
            </DigiNavigationSidebarButton>
          </li>
          <li>
            <br></br>
            <DigiNavigationSidebar
              afActive={false}
              afStickyHeader={true}
              afBackdrop={true}
              afPosition={NavigationSidebarPosition.END}
              afVariation={NavigationSidebarVariation.OVER}
              afCloseButtonText="Stäng"
            >
              <DigiNavigationVerticalMenu>
                ...
              </DigiNavigationVerticalMenu>
            </DigiNavigationSidebar>
          </li>

          <li>
            <br></br>
            <DigiNavigationTabs
              afAriaLabel="En tablist label"
            >
              <DigiNavigationTab
                afAriaLabel="Flik 1"
                afId="flikb1"
              >
                <p>Jag är flik 1</p>
              </DigiNavigationTab>
              <DigiNavigationTab
                afAriaLabel="Flik 2"
                afId="flikb2"
              >
                <p>Jag är flik 2</p>
              </DigiNavigationTab>
              <DigiNavigationTab
                afAriaLabel="Flik 3"
                afId="flikb3"
              >
                <p>Jag är flik 3</p>
              </DigiNavigationTab>
            </DigiNavigationTabs>
          </li>

          <li>
            <br></br>
            <DigiProgressStep afHeading={'Rubrik'}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Etiam magna neque, interdum vel massa eget, condimentum
              rutrum velit. Sed vitae ullamcorper sem.
            </DigiProgressStep>
          </li>

          <li>
            <br></br>
            <DigiProgressStep afHeading={'Rubrik'}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Etiam magna neque, interdum vel massa eget, condimentum
              rutrum velit. Sed vitae ullamcorper sem.
            </DigiProgressStep>
          </li>
          <li>
            <br></br>
            <DigiProgressSteps
              afCurrentStep={1}
              afVariation={ProgressStepsVariation.PRIMARY}
              afHeadingLevel={ProgressStepsHeadingLevel.H1}
            >
              <DigiProgressStep afHeading="Rubrik 1">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Etiam magna neque, interdum vel massa eget, condimentum
                rutrum velit. Sed vitae ullamcorper sem.
              </DigiProgressStep>
              <DigiProgressStep afHeading="Rubrik 2">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Etiam magna neque, interdum vel massa eget, condimentum
                rutrum velit. Sed vitae ullamcorper sem.
              </DigiProgressStep>
              <DigiProgressStep afHeading="Rubrik 3">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Etiam magna neque, interdum vel massa eget, condimentum
                rutrum velit. Sed vitae ullamcorper sem.
              </DigiProgressStep>
            </DigiProgressSteps>
          </li>

          <li>
            <br></br>
            <DigiProgressbar
              afTotalSteps={4}
              afCompletedSteps={2}
              afVariation={ProgressbarVariation.PRIMARY}
            >
            </DigiProgressbar>
          </li>

          <li>
            <br></br>
            <DigiTypographyHeadingJumbo
              afText="Det här är en jumborubrik">
            </DigiTypographyHeadingJumbo>
          </li>
          <li>
            <DigiTypographyTime
              afVariation={TypographyTimeVariation.DISTANCE}
              afDateTime="2022-12-12"
            >
            </DigiTypographyTime>
          </li>
          <li>
          <DigiFormFileUpload 
            afVariation={FormFileUploadVariation.PRIMARY}
            afValidation={FormFileUploadValidation.DISABLED}
            afFileTypes="*"
          ></DigiFormFileUpload>
          </li>
        </ul>


      </DigiLayoutBlock>
    </DigiLayoutBlock>
  );
};

export default Home;
