import { DigiLayoutBlock } from 'arbetsformedlingen-react-dist';
import styles from'./Article2.module.scss';

const Article2 = () => {

  return (
    <DigiLayoutBlock className={styles['article']}>
      <p>Article 2</p>
    </DigiLayoutBlock>
  )
}

export default Article2;