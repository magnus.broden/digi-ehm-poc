import { DigiLayoutBlock } from 'arbetsformedlingen-react-dist';
import styles from'./Article3.module.scss';

const Article3 = () => {

  return (
    <DigiLayoutBlock
      className={styles['article']}
    >
      <p>React komponenter</p>

      <ul>
          <li>
            <a href="/">startsidan (a-tagg) </a>
          </li>
      </ul>

    </DigiLayoutBlock>
  )
}

export default Article3;