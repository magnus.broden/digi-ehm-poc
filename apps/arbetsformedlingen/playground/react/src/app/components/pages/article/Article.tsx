import { DigiButton, DigiExpandableAccordion, DigiLayoutBlock } from 'arbetsformedlingen-react-dist';
import { useEffect, useState } from 'react';
import styles from './Article.module.scss';

const Article = () => {

  const heading1 = 'Originalrubrik';
  const heading2 = 'Ändrad rubrik';

  const [toggle, setToggle] = useState(false);

  function toggleClickHandler() {
    setToggle(!toggle);
  }

  function headingClickHandler() {
    setToggle(!toggle);
  }

	return (
		<DigiLayoutBlock className={styles['article']}>
			<p>Article</p>
      <DigiButton onAfOnClick={toggleClickHandler}>Ändra rubrik</DigiButton>
			<DigiExpandableAccordion 
        afHeading={!toggle ? heading1 : heading2} 
        onAfOnClick={headingClickHandler}
      >
				<p>
					Din kontakt med Arbetsförmedlingen kan se olika ut beroende på hur länge du
					är utan arbete. Vet du redan nu att du har arbete inom 3 månader kommer du
					inte att ha ett planeringssamtal med oss. Då använder du enbart våra
					digitala tjänster.
				</p>
			</DigiExpandableAccordion>
		</DigiLayoutBlock>
	);
};

export default Article;
