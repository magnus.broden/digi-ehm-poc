import { LayoutBlockContainer } from "arbetsformedlingen-dist";
import { DigiLayoutBlock, DigiLogo, DigiNavigationSidebarButton } from "arbetsformedlingen-react-dist";
import { NavLink } from "react-router-dom";
import styles from "./Header.module.scss";

const Header = () => {
  return (
    <header className={styles['app__header']}>
      <DigiLayoutBlock af-container={LayoutBlockContainer.NONE}>
          <div className={styles['app__header-inner']}>
            <NavLink to="/" className={styles['app__logo-link']}>
              <DigiLogo
                af-system-name="Designsystem"
                af-title="Designsystem"
              ></DigiLogo>
            </NavLink>
            <div className={styles['app__header-end']}>
              <DigiNavigationSidebarButton
                af-text="Meny"
                className="app__sidebar-toggle"
              ></DigiNavigationSidebarButton>
            </div>
          </div>
      </DigiLayoutBlock>
    </header>
  );
};

export default Header;
