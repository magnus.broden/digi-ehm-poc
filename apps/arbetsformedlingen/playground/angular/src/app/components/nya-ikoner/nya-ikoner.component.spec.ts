import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NyaIkonerComponent } from './nya-ikoner.component';

describe('NyaIkonerComponent', () => {
	let component: NyaIkonerComponent;
	let fixture: ComponentFixture<NyaIkonerComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [NyaIkonerComponent]
		}).compileComponents();

		fixture = TestBed.createComponent(NyaIkonerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
