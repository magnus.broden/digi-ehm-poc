import { Component } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
	selector: 'at-file-upload',
	templateUrl: './file-upload.component.html',
	styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {

	objectList = [];

	myForm: UntypedFormGroup;

	public resp = [
		{
			file_id: '44d88612fea8a8f36de82e1278abb02f',
			status: 'OK',
			details: {
				confidence: 5,
				malware_family: 54768315,
				malware_type: 114,
				severity: 4,
				signature_name: 'Trojan.Win32.Mitaka.TC.a'
			}
		},

		{
			file_id: '3e74f8be1ca4e3acaf843c8b4f5ef6cc',
			status: 'OK',
			details: {
				confidence: 0,
				severity: 0,
				signature_name: ''
			}
		}
	];

	ngOnInit() {
		this.fillArray()
	}

	fillArray() {
		const amount = 100;
		for (let index = 0; index < 500; index++) {
			this.objectList.push({
				id: index,
				title: 'Title: ' + index,
				body: 'Grazzie Thanks Gracias Tack Thank you Lol i sen danvc dasjd ad asd '
			})
		}
	}


	async validateFile(scanResp) {
		await customElements.whenDefined('digi-form-file-upload');
		const paginationElement = document.querySelector('digi-form-file-upload');
		await paginationElement?.afMValidateFile(scanResp);
	}

	onUpload(e) {
		e.preventDefault();
		
		setTimeout(() => {
			const file = e.detail
			const respIndex = Math.floor(Math.random() * 2);
			if(respIndex == 1) {
				file['status'] = 'OK';
				
			} else{
				file['status'] = 'error';
			}

			this.validateFile({ ...file, name: e.detail.name });
		}, 5000);
	}

	onRemove(e) {
		console.log(e.detail);
	}

	onCancel(e) {
		console.log(e.detail);
	}

	onRetry(e) {
		setTimeout(() => {
			const respIndex = Math.floor(Math.random() * 2);
			this.validateFile({ ...this.resp[respIndex], ...e.detail });
		}, 5000);
	}
}
