import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'digi-datepicker',
	templateUrl: './datepicker.component.html',
	styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent implements OnInit {
	constructor() {}

	multipleDates: Array<Date> = [new Date(), new Date("2023-03-24"), new Date("2023-03-25"), new Date("2023-03-26")];
	singleDate: Array<Date> = [new Date("2023-03-20")];
	minDate: Date;
	maxDate: Date;
	weekNumbers: boolean = false;

	ngOnInit(): void {}

	setDates(evt) {
		this.multipleDates = evt.detail;
	}
	setDate(evt) {
		this.singleDate = evt.detail;
	}
	setMinDate(evt) {
		this.minDate = evt.detail[0];
	}
	setMaxDate(evt) {
		this.maxDate = evt.detail[0];
	}
	removeDate(date) {
		this.multipleDates = this.multipleDates.filter(d => !this.isSameDate(d, date));
	}
	isSameDate(a: Date, b: Date): boolean {
		return this.getDateString(a) === this.getDateString(b);
	}
	getDateString(date: Date): string {
		return `${date.getDate()}${date.getMonth()}${date.getFullYear()}`;
	}
	toggleWeekNumbers() {
		this.weekNumbers = !this.weekNumbers;
	}
}
