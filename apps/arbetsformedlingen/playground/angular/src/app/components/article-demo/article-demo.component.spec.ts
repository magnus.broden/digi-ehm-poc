import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleDemoComponent } from './article-demo.component';

describe('ArticleDemoComponent', () => {
  let component: ArticleDemoComponent;
  let fixture: ComponentFixture<ArticleDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
