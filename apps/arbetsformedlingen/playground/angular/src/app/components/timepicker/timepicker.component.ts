import { Component, HostListener, OnInit } from '@angular/core';

@Component({
	selector: 'at-timepicker',
	templateUrl: './timepicker.component.html',
	styleUrls: ['./timepicker.component.scss']
})
export class TimepickerComponent implements OnInit {
	constructor() {}

	currentDate = '';

	bookings =  [
		'2022-06-12',
		'2022-05-12',
		'2022-05-16',
		'2022-05-09',
		'2022-06-20'
	];

	ngOnInit(): void {}

	get hasBookings() {
		return JSON.stringify(this.bookings);
	}

	get headingText() {
		return this.currentDate
	}

	@HostListener('document:afOnDateChange', ['$event'])
  onAfOnDateChange(event) {
		this.currentDate = event.detail;
  }
}
