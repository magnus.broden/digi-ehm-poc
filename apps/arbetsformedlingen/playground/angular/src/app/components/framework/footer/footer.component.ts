import { Component, OnInit } from '@angular/core';

import { LayoutBlockVariation, LayoutBlockContainer, LayoutContainerVariation, LogoVariation, LogoColor } from 'arbetsformedlingen-dist';

@Component({
  selector: 'at-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  LayoutBlockVariation = LayoutBlockVariation;
  LayoutBlockContainer = LayoutBlockContainer;
  LayoutContainerVariation = LayoutContainerVariation;
  LogoVariation = LogoVariation; 
  LogoColor = LogoColor;

  constructor() { }

  ngOnInit(): void {
  }

}
