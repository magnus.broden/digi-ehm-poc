import { Component, Input } from '@angular/core';

import { LayoutContainerVariation } from 'arbetsformedlingen-dist';

@Component({
  selector: 'at-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Input() navTemplate: string;

  LayoutContainerVariation = LayoutContainerVariation;

}
