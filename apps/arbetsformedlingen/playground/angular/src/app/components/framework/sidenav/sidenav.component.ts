import { Component, Input } from '@angular/core';

@Component({
  selector: 'at-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent  {
  @Input() navTemplate = '';
  @Input() navLayout = '';
}
