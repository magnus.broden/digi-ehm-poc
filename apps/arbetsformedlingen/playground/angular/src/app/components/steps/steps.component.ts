import { Component } from '@angular/core';

interface Step {
	titel: string;
	text: string;
	active: boolean;
}

@Component({
	selector: 'at-steps',
	templateUrl: './steps.component.html',
	styleUrls: ['./steps.component.scss']
})
export class StepsComponent {
	constructor() {
		this.steps = [
			{ titel: 'Title1', text: 'Första stegets text.', active: true },
			{ titel: 'Title2', text: 'Andra stegets text', active: false }
		];
	}

	steps: Step[];

	// Just to demonstrate that the steps can be changed at any time.
	addStep(): void {
		// Warning, do not use push. Stencil will go into an eternal loop.
		this.steps = [
			...this.steps,
			{
				titel: 'Titel',
				text: 'Det här steget las till manuellt',
				active: false
			}
		];
	}

	removeStep() {
		if (this.steps && this.steps.length > 2) {
			this.steps = this.steps.slice(0, this.steps.length - 1);
		}
	}
}
