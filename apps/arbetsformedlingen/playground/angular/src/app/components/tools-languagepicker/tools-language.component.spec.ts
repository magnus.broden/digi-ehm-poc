import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolsLanguagepickerComponent } from './tools-languagepicker.component';

describe('LanguagepickerComponent', () => {
	let component: ToolsLanguagepickerComponent;
	let fixture: ComponentFixture<ToolsLanguagepickerComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ToolsLanguagepickerComponent]
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ToolsLanguagepickerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
