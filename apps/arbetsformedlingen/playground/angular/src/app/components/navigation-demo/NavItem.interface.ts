export interface NavItem {
    title?: string;
    divider?: string;
    highlight?: boolean;
    children?: [];
}
