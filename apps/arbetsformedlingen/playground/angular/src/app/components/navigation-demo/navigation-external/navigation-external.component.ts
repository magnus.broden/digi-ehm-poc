import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UtilBreakpointObserverBreakpoints, NavigationSidebarVariation, NavigationVerticalMenuVariation } from 'arbetsformedlingen-dist';

import ExternalNavigation from  '../externalNavigation.json';

@Component({
  selector: 'at-navigation-external',
  templateUrl: './navigation-external.component.html',
  styleUrls: ['./navigation-external.component.scss']
})
export class NavigationExternalComponent implements OnInit {
  NavigationSidebarVariation = NavigationSidebarVariation;
  NavigationVerticalMenuVariation = NavigationVerticalMenuVariation;

  @Input() navLayout: string = 'primary';

  private _isMobile: boolean;

  navData = ExternalNavigation;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  get sideNavHeader() {
    return this._isMobile ? 'false' : 'true';
  }

  @HostListener('document:afOnChange', ['$event'])
  onAfOnChange(event) {
    if(event.target.tagName == "DIGI-UTIL-BREAKPOINT-OBSERVER") {
      this._isMobile = event.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
    }
  }

}
