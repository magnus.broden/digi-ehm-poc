import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationIntranetComponent } from './navigation-intranet.component';

describe('NavigationIntranetComponent', () => {
  let component: NavigationIntranetComponent;
  let fixture: ComponentFixture<NavigationIntranetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigationIntranetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationIntranetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
