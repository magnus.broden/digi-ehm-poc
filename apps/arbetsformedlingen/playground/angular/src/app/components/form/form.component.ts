import {
  Component,
  HostListener,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

import {
  FormTextareaValidation,
  FormInputType,
  LayoutColumnsVariation,
} from 'arbetsformedlingen-dist';

interface FormSelectItem {
  name: string;
  value: string;
}

interface FormMessage {
  errorMessage: string;
  successMessage?: string;
}

interface FormErrorListItem {
  id: string;
  message: string;
  valid?: boolean;
}

interface OptionalSkill {
  val: string;
  name: string;
  text: string;
}

@Component({
	selector: 'at-form',
	templateUrl: './form.component.html',
	styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
	@ViewChild('errorList') errorList!: ElementRef;

	submitted = false;
	showErrorList = false;
	showCheckboxList1 = true;
	showRadioGroup = true;

	FormTextareaValidation = FormTextareaValidation;
	FormInputType = FormInputType;
	LayoutColumnsVariation = LayoutColumnsVariation;

	public resp = [
		{
			file_id: '44d88612fea8a8f36de82e1278abb02f',
			status: 'OK',
			details: {
				confidence: 5,
				malware_family: 54768315,
				malware_type: 114,
				severity: 4,
				signature_name: 'Trojan.Win32.Mitaka.TC.a'
			}
		},

		{
			file_id: '3e74f8be1ca4e3acaf843c8b4f5ef6cc',
			status: 'OK',
			details: {
				confidence: 0,
				severity: 0,
				signature_name: ''
			}
		}
	];

	formErrorList: FormErrorListItem[] = [];

	formErrorMessage: { [key: string]: FormMessage } = {
		myName: <FormMessage>{
      errorMessage: 'Fyll i ditt namn',
		},
		myEmail: <FormMessage>{
			errorMessage: 'Fyll i korrekt epost-adress',
		},
		mySkills: <FormMessage>{
			errorMessage: 'Fyll i dina färdigheter',
		},
		myDescription: <FormMessage>{
			errorMessage: 'Fyll i en beskrivning',
		},
		myRole: <FormMessage>{
			errorMessage: 'Välj en roll',
		},
		myExperience: <FormMessage>{
			errorMessage: 'Ange erfarenhet',
		},
		myOptionalSkills: <FormMessage>{
			errorMessage: 'Välj erfarenhetsområden',
		}
	};

	selectItems: FormSelectItem[] = [
		{ name: 'UX-designer', value: 'uxdesigner' },
		{ name: 'Frontend-utvecklare', value: 'frontenddeveloper' },
		{ name: 'Backend-utvecklare', value: 'backenddeveloper' },
		{ name: 'Projektledare', value: 'projectmanager' },
	];

	optionalSkills: OptionalSkill[] = [
		{
			val: 'accessibility',
			name: 'accessibility',
			text: 'Tillgänglighet (Om du redan nu vet att du kommer att börja arbeta eller studera inom 3 månader kommer du inte att ha ett planeringssamtal med oss. )',
		},
		{
			val: 'devops',
			name: 'devops',
      text: 'DevOps',
    },
	];
	public skillForm!: UntypedFormGroup;
  public fileUploadForm!: UntypedFormGroup;
	public myForm!: UntypedFormGroup;

	constructor(

		private fb: UntypedFormBuilder,
		changeDetectorRef: ChangeDetectorRef
	) {
		setTimeout(() => {
			this.showCheckboxList1 = false;
			changeDetectorRef.markForCheck();
		}, 5000);
	}

	testFilterSubmited(event: any): void {
		console.log(event, 'submitted');
	}

	testFilterClosed(event: any): void {
		console.log(event, 'closed');
	}

	ngOnInit(): void {
		this.skillForm = this.fb.group({
			mySkillTitle: ['', Validators.required],
      mySkillLevel: ['', [Validators.min(0), Validators.max(3)]],
    })
		this.myForm = this.fb.group({
			myName: ['', [Validators.required]],
			myEmail: ['', [Validators.email]],
			mySkills: this.fb.array([this.skillForm]),
			myDescription: ['', [Validators.required, Validators.maxLength(60)]],
			myRole: ['', Validators.required],
			myOptionalSkills: this.fb.array([], Validators.required),
			myExperience: ['', Validators.required],
      myFiles: this.fb.array([])
		});

		this.validateForm();

    this.myForm.controls.myExperience.valueChanges.subscribe((e) => console.log(e))
	}

	get mySkills() {
		return this.myForm.controls['mySkills'] as UntypedFormArray;
	}

  get myFiles() {
    return this.myForm.controls['myFiles'] as UntypedFormArray;
  }

	addSkill() {
		const skillForm: UntypedFormGroup = this.fb.group({
			mySkillTitle: [''],
      mySkillLevel: [''],
		});

		this.mySkills.push(skillForm);
	}

	removeSkill(skillIndex: number) {
		this.mySkills.removeAt(skillIndex);
	}

	get myOptionalSkills() {
		return this.myForm.controls['myOptionalSkills'] as UntypedFormArray;
	}

	optionSkillChange(e: any) {
		const val = e.detail.target.value;
		const checked = e.detail.target.checked;

		if (checked) {
			const item = new UntypedFormControl(e.detail.target.value);
			this.myOptionalSkills.push(item);
		} else {
			let i = 0;
			this.myOptionalSkills.controls.forEach((item) => {
				if (item.value == val) {
					this.myOptionalSkills.removeAt(i);
					return;
				}
				i++;
			});
		}
	}

	validateForm() {
		this.formErrorList = [];

		for (const [key] of Object.entries(this.myForm.controls)) {
			if (this.myForm.controls[key].invalid) {
				this.formErrorList.push({
					id: key,
          message: this.formErrorMessage[key].errorMessage,
				});
			}
		}
	}
	debugClicked = 0;
	debugHandler(){
		console.log(this.formErrorList)
		this.formErrorList[0].message = "debug message";
		console.log(this.formErrorList)
		if(this.debugClicked>3){
		this.formErrorList.push({
			id: "DEBUG_TEST",
			message: "NEW DEBUG TEST",
		});
	}

		this.debugClicked++;
	}
	submitButtonHandler() {
		this.myForm.markAllAsTouched();

		this.submitted = true;

		this.validateForm();

		this.showErrorList = !this.myForm.valid ? true : false;

		if (this.showErrorList) {
			(async () => {
				const customElementPromise = customElements
					.whenDefined('digi-form-error-list')
					.then(() => console.log('form-error-list defined'));
				await customElementPromise;
				const el = document.querySelector('digi-form-error-list');
				el?.afMSetHeadingFocus();

				setTimeout(async () => {
					const customElementPromise2 = customElements
						.whenDefined('digi-notification-alert')
						.then(() => console.log('digi-notification-alert defined'));
					await customElementPromise2;
					const el2 = document.querySelector('digi-notification-alert');
					const heading = await el2?.afMGetHeadingElement();
          console.log(heading);
				}, 0);

        el.addEventListener("afOnReady", async (e: CustomEvent) => {
					/*
          Since afOnReady event bubbles, we will also get all the childrens on ready calls, 
          so if we want to use listeners target we either have to use el or do a check to 
          make sure only to handle event with certain targets...*/

					const target = e.target as unknown as HTMLDigiFormErrorListElement;
					if (target && target.tagName?.toLowerCase() === 'digi-form-error-list') {
            console.log("afOnReady event");
            console.log(e);

						await target.afMSetHeadingFocus();
					}


					el.afMSetHeadingFocus();
        })
			})();
		}
	}

	getValidationText(controlName: string) {
		return this.myForm.controls[controlName].valid
			? this.formErrorMessage[controlName]?.successMessage
			: this.formErrorMessage[controlName].errorMessage;
	}



	@HostListener('document:afOnClick', ['$event'])
	onAfOnClick(event: any) {
		if (
			event.target &&
			event.target &&
			event.target.matches('.digi-form-error-list__link')
		) {
			event.detail.preventDefault();
			const linkId = String(event.detail.target.hash).split('#').join('');
			const el = document.getElementById(linkId);
			if (el) {
				el.focus();
			}
		}
	}

	async validateFile(scanResp) {
		await customElements.whenDefined('digi-form-file-upload');
		const fileUpload = document.querySelector('digi-form-file-upload');
		const res = await fileUpload?.afMValidateFile(scanResp);
    console.log(scanResp)

    if(scanResp.status !== 'error') {
      console.log('no error')
      const fileForm: UntypedFormGroup = this.fb.group(scanResp);
      this.myFiles.push(fileForm);
    }
	}

	onUpload(e) {
		e.preventDefault();

		setTimeout(() => {
			const file = e.detail;
			const respIndex = Math.floor(Math.random() * 2);
			if (respIndex == 1) {
				file['status'] = 'OK';
			} else {
				file['status'] = 'error';
			}

			this.validateFile({ ...file, name: e.detail.name });

		}, 500);
	}

	onRemove(e) {
    const arr = [...this.myFiles.value]
    const fileIndex = arr.findIndex((item) => item.id === e.detail.id)
    if(fileIndex >= 0) {
      this.myFiles.removeAt(fileIndex);
    }
	}

	onCancel(e) {
		console.log(e.detail);
	}

	onRetry(e) {
		setTimeout(() => {
			const respIndex = Math.floor(Math.random() * 2);
			this.validateFile({ ...this.resp[respIndex], ...e.detail });
		}, 500);
	}

}
