import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'at-calendar',
	templateUrl: './calendar.component.html',
	styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
	constructor() {}

	ngOnInit(): void {}

	dates: Date[] = [new Date()];
}
