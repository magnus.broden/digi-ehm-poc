import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultifilterPocComponent } from './multifilter-poc.component';

describe('MultifilterPocComponent', () => {
	let component: MultifilterPocComponent;
	let fixture: ComponentFixture<MultifilterPocComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [MultifilterPocComponent]
		}).compileComponents();

		fixture = TestBed.createComponent(MultifilterPocComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
