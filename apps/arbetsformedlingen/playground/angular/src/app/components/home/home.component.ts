import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { ErrorPageStatusCodes } from '@digi/arbetsformedlingen';

@Component({
  selector: 'at-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  ErrorPageStatusCodes = ErrorPageStatusCodes;
    group: UntypedFormGroup;
    maxDate = new Date('2021-03-31');

    mountedDialog = false;
    toggleDialog = false;
    mockItems = Array(30);
  
    constructor(private fb: UntypedFormBuilder) {}
  
    ngOnInit(): void {
      this.group = this.fb.group({
        date1: [null],
        date2: [''],
        date3: [''],
        date4: ['']
      });
    }

    programDate = new UntypedFormControl('')

    showDialog() {
      this.mountedDialog = true;
      
      setTimeout(() => {
        this.toggleDialog = true;
      }, 200)
    }
}
