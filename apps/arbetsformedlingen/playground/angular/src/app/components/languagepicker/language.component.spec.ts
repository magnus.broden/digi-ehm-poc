import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguagepickerComponent } from './languagepicker.component';

describe('LanguagepickerComponent', () => {
	let component: LanguagepickerComponent;
	let fixture: ComponentFixture<LanguagepickerComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [LanguagepickerComponent]
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(LanguagepickerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
