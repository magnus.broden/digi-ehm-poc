import { Pipe, PipeTransform } from '@angular/core';
import { FormInputValidation } from 'arbetsformedlingen-dist';

@Pipe({ name: 'formcontrolInvalid' })
export class FormcontrolInvalidPipe implements PipeTransform {

    transform(invalid: boolean, submitted: boolean) {

        if (invalid === true && submitted) {
            return FormInputValidation.ERROR;
        }

        return FormInputValidation.NEUTRAL;
        
    }

}