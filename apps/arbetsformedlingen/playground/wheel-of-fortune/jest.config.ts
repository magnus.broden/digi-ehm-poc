/* eslint-disable */
export default {
	displayName: 'arbetsformedlingen-playground-wheel-of-fortune',
	preset: '../../../../jest.preset.js',
	setupFilesAfterEnv: ['<rootDir>/src/test-setup.ts'],
	globals: {
		'ts-jest': {
			tsconfig: '<rootDir>/tsconfig.spec.json',
			stringifyContentPathRegex: '\\.(html|svg)$'
		}
	},
	coverageDirectory:
		'../../../../coverage/apps/arbetsformedlingen/playground/wheel-of-fortune',
	transform: {
		'^.+\\.(ts|mjs|js|html)$': 'jest-preset-angular'
	},
	transformIgnorePatterns: ['node_modules/(?!.*\\.mjs$)'],
	snapshotSerializers: [
		'jest-preset-angular/build/serializers/no-ng-attributes',
		'jest-preset-angular/build/serializers/ng-snapshot',
		'jest-preset-angular/build/serializers/html-comment'
	]
};
