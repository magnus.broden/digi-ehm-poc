const express = require('express')
const compression = require('compression')
const helmet = require('helmet')
const cors = require('cors')
const path = require('path')
const app = express()
const port = process.env.PORT || 8080 

const trusted = [
  "'self'",
  '*.gstatic.com',
  '*.googleapis.com',
  '*.rekai.se',
  'af.analytics.elx.cloud',
  '*.piwik.pro',
  '*.directus.app'
];

/**
 * Setup function to set Express server settings.
 * @returns then - run callback after success
 */
const setup = function() {
  try {
    app.use(compression())
    app.use(helmet({
      referrerPolicy: { policy: 'same-origin' },
      contentSecurityPolicy: {
        directives: {
          defaultSrc: trusted,
          connectSrc: [
            'jira.arbetsformedlingen.se'
          ].concat(trusted),
          scriptSrc: [
            "'unsafe-eval'",
            "'unsafe-inline'"
          ].concat(trusted),
          styleSrc: [
            "'unsafe-inline'"
          ].concat(trusted),
          frameSrc: [
          ].concat(trusted),
          fontSrc: [
            'data'
          ].concat(trusted),
          imgSrc: [
            'https:',
            'data:'
          ].concat(trusted),
        }
      }
    }))
    app.use(cors())

    console.info(`---> Enabled security and CORS.`)
    console.warn(`\n\t\t* * * * * * * * * * * * * * * * * * * *

CORS enabled for *, please set to specific domain when running in production!

data: source enabled for images in order to use base64.\nPlease remove all base64 images and then remove this CSP setting!

\t\t* * * * * * * * * * * * * * * * * * * *\n`)

    app.use(express.static(path.join(__dirname, '../../../dist/apps/arbetsformedlingen/docs/www'), { maxAge: 31557600 }))
    app.use('*', express.static(path.join(__dirname, '../../../dist/apps/arbetsformedlingen/docs/www/index.html'), { maxAge: 31557600 }))
    app.get("/service-worker.js", (req, res) => {
      res.sendFile(path.join(__dirname, '../../../dist/apps/docs/arbetsformedlingen/www/service-worker.js'));
    });

    return {
      then: function(callback) {
        callback()
      }
    }
  } catch(err) {
    throw new Error(`Could not setup server!\n${err}`)
  }
}

/**
 * Start function for the server, tries to listen to a specified port.
 */
const start = function() {
  try {
    app.listen(port, () => {
      console.info(`---> Running Digi Docs on port ${port}...`)
    })
  } catch(err) {
    throw new Error(`Could not start server!\n${err}`)
  }
}

// Tries to run the setup, then tries to start the server.
setup().then(start);
