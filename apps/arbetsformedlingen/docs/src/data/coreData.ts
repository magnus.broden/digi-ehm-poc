import { componentCategoryMap } from './categoryData';
import { Category } from '@digi/taxonomies';
import _componentData from '../../../../../dist/libs/arbetsformedlingen/docs.json';
// import { iconsComponent } from './_iconsData';

// _coreData.components.push(iconsComponent)

export const componentData = _componentData;
export const components = componentData.components.map((component) => {
  return {
    ...component, 
    category: component.tag.includes('digi-icon')  
      ? Category.ICON 
      : componentCategoryMap[component.tag] || Category.UNCATEGORIZED
  };
});
