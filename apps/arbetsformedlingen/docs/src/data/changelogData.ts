export const changelogData = `<digi-expandable-accordion af-heading="[19.5.1] - 2023-08-30"     af-expanded="true">
<h3 id="nytt">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-icon"></digi-code>; nya ikoner: <digi-code af-code="digi-icon-screensharing"></digi-code> och <digi-code af-code="digi-icon-screensharing-off"></digi-code></li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Nytt Designmönster: Enkäter och feedbackfunktioner</li>
</ul>
</li>
</ul>
<h3 id="ändrat">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-form-file-upload"></digi-code>; rättat felstavning</li>
<li><digi-code af-code="digi-icon-camera-off"></digi-code>, <digi-code af-code="digi-icon-sign"></digi-code>; tagit bort en mask</li>
<li>Förbättrat mobilupplevelse på introduktionsidan för designmönsterna</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.5.0] - 2023-08-23"     af-expanded="true">
<h3 id="nytt-1">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-expandable-faq"></digi-code>, <digi-code af-code="digi-expandable-faq-item"></digi-code>; Ny variant av komponenten</li>
<li><digi-code af-code="digi-tools-feedback-banner"></digi-code>; Ny komponenten</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li><digi-code af-code="digi-list"></digi-code> - byt ut all list i dokumentationn webben mot komponent</li>
</ul>
</li>
</ul>
<h3 id="ändrat-1">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-bar-chart"></digi-code> - nytt attribut <digi-code af-code="af-variation"></digi-code>, så man kan välja mellan horisontell och vertikal stapeldiagram</li>
<li><digi-code af-code="digi-info-card"></digi-code> - tagit bort en förvirrande felmeddelande på info-kort</li>
<li><digi-code af-code="digi-link-external"></digi-code> - tagit bort router link från komponenten</li>
<li><digi-code af-code="digi-link-external"></digi-code>, <digi-code af-code="digi-link-internal"></digi-code>, <digi-code af-code="digi-link"></digi-code> - la in ny attribut <digi-code af-code="af-describedby"></digi-code> är ny prop i digi-link</li>
<li><digi-code af-code="digi-error-list"></digi-code>, ändring i komponenten, la in attribut <digi-code af-code="af-enable-heading-focus"></digi-code> som kontrollera Heading focus</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.4.0] - 2023-07-05"     af-expanded="true">
<h3 id="nytt-2">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-notification-error-page"></digi-code>; Ny komponent</li>
<li><digi-code af-code="digi-bar-chart"></digi-code>; Ny komponent</li>
<li>Ikonerna - man kan ange <digi-code af-code="aria-labelledby"></digi-code></li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li><digi-code af-code="Felmeddelandesidor"></digi-code> - nytt designmönster</li>
<li><digi-code af-code="Sök och sökfilter"></digi-code> - nytt designmönster</li>
</ul>
</li>
</ul>
<h3 id="ändrat-2">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-typography-heading-jumbo"></digi-code>; Liten justering i marginalen mellan rubrik och linje</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.3.1] - 2023-06-28"    >
<h3 id="ändrat-3">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-util-mutation-observer"></digi-code>, <digi-code af-code="afOptions"></digi-code> kan nu ta emot <digi-code af-code="characterData"></digi-code> utöver <digi-code af-code="childList"></digi-code> &amp; <digi-code af-code="attributes"></digi-code></li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Ny sida Rörligt och ljud - Grafisk profil.</li>
<li>Uppdaterat startsida med ny tillgänglighetsbanner.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.3.0] - 2023-06-21"    >
<h3 id="nytt-3">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li>Nya videoikoner; <digi-code af-code="digi-icon-videocamera"></digi-code>, <digi-code af-code="digi-icon-videocamera-off"></digi-code>, <digi-code af-code="digi-icon-microphone"></digi-code>, <digi-code af-code="digi-icon-microphone-off"></digi-code>, <digi-code af-code="digi-icon-phone-hangup"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Nytt innehåll på startsidan som länkar till tillgänglighetsmatrisen</li>
</ul>
</li>
</ul>
<h3 id="ändrat-4">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-form-label"></digi-code>; Flyttat beskrviningstext innanför etikett</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Ny Piktogram-sida/Whiteboardverktyg</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.2.7] - 2023-06-14"    >
<h3 id="nytt-4">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-quote-single"></digi-code>, <digi-code af-code="digi-quote-multi-container"></digi-code>; Nya komponenter för citat</li>
</ul>
</li>
</ul>
<h3 id="ändrat-5">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-list"></digi-code>; Ändrat hur komponenten läser in HTML-taggar</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.2.6] - 2023-06-13"    >
<h3 id="ändrat-6">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-form-input"></digi-code>, <digi-code af-code="digi-form-input-search"></digi-code>; Åtgärdat bugg med padding</li>
<li><digi-code af-code="digi-tools-languagepicker"></digi-code>; Åtgärdat bugg med fel färger på knappar</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.2.5] - 2023-06-07"    >
<h3 id="ändrat-7">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><p><digi-code af-code="digi-calendar-datepicker"></digi-code>;</p>
<ul>
<li>Fixat bugg med valideringen</li>
</ul>
</li>
<li><p><digi-code af-code="digi-form-select"></digi-code>;</p>
<ul>
<li>Lagt till <digi-code af-code="afOnSelect"></digi-code></li>
</ul>
</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Textkorrektur i sidan för Instruktioner för lista med tillgänglighetsbrister.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.2.4] - 2023-05-31"    >
<h3 id="ändrat-8">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-calendar"></digi-code>;<ul>
<li>La till <digi-code af-code="afInitSelectedDate"></digi-code> för att kunna ställa in förvalt år och månad</li>
</ul>
</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.2.3] - 2023-05-31"    >
<h3 id="ändrat-9">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-calendar"></digi-code>;<ul>
<li>La till <digi-code af-code="afInvalid"></digi-code> och <digi-code af-code="afValidationMessage"></digi-code> så att man manuellt kan styra valideringen.</li>
</ul>
</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Textkorrektur</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.2.2] - 2023-05-24"    >
<h3 id="ändrat-10">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-calendar-datepicker"></digi-code>;<ul>
<li>Ändrade så att man kan välja upp till 10 år fram i tiden.</li>
<li>Fixade så att kalendern lägger sig över allt under, istället för att putta ner det.</li>
<li>Lagt till ett property för att automatiskt stänga kalendern vid valt datum.</li>
</ul>
</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Ändrat innehåll <digi-code af-code="Bilder"></digi-code> under <digi-code af-code="Grafisk profil"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.2.1] - 2023-05-17"    >
<h3 id="ändrat-11">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-navigation-pagination"></digi-code>; Ändrat responsiv funktion och ny property afLimit.</li>
<li><digi-code af-code="digi-layout-container"></digi-code>; Justerat så att grid storlek och paddings stämmer överens med designmönstret.</li>
<li><digi-code af-code="digi-button"></digi-code>; Ny token: --digi--button--align-items.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.2.0] - 2023-05-12"    >
<h3 id="nytt-5">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-list"></digi-code>; En ny komponent för listor med färdiga layouter</li>
</ul>
</li>
</ul>
<h3 id="ändrat-12">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-tools-feedback"></digi-code>; Korrigerat designen så den är i synk med UI-kit.</li>
<li><digi-code af-code="digi-form-file-upload"></digi-code>; Justerat så kontroll av tillåtna filtyper fungerar likadant för input-element och &quot;dra-och-släpp&quot;-funktionen.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.1.1] - 2023-05-03"    >
<h3 id="ändrat-13">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-navigation-context-menu"></digi-code>; Förbättrad tillgänglighet i Context menu.</li>
<li><digi-code af-code="digi-button"></digi-code>; Lagt till attributen &#39;afRole&#39; och &#39;afAriaChecked&#39;.</li>
<li><digi-code af-code="digi-layout-columns"></digi-code>; Lagt till möjligheten att ange en kolumn.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.1.0] - 2023-04-27"    >
<h3 id="nytt-6">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-tools-feedback"></digi-code>; En komponent som möjliggör insamling av användarfeedback</li>
<li><digi-code af-code="digi-form-category-filter"></digi-code>; Kategorifilter tar in en lista av kategorier och skickar ut en filtrerad lista på kategorier baserat på vilka kategorier användaren väljer.</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Nytt designmönster om agentiva tjänster och AI</li>
</ul>
</li>
</ul>
<h3 id="ändrat-14">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-calendar"></digi-code>; En fix för att uppdatera datumen korrekt när man sätter en start månad</li>
<li>Alla komponenter som har en publik metod har fått en nytt event som heter <digi-code af-code="afOnReady"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.0.4] - 2023-04-19"    >
<h3 id="ändrat-15">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-navigation-tabs"></digi-code>; Vi sätter textfärgen på flikarna enligt design tokens, annars kan det bli olika beteende beroende på enhet och webbläsare.</li>
<li><digi-code af-code="custom-elements.json"></digi-code>; Vi inkluderar denna fil i npm-paketet så kan man aktivera intellisense. Exempel för hur man gör i VS Code finns på sidan <a href="/kom-i-gang/jobba-med-digi-core">Jobba med Digi Core</a></li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Korrigerat kodexempel för <digi-code af-code="digi-form-select"></digi-code> och attributet <digi-code af-code="af-description"></digi-code></li>
<li>Förbättrat layout i mobilen på tillgänglighetsmatrisen som finns på sidan <digi-code af-code="Om digital tillgänglighet"></digi-code></li>
<li>Lyft upp <digi-code af-code="Grafisk profil"></digi-code> i menystrukturen</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.0.3] - 2023-04-05"    >
<h3 id="ändrat-16">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-navigation-pagination"></digi-code>; Ändrat så aria-current och aria-label hamnar på korrekt html-element</li>
<li><digi-code af-code="digi-icon-exclamation-triangle-warning"></digi-code>; Lagt in så komponentens svg-kod får unika id:n</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Korrigerat kodexempel för digi-datepicker, digi-form-error-list</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.0.2] - 2023-03-29"    >
<h3 id="ändrat-17">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-calendar"></digi-code>; Fixade en bug där valda datum inte initierades korrekt</li>
<li><digi-code af-code="digi-navigation-breadcrumbs"></digi-code>; Ändrat standardtext i aria-label till svenska</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.0.1] - 2023-03-22"    >
<h3 id="ändrat-18">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-logo"></digi-code>; Korrigerat färg för varianten med systemnamn med inverterad textfärg</li>
<li><digi-code af-code="digi-calendar"></digi-code>; Fixade en bug där valda datum inte initierades korrekt</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[19.0.0] - 2023-03-22"    >
<h3 id="nytt-7">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-tools-languagepicker"></digi-code>; Ny komponent. Språkväljaren tillåter dig ge användaren möjlighet att ändra språk.</li>
<li><digi-code af-code="digi-info-multi-card"></digi-code>; Ny komponent. Multikort är kort som är tänkta att användas ihop i layouter med samma sorts kort på en rad, med kurerat innehåll och längder på texter</li>
</ul>
</li>
</ul>
<h3 id="ändrat-19">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code>;</p>
<ul>
<li><digi-code af-code="digi-info-card"></digi-code>; <span class="breaking-tag">Breaking</span> Vi har justerat namnstruktur för att spegla bättre hur och när respektive variant ska användas. För hjälp med att gå över till denna version, se migreringsguide på <a href="/komponenter/digi-info-card/oversikt">komponentsidan</a></li>
</ul>
</li>
<li><p><digi-code af-code="@digi/arbetsformedlingen-angular"></digi-code></p>
<ul>
<li><digi-code af-code="digi-link"></digi-code>, <digi-code af-code="digi-link-internal"></digi-code>, <digi-code af-code="digi-link-external"></digi-code>, <digi-code af-code="digi-link-button"></digi-code>; För bättre stöd med routerlinks i Angular så har vi lagt in stöd för att kunna använda komponenten som en container för ett vanligt länkelement. Detta behövs för att kunna stödja tangentbordsnavigation på korrekt vis.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.5.1] - 2023-03-21"    >
<h3 id="ändrat-20">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-loader-spinner"></digi-code>; Åtgärdat bugg som uppstod vid behov av laddningsindikatorer i <digi-code af-code="digi-navigation-tabs"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.5.0] - 2023-03-15"    >
<h3 id="nytt-8">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-calendar-datepicker"></digi-code>; Ny komponent</li>
</ul>
</li>
</ul>
<h3 id="ändrat-21">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-chart-line"></digi-code>; Åtgärdat en bugg som ritade ut streckade linjen fel om diagrammet ändrade storlek. Ändrat muspekaren när man hovrar över en legend</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.4.2] - 2023-03-09"    >
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen-angular"></digi-code><ul>
<li>Uppdaterat <digi-code af-code="rxjs"></digi-code> som peerDependency till &quot;^7.8.0&quot;</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.4.1] - 2023-03-08"    >
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-tag"></digi-code>; Lagt till möjlighet att sätta attributet aria-label på en tagg med <digi-code af-code="af-aria-label"></digi-code></li>
<li><digi-code af-code="digi-progressbar"></digi-code>; Korrigerat padding på textetiketten så den är i synk med UI-kittet</li>
<li><digi-code af-code="digi-form-checkbox"></digi-code>; Åtgärdat bugg som syns när komponenten validerar till &quot;Fel&quot; eller Varning&quot;</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Uppdaterat länkar till vår interna sharepoint på sidorna under &quot;Tillgänglighet/Process för tillgänglighetsredogörelse&quot;</li>
<li>Uppdaterat dokumentation för <digi-code af-code="digi-tag"></digi-code> med riktlinjer för tillgänglighet med nya attributet <digi-code af-code="af-aria-label"></digi-code></li>
<li>Förbättrat tillgängligheten på filtren i Tillgänglighetslistan</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.4.0] - 2023-03-03"    >
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li>Uppdaterat till Stencil 3.0</li>
<li><digi-code af-code="digi-typography"></digi-code>; Ändrat storlek på länk så den följer textstorleken den ligger i</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/arbetsformedlingen-angular"></digi-code></p>
<ul>
<li>Lagt till Angular 15 som peerDependency så byggen i DevOps-miljöer ska fungera om man uppdaterat till senaste Angular-versionen.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.3.2] - 2023-03-01"    >
<h3 id="ändrat-22">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="Digi-form-filter"></digi-code>; Fixat bugg med att sidan scrollar när man navigerar med tangentbordet</li>
<li><digi-code af-code="Digi-form-checkbox"></digi-code>; Fixat ikonjustering, justerat fokusindikering. Fixat bugg där sidan hoppade till top vid markering/klick av fält</li>
<li><digi-code af-code="Digi-form-radiobutton"></digi-code>; Justerat fokusindikering. Fixat bugg där sidan hoppade till top vid markering/klick av fält</li>
<li><digi-code af-code="Digi-form-radiogroup"></digi-code>; Buggfix med att den triggar change på init.</li>
<li><digi-code af-code="Digi-form-fieldset"></digi-code>; Justeringar padding runt fieldset</li>
<li><digi-code af-code="Digi-navigation-vertical-menu"></digi-code>; Bugg i af-variation=&quot;secondary&quot;, blev fel bakgrundsfärg på öppnad sektion.</li>
<li><digi-code af-code="Digi-form-input"></digi-code>; Lagt till nytt attribut inputmode <digi-code af-code="af-inputmode"></digi-code>.</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li><digi-code af-code="Digi-form-input-details"></digi-code>; Lagt till dokumentation för af-inputmode. Även lagt till en beskrivning för hur och när man ska använda den.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.3.1] - 2023-02-24"    >
<h3 id="ändrat-23">Ändrat</h3>
<ul>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Uppdaterat dokumentation och designmönster för knappar och vissa formulärkomponenter</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.3.0] - 2023-02-22"    >
<h3 id="nytt-9">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-icon-calender-alt-alert"></digi-code>; Lagt till en ny ikon</li>
</ul>
</li>
</ul>
<h3 id="ändrat-24">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-logo"></digi-code>; Justerat färger</li>
<li><digi-code af-code="digi-link"></digi-code>; Åtgärdat bugg som gjorde att man inte kunde ändra textstorlek</li>
<li><digi-code af-code="digi-form-input-search"></digi-code>; Anger man af-id så sätts id på input-elementet</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Uppdaterat sidan om vår grafiska profil, inklusive Do:s and Don&#39;t:s</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.2.1] - 2023-02-16"    >
<h3 id="ändrat-25">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-chart"></digi-code>; Lagt till d3 som en peerdependency till paketet</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.2.0] - 2023-02-15"    >
<h3 id="nytt-10">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-chart"></digi-code>; Ny komponent för diagram</li>
<li><digi-code af-code="digi-icon-chart"></digi-code>, <digi-code af-code="digi-icon-table"></digi-code>; Nya ikoner</li>
</ul>
</li>
</ul>
<h3 id="ändrat-26">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><p><digi-code af-code="digi-dialog"></digi-code></p>
<ul>
<li>Bytt placering på knapparna enligt designmönster.</li>
<li>Fixat bugg med fokushantering.</li>
</ul>
</li>
<li><p><digi-code af-code="digi-link-button"></digi-code></p>
<ul>
<li>Uppdaterat paddings och marginaler</li>
<li>Lagt till val för full bredd, extra storlek och möjligheter att gömma ikon .</li>
</ul>
</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Lagt ny sektion i startsida för öppen källkod</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.1.2] - 2023-02-08"    >
<h3 id="ändrat-27">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li><digi-code af-code="digi-expandable-accordion"></digi-code>,<digi-code af-code="digi-expandable-faq-item"></digi-code>; Lagt till så innehållet får visibility: hidden när den är stängd, så man inte kan navigera dit med tangetbord då.</li>
<li><digi-code af-code="digi-button"></digi-code>: Knappar med ikoner får fel höjd på mac</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/arbetsformedlingen-angular"></digi-code></p>
<ul>
<li>Lagt till value accessor för digi-form-radiogroup</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Tillgänglighetsredogörelsen, vi har lyckats släcka tre punkter</li>
<li>Lagt till &quot;Validering&quot; under Designmönster-landningssidan</li>
<li>Uppdaterat bilder under formulär-designmönster</li>
<li>Ändring av länkar till sharepoint i processen för tillgänglighetsredogörelse</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.1.1] - 2023-02-02"    >
<h3 id="nytt-11">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-icon-validation-warning"></digi-code>; Ny ikon</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.1.0] - 2023-02-01"    >
<h3 id="nytt-12">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-icon-notification-error"></digi-code>, <digi-code af-code="digi-icon-notification-info"></digi-code>, <digi-code af-code="digi-icon-notification-warning"></digi-code>; <digi-code af-code="digi-icon-notification-success"></digi-code>, <digi-code af-code="digi-icon-validation-error"></digi-code>, <digi-code af-code="digi-icon-validation-success"></digi-code>; Nya ikoner</li>
</ul>
</li>
</ul>
<h3 id="ändrat-28">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/arbetsformedlingen"></digi-code><ul>
<li><digi-code af-code="digi-form-error-list"></digi-code>; Alla ikoner är uppdaterade och justerad design</li>
<li><digi-code af-code="digi-form-validation-message"></digi-code>; Alla ikoner är uppdaterade</li>
<li><digi-code af-code="digi-form-checkbox"></digi-code>; Lagt till varningsvalidering</li>
<li><digi-code af-code="digi-navigation-vertical-menu-item"></digi-code>; Nytt attribut för att kunna ange aria-current</li>
<li><digi-code af-code="digi-code-example"></digi-code>; aria-pressed ändrat till aria-expanded</li>
<li><digi-code af-code="Ikoner"></digi-code>; Uppdaterat så att man ska kunna justera storleken på ikonerna</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[18.0.0] - 2023-01-25"    >
<h3 id="nytt-13">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/arbetsformedlingen"></digi-code></p>
<ul>
<li>Ersätter <digi-code af-code="@digi/core"></digi-code>. Innehåller förutom komponenterna även de delar som tidigare låg i dessa npm-paket: <digi-code af-code="@digi/fonts"></digi-code>, <digi-code af-code="@digi/styles"></digi-code>, <digi-code af-code="@digi/design-tokens"></digi-code></li>
</ul>
</li>
<li><p><digi-code af-code="@digi/arbetsformedlingen-angular"></digi-code></p>
<ul>
<li>Ersätter <digi-code af-code="@digi/core-angular"></digi-code><br>
<span class="breaking-tag">Breaking</span> Kräver minst version 14 av Angular</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/arbetsformedlingen-react"></digi-code></p>
<ul>
<li>Ersätter <digi-code af-code="@digi/core-react"></digi-code></li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Uppdaterat alla komponenter med kodexemplen för användning i React</li>
</ul>
</li>
<li><p><digi-code af-code="digi-form-receipt"></digi-code>; Ny komponent för att visa en kvittens för att bekräfta ett korrekt slutfört formulär (eller flöde).</p>
</li>
</ul>
<h3 id="ändrat-29">Ändrat</h3>
<ul>
<li><digi-code af-code="Dokumentation"></digi-code>;<ul>
<li>Guiderna under <digi-code af-code="Kom igång"></digi-code> är uppdaterade</li>
</ul>
</li>
<li><digi-code af-code="digi-link"></digi-code>, <digi-code af-code="digi-link-internal"></digi-code>, <digi-code af-code="digi-link-external"></digi-code>; Justerat så länkar med ikon inte har linje under länken.</li>
<li><digi-code af-code="@digi/core"></digi-code>;<ul>
<li>har ersatts av <digi-code af-code="@digi/arbetsformedlingen"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="@digi/core-angular"></digi-code>;<ul>
<li>har ersatts av <digi-code af-code="@digi/arbetsformedlingen-angular"></digi-code> och modulen för att läsa in komponenterna har bytt namn till <digi-code af-code="DigiArbetsformedlingenAngularModule"></digi-code>.</li>
</ul>
</li>
<li><digi-code af-code="@digi/core-react"></digi-code>;<ul>
<li>har ersatts av <digi-code af-code="@digi/arbetsformedlingen-react"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="@digi/fonts"></digi-code>, <digi-code af-code="@digi/styles"></digi-code>, <digi-code af-code="@digi/design-tokens"></digi-code>;<ul>
<li>Dessa paket hittas i <digi-code af-code="@digi/arbetsformedlingen"></digi-code> framöver</li>
</ul>
</li>
<li><span class="breaking-tag">Breaking</span> Paketen hämtas från ett annat npm-register så man behöver lägga till denna rad i projektets <digi-code af-code=".npmrc"></digi-code>-fil: <digi-code af-code="@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/"></digi-code></li>
<li><span class="breaking-tag">Breaking</span> Css/Scss - vi distibuerar endast en css-fil framöver. Den går bra att läsa in från en scss-fil också. Sökvägen är ändrad till: <digi-code af-code="@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css"></digi-code></li>
<li><span class="breaking-tag">Breaking</span> Typsnitten - sökvägen till css/scss-filen är ändrad till: <digi-code af-code="@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.css"></digi-code>. Typsnittsfilerna ligger under: <digi-code af-code="@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/assets/fonts"></digi-code></li>
<li><span class="breaking-tag">Breaking</span> Enums - enums har flyttat till nya paketet, så man behöver peka mot: <digi-code af-code="@digi/arbetsformedlingen"></digi-code>, ex: <digi-code af-code="import { EnumName1, EnumName2 } from '@digi/arbetsformedlingen';"></digi-code></li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[17.3.3] - 2022-12-23"    >
<h3 id="nytt-14">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><p><digi-code af-code="digi-button"></digi-code>; lagt till möjlighet att ange id-attributet på knappen med attributet <digi-code af-code="af-id"></digi-code></p>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
</li>
<li><p>&quot;Validering&quot;. Ny sida under Designmönster.</p>
</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[17.3.2] - 2022-12-14"    >
<h3 id="ändrat-30">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-form-file-upload"></digi-code>; Lämnar beta-stadiet.</li>
<li><digi-code af-code="digi-expandable-accordion"></digi-code>, <digi-code af-code="digi-expandable-faq-item"></digi-code>; åtgärdat så att innehållet inte går att navigera till när ytan är stängd.</li>
<li><digi-code af-code="digi-calendar"></digi-code>, <digi-code af-code="digi-form-file-upload"></digi-code>, <digi-code af-code="digi-navigation-vertical-menu-item"></digi-code>; knappar har fått attributet <digi-code af-code="type="button""></digi-code> för att undvika att formulär postas.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Text- och bild-korr och förtydligande av användande av knappar i mobil.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[17.3.1] - 2022-12-07"    >
<h3 id="ändrat-31">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-navigation-tabs"></digi-code>; Lagt till nytt attribut, <digi-code af-code="afPreventScrollOnFocus"></digi-code>. Nytt event <digi-code af-code="afOnTabsReady"></digi-code>.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Uppdaterat exempel för <digi-code af-code="digi-progress-steps"></digi-code> och <digi-code af-code="digi-progress-step"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[17.3.0] - 2022-11-30"    >
<h3 id="ändrat-32">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-navigation-vertical-menu-item"></digi-code>; Ändrat så vald sida är markerad i font-weight bold istället för semibold och i svart färg istället för länkfärg</li>
<li><digi-code af-code="digi-logo"></digi-code>; Justerat font-weight till 700 istället för 800</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[17.2.0] - 2022-11-23"    >
<h3 id="nytt-15">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-icon-solid-heart"></digi-code>; Ny ikon</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Tillgänglighetsmatris - en ny funktion på sidan &quot;Tillgänglighet/Om digital tillgänglighet&quot;</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[17.1.0] - 2022-11-09"    >
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-form-file-upload"></digi-code>; Ny styling för mobilläge</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Ny sortering för design tokens</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[17.0.0] - 2022-11-02"    >
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><span class="breaking-tag">Breaking</span> <digi-code af-code="digi-form-filter"></digi-code>; Vi har lagt till eventet <digi-code af-code="afOnFilterClosed"></digi-code>. Eventet skickas när filtret stängs utan att valda alternativ bekräftats. Tidigare skickades eventet <digi-code af-code="afOnSubmitFilters"></digi-code> på både vid bekräftelse och när man stängde ned filtret utan att bekräfta.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.8.2] - 2022-10-26"    >
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-logo"></digi-code>; Åtgärdat storlek på system-logotyp i mobila enheter</li>
<li><digi-code af-code="digi-form-file-upload"></digi-code>; Ändrat validering enligt ny API-struktur. Man kan få ut alla filer som är uppladdade och validerade</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.8.1] - 2022-10-19"    >
<h3 id="ändrat-33">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-dialog"></digi-code>; Åtgärdat problem med att man inte kan sätta fokus på formulärelement i dialogen</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Korrigerat texter på sidorna under &quot;Kom igång/Jobba med Digi Core&quot;</li>
<li>Uppdaterat texter på sidan &quot;Process för tillgänglighetsredogörelse&quot; under &quot;Tillgänglighet&quot;</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.8.0] - 2022-10-12"    >
<h3 id="nytt-16">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-icon-chat"></digi-code>, <digi-code af-code="digi-icon-upload"></digi-code>, <digi-code af-code="digi-icon-notification-error"></digi-code>, <digi-code af-code="digi-icon-validation-error"></digi-code>; Nya ikoner</li>
</ul>
</li>
</ul>
<h3 id="ändrat-34">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-expandable-accordion"></digi-code>,<digi-code af-code="digi-expandable-faq-item"></digi-code>; Löst bugg med att text på flera rader blir centrerat</li>
<li><digi-code af-code="digi-button"></digi-code>; Lagt till möjlighet att välja fullbredd på knappen i mindre enheter</li>
<li>Vi har lagt till aria-hidden=&quot;true&quot; på alla ikoner</li>
<li><digi-code af-code="digi-navigation-vertical-menu"></digi-code>; Lagt till en metod för att sätta aktiv nivå vid dynamisk inladdning</li>
<li><digi-code af-code="digi-link"></digi-code>, <digi-code af-code="digi-link-internal"></digi-code>, <digi-code af-code="digi-link-external"></digi-code>, <digi-code af-code="digi-link-button"></digi-code>; Förbättrat länkhantering vid dynamisk routing i t.ex. angular och react</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Korrigerat text på sidan &quot;Designmönster/Knappar&quot; kring riktlinjerna om förhållandet mellan primär och sekundär knapp</li>
<li>Uppdaterat texter på sidan &quot;Tillgänglighet/process för tillgänglighetsredogörelse&quot;</li>
<li>Uppdaterat texter på sidan &quot;Tillgänglighet/Nivåer i WCAG&quot;</li>
<li>Åtgärdat tillgänglighetsrelaterade buggar</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.7.4] - 2022-09-28"    >
<h3 id="ändrat-35">Ändrat</h3>
<ul>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Uppdaterat sidan &quot;Digi Tokens&quot; under &quot;Om designsystemet&quot;.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-form-error-list"></digi-code>; Löst bugg med fokusfunktion på rubrik.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.7.3] - 2022-09-21"    >
<h3 id="ändrat-36">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-form-file-upload"></digi-code>; Lagt in så man kan ändra rubriken. Små justeringar i designen.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Ny design på tillgänglighetslistan.</li>
<li>Lagt in så man kan se en lista över alla komponenter samt filtrera bland dessa på sidan &quot;Komponenter/Om vårt komponentbibliotek&quot;.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.7.2] - 2022-09-20"    >
<h3 id="ändrat-37">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-form-filter"></digi-code>; Löst bugg om man sätter id på kryssruta. Felet kom efter vi lagt in <digi-code af-code="digi-util-mutation-observer"></digi-code> för att lyssna efter ändringar i lista</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.7.1] - 2022-09-14"    >
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-form-filter"></digi-code>; Tillgänglighetsförbättringar.</li>
<li><digi-code af-code="digi-dialog"></digi-code>; Lagt till en slot ovanför rubrik.</li>
<li><digi-code af-code="digi-form-label"></digi-code>; Gjort det möjligt att språkanpassa texter.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>&quot;Knappar&quot;. Ny sida under Designmönster.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.7.0] - 2022-09-07"    >
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-expandable-accordion"></digi-code>, <digi-code af-code="digi-expandable-faq-item"></digi-code>; Åtgärdat bugg med typsnitt. Fixat så den inte triggar submit på formulär.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>&quot;Digi Core React&quot;. Ny sida för hur du kommer igång med React-paketet.</li>
<li>&quot;Grid och brytpunkter&quot;. Ny sida under Designmönster.</li>
<li>&quot;Grid Poc&quot;. Live-exempel för vår grid.</li>
<li>Nya sidor under &quot;Processen för tillgänglighetsredogörelsen&quot;</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-react"></digi-code></p>
<ul>
<li>Nytt bibliotek för att använda våra komponenter i React.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.6.1] - 2022-09-02"    >
<ul>
<li><digi-code af-code="@digi/dialog"></digi-code><ul>
<li>Bugfix with height in small devices</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.6.0] - 2022-08-30"    >
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="Digi-loading-spinner"></digi-code>; Ny komponent.</li>
<li><digi-code af-code="Digi-calendar"></digi-code>; Uppdateringar av layout samt stöd för att välja flera dagar</li>
<li><digi-code af-code="Digi-dialog"></digi-code>; Buggfixar på fokusfällan.</li>
<li><digi-code af-code="Digi-expandable-accordion"></digi-code>, <digi-code af-code="digi-expandable-faq-item"></digi-code>, <digi-code af-code="digi-form-validation-message"></digi-code>, <digi-code af-code="digi-navigation-pagination"></digi-code>, <digi-code af-code="digi-navigation-sidebar"></digi-code>, <digi-code af-code="digi-tag"></digi-code>; Korrigerat så aria-hidden får ett värde.</li>
<li><digi-code af-code="Digi-form-checkbox"></digi-code>, <digi-code af-code="digi-form-input"></digi-code>, <digi-code af-code="digi-form-textarea"></digi-code>; Lagt till aria-invalid.</li>
<li><digi-code af-code="Digi-form-input"></digi-code>, <digi-code af-code="digi-form-select"></digi-code>, <digi-code af-code="digi-form-textare"></digi-code>; Lagt till id-attribut på valideringsmeddelandet.</li>
<li><digi-code af-code="Digi-form-filter"></digi-code>; Lagt till aria-describedby på knappen som öppnar listan.</li>
<li><digi-code af-code="Digi-navigation-breadcrumbs"></digi-code>; Lagt in så den går att uppdatera dynamiskt.</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Länk i sidfoten till Open Source koden i GitLab.</li>
<li>Tagit bort länkarna &quot;Tips för testa&quot; i tillgänglighetslistan. Dessa kommer inkluderas direkt på sidan.</li>
<li>Tagit bort länk &quot;Testmetoder&quot; i navigationen. Testmetoder kommer inkluderas i tillgänglighetslistan.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.5.1] - 2022-08-08"    >
<ul>
<li><digi-code af-code="@digi/core-angular"></digi-code><ul>
<li>Löser buggfix med saknade moduler. Föregående version hänvisar till fel version av <digi-code af-code="@digi/core"></digi-code>.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.5.0] - 2022-07-08"    >
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-form-file-upload"></digi-code>; Ny komponent för filuppladdning.</li>
<li><digi-code af-code="digi-dialog"></digi-code>; Ny komponent för modala fönster.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Uppdatering av lagtexter under tillgänglighet.</li>
<li>Förtydligande av vilka webbläsare vi stödjer.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.4.1] - 2022-06-28"    >
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-calendar-week-view"></digi-code>; Buggfix vid hover fixad för Safari på mac.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Designmönster, ny sidstruktur</li>
<li>Introduktion; Formulär; Spacing; nya sidor under Designmönster</li>
<li>Aktiverat webbanalys med Matomo</li>
<li>Tillgänglighetslistan; små textjusteringar</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.4.0] - 2022-06-15"    >
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-form-radiogroup"></digi-code>; Komponent som håller värdet för radiobuttons.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Kom igång med Digi UI-kit; ny sida under Kom igång</li>
<li>Textuppdateringar i tillgänglighetschecklistan</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.3.0] - 2022-06-07"    >
<h3 id="nytt-17">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-calendar-week-view"></digi-code>; Komponent för att visa en veckoöversikt.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-38">Ändrat</h3>
<ul>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Uppdaterade kodexempel<ul>
<li>Navigationskomponenter</li>
</ul>
</li>
<li>Riktlinjer för animationer</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.2.1] - 2022-06-03"    >
<h3 id="ändrat-39">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-progressbar"></digi-code>; Åtgärdade en bugg med aktiv indikering</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.2.0] - 2022-05-31"    >
<h3 id="nytt-18">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-icon-filter"></digi-code>; Ny ikon</li>
</ul>
</li>
</ul>
<h3 id="ändrat-40">Ändrat</h3>
<ul>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Uppdaterade kodexempel<ul>
<li>Layoutkomponenter</li>
</ul>
</li>
<li>Uppdateringar på startsidan. Snabbgenvägar till landningssidor</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.1.2] - 2022-05-18"    >
<h3 id="ändrat-41">Ändrat</h3>
<ul>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Färgsidan under grafisk profil är uppdaterad enligt varumärket</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.1.1] - 2022-05-17"    >
<h3 id="ändrat-42">Ändrat</h3>
<ul>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Uppdaterade kodexempel<ul>
<li>Typografikomponenter</li>
<li>Mediakomponenter</li>
<li>Tabellkomponent</li>
</ul>
</li>
<li>Tillgänglighetsförbättringar på logotyplänk</li>
<li>Tillgänglighetslistan har fått funktionalitet för att importera csv-fil</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.1.0] - 2022-05-03"    >
<h3 id="ändrat-43">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li>Live-exempel täcker nu fullbredd</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Uppdaterade kodexempel<ul>
<li>Formulärkomponenter</li>
<li>Förloppsmätarkomponenter</li>
<li>Kalenderkomponenten</li>
<li>Kodkomponenter</li>
<li>Kortkomponenter</li>
<li>Logotypkomponenten</li>
<li>Länkkomponenter</li>
<li>Taggkomponenten</li>
<li>Utfällbartkomponenter</li>
</ul>
</li>
<li>Title-texter i webbläsaren sätts nu korrekt</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.0.1] - 2022-04-29"    >
<h3 id="ändrat-44">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-expandable-accordion"></digi-code>, <digi-code af-code="digi-expandable-faq-item"></digi-code>; Lagt in en knapp i rubriken för att interaktion med skärmläsare ska fungera korrekt.</li>
<li><digi-code af-code="digi-button"></digi-code>; Lagt till attributen <digi-code af-code="afAriaControls"></digi-code>, <digi-code af-code="afAriaPressed"></digi-code> och <digi-code af-code="afAriaExpanded"></digi-code>.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.0.0] - 2022-04-26"    >
<h3 id="nytt-19">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/design-tokens"></digi-code>;<br><em>Designtokens används för att style:a applikationer enligt gemensam nomenklatur och struktur.</em></p>
<ul>
<li>Library som innehåller designtokens i jsonformat och förmåga att exportera dessa i olika format (e.g. CSS, SCSS, JSON, JS, m.fl.).</li>
<li>Designtokens finns i 3 nivåer; (Component-tokens finns bara i <digi-code af-code="@digi/core"></digi-code> och <digi-code af-code="@digi/styles"></digi-code>, <digi-code af-code="@digi/design-tokens"></digi-code> innehåller bara global och brand)<ul>
<li><strong>global</strong> - skalor av färger, avstånd m.m.,</li>
<li><strong>brand</strong> - global tokens applicerade i olika kontexter, t.ex. color text primary, padding medium,</li>
<li><strong>component</strong> - komponenters specifika applicering av brand-tokens, t.ex. button color background primary.</li>
</ul>
</li>
<li>Alla designtokens hanteras i grunden här och inte längre i <digi-code af-code="@digi/styles"></digi-code> (som nu endast konsumerar designtokens i css/scss-format och exporterar vidare).</li>
<li>Olika applikationer kan nyttja designtokens i olika format, dessa format går att konfigurera direkt i <digi-code af-code="@digi/design-tokens"></digi-code> för att kunna exporteras vid behov - synka med designsystemet för att få till er applikations format!</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li>Ny struktur på komponenter som bättre följer en gemensam struktur och arkitektur. i Styles-mappen i komponentens mapp finns två filer främst; <digi-code af-code="<komponent>.variables.scss"></digi-code> (innehåller designtokens för komponenten) och <digi-code af-code="<komponent>.variations.scss"></digi-code> (innehåller mixins som applicerar designtokens beroende på komponentens aktiva variation).</li>
<li><span class="breaking-tag">Breaking</span> Designtokens för komponenter skrivs och hanteras nu direkt i <digi-code af-code="@digi/core"></digi-code> och inte längre som SCSS-filer i styles. Dessa variabler/tokens exporteras dock till <digi-code af-code="@digi/styles"></digi-code> under Components.</li>
<li>Nytt verktyg för att skapa komponenter, kör kommandot <digi-code af-code="npm run generate-component:core"></digi-code> och följ dialogen för att skapa en ny komponent i <digi-code af-code="@digi/core"></digi-code> enligt den nya strukturen, med exempelfiler.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li><span class="breaking-tag">Breaking</span> Tagit bort filerna <digi-code af-code="_entry.scss"></digi-code> och <digi-code af-code="digi.scss"></digi-code>, och ersatt dessa med <digi-code af-code="digi-styles.custom-properties.scss"></digi-code> (för alla designtokens i css/scss-format), <digi-code af-code="digi-styles.utilities.scss"></digi-code> (för alla functions och mixins, e.g. a11y--sr-only) samt <digi-code af-code="digi-styles.scss"></digi-code>(för allt från <digi-code af-code="@digi/styles"></digi-code> samtidigt, ersätter i princip <digi-code af-code="digi.scss"></digi-code>).</li>
<li>CSS-fil som innehåller alla designtokens/custom-properties (global, brand och component) samt CSS-fil som innehåller alla utility-classes finns under dist-mappen.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-45">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><span class="breaking-tag">Breaking</span> Alla enums för storlekar (S, M och L) har nu döpts om till sina fullständiga namn (SMALL, MEDIUM och LARGE).<br><strong>Se till att uppdatera era applikationer där ni använder dessa!</strong></li>
<li><span class="breaking-tag">Breaking</span> Alla komponenters custom properties/designtokens har fått uppdaterade namn. Dessa kan ni se under respektive komponent på dokumentations-sajten.<br><strong>Se till att uppdatera era applikationer där ni använder dessa!</strong></li>
<li>Använder nu nya filer i <digi-code af-code="@digi/styles"></digi-code> för variables och utilities. Laddar in css-variabler i roten/global.</li>
<li><span class="breaking-tag">Breaking</span> Ikoner har fått uppdaterad struktur för sina designtokens. Nu behöver du override:a variabeln direkt på komponenten, precis som för alla andra komponenter. Om du har en dynamisk ikon så kan du hitta den i css via t.ex. <digi-code af-code="[slot^='icon']"></digi-code> (för både icon och icon-secondary).<br><strong>Se till att uppdatera era applikationer där ni använder dessa!</strong></li>
<li><span class="breaking-tag">Breaking</span> Följande komponenter har fått uppdaterade enums: <digi-code af-code="digi-button"></digi-code>, <digi-code af-code="digi-calendar"></digi-code>, <digi-code af-code="digi-form-input"></digi-code>, <digi-code af-code="digi-form-input-search"></digi-code>, <digi-code af-code="digi-form-select"></digi-code>, <digi-code af-code="digi-form-textarea"></digi-code>, <digi-code af-code="digi-link"></digi-code>, <digi-code af-code="digi-link-button"></digi-code>, <digi-code af-code="digi-link-internal"></digi-code>, <digi-code af-code="digi-link-external"></digi-code>, <digi-code af-code="digi-logo"></digi-code>, <digi-code af-code="digi-notification-alert"></digi-code>, <digi-code af-code="digi-table"></digi-code>, <digi-code af-code="digi-tag"></digi-code>, <digi-code af-code="digi-typography"></digi-code>, <digi-code af-code="digi-breakpoint-observer"></digi-code>.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li><span class="breaking-tag">Breaking</span> Innehåller numera inga SCSS-variabler för komponenterna utan dessa är ersatta med customproperties (css-variabler). Källan till dessa är flyttade till respektive komponent i <digi-code af-code="@digi/core"></digi-code> och exporteras sedan tillbaka till <digi-code af-code="@digi/styles"></digi-code>.</li>
<li>Värdet på flera variabler har justerats, t.ex. avstånd och vissa färger.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentationen"></digi-code></p>
<ul>
<li>Använder nya designtokens och uppdatera komponenter.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[15.0.0] - 2022-04-12"    >
<h3 id="nytt-20">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li>Nya ikoner!</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Lagt till en sida för release notes.</li>
<li>Förbättrade kontaktuppgifter enligt tillgänglighetskrav.</li>
<li>Ny startsida och sida för introduktion av designsystemet.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-46">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-icon-arrow-<up|down|left|right>"></digi-code>; ikonerna för <digi-code af-code="arrow"></digi-code> har bytt namn till <digi-code af-code="digi-icon-chevron-<up|down|left|right>"></digi-code> för att bättre följa namnpraxis. Nya ikoner för arrow införda i dess ställe.
<strong>Glöm inte att byta namn på ikonerna om du lagt till dom manuellt i din applikation.</strong></li>
<li><digi-code af-code="digi-media-image"></digi-code>; Fixat fel som uppstod om man använder komponenten som <digi-code af-code="af-unlazy"></digi-code> utan att ange specifik höjd och bredd på bilden.</li>
<li><digi-code af-code="digi-icon-spinner"></digi-code>; problem med ikonen fixat.</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Förbättrat live-exempel för informationsmeddelanden.</li>
<li>Fixat problem med laddning av bilder på dokumentations-sajten.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[14.0.1] - 2022-04-11"    >
<h3 id="ändrat-47">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core-angular"></digi-code><ul>
<li><digi-code af-code="digi-progress-steps"></digi-code> och <digi-code af-code="digi-progress-step"></digi-code> saknades i core-angular. Problemet är patchat.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[14.0.0] - 2022-03-29"    >
<h3 id="nytt-21">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-progress-steps"></digi-code>; Ny komponent för att visualisera ett användarflöde.</li>
<li><digi-code af-code="digi-progress-step"></digi-code>; Ny komponent som används tillsammans med <digi-code af-code="digi-progress-steps"></digi-code>. Detta är varje steg i flödet.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-48">Ändrat</h3>
<ul>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Rättat länk till mediabanken.</li>
<li>Navigationen på dokumentationssidan använder sig av uppdateringarna i <digi-code af-code="digi-navigation-vertical-menu"></digi-code> och <digi-code af-code="digi-navigation-vertical-menu-item"></digi-code>.</li>
<li>Tagit bort dubbla landmärken runt navigationen.</li>
<li>Fixat valideringsfel på navigationen.</li>
<li>Uppdaterat tillgänglighetsredogörelsen.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-navigation-vertical-menu"></digi-code>; Layout är uppdaterad.</li>
<li><digi-code af-code="digi-navigation-vertical-menu-item"></digi-code>; Layout är uppdaterad.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.2.3] - 2022-03-25"    >
<ul>
<li><digi-code af-code="@digi/core-angular"></digi-code><ul>
<li><digi-code af-code="digi-icon-accessibility-universal"></digi-code>, <digi-code af-code="digi-icon-download"></digi-code>, <digi-code af-code="digi-icon-redo"></digi-code>, <digi-code af-code="digi-icon-trash"></digi-code> saknades i core-angular. Problemet är patchat.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.2.2] - 2022-03-24"    >
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="Code"></digi-code> typen för Code exporterades inte korrekt och skapade problem vid användning av Core i vissa sammanhang. Problemet är patchat.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.2.1] - 2022-03-23"    >
<ul>
<li><digi-code af-code="@digi/core-angular"></digi-code><ul>
<li>Tillåter alla peer dependency versioner av angular-paketen för version 12 och 13 och undviker därmed Conflicting Peer Dependency-felet när du gör npm install.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.2.0] - 2022-03-22"    >
<h3 id="ändrat-49">Ändrat</h3>
<ul>
<li><p><digi-code af-code="Dokumentationen"></digi-code></p>
<ul>
<li>Ändrat till Stencil Router V2 för att lösa buggar med bl.a. lazy-loadade bilder i dokumentationsapplikationen. (Problem med bildladdning kvarstår för vissa användare och kommer åtgärdas till nästa release.)</li>
<li>Komponentdokumentationen för <digi-code af-code="digi-link-button"></digi-code>, <digi-code af-code="digi-button"></digi-code> och <digi-code af-code="digi-info-card"></digi-code> är uppdaterad med den nya versionen av kodexemepelkomponenten</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-code-example"></digi-code>; förbättrad funktionalitet och möjlighet att kunna växla mellan olika varianter av exempelkomponenten i demoytan.</li>
<li><digi-code af-code="digi-code"></digi-code>; ändrat bakgrundsfärg från gråsvart till mörkblå, samt gjort den ljusa varianten till standard.</li>
<li><digi-code af-code="digi-code-block"></digi-code>; ändrat bakgrundsfärg från gråsvart till mörkblå.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.1.2] - 2022-03-18"    >
<h3 id="ändrat-50">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>La till komponenter under Utfällbart (expandable).</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.1.1] - 2022-03-16"    >
<h3 id="ändrat-51">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-navigation-pagination"></digi-code>; aktiv sida markerades inte korrekt om man har väldigt många sidor.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.1.0] - 2022-03-10"    >
<h3 id="nytt-22">Nytt</h3>
<ul>
<li><p><digi-code af-code="Changelog"></digi-code></p>
<ul>
<li>Ny gemensam changelog för hela monorepot. All information om driftsättningar kommer skrivas i samma dokument för att hålla ihop versionsnummer och gemensamma ändringar.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentationen"></digi-code></p>
<ul>
<li>Testmetoder; ny sida under Tillgänglighet och design.</li>
<li>Ny förbättrad caching av sidan. Laddar resurser i bakgrunden och tillåter bl.a. offline-läge och snabbare sidladdning.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-icon-trash"></digi-code>; ny ikon som illustrerar en soptunna.</li>
<li><digi-code af-code="digi-icon-accessibility-universal"></digi-code>; ny ikon som illustrerar tillgänglighet.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
<h3 id="ändrat-52">Ändrat</h3>
<ul>
<li><p><digi-code af-code="Dokumentationen"></digi-code></p>
<ul>
<li>Tillgänglighetslistan; ny förbättrad funktionalitet för att kunna följa upp status och kommentarer på olika krav samt bättre filtrering. Även ökad tillgänglighet.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.0.0] - 2022-03-08"    >
<h3 id="nytt-23">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>Nu helt releasad och har gått ur beta.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/fonts"></digi-code></p>
<ul>
<li>Nytt bibliotek för hantering av Arbetsförmedlingens typsnitt.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-53">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-button"></digi-code><ul>
<li>Lagt till saknade design tokens samt dokumenterat tokens.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.6.1] - 2022-02-18"    >
<ul>
<li><digi-code af-code="digi-form-select"></digi-code><ul>
<li><digi-code af-code="afStartSelected"></digi-code> är deprecated. Använd <digi-code af-code="afValue"></digi-code> istället.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.6.0] - 2022-02-22"    >
<h3 id="nytt-24">Nytt</h3>
<ul>
<li><p><digi-code af-code="digi-expandable-accordion"></digi-code></p>
<ul>
<li>Ny komponent</li>
</ul>
</li>
<li><p><digi-code af-code="digi-expandable-faq"></digi-code></p>
<ul>
<li>Ny komponent</li>
</ul>
</li>
<li><p><digi-code af-code="digi-expandable-faq-item"></digi-code></p>
<ul>
<li>Ny komponent</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.5.0] - 2022-02-22"    >
<h3 id="ändrat-54">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-link-(internal|external)"></digi-code><ul>
<li>Justerat så att digi-link-internal och digi-link-external använder sig av digi-link för att de ska ärva regler och fungera lika.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.4.0] - 2022-02-15"    >
<ul>
<li><digi-code af-code="digi-typography"></digi-code><ul>
<li>Lagt till maxbredd för rubriker och länkar.</li>
<li>Lagt en l-version för maxbredd.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.3.1] - 2022-02-09"    >
<ul>
<li><digi-code af-code="digi-button, digi-link-(internal|external)"></digi-code><ul>
<li>Justerat positionering av ikoner och dess storlek för knappar och länkar.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.3.0] - 2022-02-09"    >
<ul>
<li><digi-code af-code="digi-navigation-tabs"></digi-code><ul>
<li>Lagt till en publik metod för att ändra aktiv flik.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.1.1-beta.5] - 2022-01-20"    >
<ul>
<li><digi-code af-code="value"></digi-code><ul>
<li>Value och afValue sätts vid afOnInput så attributet korrekt speglas vid target.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[11.2.1-beta.0] - 2022-01-19"    >
<ul>
<li><digi-code af-code="digi-form-radiobutton"></digi-code><ul>
<li>Fixat bugg där radiogroup inte stöds. Fungerar nu på det sätt du önskar!</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.2.1] - 2022-01-19"    >
<h3 id="ändrat-55">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-media-image"></digi-code><ul>
<li>Fixat bugg med hur platshållaren fungerar</li>
<li>Ändrat bakgrunsfärg på platshållaren</li>
<li>Lagt till så attributet loading används på bilden. Sätts till <digi-code af-code="lazy"></digi-code> eller <digi-code af-code="eager"></digi-code> beroende på om man använder <digi-code af-code="afUnLazy"></digi-code> eller inte</li>
<li>Bilden går att ändra dynamiskt</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.0.0] - 2022-01-18"    >
<ul>
<li><digi-code af-code="version"></digi-code><ul>
<li>Ökade versionsnumret till 12 för att undvika problem med paket i npm.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.2.0] - 2022-01-18"    >
<ul>
<li><p><digi-code af-code="digi-layout-block"></digi-code></p>
<ul>
<li>Lade till attributet af-vertical-padding för att kunna addera padding inuti container-elementet.</li>
<li>Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på container-elementet.</li>
</ul>
</li>
<li><p><digi-code af-code="digi-layout-container"></digi-code></p>
<ul>
<li>Lade till attributet af-vertical-padding för att kunna addera padding inuti elementet.</li>
<li>Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på elementet.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[11.1.0] - 2022-01-10"    >
<ul>
<li><p><digi-code af-code="enums"></digi-code></p>
<ul>
<li>Enums finns numera under <digi-code af-code="@digi/core"></digi-code> och fungerar med bl.a. Angular.</li>
</ul>
</li>
<li><p><digi-code af-code="types"></digi-code></p>
<ul>
<li>Ändrat types från <digi-code af-code="components.d.ts"></digi-code> till <digi-code af-code="index.d.ts"></digi-code> som både exporterar components samt enums. På components finns namespace Components som kan användas t.ex. som <digi-code af-code="Components.DigiButton"></digi-code>. Förberett för interfaces om/när det ska användas och exponeras.</li>
</ul>
</li>
<li><p><digi-code af-code="output-target"></digi-code></p>
<ul>
<li>Använder nu <digi-code af-code="dist-custom-elements"></digi-code> istället för <digi-code af-code="dist-custom-elements-bundle"></digi-code> som blivit deprecated.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.1.3] - 2022-01-17"    >
<h3 id="ändrat-56">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-input"></digi-code>, <digi-code af-code="digi-form-textarea"></digi-code><ul>
<li>Tagit bort kontroll av &#39;dirty&#39; och &#39;touched&#39; av formulärelement vilket gör det mer flexibelt att välja när felmeddelanden ska visas.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-select"></digi-code><ul>
<li>Använder sig av <digi-code af-code="digi-util-mutation-observer"></digi-code> för att se om options-lista ändras programmatiskt.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-fieldset"></digi-code><ul>
<li>Lagt in möjlighet att sätta id på komponenten. Väljer man inget sätts en slumpmässig id.</li>
</ul>
</li>
<li><digi-code af-code="digi-media-image"></digi-code><ul>
<li>Löst bugg som gjorde att komponenten inte använde den bredd och höjd man angett.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[11.0.0] - 2021-12-23"    >
<ul>
<li><p><digi-code af-code="digi-form-*"></digi-code></p>
<ul>
<li>Justerat formulärkomponenterna så att de kan använda <digi-code af-code="value"></digi-code> istället för <digi-code af-code="afValue"></digi-code> (som nu är markerat deprecated). Även <digi-code af-code="checked"></digi-code> ersätter <digi-code af-code="afChecked"></digi-code>. Detta är gjort för att bättre fungera med Angular samt det nya biblioteket Digi Core Angular som är påväg ut.</li>
</ul>
</li>
<li><p><digi-code af-code="digi-form-select"></digi-code></p>
<ul>
<li>Ny logik för att hitta valt värde och skicka rätt event enligt samma practice som de andra formulärelementen.</li>
</ul>
</li>
<li><p><digi-code af-code="digi-form-radiobutton"></digi-code></p>
<ul>
<li>Tog bort <digi-code af-code="afChecked"></digi-code> och använder nu <digi-code af-code="afValue"></digi-code>för att initialt sätta ett värde. När <digi-code af-code="value"></digi-code> är samma som <digi-code af-code="afValue"></digi-code> så är radioknappen icheckad (följer bättre radiogroup).</li>
</ul>
</li>
<li><p><digi-code af-code="övrigt"></digi-code></p>
<ul>
<li>Digi Core är nu förberett för att kunna exportera filer via output-target till Angular. Ett nytt bibliotek <digi-code af-code="@digi/core-angular"></digi-code> kommer snart som är till för angular-appar. Det här biblioteket wrappar Digi Core och gör det enklare att använda t.ex. Reactive Forms.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.1.2] - 2021-12-15"    >
<h3 id="ändrat-57">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-typography"></digi-code><ul>
<li>Lagt till så ul- och dl-listor får samma textstorlek som stycken och även samma hantering för att inte få för långa textrader</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Justerat så att primär och sekundär variant på knapparna används korrekt. För att ändra utseende på knapparna så behöver knapparnas original-variabler ändras direkt, t.ex. <digi-code af-code="--digi-button--background"></digi-code>, dock finns variabler för t.ex. width, padding, m.m.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.1.1] - 2021-12-10"    >
<h3 id="ändrat-58">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-filter"></digi-code><ul>
<li>Löst en bugg där komponenten tidigare fungerade felaktigt om någon kryssruta var aktiv vid sidladdning.</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-tabs"></digi-code><ul>
<li>Lagt in stöd så komponenten känner av om man lägger till eller tar bort <digi-code af-code="digi-navigation-tab"></digi-code>-komponenter för att kunna dynamiskt ändra antal flikar.</li>
</ul>
</li>
<li><digi-code af-code="digi-info-card"></digi-code><ul>
<li>Tagit bort felaktig marginal på rubriken.</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-tab"></digi-code><ul>
<li>Ändrat så fokusram på en panel endast markeras när man navigerar dit med skärmläsare.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-filter"></digi-code><ul>
<li>Fixat så att komponenten fungerar korrekt även om man använder den inuti <digi-code af-code="digi-navigation-tab"></digi-code>. Tidigare stängdes listan när man försökte klicka på en kryssruta.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.1.0] - 2021-11-23"    >
<h3 id="nytt-25">Nytt</h3>
<ul>
<li><digi-code af-code="digi-icon-exclamation-triangle-warning"></digi-code><ul>
<li>Lagt till en ny ikon. Denna ikon används bl.a. för varningsmeddelanden i formulär. Tidigare behövdes den ikonen läsas in som en asset i projektet, nu kommer inte detta behövas.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-59">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-link-button"></digi-code><ul>
<li>Uppdaterat komponenten så man kan välja mellan olika storlekar.</li>
<li>Uppdaterat så alla layouter som finns i UI-kit också går att använda i kod.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-input"></digi-code>, <digi-code af-code="digi-form-select"></digi-code>, <digi-code af-code="digi-form-textarea"></digi-code><ul>
<li>Korrigerat så layout av formulärelement när de indikerar &quot;fel&quot;, &quot;varning&quot; och &quot;korrekt&quot; följer UI-kit.</li>
</ul>
</li>
<li><digi-code af-code="digi-icon-arrow-down"></digi-code><ul>
<li>Korrigerat layout så den följer UI-kit</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-breadcrumbs"></digi-code><ul>
<li>Ändrat uppdelaren mellan länkar till &#39;/&#39;, istället för &#39;&gt;&#39;.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.0.0] - 2021-10-19"    >
<h3 id="ändrat-60">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-navigation-menu"></digi-code><ul>
<li>Allt innehåll i Navigation Menu ska ligga i en <digi-code af-code="<ul>"></digi-code>-tagg och listas med respektive <digi-code af-code="<li>"></digi-code>-taggar.</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-menu-item"></digi-code><ul>
<li>En Navigation Menu Item som ska agera som expanderbart menyalternativ ska följas av en <digi-code af-code="<ul>"></digi-code>-tagg, t.ex.</li>
</ul>
<digi-code-block af-code="<digi-navigation-menu-item></digi-navigation-menu-item>
  <ul>...</ul>"></digi-code-block>
<p>För att göra raden expanderbar måste attributet <digi-code af-code="af-active-subnav"></digi-code> användas med antingen <digi-code af-code="true"></digi-code> för för-expanderad eller <digi-code af-code="false"></digi-code> för stängd.</p>
</li>
<li><digi-code af-code="digi-layout-media"></digi-code><ul>
<li>Lagt till token <digi-code af-code="--digi-layout-media-object--flex-wrap"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-progressbar"></digi-code><ul>
<li>Lagt till möjlighet att tända valfria steg i komponenten, som följer ordningen på vilka formulär-element som är gjorda.</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Lagt till en publik metod för att ändra aktivt steg i komponenten.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Lade till default värde på linkItems för att undvika JS fel vid tom lista</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.6] - 2021-09-30"    >
<h3 id="ändrat-61">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-code-example"></digi-code><ul>
<li>Ändrat bakgrundsfärg på vertygslistan</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.5] - 2021-09-28"    >
<h3 id="ändrat-62">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-input-search"></digi-code><ul>
<li>Lagt till möjlighet att dölja knappen</li>
<li>Buggfix, knappens text gick inte att ändra</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.4] - 2021-09-24"    >
<h3 id="ändrat-63">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Ändrat färgen på notifikationen från &quot;info&quot; till &quot;danger&quot;</li>
</ul>
</li>
<li><digi-code af-code="digi-notification-alert"></digi-code><ul>
<li>Justerat padding för medium storlek</li>
</ul>
</li>
<li><digi-code af-code="digi-form-validation-message"></digi-code><ul>
<li>Ändrat default varningsmeddelande till att vara tomt</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.3] - 2021-09-16"    >
<h3 id="ändrat-64">Ändrat</h3>
<ul>
<li><digi-code af-code="design-tokens"></digi-code><ul>
<li>Ändrat färgen <digi-code af-code="$digi--ui--color--green"></digi-code></li>
<li>Ändrat färgen <digi-code af-code="$digi--ui--color--pink"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.2] - 2021-08-27"    >
<h3 id="ändrat-65">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-filter"></digi-code><ul>
<li>Lagt till afOnChange EventEmitter</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.1] - 2021-08-18"    >
<h3 id="ändrat-66">Ändrat</h3>
<ul>
<li>Lagt till en ny outputTarget <digi-code af-code="dist-custom-elements-bundle"></digi-code></li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.0] - 2021-07-02"    >
<h3 id="nytt-26">Nytt</h3>
<ul>
<li><digi-code af-code="digi-progressbar"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
</ul>
<h3 id="ändrat-67">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-radiobutton"></digi-code><ul>
<li>Korrigeringar av layout</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.3.0] - 2021-06-18"    >
<h3 id="ändrat-68">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Skriv inte ut pagineringen om det bara är en sida. Däremot visas fortfarande texten &quot;Visar 1-15 av XXX&quot;.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.2.1] - 2021-06-15"    >
<h3 id="ändrat-69">Ändrat</h3>
<ul>
<li>Lagt till alla enums under <digi-code af-code="@digi/core/dist/enum/"></digi-code></li>
<li>Korrigerat felaktigheter i alla readmefiler. Främst gällande felaktiga paths till enum-importer.</li>
<li>Lagt till enum-mappen till disten.</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.2.0] - 2021-06-15"    >
<h3 id="nytt-27">Nytt</h3>
<ul>
<li><digi-code af-code="digi-calendar"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-typography-meta"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-logo"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-util-mutation-observer"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-form-radiobutton"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
</ul>
<h3 id="ändrat-70">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-fieldset"></digi-code><ul>
<li>Ändrade padding i fieldset till 0 för att linjera med komponenter utanför fieldset</li>
</ul>
</li>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Lagt till mutation observer för att se ändringar i komponenten</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.1.0] - 2021-05-25"    >
<h3 id="nytt-28">Nytt</h3>
<ul>
<li><digi-code af-code="digi-util-breakpoint-observer"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-form-select"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-form-fieldset"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-tag"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
</ul>
<h3 id="ändrat-71">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Felplacering av ikon i knapparna föregående/nästa vid navigering av siffer-knapparna.</li>
<li>Centrering av text i siffer-knapparna efter css-reset tagits bort</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.0.0] - 2021-05-04"    >
<h3 id="ändrat-72">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-core"></digi-code><ul>
<li>Lagt till ny docs tag <digi-code af-code="@enums"></digi-code> i alla komponenter</li>
</ul>
</li>
<li><digi-code af-code="digi-media-figure"></digi-code><ul>
<li>Ändrat enum <digi-code af-code="MediaFigureAlign"></digi-code> till <digi-code af-code="MediaFigureAlignment"></digi-code></li>
<li>Ändrat prop <digi-code af-code="afAlign"></digi-code> till <digi-code af-code="afAlignment"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Ändrat enum <digi-code af-code="ErrorListHeadingLevel"></digi-code> till <digi-code af-code="FormErrorListHeadingLevel"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[8.1.0] - 2021-05-03"    >
<h3 id="nytt-29">Nytt</h3>
<ul>
<li><digi-code af-code="digi-core"></digi-code><ul>
<li>Lagt till alla <digi-code af-code="enums"></digi-code> till paketet. Dessa hittas under <digi-code af-code="@digi/core/enum/mitt-enum-namn.emum.ts"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[8.0.0] - 2021-04-30"    >
<h3 id="changed">Changed</h3>
<ul>
<li><digi-code af-code="digi-core"></digi-code><ul>
<li>Ändrat all dokumentation inne i komponenterna till svenska</li>
</ul>
</li>
<li><digi-code af-code="digi-form-filter"></digi-code><ul>
<li>Ändrat event <digi-code af-code="afOnSubmittedFilter"></digi-code> till <digi-code af-code="afOnSubmitFilter"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-form-input-search"></digi-code><ul>
<li>Ändrat prop <digi-code af-code="afButtonLabel"></digi-code> till <digi-code af-code="afButtonText"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-tabs"></digi-code><ul>
<li>Ändrat prop <digi-code af-code="afTablistAriaLabel"></digi-code> till <digi-code af-code="afAriaLabel"></digi-code></li>
<li>Ändrat prop <digi-code af-code="afInitActiveTabIndex"></digi-code> till <digi-code af-code="afInitActiveTab"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Ändrat prop <digi-code af-code="afStartPage"></digi-code> till <digi-code af-code="afInitActivePage"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="dgi-notification-alert"></digi-code><ul>
<li>Ändrat event <digi-code af-code="afOnCloseAlert"></digi-code> till <digi-code af-code="afOnClose"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[7.2.0] - 2021-04-23"    >
<h3 id="added">Added</h3>
<ul>
<li><digi-code af-code="digi-notification-alert"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
<li><digi-code af-code="digi-link"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
</ul>
<h3 id="changed-1">Changed</h3>
<ul>
<li><digi-code af-code="digi-layout-media-object"></digi-code><ul>
<li>Included Stretch alignment</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[7.1.0] - 2021-04-19"    >
<h3 id="added-1">Added</h3>
<ul>
<li><digi-code af-code="digi-typography-time"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
</ul>
<h3 id="changed-2">Changed</h3>
<ul>
<li><digi-code af-code="Navigation-pagination"></digi-code> <digi-code af-code="Navigation-tabs"></digi-code> <digi-code af-code="Form-filter"></digi-code> <digi-code af-code="Form-textarea"></digi-code> <digi-code af-code="Form-input"></digi-code> <digi-code af-code="Form-input-search"></digi-code><ul>
<li>Solved style issues after removal of base-reset</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[7.0.0] - 2021-04-19"    >
<h3 id="added-2">Added</h3>
<ul>
<li><digi-code af-code="digi-navigation-context-menu"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-breadcrumbs"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
</ul>
<h3 id="breaking-changes">Breaking Changes</h3>
<ul>
<li><strong>Removed reset css from all components</strong><ul>
<li>Previously, all components included a reset css to prevent global styles from leaking in. This created a massive duplication of css and is now removed. Every consuming app is now responsible for handling css leakage. We are continuously correcting css problems that this may have caused inside the components themselves. If you encounter any problems, please report them to us.</li>
</ul>
</li>
<li><strong>Removed <digi-code af-code="global"></digi-code> folder</strong><ul>
<li>All uncompiled scss is being moved to a new library called <digi-code af-code="@digi/styles"></digi-code>, which will very soon be available from the package manager. In the meantime, you can use the old <digi-code af-code="@af/digi-core"></digi-code>-package.</li>
</ul>
</li>
</ul>
<h3 id="changed-3">Changed</h3>
<ul>
<li><digi-code af-code="digi-button"></digi-code><ul>
<li>Included hasIconSecondary within handleErrors function</li>
<li>Added outline and text-align variables</li>
</ul>
</li>
</ul>

</digi-expandable-accordion af-heading>
`