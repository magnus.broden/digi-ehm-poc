import { newSpecPage } from '@stencil/core/testing';
import { DigiToolsLanguagepicker } from './digi-tools-languagepicker-details';

describe('digi-tools-languagepicker-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiToolsLanguagepicker],
			html:
				'<digi-tools-languagepicker-details></digi-tools-languagepicker-details>'
		});
		expect(root).toEqualHtml(`
      <digi-tools-languagepicker-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-tools-languagepicker-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiToolsLanguagepicker],
			html: `<digi-tools-languagepicker-details first="Stencil" last="'Don't call me a framework' JS"></digi-tools-languagepicker-details>`
		});
		expect(root).toEqualHtml(`
      <digi-tools-languagepicker-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-tools-languagepicker-details>
    `);
	});
});
