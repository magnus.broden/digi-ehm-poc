import { Component, Prop, h, State } from '@stencil/core';
import {
	CodeExampleVariation,
	LinkButtonSize,
	LinkButtonVariation,
	CodeBlockVariation,
	CodeExampleLanguage
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-code-example-details',
	styleUrl: 'digi-code-example-details.scss'
})
export class DigiCodeExampleDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() linkButtonVariation: LinkButtonVariation = LinkButtonVariation.PRIMARY;
	@State() linkButtonSize: LinkButtonSize = LinkButtonSize.MEDIUM;
	@State() codeBlockVariation: CodeBlockVariation = CodeBlockVariation.DARK;
	@State() exampleVariation: CodeExampleVariation = CodeExampleVariation.LIGHT;

	get linkButtonCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-link-button
	af-href="#"
	af-size="${this.linkButtonSize}"
	af-variation="${this.linkButtonVariation}"
>
	<digi-icon-arrow-right slot="icon" />
	Jag är en länk
</digi-link-button>`,
		[CodeExampleLanguage.ANGULAR]: `\
<digi-link-button
	[attr.af-href]="'#'"
	[attr.af-size]="LinkButtonSize.${Object.keys(LinkButtonSize).find(key => LinkButtonSize[key] === this.linkButtonSize)}"
	[attr.af-variation]="LinkButtonVariation.${Object.keys(LinkButtonVariation).find(key => LinkButtonVariation[key] === this.linkButtonVariation)}"
>
	<digi-icon-arrow-right slot="icon" />
	Jag är en länk
</digi-link-button>`,
		[CodeExampleLanguage.REACT]: `\
<DigiLinkButton
	afHref="#"
	afSize={LinkButtonSize.${Object.keys(LinkButtonSize).find(key => LinkButtonSize[key] === this.linkButtonSize)}}
	afVariation={LinkButtonVariation.${Object.keys(LinkButtonVariation).find(key => LinkButtonVariation[key] === this.linkButtonVariation)}}
>
	<DigiIconArrowRight slot="icon" />
	Jag är en länk
</DigiLinkButton>`
		}
	}
	render() {
		return (
			<div class="digi-code-example-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Med hjälp av kodexempelkomponenten går det att visa live-exempel
              av kodsnuttar. I högerspalten kan formlärkomponenter läggas in, t.ex. radioknappar, för att kunna växla
              mellan olika varianter och kodspråk. Det finns också möjlighet
              att kopiera koden som genererats.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example
								af-code={JSON.stringify(this.linkButtonCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
							>
								<div class="slot__controls" slot="controls">
									<digi-form-fieldset afName="Variation" afLegend="Variation" onChange={e => this.linkButtonVariation = (e.target as any).value}>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Primär"
											afValue={LinkButtonVariation.PRIMARY}
											afChecked={this.linkButtonVariation === LinkButtonVariation.PRIMARY}
										/>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Primär alt"
											afValue={LinkButtonVariation.PRIMARY_ALT}
											afChecked={this.linkButtonVariation === LinkButtonVariation.PRIMARY_ALT}
										/>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Sekundär"
											afValue={LinkButtonVariation.SECONDARY}
											afChecked={this.linkButtonVariation === LinkButtonVariation.SECONDARY}
										/>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Sekundär alt"
											afValue={LinkButtonVariation.SECONDARY_ALT}
											afChecked={this.linkButtonVariation === LinkButtonVariation.SECONDARY_ALT}
										/>
									</digi-form-fieldset>
									<digi-form-fieldset afName="Storlek" afLegend="Storlek" onChange={e => this.linkButtonSize = (e.target as any).value}>
										<digi-form-radiobutton
											afName="Storlek"
											afLabel="Medium"
											afValue={LinkButtonSize.MEDIUM}
											afChecked={this.linkButtonSize === LinkButtonSize.MEDIUM}
										/>
										<digi-form-radiobutton
											afName="Storlek"
											afLabel="Stor"
											afValue={LinkButtonSize.LARGE}
											afChecked={this.linkButtonSize === LinkButtonSize.LARGE}
										/>

									</digi-form-fieldset>
								</div>
								<digi-link-button
									afHref="#"
									af-target="_blank"
									afVariation={this.linkButtonVariation}
									afSize={this.linkButtonSize}
								>
									Jag är en knapplänk
								</digi-link-button>
							</digi-code-example>
						</article>
						<article>
							<h3>Kod</h3>
							<h4>Fler än ett språk</h4>
							<digi-code-block
								af-language='javascript'
								af-code='get linkButtonCode() {
    return {
      HTML:`\
      <digi-link-button
        af-href="#"
        af-size="${this.linkButtonSize}"
        af-variation="${this.linkButtonVariation}"
      >
        <digi-icon-arrow-right slot="icon" />
          Jag är en länk
        </digi-link-button>`,
      Angular:`\
      <digi-link-button
        [attr.af-href]="#"
        [attr.af-size]="LinkButtonSize.${Object.keys(LinkButtonSize).find(key => 
          LinkButtonSize[key] === this.linkButtonSize)}"
        [attr.af-variation]="LinkButtonVariation.${Object.keys(LinkButtonVariation).find(key => 
          LinkButtonVariation[key] === this.linkButtonVariation)}"
      >
        <digi-icon-arrow-right slot="icon" />
        Jag är en länk
      </digi-link-button>`,
      React:`\
      <DigiLinkButton
        afHref="#"
        afSize="LinkButtonSize.${Object.keys(LinkButtonSize).find(key => 
          LinkButtonSize[key] === this.linkButtonSize)}"
        afVariation="LinkButtonVariation.${Object.keys(LinkButtonVariation).find(key => 
          LinkButtonVariation[key] === this.linkButtonVariation)}"
      >
        <DigiIconArrowRight slot="icon" />
        Jag är en länk
      </DigiLinkButton>`
          }
        }'
							></digi-code-block>
						</article>
						<digi-layout-container af-margin-bottom af-margin-top>
							<article>
								<p>
									Lägg in den kod som ska visas i{' '}
									<digi-code af-code="af-code" />. Används flera språk så
									kan en geter skapas som i exemplet ovan och då genereras
									radioknappar för det längst ner i högerspalten. Är det
									bara ett språk kan det läggas in direkt.
								</p>
								<p>
									Lägg sedan in en div i sloten 'controls' och ge den klassen "slot__controls". Efter det kan en eller flera{' '}
									<digi-code af-code="digi-form-fieldset" /> med
									de formulärselement, t.ex.{' '}
									<digi-code af-code="digi-form-radiobutton" />, som du
									behöver läggas in.
								</p>
								<p>
									Efter div:en för kontroller läggs sedan koden för exemplet
									som ska visas in.
								</p>
							</article>
						</digi-layout-container>
						<digi-code-block
							af-code='
             <digi-code-example 
             af-code={JSON.stringify(this.linkButtonCode)}
           >
             <div class="slot__controls" slot="controls">
                <digi-form-fieldset afName="Variation" afLegend="Variation" 
                onChange={e => this.linkButtonVariation = (e.target as any).value}>
                  <digi-form-radiobutton
                  afName="Variation"
                  afLabel="Primär"
                  afValue={LinkButtonVariation.PRIMARY}
                  afChecked={this.linkButtonVariation === LinkButtonVariation.PRIMARY}
                  />
                  (...)
                  />
                </digi-form-fieldset>
                <digi-form-fieldset afName="Storlek" afLegend="Storlek" 
                onChange={e => this.linkButtonSize = (e.target as any).value}>
                  <digi-form-radiobutton
                  afName="Storlek"
                  afLabel="Medium"
                  afValue={LinkButtonSize.MEDIUM}
                  afChecked={this.linkButtonSize === LinkButtonSize.MEDIUM}
                  />
                  (...)
                </digi-form-fieldset>
             </div>
             <digi-link-button afHref="#" af-target="_blank" 
             afVariation={this.linkButtonVariation} 
             afSize={this.linkButtonSize}>
                Jag är en knapplänk
              </digi-link-button>
            </digi-code-example>'
						></digi-code-block>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-margin-bottom>
							<h2>Varianter</h2>
							<p>
								Det går att ändra exemplets bakgrund med hjälp av <digi-code af-code="af-example-variation" />. Alternativen är ljus (light) och mörkt (dark), varav ljus är standard.
								Det går även att ändra bakgrunden på kodblocket med hjälp av <digi-code af-code="af-code-block-variation" />. Det går att välja mellan ett mörkt färgschema (dark) och ett ljust (light), varav ljus är standard.

							</p>
							<digi-code-example
								af-code='<digi-link-button>
  Jag är en knapplänk
</digi-link-button>'
								afCodeBlockVariation={this.codeBlockVariation}
								afExampleVariation={this.exampleVariation}
							>
								<div class="slot__controls" slot="controls">
									<digi-form-fieldset
										afName="bgVariation"
										afLegend="Bakgrund på exemplet"
										onChange={(e) =>
											(this.exampleVariation = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="bgVariation"
											afLabel="Ljus"
											afValue={CodeExampleVariation.LIGHT}
											afChecked={
												this.exampleVariation === CodeExampleVariation.LIGHT
											}
										/>
										<digi-form-radiobutton
											afName="bgVariation"
											afLabel="Mörk"
											afValue={CodeExampleVariation.DARK}
											afChecked={
												this.exampleVariation === CodeExampleVariation.DARK
											}
										/>
									</digi-form-fieldset>
									<digi-form-fieldset
										afName="colorScheme"
										afLegend="Färgschema på kodblocket"
										onChange={(e) =>
											(this.codeBlockVariation = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="colorScheme"
											afLabel="Mörk"
											afValue={CodeBlockVariation.DARK}
											afChecked={
												this.codeBlockVariation === CodeBlockVariation.DARK
											}
										/>
										<digi-form-radiobutton
											afName="colorScheme"
											afLabel="Ljus"
											afValue={CodeBlockVariation.LIGHT}
											afChecked={
												this.codeBlockVariation === CodeBlockVariation.LIGHT
											}
										/>
									</digi-form-fieldset>
								</div>
								<digi-link-button
									afHref="#"
									af-target="_blank"
								>
									Jag är en knapplänk
								</digi-link-button>
							</digi-code-example>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
