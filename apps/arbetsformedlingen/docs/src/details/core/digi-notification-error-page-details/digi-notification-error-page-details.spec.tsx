import { newSpecPage } from '@stencil/core/testing';
import { DigiNotificationErrorPage } from './digi-notification-error-page-details';

describe('digi-notification-error-page-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiNotificationErrorPage],
			html:
				'<digi-notification-error-page-details></digi-notification-error-page-details>'
		});
		expect(root).toEqualHtml(`
      <digi-notification-error-page-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-notification-error-page-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiNotificationErrorPage],
			html: `<digi-notification-error-page-details first="Stencil" last="'Don't call me a framework' JS"></digi-notification-error-page-details>`
		});
		expect(root).toEqualHtml(`
      <digi-notification-error-page-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-notification-error-page-details>
    `);
	});
});
