import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  LayoutBlockVariation,
  FormCheckboxVariation,
  FormCheckboxValidation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-form-checkbox-details',
	styleUrl: 'digi-form-checkbox-details.scss'
})
export class DigiFormCheckboxDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() formCheckboxVariation: FormCheckboxVariation = FormCheckboxVariation.PRIMARY;
  @State() formCheckboxValidation: FormCheckboxValidation;
  @State() validation: boolean = false;

  get formCheckboxCode() {
    return {
      [CodeExampleLanguage.HTML]: `<digi-form-checkbox
  af-label="Kryssruta"
  af-variation="${this.formCheckboxVariation}"
${this.formCheckboxValidation !== undefined ? `  af-validation="${this.formCheckboxValidation}"\n` : ''}/>`,
      [CodeExampleLanguage.ANGULAR]: `<digi-form-checkbox
  [attr.af-label]="'Kryssruta'"
  [attr.af-variation]="FormCheckboxVariation.${Object.keys(FormCheckboxVariation).find((key) => FormCheckboxVariation[key] === this.formCheckboxVariation)}"
${this.formCheckboxValidation !== undefined ? `  [attr.af-validation]="FormCheckboxValidation.${Object.keys(FormCheckboxValidation).find((key) => FormCheckboxValidation[key] === this.formCheckboxValidation)}"\n` : ''}/>`,
      [CodeExampleLanguage.REACT]: `<DigiFormCheckbox
  afLabel="Kryssruta"
  afVariation={FormCheckboxVariation.${Object.keys(FormCheckboxVariation).find((key) => FormCheckboxVariation[key] === this.formCheckboxVariation)}}
${this.formCheckboxValidation !== undefined ? `  afValidation={FormCheckboxValidation.${Object.keys(FormCheckboxValidation).find((key) => FormCheckboxValidation[key] === this.formCheckboxValidation)}}\n` : ''}/>`
    };
  }

  render() {
    return (
      <div class="digi-form-checkbox-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <div class="block-spacer">
              <digi-typography-preamble>
                Kryssruta används för att ge möjlighet till valfritt antal val mellan olika alternativ. Kryssruta kan också användas som ett ensamt element för att ge valmöjlighet till av/på-läge.
              </digi-typography-preamble>
            </div>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && (<h2>Exempel</h2>)}
            <digi-code-example
              af-code={JSON.stringify(this.formCheckboxCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}
            >
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variation"
                  onChange={e => this.formCheckboxVariation = (e.target as any).value}
                >
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={FormCheckboxVariation.PRIMARY}
                    afChecked={this.formCheckboxVariation === FormCheckboxVariation.PRIMARY}
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={FormCheckboxVariation.SECONDARY}
                    afChecked={this.formCheckboxVariation === FormCheckboxVariation.SECONDARY}
                  />
                </digi-form-fieldset>
                <digi-form-fieldset
                  afName="Validering"
                  afLegend="Validering"
                  onChange={e => this.formCheckboxValidation = (e.target as any).value === "none" ? undefined : (e.target as any).value}
                >
                  <digi-form-radiobutton
                    afName="Validering"
                    afLabel="Ingen"
                    afValue="none"
                    afChecked={this.formCheckboxValidation !== FormCheckboxValidation.ERROR && this.formCheckboxValidation !== FormCheckboxValidation.WARNING}
                  />
                  <digi-form-radiobutton
                    afName="Validering"
                    afLabel="Fel"
                    afValue={FormCheckboxValidation.ERROR}
                    afChecked={this.formCheckboxValidation === FormCheckboxValidation.ERROR}
                  />
                  <digi-form-radiobutton
                    afName="Validering"
                    afLabel="Varning"
                    afValue={FormCheckboxValidation.WARNING}
                    afChecked={this.formCheckboxValidation === FormCheckboxValidation.WARNING}
                  />
                </digi-form-fieldset>
              </div>
              <digi-form-checkbox
                afLabel="Kryssruta"
                afVariation={this.formCheckboxVariation}
                af-validation={this.formCheckboxValidation}
              />
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom afMarginTop>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>Kryssrutan finns i två varianter, en primär (förvald variant) och en sekundär. Vi använder i första hand den primära, men när det inte fungerar så går det bra att använda den sekundära.</p>
              <h3>Validering</h3>
              <p>Inmatningsfältet kan ha tre olika valideringsstatus: Felaktig, varning, eller ingen alls.</p>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-block
              af-variation={LayoutBlockVariation.SYMBOL}
              af-vertical-padding
              af-margin-bottom
            >
              <h2>Övriga riktlinjer</h2>
              <digi-list>
                <li>Kryssrutan kan vara förvald.</li>
                <li>När flera kryssrutor presenteras tillsammans så behöver de ha en överskrift (legend) så att det blir tydligt vad användaren tar ställning till. Kryssrutorna bör då presenteras vertikalt.</li>
                <li>Att göra ett val i en kryssruta påverkar inte någon av de övriga kryssrutorna.</li>
                <li>Avståndet mellan kryssrutor bör vara tillräckligt stort för att vara en bra klickyta oavsett enhet.</li>
                <li>Etiketten ska vara tydlig och beskriva alternativet.</li>
                <li>Om många kryssrutor presenteras i en lång lista, fundera på om de kan grupperas i sub-grupper för ökad tydlighet.</li>
                <li>Använd inte kryssruta som en action/exekvera-funktion.</li>
              </digi-list>
            </digi-layout-block>
          )}
        </digi-typography>
      </div>
    );
  }
}
