import { Component, h, Host, Prop } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-bar-chart-details',
	styleUrl: 'digi-bar-chart-details.scss'
})
export class DigiBarChart {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	barChartdata: any = {
		data: {
			xValues: [
				1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21
			],
			series: [
				{
					yValues: [
						708333, 6011283, 469751, 5996719, 17672408, 16894458, 1455858, 12711914,
						722069, 22621904, 2897470, 2203106, 0, 27045369, 4316673, 0, 18571956,
						5700205, 1672010, 0
					],
					title: 'Megawattimmar'
				}
			],
			xValueNames: [
				'Blekinge',
				'Dalarna',
				'Gotland',
				'Gävleborg',
				'Halland',
				'Jämtland',
				'Jönköping',
				'Kalmar',
				'Kronoberg',
				'Norrbotten',
				'Skåne',
				'Stockholm',
				'Södermanland',
				'Uppsala',
				'Värmland',
				'Västerbotten',
				'Västernorrland',
				'Västra Götaland',
				'Örebro',
				'Östergötland'
			]
		},
		x: 'Län',
		y: 'MWh',
		title: 'Elproduktion i sverige år 2021 (MWh)',
		subTitle: 'Uppdelat per län, hämtat från Statens energimyndighet',
		infoText: 'Denna statistik avser hela landet och kan inte filtreras bla bla bla bla',
		meta:{
			valueLabels: true
		}
	}

	get chartData() {
		return JSON.stringify(this.barChartdata);
	}
	get barChartCode() {
		return {
			[CodeExampleLanguage.HTML]: `<digi-bar-chart    
  af-chart-data={this.chartData}
  af-heading-level="h2"
	af-variation="horizontal"
>
</digi-bar-chart>`,
			[CodeExampleLanguage.ANGULAR]: `<digi-bar-chart 
  [attr.af-chart-data]="chartData" 
  [attr.af-heading-level]="afHeadingLevel.H2"
	[attr.af-variation]="'horizontal'"
>
</digi-bar-chart>
`,
			[CodeExampleLanguage.REACT]: `<digi-bar-chart 
  afChartData={this.chartData}
  afHeadingLevel={afHeadingLevel.H2}
	afVariation={'horizontal'}
>
</digi-bar-chart>
`
		};
	}
	render() {
		return (
			<Host>
				<div class="digi-bar-chart-details">
					<digi-typography>
						{!this.afShowOnlyExample && (
							<digi-layout-container afNoGutter afMarginTop>
								<digi-typography-preamble>
									Stapeldiagram är ett typ av diagram som lämpar sig att använda om man
									vill jämföra storheter sinsemellan.
								</digi-typography-preamble>
							</digi-layout-container>
						)}
						<digi-layout-container af-no-gutter af-margin-bottom>
							{!this.afShowOnlyExample && <h2>Exempel</h2>}
							<digi-code-example
								af-code={JSON.stringify(this.barChartCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'}
								af-hide-code={this.afHideCode ? 'true' : 'false'}
							>
								<div class="slot__controls" slot="controls">
									<digi-form-fieldset></digi-form-fieldset>
								</div>
								<div style={{ width: '600px', height: '600px' }}>
									<digi-bar-chart
										af-chart-data={this.chartData}
										af-heading-level="h2"
										af-variation="horizontal"
									></digi-bar-chart>
								</div>
							</digi-code-example>
						</digi-layout-container>
						<digi-layout-container af-no-gutter af-margin-bottom>
							{!this.afShowOnlyExample && (
								<article>
									<h2>Beskrivning</h2>
									<h3>Variationer</h3>
									<p>
										Det finns två olika variationer, antingen ett stående (vertikalt), eller ett liggande stapeldiagram (horisontellt)
									</p>
									<h3>Storlek</h3>
									<p>
										Stapeldiagrammets storlek anpassar sig efter dess förälderelement.
										Förälderelementen måste ha en storlek, annars så får diagrammet
										storlek 0. Exempelvis
										<digi-code afCode="style={{ 'width': '400px', 'height': '200px' }} "></digi-code>
									</p>
									<h3>Att skicka in data</h3>
									<p>
										För att skicka in data till stapeldiagrammet så finns det en
										objektstruktur man måste följa, den ser ut som sådan:
										<br />
										<span class="italic">
											Diagrammet har samma datastruktur som linjediagram, därför kan
											namnsättningen vara missvisande
										</span>
										<br />
										<digi-code-block
											af-language="typescript"
											af-code="ChartLineData {
	data: { 
		xValues: number[]; 
		series: ChartLineSeries[]; 
		xValueNames?: string[]; 
	}; 
	x:string; 
	y:string; 
	title:string; 
	subTitle?:string; 
	infoText?:string;
	meta?: { 
		numberOfReferenceLines?: number; 
		percentage?:boolean; 
		hideXAxis?:boolean;
		valueLabels?: boolean;
		labelProperties?: {
			significantDigits?: number;
			shortHand?: boolean;
		}
	}; 
}"
											af-variation="dark"
										></digi-code-block>
										<br />
										<digi-code afCode="data"></digi-code> Innehåller{' '}
										<digi-code afCode="xValues"></digi-code> som är en array med numeriska
										värden, från 1 till N antal datapunkter som finns i datan. Om man har
										ett stapeldiagram med befolkningen i blekinges 5 kommuner, så skulle
										alltså <digi-code afCode="xValues"></digi-code> vara en lista med{' '}
										<br /> <digi-code afCode="[1,2,3,4,5]"></digi-code>, <br /> för att
										visa namnen på kommunerna, bör man även använda sig av nyckeln{' '}
										<digi-code afCode="xValueNames"></digi-code>, som är en lista på
										strängar av matchande längd med{' '}
										<digi-code afCode="xValues"></digi-code>. Som i detta fall skulle
										vara: <br />{' '}
										<digi-code afCode="['Karlshamn','Karlskrona','Olofström','Ronneby','Sölvesborg']"></digi-code>
										<br />
										<br />
										<digi-code afCode="series"></digi-code> Innehåller själva datapunkerna
										och har en liten egen struktur som ser ut såhär:
										<digi-code-block
											af-language="typescript"
											af-code="ChartLineSeries {
 yValues: number[];
 title: string;
 colorToken?;
}"
											af-variation="dark"
										></digi-code-block>
										Serien motsvarar ett mått, därför har varje serie en{' '}
										<digi-code afCode="title"></digi-code>-nyckel för att namnge vilket
										mått det är man tittar på, till skillnad från linjediagrammet, så kan
										stapeldiagrammet här bara innehålla 1 serie med datapunkter.
										<digi-code afCode="colorToken"></digi-code>-nyckeln används inte i
										detta diagram.
										<br />
										<br />
										<digi-code afCode="yValues"></digi-code> är en lista på alla numeriska
										värden per stapel,<digi-code afCode="yValues"></digi-code> måste inte
										vara samma längd som Y-values är ett lite missvisande namn eftersom i
										det stående horisontella diagrammet som detta så motsvarar detta
										egentligen värdena på X-axeln.
										<digi-code afCode="xValues"></digi-code>
										<br />
										<br />
										<digi-code afCode="x"></digi-code> är namnet på y-axelns värden,
										kommun i exemplet ovan
										<br />
										<br />
										<digi-code afCode="y"></digi-code> är namnet på x-axelns värden, det
										som ger uttryck för varje stapels storlek
										<br />
										<br />
										<digi-code afCode="title"></digi-code> är namnet diagrammet.
										<br />
										<br />
										<digi-code afCode="subTitle"></digi-code> är en eventuell underrubrik
										till diagrammet.
										<br />
										<br />
										<digi-code afCode="infoText"></digi-code> är en eventuell övrig
										infotext till diagrammet till diagrammet.
										<br />
										<br />
										<digi-code afCode="meta"></digi-code> har några properties som ska styra utseendet på stapeldiagrammet,
										<digi-code afCode="hideXAxis"></digi-code> om man inte vill visa någon x-axel eller referenslinjer.
										<digi-code afCode="valueLabels"></digi-code> styr om värdeettiketter ska visas vid staplarna eller ej.
										Under labelproperties, så finns <digi-code afCode="significantDigits"></digi-code>. significantDigits säger hur många värdesiffror diagrammet ska visa i labels.
										Skulle diagrammet ha ett värde på säg 3563, och vi sätter significant digits till 3, så kommer värdet ovan stapeln visa 3560, skulle vi sälja 2, så visas istället 3600.
										<digi-code afCode="shortHand"></digi-code> Shorthand är en boolean som förkortar långa siffror, och använder sig default av 2 i significant digits. 1 230 000 skulle med shorthand skrivas som '1,2 mn'.
										<br />
										<br />
										Utöver detta, ska komponent ha rubiknivå. Du kan välja rubriknivå för
										rubriken genom att använda{' '}
										<digi-code afCode="digi-heading-level"></digi-code>.<br />
									</p>

									<h3>Tillgänglighet</h3>
									<p>
										Diagrammet användar aria-hidden för att dölja själva diagram-ytan.
										<br /> Digrammet erbjuder även möjligheten till användaren att visa
										informationen i tabellform. Detta uppnås genom att man trycker på visa
										som tabell, knappen är går att navigera till med tangentbord.
										<br /> Interaktionen med diagrammet erbjuder en sticky tooltip som
										inte förrändras med hover, utan går bara att öppna och stänga med
										klick.
									</p>

									<h3>Att tänka på</h3>
									<p>
										Diagram kan för ovana användare vara lite avskräckande, de kräver
										uppmärksamhet och bör därför vara sparsamt använda på en sida. Undvik
										att ha flera diagram i närheten av varandra, en bra tumregel kan vara
										ett diagram per vy.
									</p>
									<digi-layout-block
										af-variation="symbol"
										af-vertical-padding
										af-margin-bottom
									>
										<h2>Riktlinjer</h2>
										<digi-list>
											<li>
												Stapeldiagram passar bra när man vill jämföra storheter, vill man
												kunna se hur saker förrändras över tid så passar i regel
												linjediagram bättre. Men det kan även finnas fall då en storhet över
												tid kan vara intresseant att titta på.
											</li>
											<li>
												Diagram tar väldigt mycket uppmärksamhet, och bör omfamnas av
												tillräckligt med White space. Ett stapeldiagram tar med fördel upp
												en större del av sidans bredd.
											</li>
											<li>
												Har man andra diagram eller statistiska element på sidan bör de
												ligga på lämpligt avstånd från diagrammet, såvida de inte är tätt
												kopplade med datan i diagrammet.
											</li>
										</digi-list>
									</digi-layout-block>
								</article>
							)}
						</digi-layout-container>
					</digi-typography>
				</div>
			</Host>
		);
	}
}
