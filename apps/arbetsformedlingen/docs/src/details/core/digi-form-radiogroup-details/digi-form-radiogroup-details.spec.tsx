import { newSpecPage } from '@stencil/core/testing';
import { DigiFormRadiogroup } from './digi-form-radiogroup-details';

describe('digi-form-radiogroup-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormRadiogroup],
			html: '<digi-form-radiogroup-details></digi-form-radiogroup-details>'
		});
		expect(root).toEqualHtml(`
      <digi-form-radiogroup-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-form-radiogroup-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormRadiogroup],
			html: `<digi-form-radiogroup-details first="Stencil" last="'Don't call me a framework' JS"></digi-form-radiogroup-details>`
		});
		expect(root).toEqualHtml(`
      <digi-form-radiogroup-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-form-radiogroup-details>
    `);
	});
});
