import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-navigation-sidebar-button-details',
	styleUrl: 'digi-navigation-sidebar-button-details.scss'
})
export class DigiNavigationSidebarButtonDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get navigationSidebarCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-sidebar-button 
	af-text="Meny">
</digi-navigation-sidebar-button>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-sidebar-button 
	[attr.af-text]="'Meny'">
</digi-navigation-sidebar-button>`,
			[CodeExampleLanguage.REACT]: `\
<DigiNavigationSidebarButton 
	afText="Meny">
</DigiNavigationSidebarButton>`
		}
	}
	render() {
		return (
			<div class="digi-navigation-sidebar-button-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Detta är en knapp som går att använda i till exempel sidhuvudet för att öppna och stänga sidofältet.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.navigationSidebarCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<digi-navigation-sidebar-button af-text="Meny"></digi-navigation-sidebar-button>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-margin-bottom>
							<h2>Beskrivning</h2>
							<p>
								Detta är en standardknapp som används för att öppna och stänga sidofältet.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		)
	}
}
