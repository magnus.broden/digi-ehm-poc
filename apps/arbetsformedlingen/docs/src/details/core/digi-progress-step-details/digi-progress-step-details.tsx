import { Component, Prop, h, State } from '@stencil/core';
// import { ProgressStepVariation } from 'dist/libs/core/dist/types';
import {
  CodeExampleLanguage,
  ProgressStepsStatus,
  ProgressStepStatus,
  ProgressStepVariation,
  ProgressStepHeadingLevel
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-progress-step-details',
  styleUrl: 'digi-progress-step-details.scss'
})
export class DigiProgressStepDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() progressStepVariation: ProgressStepVariation =
    ProgressStepVariation.PRIMARY;
  @State() progressStepStatus: ProgressStepStatus = ProgressStepStatus.DONE;
  @State() progressStepHeadingLevel: ProgressStepHeadingLevel =
    ProgressStepHeadingLevel.H2;

  get progressStepCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-progress-step
  af-heading="Rubrik"
  af-step-status="${this.progressStepStatus}"
  af-variation="${this.progressStepVariation}"
  af-heading-level="${this.progressStepHeadingLevel}"
>
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Etiam magna neque, interdum vel massa eget, condimentum
rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-step>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-progress-step
  [attr.af-heading]="'Rubrik'"
  [attr.af-step-status]="ProgressStepStatus.${Object.keys(ProgressStepStatus).find(
        (key) => ProgressStepsStatus[key] === this.progressStepStatus
      )}"
  [attr.af-variation]="ProgressStepVariation.${Object.keys(
        ProgressStepVariation
      ).find((key) => ProgressStepVariation[key] === this.progressStepVariation)}"
  [attr.af-heading-level]="ProgressStepHeadingLevel.${Object.keys(ProgressStepHeadingLevel).find(
        (key) => ProgressStepHeadingLevel[key] === this.progressStepHeadingLevel
      )}"
>
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Etiam magna neque, interdum vel massa eget, condimentum
rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-step>`,
      [CodeExampleLanguage.REACT]: `\
<DigiProgressStep
  afHeading="Rubrik"
  afStepStatus={ProgressStepStatus.${Object.keys(ProgressStepStatus).find(
        (key) => ProgressStepsStatus[key] === this.progressStepStatus
      )}}
  afVariation={ProgressStepVariation.${Object.keys(
        ProgressStepVariation
      ).find((key) => ProgressStepVariation[key] === this.progressStepVariation)}}
  afHeadingLevel={ProgressStepHeadingLevel.${Object.keys(ProgressStepHeadingLevel).find(
        (key) => ProgressStepHeadingLevel[key] === this.progressStepHeadingLevel
      )}}
>
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Etiam magna neque, interdum vel massa eget, condimentum
rutrum velit. Sed vitae ullamcorper sem.
</DigiProgressStep>`
    };
  }

  render() {
    return (
      <div class="digi-progress-step-details">
        <digi-typography>

          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten kan användas enskilt eller tillsammans med{' '}
              <digi-code af-code="<digi-progress-steps>" /> för att
              visualisera ett användarflöde i vertikal riktning.
            </digi-typography-preamble>
          )}

          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && (<h2>Exempel</h2>)}
              <digi-code-example
                af-code={JSON.stringify(this.progressStepCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
              >
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset
                    afName="Variation"
                    afLegend="Variation"
                    onChange={(e) =>
                      (this.progressStepVariation = (e.target as any).value)
                    }
                  >
                    <digi-form-radiobutton
                      afName="Variation"
                      afLabel="Primär"
                      afValue={ProgressStepVariation.PRIMARY}
                      afChecked={
                        this.progressStepVariation ===
                        ProgressStepVariation.PRIMARY
                      }
                    />
                    <digi-form-radiobutton
                      afName="Variation"
                      afLabel="Sekundär"
                      afValue={ProgressStepVariation.SECONDARY}
                      afChecked={
                        this.progressStepVariation ===
                        ProgressStepVariation.SECONDARY
                      }
                    />
                  </digi-form-fieldset>
                  <digi-form-fieldset
                    afName="Status"
                    afLegend="Status"
                    onChange={(e) =>
                      (this.progressStepStatus = (e.target as any).value)
                    }
                  >
                    <digi-form-radiobutton
                      afName="Status"
                      afLabel="Klar"
                      afValue={ProgressStepStatus.DONE}
                      afChecked={
                        this.progressStepStatus === ProgressStepStatus.DONE
                      }
                    />
                    <digi-form-radiobutton
                      afName="Status"
                      afLabel="Nuvarande"
                      afValue={ProgressStepStatus.CURRENT}
                      afChecked={
                        this.progressStepStatus === ProgressStepStatus.CURRENT
                      }
                    />
                    <digi-form-radiobutton
                      afName="Status"
                      afLabel="Kommande"
                      afValue={ProgressStepStatus.UPCOMING}
                      afChecked={
                        this.progressStepStatus ===
                        ProgressStepStatus.UPCOMING
                      }
                    />
                  </digi-form-fieldset>
                  <digi-form-select
                    afLabel="Rubriknivå"
                    onAfOnChange={(e) =>
                      (this.progressStepHeadingLevel = (e.target as any).value)
                    }
                    af-variation="small"
                    af-start-selected={1}
                  >
                    <option value={ProgressStepHeadingLevel.H1}>{ProgressStepHeadingLevel.H1}</option>
                    <option value={ProgressStepHeadingLevel.H2}>{ProgressStepHeadingLevel.H2}</option>
                    <option value={ProgressStepHeadingLevel.H3}>{ProgressStepHeadingLevel.H3}</option>
                    <option value={ProgressStepHeadingLevel.H4}>{ProgressStepHeadingLevel.H4}</option>
                    <option value={ProgressStepHeadingLevel.H5}>{ProgressStepHeadingLevel.H5}</option>
                    <option value={ProgressStepHeadingLevel.H6}>{ProgressStepHeadingLevel.H6}</option>
                  </digi-form-select>
                </div>
                <digi-progress-step
                  afHeading={
                    'Rubrik'
                  }
                  afHeadingLevel={this.progressStepHeadingLevel}
                  af-step-status={this.progressStepStatus}
                  af-variation={this.progressStepVariation}
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Etiam magna neque, interdum vel massa eget, condimentum
                  rutrum velit. Sed vitae ullamcorper sem.
                </digi-progress-step>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <article>
              <digi-layout-container af-margin-bottom>
                <h2>Beskrivning</h2>
                <p>
                  Rubriken läggs in med <digi-code af-code="af-heading" /> och
                  har rubriknivå H2 som standard. Vill du ändra rubriknivån kan
                  du använda <digi-code af-code="af-heading-level" />.
                  Typescript-användare bör importera och använda
                  <digi-code af-code="ProgressStepHeadingLevel" />. Stylingen på
                  kommer se likadan ut även om du ändrar till en annan
                  rubriknivå.
                </p>
              </digi-layout-container>
            </article>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-container af-margin-bottom>
              <article>
                <h2>Varianter</h2>
                <p>
                  Komponenten finns i två varianter, en primär (primary) i grönt
                  som är standard och en sekundär (secondary) i blått. Variant
                  sätts med hjälp av <digi-code af-code="af-variation" />.
                </p>
              </article>
              <article>
                <h2>Status</h2>
                <p>
                  Komponenten kan visa olika statusar (Klar, nuvarande och
                  kommande) beroende på var i flödet användaren är. Statusen
                  sätts med hjälp av <digi-code af-code="af-step-status" />.
                </p>
              </article>
            </digi-layout-container>
          )}

        </digi-typography>
      </div>
    );
  }
}
