import { newSpecPage } from '@stencil/core/testing';
import { DigiFormCategoryFilter } from './digi-form-category-filter-details';

describe('digi-form-category-filter-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormCategoryFilter],
			html:
				'<digi-form-category-filter-details></digi-form-category-filter-details>'
		});
		expect(root).toEqualHtml(`
      <digi-form-category-filter-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-form-category-filter-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormCategoryFilter],
			html: `<digi-form-category-filter-details first="Stencil" last="'Don't call me a framework' JS"></digi-form-category-filter-details>`
		});
		expect(root).toEqualHtml(`
      <digi-form-category-filter-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-form-category-filter-details>
    `);
	});
});
