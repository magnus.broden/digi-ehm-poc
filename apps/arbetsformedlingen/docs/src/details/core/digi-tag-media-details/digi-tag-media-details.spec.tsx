import { newSpecPage } from '@stencil/core/testing';
import { DigiTagMedia } from './digi-tag-media-details';

describe('digi-tag-media-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiTagMedia],
			html: '<digi-tag-media-details></digi-tag-media-details>'
		});
		expect(root).toEqualHtml(`
      <digi-tag-media-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-tag-media-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiTagMedia],
			html: `<digi-tag-media-details first="Stencil" last="'Don't call me a framework' JS"></digi-tag-media-details>`
		});
		expect(root).toEqualHtml(`
      <digi-tag-media-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-tag-media-details>
    `);
	});
});
