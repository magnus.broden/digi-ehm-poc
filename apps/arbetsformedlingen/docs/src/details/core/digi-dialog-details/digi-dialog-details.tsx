import { Component, h, Host, Prop, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	DialogHeadingLevel,
	DialogSize
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-dialog-details',
	styleUrl: 'digi-dialog-details.scss'
})
export class DigiDialog {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() isActive: boolean;
	@State() dialogSize: DialogSize = DialogSize.MEDIUM;
	// @State() dialogVariation: DialogVariation = DialogVariation.PRIMARY;

	get dialogCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-dialog 
	af-size="${this.dialogSize}"
	af-show-dialog="${this.isActive ? 'true' : 'false'}"
	af-heading="Rubrik"
	af-primary-button-text="Skicka" 
	af-secondary-button-text="Avbryt"
>
</digi-dialog>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-dialog 
	[attr.af-size]="dialogSize.${Object.keys(DialogSize).find(key => DialogSize[key] === this.dialogSize)}"
	[attr.af-show-dialog]="${this.isActive ? 'true' : 'false'}"
	[attr.af-heading]="'Rubrik'" 
	[attr.af-primary-button-text]="'Skicka'" 
	[attr.af-secondary-button-text]="'Avbryt'"
>
</digi-dialog>`,
			[CodeExampleLanguage.REACT]: `\
<DigiDialog 
	afSize={dialogSize.${Object.keys(DialogSize).find(key => DialogSize[key] === this.dialogSize)}}
	afShowDialog={${this.isActive ? 'true' : 'false'}}
	afHeading="Rubrik" 
	afPrimaryButtonText="Skicka" 
	afSecondaryButtonText="Avbryt"
>
</DigiDialog>`
		};
	}

	changeSizeHandler(e) {
		this.dialogSize = e.target.value;
	}

	render() {
		return (
			<Host>
				<div class="digi-dialog-details">
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används när man behöver öppna upp ett modalt fönster
              ovanför huvudflödet för att utföra en avgränsad uppgift.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example
							afCode={JSON.stringify(this.dialogCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<div class="slot__controls" slot="controls">
								<digi-form-fieldset
									af-legend="Storlekar"
									af-name="Storlekar"
									onChange={(e) => this.changeSizeHandler(e)}
								>
									<digi-form-radiobutton
										afName="Storlekar"
										afLabel="Stor"
										afValue={DialogSize.LARGE}
									/>
									<digi-form-radiobutton
										afName="Storlekar"
										afLabel="Mellan"
										afValue={DialogSize.MEDIUM}
										afChecked={true}
									/>
									<digi-form-radiobutton
										afName="Storlekar"
										afLabel="Liten"
										afValue={DialogSize.SMALL}
									/>
								</digi-form-fieldset>
								{/* <digi-form-fieldset
									af-legend="Varianter"
									af-name="Varianter"
									onChange={(e) => this.changeVariationHandler(e)}
								>
									<digi-form-radiobutton
										afName="Varianter"
										afLabel="Primär"
										afValue={DialogVariation.PRIMARY}
										afChecked={true}
									/>
									<digi-form-radiobutton
										afName="Varianter"
										afLabel="Sekundär"
										afValue={DialogVariation.SECONDARY}
									/>
								</digi-form-fieldset> */}
							</div>
							<digi-button
								af-variation="primary"
								onAfOnClick={() => (this.isActive = !this.isActive)}
							>
								Open dialog
							</digi-button>
							<digi-dialog
								af-size={this.dialogSize}
								af-show-dialog={this.isActive ? 'true' : 'false'}
								af-heading="Rubrik"
								af-heading-level={DialogHeadingLevel.H2}
								af-primary-button-text="Skicka"
								af-secondary-button-text="Avbryt"
								onAfOnClose={() => (this.isActive = false)}
								onAfSecondaryButtonClick={() => (this.isActive = !this.isActive)}
							>
								<p>
									Det här är bara ord för att illustrera hur det ser ut med text inuti.
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
									commodo egestas elit in consequat. Proin in ex consectetur, laoreet
									augue sit amet, malesuada tellus.
								</p>
							</digi-dialog>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<article>
								<h2>Beskrivning</h2>
								<h3>Storlekar</h3>
								<p>Det modala fönstret kan ha tre olika storlekar, liten, mellan (standard) och stor.</p>
								{/* <h3>Varianter</h3>
							<p>Det finns två varianter, primär (standard) och sekundär.</p> */}
								<h3>Rubriknivå</h3>
								<p>Du kan ändra rubriknivå för rubriken genom att använda <digi-code afCode='dialog-heading-level'></digi-code>. Typescript-användare bör importera och använda <digi-code afCode='DialogHeadingLevel'></digi-code></p>
								<h3>Tillgänglighet</h3>
								<p>När man stänger ned det modala fönstret kommer fokus hamna på det sista aktiva elementet innan modalen öppnades. Behöver fokus anges på ett annat element så kan du använda dig av <digi-code afCode='afFallbackElementSelector'></digi-code>. För det mesta är det förväntade beteendet att man får fokus på det sista aktiva elementet. </p>
							</article>
						</digi-layout-container>
					)}
					{!this.afShowOnlyExample && (
						<digi-layout-block
							af-variation="symbol"
							af-vertical-padding
							af-margin-bottom
						>
							<h2>Riktlinjer</h2>
							<digi-list>
								<li>Man får endast visa en modal åt gången</li>
								<li>Texten bör vara kort men innehålla den information som användaren behöver för att kunna utföra den avgränsade uppgiften</li>
							</digi-list>
						</digi-layout-block>
					)}
				</div>
			</Host>
		);
	}
}
