import { Component, Prop, h, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormFileUploadValidation,
	FormFileUploadVariation,
	LayoutBlockVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-form-file-upload-details',
	styleUrl: 'digi-form-file-upload-details.scss'
})
export class DigiFormFileUpload {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() fileUploadExampleData: { [key: string]: any } = {
		variation: FormFileUploadVariation.PRIMARY,
		validation: FormFileUploadValidation.ENABLED
	};

	resp = [
		{
			file_id: '44d88612fea8a8f36de82e1278abb02f',
			status: 'INFECTED',
			details: {
				confidence: 5,
				malware_family: 54768315,
				malware_type: 114,
				severity: 4,
				signature_name: 'Trojan.Win32.Mitaka.TC.a'
			}
		},
		{
			file_id: '3e74f8be1ca4e3acaf843c8b4f5ef6cc',
			status: 'OK',
			details: {
				confidence: 0,
				severity: 0,
				signature_name: ''
			}
		}
	];

	get fileUploadExampleCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-file-upload 
	af-variation=${this.fileUploadExampleData.variation}
	af-validation=${this.fileUploadExampleData.validation}
	af-file-types="*"
></digi-form-file-upload>`,
		[CodeExampleLanguage.ANGULAR]:`\
<digi-form-file-upload 
	[attr.af-variation]="FormFileUpload.${Object.keys(FormFileUploadVariation).find((key) => FormFileUploadVariation[key] === this.fileUploadExampleData.variation)}"
	[attr.af-validation]="FormFileUpload.${Object.keys(FormFileUploadValidation).find((key) =>FormFileUploadValidation[key] === this.fileUploadExampleData.validation)}"
	[attr.af-file-types]="*"
></digi-form-file-upload>`,
		[CodeExampleLanguage.REACT]:`\
<DigiFormFileUpload 
	afVariation={FormFileUploadVariation.${Object.keys(FormFileUploadVariation).find((key) => FormFileUploadVariation[key] === this.fileUploadExampleData.variation)}}
	afValidation={FormFileUploadValidation.${Object.keys(FormFileUploadValidation).find((key) => FormFileUploadValidation[key] === this.fileUploadExampleData.validation)}}
	afFileTypes="*">
</DigiFormFileUpload>`
		};
	}

	@State() showExample: boolean = true;

	changeFileUploadExampleData(newData: Object) {
		this.showExample = false;
		setTimeout(() => (this.showExample = true), 0);

		this.fileUploadExampleData = newData;
	}

	changeVariationHandler(e) {
		this.changeFileUploadExampleData({
			...this.fileUploadExampleData,
			variation: e.target.value
		});
	}

	changeValidationHandler(e) {
		this.changeFileUploadExampleData({
			...this.fileUploadExampleData,
			validation: e.target.value
		});
	}

	async validateFile(scanResp) {
		await customElements.whenDefined('digi-form-file-upload');
		const paginationElement = document.querySelector('digi-form-file-upload');
		await paginationElement?.afMValidateFile(scanResp);
	}

	onUpload(e) {
		setTimeout(() => {
			const respIndex = Math.floor(Math.random() * 2);
			this.validateFile({
				...this.resp[respIndex],
				...e.detail,
				name: e.detail.name
			});
		}, 5000);
	}

	onRemove(e) {
		console.log(e.detail);
	}

	onCancel(e) {
		console.log(e.detail);
	}

	onRetry(e) {
		setTimeout(() => {
			const respIndex = Math.floor(Math.random() * 2);
			this.validateFile({
				...this.resp[respIndex],
				...e.detail,
				name: e.detail.name
			});
		}, 5000);
	}

	render() {
		return (
			<div class="digi-form-file-upload-details">
				{!this.afShowOnlyExample && (
          <digi-typography-preamble>
            Filuppladdare tillåter användare att välja eller dra en fil åt gången att
            ladda upp till en specifik plats.
          </digi-typography-preamble>
				)}
				<digi-layout-container af-no-gutter af-margin-bottom>
					{!this.afShowOnlyExample && <h2>Exempel</h2>}
					<digi-code-example
						af-code={JSON.stringify(this.fileUploadExampleCode)}
						af-hide-controls={this.afHideControls ? 'true' : 'false'}
						af-hide-code={this.afHideCode ? 'true' : 'false'}
						af-controls-position="end"
					>
						<div class="slot__controls" slot="controls">
							<digi-form-fieldset
								af-legend="Varianter"
								af-name="Varianter"
								onChange={(e) => this.changeVariationHandler(e)}
							>
								<digi-form-radiobutton
									af-name="Varianter"
									afLabel="Primär"
									afValue={FormFileUploadVariation.PRIMARY}
									afChecked={true}
								/>
								<digi-form-radiobutton
									af-name="Varianter"
									afLabel="Sekundär"
									afValue={FormFileUploadVariation.SECONDARY}
								/>
								<digi-form-radiobutton
									af-name="Varianter"
									afLabel="Tertiär"
									afValue={FormFileUploadVariation.TERTIARY}
								/>
							</digi-form-fieldset>
							<digi-form-fieldset
								af-legend="Validering"
								af-name="Validering"
								onChange={(e) => this.changeValidationHandler(e)}
							>
								<digi-form-radiobutton
									af-name="Validering"
									afLabel="På"
									afValue={FormFileUploadValidation.ENABLED}
									afChecked={true}
								/>
								<digi-form-radiobutton
									af-name="Validering"
									afLabel="Av"
									afValue={FormFileUploadValidation.DISABLED}
								/>
							</digi-form-fieldset>
						</div>
						{this.showExample && (
							<digi-form-file-upload
								onAfOnUploadFile={(e) => this.onUpload(e)}
								onAfOnCancelFile={(e) => this.onCancel(e)}
								onAfOnRemoveFile={(e) => this.onRemove(e)}
								onAfOnRetryFile={(e) => this.onRetry(e)}
								af-variation={this.fileUploadExampleData.variation}
								af-validation={this.fileUploadExampleData.validation}
								afFileTypes={'*'}
							></digi-form-file-upload>
						)}
					</digi-code-example>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom afMarginTop>
							<h2>Beskrivning</h2>
							<p>
								Exemplet ovan är inte uppkopplat till antivirus tjänsten och visar
								därför inte riktig data.
							</p>

							<h3>Varianter</h3>
							<p>
								Filuppladdaren finns i tre varianter, primär (primary), sekundär
								(secondary) och tertiär (tertiary), som du kan välja med{' '}
								<digi-code af-variation="light" af-code="af-variation" />.
							</p>
							<p>
								Den sekundära varianten är till för mindre enheter och har därför ingen
								drag and drop-funktionalitet.
							</p>
							<h3>Validering</h3>
							<p>
								Som standard är validering av filer på för komponenten Filuppladdare,
								denna funktion går att stänga av men gör man detta ska man vara säker på
								riskerna detta medföljer.
							</p>
							<p>
								Med validering påslagen så skickar komponenten ut filen som blivit
								upladdad, denna fil behöver sedan bli skickad till Byggstenen Skydda's
								api för virusskanning, mer om detta kan man läsa om i deras
								dokumentation av api:et.
							</p>
							<p>
								Komponenten behöver sedan få ett svar om filen blivit godkänd eller
								inte, detta görs med en av komponentens metoder som heter
								"afMValidateFile". afMValidateFile förväntar sig ett filobjekt med
								filens id, namn och status samt filen själv. Filens status kan antigen
								vara "OK" eller "error".
							</p>
							<p>
								Läs mer om API-dokumentationen här: <br />
								<digi-link afHref="https://confluence.arbetsformedlingen.se/pages/viewpage.action?pageId=80547393">
									Antivirus api av Byggstenen Skydda
								</digi-link>
							</p>

							<h3>Användning</h3>
							<digi-list>
								<li>
									Ge input fältet i komponenten ett id genom att använda{' '}
									<digi-code af-variation="light" af-code=" af-id" />, annars kommer ett
									autogeneras.
								</li>
								<li>
									Attributet <digi-code af-variation="light" af-code=" af-max-files" />{' '}
									anger det maximala antalet filer som kan laddas upp.
								</li>
								<li>
									Med attributet{' '}
									<digi-code af-variation="light" af-code=" af-file-max-size" /> sätts en
									maximal filstorlek som kan laddas upp.{' '}
								</li>
								<li>
									Attributet
									<digi-code af-variation="light" af-code=" af-file-types" /> anger vilka
									filtyper som är accepterade, formatet skrivs i samma form som ett
									accept-attribut för{' '}
									<digi-code afCode="<input [type=file] />"></digi-code>. Obligatoriskt!{' '}
								</li>
								<li>
									Sätt rubrik för uppladdade filer genom att sätta attributet{' '}
									<digi-code af-variation="light" af-code="af-heading-files" />
								</li>
								<li>
									Sätt rubiknivå genom att sätta attributet{' '}
									<digi-code af-variation="light" af-code="af-heading-level" />
								</li>

								<li>
									Ange filuppladdarens etikett genom att använda{' '}
									<digi-code af-variation="light" af-code=" af-label" />
								</li>
								<li>
									Med attributet{' '}
									<digi-code af-variation="light" af-code=" af-label-description" />{' '}
									anger du en beskrivningstext.{' '}
								</li>
								<li>
									Med attributet{' '}
									<digi-code af-variation="light" af-code=" af-upload-btn-text" /> anger
									du text på uppladdningsknappen.{' '}
								</li>
								<li>
									Använd metoden{' '}
									<digi-code af-variation="light" af-code=" afMValidateFile " /> för att
									validera fil efter scanning. Metoden förväntar sig få ett objekt med
									filens namn eller id och status på filen. Om filen är godkänd ska den
									ha statusvärdet 'OK'. Vill man skicka med ett error meddelande med
									filen så ska den ligga under nyckelvärdet 'error' i filobjektet,
									statusvärdet ska då vara 'error'.
								</li>
								<li>
									Kalla på metoden{' '}
									<digi-code af-variation="light" af-code=" afMGetAllFiles " /> så
									returnerar metoden alla uppladdade filer, är validering påslagen
									returneras bara de godkända filerna ut.
								</li>
								<li>
									Man kan importa en array med fil objekt med metoden
									<digi-code af-variation="light" af-code=" afMImportFiles " />. Filerna
									kommer inte trigga uppladdningsevent, de kommer endast visas upp med
									den informationen som de importades med. Ett filobjekt måste innehålla
									id, status och filen själv, har filen ett error meddelade kopplat till
									sig bör de skickas med också under 'error'. Status kan ha värdet
									'accepted', 'pending' eller 'error', är det något annat så kommer filen
									inte att visas upp. Fil dubbleter läggs inte till i komponenten.
								</li>
							</digi-list>
						</digi-layout-container>
					)}
					{!this.afShowOnlyExample && (
						<digi-layout-block
							af-variation={LayoutBlockVariation.SYMBOL}
							af-vertical-padding
							af-margin-bottom
						>
							<h2>Övriga riktlinjer</h2>
							<digi-list>
								<li>
									Uppkoppling mot Skyddas Api måste gå via en backend för
									frontend-lösningen, tex en Node Express server, som ska skydda token
									som behövs för att göra anrop.
								</li>
							</digi-list>
						</digi-layout-block>
					)}
				</digi-layout-container>
			</div>
		);
	}
}
