import { Component, Prop, h, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	ProgressStepsHeadingLevel,
	ProgressStepsVariation
} from '@digi/arbetsformedlingen';
// import { LayoutBlockVariation } from 'libs/core/src';

@Component({
	tag: 'digi-progress-steps-details',
	styleUrl: 'digi-progress-steps-details.scss'
})
export class DigiProgressStepsDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() progressStepsVariation: ProgressStepsVariation =
		ProgressStepsVariation.PRIMARY;
	@State() progressStepsHeadingLevel: ProgressStepsHeadingLevel =
		ProgressStepsHeadingLevel.H2;
	@State() currentProgressSteps = 2;

	get progressStepsCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-progress-steps 
	af-current-step="${this.currentProgressSteps}"
	af-variation="${this.progressStepsVariation}"
	af-heading-level="${this.progressStepsHeadingLevel}"
>
<digi-progress-step afHeading="Rubrik 1">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-step>
<digi-progress-step afHeading="Rubrik 2">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-step>
<digi-progress-step afHeading="Rubrik 3">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-step>
</digi-progress-steps>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-progress-steps 
	[attr.af-current-step]="1"
	[attr.af-variation]="ProgressStepsVariation.${Object.keys(ProgressStepsVariation).find((key) => ProgressStepsVariation[key] === this.progressStepsVariation)}"
	[attr.af-heading-level]="ProgressStepsHeadingLevel.${Object.keys(ProgressStepsHeadingLevel).find((key) => ProgressStepsHeadingLevel[key] === this.progressStepsHeadingLevel)}"
>
<digi-progress-step afHeading="Rubrik 1">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-step>
<digi-progress-step afHeading="Rubrik 2">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-step>
<digi-progress-step afHeading="Rubrik 3">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-step>
</digi-progress-steps>`,
			[CodeExampleLanguage.REACT]: `\
<DigiProgressSteps 
	afCurrentStep={1}
	afVariation={ProgressStepsVariation.${Object.keys(ProgressStepsVariation).find((key) => ProgressStepsVariation[key] === this.progressStepsVariation)}}
	afHeadingLevel={ProgressStepsHeadingLevel.${Object.keys(ProgressStepsHeadingLevel).find((key) => ProgressStepsHeadingLevel[key] === this.progressStepsHeadingLevel)}}
>
<DigiProgressStep afHeading="Rubrik 1">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</DigiProgressStep>
<DigiProgressStep afHeading="Rubrik 2">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</DigiProgressStep>
<DigiProgressStep afHeading="Rubrik 3">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</DigiProgressStep>
</DigiProgressSteps>`
		};
	}

	render() {
		return (
			<div class="digi-progress-steps-details">
				<digi-typography>

					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används tillsammans med{' '}
              <digi-code
                af-code="<digi-progress-step>"
              />{' '}
              för att visualisera ett användarflöde i vertikal riktning.
            </digi-typography-preamble>
					)}

					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example
								af-code={JSON.stringify(this.progressStepsCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
							>
								<digi-progress-steps
									af-current-step={this.currentProgressSteps}
									af-variation={this.progressStepsVariation}
									af-heading-level={this.progressStepsHeadingLevel}
								>
									<digi-progress-step
										afHeading={
											'Rubrik 1'
										}
									>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										Etiam magna neque, interdum vel massa eget, condimentum
										rutrum velit. Sed vitae ullamcorper sem.
									</digi-progress-step>
									<digi-progress-step
										afHeading={
											'Rubrik 2'
										}
									>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										Etiam magna neque, interdum vel massa eget, condimentum
										rutrum velit. Sed vitae ullamcorper sem.
									</digi-progress-step>
									<digi-progress-step
										afHeading={
											'Rubrik 3'
										}
									>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										Etiam magna neque, interdum vel massa eget, condimentum
										rutrum velit. Sed vitae ullamcorper sem.
									</digi-progress-step>
								</digi-progress-steps>

								<div class="slot__controls" slot="controls">
									<digi-form-fieldset
										afName="Variation"
										afLegend="Variation"
										onChange={(e) =>
											(this.progressStepsVariation = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Primär"
											afValue={ProgressStepsVariation.PRIMARY}
											afChecked={
												this.progressStepsVariation ===
												ProgressStepsVariation.PRIMARY
											}
										/>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Sekundär"
											afValue={ProgressStepsVariation.SECONDARY}
											afChecked={
												this.progressStepsVariation ===
												ProgressStepsVariation.SECONDARY
											}
										/>
									</digi-form-fieldset>

									<digi-form-select
										afLabel="Nuvarande steg"
										onAfOnChange={(e) =>
											(this.currentProgressSteps = (e.target as any).value)
										}
										af-variation="small"
										af-start-selected={this.currentProgressSteps - 1}
									>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</digi-form-select>
									<digi-form-select
										afLabel="Rubriknivå"
										onAfOnChange={(e) =>
											(this.progressStepsHeadingLevel = (e.target as any).value)
										}
										af-variation="small"
										af-start-selected={1}
									>
										<option value={ProgressStepsHeadingLevel.H1}>{ProgressStepsHeadingLevel.H1}</option>
										<option value={ProgressStepsHeadingLevel.H2}>{ProgressStepsHeadingLevel.H2}</option>
										<option value={ProgressStepsHeadingLevel.H3}>{ProgressStepsHeadingLevel.H3}</option>
										<option value={ProgressStepsHeadingLevel.H4}>{ProgressStepsHeadingLevel.H4}</option>
										<option value={ProgressStepsHeadingLevel.H5}>{ProgressStepsHeadingLevel.H5}</option>
										<option value={ProgressStepsHeadingLevel.H6}>{ProgressStepsHeadingLevel.H6}</option>
									</digi-form-select>

								</div>
							</digi-code-example>
						</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-margin-bottom>
							<article>
								<h2>Beskrivning</h2>
								<p>
									Rubriknivån för alla förloppsteg kan ändras med
									<digi-code af-code="af-heading-level" />, standard är H2.
									Typescript-användare bör importera och använda
									<digi-code af-code="ProgressStepsHeadingLevel" />. Stylingen
									på kommer se likadan ut även om du ändrar till en annan
									rubriknivå.
								</p>

								<h3>Variationer</h3>
								<p>
									Komponenten finns i två varianter, en primär (primary) i grönt
									som är standard och en sekundär (secondary) i blått. Variation
									sätts med hjälp av
									<digi-code af-code="af-variation" />.
								</p>
							</article>
						</digi-layout-container>
					)}
				</digi-typography>
			</div>
		);
	}
}
