import { Component, Prop, h, State } from '@stencil/core';
import { InfoCardVariation, InfoCardType, InfoCardHeadingLevel, InfoCardSize, InfoCardBorderPosition, CodeExampleLanguage, NotificationAlertSize } from '@digi/arbetsformedlingen';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'digi-info-card-details',
	styleUrl: 'digi-info-card-details.scss'
})
export class DigiInfoCardDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() infoCardType: InfoCardType = InfoCardType.TIP;
	@State() infoCardVariation: InfoCardVariation = InfoCardVariation.PRIMARY;
	@State() infoCardHeadingLevel: InfoCardHeadingLevel = InfoCardHeadingLevel.H2;
	@State() infoCardSize: InfoCardSize = InfoCardSize.STANDARD;
	@State() infoCardBorderPosition: InfoCardBorderPosition = InfoCardBorderPosition.TOP;

	get infoCardCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-info-card
	af-heading="Jag är ett infokort"
	af-heading-level="h2"
	af-type="${this.infoCardType}"
	af-link-href="Frivillig länk"\
	\n \taf-link-text="Frivillig länktext"\
	${this.infoCardType ? `\n \taf-variation="${this.infoCardVariation}"` : ''}\
	${this.infoCardType == InfoCardType.TIP ? `\n \taf-size="${this.infoCardSize}"` : `\n \taf-border-position="${this.infoCardBorderPosition}"`}
>
	<p>
		Det här är bara ord för att illustrera hur det ser ut med text inuti.
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Suspendisse commodo egestas elit in consequat. Proin in ex consectetur, 
		laoreet augue sit amet, malesuada tellus.
	</p>
</digi-info-card>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-info-card
	[attr.af-heading]="'Jag är ett infokort'"
	[attr.af-heading-level]="InfoCardHeadingLevel.H2"
	[attr.af-type]="InfoCardType.${Object.keys(InfoCardType).find((key) => InfoCardType[key] === this.infoCardType)}"
	[attr.af-link-href]="'Frivillig länk'"\
	\n \t[attr.af-link-text]="'Frivillig länktext'"\
	${this.infoCardType ? `\n \t[attr.af-variation]="InfoCardVariation.${Object.keys(InfoCardVariation).find((key) => InfoCardVariation[key] === this.infoCardVariation)}"` : ''}\
	${this.infoCardType == InfoCardType.TIP ? `\n \t[attr.af-size]="infoCardSize.${Object.keys(InfoCardSize).find((key) => InfoCardSize[key] === this.infoCardSize)}"` : 
	`\n \t[attr.af-border-position]="infoCardBorderPosition.${Object.keys(InfoCardBorderPosition).find((key) => InfoCardBorderPosition[key] === this.infoCardBorderPosition)}"`}
>
	<p>
		Det här är bara ord för att illustrera hur det ser ut med text inuti.
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Suspendisse commodo egestas elit in consequat. Proin in ex consectetur, 
		laoreet augue sit amet, malesuada tellus.
	</p>
</digi-info-card>`,
			[CodeExampleLanguage.REACT]: `\
<DigiInfoCard
	afHeading="Jag är ett infokort"
	afHeadingLevel={InfoCardHeadingLevel.H2}
	afType={InfoCardType.${Object.keys(InfoCardType).find((key) => InfoCardType[key] === this.infoCardType)}}
	afLinkHref="Frivillig länk"\
	\n \tafLinkText="Frivillig länktext"\
	${this.infoCardType ? `\n \tafVariation={InfoCardVariation.${Object.keys(InfoCardVariation).find((key) => InfoCardVariation[key] === this.infoCardVariation)}}` : ''}\
	${this.infoCardType == InfoCardType.TIP ? `\n \tafSize={infoCardSize.${Object.keys(InfoCardSize).find((key) => InfoCardSize[key] === this.infoCardSize)}}` : 
	`\n \tafBorderPosition={infoCardBorderPosition.${Object.keys(InfoCardBorderPosition).find((key) => InfoCardBorderPosition[key] === this.infoCardBorderPosition)}}`}
>
	<p>
		Det här är bara ord för att illustrera hur det ser ut med text inuti. Lorem ipsum dolor sit amet,
		consectetur adipiscing elit. Suspendisse commodo egestas elit in consequat. Proin in ex consectetur,
		laoreet augue sit amet, malesuada tellus.
	</p>
</DigiInfoCard>`,

		};
	}
	render() {
		return (
			<div class="digi-info-card-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
						<digi-typography-preamble>
							Infokort används för relaterat innehåll som exempelvis tips- och
							faktarutor.
						</digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example af-code={JSON.stringify(this.infoCardCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
								{this.infoCardType === InfoCardType.TIP && (
									<digi-info-card
										afHeading="Jag är ett Fakta/tipskort"
										afHeadingLevel={InfoCardHeadingLevel.H2}
										afVariation={this.infoCardVariation}
										afType={this.infoCardType}
										afSize={this.infoCardSize}
										af-link-text="Frivillig länktext"
										af-link-href="Frivillig länk"
									>
										<p>
											Det här är bara ord för att illustrera hur det ser ut
											med text inuti. Lorem ipsum dolor sit amet,
											consectetur adipiscing elit. Suspendisse commodo
											egestas elit in consequat. Proin in ex consectetur,
											laoreet augue sit amet, malesuada tellus.
										</p>
									</digi-info-card>
								)}
								{this.infoCardType === InfoCardType.RELATED && (
									<digi-info-card
										afHeading="Jag är ett relaterat kort"
										afHeadingLevel={InfoCardHeadingLevel.H2}
										afVariation={this.infoCardVariation}
										afType={this.infoCardType}
										afBorderPosition={this.infoCardBorderPosition}
										af-link-text="Frivillig länktext"
										af-link-href="Frivillig länk"
									>
										<p>
											Det här är bara ord för att illustrera hur det ser ut
											med text inuti. Lorem ipsum dolor sit amet,
											consectetur adipiscing elit. Suspendisse commodo
											egestas elit in consequat. Proin in ex consectetur,
											laoreet augue sit amet, malesuada tellus.
										</p>
									</digi-info-card>
								)}

								<div class="slot__controls" slot="controls">
									<digi-form-fieldset
										afName="Type"
										afLegend="Typ"
										onChange={(e) =>
											(this.infoCardType = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="Type"
											afLabel="Fakta- och tipskort"
											afValue={InfoCardType.TIP}
											afChecked={this.infoCardType === InfoCardType.TIP}
										/>
										<digi-form-radiobutton
											afName="Type"
											afLabel="Relaterat kort"
											afValue={InfoCardType.RELATED}
											afChecked={this.infoCardType === InfoCardType.RELATED}
										/>
									</digi-form-fieldset>

									{(this.infoCardType === InfoCardType.TIP &&
										<digi-form-fieldset
											afName="Size"
											afLegend="Storlek"
											onChange={(e) =>
												(this.infoCardType = (e.target as any).value)
											}
										>
											<digi-form-radiobutton
												afName="Size"
												afLabel="Standardkort"
												afValue={InfoCardSize.STANDARD}
												afChecked={this.infoCardSize === InfoCardSize.STANDARD}
											/>
										</digi-form-fieldset>)}

									{(this.infoCardType === InfoCardType.RELATED &&
										<digi-form-fieldset
											afName="Position"
											afLegend="Justering"
											onChange={(e) =>
												(this.infoCardBorderPosition = (e.target as any).value)
											}
										>
											<digi-form-radiobutton
												afName="Position"
												afLabel="Toplinje"
												afValue={InfoCardBorderPosition.TOP}
												afChecked={this.infoCardBorderPosition === InfoCardBorderPosition.TOP}
											/>
											<digi-form-radiobutton
												afName="Position"
												afLabel="Vänsterlinje"
												afValue={InfoCardBorderPosition.LEFT}
												afChecked={this.infoCardBorderPosition === InfoCardBorderPosition.LEFT}
											/>
										</digi-form-fieldset>)}


									<digi-form-fieldset
										afName="Variant"
										afLegend="Variant"
										onChange={(e) =>
											(this.infoCardVariation = (e.target as any).value)
										}
									>
										{' '}
										{this.infoCardType && (
											<span>
												<digi-form-radiobutton
													afName="Variant"
													afLabel="Primär"
													afValue={InfoCardVariation.PRIMARY}
													afChecked={
														this.infoCardVariation ===
														InfoCardVariation.PRIMARY
													}
												/>
												<digi-form-radiobutton
													afName="Variant"
													afLabel="Sekundär"
													afValue={InfoCardVariation.SECONDARY}
													afChecked={
														this.infoCardVariation ===
														InfoCardVariation.SECONDARY
													}
												/>{' '}
											</span>
										)}
									</digi-form-fieldset>
								</div>
							</digi-code-example>
						</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <digi-notification-alert
                afSize={NotificationAlertSize.MEDIUM}
              >
                <span class="breaking-tag">Breaking changes</span> - version 19.0.0. Läs nedan vad du behöver ändra.
              </digi-notification-alert><br />
              <digi-expandable-accordion
                af-heading="Migreringsguide till version 19.0.0"
              >
                <digi-list>
                  <li>
                    Infokort - Tips
                    <digi-list>
                      <li>Tidigare kod: <digi-code afCode={`<digi-info-card af-type="tip" ...`} af-code={`<digi-info-card af-type="tip" ...`}></digi-code></li>
                      <li>Ny kod: Inga ändringar</li>
                    </digi-list>
                  </li>
                  <li>
                    Infokort - Info
                    <digi-list>
                      <li>Tidigare kod: <digi-code afCode={`<digi-info-card af-type="info" ...`} af-code={`<digi-info-card af-type="info" ...`}></digi-code></li>
                      <li>Ny kod: <digi-code afCode={`<digi-info-card af-type="related" af-border-position="top" af-variation="secondary" ...`} af-code={`<digi-info-card af-type="related" af-border-position="top" af-variation="secondary" ...`}></digi-code></li>
                    </digi-list>
                  </li>
                  <li>
                    Infokort - Singel
                    <digi-list>
                      <li>Tidigare kod: <digi-code afCode={`<digi-info-card af-type="single" ...`} af-code={`<digi-info-card af-type="single" ...`}></digi-code></li>
                      <li>Ny kod: <digi-code afCode={`<digi-info-card af-type="related" af-border-position="left" af-variation="secondary" ...`} af-code={`<digi-info-card af-type="related" af-border-position="left" af-variation="secondary" ...`}></digi-code></li>
                    </digi-list>
                  </li>
                  <li>
                    Infokort - Multi
                    <digi-list>
                      <li>Tidigare kod: <digi-code afCode={`<digi-info-card af-type="multi" ...`} af-code={`<digi-info-card af-type="multi" ...`}></digi-code></li>
                      <li>Ny kod: <digi-code afCode={`<digi-info-card-multi af-type="related" ...`} af-code={`<digi-info-card-multi af-type="related" ...`}></digi-code><br />
                      Ny komponent: <a {...href('/komponenter/digi-info-card-multi/oversikt')}>Multikort</a></li>
                    </digi-list>
                  </li>
                </digi-list>
              </digi-expandable-accordion><br /><br />
              <digi-info-card
                afHeading="Riktlinjer"
                afHeadingLevel={InfoCardHeadingLevel.H3}
              >
                <div class="digi-typography digi-typography--s">
                <digi-list>
                  <li>Fakta- och tipskort primär har grön bakgrund och används för innehåll som ska skilja sig från löpande brödtext.</li>
                  <li>Fakta- och tipskort sekundär har grå bakgrund och används om helhetsintrycket på sidan blir för rörigt med grön bakgrund.</li>
                  <li>Relaterat infokort, används i slutet av innehållet för att puffa för relaterat innehåll och leda vidare till nästa steg.</li>
                  <li>Relaterat infokort finns i två varianter, med linje till vänster eller i toppen. Detta för att kunna särskilja detta kort i en varierad grafisk miljö där andra kort-typer inte fungerar ihop.</li>
                </digi-list>
                </div>
              </digi-info-card>
            </digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
