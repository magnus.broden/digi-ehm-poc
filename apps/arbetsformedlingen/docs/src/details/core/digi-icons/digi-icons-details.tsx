import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'digi-icons-details',
  styleUrl: 'digi-icons-details.scss',
})
export class DigiIconsDetails {
  @Prop() component: string;

  render() {
    return (
      <div class="digi-icons-details">
        <digi-typography>
          <digi-layout-block af-variation="primary">
            <digi-typography-preamble>
              Ikoner används...
            </digi-typography-preamble>
          </digi-layout-block>
          <digi-layout-block>
            <h2>Ikoner...</h2>
          </digi-layout-block>
        </digi-typography>
      </div>
    );
  }
}
