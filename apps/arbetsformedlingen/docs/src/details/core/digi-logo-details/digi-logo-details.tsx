import { Component, Prop, h, State } from '@stencil/core';
import { CodeExampleLanguage, LogoColor, LogoVariation } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-logo-details',
	styleUrl: 'digi-logo-details.scss'
})
export class DigiLogoDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasVariant: boolean = true;
	@State() hasTitle: boolean = false;
	@State() logoColor: LogoColor = LogoColor.PRIMARY;
	@State() logoVariation: LogoVariation = LogoVariation.LARGE;

	get logoCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-logo
	af-variation="${this.logoVariation}"
	af-color="${this.logoColor}"\
	${this.hasTitle ? `\n\taf-system-name="Titellogotyp"\t` : ''}
>
</digi-logo>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-logo 
	[attr.af-variation]="LogoVariation.${Object.keys(LogoVariation).find(key => LogoVariation[key] === this.logoVariation)}"
	[attr.af-color]="LogoColor.${Object.keys(LogoColor).find(key => LogoColor[key] === this.logoColor)}"\
	${this.hasTitle ? `\n\t[attr.af-system-name]="'Titellogotyp"'"\t` : ''}
>
</digi-logo>`,
			[CodeExampleLanguage.REACT]: `\
<DigiLogo 
	afVariation={LogoVariation.${Object.keys(LogoVariation).find(key => LogoVariation[key] === this.logoVariation)}}
	afColor={LogoColor.${Object.keys(LogoColor).find(key => LogoColor[key] === this.logoColor)}}\
	${this.hasTitle ? `\n\tafSystemName="Titellogotyp"\t` : ''}
>
</DigiLogo>`
		}
	}

	@State() showSystemName: boolean;

	render() {
		return (
			<div class="digi-logo-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Det här är en komponent med olika varianter av
              Arbetsförmedlingens logotyp. Läs mer information om logotypen och dess riktlinjer på grafiska profilen. <digi-link afHref="https://designsystem.arbetsformedlingen.se/grafisk-profil/logotyp"> Se grafiska profilen</digi-link>
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.logoCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							{
								<digi-layout-block af-variation={this.logoColor === LogoColor.SECONDARY && this.logoVariation === LogoVariation.LARGE ? "profile" : "transparent"} >
									<digi-logo
										afTitle={this.hasTitle ? "Arbetsförmedlingen" : ""}
										afSystemName={this.hasTitle && this.logoVariation === LogoVariation.LARGE ? "Titellogotyp" : ""}
										afVariation={this.logoVariation}
										afColor={this.logoColor}
									>
									</digi-logo>
								</digi-layout-block>
							}
							<div class="slot__controls" slot="controls">
								<digi-form-fieldset
									afName="Variant"
									afLegend="Storlek"
									onChange={(e) =>
										(this.logoVariation = (e.target as any).value)
									}
								>
									<digi-form-radiobutton
										afName="Storlek"
										afLabel="Stor"
										afValue={LogoVariation.LARGE}
										afChecked={this.logoVariation === LogoVariation.LARGE}
										onAfOnChange={() => this.hasVariant = true}
									/>
									<digi-form-radiobutton
										afName="Storlek"
										afLabel="Liten"
										afValue={LogoVariation.SMALL}
										afChecked={this.logoVariation === LogoVariation.SMALL}
										onAfOnChange={() => this.hasVariant = false}
									/>
								</digi-form-fieldset>
								<digi-form-fieldset
									afName="Variant"
									afLegend="Variant"
									onChange={(e) =>
										(this.logoColor = (e.target as any).value)
									}
								>
									{this.hasVariant && (
										<digi-form-radiobutton
											afName="Variant"
											afLabel="Primär"
											afValue={LogoColor.PRIMARY}
											afChecked={this.logoColor === LogoColor.PRIMARY}
										/>
									)
									}
									{this.hasVariant && (
										<digi-form-radiobutton
											afName="Variant"
											afLabel="Sekundär"
											afValue={LogoColor.SECONDARY}
											afChecked={this.logoColor === LogoColor.SECONDARY}
										/>
									)
									}
									{!this.hasVariant && (
										<p>
											<em>
												Logo varianter är inte tillgängliga för
												liten storlek.
											</em>
										</p>
									)
									}
								</digi-form-fieldset>
								<digi-form-fieldset>
									{this.hasVariant && (
										<digi-form-checkbox
											afLabel="Eget namn"
											afChecked={this.hasTitle}
											onAfOnChange={() => this.hasTitle ? this.hasTitle = false : this.hasTitle = true}
										></digi-form-checkbox>
									)
									}
									{!this.hasVariant && (
										<p>
											<em>
												Titleelementet är inte tillgängliga för
												liten storlek.
											</em>
										</p>
									)
									}
								</digi-form-fieldset>
							</div>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-margin-bottom>
							<h3>Storlekar</h3>
							<p>
								Logotypen finns i två storlekar, en stor och en liten, varav den stora med text är standard. För att ändra storlek används <digi-code af-code="af-variation" />.
							</p>
							<h3>Färger</h3>
							<p>
								Den stora varianten finns också i två färger, en primär med mörk text som kan användas på ljus bakgrund och en sekundär med vit text som kan användas på mörk bakgrund. För att ändra färg används <digi-code af-code="af-color" />.{' '}
							</p>
							<h3>Med systemnamn</h3>
							<p>
								Om du vill använda logotypen tillsammans med ett eget namn så kan du använda <digi-code af-code="af-system-name" />.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		)
	}
}
