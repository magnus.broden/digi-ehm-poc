import { newSpecPage } from '@stencil/core/testing';
import { DigiInfoCardMultiDetails } from './digi-info-card-multi-details';

describe('digi-info-card-multi-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiInfoCardMultiDetails],
			html: '<digi-info-card-multi-details></digi-info-card-multi-details>'
		});
		expect(root).toEqualHtml(`
      <digi-info-card-multi-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-info-card-multi-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiInfoCardMultiDetails],
			html: `<digi-info-card-multi-details first="Stencil" last="'Don't call me a framework' JS"></digi-info-card-multi-details>`
		});
		expect(root).toEqualHtml(`
      <digi-info-card-multi-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-info-card-multi-details>
    `);
	});
});
