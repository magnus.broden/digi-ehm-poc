import { Component, Prop, h, State, getAssetPath } from '@stencil/core';
import { CodeExampleLanguage, MediaFigureAlignment } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-media-figure-details',
	styleUrl: 'digi-media-figure-details.scss'
})
export class DigiMediaFigureDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasCaption: boolean = true;
	@State() mediaFigureAlignment: MediaFigureAlignment =
		MediaFigureAlignment.START;

	get mediaFigureCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-media-figure\
	${this.hasCaption ? `\n\taf-figcaption="Jag är en bildtext"
	af-alignment="${this.mediaFigureAlignment}"\n >` : '>'}
	<digi-media-image af-src="..."/>
</digi-media-figure>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-media-figure
	${this.hasCaption ? `\n\t[attr.af-figcaption]="Jag är en bildtext"
	[attr.af-alignment]="MediaFigureAlignment.${Object.keys(MediaFigureAlignment).find((key) => MediaFigureAlignment[key] === this.mediaFigureAlignment)}"\n >` : '>'}
	<digi-media-image [attr.af-src]="..."/>
</digi-media-figure>`,
			[CodeExampleLanguage.REACT]: `\
<DigiMediaFigure\
	${this.hasCaption ? `\n\tafFigcaption="Jag är en bildtext"
	afAlignment={MediaFigureAlignment.${Object.keys(MediaFigureAlignment).find((key) => MediaFigureAlignment[key] === this.mediaFigureAlignment)}}\n >` : '>'}
	<digi-media-image afSrc="..."/>
</DigiMediaFigure>`
		};
	}
	render() {
		return (
			<div class="digi-media-figure-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Denna komponent kan användas för att omsluta en bild eller vår
              mediebildkomponent. Det går också att lägga till en bildtext.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.mediaFigureCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<div class="slot__controls" slot="controls">
								<digi-form-fieldset afLegend="Bildtext">
									<digi-form-checkbox
										afLabel="Ja"
										afChecked={this.hasCaption}
										onAfOnChange={() =>
											this.hasCaption
												? (this.hasCaption = false)
												: (this.hasCaption = true)
										}
									></digi-form-checkbox>
								</digi-form-fieldset>
								{this.hasCaption && (
									<digi-form-fieldset
										afName="Justering"
										afLegend="Bildtextens placering"
										onChange={(e) =>
											(this.mediaFigureAlignment = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="Justering"
											afLabel="Vänster"
											afValue={MediaFigureAlignment.START}
											afChecked={this.mediaFigureAlignment === MediaFigureAlignment.START}
										/>
										<digi-form-radiobutton
											afName="Justering"
											afLabel="Center"
											afValue={MediaFigureAlignment.CENTER}
											afChecked={
												this.mediaFigureAlignment === MediaFigureAlignment.CENTER
											}
										/>
										<digi-form-radiobutton
											afName="Justering"
											afLabel="Höger"
											afValue={MediaFigureAlignment.END}
											afChecked={this.mediaFigureAlignment === MediaFigureAlignment.END}
										/>
									</digi-form-fieldset>
								)}
							</div>
							<div style={{ width: '420px' }}>
								{this.hasCaption && (
									<digi-media-figure
										af-figcaption="Jag är en bildtext"
										af-alignment={this.mediaFigureAlignment}
									>
										<digi-media-image
											afUnlazy
											afHeight="400"
											afWidth="400"
											af-src={getAssetPath('/assets/images/logotype-sign.svg')}
											afAlt="Arbetsförmedlingens logotyp som en fasadskyld"
										></digi-media-image>
									</digi-media-figure>
								)}
								{!this.hasCaption && (
									<digi-media-figure af-alignment={this.mediaFigureAlignment}>
										<digi-media-image
											afUnlazy
											afHeight="400"
											afWidth="400"
											af-src={getAssetPath('/assets/images/logotype-sign.svg')}
											afAlt="Arbetsförmedlingens logotyp som en fasadskyld"
										></digi-media-image>
									</digi-media-figure>
								)}
							</div>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container afNoGutter afMarginBottom>
							<h2>Beskrivning</h2>
							<h2>Placering</h2>
							<p>
								Bildtextens placering kan ändras med hjälp av{' '}
								<digi-code af-code="af-alignment" />. Alternativen som finns är{' '}
								<digi-code af-code="start" /> (vänsterjusterad),{' '}
								<digi-code af-code="center" /> (centrerad) och{' '}
								<digi-code af-code="end" /> (högerjusterad), varav{' '}
								<digi-code af-code="start" /> är standard. Typescript-användare bör
								importera och använda <digi-code af-code="MediaFigureAlignment" />.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
