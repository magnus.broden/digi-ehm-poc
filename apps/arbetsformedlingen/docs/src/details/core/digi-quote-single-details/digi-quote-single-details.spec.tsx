import { newSpecPage } from '@stencil/core/testing';
import { DigiQuoteSingle } from './digi-quote-single-details';

describe('digi-quote-single-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiQuoteSingle],
			html: '<digi-quote-single-details></digi-quote-single-details>'
		});
		expect(root).toEqualHtml(`
      <digi-quote-single-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-quote-single-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiQuoteSingle],
			html: `<digi-quote-single-details first="Stencil" last="'Don't call me a framework' JS"></digi-quote-single-details>`
		});
		expect(root).toEqualHtml(`
      <digi-quote-single-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-quote-single-details>
    `);
	});
});
