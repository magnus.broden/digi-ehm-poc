import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'digi-util-intersection-observer-details',
  styleUrl: 'digi-util-intersection-observer-details.scss'
})
export class DigiUtilIntersectionObserverDetails {
  @Prop() component: string;

  render() {
    return (
      <div class="digi-util-intersection-observer-details">
        <digi-typography>
          <digi-layout-block af-variation="primary">
            <digi-typography-preamble>
              Detta är en komponent som implementerar {' '}
              <digi-link af-variation="small" afHref="https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API">
                Intersection Observer API.
              </digi-link>
              Den avger händelser när den matchar satta egenskaper
              (standard är när den skapas och lämnar
              viewport).
              <br />
              <br />
              <digi-link af-variation="small" afHref="https://digi-core.netlify.app/?path=/docs/util-digi-util-intersection-observer--standard" af-target="_blank">
                <digi-icon-arrow-right slot="icon"></digi-icon-arrow-right>
                Länk till dokumentation i Storybook
              </digi-link>
            </digi-typography-preamble>
          </digi-layout-block>
        </digi-typography>
      </div>
    );
  }
}
