import { Component, Prop, h, State, Listen } from '@stencil/core';
import {
	CodeExampleLanguage,
	NavigationVerticalMenuVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-navigation-vertical-menu-item-details',
	styleUrl: 'digi-navigation-vertical-menu-item-details.scss'
})
export class DigiNavigationVerticalMenuItem {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasActiveSubnav: boolean = false;
	@State() isActive: boolean = true;
	@State() hasIcon: boolean = true;
	@State() navigationVerticalMenuVariation: NavigationVerticalMenuVariation = NavigationVerticalMenuVariation.PRIMARY;

	get navigationVericalMenuItemCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-vertical-menu 
	af-variation="${this.navigationVerticalMenuVariation}"
>
	<ul>
		<li>
			<digi-navigation-vertical-menu-item
				af-text="Menyval"
				af-active="${this.isActive}"
				af-active-subnav="false"\
				${this.isActive ? `\n\t\t\t\taf-aria-current="page"\t ` : `\ `}
			>\
			${this.hasIcon ? `\n\t\t\t<digi-icon-home slot="icon" />` : ""}\	
			</digi-navigation-vertical-menu-item>
		</li>
	</ul>
</digi-navigation-vertical-menu>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-vertical-menu
[attr.af-variation]="NavigationVerticalMenuVariation.${Object.keys(NavigationVerticalMenuVariation).find((key) =>NavigationVerticalMenuVariation[key] ===this.navigationVerticalMenuVariation)}"
>
	<ul>
		<li>
			<digi-navigation-vertical-menu-item
				[attr.af-text]="'Menyval'"
				[attr.af-active]="${this.isActive}"
				[attr.af-active-subnav]="false"\
				${this.isActive ? `\n\t\t\t\t[attr.af-aria-current]="'page'"\t ` : `\ `}
			>\
		${this.hasIcon ? `\n\t\t\t<digi-icon-home slot="icon" />` : ""}
			</digi-navigation-vertical-menu-item>
		</li>
	</ul>
</digi-navigation-vertical-menu>`,
			[CodeExampleLanguage.REACT]: `\
<DigiNavigationVerticalMenu
afVariation={NavigationVerticalMenuVariation.${Object.keys(NavigationVerticalMenuVariation).find((key) => NavigationVerticalMenuVariation[key] === this.navigationVerticalMenuVariation)}}
>
	<ul>
		<li>
			<DigiNavigationVerticalMenuItem
				afText="Menyval"
				afActive={${this.isActive}}
				afActiveSubnav={false}\
				${this.isActive ? `\n\t\t\t\tafAriaCurrent="page"\t ` : `\ `}
			>\
		${this.hasIcon ? `\n\t\t\t<DigiIconHome slot="icon" />` : ""}
			</DigiNavigationVerticalMenuItem>
		</li>
	</ul>
</DigiNavigationVerticalMenu>`
		};
	}

	setActiveMenuItem(target) {
		const parent = target.closest('digi-navigation-vertical-menu');
		const menuItems = parent.querySelectorAll(
			'digi-navigation-vertical-menu-item[af-active="true"]'
		);
		if (menuItems) {
			menuItems.forEach((el) => el.setAttribute('af-active', 'false'));
		}
		if (target) {
			target.setAttribute('af-active', 'true');
		}
	}

	@Listen('afOnClick')
	clickHandler(e) {
		if (
			e.target.matches(
				'.digi-navigation-vertical-menu-item-details digi-navigation-vertical-menu-item:not([af-active-subnav])'
			)
		) {
			e.detail.preventDefault();
			this.setActiveMenuItem(e.target);
		}
	}

	render() {
		return (
			<div class="digi-navigation-vertical-menu-item-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används tillsammans med{' '}
              <a href="/komponenter/digi-navigation-vertical-menu/oversikt">
                <digi-code af-code="<digi-navigation-vertical-menu>"></digi-code>{' '}
              </a>
              för att skapa vertikala menyer, t.ex. rullgardinsmenyer.
            </digi-typography-preamble>
					)}

					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example
								af-code={JSON.stringify(this.navigationVericalMenuItemCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
							>
								<digi-navigation-vertical-menu
									af-variation={this.navigationVerticalMenuVariation}
								>
									<ul>
										<li>
											<digi-navigation-vertical-menu-item
												afText={this.hasIcon ? 'Menyval' : 'Menyval '}
												afActive={this.isActive}
												afAriaCurrent={this.isActive ? 'page' : ''}
												
											// af-active-subnav="false"
											>
												{this.hasIcon && <digi-icon-home slot="icon" />}
											</digi-navigation-vertical-menu-item>
										</li>
									</ul>
								</digi-navigation-vertical-menu>

								<div class="slot__controls" slot="controls">
									<digi-form-fieldset
										afName="Variation"
										afLegend="Variation"
										onChange={(e) =>
											(this.navigationVerticalMenuVariation = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Primär"
											afValue={NavigationVerticalMenuVariation.PRIMARY}
											afChecked={
												this.navigationVerticalMenuVariation ===
												NavigationVerticalMenuVariation.PRIMARY
											}
										/>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Sekundär"
											afValue={NavigationVerticalMenuVariation.SECONDARY}
											afChecked={
												this.navigationVerticalMenuVariation ===
												NavigationVerticalMenuVariation.SECONDARY
											}
										/>
									</digi-form-fieldset>
									<digi-form-fieldset>
										<digi-form-checkbox
											afLabel="Aktivt menyval"
											afChecked={this.isActive}
											onAfOnChange={() =>
												this.isActive ? (this.isActive = false) : (this.isActive = true)
											}
										/>
									</digi-form-fieldset>
									<digi-form-fieldset>
										<digi-form-checkbox
											afLabel="Länk med ikon"
											afChecked={this.hasIcon}
											onAfOnChange={() =>
												this.hasIcon ? (this.hasIcon = false) : (this.hasIcon = true)
											}
										/>
									</digi-form-fieldset>
								</div>
							</digi-code-example>
						</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-margin-bottom>
							<h2>Beskrivning</h2>
							<p>
								Komponenten måste användas tillsammans med{' '}
								<a href="/komponenter/digi-navigation-vertical-menu/oversikt">
									<digi-code af-code="<digi-navigation-vertical-menu>"></digi-code>
								</a>
								. Attributet <digi-code af-code="af-text" /> används för att ange
								menyvalets namn.
							</p>
							<h3>Varianter</h3>
							<p>
								Komponenten finns i olika varianter: menyval med länk, undermeny med
								knapp, länk med ikon och förvalt menyval.
							</p>

							<h4>Aktivt menyval</h4>
							<p>
								Attributet <digi-code af-code="af-active" /> används för att markera ett
								menyval som aktivt.
							</p>
							<h4>Undermeny med knapp</h4>
							<p>
								Om man nästlar menyns ul-taggar så kommer en chevronknapp dyka upp som
								kan användas för att fälla ut nästa nivå. Man behöver även lägga med
								attributet <digi-code af-code="af-active-subnav" /> {' '}
								för att ange om den är stängd eller öppen.
							</p>
							<h4>Menyval med länk</h4>
							<p>
								Attributet <digi-code af-code="af-href" /> används för att ange en
								länkdestination.
							</p>
							<h4>Länk med ikon</h4>
							<p>
								Om en valfri ikonkomponent läggs in efter menykomponenten med attributet{' '}
								<digi-code af-code='slot="icon"' />{' '}
								så kommer menyvalsnamnet inledas med vald ikon.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
