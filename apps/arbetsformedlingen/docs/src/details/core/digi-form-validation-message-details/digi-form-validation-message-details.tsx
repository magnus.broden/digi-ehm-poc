import { Component, Prop, h, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormValidationMessageVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-form-validation-message-details',
	styleUrl: 'digi-form-validation-message-details.scss'
})
export class DigiFormValidationMessageDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formValidationMessageVariation: FormValidationMessageVariation = FormValidationMessageVariation.SUCCESS;
	@State() hasDescription: boolean = false;
	get textareaCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-validation-message
	af-variation="${this.formValidationMessageVariation}"
>
</digi-form-validation-message>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-validation-message
	[attr.af-variation]="FormValidationMessageVariation.${Object.keys(FormValidationMessageVariation).find((key) => FormValidationMessageVariation[key] === this.formValidationMessageVariation)}"
>
</digi-form-validation-message>`,
			[CodeExampleLanguage.REACT]: `\
<DigiFormValidationMessage
	afVariation={FormValidationMessageVariation.${Object.keys(FormValidationMessageVariation).find((key) => FormValidationMessageVariation[key] === this.formValidationMessageVariation)}}
>
</DigiFormValidationMessage>`
		};
	}
	render() {
		return (
			<div class="digi-form-validation-message-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Valideringsmeddelandekomponenten används för att visa
              valideringsmeddelanden.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.textareaCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<div style={{ width: '400px' }}>
								<digi-form-validation-message
									af-variation={this.formValidationMessageVariation}
								>
									Jag är ett valideringsmeddelande
								</digi-form-validation-message>
							</div>
							<div class="slot__controls" slot="controls">
								<digi-form-fieldset
									afName="Validering"
									afLegend="Valideringstyp"
									onChange={(e) =>
									(this.formValidationMessageVariation = (
										e.target as any
									).value)
									}
								>
									<digi-form-radiobutton
										afName="Validering"
										afLabel="Godkänd"
										afValue={FormValidationMessageVariation.SUCCESS}
										afChecked={
											this.formValidationMessageVariation ===
											FormValidationMessageVariation.SUCCESS
										}
									/>
									<digi-form-radiobutton
										afName="Validering"
										afLabel="Felaktig"
										afValue={FormValidationMessageVariation.ERROR}
										afChecked={
											this.formValidationMessageVariation ===
											FormValidationMessageVariation.ERROR
										}
									/>
									<digi-form-radiobutton
										afName="Validering"
										afLabel="Varning"
										afValue={FormValidationMessageVariation.WARNING}
										afChecked={
											this.formValidationMessageVariation ===
											FormValidationMessageVariation.WARNING
										}
									/>
								</digi-form-fieldset>
							</div>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-botton>
							<h2>Beskrivning</h2>
							<p>
								Valideringsmeddelandekomponenten använder en slot för att visa
								meddelandetexten och förväntar sig en textnod eller liknande
								element. Komponenten används i många formulärkomponenter men går
								även att använda fristående. Komponenten förser inte sina egna
								tillgänglighetsfunktioner för levande regiontyp, dessa förväntas
								att förses av applikationen som använder komponenten. Använd
								till exempel {' '}<digi-code af-code="aria-atomic" /> {' '}och{' '}
								<digi-code af-code="role='alert'" /> {' '} på dess överordnade
								element.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
