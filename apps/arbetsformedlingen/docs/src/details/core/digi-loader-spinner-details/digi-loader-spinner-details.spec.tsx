import { newSpecPage } from '@stencil/core/testing';
import { DigiLoaderSpinner } from './digi-loader-spinner-details';

describe('digi-loader-spinner-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiLoaderSpinner],
			html: '<digi-loader-spinner-details></digi-loader-spinner-details>'
		});
		expect(root).toEqualHtml(`
      <digi-loader-spinner-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-loader-spinner-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiLoaderSpinner],
			html: `<digi-loader-spinner-details first="Stencil" last="'Don't call me a framework' JS"></digi-loader-spinner-details>`
		});
		expect(root).toEqualHtml(`
      <digi-loader-spinner-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-loader-spinner-details>
    `);
	});
});
