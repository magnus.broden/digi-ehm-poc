import { newSpecPage } from '@stencil/core/testing';
import { DigiChartLine } from './digi-chart-line-details';

describe('digi-chart-line-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiChartLine],
			html: '<digi-chart-line-details></digi-chart-line-details>'
		});
		expect(root).toEqualHtml(`
      <digi-chart-line-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-chart-line-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiChartLine],
			html: `<digi-chart-line-details first="Stencil" last="'Don't call me a framework' JS"></digi-chart-line-details>`
		});
		expect(root).toEqualHtml(`
      <digi-chart-line-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-chart-line-details>
    `);
	});
});
