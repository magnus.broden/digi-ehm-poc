import { Component, Prop, h, State } from '@stencil/core';
import {
	CodeExampleLanguage
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-layout-container-details',
	styleUrl: 'digi-layout-container-details.scss'
})
export class DigiLayoutContainerDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasPadding: boolean = false;
	@State() hasGutter: boolean = true;

	get layoutContainerCode() {
		return {
[CodeExampleLanguage.HTML]: `\
<digi-layout-container${this.hasPadding ? ' af-vertical-padding' : ' '}${!this.hasGutter ? ' af-no-gutter' : ''}>
	<h2>Rubrik</h2>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada
		nunc sed purus mollis scelerisque. Curabitur bibendum leo
		quis ante porttitor tincidunt. 
		nibh.
	</p>
</digi-layout-container>`,
[CodeExampleLanguage.ANGULAR]: `\
<digi-layout-container${this.hasPadding ? ' [attr.af-vertical-padding]' : ' '}${!this.hasGutter ? ' [attr.af-no-gutter]' : ''}>
	<h2>Rubrik</h2>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada
		nunc sed purus mollis scelerisque. Curabitur bibendum leo
		quis ante porttitor tincidunt. 
		nibh.
	</p>
</digi-layout-container>`,
[CodeExampleLanguage.REACT]: `\
<DigiLayoutContainer${this.hasPadding ? ' afVerticalPadding' : ' '}${!this.hasGutter ? ' afNoGutter' : ''}>
	<h2>Rubrik</h2>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Etiam magna neque, interdum vel massa eget, condimentum
		rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada
		nunc sed purus mollis scelerisque. Curabitur bibendum leo
		quis ante porttitor tincidunt. 
		nibh.
	</p>
</DigiLayoutContainer>`
		}
	};


	render() {
		return (
			<div class="digi-layout-container-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Container-komponenten fungerar som en Bootstraps{' '}
              <digi-code af-code=".container"></digi-code>-klass.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							{!this.afShowOnlyExample && (
								<p>
									Bakgrunden på <digi-code af-code="digi-layout-container" /> är
									transparent, den grå bakgrundsfärgen är endast för att göra
									live-exemplet tydligare.
								</p>
							)}
							<digi-code-example af-code={JSON.stringify(this.layoutContainerCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
								<div class="slot__controls" slot="controls">
									<digi-form-fieldset style={{ marginTop: '10px' }}>
										<digi-form-checkbox
											afLabel="Vertikal padding"
											afChecked={this.hasPadding}
											onAfOnChange={() =>
												this.hasPadding
													? (this.hasPadding = false)
													: (this.hasPadding = true)
											}
										></digi-form-checkbox>
									</digi-form-fieldset>
									<digi-form-fieldset>
										<digi-form-checkbox
											afLabel="Ingen gutter"
											afChecked={!this.hasGutter}
											onAfOnChange={() =>
												this.hasGutter ? (this.hasGutter = false) : (this.hasGutter = true)
											}
										></digi-form-checkbox>
									</digi-form-fieldset>
								</div>
								<div style={{ background: '#F1F1F1' }}>
									{this.hasPadding && this.hasGutter && (
										<digi-layout-container afVerticalPadding>
											<h2>Rubrik</h2>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
												magna neque, interdum vel massa eget, condimentum rutrum velit. Sed
												vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis
												scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt.
												nibh.
											</p>
										</digi-layout-container>
									)}
									{this.hasPadding && !this.hasGutter && (
										<digi-layout-container afVerticalPadding afNoGutter>
											<h2>Rubrik</h2>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
												magna neque, interdum vel massa eget, condimentum rutrum velit. Sed
												vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis
												scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt.
												nibh.
											</p>
										</digi-layout-container>
									)}
									{!this.hasPadding && !this.hasGutter && (
										<digi-layout-container afNoGutter>
											<h2>Rubrik</h2>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
												magna neque, interdum vel massa eget, condimentum rutrum velit. Sed
												vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis
												scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt.
												nibh.
											</p>
										</digi-layout-container>
									)}
									{!this.hasPadding && this.hasGutter && (
										<digi-layout-container>
											<h2>Rubrik</h2>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
												magna neque, interdum vel massa eget, condimentum rutrum velit. Sed
												vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis
												scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt.
												nibh.
											</p>
										</digi-layout-container>
									)}
								</div>
							</digi-code-example>
						</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h2>Beskrivning</h2>
							<h3>Varianter</h3>
							<p>
								Containerkomponenten finns i varianterna static och fluid som går att
								ställa in med <digi-code af-code="af-variation" />. Typescript-användare
								bör importera och använda{' '}
								<digi-code af-code="LayoutContainerVariation" />.
							</p>
							<h4>Static</h4>
							<p>
								En statisk container kommer att få fasta bredder i olika skärmstorlekar.
								Detta är standard för komponenten.
							</p>
							<h4>Fluid</h4>
							<p>
								Den här variationen av container-komponenten har ingen max-bredd så den
								kommer alltid att vara 100% bred.
							</p>
							<h3>Paddings och margins</h3>
							<p>
								{' '}
								Det går att lägga till paddings och margins med olika attribut.{' '}
								<digi-code af-code="af-margin-bottom" /> lägger till extra margin i
								botten och <digi-code af-code="af-vertical-padding" /> lägger till
								padding på båda sidorna. <digi-code af-code="af-no-gutter" /> tar bort
								marginalen på sidorna av containern.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
