import { Component, Prop, h, State } from '@stencil/core';
import { CodeExampleLanguage, LayoutBlockVariation } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-layout-block-details',
	styleUrl: 'digi-layout-block-details.scss'
})
export class DigiLayoutBlockDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() layoutBlockVariation: LayoutBlockVariation =
		LayoutBlockVariation.PRIMARY;

	get layoutBlockCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-layout-block af-variation="${this.layoutBlockVariation}">
<digi-typography>
<h2>Det här är en primär blockvariant</h2>
<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna neque, interdum vel massa eget, 
	condimentum rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque. 
	Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est.
</p>
</digi-typography>
</digi-layout-block>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-layout-block [attr.af-variation]="LayoutBlockVariation.${Object.keys(LayoutBlockVariation).find((key) => LayoutBlockVariation[key] === this.layoutBlockVariation)}">
<digi-typography>
<h2>Det här är en primär blockvariant</h2>
<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna neque, interdum vel massa eget, 
	condimentum rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque. 
	Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est.
</p>
</digi-typography>
</digi-layout-block>`,
[CodeExampleLanguage.REACT]: `\
<DigiLayoutBlock afVariation={LayoutBlockVariation.${Object.keys(LayoutBlockVariation).find((key) => LayoutBlockVariation[key] === this.layoutBlockVariation)}}>
<DigiTypography>
<h2>Det här är en primär blockvariant</h2>
<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna neque, interdum vel massa eget, 
	condimentum rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque. 
	Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est.
</p>
</DigiTypography>
</DigiLayoutBlock>`
		};
	}
	render() {
		return (
			<div class="digi-layout-block-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Det här är en layoutkomponent som är avsedd för att skapa block i full
              bredd med en inre container. Den kan användas som ett grundläggande
              byggblock för sidor.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example af-code={JSON.stringify(this.layoutBlockCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
								<div class="slot__controls" slot="controls">
									<digi-form-select
										afLabel="Variant"
										onAfOnChange={(e) =>
											(this.layoutBlockVariation = (e.target as any).value)
										}
										af-variation="small"
										af-start-selected="0"
									>
										<option value="primary">Primär</option>
										<option value="secondary">Sekundär</option>
										<option value="profile">Profil</option>
										<option value="symbol">Symbol</option>
										<option value="tertiary">Tertiär</option>
										<option value="transparent">Transparant</option>
									</digi-form-select>
								</div>
								<digi-layout-block
									afMarginBottom
									afMarginTop
									afVerticalPadding
									af-variation={this.layoutBlockVariation}
								>
									<digi-typography>
										<div
											style={
												this.layoutBlockVariation == 'profile' ||
													this.layoutBlockVariation == 'tertiary'
													? { color: 'white' }
													: {}
											}
										>
											<h2>Det här är en primär blockvariant</h2>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
												magna neque, interdum vel massa eget, condimentum rutrum velit. Sed
												vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis
												scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt.
												Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est.
											</p>
										</div>
									</digi-typography>
								</digi-layout-block>
							</digi-code-example>
						</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h2>Beskrivning</h2>

							<h3>Varianter</h3>
							<p>
								Blockkomponenten finns i olika varianter som styr blockets bakgrundsfärg: primär, sekundär, tertiär,
								transparent symbol och profil. Det ställs in genom att använda{' '}
								<digi-code af-code="af-variation" />.
							</p>

							<h3>Containervarianter</h3>
							<p>
								Blockkomponenten använder <digi-code af-code=" digi-layout-container" />{' '}
								och som finns i tre olika varianter, static, fluid och none. Static är
								standard, fluid lägger till gutters på sidorna och none tar bort den
								inre containern helt. För att ställa in det används{' '}
								<digi-code af-code="af-container" />.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
