import { newSpecPage } from '@stencil/core/testing';
import { DigiListDetails } from './digi-list-details';

describe('digi-list-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiListDetails],
			html: '<digi-list-details></digi-list-details>'
		});
		expect(root).toEqualHtml(`
      <digi-list-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-list-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiListDetails],
			html: `<digi-list-details first="Stencil" last="'Don't call me a framework' JS"></digi-list-details>`
		});
		expect(root).toEqualHtml(`
      <digi-list-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-list-details>
    `);
	});
});
