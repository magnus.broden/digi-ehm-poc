import { Component, h, Host, Prop } from '@stencil/core';
import {
	CodeExampleLanguage,
	QuoteSingleVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-quote-multi-container-details',
	styleUrl: 'digi-quote-multi-container-details.scss'
})
export class DigiQuoteMultiContainer {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get quoteMultiContainerCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-quote-multi-container
af-heading="En Rubrik"
af-heading-level="h2"
>
	<digi-quote-single
	af-variation="primary"
	af-quote-author-name="Namn"
	af-quote-author-title="Roll"	
	>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</digi-quote-single>
	<digi-quote-single>
		...
	</digi-quote-single>
	<digi-quote-single>
		...
	</digi-quote-single>
</digi-quote-mult-container>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-quote-multi-container
af-heading="En Rubrik"
af-heading-level="QuoteMultiContainerHeadingLevel.H2"
>
	<digi-quote-single
	[attr.af-variation]="QuoteSingleVariation.PRIMARY"
	[attr.af-quote-author-name]="Namn"
	[attr.af-quote-author-title]="Roll"	
	>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</digi-quote-single>
	<digi-quote-single>
		...
	</digi-quote-single>
	<digi-quote-single>
		...
	</digi-quote-single>
</digi-quote-mult-container>`,
			[CodeExampleLanguage.REACT]: `\
<digi-quote-multi-container
afHeading="En Rubrik"
afHeadingLevel={QuoteMultiContainerHeadingLevel.H2}
>
	<digi-quote-single
	afVariation={QuoteSingleVariation.PRIMARY}
	afQuoteAuthorName="Namn"
	afQuoteAuthorTitle="Roll"	
	>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</digi-quote-single>
	<digi-quote-single>
		...
	</digi-quote-single>
	<digi-quote-single>
		...
	</digi-quote-single>
</digi-quote-mult-container>`
		};
	}

	render() {
		return (
			<Host>
				<div class="digi-quote-multi-container-details">
					<digi-typography>
						{!this.afShowOnlyExample && (
							<digi-typography-preamble>
								Citatbehållare används av citatkomponenten för att visa upp flera citat bredvid varandra.
							</digi-typography-preamble>
						)}
						<digi-layout-container af-no-gutter af-margin-bottom>
							<article>
								{!this.afShowOnlyExample && <h2>Exempel</h2>}
								<digi-code-example
									af-code={JSON.stringify(this.quoteMultiContainerCode)}
									af-hide-controls={this.afHideControls ? 'true' : 'false'}
									af-hide-code={this.afHideCode ? 'true' : 'false'}
								>
									<digi-quote-multi-container
									af-heading="En Rubrik"
									af-heading-level="h2"
									>
										<digi-quote-single
											afVariation={QuoteSingleVariation.PRIMARY}
											afQuoteAuthorName="Namn"
											afQuoteAuthorTitle="Roll"
										>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
											eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
											ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
											aliquip ex ea commodo consequat. Duis aute irure dolor in
											reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
											pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
											culpa qui officia deserunt mollit anim id est laborum.
										</digi-quote-single>
										<digi-quote-single
											afVariation={QuoteSingleVariation.PRIMARY}
											afQuoteAuthorName="Namn"
											afQuoteAuthorTitle="Roll"
										>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
											eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
											ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
											aliquip ex ea commodo consequat. Duis aute irure dolor in
											reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
											pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
											culpa qui officia deserunt mollit anim id est laborum.
										</digi-quote-single>
										<digi-quote-single
											afVariation={QuoteSingleVariation.PRIMARY}
											afQuoteAuthorName="Namn"
											afQuoteAuthorTitle="Roll"
										>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
											eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
											ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
											aliquip ex ea commodo consequat. Duis aute irure dolor in
											reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
											pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
											culpa qui officia deserunt mollit anim id est laborum.
										</digi-quote-single>
									</digi-quote-multi-container>
								</digi-code-example>
							</article>
						</digi-layout-container>
					</digi-typography>
				</div>
			</Host>
		);
	}
}
