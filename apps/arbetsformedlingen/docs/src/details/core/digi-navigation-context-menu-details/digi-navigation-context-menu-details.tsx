import { Component, Prop, h } from '@stencil/core';
import {
	NavigationContextMenuItemType,
	CodeExampleLanguage,
	InfoCardHeadingLevel
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-navigation-context-menu-details',
	styleUrl: 'digi-navigation-context-menu-details.scss'
})
export class DigiNavigationContextMenuDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get navigationContextMenuItemTypeCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-context-menu
	af-text="Rullgardinsmeny"
	af-start-selected="0"
>
	<digi-navigation-context-menu-item
		af-text="Menyval 1"
		af-type="button"
	>
	</digi-navigation-context-menu-item>
	<digi-navigation-context-menu-item
		af-text="Menyval 2"
		af-type="button"
	>
	</digi-navigation-context-menu-item>
</digi-navigation-context-menu>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-context-menu
	[attr.af-text]="'Rullgardinsmeny'"
	[attr.af-start]-selected="0"
>
	<digi-navigation-context-menu-item
		[attr.af-text]="'Menyval 1'"
		[attr.af-type]="NavigationContextMenuItemType.BUTTON"
	>
	</digi-navigation-context-menu-item>
	<digi-navigation-context-menu-item
		[attr.af-text]="'Menyval 2'"
		[attr.af-type]="NavigationContextMenuItemType.BUTTON"
	>
	</digi-navigation-context-menu-item>
</digi-navigation-context-menu>`,
			[CodeExampleLanguage.REACT]: `\
<DigiNavigationContextMenu
	afText="Rullgardinsmeny"
	afStartSelected={0}
>
	<DigiNavigationContextMenuItem
		afText="Menyval 1"
		afType={NavigationContextMenuItemType.BUTTON}
	>
	</DigiNavigationContextMenuItem>
	<DigiNavigationContextMenuItem
		afText="Menyval 2"
		afType={NavigationContextMenuItemType.BUTTON}
	>
	</DigiNavigationContextMenuItem>
</DigiNavigationContextMenu>`
		};
	}
	render() {
		return (
			<div class="digi-navigation-context-menu-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Rullgardinsmenyn använder sig av komponenten{' '}
              <a href="/komponenter/digi-navigation-context-menu-item/oversikt"><digi-code af-code="<digi-navigation-context-menu-item>" /></a> för att skapa
              navigering med knappar eller länkar som menyval.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example
							af-code={JSON.stringify(this.navigationContextMenuItemTypeCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
						>
							<div style={{ height: '120px' }}>
								<digi-navigation-context-menu
									afText="Rullgardinsmeny"
									af-start-selected="0"
								>
									<digi-navigation-context-menu-item
										afText="Menyval 1"
										afType={NavigationContextMenuItemType.BUTTON}
									></digi-navigation-context-menu-item>
									<digi-navigation-context-menu-item
										afText="Menyval 1"
										afType={NavigationContextMenuItemType.BUTTON}
									></digi-navigation-context-menu-item>
								</digi-navigation-context-menu>
							</div>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-margin-bottom afNoGutter>
							<h2>Beskrivning</h2>
							<p>
								Visningsnamnet på rullgardinsmenyn anges via{' '}
								<digi-code af-code="'Rullgardinsmeny'" />. Ange vilket val som ska vara
								förvalt genom <digi-code af-code="af-start-selected" />. Värdet anges
								som ett nummer, om inget värde anges är det första elementet (0) i
								listan förvalt.
							</p>

							<h3>Varianter</h3>
							<p>
								Rullgardinsmenyn finns enbart i en variant. Den använder dock{' '}
								<digi-code af-code="digi-navigation-context-menu-item" /> för elementen
								som finns i två varianter, länkar och knappar. Vid användning av typen
								"knapp" emittas ett event med valt värde vid klick och rullgardinsmenyn
								stänger sig. Vid användning av knappar behövs ej attributet{' '}
								<digi-code af-code="af-href" />.
							</p>

							<digi-info-card
								afHeadingLevel={InfoCardHeadingLevel.H3}
								afHeading="Riktlinjer"
							>
								<digi-list>
									<li>
										Rullgardinsmeny ska alltid ha en etikett intill sig där det framgår
										vad användaren ska göra sitt val utifrån.
									</li>
									<li>
										Rullgardinsmenyn ska i utgångsläget innehålla en text, där det tydligt
										framgår vad man väljer (till exempel "Välj yrke", inte endast "Välj").
									</li>
									<li>
										Hela fältet för rullgardinsmeny är klickyta som fäller ut/fäller ihop
										menyn.
									</li>
									<li>
										De valbara alternativen i en rullgardinsmeny i formulär får inte vara
										länkar.
									</li>
								</digi-list>
							</digi-info-card>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
