import { Component, Prop, h, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormCheckboxVariation,
	InfoCardHeadingLevel,
	LinkVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-link-details',
	styleUrl: 'digi-link-details.scss'
})
export class DigiLinkDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() icon: boolean = false;
	@State() linkVariation: LinkVariation = LinkVariation.SMALL;

  @State() showExample: boolean = true;

  iconChangeHandler() {
    this.icon ? (this.icon = false) : (this.icon = true)
    this.showExample = false;
		setTimeout(() => (this.showExample = true), 0);
  }

	get linkCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-link
	af-href="#"
	af-target="_blank"
	af-variation="${Object.keys(LinkVariation).find((key) => LinkVariation[key] === this.linkVariation).toLocaleLowerCase()}"
>\
	${this.icon ? '\n\t<digi-icon-file-pdf slot="icon"></digi-icon-file-pdf>' : '\ '}
	Jag är en länk
</digi-link>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-link
	[attr.af-link-container]="true"
	[attr.af-variation]="LinkVariation.${Object.keys(LinkVariation).find((key) => LinkVariation[key] === this.linkVariation)}"
>
  <a href="/" [routerLink]="/">${this.icon ? '\n\t<digi-icon-file-pdf slot="icon"></digi-icon-file-pdf>' : '\ '}
    Jag är en länk
  </a>
</digi-link>`,
			[CodeExampleLanguage.REACT]: `\
<DigiLink
	afHref="/"
	afVariation={LinkVariation.${Object.keys(LinkVariation).find((key) => LinkVariation[key] === this.linkVariation)}}
>\
	${this.icon ? '\n\t<digi-icon-file-pdf slot="icon"></digi-icon-file-pdf>' : '\ '}
	Jag är en länk
</DigiLink>`,
		};
	}
	render() {
		return (
			<div class="digi-link-details">
				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Länkens syfte är att förflytta användaren ifrån aktuell sida till en
              annan sida. En länk kan även användas för att förflytta användaren till
              en annan plats på samma sida. Länkar skall aldrig användas för att
              genomföra en åtgärd. Vid dessa situationer skall istället en knapp
              användas.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && <h2>Exempel</h2>}
						<digi-code-example
							af-code={JSON.stringify(this.linkCode)}
							// afCodeBlockVariation={CodeExampleVariation.LIGHT}
							af-hide-controls={this.afHideControls ? 'true' : 'false'}
							af-hide-code={this.afHideCode ? 'true' : 'false'}
						>
							<div class="slot__controls" slot="controls">
								<digi-form-fieldset
									afName="Storlek"
									afLegend="Storlek"
									onChange={(e) => (this.linkVariation = (e.target as any).value)}
								>
									<digi-form-radiobutton
										afName="Storlek"
										afLabel="Liten"
										afValue={LinkVariation.SMALL}
										afChecked={this.linkVariation === LinkVariation.SMALL}
									/>
									<digi-form-radiobutton
										afName="Storlek"
										afLabel="Stor"
										afValue={LinkVariation.LARGE}
										afChecked={this.linkVariation === LinkVariation.LARGE}
									/>
								</digi-form-fieldset>
								<digi-form-fieldset>
									<digi-form-checkbox
										afLabel="Icon"
										afVariation={FormCheckboxVariation.PRIMARY}
										afChecked={this.icon}
										onAfOnChange={() => this.iconChangeHandler()}
									></digi-form-checkbox>
								</digi-form-fieldset>
							</div>
							{this.showExample && (
                !this.icon ? (
                  <digi-link
                    afHref="#"
                    af-target="_blank"
                    af-variation={this.linkVariation}
                  >
                    Jag är en länk
                  </digi-link>
                )
                : (
                  <digi-link
                    afHref="#"
                    af-target="_blank"
                    af-variation={this.linkVariation}
                  >
                    <digi-icon-file-pdf slot="icon"></digi-icon-file-pdf>
                    Jag är en länk
                  </digi-link>
                )
              )}
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container afNoGutter afMarginBottom>
							<digi-notification-alert
								af-size="medium"
								af-variation="info"
							>
								Uppdaterat exempel för hur man ska använda länk-komponenter i Angular från version 19.0.0. Läs mer nedan under riktlinjer och tillgänglighetsproblem
              </digi-notification-alert>
							<h2>Beskrivning</h2>
							<h3>Varianter</h3>
							<p>
								Länk-komponenten finns i två storlekar, varav den lilla är standard.
								Använd <digi-code af-code="af-variation"></digi-code> för att välja
								vilken du vill ha, t.ex.{' '}
								<digi-code af-code="af-variation='large'"></digi-code>.
							</p>
							<h3>Med ikon</h3>
							<p>
								Du kan använda digi-link med en ikon genom att använda{' '}
								<digi-code af-code="slot='icon'"></digi-code> i ikon-taggen.
							</p>
							<digi-info-card
								afHeading="Riktlinjer och tillgänglighet"
								afHeadingLevel={InfoCardHeadingLevel.H3}
							>
								<div class="digi-typography digi-typography--s">
									<digi-list>
										<li>
											En länktext ska vara tydlig så att användaren förstår vart en länk
											leder även om länken lyfts ur sitt sammanhang. Undvik länkar som
											endast heter "Tillbaka" eller "Läs mer".
										</li>
										<li>Skriv länktexten så kort som möjligt.</li>
										<li>
											Undvik fraser som "Gå till...", "Till sidan..." i länken, då det är
											självklart att länken leder till en annan del av webbplatsen.
										</li>
										<li>
											Navigering på webbplatsen och inom tjänsterna sker primärt via
											brödsmulor. Tillbakalänkar används endast i undantagsfall.
										</li>
										<li>
											Alla ikoner som utgör en länk och som definieras inom &lt;i&gt; skall
											ha attributet <digi-code af-code='aria-hidden="true"'></digi-code>.
										</li>
										<li>
											Behöver du förtydliga länkens syfte eller mål så kan attributet{' '}
											<digi-code af-code="aria-label"></digi-code> användas. Det är svårt
											att säga när attributet behövs, men om flera länkar i t.ex. ett
											sökresultat är identiska så kan aria-label användas.
											<br />
											<br />
											Kom ihåg att attributet aria-label:
											<br />
											- endast används av skärmläsare och andra liknande verktyg.
											<br />- skriver över länktexten för användare av dessa verktyg.{' '}
										</li>
                    <li>
                      För att kunna använda Angulars <digi-code af-code="routerLink"></digi-code> med vår komponent behöver man använd attributet <digi-code af-code="af-container-link"></digi-code> och istället använda komponenten som en hållare av ett vanligt länkelement.<br /><br />
                      Använder man Angular RouterLink direkt på vårt element så får man tillgänglighetsproblem då Angular lägger till tabindex i koden som gör att man får fokus på fel element när man navigerar med tangentbord.
                      Använder man @digi/arbetsformedlingen-angular i tidigare version än 19.0.0 så rekommenderar vi att använda <digi-code af-code="af-override-link"></digi-code> och istället sköta routing programmatisk.
                    </li>
									</digi-list>
								</div>
							</digi-info-card>
						</digi-layout-container>
					)}
				</digi-typography>
			</div>
		);
	}
}
