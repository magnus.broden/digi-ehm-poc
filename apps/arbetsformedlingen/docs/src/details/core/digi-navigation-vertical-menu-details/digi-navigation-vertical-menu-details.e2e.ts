import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-vertical-menu-details', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-navigation-vertical-menu-details></digi-navigation-vertical-menu-details>'
    );

    const element = await page.find('digi-navigation-vertical-menu-details');
    expect(element).toHaveClass('hydrated');
  });

  it('displays the specified name', async () => {
    const page = await newE2EPage({ url: '/profile/joseph' });

    const profileElement = await page.find(
      'app-root >>> digi-navigation-vertical-menu-details'
    );
    const element = profileElement.shadowRoot.querySelector('div');
    expect(element.textContent).toContain('Hello! My name is Joseph.');
  });

  // it('includes a div with the class "digi-navigation-vertical-menu-details"', async () => {
  //   const page = await newE2EPage({ url: '/profile/joseph' });

  // I would like to use a selector like this above, but it does not seem to work
  //   const element = await page.find('app-root >>> digi-navigation-vertical-menu-details >>> div');
  //   expect(element).toHaveClass('digi-navigation-vertical-menu-details');
  // });
});
