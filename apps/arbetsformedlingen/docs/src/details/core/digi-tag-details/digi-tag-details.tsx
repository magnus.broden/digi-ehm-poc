import { Component, Prop, h, State } from '@stencil/core';
import { TagSize, CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-tag-details',
	styleUrl: 'digi-tag-details.scss',
})
export class DigiTagDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() tagSize: TagSize = TagSize.SMALL;
	@State() closeable: boolean = true;
	@State() tagIcon: boolean = true;

	get tagCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-tag    
	af-text="Jag är en tagg"
	af-size="${Object.keys(TagSize).find(key => TagSize[key] === this.tagSize).toLocaleLowerCase()}"
	af-no-icon="${!this.closeable}"${this.closeable ? '\n	af-aria-label="ta bort"' : ""}>
</digi-tag>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-tag 
	[attr.af-text]="Jag är en tagg"
	[attr.af-size]="TagSize.${Object.keys(TagSize).find(key => TagSize[key] === this.tagSize)}"
	[attr.af-no-icon]="${!this.closeable}"${this.closeable ? '\n	[attr.af-aria-label]="ta bort"' : ""}>
</digi-tag>`,
			[CodeExampleLanguage.REACT]: `\
<DigiTag
	afText="Jag är en tagg"
	afSize={TagSize.${Object.keys(TagSize).find(key => TagSize[key] === this.tagSize)}}
	afNoIcon={${!this.closeable}}${this.closeable ? '\n	afAriaLabel="ta bort"' : ""}>
</DigiTag>`
		};
	}

	render() {
		return (
			<div class="digi-tag-details">
				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Taggar används för att visuellt representera nya eller kategoriserade innehållselement. Taggens text sätts via
              <digi-code af-code="af-text='Taggens text'"></digi-code>
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example
							af-code={JSON.stringify(this.tagCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
						>

							<div class="slot__controls" slot="controls">
								<digi-form-fieldset afName="Storlek" afLegend="Storlek" onChange={e => this.tagSize = (e.target as any).value}>
									<digi-form-radiobutton
										afName="Storlek"
										afLabel="Liten"
										afValue={TagSize.SMALL}
										afChecked={this.tagSize === TagSize.SMALL}
									/>
									<digi-form-radiobutton
										afName="Storlek"
										afLabel="Mellan"
										afValue={TagSize.MEDIUM}
										afChecked={this.tagSize === TagSize.MEDIUM}
									/>
									<digi-form-radiobutton
										afName="Storlek"
										afLabel="Stor"
										afValue={TagSize.LARGE}
										afChecked={this.tagSize === TagSize.LARGE}
									/>
								</digi-form-fieldset>
								<digi-form-fieldset afLegend="Stängningsbar">
									<digi-form-checkbox
										afLabel="Ja"
										afChecked={this.closeable}
										onAfOnChange={() => this.closeable ? this.closeable = false : this.closeable = true}
									>

									</digi-form-checkbox>
								</digi-form-fieldset>
							</div>
							<digi-tag
								afNoIcon={!this.closeable}
								afSize={this.tagSize}
								afText="Jag är en tagg"
							>
							</digi-tag>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container afNoGutter afMarginBottom>
							<h2>Beskrivning</h2>
							<h3>Storlek</h3>
							<p>
								Taggens storlek kan sättas via <digi-code
									af-code="af-size"></digi-code>. Taggarna finns i tre olika storlekar <digi-code
										af-code="af-size='large', af-size='medium'"></digi-code> och <digi-code af-code="af-size='small'"></digi-code>.
							</p>
							<h3>Ikon</h3>
							<p>
								Ikonen går att dölja genom att använda <digi-code
									af-code="af-noIcon='true'"></digi-code>.
							</p>
							<h3>Tillgänglighet</h3>
							<p>
								Använd <digi-code af-code="af-aria-label"></digi-code> när en tagg ska kunna stängas eller tas bort. Skärmläsare ska kunna återge taggens funktion.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>
			</div>
		);
	}
}
