import { Component, Prop, h, State, getAssetPath } from '@stencil/core';
import {
	CodeExampleLanguage,
	LayoutColumnsElement,
	LayoutColumnsVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-layout-columns-details',
	styleUrl: 'digi-layout-columns-details.scss'
})
export class DigiLayoutColumnsDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() layoutColumnsVariation: any = 'none';
	@State() layoutColumnsElement: LayoutColumnsElement = LayoutColumnsElement.DIV;

	get layoutColumnCode() {
		return {
[CodeExampleLanguage.HTML]: `\
<digi-layout-columns
	af-element="${this.layoutColumnsElement}"
${this.layoutColumnsVariation !== 'none'
				? `\taf-variation="${this.layoutColumnsVariation}"\n>`
				: '>'
			}
	<digi-media-image>
	</digi-media-image>
	...  
</digi-layout-columns>`,
[CodeExampleLanguage.ANGULAR]: `\
<digi-layout-columns
	[attr.af-element]="LayoutColumnsElement.${Object.keys(
			LayoutColumnsElement
		).find((key) => LayoutColumnsElement[key] === this.layoutColumnsElement)}"
${this.layoutColumnsVariation !== 'none'
				? `\t[attr.af-variation]="LayoutColumnsVariation.${Object.keys(
					LayoutColumnsVariation
				).find(
					(key) => LayoutColumnsVariation[key] === this.layoutColumnsVariation
				)}"\n>`
				: '>'
			}
	<digi-media-image>
	</digi-media-image>
	...  
</digi-layout-columns>`,
[CodeExampleLanguage.REACT]: `\
<DigiLayoutColumns
	afElement={LayoutColumnsElement.${Object.keys(LayoutColumnsElement).find((key) => LayoutColumnsElement[key] === this.layoutColumnsElement)}}
${this.layoutColumnsVariation !== 'none' ? `\tafVariation={LayoutColumnsVariation.${Object.keys(LayoutColumnsVariation).find((key) => LayoutColumnsVariation[key] === this.layoutColumnsVariation)}}\n>` : '>'}
	<DigiMediaImage>
	</DigiMediaImage>
	...  
</DigiLayoutColumns>`
		};
	}
	render() {
		return (
			<div class="digi-layout-columns-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Komponenten används för att lägga innehåll i kolumner.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example af-code={JSON.stringify(this.layoutColumnCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
								<div class="slot__controls" slot="controls">
									<digi-form-fieldset
										afLegend="Antal kolumner"
										afName="variant"
										onChange={(e) =>
											(this.layoutColumnsVariation = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="variant"
											afLabel="Ingen"
											afValue="none"
											afChecked={this.layoutColumnsVariation === 'none'}
										/>
                    <digi-form-radiobutton
											afName="variant"
											afLabel="En"
											afValue="one"
											afChecked={
												this.layoutColumnsVariation === LayoutColumnsVariation.ONE
											}
										/>
										<digi-form-radiobutton
											afName="variant"
											afLabel="Två"
											afValue="two"
											afChecked={
												this.layoutColumnsVariation === LayoutColumnsVariation.TWO
											}
										/>
										<digi-form-radiobutton
											afName="variant"
											afLabel="Tre"
											afValue="three"
											afChecked={this.layoutColumnsVariation === 'three'}
										/>
									</digi-form-fieldset>
									<digi-form-fieldset
										afLegend="Elementtyp"
										afName="element"
										onChange={(e) =>
											(this.layoutColumnsElement = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="element"
											afLabel="<div>"
											afValue={LayoutColumnsElement.DIV}
											afChecked={this.layoutColumnsElement === LayoutColumnsElement.DIV}
										/>
										<digi-form-radiobutton
											afName="element"
											afLabel="<ul>"
											afValue={LayoutColumnsElement.UL}
											afChecked={this.layoutColumnsElement === LayoutColumnsElement.UL}
										/>
										<digi-form-radiobutton
											afName="element"
											afLabel="<ol>"
											afValue={LayoutColumnsElement.OL}
											afChecked={this.layoutColumnsElement === LayoutColumnsElement.OL}
										/>
									</digi-form-fieldset>
								</div>
								{this.layoutColumnsElement === LayoutColumnsElement.DIV &&
									this.layoutColumnsVariation === 'none' && (
										<digi-layout-columns afElement={this.layoutColumnsElement}>
											<digi-media-image
												afUnlazy
												afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
												afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
											></digi-media-image>

											<digi-media-image
												afUnlazy
												afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
												afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
											></digi-media-image>

											{!this.afShowOnlyExample && (
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											)}

											{!this.afShowOnlyExample && (
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											)}

										</digi-layout-columns>
									)}
								{this.layoutColumnsElement === LayoutColumnsElement.DIV &&
									this.layoutColumnsVariation !== 'none' && (
										<digi-layout-columns
											afElement={this.layoutColumnsElement}
											afVariation={this.layoutColumnsVariation}
										>
											<digi-media-image
												afUnlazy
												afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
												afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
											></digi-media-image>
											<digi-media-image
												afUnlazy
												afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
												afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
											></digi-media-image>
											<digi-media-image
												afUnlazy
												afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
												afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
											></digi-media-image>
											<digi-media-image
												afUnlazy
												afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
												afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
											></digi-media-image>
										</digi-layout-columns>
									)}
								{this.layoutColumnsElement !== LayoutColumnsElement.DIV &&
									this.layoutColumnsVariation === 'none' && (
										<digi-layout-columns afElement={this.layoutColumnsElement}>
											<li>
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											</li>
											<li>
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											</li>
											<li>
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											</li>
											<li>
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											</li>
										</digi-layout-columns>
									)}
								{this.layoutColumnsElement !== LayoutColumnsElement.DIV &&
									this.layoutColumnsVariation !== 'none' && (
										<digi-layout-columns
											afElement={this.layoutColumnsElement}
											afVariation={this.layoutColumnsVariation}
										>
											<li>
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											</li>
											<li>
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											</li>
											<li>
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											</li>
											<li>
												<digi-media-image
													afUnlazy
													afSrc={getAssetPath('/assets/images/employment-service-sign.jpg')}
													afAlt="Fasadskylt med Arbetsförmedlingens logotyp."
												></digi-media-image>
											</li>
										</digi-layout-columns>
									)}
							</digi-code-example>
						</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h2>Beskrivning</h2>
							<h3>Bredd och gap</h3>
							<p>
								Som standard kommer den att placera alla underordnade element på samma
								rad, men genom att ställa in ett{' '}
								<digi-code af-code="--digi-layout-columns--min-width" />
								önskat värde kommer det att börja flöda till ny rad när underordnade
								element når sin minsta bredd. Kolumngapet kan anpassas med{' '}
								<digi-code af-code="--digi--layout-columns--gap--column" />. Det går
								även att anpassa radgapet med{' '}
								<digi-code af-code="--digi--layout-columns--gap--row" />.
							</p>
							<h3>Antal kolumner</h3>
							<p>
								Med hjälp av{' '}
								<digi-code af-code="af-variation" /> {' '}går det att ställa in om man vill ändra till att
								ha en, två eller tre kolumner.
							</p>
							<h3>Element</h3>
							<p>Det går att ändra elementtyp som ska wrappa de underordnade
								elementen med hjälp av {' '}<digi-code af-code="af-element" />. Som standard
								är det <digi-code af-code="div" /> men det går också att välja{' '}
								<digi-code af-code="ol" />{' '} och {' '}<digi-code af-code="ul" /> väljer man
								någon av listtaggarna så måste också underliggande element ligga i{' '}
								<digi-code af-code="li" />
								-taggar.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
