import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'digi-util-mutation-observer-details',
  styleUrl: 'digi-util-mutation-observer-details.scss'
})
export class DigiUtilMutationObserverDetails {
  @Prop() component: string;

  render() {
    return (
      <div class="digi-util-mutation-observer-details">
        <digi-typography>
          <digi-layout-block af-variation="primary">
            <digi-typography-preamble>
              Den här komponenten implementerar Mutation Observer API. Det är
              mycket användbart för att upptäcka ändringar i element inuti
              dokumentet (när noder läggs till eller tas bort, ändrar attribut etc).
              <br />
              <br />
              <digi-link af-variation="small" afHref="https://digi-core.netlify.app/?path=/docs/util-digi-util-mutation-observer--standard" af-target="_blank">
                <digi-icon-arrow-right slot="icon"></digi-icon-arrow-right>
                Länk till dokumentation i Storybook
              </digi-link>
            </digi-typography-preamble>
          </digi-layout-block>
        </digi-typography>
      </div>
    );
  }
}
