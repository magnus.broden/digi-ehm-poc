import { newE2EPage } from '@stencil/core/testing';

describe('digi-typography-meta-details', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-typography-meta-details></digi-typography-meta-details>');

    const element = await page.find('digi-typography-meta-details');
    expect(element).toHaveClass('hydrated');
  });

  it('displays the specified name', async () => {
    const page = await newE2EPage({ url: '/profile/joseph' });

    const profileElement = await page.find('app-root >>> digi-typography-meta-details');
    const element = profileElement.shadowRoot.querySelector('div');
    expect(element.textContent).toContain('Hello! My name is Joseph.');
  });

  // it('includes a div with the class "digi-typography-meta-details"', async () => {
  //   const page = await newE2EPage({ url: '/profile/joseph' });

  // I would like to use a selector like this above, but it does not seem to work
  //   const element = await page.find('app-root >>> digi-typography-meta-details >>> div');
  //   expect(element).toHaveClass('digi-typography-meta-details');
  // });
});
