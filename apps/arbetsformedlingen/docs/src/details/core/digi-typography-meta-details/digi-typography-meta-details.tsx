import { Component, Prop, h, State } from '@stencil/core';
import { CodeExampleLanguage, TypographyMetaVariation } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-typography-meta-details',
	styleUrl: 'digi-typography-meta-details.scss',
})
export class DigiTypographyMetaDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() typographyMetaVariation: TypographyMetaVariation = TypographyMetaVariation.PRIMARY;

	get typographMetaCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-typography-meta
	af-variation="${this.typographyMetaVariation}"
>
	<p>
		10 Januari 2021
	</p>
	<p slot="secondary">
		Elektrogatan 4 Solna, 113 99 Stockholm.
	</p>
</digi-typography-meta>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-typography-meta
	[attr.af-variation]="TypographyMetaVariation.${Object.keys(TypographyMetaVariation).find(key => TypographyMetaVariation[key] === this.typographyMetaVariation)}"
>
	<p>
		10 Januari 2021
	</p>
	<p slot="secondary">
		Elektrogatan 4 Solna, 113 99 Stockholm.
	</p>
</digi-typography-meta>`,
			[CodeExampleLanguage.REACT]: `\
<DigiTypographyMeta
	afVariation={TypographyMetaVariation.${Object.keys(TypographyMetaVariation).find(key => TypographyMetaVariation[key] === this.typographyMetaVariation)}}
>
	<p>
		10 Januari 2021
	</p>
	<p slot="secondary">
		Elektrogatan 4 Solna, 113 99 Stockholm.
	</p>
</DigiTypographyMeta>`
		}
	}
	render() {
		return (
			<div class="digi-typography-meta-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Metatextkomponenten används för att skapa två rader text ovanför varandra med en primär text och en sekundär, beskrivande text.
              Den sekundära texten ställs in med attributet <digi-code af-code="slot='secondary'" />.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>

						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example
								af-code={JSON.stringify(this.typographMetaCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
							>

								<div class="slot__controls" slot="controls">
									<digi-form-fieldset afName="Variant" afLegend="Variant" onChange={e => this.typographyMetaVariation = (e.target as any).value}>
										<digi-form-radiobutton
											afName="Variant"
											afLabel="Primär"
											afValue={TypographyMetaVariation.PRIMARY}
											afChecked={this.typographyMetaVariation === TypographyMetaVariation.PRIMARY}
										/>
										<digi-form-radiobutton
											afName="Variant"
											afLabel="Sekundär"
											afValue={TypographyMetaVariation.SECONDARY}
											afChecked={this.typographyMetaVariation === TypographyMetaVariation.SECONDARY}
										/>

									</digi-form-fieldset>
								</div>
								<digi-typography-meta
									afVariation={this.typographyMetaVariation}
								>
									<p>
										10 Januari 2021
									</p>
									<p slot="secondary">
										Elektrogatan 4 Solna, 113 99 Stockholm.
									</p>
								</digi-typography-meta>
							</digi-code-example>
						</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-top>
							<h2>Beskrivning</h2>
							<h3>Varianter</h3>
							<p>
								Metatextkomponenten finns i två olika varianter, en primär och en sekundär.
							</p>
							<h4>Primär</h4>
							<p>
								Den primära variationen ställs in med <digi-code af-code="af-variation='primary'" /> och gör texten som inte är markerad som sekundär fetad.
							</p>

							<h4>Sekundär</h4>
							<p>Den sekundära variationen ställs in med <digi-code af-code="af-variation='secondary'" /> och gör texten som är markerad som sekundär fetad.</p>

						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
