import { Component, Prop, h, State } from '@stencil/core';
import { CodeExampleLanguage, NavigationContextMenuItemType } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-navigation-context-menu-item-details',
	styleUrl: 'digi-navigation-context-menu-item-details.scss',
})
export class DigiNavigationContextMenuItemDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() navigationContextMenuItemType: NavigationContextMenuItemType = NavigationContextMenuItemType.LINK;

	get navigationContextMenuItemCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-context-menu
	af-text="Rullgardinsmeny"
	af-start-selected="0"
>
	<digi-navigation-context-menu-item
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `af-text="Detta är länk 1"`
					: `af-text="Detta är knapp 1"`
				}
		af-type="${this.navigationContextMenuItemType}"
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `af-href="#"\n\t`
					: ``
				}>
	</digi-navigation-context-menu-item>
	<digi-navigation-context-menu-item
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `af-text="Detta är länk 2"`
					: `af-text="Detta är knapp 2"`
				}
		af-type="${this.navigationContextMenuItemType}"
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `af-href="#"\n\t`
					: ``
				}>
	</digi-navigation-context-menu-item>
</digi-navigation-context-menu>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-context-menu
	af-text="Rullgardinsmeny"
	af-start-selected="0"
>
	<digi-navigation-context-menu-item
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `[attr.af-text]="'Detta är länk 1'"`
					: `[attr.af-text]="'Detta är knapp 1'"`
				}
		[attr.af-type]="NavigationContextMenuItemType.${Object.keys(
					NavigationContextMenuItemType
				).find(
					(key) =>
						NavigationContextMenuItemType[key] === this.navigationContextMenuItemType
				)}"
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `af-href="'#'"\n\t`
					: ``
				}>
	</digi-navigation-context-menu-item>
	<digi-navigation-context-menu-item
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `af-text="'Detta är länk 2'"`
					: `af-text="'Detta är knapp 2'"`
				}
		[attr.af-type]="NavigationContextMenuItemType.${Object.keys(
					NavigationContextMenuItemType
				).find(
					(key) =>
						NavigationContextMenuItemType[key] === this.navigationContextMenuItemType
				)}"
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `[attr.af-href]="'#'"\n\t`
					: ``
				}>
	</digi-navigation-context-menu-item>
</digi-navigation-context-menu>`,

			[CodeExampleLanguage.REACT]: `\
<DigiNavigationContextMenu
	afText="Rullgardinsmeny"
	afStart-selected={0}
>
	<DigiNavigationContextMenuItem
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `afText="Detta är länk 1"`
					: `afText="Detta är knapp 1"`
				}
		afType={NavigationContextMenuItemType.${Object.keys(
					NavigationContextMenuItemType
				).find(
					(key) =>
						NavigationContextMenuItemType[key] === this.navigationContextMenuItemType
				)}}
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `afHref="#"\n\t`
					: ``
				}>
	</DigiNavigationContextMenuItem>
	<DigiNavigationContextMenuItem
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `afText="Detta är länk 2"`
					: `afText="Detta är knapp 2"`
				}
		afType={NavigationContextMenuItemType.${Object.keys(
					NavigationContextMenuItemType
				).find(
					(key) =>
						NavigationContextMenuItemType[key] === this.navigationContextMenuItemType
				)}}
		${this.navigationContextMenuItemType === NavigationContextMenuItemType.LINK
					? `afHref="#"\n\t`
					: ``
				}>
	</DigiNavigationContextMenuItem>
</DigiNavigationContextMenu>`
		};
	}
	render() {
		return (
			<div class="digi-navigation-context-menu-item-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Rullgardinsmenyelementen används av rullgardinsmenyn <a href="/komponenter/digi-navigation-context-menu/oversikt"><digi-code af-code="<digi-navigation-context-menu>" /></a> för att skapa en lista av alternativ.
            </digi-typography-preamble>
					)}

					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example
							af-code={JSON.stringify(this.navigationContextMenuItemCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
						>
							<div style={{ height: '120px' }}>
								<digi-navigation-context-menu
									afText="Rullgardinsmeny"
									af-start-selected="0"
								>
									<digi-navigation-context-menu-item
										afText={
											this.navigationContextMenuItemType ===
												NavigationContextMenuItemType.LINK
												? `Detta är länk 1`
												: `Detta är knapp 1`
										}
										afHref={
											this.navigationContextMenuItemType ===
												NavigationContextMenuItemType.LINK
												? `#`
												: ``
										}
										afType={this.navigationContextMenuItemType}
									></digi-navigation-context-menu-item>
									<digi-navigation-context-menu-item
										afText={
											this.navigationContextMenuItemType ===
												NavigationContextMenuItemType.LINK
												? `Detta är länk 2`
												: `Detta är knapp 2`
										}
										afHref={
											this.navigationContextMenuItemType ===
												NavigationContextMenuItemType.LINK
												? `#`
												: ``
										}
										afType={this.navigationContextMenuItemType}
									></digi-navigation-context-menu-item>
								</digi-navigation-context-menu>
							</div>
							<div class="slot__controls" slot="controls">
								<digi-form-fieldset
									afName="Variant"
									afLegend="Variant"
									onChange={(e) =>
										(this.navigationContextMenuItemType = (e.target as any).value)
									}
								>
									<digi-form-radiobutton
										afName="Länk"
										afLabel="Länk"
										afValue={NavigationContextMenuItemType.LINK}
										afChecked={
											this.navigationContextMenuItemType ===
											NavigationContextMenuItemType.LINK
										}
									/>
									<digi-form-radiobutton
										afName="Knapp"
										afLabel="Knapp"
										afValue={NavigationContextMenuItemType.BUTTON}
										afChecked={
											this.navigationContextMenuItemType ===
											NavigationContextMenuItemType.BUTTON
										}
									/>
								</digi-form-fieldset>
							</div>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom afMarginTop>
							<h2>Beskrivning</h2>
							<h3>Varianter</h3>
							<p>
								<digi-code af-code="digi-navigation-context-menu-item" /> finns i två varianter: länkar och knappar.
								Vid användning av typen "knapp" emittas ett event med valt värde vid
								klick och rullgardinsmenyn stänger sig.
								Vid användning av knappar behövs ej attributet{' '}
								<digi-code af-code="af-href" />.
							</p>
							<digi-info-card
								afHeading="Bra att tänka på"
								afHeadingLevel={`h3` as any}
							>
								<digi-list>
									<li>
										Alternativen i listan måste sorteras på ett logiskt sätt. Det kan till exempel vara i bokstavsordning eller i grupper.
									</li>
								</digi-list>
							</digi-info-card>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
