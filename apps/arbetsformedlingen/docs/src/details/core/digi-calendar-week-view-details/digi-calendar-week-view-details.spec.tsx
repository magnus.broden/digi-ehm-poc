import { newSpecPage } from '@stencil/core/testing';
import { DigiCalendarWeekView } from './digi-calendar-week-view-details';

describe('digi-calendar-week-view-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiCalendarWeekView],
			html:
				'<digi-calendar-week-view-details></digi-calendar-week-view-details>'
		});
		expect(root).toEqualHtml(`
      <digi-calendar-week-view-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-calendar-week-view-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiCalendarWeekView],
			html: `<digi-calendar-week-view-details first="Stencil" last="'Don't call me a framework' JS"></digi-calendar-week-view-details>`
		});
		expect(root).toEqualHtml(`
      <digi-calendar-week-view-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-calendar-week-view-details>
    `);
	});
});
