import { Component, h, Host, Listen, Prop, State } from '@stencil/core';
import { ButtonVariation, CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-calendar-week-view-details',
	styleUrl: 'digi-calendar-week-view-details.scss',
	scoped: true
})
export class DigiCalendarWeekView {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() selectedDate: string;

	private dates = [
		'2022-06-12',
		'2022-05-12',
		'2022-05-16',
		'2022-05-09',
		'2022-06-20'
	];

	@Listen('afOnDateChange')
	dateChangeHandler(e) {
		this.selectedDate = e.detail;
	}

	get calendarWeekViewCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-calendar-week-view 
	af-dates='["2022-06-12","2022-05-12","2022-05-16","2022-05-09","2022-06-20"]'>
	<p>Lista över valbara datum...</p>
</digi-calendar-week-view>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-calendar-week-view 
	[attr.af-dates]='["2022-06-12","2022-05-12","2022-05-16","2022-05-09","2022-06-20"]'>
	<p>Lista över valbara datum...</p>
</digi-calendar-week-view>`,
			[CodeExampleLanguage.REACT]: `\
<DigiCalendarWeekView 
	afDates="['2022-06-12','2022-05-12','2022-05-16','2022-05-09','2022-06-20']">
	<p>Lista över valbara datum...</p>
</DigiCalendarWeekView>`
		};
	}

	get hasBookings() {
		return this.dates.includes(this.selectedDate);
	}

	render() {
		return (
			<Host>
				<div class="digi-calendar-week-view-details">
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten kan visa upp en veckovy där man kan bläddra mellan veckor och enkelt välja enskild dag.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						<digi-code-example af-code={JSON.stringify(this.calendarWeekViewCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<digi-calendar-week-view af-dates='["2022-06-12","2022-05-12","2022-05-16","2022-05-09","2022-06-20"]'>
								<div class="digi-calendar-week-view-details__result">
									<p>
										Innehållet nedanför veckovyn ligger i en slot och är endast ett exempel.
									</p>
									<h2 class="digi-calendar-week-view-details__heading">
										{this.selectedDate}
									</h2>
									{this.hasBookings && (
										<ul class="digi-calendar-week-view-details__list">
											<li>
												<digi-button afVariation={ButtonVariation.SECONDARY}>
													09:00-11:00
												</digi-button>
											</li>
											<li>
												<digi-button afVariation={ButtonVariation.SECONDARY}>
													09:30-11:30
												</digi-button>
											</li>
										</ul>
									)}
									{!this.hasBookings && (
										<p>
											Inga lediga tider.
										</p>
									)}
								</div>
							</digi-calendar-week-view>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom afMarginTop>
							<article>
								<h2>Beskrivning</h2>
								<p>Komponenten kan ta emot en lista av datum som innehåller t.ex. bokningsbara tider.</p>
								<p>Endast de veckor med valbara dagar visas i komponenten och man kan alltså endast navigera mellan dessa veckor.</p>
								<p>Som standard är första valbara dagen vald. Vill man själv ändra till ett annat datum kan man använda metoden <digi-code afCode='afMSetActiveDate'></digi-code></p>
							</article>
						</digi-layout-container>
					)}
					{!this.afShowOnlyExample && (
						<digi-layout-block af-container='none' af-margin-bottom>
							<p>
								<digi-info-card
									afHeading="Bra att veta"
									afHeadingLevel={`h3` as any}
								>
									<digi-list>
										<li>
											Om du lägger in resultatet inuti komponenten så skrivs det ut i en slot med attributen <digi-code afCode='role="status"' af-code='role="status"'></digi-code> och <digi-code afCode='aria-live="assertive"' af-code='aria-live="assertive"'></digi-code> vilket gör att användare av skärmläsare informeras vid uppdateringar.
										</li>
									</digi-list>
								</digi-info-card>
							</p>
							<br />
						</digi-layout-block>
					)}
				</div>
			</Host>
		);
	}
}
