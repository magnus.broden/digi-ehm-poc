import { Component, Prop, h, State } from '@stencil/core';
import {
	CodeExampleLanguage
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-calendar-details',
	styleUrl: 'digi-calendar-details.scss'
})
export class DigiCalendarDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() showWeek: boolean = false;
	@State() multipleDates: boolean = false;
	@State() hasFooter: boolean = false;

	get calendarCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-calendar
	af-active="true"\
	${this.showWeek ? `\n\taf-show-week-number="true"` : ''}\
	${this.multipleDates ? '\n\taf-multiple-dates="true"' : '\ '}
>\
${this.hasFooter ? '\n\t<digi-button slot="calendar-footer">Välj</digi-button>' : ''}
</digi-calendar>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-calendar
	[attr.af-active]="true"\
	${this.showWeek ? '\n\t[attr.af-show-week-number]="true"' : ''}\
	${this.multipleDates ? '\n\t[attr.af-multiple-dates]="true"' : '\ '}
>\
${this.hasFooter ? '\n\t<digi-button slot="calendar-footer">Välj</digi-button>' : ''}
</digi-calendar>`,
			[CodeExampleLanguage.REACT]: `\
<DigiCalendar
	afActive={true}\
	${this.showWeek ? '\n\tafShowWeekNumber={true}' : ''}\
	${this.multipleDates ? '\n\tafMultipleDates={true}' : '\ '}
>\
${this.hasFooter ? '\n\t<DigiButton slot="calendar-footer">Välj</DigiButton>' : ''}
</DigiCalendar>`
		};
	}
	render() {
		return (
			<div class="digi-calendar-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Kalendern används för att visuellt representera ett datum och ge
              konsekventa medel för att navigera dess delar genom vyer i dagar,
              månader och år.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example af-code={JSON.stringify(this.calendarCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
								{!this.hasFooter && (
									<digi-calendar
										afActive={true}
										afMultipleDates={this.multipleDates}
										afShowWeekNumber={this.showWeek}
										afInitSelectedMonth={0}
									></digi-calendar>
								)}

								{this.hasFooter && (
									<digi-calendar
										afActive={true}
										afMultipleDates={this.multipleDates}
										afShowWeekNumber={this.showWeek}
										afInitSelectedMonth={0}
									>
										<digi-button
											slot="calendar-footer"
											af-variation="primary"
											aria-label="Välj"
											af-size="medium"
										>
											Välj
										</digi-button>
									</digi-calendar>
								)}

								<div class="slot__controls" slot="controls">
									<digi-form-fieldset afLegend="Visa veckonummer">
										<digi-form-checkbox
											afLabel="Ja"
											afChecked={this.showWeek}
											onAfOnChange={() =>
												this.showWeek ? (this.showWeek = false) : (this.showWeek = true)
											}
										></digi-form-checkbox>
									</digi-form-fieldset>
									<digi-form-fieldset afLegend="Välj multipla dagar">
										<digi-form-checkbox
											afLabel="Ja"
											afChecked={this.multipleDates}
											onAfOnChange={() =>
												this.multipleDates ? (this.multipleDates = false) : (this.multipleDates = true)
											}
										></digi-form-checkbox>
									</digi-form-fieldset>
									<digi-form-fieldset afLegend="Footer">
										<digi-form-checkbox
											afLabel="Ja"
											afChecked={this.hasFooter}
											onAfOnChange={() =>
												this.hasFooter ? (this.hasFooter = false) : (this.hasFooter = true)
											}
										></digi-form-checkbox>
									</digi-form-fieldset>
								</div>
							</digi-code-example>
						</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h2>Beskrivning</h2>
							<h3>Generellt</h3>
							<p>
								Kalendern finns i en horisontell layout som visar en månad i taget.
							</p>
							<p>
								Om nuvarande månad visas när kalendern laddas in är dagens datum
								fokuserat, annars månadens första dag (dvs. 1). Navigerar man runt i
								kalendern ändras fokuserat datum till senast fokuserat.
							</p>
							<p>Nuvarande månads datum har en färgidentifiering.</p>
							<p>
								Kalendern är tillgänglighetsanpassad och går att navigera runt i med
								knapparna <digi-code af-code="upp" />, <digi-code af-code="ned" />,{' '}
								<digi-code af-code="vänster" />, <digi-code af-code="höger" />,
								<digi-code af-code="shift" />, <digi-code af-code="shift-tab" /> och{' '}
								<digi-code af-code="enter" />.
							</p>
						</digi-layout-container>
					)}
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h3>Varianter</h3>
							<p>
								Kalendern finns enbart i en variant. Genom att använda sloten
								calendar-footer ex. <digi-code af-code="slot='calendar-footer'" /> går
								det att visa element under kalendern, t.ex. knappar.
							</p>
						</digi-layout-container>
					)}
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h3>Användning</h3>
							<digi-list>
								<li>
									Det går att välja vilken månad som ska visas som förvald genom att
									sätta ett nummer värde på propen{' '}
									<digi-code af-code="af-init-selected-month" />. Ex.{' '}
									<digi-code af-code="af-init-selected-month='0'" /> visar Januari,
									<digi-code af-code="af-init-selected-month='1'" /> visar Februari osv.
									Den månad som dagens datum är på är förvalt om inget annat anges.
								</li>
								<li>
									Hela kalendern kan döljas genom att sätta värdet false i propen{' '}
									<digi-code af-code="af-active='false'" />, visas som förvalt.
								</li>
								<li>
									Det går att markera och få ut mer än ett datum i kalender om du anger {' '}
									<digi-code af-code="af-multiple-dates='true'" />
									Detta innebär att du får ut alla markerade datum vid eventet
									afOnDateSelectedChange.
								</li>
								<li>
									Det går av välva om du vill visa veckonummer i kalendern med {' '}
									<digi-code af-code="af-show-week-number='true'" />
									Veckorna går ej att välja och är bara till för att hjälpa visuellt.
								</li>
							</digi-list>
						</digi-layout-container>
					)}
				</digi-typography>
			</div>
		);
	}
}
