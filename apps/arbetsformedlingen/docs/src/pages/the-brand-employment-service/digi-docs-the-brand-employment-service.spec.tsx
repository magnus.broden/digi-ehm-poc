import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsTheBrandEmploymentService } from './digi-docs-the-brand-employment-service';

describe('digi-docs-the-brand-employment-service', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsTheBrandEmploymentService],
      html: '<digi-docs-the-brand-employment-service></digi-docs-the-brand-employment-service>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-the-brand-employment-service>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-the-brand-employment-service>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsTheBrandEmploymentService],
      html: `<digi-docs-the-brand-employment-service first="Stencil" last="'Don't call me a framework' JS"></digi-docs-the-brand-employment-service>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-the-brand-employment-service first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-the-brand-employment-service>
    `);
  });
});
