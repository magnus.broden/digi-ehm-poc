import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-the-brand-employment-service', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent(
      '<digi-docs-the-brand-employment-service></digi-docs-the-brand-employment-service>'
    );
    const element = await page.find('digi-docs-the-brand-employment-service');
    expect(element).toHaveClass('hydrated');
  });

  it('renders changes to the name data', async () => {
    const page = await newE2EPage();

    await page.setContent(
      '<digi-docs-the-brand-employment-service></digi-docs-the-brand-employment-service>'
    );
    const component = await page.find('digi-docs-the-brand-employment-service');
    const element = await page.find(
      'digi-docs-the-brand-employment-service >>> div'
    );
    expect(element.textContent).toEqual(`Hello, World! I'm `);

    component.setProperty('first', 'James');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James`);

    component.setProperty('last', 'Quincy');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

    component.setProperty('middle', 'Earl');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
  });
});
