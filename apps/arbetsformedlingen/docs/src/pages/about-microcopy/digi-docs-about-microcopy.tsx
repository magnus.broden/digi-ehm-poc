import { Component, h } from '@stencil/core';


@Component({
  tag: 'digi-docs-about-microcopy',
  styleUrl: 'digi-docs-about-microcopy.scss',
  scoped: true
})
export class DigiDocsAboutMicrocopy {
  

  render() {
    return (
      <div class="digi-docs-about-digi-core">
        <digi-docs-page-layout
          af-edit-href="pages/digi-docs-about-digi-core/digi-docs-about-digi-core.tsx"
        >
          <digi-typography>
            <digi-layout-block>
              <digi-typography-heading-jumbo af-text="Mikrocopy"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                All mikrocopy grundar sig i de digitala upplevelseprinciperna
                och ska följa innehållstrategin och den tonalitet som tagits
                fram för hur vi skriver på Arbetsförmedlingen: professionellt,
                inspirerande och förtroendeingivande.
              </digi-typography-preamble>
              <br />
              <p>
              Mikrocopy ska alltid vara informativ och tydlig och vägleda användaren så att hen känner sig trygg och har kontroll.
              </p>
              <p>
              Målet är att skapa en röd tråd genom alla våra produkter, tjänster och kanaler så att vi har EN röst oavsett vilken kanal användaren möter oss i. Syftet är att användaren ska kunna gå mellan våra tjänster sömlöst, utföra sitt ärende på ett smidigt sätt och också kunna lita på att resultatet blir det förväntade. 
               </p>
               <p>
               När vi lyckas skapa både tydlighet och trygghet på en så pass hög och jämn nivå att det känns naturligare att använda våra digitala tjänster än att lyfta på luren, då har vi nått vårt mål.
               </p>
               <p>Designsystemets roll är att stötta upp med riktlinjer och just nu undersöker designsystemet även möjligheten att kunna erbjuda mikrocopy på komponentnivå. Detta innebär att myndigheten skulle kunna ha ett kodbibliotek där vissa ord alltid är kopplade till vissa interaktioner. Det gör att vi på kodnivå kan skapa ett enhetligt språkbruk inte bara i utan även mellan våra tjänster. </p>
              <p>Orden och termerna är granskade av copywriters, kommunikatörer och representanter för tillgänglighet. På det sättet kan alla som använder innehållet ur designsystemet lita på att innehållet uppfyller kraven på tillgänglighet och följer Arbetsförmedlingens varumärkesriktlinjer.</p>
              <p>
              Tanken är att ingen ska behöva gissa eller hitta på egna termer för befintliga interaktioner och flöden. Det sparar tid för alla, låter oss fokusera på rätt saker och hjälper nyanställda komma igång snabbt. </p>
              <br />
            </digi-layout-block>
          </digi-typography>
        </digi-docs-page-layout>
      </div>
    );
  }
}
