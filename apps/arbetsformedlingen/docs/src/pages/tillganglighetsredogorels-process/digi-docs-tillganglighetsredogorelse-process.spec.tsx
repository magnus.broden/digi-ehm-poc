import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsTillganglighetsredogorelseProcess } from './digi-docs-tillganglighetsredogorelse-process';

describe('digi-docs-tillganglighetsredogorelse-process', () => {
  it('renders', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsTillganglighetsredogorelseProcess],
      html: '<digi-docs-tillganglighetsredogorelse-process></digi-docs-tillganglighetsredogorelse-process>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-tillganglighetsredogorelse-process>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-tillganglighetsredogorelse-process>
    `);
  });

  it('renders with values', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsTillganglighetsredogorelseProcess],
      html: `<digi-docs-tillganglighetsredogorelse-process first="Stencil" last="'Don't call me a framework' JS"></digi-docs-tillganglighetsredogorelse-process>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-tillganglighetsredogorelse-process first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-tillganglighetsredogorelse-process>
    `);
  });
});
