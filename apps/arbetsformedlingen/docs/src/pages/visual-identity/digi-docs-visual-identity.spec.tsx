import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsVisualIdentity } from './digi-docs-visual-identity';

describe('digi-docs-visual-identity', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsVisualIdentity],
      html: '<digi-docs-visual-identity></digi-docs-visual-identity>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-visual-identity>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-visual-identity>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsVisualIdentity],
      html: `<digi-docs-visual-identity first="Stencil" last="'Don't call me a framework' JS"></digi-docs-visual-identity>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-visual-identity first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-visual-identity>
    `);
  });
});
