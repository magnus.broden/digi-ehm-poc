import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'digi-docs-accessibility-and-design',
  styleUrl: 'digi-docs-accessibility-and-design.scss',
  scoped: true
})
export class DigiDocsAccessibilityAndDesign {
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  private getText(): string {
    return `${this.first} ${this.middle} ${this.last}`;
  }

  render() {
    return <div>A och D{this.getText()}</div>;
  }
}
