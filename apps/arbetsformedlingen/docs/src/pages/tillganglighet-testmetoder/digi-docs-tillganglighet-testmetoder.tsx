import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'digi-docs-tillganglighet-testmetoder',
  styleUrl: 'digi-docs-tillganglighet-testmetoder.scss',
  scoped: true
})
export class TillganglighetTestmetoder {
  @State() pageName = 'Testmetoder';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/tillganglighet-testmetoder/digi-docs-tillganglighet-testmetoder.tsx"
        >
          <span slot="preamble">
            
            /** Vi kanske kommer använda den här sidan framöver,  men i så fall för att lista testmetoder vi har från Tillgänglighetslistan (inte duplicerat innehåll). */
            Vi har påbörjat arbetet med att se över och förenkla alla texter för
            testmetoder. Fram tills att det arbetet är klart kan du hitta de
            befintliga testmetoderna på gamla Digi.
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <digi-link
                af-target="_blank"
                af-variation="small"
                afHref="https://digi.arbetsformedlingen.se/tillganglighet/testmetoder"
              >
                                 {' '}
                <digi-icon-external-link-alt slot="icon"></digi-icon-external-link-alt>
                                  Testmetoder på gamla Digi (öppnas i nytt
                fönster)
              </digi-link>
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}

