import { newSpecPage } from '@stencil/core/testing';
import { TillganglighetTestmetoder } from './digi-docs-tillganglighet-testmetoder';

describe('digi-docs-tillganglighet-testmetoder', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [TillganglighetTestmetoder],
      html: '<digi-docs-tillganglighet-testmetoder></digi-docs-tillganglighet-testmetoder>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-tillganglighet-testmetoder>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-tillganglighet-testmetoder>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [TillganglighetTestmetoder],
      html: `<digi-docs-tillganglighet-testmetoder first="Stencil" last="'Don't call me a framework' JS"></digi-docs-tillganglighet-testmetoder>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-tillganglighet-testmetoder first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-tillganglighet-testmetoder>
    `);
  });
});
