import { Component, h, State } from '@stencil/core';

@Component({
	tag: 'digi-docs-copy-languages',
	styleUrl: 'digi-docs-copy-languages.scss',
	scoped: true
})
export class CopyLanguages {
	@State() pageName = 'Språk';

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/copy-languages/digi-docs-copy-languages.tsx"
				>
				TeST
				</digi-docs-page-layout>
			</host>
		);
	}
}
