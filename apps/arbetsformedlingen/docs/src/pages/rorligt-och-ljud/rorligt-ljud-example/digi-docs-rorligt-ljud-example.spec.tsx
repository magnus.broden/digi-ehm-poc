import { newSpecPage } from '@stencil/core/testing';
import { GoodBadExample } from './digi-docs-rorligt-ljud-example';

describe('digi-docs-rorligt-ljud-example', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [GoodBadExample],
			html: '<digi-docs-rorligt-ljud-example></digi-docs-rorligt-ljud-example>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-rorligt-ljud-example>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-rorligt-ljud-example>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [GoodBadExample],
			html: `<digi-docs-rorligt-ljud-example first="Stencil" last="'Don't call me a framework' JS"></digi-docs-rorligt-ljud-example>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-rorligt-ljud-example first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-rorligt-ljud-example>
    `);
	});
});
