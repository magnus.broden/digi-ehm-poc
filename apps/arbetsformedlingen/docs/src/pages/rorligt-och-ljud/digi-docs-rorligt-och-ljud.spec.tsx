import { newSpecPage } from '@stencil/core/testing';
import { RorligtOchLjud } from './digi-docs-rorligt-och-ljud';

describe('digi-docs-rorligt-och-ljud', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [RorligtOchLjud],
			html: '<digi-docs-rorligt-och-ljud></digi-docs-rorligt-och-ljud>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-rorligt-och-ljud>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-rorligt-och-ljud>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [RorligtOchLjud],
			html: `<digi-docs-rorligt-och-ljud first="Stencil" last="'Don't call me a framework' JS"></digi-docs-rorligt-och-ljud>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-rorligt-och-ljud first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-rorligt-och-ljud>
    `);
	});
});
