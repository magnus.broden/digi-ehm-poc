import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-anvandningstester', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-anvandningstester></digi-docs-anvandningstester>');

    const element = await page.find('digi-docs-anvandningstester');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-anvandningstester></digi-docs-anvandningstester>');

    const element = await page.find('digi-docs-anvandningstester >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
