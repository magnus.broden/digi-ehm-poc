import { newSpecPage } from '@stencil/core/testing';
import { AboutAccessibilityAndLanguage } from './digi-docs-about-accessibility-and-language';

describe('digi-docs-about-accessibility-and-language', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutAccessibilityAndLanguage],
			html:
				'<digi-docs-about-accessibility-and-language></digi-docs-about-accessibility-and-language>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-accessibility-and-language>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-accessibility-and-language>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutAccessibilityAndLanguage],
			html: `<digi-docs-about-accessibility-and-language first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-accessibility-and-language>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-accessibility-and-language first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-accessibility-and-language>
    `);
	});
});
