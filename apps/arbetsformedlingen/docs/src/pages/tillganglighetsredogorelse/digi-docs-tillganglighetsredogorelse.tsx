import { Component, h, State } from '@stencil/core';


@Component({
  tag: 'digi-docs-tillganglighetsredogorelse',
  styleUrl: 'digi-docs-tillganglighetsredogorelse.scss',
  scoped: true
})
export class DigiDocsTillganglighetsredogorelse {
  
  @State() pageName = 'Tillgänglighetsredogörelse';

  render() {
    return (
      
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/tillganglighetsredogorelse/digi-docs-tillganglighetsredogorelse.tsx"
        >
          <span slot="preamble">
            Detta är tillgänglighetsredogörelesen för webbplatsen Designsystem,
            vilken Arbetsförmedlingen är ägare till. Vi vill att alla ska kunna
            använda webbplatsen, oavsett behov. Här redogör vi för hur
            webbplatsen Designsystem uppfyller lagen om tillgänglighet till
            digital offentlig service, eventuella kända tillgänglighetsproblem
            och hur du kan rapportera brister till oss så att vi kan åtgärda
            dem.</span>

            <digi-layout-container af-margin-bottom>
            <article>
              <h2>Hur tillgänglig är webbplatsen?</h2>
            <p>
              Vi är medvetna om att delar av webbplatsen Designsystem inte är
              helt tillgängliga. I avsnittet om innehåll som inte är
              tillgängligt listar vi brister i tillgänglighet som är kända just
              nu.
            </p>
            </article>
            </digi-layout-container>
            <digi-layout-container af-margin-bottom>
            <article>
              <h2>Kontakta oss om du hittar fler brister </h2>
            <p>
              Vi strävar hela tiden efter att förbättra webbplatsens
              tillgänglighet. Om du upptäcker problem som inte är beskrivna på
              den här sidan, eller om du anser att vi inte uppfyller lagens
              krav, meddela oss så att vi får veta att problemet finns.
              <br />
              <br />
              <digi-link-external
                afHref="https://arbetsformedlingen.se/synpunkter-tillganglighet"
                af-target="_blank"
                af-variation="small"
              >
                Lämna synpunkter på digital tillgänglighet
              </digi-link-external>
            </p>
            </article>
            </digi-layout-container>
            <digi-layout-container af-margin-bottom>
            <article>
              <h2>Kontakta tillsynsmyndigheten </h2>
            <p>
              Myndigheten för digital förvaltning har ansvaret för tillsyn för
              lagen om tillgänglighet till digital offentlig service. Om du inte
              är nöjd med hur vi hanterar dina synpunkter kan du{' '}
              <a href="https://www.digg.se/tdosanmalan">
                kontakta Myndigheten för digital förvaltning (länk till extern
                webbplats)
              </a>{' '}
              och berätta det.
            </p>
            </article>
            </digi-layout-container>
            <digi-layout-container af-margin-bottom>
            <article>
              <h2>Teknisk information om webbplatsens tillgänglighet</h2>
            <p>
              Den här webbplatsen är delvis förenlig med lagen om tillgänglighet
              till digital offentlig service, på grund av de brister som
              beskrivs nedan.
            </p>

            <h3>Innehåll som inte är tillgängligt</h3>
            <p>
              Vi är medvetna om att följande punkter inte följer lagkraven:{' '}
            </p>
            <h4>Övergripande brister på webbplatsen</h4>
            <digi-list>
              <li>
                Webbplatsen saknar en genväg som låter användare, som navigerar
                med tangentbord, hoppa över innehåll som upprepas på flera
                sidor, exempelvis sidhuvudet och den vertikala menyn.
              </li>
              <li>
                När användare som navigerar med skärmläsare klickar på en länk i
                den vertikala menyn, framgår det inte att nytt innehåll har
                laddats in. Användarens Fokus flyttas inte till början av sidans
                innehåll.
              </li>
            </digi-list>
            <h4>Brister i specifika komponenter</h4>
            <digi-list>
              <li>
                I flera av komponenterna framhävs visuellt fokus på ett otydligt
                sätt.
              </li>
            </digi-list>
            <h4>Brister i sektionen Grafisk profil</h4>
            <digi-list>
              <li>
                På sidorna Logotyp och Grafik finns flera bilder som innehåller
                text, exempelvis mått för storlek på logotyp och siffror i diagram.
              </li>
            </digi-list>
            <p>
              Vår ambition är åtgärda de kända tillgänglighetsbristerna senast den
              2023-06-30.
            </p>
            <h4>Oskäligt betungande anpassning</h4>
            <p>
              Arbetsförmedlingen åberopar undantag för oskäligt betungande
              anpassning enligt 12 § lagen om tillgänglighet till digital
              offentlig service för nedanstående innehåll för Designsystemet:
            </p>
            <digi-list>
              <li>
                Vissa komponenter i Digi-ng är inte förenliga med lagen om
                tillgänglighet till digital offentlig service och kommer heller
                inte att uppfylla kraven eftersom denna del av
                Arbetsförmedlingens Designsystem ska avvecklas på sikt. För
                nyutveckling hänvisas våra utvecklingsteam till Digi-core där
                komponenterna granskas kontinuerligt.
              </li>
            </digi-list>
            <h3>Hur vi har testat webbplatsen</h3>
            <p>
              Vi har gjort en genomgående självskattning (intern testning) av
              Designsystemet med hjälp av våra egna experter.
            </p>
            <p>
              Senaste bedömningen gjordes den 2 februari 2023.
              <br />
              <br />
              Redogörelsen uppdaterades den 2 februari 2023.{' '}
            </p>
            <h3>Hur vi jobbar med digital tillgänglighet</h3>
            <p>
              Vi strävar efter att Arbetsförmedlingens webbplatser ska kunna
              uppfattas, hanteras och förstås av alla användare, oavsett behov
              eller funktionsnedsättning och oberoende av vilka hjälpmedel du
              använder. Vi ska uppnå grundläggande tillgänglighet
              genom att följa WCAG 2.1 på nivå AA.
            </p>
            <p>
              Innehåll, komponenter, UI-kit och ikoner som omfattas av nuvarande
              MVP för Arbetsförmedlingens designsystem granskas kontinuerligt
              och denna redogörelse uppdateras löpande.
            </p>
            <p>
              Ett arbete pågår även för att se över Arbetsförmedlingens digitala
              riktlinjer. För att ge våra medarbetare rätt förutsättningar och
              kunskap om digital tillgänglighet lägger vi stort fokus på att
              utbilda de som utvecklar våra digitala tjänster.
            </p>
            </article>
            </digi-layout-container>
      </digi-docs-page-layout>
    );
  }
}
