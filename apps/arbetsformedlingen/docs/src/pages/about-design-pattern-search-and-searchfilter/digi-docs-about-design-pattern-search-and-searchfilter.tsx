import { Component, h, Prop } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { LinkVariation } from 'libs/core/package/src';
import {
	NavigationContextMenuItemType
} from '@digi/arbetsformedlingen';
import { router } from '../../global/router';
import state from '../../store/store';

@Component({
	tag: 'digi-docs-about-design-pattern-search-and-searchfilter',
	styleUrl: 'digi-docs-about-design-pattern-search-and-searchfilter.scss',
	scoped: true
})
export class AboutDesignPatternSearchAndSearchfilter {
	Router = router;

	@Prop() pageName = 'Sök och sökfilter';

	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-search-and-searchfilter/digi-docs-about-design-pattern-search-and-searchfilter.tsx"
				>
					<Helmet>
						<meta
							property="rek:pubdate"
							content="Mon Jun 26 2023 11:49:14 GMT+0200 (Central European Summer Time)"
						/>
					</Helmet>
					<span slot="preamble">
						En viktig del av användarupplevelsen på en större webbplats är en väl
						fungerande och relevant sökfunktion. Då kan till exempel filter hjälpa
						användare att hitta passande innehåll baserat på olika kriterier.
						Samtidigt förbättrar det upplevelsen genom att snabbt avgränsa resultatet
						till mer specifika sökträffar.
					</span>
					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<br />
						<h2>Generellt om sök</h2>
						<p>
							Det är viktigt att vara tydlig genom att se till att sökfältet är synligt
							och att användarna förstår vad de kan söka efter. Det är avgörande att
							sökresultaten är enkla att förstå och sorterade på ett logiskt sätt. Att
							ge användarna feedback under sökningen är också viktigt, till exempel
							genom att visa antalet hittade resultat. En söklösning bör utformas för
							att passa den tillgängliga datan som ska visas för användaren och
							målgruppen.
						</p>
						<br />
						<h2>Visa sökförslag direkt</h2>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<h3>Search-as-you-type</h3>
								<p>
									När användaren börjar skriva in sökord i sökrutan, visa sökförslag
									direkt. Detta ger användaren omedelbar feedback på vad som kommer att
									visas när de söker. Det kan även vara lämpligt att ge förslag på
									populära söktermer eller relaterade sökningar.
								</p>
								<h3>Använd auto-suggest</h3>
								<p>
									Auto-suggest kan hjälpa användaren att hitta söktermer genom att
									automatiskt fylla i sökfraser baserat på tidigare sökningar, populära
									sökfraser eller synonymer.
								</p>
								<h3>Visa relevanta resultat för varje sökord</h3>
								<p>
									Om användaren skriver in flera ord i sökrutan, visar vi relevanta
									sökresultat för varje ord i realtid. Detta kan hjälpa användaren att
									hitta det de söker snabbare. Tillåt sökning efter flera termer, om
									möjligt även genom att använda boolean-operatörer som "AND" och "OR".
								</p>
								<h3>Möjlighet att ge feedback</h3>
								<p>
									Låt användare ge feedback på sökfunktionen genom att implementera
									feedbackmöjligheter. Detta kan hjälpa till att förbättra sökresultaten
									över tid.
								</p>
								<digi-link-internal
									afHref="komponenter/digi-tools-feedback/oversikt"
									af-variation="small"
								>
									Se info om vår feedbackkomponent
								</digi-link-internal>
							</div>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<h3>Sökfält</h3>
									<br />
									<digi-form-input-search
										afLabel="Sök efter artiklar, personer, dokument, nyheter osv."
										af-variation="large"
										af-type="search"
										afButtonText="Sök"
									></digi-form-input-search>
									<br />
									<digi-media-image
										afUnlazy
										af-src="/assets/images/pattern/search-autosuggest.png"
										afAlt="Exempel på hur sök med auto suggest fungerar"
									></digi-media-image>
									<br />
									<p>
										Baskomponent för sökfält. Använd auto-suggest när det är relevant med
										tanke på underliggande data och användarens behov. Förslagen kan
										kompletteras med indikation på olika kategorier för att ge användaren
										en snabb överblick.
									</p>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<h2>Om våra filter, kategori- och multifilter</h2>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									Det är viktigt att filter inte begränsar sökresultat för mycket i det
									första skedet eftersom användare då kan missa relevant information.
									Filtret bör också vara anpassat för mindre skärmar som till exempel
									mobiltelefoner och surfplattor.
								</p>
								<p>
									Genom att erbjuda ett kategorifilter i början av ett sökflöde hjälper
									vi användaren att få en grov överblick och skapa rätt förväntan om vad
									hen kan söka på. Även för den som söker via fritextfält.
								</p>
								<p>
									Genom att erbjuda ett multifilter i nästa steg av en sökning, får
									användaren överblick över alternativ och kan finjustera sin sökning
									vidare. Multifilter har sin styrka i deras förmåga att ge möjlighet att
									filtrera sökresultaten baserat på flera kriterier samtidigt, utan att
									behöva göra flera separata sökningar. Genom att använda multifilter
									knappar kan användare enkelt klicka på de olika filteralternativen som
									de vill tillämpa, och sökresultaten justeras i realtid efter deras val.
									Detta ger användaren en direkt feedback på vilka filter som är aktiva
									och hur många resultat som matchar varje filter.
								</p>
								<p>
									En annan styrka med multifilter-knappar är att de är visuellt
									komprimerade och tar lite plats både i desktop- och mobilgränssnitt.
								</p>
							</div>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<h3>Filterkomponenter</h3>
									<h4>Kategorifilter</h4>
									<digi-form-category-filter
										afCategories={[
											{
												name: 'Kategori Ett',
												hits: 3
											},
											{
												name: 'Kategori Två',
												hits: 5
											},
											{
												name: 'Kategori Tre',
												hits: 4
											}
										]}
										af-visible-collapsed="2"
										af-start-collapsed="true"
									></digi-form-category-filter>
									<h4>Multifilter</h4>
									<div class="digi-docs-about-design-pattern-search-and-searchfilter--multifilter">
										<digi-form-filter
											afFilterButtonText="Enhet"
											afSubmitButtonText="Filtrera"
										>
											<digi-form-checkbox afLabel="Val 1" af-label="Val 1" />
											<digi-form-checkbox afLabel="Val 2" af-label="Val 2" />
											<digi-form-checkbox afLabel="Val 3" af-label="Val 3" />
											<digi-form-checkbox afLabel="Val 3" af-label="Val 4" />
										</digi-form-filter>
										<digi-form-filter
											afFilterButtonText="Plats"
											afSubmitButtonText="Filtrera"
										>
											<digi-form-checkbox afLabel="Val 1" af-label="Val 1" />
											<digi-form-checkbox afLabel="Val 2" af-label="Val 2" />
											<digi-form-checkbox afLabel="Val 3" af-label="Val 3" />
											<digi-form-checkbox afLabel="Val 3" af-label="Val 4" />
										</digi-form-filter>
									</div>
									<br />
									<p>
										Baskomponenter för kategorifilter och multifilter. Kategorifilter är
										responsivt anpassat för enskilda skärmbredder. Med funktionen
										visa-fler-filter, öppnar användaren fler filter. Multifilterknappar
										kan innehålla många filterval, men tar liten plats i stängt läge.
									</p>
								</digi-layout-block>
							</div>
							<br />
						</digi-layout-columns>
						<h2>Kategorier</h2>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									Facetter är attribut som filtrerar sökresultat som till exempel
									dokument, sidor kontor, grupper, nyheter, etceterera. Det ger en
									överblick av kategorierna och attributen för sökresultaten. Facettering
									passar när sökresultaten är många och användaren behöver sortera för
									att hitta relevanta resultat.
								</p>
								<p>
									Att visa kategorier är användbart för webbplatser och intranät med
									stora datamängder, för att hjälpa användare att ge överblick, förstå
									strukturer och hitta relevant information. Det kan vara en fördel att
									redan vid ”search-as-you-type” få förslag på sökresultat strukturerat i
									kategorier.
								</p>
								<h2>Taggar</h2>
								<p>
									Genom att visa taggar från sökningen, kan användare snabbt identifiera
									vilka sökord som är aktiverade för sökningen. Detta gör det enkelt för
									användare att hitta relevanta resultat snabbare och enklare att lägga
									till och ta bort olika sökval. (Det går även att visa förslagstaggar,
									baserad på användarens tidigare sökningar.)
								</p>
								<h2>Sortering</h2>
								<p>
									Med sortering rangordnar användaren sökresultat efter kriterier som
									relevans, publiceringsdatum, eller alfabetisk ordning. Sortering är
									viktigt för att effektivt hitta information i ett söksystem.
								</p>
							</div>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<h3>Taggar och sortering</h3>
									<br />
									<digi-tag
										afText="Enhet: Data ledning och stöd"
										af-size="small"
										af-no-icon="false"
										af-aria-label="ta bort"
									></digi-tag>{' '}
									<digi-tag
										afText="Roll: Kvalificerad handläggare"
										af-size="small"
										af-no-icon="false"
										af-aria-label="ta bort"
									></digi-tag>
									<br />
									<br />
									<digi-navigation-context-menu
										afText="Sortera efter relevans"
										af-start-selected="0"
									>
										<digi-navigation-context-menu-item
											afText="Sorteringsval 1"
											afType={NavigationContextMenuItemType.BUTTON}
										></digi-navigation-context-menu-item>
										<digi-navigation-context-menu-item
											afText="Sorteringsval 2"
											afType={NavigationContextMenuItemType.BUTTON}
										></digi-navigation-context-menu-item>
									</digi-navigation-context-menu>
									<br />
									<p>
										Användare kan snabbt identifiera aktiva sökord genom att visa
										söktaggar, vilket gör det enkelt att anpassa och filtrera sökresultat.
										Sortering möjliggör rangordning av sökresultat baserat till exempel på
										relevans, publiceringsdatum eller alfabetisk ordning.
									</p>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
					</digi-layout-block>
					<digi-layout-block afVerticalPadding af-nogutter afMarginTop>
						<h2>Modulär komponenter</h2>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									Bilden visar vy designad med modulära komponenter för sökfältet och
									tillgängliga sökfilter. En söklösning bör utformas för att passa den
									underliggande datan som ska visas för användaren, och den tänkta
									målgruppens behov.
								</p>
							</div>
							<div>
								<digi-layout-block
									af-variation="symbol"
									afVerticalPadding
									class="digi-docs-about-design-pattern-search-and-searchfilter--poc-card"
								>
									<div class="digi-docs-about-design-pattern-search-and-searchfilter--illu">
										<digi-media-image
											afUnlazy
											af-src="/assets/images/pattern/clicker.png"
											afAlt="Illustration"
										></digi-media-image>
									</div>
									<h3>Test komponenterna i sammanhang </h3>
									<digi-link-external
										afHref="https://multifilter-poc.netlify.app/multifilter-poc"
										afTarget="_blank"
										afVariation={LinkVariation.SMALL}
									>
										Pröva en sök-POC
									</digi-link-external>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<br />
						<div class="digi-docs-about-design-pattern-search-and-searchfilter--image">
							<digi-media-image
								afUnlazy
								af-src="/assets/images/pattern/multifilter-poc.png"
								afAlt="Skärmdup på multifilter POC"
							></digi-media-image>
						</div>
					</digi-layout-block>
					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<h2>Mät, iterera och optimera</h2>
						<p>
							Det är viktigt att samla in och analysera statistik och data i
							sökfunktionen så att man inte bara kan upptäcka och åtgärda problem. Samt
							även justera och förbättra sökfunktionaliteten över tid. Genom att veta
							hur användarna söker och vilka sökfraser som används och analysera denna
							data kan man identifiera mönster och trender som bör användas för att
							förbättra relevansen av sökresultaten.
						</p>
						<h3>Definera målgruppen och användarnas behov</h3>
						<p>
							Det är viktigt att ha en klar förståelse för vilken typ av användare som
							sökmotorn riktar sig till och vilka behov de har. Detta kan hjälpa till
							att identifiera vilka typer av sökresultat som är mest relevanta och
							vilka faktorer som bör prioriteras i en rankingprocess, för att uppnå
							övergripande mål för webbplatsen.
						</p>
						<h3>Testa och justera kontinuerligt</h3>
						<p>
							Det är viktigt att kontinuerligt testa och justera rankingalgoritmer för
							att säkerställa att de ger de mest relevanta sökresultaten för
							användarna. Genom att använda A/B-testning eller andra testmetoder kan
							man mäta effekten av förändringar och optimera rankingalgoritmerna
							ytterligare. Ett exempel på viktiga justeringar är ”synonym-optimering”
							som kan vara en viktig del av en sökmotors rankingalgoritmer, eftersom
							många användare använder olika termer och fraser för att söka efter samma
							sak.
						</p>
						<h3>Prioritera kvalitet över kvantitet</h3>
						<p>
							Det är bättre att visa ett mindre antal sökresultat som är relevanta för
							användaren än att visa ett stort antal sökresultat som inte är relevanta.
							Prioritera därför kvalitet över kvantitet i rankingprocessen.
						</p>
						<br />
						<hr></hr>
						<br />
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<h4> Designmönster</h4>
								<p>Se relaterade designmönster</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/formular"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Formulär
									</digi-link-button>
								</digi-docs-related-links>
							</div>
							<div>
								<h4>Komponenter</h4>
								<p>Se relaterade komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/komponenter/digi-form-category-filter/oversikt"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Kategorifilter komponent
									</digi-link-button>
								</digi-docs-related-links>{' '}
								{''}
								<br />
								<digi-docs-related-links>
									<digi-link-button
										afHref="/komponenter/digi-form-filter/oversikt"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Multifilter komponent
									</digi-link-button>
								</digi-docs-related-links>
								{''}
								<br />
								<digi-docs-related-links>
									<digi-link-button
										afHref="/komponenter/digi-form-input-search/oversikt"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Sökfält komponent
									</digi-link-button>
								</digi-docs-related-links>
								{''}
								<br />
								<digi-docs-related-links>
									<digi-link-button
										afHref="/komponenter/digi-tag/oversikt"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Tagg komponent
									</digi-link-button>
								</digi-docs-related-links>
							</div>
							<br />
						</digi-layout-columns>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
