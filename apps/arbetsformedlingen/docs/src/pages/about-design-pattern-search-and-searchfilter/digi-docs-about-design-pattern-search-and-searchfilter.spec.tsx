import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternSearchAndSearchfilter } from './digi-docs-about-design-pattern-search-and-searchfilter';

describe('digi-docs-about-design-pattern-search-and-searchfilter', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternSearchAndSearchfilter],
			html:
				'<digi-docs-about-design-pattern-search-and-searchfilter></digi-docs-about-design-pattern-search-and-searchfilter>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-search-and-searchfilter>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-search-and-searchfilter>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternSearchAndSearchfilter],
			html: `<digi-docs-about-design-pattern-search-and-searchfilter first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-search-and-searchfilter>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-search-and-searchfilter first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-search-and-searchfilter>
    `);
	});
});
