import { newSpecPage } from '@stencil/core/testing';
import { DesignTokensDescription } from './digi-docs-design-tokens-description';

describe('digi-docs-design-tokens-description', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DesignTokensDescription],
			html:
				'<digi-docs-design-tokens-description></digi-docs-design-tokens-description>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-design-tokens-description>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-design-tokens-description>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DesignTokensDescription],
			html: `<digi-docs-design-tokens-description first="Stencil" last="'Don't call me a framework' JS"></digi-docs-design-tokens-description>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-design-tokens-description first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-design-tokens-description>
    `);
	});
});
