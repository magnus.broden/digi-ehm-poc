import { Component, getAssetPath, h, Host, State } from '@stencil/core';
import { router } from '../../global/router';
import state from '../../store/store';

@Component({
	tag: 'digi-docs-design-pattern',
	styleUrl: 'digi-docs-design-pattern.scss',
	scoped: true
})
export class DesignPattern {
	Router = router;
	@State() pageName = 'Designmönster';

	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<Host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/design-pattern/digi-docs-design-pattern.tsx"
				>
					<span slot="preamble">
						För att lösa vanliga designproblem gör vi kontinuerligt nya designmönster.
						I varje designmönster finns återanvändbara kombinationer av komponenter
						och beteenden. Designmönster och komponenter gör det snabbt att bygga nya
						produkter och ger våra användare en känsla av igenkänning och trygghet i
						alla Arbetsförmedlingens tjänster.
					</span>

					<digi-layout-block af-variation="secondary" af-no-gutter afMarginTop>
						<digi-layout-columns
							af-variation={state.responsive321Columns}
							class="digi-docs-design-pattern__columns"
						>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/grids-pattern.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Grid och brytpunkter</h2>
								<p>
									Här beskriver vi brytpunkter och responsiv bakgrundsgrid som vi
									rekommenderar att använda för att få ett homogent uttryck på våra
									webbar och tjänster.
								</p>
								<digi-link-internal
									afHref="/designmonster/grid-och-brytpunkter"
									af-variation="small"
								>
									Grid och brytpunkter
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/spacing-pattern.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Spacing</h2>
								<p>
									Spacing är en av Designsystemets grundstenar för att enkelt att sätta
									harmoniska och förutsägbara avstånd mellan komponenter och andra objekt
									i en design.
								</p>
								<digi-link-internal
									afHref="/designmonster/spacing"
									af-variation="small"
									onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Spacing
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/button-pattern.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Knappar</h2>
								<p>
									Knappar är klickbara element som används för att handlingar eller
									exekvera en funktion. De tillåter användare att interagera med sidor på
									en mängd olika sätt. Vi har olika varianter på knapparna och olika
									storlekar.
								</p>
								<digi-link-internal
									afHref="/designmonster/knappar"
									af-variation="small"
									onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Knappar
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/patterns-in-forms.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Formulär</h2>
								<p>
									Formulär är en grupp komponenter som ger användaren möjlighet att
									skicka in information till oss. För att få den bästa
									användarupplevelsen är det viktigt att de arrangeras konsekvent.
								</p>
								<digi-link-internal
									afHref="/designmonster/formular"
									af-variation="small"
									onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Formulär
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/validation-pattern.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Validering</h2>
								<p>
									Validering i formulärfält identifierar och hjälper till att korrigera
									eventuella hinder för användarens inmatning av data.
								</p>
								<digi-link-internal
									afHref="/designmonster/validering"
									af-variation="small"
									onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Validering
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/search-and-search-filter-puff.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Sök och sökfilter</h2>
								<p>
									Sök och filtrering ger ett användbart och effektivt sätt att söka och
									filtrera data, vilket förbättrar användarupplevelsen och möjliggör
									exakt informationssökning.
								</p>
								<digi-link-internal
									afHref="/designmonster/sok-och-sokfilter"
									af-variation="small"
									onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Sök och sökfilter
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/feedback-pattern.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Enkäter och feedback</h2>
								<p>
									Feedback ger insiktern i användarnas behov och preferenser och ökar
									engagemang och lojalitet till din tjänst. Genom att samla in feedback
									från användarna kan du tidigt identifiera förbättringsområden i en
									utvecklingsprocess.
								</p>
								<digi-link-internal
									afHref="/designmonster/enkater-och-feedback"
									af-variation="small"
									onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Enkäter och feedback
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/felmed-pattern.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Felmeddelandesidor</h2>
								<p>
									Felmeddelandesidor är ett viktigt verktyg för att hantera tekniska
									problem. Genom användarvänliga budskap kan vi minska användarnas
									frustration och förbättra deras övergripande upplevelse av vår
									webbplats.
								</p>
								<digi-link-internal
									afHref="/designmonster/felmeddelandesidor"
									af-variation="small"
								>
									Felmeddelandesidor
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/design-pattern-cards/pictogram-pattern.svg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Piktogram</h2>
								<p>
									Piktogram är ett internt visuellt hjälpmedel som skapar en koppling
									mellan kundernas aktiviteter och våra interna processer. Detta främjar
									gemensam förståelse och stärker samarbetet inom organisationen.
								</p>
								<digi-link-internal
									afHref="/designmonster/pictogram"
									af-variation="small"
								>
									Piktogram
								</digi-link-internal>
							</div>
							<div class="digi-docs-design-pattern__block">
								<digi-media-image
									afUnlazy
									afHeight="275"
									afWidth="455"
									afSrc={getAssetPath(
										'/assets/images/pattern/agentive-services/puff-pict-human-ai@2x.jpg'
									)}
									afAlt="..."
								></digi-media-image>
								<h2>Agentiva tjänster och AI</h2>
								<p>
									Agentiva digitala tjänster anpassar sig till användarens behov och
									utför arbete med AI, men bör utformas för mänsklig kontroll, vara
									transparenta om data och beslut, ge användaren val och feedback.
								</p>
								<digi-link-internal
									afHref="/designmonster/agentiva-tjanster"
									af-variation="small"
									onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Agentiva tjänster och AI
								</digi-link-internal>
							</div>
						</digi-layout-columns>
					</digi-layout-block>
				</digi-docs-page-layout>
			</Host>
		);
	}
}
