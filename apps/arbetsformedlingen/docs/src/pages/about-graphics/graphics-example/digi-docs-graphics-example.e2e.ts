import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-graphics-example', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-docs-graphics-example></digi-docs-graphics-example>'
		);
		const element = await page.find('digi-docs-graphics-example');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-docs-graphics-example></digi-docs-graphics-example>'
		);
		const component = await page.find('digi-docs-graphics-example');
		const element = await page.find('digi-docs-graphics-example >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
