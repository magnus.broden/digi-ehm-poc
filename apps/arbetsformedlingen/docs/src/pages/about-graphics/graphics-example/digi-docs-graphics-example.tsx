import { Component, getAssetPath, h, Host, Prop } from '@stencil/core';
import { GraphicsExampleType } from './graphics-example-type.enum';


@Component({
	tag: 'digi-docs-graphics-example',
	styleUrl: 'digi-docs-graphics-example.scss',
	scoped: true,
})
export class GraphicsExample {

	@Prop() exampleType: GraphicsExampleType = GraphicsExampleType.GOOD;

	get exampleIcon() {
		return (
			this.exampleType === GraphicsExampleType.GOOD ?
				<img src={getAssetPath('/assets/images/graphics-example/example-icon-good.svg')} /> :
				<img src={getAssetPath('/assets/images/graphics-example/example-icon-bad.svg')} />
		);
	}

	get cssModifiers() {
    return {
      'digi-docs-graphics-example--good': this.exampleType === GraphicsExampleType.GOOD,
      'digi-docs-graphics-example--bad': this.exampleType === GraphicsExampleType.BAD,
    };
  }

	render() {
		return (
			<Host class={{
				"digi-docs-graphics-example": true,
				...this.cssModifiers
			}}>
				<div class="digi-docs-graphics-example__content">
					<div class="digi-docs-graphics-example__icon">
						{ this.exampleIcon }
					</div>
					<slot></slot>
				</div>
				<div class="digi-docs-graphics-example__footer">
					<slot name="footer"></slot>
				</div>
			</Host>
		);
	}
}
