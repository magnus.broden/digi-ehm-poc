import { newSpecPage } from '@stencil/core/testing';
import { AboutGraphics } from './digi-docs-about-graphics';

describe('digi-docs-about-graphics', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutGraphics],
			html: '<digi-docs-about-graphics></digi-docs-about-graphics>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-graphics>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-graphics>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutGraphics],
			html: `<digi-docs-about-graphics first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-graphics>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-graphics first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-graphics>
    `);
	});
});
