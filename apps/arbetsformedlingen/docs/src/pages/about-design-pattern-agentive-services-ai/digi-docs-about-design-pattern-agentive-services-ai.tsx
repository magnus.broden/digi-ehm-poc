import { Component, h } from '@stencil/core';
import { router } from '../../global/router';
import { InfoCardVariation, InfoCardType, InfoCardHeadingLevel } from '@digi/arbetsformedlingen';
@Component({
	tag: 'digi-docs-about-design-pattern-agentive-services-ai',
	styleUrl: 'digi-docs-about-design-pattern-agentive-services-ai.scss',
	scoped: true
})
export class AboutDesignPatternAgentive {
	Router = router;
	//@State() pageName = 'Agentiva tjänster och artificiell intelligens';

	linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

	render() {
		return (
			<host>
				<digi-docs-page-layout
					//af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-forms/digi-docs-about-design-pattern-agentive-services-ai.tsx"
				>
					<digi-pattern-agentive-intro></digi-pattern-agentive-intro>

					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
								<h2>
									Exempel på agentiva tjänster och dess potential för offentliga aktörer
								</h2>
								<p>
									Ett exempel på en agentiv tjänst är en personlig assistent som använder
									maskininlärning för att lära sig användares beteenden och vanor för att
									sedan ta över hela eller delar av arbetet. Assistenter som Siri från
									Apple, Google Assistant och Amazon Alexa kan svara på användarens
									frågor, ge personliga rekommendationer och hjälpa till att utföra
									uppgifter som att boka möten eller beställa mat. Genom att använda
									artificiell intelligens kan assistenten lära sig användarens
									preferenser och ge mer relevanta svar och rekommendationer över tid.
								</p>
								<h2>
									Rekryteringstjänster med agentteknologi kräver mänsklig kontroll
								</h2>
								<p>
									Här finns stor potential hos offentliga aktörer som Arbetsförmedlingen
									att exempelvis utforma tjänster som agerar som rekryteringspartner
									(head-hunter). Men det är viktigt att dessa tjänster utformas med
									mänsklig kontroll i åtanke. Det innebär att den digitala produkten ska
									utformas på ett sätt som ger användaren en känsla av kontroll och
									förståelse över hur tjänsten fungerar och hur den påverkar användaren.
								</p>
						<br />
						<h2>Rekommendationer för dig som designar agentiva tjänster </h2>
						<p>
							<digi-list>
								<li>
									Förklara syftet och omfattningen av den agentiva tjänsten för
									användaren på ett tydligt sätt. Använd enkla och lättförståeliga termer
									och undvik tekniskt jargong.
								</li>
								<li>
									Ge användaren möjlighet att välja om tjänsten ska aktiveras eller
									avaktiveras. Användaren bör ha fullständig kontroll över när och hur
									tjänsten används och möjlighet att konfigurera olika delar av detta.
								</li>
								<li>
									Visa och ge användaren möjlighet att konfigurera tjänsten efter
									personliga preferenser.
								</li>
								<li>
									Visa tydligt vilka data som samlas in och hur de används av tjänsten.
									Användaren bör ha möjlighet att se och redigera den data som samlas in
									om hen så önskar och ha möjlighet att välja bort olika dataströmmar.
								</li>
								<li>
									Var transparent om hur beslut tas av tjänsten och vilka faktorer som
									påverkar dessa beslut. Användaren bör ha möjlighet att förstå och
									granska de beslut som tas av tjänsten.
								</li>
								<li>
									Ge användaren möjlighet att ge feedback och påverka hur tjänsten
									fungerar. Användaren bör ha möjlighet att påverka och förbättra
									tjänsten genom att ge feedback och förslag.
								</li>
							</digi-list>
							Genom att följa rekommendationerna kan man skapa digitala produkter som
							är mer transparenta och lättförståeliga för användarna och som ger dem en
							känsla av kontroll över agentiva tjänster.
						</p>
					</digi-layout-block>
					<digi-layout-block
						af-variation="primary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
								<h2>Agentiva tjänster i förhållande till människa och AI</h2>
								<p>
									Agentiva tjänster utvecklas i många branscher i en rasande takt och är
									en interaktionsmodell och ett sätt att använda teknologi för att
									förstärka den mänskliga förmågan att fatta bra beslut. Antingen i nya,
									eller redan existerande tjänster. För att tjänsterna vi designar ska
									vara rättvisa, inkluderande och tillförlitliga så måste de vara möjliga
									att inspektera på ett vettigt sätt. Detta kräver också en ny typ av
									designförmåga, liksom ett etiskt förhållningssätt till hur vi samlar in
									och använder data.
								</p>
						<digi-layout-block af-variation="primary" afVerticalPadding class="graph">
							<digi-media-image
								afUnlazy
								afSrc="/assets/images/pattern/agentive-services/graph-ai-to-people.svg"
								afAlt="Graf som beskriviver olika krav på autonomi, adaptivitet och interaktivitet beroende på en skala mellan automatiserat och inget stöd i tjänster"
							></digi-media-image>
						</digi-layout-block>
						<br />
						<digi-info-card
							afHeading="Återkommande begrepp"
							afHeadingLevel={InfoCardHeadingLevel.H3}
							afType={InfoCardType.TIP}
							afVariation={InfoCardVariation.SECONDARY}
						>
							<digi-list>
								<li>
									Autonomi - förmåga att uträtta uppgifter i komplexa miljöer utan
									ständig styrning av användaren.
								</li>
								<li>
									Adaptivitet - kapaciteten att förbättra sin prestationsförmåga genom
									att lära sig av erfarenheter.
								</li>
								<li>
									Interaktivitet - att agenten har möjlighet att uppfatta och interagera
									med andra agenter (artificiella såväl med människor) och att det finns
									gränssnitt för människor att interagera med och påverka agenten.
								</li>
							</digi-list>
						</digi-info-card>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
