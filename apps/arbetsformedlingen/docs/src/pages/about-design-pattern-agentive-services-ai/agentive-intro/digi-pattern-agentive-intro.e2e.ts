import { newE2EPage } from '@stencil/core/testing';

describe('digi-pattern-agentive-intro', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<digi-pattern-agentive-intro></digi-pattern-agentive-intro>');
		const element = await page.find('digi-pattern-agentive-intro');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<digi-pattern-agentive-intro></digi-pattern-agentive-intro>');
		const component = await page.find('digi-pattern-agentive-intro');
		const element = await page.find('digi-pattern-agentive-intro >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
