import { Component, h, getAssetPath } from '@stencil/core';

import state from '../../../src/store/store';

import { router } from '../../global/router';

import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-graphics',
  styleUrl: 'digi-docs-graphics.scss',
  scoped: true
})
export class DigiDocsGraphics {
  Router = router;

  getTabId(): number {
    const tabName:string = state.router.activePath;

    switch (tabName) {
      case '/grafisk-profil/grafik/':
        return 0;
      case '/grafisk-profil/grafik/funktionsikoner':
        return 1;
      default:
        return 0;
    }
  }

  toggleNavTabHandler(e) {
    const tag = e.target.dataset.tag;
    this.Router.push(`${state.routerRoot}grafisk-profil/grafik/${tag}`);
  }
  
  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-edit-href="pages/digi-docs-graphics/digi-docs-graphics.tsx"
        >
          {/* <digi-navigation-tabs
              af-aria-label="Categories"
              af-init-active-tab={this.getTabId()}
            >
              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="oversikt"
                afAriaLabel="Översikt"
              ></digi-navigation-tab>

              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="funktionsikoner"
                afAriaLabel="Funktionsikoner"
              ></digi-navigation-tab>
            </digi-navigation-tabs>
            <stencil-router id="graphicsRouter" root={state.routerRoot}>
              <stencil-route-switch>
                <stencil-route
                  url="/grafisk-profil/grafik/oversikt"
                  component="digi-docs-graphics-overview"
                  exact={true}
                />
                <stencil-route
                  url="/grafisk-profil/grafik/funktionsikoner"
                  component="digi-docs-graphics-function-icons"
                  exact={true}
                />
              </stencil-route-switch>
            </stencil-router> */}
          <digi-typography>
            <digi-layout-container>
              <digi-typography-heading-jumbo af-text="Grafik"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Vi använder grafik för att förstärka, kommunicera och beskriva.
                Utöver den unika grafik som görs för de olika tjänsterna så har
                vi egen infografik, ikonspråk och diagram som knyter allt samman
                och bygger Arbetsförmedlingens varumärke.
              </digi-typography-preamble>
            </digi-layout-container>
            <br />
            <digi-layout-container>
              <p>
                Huvudfärg för grafikkomponter är densamma som textfärgen eller
                profilblått. För bakgrunder och utfyllnad i grafiken använder vi
                i första hand våra profilfärger i dess ursprungsnyans. När det
                inte passar och grafiken tar för mycket fokus använder vi våra
                ljusare nyanser av profilfärgerna och i andra hans ljusare
                nyanser av komplementfärgerna.
              </p>
              <h2>Infografik</h2>
              <p>
                Vi har ett stort bibliotek med infografik framtagen för att
                beskriva eller förstärka. Det kan vara uppmaningar,
                upplysningar, flödesbeskrivningar, situationer, aktivitet och
                föremål.
              </p>
              <p>
                Saknar du någon typ av infografik finns möjlighet att göra en
                beställning på att ta fram ny grafik. Anpassat efter era behov.
                Kontakta kommunikationsavdelningen för att göra en beställning.
              </p>
              <p>
                <digi-link
                  af-variation="small"
                  afHref={`mailto:kommunikationsavdelningen@arbetsformedlingen.se`}
                >
                  <digi-icon-envelope
                    slot="icon"
                    aria-hidden="true"
                  ></digi-icon-envelope>
                  Kontakta kommunikationsavdelningen
                </digi-link>
              </p>
              <div class="digi-docs__graphics-infographics">
                <digi-media-image
                  afUnlazy
                  afSrc={getAssetPath("/assets/images/infographics.png")}
                  afAlt="Exempel på infografik"
                ></digi-media-image>
              </div>
              <br />
              <h2>Diagram</h2>
              <p>
                Vi använder flera typer av diagram. De vi använder främst är
                stapeldiagram och cirkeldiagram. När du producerar diagram
                oavsett om det är för webb eller tryck är det viktigt att du
                tänker på att de är lätta att läsa av. Kontrasten mellan text,
                bakgrund och grafik ska följa de tillgänglighetskrav vi har.
                Exempel för att öka kontrasten är att ge staplarna eller
                tårtbitarna en kantlinje eller att använda sig av mörk bakgrund.
                För att underlätta för färgblinda använd ett mönster i
                fyllnadsfärgen då det är möjligt. Tänk ockspå på att ett diagram
                även skall kunna läsas av med en skärmläsare. Då kan en tabell
                eller liknande vara en lösning.
              </p>
              <div class="digi-docs__graphics-diagrams">
                <digi-media-image
                  afUnlazy
                  class="digi-docs__graphics-diagrams"
                  afSrc={getAssetPath("/assets/images/diagrams.png")}
                  afAlt="Exempel på diagram enligt Arbetsförmedlingens grafiska"
                ></digi-media-image>
              </div>
              <h2>Funktionsikoner </h2>
              <p>
                Vi använder i största möjliga utsträckning neutrala och
                traditionella ikoner från ikonbiblioteket Font Awesome. Vi
                använder ett eget urval som representerar de behov som vi har
                inom Arbetsförmedlingen. Biblioteket fylls kontinuerligt på och
                saknar du en viss ikon så kontakta oss via funktionsbrevlådan så
                hjälper vi till att lägga upp den i systemet.
              </p>
              <digi-link
                af-variation="small"
                afHref="mailto:designsystem@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan"
              >
                <digi-icon-envelope slot="icon"></digi-icon-envelope>
                Mejla designsystem@arbetsformedlingen.se
              </digi-link>
              <br />
              <br />
            </digi-layout-container>
            <digi-layout-block afVariation={LayoutBlockVariation.SYMBOL}>
              <br />
              <h3>Riktlinjer</h3>
              <digi-list>
                <li>
                  {' '}
                  Använd endast den grafik, ikoner och diagram som finns i våra
                  resursbibliotek. Eftersom de uppfyller kraven vi har på
                  tillgänglighet samt följer Arbetsförmedlingens
                  varumärkesriktlinjer.
                </li>
                <li>Använd dig endast av Arbetsförmedlingens profilfärger. </li>
                <li>
                  All typ av grafik används primärt ihop med en förklarande text
                  eller rubrik. I de fall det inte är möjligt är det viktigt att
                  det finns en alt-text eller div-tag som förklarar grafikens
                  innehåll.
                </li>
              </digi-list>
              <br />
            </digi-layout-block>
            <br />
            <digi-layout-block afVariation={LayoutBlockVariation.SECONDARY}>
              <br />
              <h3>…då blir det fel</h3>
              <digi-list>
                <li>
                  Du får inte ändra eller modifiera en ikon eller
                  infografikkomponent som sedan kan misstolkas eller blandas
                  ihop med annan.{' '}
                </li>
                <li>
                  Valet av ikon skall vara starkt visuellt kopplat till dess
                  funktion. Det är exempelvis inte lämpligt att ha en ikon med
                  en penna för att symbolisera en länk.
                </li>
                <li>
                  Funktionsikonerna är att betrakta som en visuell förstärkare
                  och komplement i tex. knappar eller menyer. De skall inte
                  användas som utsmyckade illustration.{' '}
                </li>
              </digi-list>
              <br />
            </digi-layout-block>
          </digi-typography>
        </digi-docs-page-layout>
      </host>
    );
  }
}
