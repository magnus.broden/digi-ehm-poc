import { Component, h, Prop } from '@stencil/core';
import Helmet from '@stencil/helmet';

import {
	CodeLanguage,
	CodeBlockLanguage,
	CodeVariation
} from '@digi/arbetsformedlingen';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'digi-docs-work-with-digi-core-angular-legacy',
	styleUrl: 'digi-docs-work-with-digi-core-angular-legacy.scss',
	scoped: true
})
export class WorkWithDigiCoreAngularLegacy {
	@Prop() pageName = '@digi/core-angular@17.x.x';

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/work-with-digi-core-angular-legacy/digi-docs-work-with-digi-core-angular-legacy.tsx"
				>
					<Helmet>
						<meta
							property="rek:pubdate"
							content="Thu Jan 12 2023 14:58:00 GMT+0100 (centraleuropeisk normaltid)"
						/>
					</Helmet>
					<span slot="preamble">
            Det här är en guide för att installera Digi Core Angular för Angular version 13. Det kommer inte ske någon nyutveckling för den här versionen
          </span>
          <digi-layout-container af-margin-top af-margin-bottom>
            <article>
              <p>Använder ni Angular version 14 eller nyare, följ den här guiden istället: <a {...href('/kom-i-gang/jobba-med-digi-core/digi-core-angular')}>Digi Core Angular</a></p>
              <h2>Installera Digi Core Angular (legacy)</h2>
              <h3>NPM</h3>
              <p>
                Börja med att konfigurera Nexus, om ditt projekt inte redan
                gjort det. Lägg till en fil som heter
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode=".npmrc"
                ></digi-code>{' '}
                i samma mapp som din
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode="package.json"
                ></digi-code>
                och lägg till den här raden:
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  afCode="registry=http://nexus.ams.se:8081/nexus/content/groups/npm-releases"
                ></digi-code>
              </p>
              <p>
                Installera Digi Core Angular genom att köra
                <br />
                <digi-code
                  afLanguage={CodeLanguage.BASH}
                  afVariation={CodeVariation.LIGHT}
                  afCode="npm i @digi/core-angular @digi/core @digi/styles @digi/fonts"
                />
                <br />i roten av ditt angular-projekt.
                <br /><br />
                <em>Core Angular kör på version 13 av Angular och kan skapa
                dependency-problem om du kör en äldre version av Angular än 12 i ditt
                projekt!</em>
              </p>
              <h3>Lägg till modulen</h3>
              <p>
                För att få med core-komponenterna i angular-appen så behöver du
                importera modulen{' '}
                <digi-code
                  afLanguage={CodeLanguage.TYPESCRIPT}
                  afVariation={CodeVariation.LIGHT}
                  afCode="DigiCoreAngularModule"
                />
                . Här följer ett exempel på filen{' '}
                <digi-code
                  afLanguage={CodeLanguage.TYPESCRIPT}
                  afVariation={CodeVariation.LIGHT}
                  afCode="app.module.ts"
                />
                :
              </p>
              <digi-code-block
                afLanguage={CodeBlockLanguage.TYPESCRIPT}
                afCode={`import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { DigiCoreAngularModule } from '@digi/core-angular';
import { MyPageComponent } from './pages/my-page/my-page.component'

@NgModule({
  declarations: [AppComponent, MyPageComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: "my-page", component: MyPageComponent}
    ], { initialNavigation: 'enabledBlocking' }),
    ReactiveFormsModule,
    DigiCoreAngularModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}`}
              ></digi-code-block>
              <h3>Lägg till styles</h3>
              <p>
                Du behöver också lägga till styles i din app för att få rätt
                utseende. Detta gör du genom att lägga till något av följande i
                din{' '}
                <digi-code
                  afLanguage={CodeLanguage.TYPESCRIPT}
                  afVariation={CodeVariation.LIGHT}
                  afCode="styles.scss"
                />
                :<br />
                <digi-code
                  afLanguage={CodeLanguage.SCSS}
                  afVariation={CodeVariation.LIGHT}
                  afCode="@import '@digi/core/dist/digi/digi.css'"
                />{' '}
                som CSS från Digi Core, eller
                <digi-code
                  afLanguage={CodeLanguage.SCSS}
                  afVariation={CodeVariation.LIGHT}
                  afCode="@import '@digi/styles/src/digi-styles.scss'"
                />{' '}
                som SCSS från Digi Styles.
              </p>
              <h3>Lägg till typsnitt</h3>
              <p>
                För att läsa in typsnitten behöver man läsa in CSS/SCSS-filen i t.ex. din globala
                SCSS-fil:
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  af-code="@import '@digi/fonts/src/fonts.scss';"
                />
              </p>
              <p>
                Sen behöver man inkludera filerna i sin applikation. I Angular
                kan man göra det genom att inkludera dem under
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  af-code="assets"
                />{' '}
                i filen{' '}
                <digi-code
                  afVariation={CodeVariation.LIGHT}
                  af-code="angular.json"
                />
              </p>
              <digi-code-block
                  afLanguage={CodeBlockLanguage.JSON}
                  afCode={`\
"assets": [
  "src/favicon.ico",
  "src/assets",
  {
    "glob": "**/*",
    "input": "./node_modules/@digi/fonts/src/assets/fonts",
    "output": "/assets/fonts"
  }
],`}
                ></digi-code-block>
              <br />
              <p>
                I andra plattformar kan man kopiera över dessa t.ex. med
                webpack eller liknande teknik.
              </p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Använda Digi Core Angular</h2>
              <h3>Använda enums</h3>
              <p>
                Du finner alla enums under Digi Core och kan referera dom såhär:
                <br />
                <digi-code
                  afLanguage={CodeLanguage.TYPESCRIPT}
                  afVariation={CodeVariation.LIGHT}
                  afCode="import { EnumName1, EnumName2 } from '@digi/core';"
                />
              </p>
              <h3>Reactive Forms</h3>
              <p>
                Du behöver inte tänka på något särskilt när det kommer till
                Reactive Forms och Digi Core Angular. Komponenterna använder
                value och checked för input och via en ValueAccessor i Digi Core
                Angular så lyssnar även Reactive Forms korrekt på komponenternas
                change-event. Här följer ett exempel på ett enkelt formulär som
                använder Digi Core Angular:
              </p>
              <digi-navigation-tabs afAriaLabel="Formulär-exempel">
                <digi-navigation-tab
                  afAriaLabel="my-form.component.html"
                  afId="my-form-html"
                >
                  <digi-code-block
                    afLanguage={CodeBlockLanguage.HTML}
                    afCode={`<form [formGroup]="myForm" class="my-form">
  <digi-form-input
    afId="myEmail"
    afLabel="Epost-adress"
    [afType]="FormInputType.EMAIL"
    afRequired="true"
    afAutocomplete="on"
    formControlName="myEmail"
  ></digi-form-input>

  <digi-form-input
    afId="myAge"
    afLabel="Min ålder"
    [afType]="FormInputType.NUMBER"
    afRequired="true"
    afAutocomplete="on"
    formControlName="myAge"
  ></digi-form-input>

  <digi-form-textarea
    afId="myDescription"
    afLabel="Beskrivning"
    afRequired="true"
    formControlName="myDescription"
  ></digi-form-textarea>

  <digi-form-select
    afId="myJob"
    afLabel="Yrke"
    afRequired="true"
    formControlName="myJob"
  >
    <option name="Välj yrke" value="">Välj yrke</option>
    <option name="handlaggare" value="handlaggare">Handläggare</option>
    <option name="konsult" value="konsult">Konsult</option>
  </digi-form-select>

  <digi-form-fieldset afLegend="Svara på frågan" afId="myChoice">
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="Yes"
      afId="Yes"
      afValue="Yes"
    ></digi-form-radiobutton>
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="Maybe"
      afId="Maybe"
      afValue="Maybe"
    ></digi-form-radiobutton>
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="No"
      afId="No"
      afValue="No"
    ></digi-form-radiobutton>
  </digi-form-fieldset>

  <br />

  <digi-form-fieldset afLegend="Klicka i" afId="myBox" formArrayName="myBox">
    <digi-form-checkbox
      formControlName="0"
      afName="myBox"
      afLabel="Val 1"
      afId="val-1"
    ></digi-form-checkbox>
    <digi-form-checkbox
      formControlName="1"
      afName="myBox"
      afLabel="Val 2"
      afId="val-2"
    ></digi-form-checkbox>
    <digi-form-checkbox
      formControlName="2"
      afName="myBox"
      afLabel="Val 3"
      afId="val-3"
    ></digi-form-checkbox>
  </digi-form-fieldset>

  <br />

  <digi-button (afOnClick)="clickButtonHandler()">Skicka</digi-button>
</form>`}
                  ></digi-code-block>
                </digi-navigation-tab>
                <digi-navigation-tab
                  afAriaLabel="my-form.component.ts"
                  afId="my-form-ts"
                >
                  <digi-code-block
                    afLanguage={CodeBlockLanguage.TYPESCRIPT}
                    afCode={`import { Component, HostListener, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { FormInputValidation, FormInputType } from '@digi/core';

@Component({
  selector: 'my-form',
  templateUrl: './my-form.component.html',
  styleUrls: ['./my-form.component.scss']
})
export class MyFormComponent implements OnInit {

  FormInputValidation = FormInputValidation;
  FormInputType = FormInputType;

  myForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.myForm = this.fb.group({});
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      myEmail: ['', [Validators.email, Validators.required]], 
      myAge: [null, [Validators.required]], 
      myDescription: ['', [Validators.required, Validators.maxLength(60)]],
      myJob: ['', Validators.required],
      myChoice: [null, Validators.required],
      myBox: new FormArray([
        new FormControl(false),
        new FormControl(false),
        new FormControl(false)
      ])
    });
  }

  clickButtonHandler() {
    console.log(this.myForm.value);
    alert('click!');
  }
}`}
                  ></digi-code-block>
                </digi-navigation-tab>
              </digi-navigation-tabs>
            </article>
          </digi-layout-container>
				</digi-docs-page-layout>
			</host>
		);
	}
}
