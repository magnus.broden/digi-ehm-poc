import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternDataVisualization } from './digi-docs-about-design-pattern-data-visualization';

describe('digi-docs-about-design-pattern-data-visualization', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternDataVisualization],
			html:
				'<digi-docs-about-design-pattern-data-visualization></digi-docs-about-design-pattern-data-visualization>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-data-visualization>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-data-visualization>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternDataVisualization],
			html: `<digi-docs-about-design-pattern-data-visualization first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-data-visualization>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-data-visualization first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-data-visualization>
    `);
	});
});
