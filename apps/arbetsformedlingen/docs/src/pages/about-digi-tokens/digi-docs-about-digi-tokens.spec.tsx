import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutDigiTokens } from './digi-docs-about-digi-tokens';

describe('digi-docs-about-digi-tokens', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutDigiTokens],
      html: '<digi-docs-about-digi-tokens></digi-docs-about-digi-tokens>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-digi-tokens>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-digi-tokens>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutDigiTokens],
      html: `<digi-docs-about-digi-tokens first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-digi-tokens>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-digi-tokens first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-digi-tokens>
    `);
  });
});
