import { Component, h, State } from '@stencil/core';
import { CodeBlockLanguage } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-docs-about-digi-tokens',
	styleUrl: 'digi-docs-about-digi-tokens.scss',
	scoped: true
})
export class DigiDocsAboutDigiTokens {
	@State() pageName = 'Digi Tokens';

	render() {
		return (
			<digi-typography>
				<div class="digi-docs-about-digi-tokens">
					<digi-docs-page-layout
						af-page-heading={this.pageName}
						af-edit-href="pages/digi-docs-about-digi-tokens/digi-docs-about-digi-tokens.tsx"
					>
            <span slot="preamble">
              Design tokens representerar alla designbeslut och är basen till all
              design i designsystemet. Det kan vara allt från färger, typografi,
              kantlinjer, avstånd med mera. Dessa skapas centralt och uppdateras från
              ett och samma ställe. Digi Core erbjuder tre nivåer av tokens: globala
              tokens, alias-tokens och komponent-tokens.
            </span>
						<digi-layout-block afMarginTop afMarginBottom>
							<p>
								Design tokens knyter ihop designbeslut som annars skulle sakna en tydlig
								relation. Om en designers skiss och en utvecklares implementation båda
								refererar till samma designtoken så kan man känna sig trygg i att t.ex.
								samma färg används på båda ställena. Skulle värdet på färgen ändras så
								kan man också känna sig trygg i att det uppdateras för båda. Man skulle
								kunna säga att det är ett språk för designbeslut och att vi alla kan
								prata samma språk oavsett roll.
							</p>
							<h2>Globala tokens</h2>
							<p>
								Genom att använda CSS-filen{' '}
								<digi-code afCode="@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css"></digi-code> får
								du tillgång till en handfull anpassade CSS-egenskaper på :root-scope.
								Dessa följer denna namngivningskonvention:{' '}
								<digi-code afCode="--digi--global--category--token-name--variation"></digi-code>
								<br />
								<br />
								De flesta tokens kan grupperas i en av följande kategorier:
							</p>
								<digi-list>
									<li>Animation</li>
									<li>Border</li>
									<li>Color</li>
									<li>Layout</li>
									<li>Typography</li>
								</digi-list>
								<p>
									Exempelvis ser några grundläggande tokens för teckenstorlek ut så här:
									<br />
									<br />
									<digi-code af-code="--digi--global--typography--font-size--base"></digi-code>{' '}
									<br />
									<digi-code af-code="--digi--global--typography--font-size--small"></digi-code>{' '}
									<br />
									<digi-code af-code="--digi--global--typography--font-size--large"></digi-code>{' '}
									<br />
									<br />
									Och färger kan se ut så här:
									<br />
									<br />
									<digi-code afCode="--digi--global--color--profile--white--base"></digi-code>
									<br />
									<digi-code afCode="--digi--global--color--function--info--base"></digi-code>
									<br />
									<digi-code afCode="--digi--global--color--neutral--grayscale--base"></digi-code>
									<br />
									<br />
									Dessa variabler är alltid tillgängliga att använda eftersom de är en del
									av :root (det vill säga på HTML-elementet).
							</p>

							<h2>Alias tokens</h2>
							<p>
								Som namnet antyder så är dessa tokens ett alias för de globala tokens
								som finns. Dessa hjälper till med att förtydliga sammanhang och
								intention. Alla globala tokens är unika men alias-tokens kan ha samma
								värde - till exempel så kan vit färg användas både som inverterad
								textfärg eller som primär bakgrundsfärg. Dessa tokens hänvisar då till
								namnet på det globala token som används, inte till dess värde direkt.{' '}
								<br />
								<br />
								<digi-code af-code="--digi--color--background--primary: var(--digi--global--color--profile--white--base)"></digi-code>{' '}
								<br />
								<digi-code af-code="--digi--color--text--inverted: var(--digi--global--color--profile--white--base)"></digi-code>{' '}
								<br />
							</p>

							<h2>Tokens för komponenter</h2>
							<p>
								Varje enskild komponent har vanligtvis också en uppsättning av
								komponentspecifika tokens. Dessa följer denna namnkonvention:
								<br />
								<br />
								<digi-code afCode="--component--name--token-name--variation"></digi-code>
								<br />
								<br />
								Dessa är inte tillgängliga globalt, men definieras i själva komponenten
								(och finns därför inte i dokumentet förrän komponenten används). Dessa
								bör i de flesta fall baseras på alias tokens.
								<br />
								<br />
								Till exempel ser några tokens från digi-knappen ut så här:
								<br />
								<br />
								<digi-code afCode="--digi--button--color--text--primary--default: var(--digi--color--text--inverted)"></digi-code>
								<br />
								<digi-code afCode="--digi--button--color--background--primary--default: var(--digi--color--background--inverted-1)"></digi-code>
								<br />
								<br />
								Se varje komponents individuella dokumentation för att se vilka
								variabler som är tillgängliga.
							</p>

							<h2>Använda tokens</h2>
							<p>
								Eftersom dessa är anpassade CSS-egenskaper, fungerar de som förväntat.
								De tillåter också flexibilitet när systemet implementeras. Genom att
								kombinera globala variabler och komponentvariabler på smarta sätt kan du
								ändra standardutseendet med bara några få ändringar.
								<digi-list>
									<li>
										Undvik att läsa in stora CSS-filer om enbart någon enstaka variabel
										behövs genom att sätta ett klassnamn på HTML-elementet och använd dig
										av variabler för att få önskat utseende. Exempel nedan för att sätta
										en marginal nedåt med en variabel:
										<br />
										<digi-code-block
											afCode="
.my-class {
  margin-bottom: var(--digi--margin--medium);
}
                    "
											afLanguage={CodeBlockLanguage.CSS}
											af-code="
.my-class {
  margin-bottom: var(--digi--margin--medium);
}
                    "
											af-language={CodeBlockLanguage.CSS}
										></digi-code-block>
									</li>
									<li>
										Använd CSS-variabler i första hand och undvik sass-variabler i den mån
										det går.
									</li>
									<li>
										Undvik användning av pixelvärden i CSS:en, använd istället rem-värden
										via importering av rem-funktionen som kan användas enligt exempel
										nedan:
										<digi-code-block
											afCode='
@use "@digi/styles/src/functions/rem" as *;

.my-class {
  margin-bottom: #{rem(16)}
}
                    '
											afLanguage={CodeBlockLanguage.CSS}
											af-code='
@use "@digi/styles/src/functions/rem" as *;

.my-class {
  margin-bottom: #{rem(16)}
}
                    '
											af-language={CodeBlockLanguage.CSS}
										></digi-code-block>
										16 motsvarar 16px. Detta är endast ett exempel för att visa hur
										funktionen fungerar. Bättre lösning i detta fall hade varit att
										använda en token för marginalen:{' '}
										<digi-code afCode="margin-bottom: var(--digi--margin--medium);"></digi-code>
									</li>
								</digi-list>
							</p>
						</digi-layout-block>
					</digi-docs-page-layout>
				</div>
			</digi-typography>
		);
	}
}
