import { newSpecPage } from '@stencil/core/testing';
import { ExternaWebbplatser } from './digi-docs-externa-webbplatser';

describe('digi-docs-externa-webbplatser', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [ExternaWebbplatser],
			html: '<digi-docs-externa-webbplatser></digi-docs-externa-webbplatser>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-externa-webbplatser>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-externa-webbplatser>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [ExternaWebbplatser],
			html: `<digi-docs-externa-webbplatser first="Stencil" last="'Don't call me a framework' JS"></digi-docs-externa-webbplatser>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-externa-webbplatser first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-externa-webbplatser>
    `);
	});
});
