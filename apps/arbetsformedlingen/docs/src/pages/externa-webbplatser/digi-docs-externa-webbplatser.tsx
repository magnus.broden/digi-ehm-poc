import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';

@Component({
	tag: 'digi-docs-externa-webbplatser',
	styleUrl: 'digi-docs-externa-webbplatser.scss',
	scoped: true
})
export class ExternaWebbplatser {
	Router = router;

	@State() pageName = 'Instruktioner för externa webbplatser';

	linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/externa-webbplatser/digi-docs-externa-webbplatser.tsx"
				>
					<span slot="preamble">
						Varje extern webbplats som Arbetsförmedlingen ansvarar för ska ha en egen
						tillgänglighetsredogörelse. Ansvariga ska uppdatera texterna i
						redogörelsen i samband med varje release.
					</span>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Vad räknas som en extern webbplats?</h2>
							<p>
								Webbplatser som tillhör Arbetsförmedlingen, men som har en annan webbadress
                              	än arbetsformedlingen.se är externa webbplatser. Även delar eller avdelningar
                              	på arbetsformedlingen.se, som inte ingår i webbplatsens
								informationsstruktur och har ett avvikande sidhuvud, sidfot eller huvudmeny,
								är externa webbplatser.
							</p>
							<h2>Vad gäller för externa webbplatser?</h2>
							<p>
								Ansvariga för webbplatsen ska publicera en egen tillgänglighetsredogörelse
                              	för sin webbplats. Redogörelsen ska vara publicerad i ett tillgängligt format
                              	och sidan i sig ska uppfylla kraven på tillgänglighet.
							</p>
							<p>
								Tillgänglighetsredogörelsen ska publiceras på en lämplig plats och vara lätt att
                              	hitta på den aktuella webbplatsen. Förslagsvis under "Om webbplatsen"
                              	eller motsvarande. En länk till redogörelsen bör även finnas i webbplatsens
                              	sidfot och lättåtkomlig från webbplatsens alla sidor.
							</p>
							<p>Alla Arbetsförmedlingens redogörelser ska överlag se likadana ut.</p>
                          	<p>Obeservera att tillgänglighetsredogörelsen bara behöver uppdateras om ni
                              har hittat nya brister eller om ni har gjort ändringar i redogörelsen,
                              exempelvis att brister har åtgärdats. 
                          	</p>
							<h2>Gör så här:</h2>
							<h3>Steg 1: Utse en huvudansvarig</h3>
							<digi-list>
								<li>
									Bestäm vem som är huvudansvarig för webbplatsens redogörelsesida och
									var sidan ska publiceras.
								</li>
								<li>
									Utse även en person för att sammanställa listor på
									tillgänglighetsbrister om redogörelsen omfattar fler produkter eller
									tjänster.
								</li>
							</digi-list>
							<h3>Steg 2: Testa webbplatsen och lista era brister</h3>
							<digi-list>
								<li>
									Ta reda på vilka brister ni har genom att utgå från
									tillgänglighetslistan i designsystemet.
								</li>
								<li>
									Lista era brister. Följ gärna instruktionerna för lista på brister till
									största del. På sidan finns en mall och ett tillvägagångssätt för att
									fylla i era brister. Delen ”Information till redaktören” i mallen
									fungerar som information till den hos er som sammanställer hela
									redogörelsen.
								</li>
							</digi-list>

							<digi-link-internal
								afHref="/tillganglighet-och-design/tillganglighet-checklista"
								af-variation="small"
								onAfOnClick={(e) => this.linkClickHandler(e)}
							>
								Tillgänglighetslistan i Designsystemet
							</digi-link-internal>

							<br />
							<br />
							<digi-link-internal
								afHref="/tillganglighetsredogorelse/lista-med-tillganglighetsbrister"
								af-variation="small"
								onAfOnClick={(e) => this.linkClickHandler(e)}
							>
								Instruktioner för lista med tillgänglighetsbrister
							</digi-link-internal>

							<h3>Steg 3: Strukturera och sammanfatta brister</h3>
							<digi-list>
								<li>
									Bestäm hur ni vill strukturera era brister. Ni kan dela upp bristerna
									utifrån tjänst/sida eller annat sätt som är naturlig för era användare.
									Vill ni ha alla brister på samma sida som redogörelsen eller uppdelat
									på olika undersidor?
								</li>
								<li>Sammanfatta bristerna i punktform.</li>
                              	<li>Uppge ett datum för när bristen planeras att åtgärdas. Här går det bra
                                  	att gruppera flera brister under ett gemensamt datum.
                                  	<br />
                              		Exempel: Följande tillgänglighetsbrister planeras att vara åtgärdade
                                    före den 31 december 2022.
                                </li>
                            </digi-list>
							<h3>Steg 4: Kontrollera era kunders kontaktvägar</h3>
							<digi-list>
								<li>
									Länka till formuläret för att lämna
									synpunkter på digital tillgänglighet som finns på Arbetsförmedlingens
									webbplats.
								</li>
							</digi-list>

							<p>
								<digi-link-external
									afHref="https://arbetsformedlingen.se/kontakt/ge-oss-tips-och-synpunkter/synpunkter-pa-digital-tillganglighet"
									af-variation="small"
									// onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Formulär för att lämna synpunkter på bristande tillgänglighet på
									Arbetsförmedlingens webbplats
								</digi-link-external>
							</p>
							<digi-link
								af-variation="small"
								afHref="mailto:webbtillganglighet@arbetsformedlingen.se"
							>
								<digi-icon-envelope slot="icon"></digi-icon-envelope>Funktionsbrevlådan
								för webbtillgänglighet
							</digi-link>

							<h3>Steg 5: Skapa en redogörelse utifrån mallen</h3>
							<digi-list>
								<li>
									Utgå från mallen nedan för hela redogörelsen och för att skapa
									resterande av redogörelsen. I mallen är det tydligt vilka delar ni
									publicerar rakt av och vilka delar ni själva behöver bestämma hur ni
									ska fylla i.
								</li>
							</digi-list>
							<h4>Mall för tillgänglighetsredogörelse</h4>
							<p>
								<digi-link-external
									afHref="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Tillg%C3%A4nglighetsredog%C3%B6relser/Mallar/Mall%20tillg%C3%A4nglighetsredog%C3%B6relsen%20externa%20webbplatser.docx?d=wfae57a6ca1a54f4dab4afa1704986067"
									af-variation="small"
									// onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Mall för tillgänglighetsredogörelse för externa webbplatser på
									SharePoint (Word-dokument och intern länk endast för medarbetare på
									Arbetsförmedlingen)
								</digi-link-external>
							</p>
							<h3>Steg 6: Ladda upp den färdiga redogörelsen</h3>
							<digi-list>
								<li>Ladda upp redogörelsen under innevarande månad på ytan nedan. Ni
                              		behöver bara ladda upp en uppdatering av redogörelsen om ni har
                              		hittat nya brister, åtgärdat tidigare brister eller gjort andra
                              		ändringar.
                              	</li>
							</digi-list>

							<p>
								<digi-link-external
									afHref="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Forms/AllItems.aspx?csf=1&e=bq5ihn%2F&FolderCTID=0x0120005C2E125F40DFDF4CA509E09B8112068C&id=%2Fsites%2Ftillganglighet%2FDelade%20dokument%2FTillg%C3%A4nglighetsredog%C3%B6relser%2FRedog%C3%B6relser%20och%20checklistor%2F2023"
									af-variation="small"
									// onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Tillgänglighetsredogörelser 2023 på SharePoint (intern länk endast för
									medarbetare på Arbetsförmedlingen)
								</digi-link-external>
							</p>
						</article>
					</digi-layout-container>
				</digi-docs-page-layout>
			</host>
		);
	}
}
