import { Component, h, State } from '@stencil/core';

import {
	CodeLanguage,
	CodeBlockLanguage,
	CodeVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-docs-work-with-digi-core-angular',
	styleUrl: 'digi-docs-work-with-digi-core-angular.scss',
	scoped: true
})
export class DigiDocsWorkWithDigiCoreAngular {
	@State() pageName = 'Digi Core Angular';

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/digi-docs-work-with-digi-core-angular/digi-docs-work-with-digi-core-angular.tsx"
				>
					<span slot="preamble">
						Biblioteket Digi Core Angular är ett lager på webbkomponenterna i Digi
						Core som förenklar utvecklingen i Angular. Digi Core Angular är en så
						kallad output-target från StencilJS som gör så att vi kan använda samma
						kodbas i alla våra kodbibliotek.
					</span>
					<digi-layout-container af-margin-top af-margin-bottom>
						<article>
							<h2>Installera Digi Core Angular</h2>
							<h3>NPM</h3>
							<p>
								Börja med att konfigurera Nexus. Lägg till en fil som heter
								<digi-code
									afVariation={CodeVariation.LIGHT}
									afCode=".npmrc"
								></digi-code>{' '}
								i samma mapp som din{' '}
								<digi-code
									afVariation={CodeVariation.LIGHT}
									afCode="package.json"
								></digi-code>{' '}
								om du inte redan har en sådan och lägg till den här raden bland dina övriga proxyn:
								<digi-code
									afVariation={CodeVariation.LIGHT}
									afCode="@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/"
								></digi-code>
							</p>
							<p>
								Installera Digi Core Angular genom att köra
								<br />
								<digi-code
									afLanguage={CodeLanguage.BASH}
									afVariation={CodeVariation.LIGHT}
									afCode="npm i @digi/arbetsformedlingen @digi/arbetsformedlingen-angular"
								/>
								<br />i roten av ditt angular-projekt.
								<br />
								<br />
								<em>
									Detta paket kör på version 14 av Angular och är inte kompatibel med äldre versioner!
								</em>
							</p>
							<h3>Lägg till modulen</h3>
							<p>
								För att få med core-komponenterna i angular-appen så behöver du
								importera modulen{' '}
								<digi-code
									afLanguage={CodeLanguage.TYPESCRIPT}
									afVariation={CodeVariation.LIGHT}
									afCode="DigiArbetsformedlingenAngularModule"
								/>
								. Här följer ett exempel på filen{' '}
								<digi-code
									afLanguage={CodeLanguage.TYPESCRIPT}
									afVariation={CodeVariation.LIGHT}
									afCode="app.module.ts"
								/>
								:
							</p>
							<digi-code-block
								afLanguage={CodeBlockLanguage.TYPESCRIPT}
								afCode={`import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { DigiArbetsformedlingenAngularModule } from '@digi/arbetsformedlingen-angular';
import { MyPageComponent } from './pages/my-page/my-page.component'

@NgModule({
  declarations: [AppComponent, MyPageComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: "my-page", component: MyPageComponent}
    ], { initialNavigation: 'enabledBlocking' }),
    ReactiveFormsModule,
    DigiArbetsformedlingenAngularModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}`}
							></digi-code-block>
							<h3>Lägg till styles</h3>
							<p>
								Du behöver också lägga till styles i din app för att få rätt utseende.
								Detta gör du genom att importera vår css-fil<br />
								<digi-code
									afLanguage={CodeLanguage.SCSS}
									afVariation={CodeVariation.LIGHT}
									afCode="@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css';"
								/>
							</p>
							<h3>Lägg till typsnitt</h3>
							<p>
								För att läsa in typsnitten behöver man läsa in CSS/SCSS-filen i t.ex.
								din globala SCSS-fil:
								<digi-code
                  afLanguage={CodeLanguage.SCSS}
									afVariation={CodeVariation.LIGHT}
									af-code="@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.scss';"
								/>
							</p>
						</article>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Använda Digi Core Angular</h2>
							<h3>Använda enums</h3>
							<p>
								Du finner alla enums under Digi Core och kan referera dom såhär:
								<br />
								<digi-code
									afLanguage={CodeLanguage.TYPESCRIPT}
									afVariation={CodeVariation.LIGHT}
									afCode="import { EnumName1, EnumName2 } from '@digi/arbetsformedlingen';"
								/>
							</p>
							<h3>Reactive Forms</h3>
							<p>
								Du behöver inte tänka på något särskilt när det kommer till Reactive
								Forms och Digi Core Angular. Komponenterna använder value och checked
								för input och via en ValueAccessor i Digi Core Angular så lyssnar även
								Reactive Forms korrekt på komponenternas change-event. Här följer ett
								exempel på ett enkelt formulär som använder Digi Core Angular:
							</p>
							<digi-navigation-tabs afAriaLabel="Formulär-exempel">
								<digi-navigation-tab
									afAriaLabel="my-form.component.html"
									afId="my-form-html"
								>
									<digi-code-block
										afLanguage={CodeBlockLanguage.HTML}
										afCode={`<form [formGroup]="myForm" class="my-form">
  <digi-form-input
    afId="myEmail"
    afLabel="Epost-adress"
    [afType]="FormInputType.EMAIL"
    afRequired="true"
    afAutocomplete="on"
    formControlName="myEmail"
  ></digi-form-input>

  <digi-form-input
    afId="myAge"
    afLabel="Min ålder"
    [afType]="FormInputType.NUMBER"
    afRequired="true"
    afAutocomplete="on"
    formControlName="myAge"
  ></digi-form-input>

  <digi-form-textarea
    afId="myDescription"
    afLabel="Beskrivning"
    afRequired="true"
    formControlName="myDescription"
  ></digi-form-textarea>

  <digi-form-select
    afId="myJob"
    afLabel="Yrke"
    afRequired="true"
    formControlName="myJob"
  >
    <option name="Välj yrke" value="">Välj yrke</option>
    <option name="handlaggare" value="handlaggare">Handläggare</option>
    <option name="konsult" value="konsult">Konsult</option>
  </digi-form-select>

  <digi-form-fieldset afLegend="Svara på frågan" afId="myChoice">
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="Yes"
      afId="Yes"
      afValue="Yes"
    ></digi-form-radiobutton>
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="Maybe"
      afId="Maybe"
      afValue="Maybe"
    ></digi-form-radiobutton>
    <digi-form-radiobutton
      formControlName="myChoice"
      afName="myChoice"
      afLabel="No"
      afId="No"
      afValue="No"
    ></digi-form-radiobutton>
  </digi-form-fieldset>

  <br />

  <digi-form-fieldset afLegend="Klicka i" afId="myBox" formArrayName="myBox">
    <digi-form-checkbox
      formControlName="0"
      afName="myBox"
      afLabel="Val 1"
      afId="val-1"
    ></digi-form-checkbox>
    <digi-form-checkbox
      formControlName="1"
      afName="myBox"
      afLabel="Val 2"
      afId="val-2"
    ></digi-form-checkbox>
    <digi-form-checkbox
      formControlName="2"
      afName="myBox"
      afLabel="Val 3"
      afId="val-3"
    ></digi-form-checkbox>
  </digi-form-fieldset>

  <br />

  <digi-button (afOnClick)="clickButtonHandler()">Skicka</digi-button>
</form>`}
									></digi-code-block>
								</digi-navigation-tab>
								<digi-navigation-tab
									afAriaLabel="my-form.component.ts"
									afId="my-form-ts"
								>
									<digi-code-block
										afLanguage={CodeBlockLanguage.TYPESCRIPT}
										afCode={`import { Component, HostListener, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { FormInputValidation, FormInputType } from '@digi/arbetsformedlingen';

@Component({
  selector: 'my-form',
  templateUrl: './my-form.component.html',
  styleUrls: ['./my-form.component.scss']
})
export class MyFormComponent implements OnInit {

  FormInputValidation = FormInputValidation;
  FormInputType = FormInputType;

  myForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.myForm = this.fb.group({});
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      myEmail: ['', [Validators.email, Validators.required]], 
      myAge: [null, [Validators.required]], 
      myDescription: ['', [Validators.required, Validators.maxLength(60)]],
      myJob: ['', Validators.required],
      myChoice: [null, Validators.required],
      myBox: new FormArray([
        new FormControl(false),
        new FormControl(false),
        new FormControl(false)
      ])
    });
  }

  clickButtonHandler() {
    console.log(this.myForm.value);
    alert('click!');
  }
}`}
									></digi-code-block>
								</digi-navigation-tab>
							</digi-navigation-tabs>
						</article>
					</digi-layout-container>
				</digi-docs-page-layout>
			</host>
		);
	}
}
