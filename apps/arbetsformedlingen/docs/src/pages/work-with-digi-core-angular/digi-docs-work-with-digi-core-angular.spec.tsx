import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsWorkWithDigiCoreAngular } from './digi-docs-work-with-digi-core-angular';

describe('digi-docs-work-with-digi-core-angular', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsWorkWithDigiCoreAngular],
      html: '<digi-docs-work-with-digi-core-angular></digi-docs-work-with-digi-core-angular>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-work-with-digi-core-angular>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-work-with-digi-core-angular>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsWorkWithDigiCoreAngular],
      html: `<digi-docs-work-with-digi-core-angular first="Stencil" last="'Don't call me a framework' JS"></digi-docs-work-with-digi-core-angular>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-work-with-digi-core-angular first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-work-with-digi-core-angular>
    `);
  });
});
