import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsGraphicsOverview } from './digi-docs-graphics-overview';

describe('digi-docs-graphics-overview', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsGraphicsOverview],
      html: '<digi-docs-graphics-overview></digi-docs-graphics-overview>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-graphics-overview>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-graphics-overview>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsGraphicsOverview],
      html: `<digi-docs-graphics-overview first="Stencil" last="'Don't call me a framework' JS"></digi-docs-graphics-overview>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-graphics-overview first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-graphics-overview>
    `);
  });
});
