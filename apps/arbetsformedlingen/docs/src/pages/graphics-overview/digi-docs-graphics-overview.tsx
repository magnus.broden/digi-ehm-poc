import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'digi-docs-graphics-overview',
  styleUrl: 'digi-docs-graphics-overview.scss',
  scoped: true
})
export class DigiDocsGraphicsOverview {
  @State() componentName: string;

  render() {
    return (
      <div class="digi-docs-graphics-overview">
        <digi-docs-page-layout >
          <digi-layout-block >
            <p>Översikt</p>
          </digi-layout-block>


        </digi-docs-page-layout>
      </div>
    );
  }
}
