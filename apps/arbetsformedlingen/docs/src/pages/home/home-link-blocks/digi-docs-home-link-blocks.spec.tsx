import { newSpecPage } from '@stencil/core/testing';
import { HomeLinkBlocks } from './digi-docs-home-link-blocks';

describe('digi-docs-home-link-blocks', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [HomeLinkBlocks],
			html: '<digi-docs-home-link-blocks></digi-docs-home-link-blocks>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-home-link-blocks>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-home-link-blocks>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [HomeLinkBlocks],
			html: `<digi-docs-home-link-blocks first="Stencil" last="'Don't call me a framework' JS"></digi-docs-home-link-blocks>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-home-link-blocks first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-home-link-blocks>
    `);
	});
});
