import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-home', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-home></digi-docs-home>');

    const element = await page.find('digi-docs-home');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-home></digi-docs-home>');

    const element = await page.find('digi-docs-home >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
