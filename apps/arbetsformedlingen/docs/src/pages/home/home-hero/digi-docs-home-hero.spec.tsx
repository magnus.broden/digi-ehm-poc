import { newSpecPage } from '@stencil/core/testing';
import { HomeHero } from './digi-docs-home-hero';

describe('digi-docs-home-hero', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [HomeHero],
			html: '<digi-docs-home-hero></digi-docs-home-hero>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-home-hero>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-home-hero>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [HomeHero],
			html: `<digi-docs-home-hero first="Stencil" last="'Don't call me a framework' JS"></digi-docs-home-hero>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-home-hero first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-home-hero>
    `);
	});
});
