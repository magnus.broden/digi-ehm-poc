import { newSpecPage } from '@stencil/core/testing';
import { HomeHeroAlt } from './digi-docs-home-hero-alt';

describe('digi-docs-home-hero-alt', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [HomeHeroAlt],
			html: '<digi-docs-home-hero-alt></digi-docs-home-hero-alt>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-home-hero-alt>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-home-hero-alt>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [HomeHeroAlt],
			html: `<digi-docs-home-hero-alt first="Stencil" last="'Don't call me a framework' JS"></digi-docs-home-hero-alt>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-home-hero-alt first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-home-hero-alt>
    `);
	});
});
