import { Component, getAssetPath, h } from '@stencil/core';
import { router } from '../../../global/router';

@Component({
	tag: 'digi-docs-home-hero-alt',
	styleUrl: 'digi-docs-home-hero-alt.scss',
	scoped: true
})
export class HomeHeroAlt {
	Router = router;
  
	linkHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<host>
				<div class="digi-docs-home-hero-alt">
					<div class="digi-docs-home-hero-alt__text">
						<digi-typography><h1>Skapa användbara och tillgängliga tjänster för den svenska arbetsmarknaden</h1></digi-typography>
						<p class="digi-docs-home-hero-alt__preamble">
							I Arbetsförmedlingens designsystem  hittar du teknik, design och verktyg baserat på gemensamma insikter, research och användarbehov.
						</p>
						<digi-link-button
							afHref="/om-designsystemet/introduktion"
							af-target="_blank"
							onAfOnClick={(e) => this.linkHandler(e)}
						>
							Introduktion
						</digi-link-button>
					</div>
					<div class="digi-docs-home-hero-alt__media">
						<digi-media-image
							class="digi-docs-home-hero-alt__media-image"
							afUnlazy
							afSrc={getAssetPath('/assets/images/startsidan-illustration.png')}
							afAlt="Personer i centrum med vissa av designsystemets komponenter runtomkring, illustration."
						></digi-media-image>
					</div>
				</div>
			</host>
		);
	}
}
