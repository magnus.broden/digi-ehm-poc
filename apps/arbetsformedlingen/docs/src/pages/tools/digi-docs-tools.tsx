import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'digi-docs-tools',
  styleUrl: 'digi-docs-tools.scss',
  scoped: true
})
export class DigiDocsTools {
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  private getText(): string {
    return `${this.first} ${this.middle} ${this.last}`;
  }

  render() {
    return <div>Tools{this.getText()}</div>;
  }
}
