import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsTools } from './digi-docs-tools';

describe('digi-docs-tools', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsTools],
      html: '<digi-docs-tools></digi-docs-tools>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-tools>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-tools>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsTools],
      html: `<digi-docs-tools first="Stencil" last="'Don't call me a framework' JS"></digi-docs-tools>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-tools first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-tools>
    `);
  });
});
