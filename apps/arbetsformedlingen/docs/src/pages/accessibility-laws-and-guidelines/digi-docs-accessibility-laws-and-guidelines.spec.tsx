import { newSpecPage } from '@stencil/core/testing';
import { AccessibilityLawsAndGuidelines } from './digi-docs-accessibility-laws-and-guidelines';

describe('digi-docs-accessibility-laws-and-guidelines', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [AccessibilityLawsAndGuidelines],
      html: '<digi-docs-accessibility-laws-and-guidelines></digi-docs-accessibility-laws-and-guidelines>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-accessibility-laws-and-guidelines>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-accessibility-laws-and-guidelines>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [AccessibilityLawsAndGuidelines],
      html: `<digi-docs-accessibility-laws-and-guidelines first="Stencil" last="'Don't call me a framework' JS"></digi-docs-accessibility-laws-and-guidelines>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-accessibility-laws-and-guidelines first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-accessibility-laws-and-guidelines>
    `);
  });
});
