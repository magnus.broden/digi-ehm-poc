import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsColorsTokens } from './digi-docs-colors-tokens';

describe('digi-docs-colors-tokens', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorsTokens],
      html: '<digi-docs-colors-tokens></digi-docs-colors-tokens>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-colors-tokens>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-colors-tokens>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorsTokens],
      html: `<digi-docs-colors-tokens first="Stencil" last="'Don't call me a framework' JS"></digi-docs-colors-tokens>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-colors-tokens first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-colors-tokens>
    `);
  });
});
