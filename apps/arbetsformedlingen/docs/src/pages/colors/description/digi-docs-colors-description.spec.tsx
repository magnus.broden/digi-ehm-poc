import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsColorsDescription } from './digi-docs-colors-description';

describe('digi-docs-colors-description', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorsDescription],
      html: '<digi-docs-colors-description></digi-docs-colors-description>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-colors-description>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-colors-description>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorsDescription],
      html: `<digi-docs-colors-description first="Stencil" last="'Don't call me a framework' JS"></digi-docs-colors-description>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-colors-description first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-colors-description>
    `);
  });
});
