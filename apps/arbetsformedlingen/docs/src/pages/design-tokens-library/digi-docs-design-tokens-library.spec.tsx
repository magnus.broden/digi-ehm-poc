import { newSpecPage } from '@stencil/core/testing';
import { DesignTokensLibrary } from './digi-docs-design-tokens-library';

describe('digi-docs-design-tokens-library', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DesignTokensLibrary],
			html: '<digi-docs-design-tokens-library></digi-docs-design-tokens-library>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-design-tokens-library>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-design-tokens-library>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DesignTokensLibrary],
			html: `<digi-docs-design-tokens-library first="Stencil" last="'Don't call me a framework' JS"></digi-docs-design-tokens-library>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-design-tokens-library first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-design-tokens-library>
    `);
	});
});
