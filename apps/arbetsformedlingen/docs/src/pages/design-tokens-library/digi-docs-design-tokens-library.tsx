import { Component, h, State } from '@stencil/core';
// import { LayoutBlockVariation } from 'libs/core/src';
import designTokens from '../../../../../../dist/libs/arbetsformedlingen/design-tokens/styleguide/web.json';
import { TokenFormat } from '../../components/design-tokens/token-format.enum';
type Tokens = typeof designTokens;

@Component({
	tag: 'digi-docs-design-tokens-library',
	styleUrl: 'digi-docs-design-tokens-library.scss',
	scoped: true
})
export class DesignTokensLibrary {
	@State() pageName = 'Design Tokens Library';
	@State() tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY;
	@State() searchTerm: string = '';

	@State() colorTokens: Tokens;
	@State() borderTokens: Tokens;
	@State() layoutTokens: Tokens;
	@State() animationTokens: Tokens;
	@State() typographyTokens: Tokens;

	componentWillLoad() {
		this.colorTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/color/color.brand')
			)
		];

		this.borderTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/border/border.brand')
			)
		];

		this.layoutTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/layout/layout.brand')
			)
		];

		this.animationTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/animation/animation.brand')
			)
		];

		this.typographyTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/typography/typography.brand')
			)
		];
	}
	get colorTokenList() {
		const colorTokenList = this.colorTokens.filter(
			(token) =>
				this.setTokenFormat(token.name)
					.toLowerCase()
					.includes(this.searchTerm.toLowerCase()) ||
				token.name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
				token.value.toLowerCase().includes(this.searchTerm.toLowerCase())
		);
		return colorTokenList;
	}

	get borderTokenList() {
		const borderTokenList = this.borderTokens.filter(
			(token) =>
				this.setTokenFormat(token.name)
					.toLowerCase()
					.includes(this.searchTerm.toLowerCase()) ||
				token.name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
				token.value.toLowerCase().includes(this.searchTerm.toLowerCase())
		);
		return borderTokenList;
	}

	get layoutTokenList() {
		const layoutTokenList = this.layoutTokens.filter(
			(token) =>
				this.setTokenFormat(token.name)
					.toLowerCase()
					.includes(this.searchTerm.toLowerCase()) ||
				token.name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
				token.value.toLowerCase().includes(this.searchTerm.toLowerCase())
		);
		return layoutTokenList;
	}

	get animationTokenList() {
		const animationTokenList = this.animationTokens.filter(
			(token) =>
				this.setTokenFormat(token.name)
					.toLowerCase()
					.includes(this.searchTerm.toLowerCase()) ||
				token.name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
				token.value.toLowerCase().includes(this.searchTerm.toLowerCase())
		);
		return animationTokenList;
	}

	get typographyTokenList() {
		const typographyTokenList = this.typographyTokens.filter(
			(token) =>
				this.setTokenFormat(token.name)
					.toLowerCase()
					.includes(this.searchTerm.toLowerCase()) ||
				token.name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
				token.value.toLowerCase().includes(this.searchTerm.toLowerCase())
		);
		return typographyTokenList;
	}

	isSubset(superObj, subObj) {
		return Object.keys(subObj).every((ele) => {
			if (typeof subObj[ele] == 'object') {
				return this.isSubset(superObj[ele], subObj[ele]);
			}
			return subObj[ele] === superObj[ele];
		});
	}

	tokenFormatChangeHandler(e) {
		this.tokenFormat = e.detail.target.value;
	}

	setTokenFormat(e) {
		switch (this.tokenFormat) {
			case TokenFormat.CUSTOM_PROPERTY:
				return `--${e}`;
			case TokenFormat.SASS:
				return `$${e}`;
		}
	}

	filterInputHandler(e) {
		if (e.detail.target.tagName == 'INPUT') {
			this.searchTerm = e.detail.target.value;
		}
	}
	render() {
		return (
			<div class="digi-docs-design-tokens-library">
				<digi-docs-page-layout>
					<digi-layout-container
						afVerticalPadding
						afMarginBottom
						afMarginTop
						afNoGutter
					>
						<div class="digi-docs-design-tokens__filter-wrapper">
							<div class="digi-docs-design-tokens__filter-search">
								<digi-form-input-search
									afLabel={`Filtrera token`}
									af-hide-button="true"
									onAfOnInput={(e) => this.filterInputHandler(e)}
								></digi-form-input-search>
							</div>{' '}
							<div class="digi-docs-design-tokens__filter-format">
								<digi-form-select
									afLabel="Format"
									afId="tokenFormat"
									af-variation="medium"
									afDisableValidation={true}
									onAfOnChange={(e) => this.tokenFormatChangeHandler(e)}
								>
									<option value="custom-property">CSS-variabel </option>
									<option value="sass">Sass </option>
								</digi-form-select>
							</div>
						</div>
						<br />

						{this.colorTokenList.length > 0 && (
							// <digi-layout-block
							// 	afVariation={LayoutBlockVariation.SECONDARY}
							// 	afVerticalPadding
							// >
							<div class="digi-docs-design-tokens__tokens-wrapper">
								{/* <digi-card> */}
								<h2 class="digi-docs-design-tokens__card-heading">Färger</h2>
								<digi-table>
									<table>
										<thead>
											<tr>
												<th>
													<span class="digi-docs-design-tokens__example-column">
														Exempel
													</span>
												</th>
												<th>Token</th>
												<th>Värde</th>
											</tr>
										</thead>
										<tbody>
											{this.colorTokenList.map((token) => {
												return (
													<tr>
														<td>
															<digi-docs-color-swatch token={JSON.stringify(token)} />
														</td>
														<td>
															<div class="digi-docs-design-tokens__code-column">
																<digi-code
																	afCode={`${this.setTokenFormat(token.name)}`}
																></digi-code>
																<digi-docs-token-copy
																	afValue={`${this.setTokenFormat(token.name)}`}
																/>
															</div>
														</td>
														<td>{token.value}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</digi-table>
								{/* </digi-card> */}
							</div>
							// </digi-layout-block>
						)}
						<br />
						<br />
						<br />

						{this.borderTokenList.length > 0 && (
							// <digi-layout-block
							// 	afVariation={LayoutBlockVariation.SECONDARY}
							// 	afVerticalPadding
							// >
							<div class="digi-docs-design-tokens__tokens-wrapper">
								{/* <digi-card> */}
								<h2 class="digi-docs-design-tokens__card-heading">Ramlinje</h2>
								<digi-table>
									<table>
										<thead>
											<tr>
												<th>
													<span class="digi-docs-design-tokens__example-column">
														Exempel
													</span>
												</th>
												<th>Token</th>
												<th>Värde</th>
											</tr>
										</thead>
										<tbody>
											{this.borderTokenList.map((token) => {
												return (
													<tr>
														<td>
															<digi-docs-token-demo
																afRadius={
																	String(token.name).includes('border-radius')
																		? token.value
																		: null
																}
																afWidth={
																	String(token.name).includes('border-width')
																		? token.value
																		: null
																}
																afStyle={
																	String(token.name).includes('border-style')
																		? token.value
																		: null
																}
															/>
														</td>
														<td>
															<div class="digi-docs-design-tokens__code-column">
																<digi-code
																	afCode={`${this.setTokenFormat(token.name)}`}
																></digi-code>
																<digi-docs-token-copy
																	afValue={`${this.setTokenFormat(token.name)}`}
																/>
															</div>
														</td>
														<td>{token.value}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</digi-table>
								{/* </digi-card> */}
							</div>
							// </digi-layout-block>
						)}
						<br />
						<br />
						<br />

						{this.layoutTokenList.length > 0 && (
							<div class="digi-docs-design-tokens__tokens-wrapper">
								<h2 class="digi-docs-design-tokens__card-heading">Layout</h2>
								<digi-table>
									<table>
										<thead>
											<tr>
												<th>
													<span class="digi-docs-design-tokens__example-column">
														Exempel
													</span>
												</th>
												<th>Token</th>
												<th>Värde</th>
											</tr>
										</thead>
										<tbody>
											{this.layoutTokenList
												.sort((a, b) => {
													if (a.value > b.value) return 1;
													if (a.value < b.value) return -1;
													return 0;
												})
												.map((token) => {
													return (
														<tr>
															<td>
																{(String(token.name).includes('digi--gutter') ||
																	String(token.name).includes('digi--padding') ||
																	String(token.name).includes('digi--margin')) && (
																	<digi-docs-token-demo afSize={token.value} />
																)}
															</td>
															<td>
																<div class="digi-docs-design-tokens__code-column">
																	<digi-code
																		afCode={`${this.setTokenFormat(token.name)}`}
																	></digi-code>
																	<digi-docs-token-copy
																		afValue={`${this.setTokenFormat(token.name)}`}
																	/>
																</div>
															</td>
															<td>{token.value}</td>
														</tr>
													);
												})}
										</tbody>
									</table>
								</digi-table>
							</div>
						)}
						<br />
						<br />
						<br />
						{this.animationTokenList.length > 0 && (
							<div class="digi-docs-design-tokens__tokens-wrapper">
								<h2 class="digi-docs-design-tokens__card-heading">Animationer</h2>
								<digi-table>
									<table>
										<thead>
											<tr>
												<th>
													<span class="digi-docs-design-tokens__example-column">
														Exempel
													</span>
												</th>
												<th>Token</th>
												<th>Värde</th>
											</tr>
										</thead>
										<tbody>
											{this.animationTokenList.map((token) => {
												return (
													<tr>
														<td>
															<digi-docs-token-demo
																afAnimationDuration={
																	String(token.name).includes('animation--duration')
																		? token.value
																		: null
																}
																afAnimationRotation={
																	String(token.name).includes('animation--rotation')
																		? token.value
																		: null
																}
															/>
														</td>
														<td>
															<div class="digi-docs-design-tokens__code-column">
																<digi-code
																	afCode={`${this.setTokenFormat(token.name)}`}
																></digi-code>
																<digi-docs-token-copy
																	afValue={`${this.setTokenFormat(token.name)}`}
																/>
															</div>
														</td>
														<td>{token.value}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</digi-table>
							</div>
						)}
						<br />
						<br />
						<br />
						{this.typographyTokenList.length > 0 && (
							<div class="digi-docs-design-tokens__tokens-wrapper">
								<h2 class="digi-docs-design-tokens__card-heading">Typografi</h2>
								<digi-table>
									<table>
										<thead>
											<tr>
												<th>
													<span class="digi-docs-design-tokens__example-column">
														Exempel
													</span>
												</th>
												<th>Token</th>
												<th>Värde</th>
											</tr>
										</thead>
										<tbody>
											{this.typographyTokenList.map((token) => {
												return (
													<tr>
														<td>
															<div class="typo-demo">
																<digi-docs-token-demo
																	afFontSize={
																		String(token.name).includes('font-size') ? token.value : null
																	}
																	afTextDecoration={
																		String(token.name).includes('text-decoration')
																			? token.value
																			: null
																	}
																	afFontWeight={
																		String(token.name).includes('font-weight')
																			? token.value
																			: null
																	}
																	afLineHeight={
																		String(token.name).includes('line-height')
																			? token.value
																			: null
																	}
																>
																	{' '}
																	Aa
																</digi-docs-token-demo>
															</div>
														</td>
														<td>
															<div class="digi-docs-design-tokens__code-column">
																<digi-code
																	afCode={`${this.setTokenFormat(token.name)}`}
																></digi-code>
																<digi-docs-token-copy
																	afValue={`${this.setTokenFormat(token.name)}`}
																/>
															</div>
														</td>
														<td>{token.value}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</digi-table>
							</div>
						)}
					</digi-layout-container>
				</digi-docs-page-layout>
			</div>
		);
	}
}
