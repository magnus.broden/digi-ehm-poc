import { newSpecPage } from '@stencil/core/testing';
import { InternaWebbplatser } from './digi-docs-interna-webbplatser';

describe('digi-docs-interna-webbplatser', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [InternaWebbplatser],
			html: '<digi-docs-interna-webbplatser></digi-docs-interna-webbplatser>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-interna-webbplatser>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-interna-webbplatser>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [InternaWebbplatser],
			html: `<digi-docs-interna-webbplatser first="Stencil" last="'Don't call me a framework' JS"></digi-docs-interna-webbplatser>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-interna-webbplatser first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-interna-webbplatser>
    `);
	});
});
