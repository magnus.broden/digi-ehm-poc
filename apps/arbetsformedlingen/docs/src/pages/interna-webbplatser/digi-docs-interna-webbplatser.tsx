import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';

@Component({
	tag: 'digi-docs-interna-webbplatser',
	styleUrl: 'digi-docs-interna-webbplatser.scss',
	scoped: true
})
export class InternaWebbplatser {
	Router = router;
	@State() pageName = 'Instruktioner för interna webbplatser';

	linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/interna-webbplatser/digi-docs-interna-webbplatser.tsx"
				>
					<span slot="preamble">
						Även interna webbplatser som Arbetsförmedlingen står bakom ska ha en egen
						tillgänglighetsredogörelse. Redogörelsen kan variera i omfattning beroende
						på om den aktuella webbplatsen omfattas av lagkrav eller inte. Ansvariga
						ska uppdatera texterna i tillgänglighetsredogörelsen regelbundet i samband
						med release.
					</span>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Vad räknas som en intern webbplats?</h2>
							<p>
								Interna webbplatser används internt av medarbetare på Arbetsförmedlingen
								och nås ine av allmänheten. Tillgänglighet är minst lika viktigt för
								våra medarbetare och av stor betydelse för vår arbetsmiljö och
								effektivitet, samt för att alla på myndigheten ska kunna använda våra
								interna tjänster och webbplatser.
							</p>
							<p>
								Till interna webbplatser hör exempelvis Intranätet, SharePoint,
								Confluence och Digitalportalen.
							</p>
							<h2>Vad gäller för interna webbplatser?</h2>
							<p>
								Ansvariga för webbplatsen ska själva publicera en
								tillgänglighetsredogörelse för sin webbplats. Redogörelsen ska vara
								publicerad i ett tillgängligt format och sidan i sig ska uppfylla
								tillgänglighetskraven.
							</p>
							<p>
								<strong>För webbplatser som omfattas av DOS-lagen</strong>, exempelvis
								intranätet, ska en fullständig tillgänglighetsredogörelse upprättas
								enligt mallen längre ner på denna sida.
							</p>
							<p>
								<strong>
									För interna webbplatser och system som inte omfattas av DOS-lagen
								</strong>
								, exempelvis BÄR, ska en förenklad tillgänglighetsredogörelse upprättas
								som redogör för de allvarligaste tillgänglighetsbristerna i det aktuella
								systemet. Det finns en mall för detta längre ner på denna sida.
							</p>
							<p>
								Redogörelsen ska publiceras på webbplatsen, på lämplig plats.
								Förslagsvis under "Om webbplatsen" eller någon motsvarande del. En länk
								till redogörelsen bör även finnas i sidfoten, åtkomligt från varje sida
								på webbplatsen. Om det saknas en generell informationssida om
								webbplatsen, och även en sidfot, kan redogörelsen publiceras i direkt
								anslutning till webbplatsens startpunkt, på dess startsida eller
								inloggningssida. Om användaren kan logga in, bör det även finnas en länk
								till redogörelsen som är åtkomlig efter att användaren har loggat in.
							</p>
							<p>Alla Arbetsförmedlingens redogörelser ska överlag se likadana ut.</p>
							<h2>Gör så här:</h2>
							<h3>Steg 1: Utse en huvudansvarig</h3>
							<digi-list>
								<li>
									Bestäm vem som är huvudansvarig för webbplatsens redogörelse och var
									den ska publiceras.
								</li>
								<li>
									Utse även en person för att sammanställa tillgänglighetsbrister om
									redogörelsen omfattar fler produkter eller förmågor.
								</li>
							</digi-list>
							<h3>Steg 2: Testa er webbplats och lista era brister</h3>
							<digi-list>
								<li>
									Ta reda på vilka brister ni har genom att utgå från
									tillgänglighetslistan i designsystemet
								</li>
								<li>
									<strong>Webbplatser som omfattas av DOS-lagen: </strong>Lista era
									brister. Ni kan följa instruktionerna för lista på brister till största
									del. På sidan finns en mall och ett tillvägagångssätt för att fylla i
									era brister. Delen ”Information till redaktören” i mallen är
									information till den hos er som sammanställer hela redogörelsen. Kika
									gärna på sidan med exempeltexter.
								</li>
								<li>
									<strong>Webbplatser som inte omfattas av DOS-lagen: </strong>Lista de
									allvarligaste bristerna. Hit räknas bland annat användbarhetsproblem
									som ni har sett i samband med användningstester av er webbplats. Även
									avsteg från punkter i tillgänglighetslistan markerade med en stjärna
									(A*) eftersom de betraktas som särskilt hindrande. Det är dessutom
									särskilt allvarligt om det exempelvis inte är möjligt för en användare
									att navigera med tangentbord på webbplatsen eller om den innehåller
									tungt grafiskt innehåll som inte beskrivs med text.
								</li>
							</digi-list>

							<digi-link-internal
								afHref="/tillganglighet-och-design/tillganglighet-checklista"
								af-variation="small"
								onAfOnClick={(e) => this.linkClickHandler(e)}
							>
								Tillgänglighetslistan i designsystemet
							</digi-link-internal>
							<br />
							<br />
							<digi-link-internal
								afHref="/tillganglighetsredogorelse/lista-med-tillganglighetsbrister"
								af-variation="small"
								onAfOnClick={(e) => this.linkClickHandler(e)}
							>
								Instruktioner för lista med tillgänglighetsbrister
							</digi-link-internal>

							<h3>Steg 3: Strukturera och sammanfatta er brister</h3>
							<digi-list>
								<li>
									Bestäm hur ni vill strukturera era brister. Ni kan dela upp bristerna
									utifrån tjänst/sida eller annat sätt som är naturlig för era användare.
									Vill ni ha alla brister på samma sida som själva redogörelsen eller
									uppdelat på olika undersidor?
								</li>
								<li>Sammanfatta bristerna i punktform.</li>
							</digi-list>
							<h3>Steg 4: Kontrollera era kunders kontaktvägar</h3>
							<digi-list>
								<li>
									<strong>Ni behöver inte bygga en egen kommentarsfunktion.</strong>Det
									räcker att ni länkar till formuläret för att anmäla bristande digital
									tillgänglighet i Självbetjäningsportalen.
								</li>
								<li>
									Om ni har egen kundtjänst eller använder andra kontaktvägar, kan ni
									kontakta funktionsbrevlådan för webbtillgänglighet så diskuterar vi hur
									ni gör.
								</li>
							</digi-list>

							<p>
								<digi-link-external
									afHref="https://serviceportal.arbetsformedlingen.se/sp?id=sc_cat_item&sys_id=25350c4ec08144504a4023c51e47e1a4"
									af-variation="small"
									// onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Formulär för att rapportera bristande tillgänglighet i
									Självbetjäningsportalen
								</digi-link-external>
							</p>
							<digi-link
								af-variation="small"
								afHref="mailto:webbtillganglighet@arbetsformedlingen.se"
							>
								<digi-icon-envelope slot="icon"></digi-icon-envelope>
								Funktionsbrevlådan för webbtillgänglighet
							</digi-link>

							<h3>Steg 5: Skapa en redogörelse utifrån mallen</h3>
							<digi-list>
								<li>
									Utgå från den av mallarna nedan som passar för er webbplats för att
									skapa resterande delar i redogörelsen. I mallen kan ni se vilka delar
									ni publicerar rakt av och vilka delar ni själva behöver bestämma hur ni
									ska fylla i.
								</li>
							</digi-list>
							
							<h4>Mallar för tillgänglighetsredogörelse</h4>
									<digi-list>
									<li>Webbplatser som omfattas av DOS-lagen:
									<digi-link-external
										afHref="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Tillg%C3%A4nglighetsredog%C3%B6relser/Mallar/Mall%20tillg%C3%A4nglighetsredog%C3%B6relsen%20externa%20webbplatser.docx?d=wfae57a6ca1a54f4dab4afa1704986067"
										af-variation="small"
										af-href="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Tillg%C3%A4nglighetsredog%C3%B6relser/Mallar/Mall%20tillg%C3%A4nglighetsredog%C3%B6relsen%20externa%20webbplatser.docx?d=wfae57a6ca1a54f4dab4afa1704986067"
										// onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Mall för fullständig tillgänglighetsredogörelse på SharePoint
										(Word-dokument och intern länk endast för medarbetare på
										Arbetsförmedlingen)
									</digi-link-external>
									</li>
									<li>
									Webbplatser som inte omfattas av DOS-lagen:
									<digi-link-external
										afHref="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Tillg%C3%A4nglighetsredog%C3%B6relser/Mallar/Mall%20f%C3%B6renklad%20tillg%C3%A4nglighetsredog%C3%B6relse%20interna%20webbplatser.docx?d=wb96f2badee93446d80f1dc9bed15ead9"
										af-variation="small"
										af-href="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Tillg%C3%A4nglighetsredog%C3%B6relser/Mallar/Mall%20f%C3%B6renklad%20tillg%C3%A4nglighetsredog%C3%B6relse%20interna%20webbplatser.docx?d=wb96f2badee93446d80f1dc9bed15ead9"
										// onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Mall för förenklad tillgänglighetsredogörelse på SharePoint
										(Word-dokument och intern länk endast för medarbetare på
										Arbetsförmedlingen)
									</digi-link-external>
									</li>
							</digi-list>
							<h3>Steg 6: Ladda upp och publicera den färdiga redogörelsen</h3>
							<digi-list>
								<li>Ladda upp redogörelsen under innevarande månad på ytan nedan.</li>
								<li>Publicera redogörelsen på den plats ni utsett.</li>
							</digi-list>

							<p>
								<digi-link-external
									afHref="https://space.arbetsformedlingen.se/sites/tillganglighet/Delade%20dokument/Forms/AllItems.aspx?csf=1&e=bq5ihn%2F&FolderCTID=0x0120005C2E125F40DFDF4CA509E09B8112068C&id=%2Fsites%2Ftillganglighet%2FDelade%20dokument%2FTillg%C3%A4nglighetsredog%C3%B6relser%2FRedog%C3%B6relser%20och%20checklistor%2F2023"
									af-variation="small"
									// onAfOnClick={(e) => this.linkClickHandler(e)}
								>
									Tillgänglighetsredogörelser 2023 på SharePoint (intern länk endast för
									medarbetare på Arbetsförmedlingen)
								</digi-link-external>
							</p>
						</article>
					</digi-layout-container>
				</digi-docs-page-layout>
			</host>
		);
	}
}
