import { newSpecPage } from '@stencil/core/testing';
import { DesignTokens } from './digi-docs-design-tokens';

describe('digi-docs-design-tokens', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DesignTokens],
			html: '<digi-docs-design-tokens></digi-docs-design-tokens>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-design-tokens>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-design-tokens>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DesignTokens],
			html: `<digi-docs-design-tokens first="Stencil" last="'Don't call me a framework' JS"></digi-docs-design-tokens>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-design-tokens first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-design-tokens>
    `);
	});
});
