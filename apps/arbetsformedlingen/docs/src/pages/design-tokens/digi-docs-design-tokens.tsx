import { Component,h, State } from '@stencil/core';
import state from '../../../src/store/store';
import designTokens from '../../../../../../dist/libs/arbetsformedlingen/design-tokens/styleguide/web.json';
import { TokenFormat } from '../../components/design-tokens/token-format.enum';
import { router } from '../../global/router';
import { Route } from 'stencil-router-v2';
import { LayoutBlockContainer } from '@digi/arbetsformedlingen';


type Tokens = typeof designTokens;

@Component({
	tag: 'digi-docs-design-tokens',
	styleUrl: 'digi-docs-design-tokens.scss',
	scoped: true
})
export class DesignTokens {
	Router = router;
	@State() pageName = 'Design Tokens';

	@State() tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY;
	@State() searchTerm: string = '';

	@State() colorTokens: Tokens;
	@State() borderTokens: Tokens;
	@State() layoutTokens: Tokens;
	@State() animationTokens: Tokens;

	@State() tabId: number;

	componentWillLoad() {
		this.setTabId();
		this.colorTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/color/color.brand')
			)
		];

		this.borderTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/border/border.brand')
			)
		];

		this.layoutTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/layout/layout.brand')
			)
		];

		this.animationTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/animation/animation.brand')
			)
		];
	}

	setTabId(): void {
		const tabName: string = state.router.activePath;

		switch (tabName) {
			case '/design-tokens/bibliotek':
				this.tabId = 0;
				break;
			case '/design-token/beskrivning':
				this.tabId = 1;
				break;
			case '/design-token/api':
				this.tabId = 2;
				break;
			default:
				this.tabId = 0;
				break;
		}
	}

	toggleNavTabHandler(e) {
		const tag = e.target.dataset.tag;
		this.Router.push(`${state.routerRoot}design-tokens/${tag}`);
	}
	abstractLinkHandler(e) {
		e.detail.preventDefault();
		window.open(e.target.afHref, '_blank');
	}

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/design-tokens/digi-docs-design-tokens.tsx"
				>
					<digi-layout-block afVerticalPadding>
						<digi-typography-preamble>
							Design tokens representerar alla designbeslut och är basen till all
							design i designsystemet. Det kan vara allt från färger, typografi,
							kantlinjer, avstånd med mera. Dessa skapas centralt och uppdateras från
							ett och samma ställe.
						</digi-typography-preamble>
					</digi-layout-block>
				<br />
				<br />
					<digi-layout-block afVerticalPadding>
						<digi-navigation-tabs
							af-aria-label="Innehåll för design tokens"
							af-init-active-tab={this.tabId}
						>
							<digi-navigation-tab
								onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
								data-tag="bibliotek"
								afAriaLabel="Bibliotek"
							></digi-navigation-tab>
							<digi-navigation-tab
								onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
								data-tag="beskrivning"
								afAriaLabel="Beskrivning"
							></digi-navigation-tab>
							{/* <digi-navigation-tab
							onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
							data-tag="api"
							afAriaLabel="Kod"
						></digi-navigation-tab> */}
						</digi-navigation-tabs>
						<digi-layout-block af-container={LayoutBlockContainer.NONE}>
							<this.Router.Switch>
								<Route path="/design-tokens/bibliotek">
									<digi-docs-design-tokens-library />
								</Route>
								<Route path="/design-tokens/beskrivning">
									<digi-docs-design-tokens-description />
								</Route>
							</this.Router.Switch>
						</digi-layout-block>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
