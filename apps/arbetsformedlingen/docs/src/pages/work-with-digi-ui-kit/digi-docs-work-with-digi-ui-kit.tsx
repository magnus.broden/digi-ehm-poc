import { Component, getAssetPath, h, State } from '@stencil/core';
import { InfoCardHeadingLevel, InfoCardVariation, LayoutColumnsVariation } from '@digi/arbetsformedlingen';
import { router } from '../../global/router';

@Component({
	tag: 'digi-docs-work-with-digi-ui-kit',
	styleUrl: 'digi-docs-work-with-digi-ui-kit.scss',
	scoped: true
})
export class DigiDocsWorkWithDigiUiKit {
	@State() pageName = 'Jobba med Digi UI Kit';
	Router = router;
	linkHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}
	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/work-with-digi-ui-kit/digi-docs-work-with-digi-ui-kit.tsx"
				>
					<span slot="preamble">
						Om du designar gränssnitt för Arbetsförmedlingens digitala tjänster är
						Digi UI Kit (“UI-kittet”) grunden för det arbetet. I UI-kittet hittar du
						färdiga komponenter som uppfyller kraven på tillgänglighet och är baserade
						på Arbetsförmedlingens grafiska profil. Digi Core är en spegling av
						motsvarande komponenter, fast i kod. Tillsammans utgör UI-kittet och Digi
						Core kärnan i designsystemets komponentbibliotek.
					</span>
					<digi-layout-container af-margin-bottom af-margin-top>
						<digi-info-card
							class="digi-docs-work-with-digi-ui-kit__infocard-narrow"
							afHeading="Snabbstart: Kom igång med UI-kittet"
							afHeadingLevel={InfoCardHeadingLevel.H2}
						>
							<p>
								För dig som jobbat med UI-kittet tidigare eller fått en muntlig
								genomgång.
							</p>
								<digi-list af-list-type='numbered'>
									<li>
										Beställ licens för Sketch och Abstract i{' '}
										<digi-link af-variation="small" afHref="https://serviceportal.arbetsformedlingen.se/sp?id=sc_category&sys_id=e7f5d0c57140a8504a40604d772bb541" af-href="https://serviceportal.arbetsformedlingen.se/sp?id=sc_category&sys_id=e7f5d0c57140a8504a40604d772bb541">Självserviceportalen</digi-link>
										.
									</li>
									<li>
										Ladda ner och installera programvarorna;{' '}
										<digi-link
											af-variation="small"
											afHref="https://www.sketch.com/downloads/mac/"
											af-href="https://www.sketch.com/downloads/mac/"
										>
											Sketch
										</digi-link>{' '}
										och{' '}
										<digi-link
											af-variation="small"
											afHref="https://app.abstract.com/download"
											af-href="https://app.abstract.com/download"
										>
											Abstract
										</digi-link>
										.
									</li>
									<li>
										Ladda ner och installera typsnittet{' '}
										<digi-link
											af-variation="small"
											afHref="https://space.arbetsformedlingen.se/sites/designpaarbetsformedlingen/Delade%20dokument/open-sans.zip?csf=1&e=FBgguD"
											af-href="https://space.arbetsformedlingen.se/sites/designpaarbetsformedlingen/Delade%20dokument/open-sans.zip?csf=1&e=FBgguD"
										>
											OpenSans
										</digi-link>
										.
									</li>
									<li>Skapa nytt projekt eller ladda upp en Sketchfil i Abstract.</li>
									<li>Länka UI-kittet till ditt projekt i Abstract och du är igång.</li>
								</digi-list>
						</digi-info-card>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Bakgrund och syfte</h2>
							<p>
								UI-kittet bygger på Arbetsförmedlingens samlade komponenter och har
								byggts upp under många år. Vi gör kontinuerligt omtag för att uppdatera
								funktioner, tillgänglighet och design. UI-kittet består av komponenter
								som de flesta tjänster har nytta av. Därför innehåller UI-kittet inte
								alla komponenter som finns i Arbetsförmedlingens tjänster.
							</p>
						</article>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Licenser och typsnitt</h2>
							<p>
								För att använda UI-kittet som designer behöver du ha tillgång till
								Sketch, en applikation som endast fungerar på Mac. Du behöver också
								Abstract, en applikation för Mac, men som även finns i webbversion som
								fungerar för PC. I Abstract kan du koppla UI-kittet och versionshantera
								din design. Det gör att du automatiskt får tillgång till löpande
								uppdateringar. Licensfrågor hanteras internt av Licensstöd. Har du
								problem med behörigheter kan du kontakta oss.
							</p>
							<p>
								Du behöver också ladda ner typsnittet OpenSans för att typografin skall
								stämma i UI-kittets designfiler.
							</p>
							<digi-link-button
								afHref="https://space.arbetsformedlingen.se/sites/designpaarbetsformedlingen/Delade%20dokument/open-sans.zip?csf=1&e=FBgguD"
								af-target="_blank"
							>
								Ladda ner OpenSans
							</digi-link-button>
						</article>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Hitta UI-kittet</h2>
							<p>
								UI-kittet hittar du i Abstract. Alla som har en
								@arbetsformedlingen.se-adress kommer automatiskt åt det från början. Har
								du inte redan ett konto, kommer du behöva skapa ett med din e-postadress
								från Arbetsförmedlingen. I Abstract ser du att UI-kittet är en samling
								Sketch-bibliotek. Tanken är att du ska koppla dem till dina egna projekt
								i Abstract för att kunna använda innehållet och prenumerera på
								uppdateringar.
							</p>
							<digi-link-button afHref="https://share.goabstract.com/cc87fe30-0cac-4000-8f1b-9e9faecea79d">
								UI-kittet i Abstract
							</digi-link-button>
							<br />
							<br />
							<p>
								Är du extern leverantör eller aktör som skall utforma tjänster åt
								Arbetsförmedlingen och inte har tillgång till UI-kittet i Abstract?
								Kontakta oss, så ser vi till att du får tillgång till Ui-kittet.
							</p>
						</article>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Lägga till UI-kittet</h2>
							<p>
								För att lägga till UI-kittet till ditt designprojekt, gör följande:
							</p>
							<digi-list af-list-type='numbered'>
								<li>Starta nytt projekt (eller öppna upp ett projekt) i Abstract.</li>
								<li>
									Gå till <strong>Files</strong> och klicka drop down-listan och{' '}
									<strong>Add file</strong>.<br />
									<br />
									<digi-media-image
										afWidth="500"
										af-width="500"
										afUnlazy
										af-unlazy
										afSrc={getAssetPath('/assets/images/ui-kit/add-file.png')}
										af-src={getAssetPath('/assets/images/ui-kit/add-file.png')}
										afAlt="Lägg till fil i Abstract."
										af-alt="Lägg till fil i Abstract."
									></digi-media-image>
								</li>
								<br />
								<li>
									Välj <strong>Link library</strong>... och sök{' '}
									<strong>Digi UI Kit</strong> - markera alla delar du vill använda och
									klicka <strong>Link Library</strong>.<br />
									<br />
									<digi-media-image
										afWidth="500"
										afUnlazy
										af-width="500"
										af-unlazy
										afSrc={getAssetPath('/assets/images/ui-kit/link-library.png')}
										afAlt="Länka bibliotek i Abstract."
										af-src={getAssetPath('/assets/images/ui-kit/link-library.png')}
										af-alt="Länka bibliotek i Abstract."
									></digi-media-image>
								</li>
							</digi-list>
							<p>
								Nu kan du välja vilka libraries du vill lägga till i ditt projekt. Det
								går alltid att lägga till eller ta bort libraries senare också.
							</p>
						</article>
						<br />
						<digi-info-card
							class="digi-docs-work-with-digi-ui-kit__infocard-narrow"
							afHeading="Observera"
							afHeadingLevel={InfoCardHeadingLevel.H3}
						>
							<p>
								Att använda UI-kittet och dess länkade bibliotek fungerar enbart om du
								skapar projekt via Abstract. Om du har projekt som inte är
								versionshanterade i Abstract, kommer denna länkning inte heller att
								fungera.
							</p>
						</digi-info-card>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>UI-kittets uppbyggnad</h2>
							<p>
								UI-kittet består av ett antal olika filer som är sparade som Libraries i
								Abstract. De är indelade i olika kategorier. Navigation innehåller till
								exempel alla delar som har med navigation att göra, precis som det är
								uppbyggt i Digi Core.
							</p>
							<p>
								Varje komponent är uppbyggd av fler olika element som i sin tur skapar
								en så kallad symbol i Sketch. Symbolen i sig innehåller ofta olika typer
								av dynamiska funktioner. Exempel på sådana funktioner kan vara
								möjligheten att skala om storleken, ändra texter och lägga till en ikon
								direkt via verktygsmenyn utan att behöva ändra direkt i grafiken.
							</p>
							<p>
								Samma uppbyggnad speglas i Digi Core med kod, kallat designtokens. Det
								är ett gemensamt språk för att kunna förstå både kod och design på samma
								gång.
							</p>
							<h3>Libraries</h3>
							<p>
								Det är dessa biblioteksfiler som tillsammans med symboler som gör att
								designen i UI-kittet kan återanvändas i alla andra projekt som använder
								dessa filer. Några av kategorierna som finns är till exempel:
								Färgpalett, Ikoner, Knappar, Länkar, Navigation och Stilar.
							</p>
							<p>
								Färgpalett, Stilar och Typografi utgör grunden till hela UI-kittet. De
								innehåller globala parametrar och inställningar som används av alla
								andra symboler i någon utsträckning. Dessa filer ändras sällan, men när
								ändringar väl sker, blir det stor påverkan på övriga filer.
							</p>
							<digi-media-image
								afWidth="220"
								afUnlazy
								afSrc={getAssetPath('/assets/images/ui-kit/categories.png')}
								afAlt="Kategorier i ui-kittet."
							></digi-media-image>
							<h3>Symboler</h3>
							<p>
								Symboler är Sketchs benämning på återanvändbara designkomponenter.
								UI-kittets library-filer är uppbyggda med symboler för att det ska vara
								lätt att skapa nya designkomponenter samt underhålla befintliga.
							</p>
							<p>
								Vi försöker även hålla symbolerna uppdaterade och synkade med Digi Core.
								Detta innebär att, när en komponent ändras i UI-kittet behöver
								motsvarande ändring göras även i Digi Core - och vice versa.
							</p>
							<h4>Base symbols</h4>
							<p>
								Detta är en kategori med symboler som används i en library-fil för att
								bygga upp andra symboler, men som inte ska användas fristående. Base
								symbols är uppmärkta enligt följande.
							</p>
							<digi-media-image
								afWidth="500"
								afUnlazy
								afSrc={getAssetPath('/assets/images/ui-kit/base-symbols.png')}
								afAlt="Base symbols."
							></digi-media-image>
						</article>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<digi-info-card
							class="digi-docs-work-with-digi-ui-kit__infocard-narrow"
							afHeading="Observera"
							afHeadingLevel={InfoCardHeadingLevel.H4}
						>
							<p>
								Var mycket restriktiv innan du ändrar något i UI-kittets symboler, det
								får konsekvenser i alla symboler som använder den grundkomponenten.
							</p>
						</digi-info-card>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Att använda komponenterna (symbolerna) i UI-kittet</h2>
							<h3>Komponenter</h3>
							<p>
								När du lagt in UI-kittet i ditt projekt finner du dem under symbols via{' '}
								<strong>Insert</strong> eller <strong>+</strong> i menyn. Du kan lägga
								till komponenterna direkt därifrån, eller om du vill se dem först så kan
								du välja att se en överblick med alla komponenter i översiktsvyn.
							</p>
							<digi-media-image
								afWidth="500"
								afUnlazy
								afSrc={getAssetPath('/assets/images/ui-kit/using-components.png')}
								afAlt="Lägger till komponent"
							></digi-media-image>
							<p>
								I bildexemplet visas hur man lägger till symbolen Infokort av typen Mer
								information - Desktop. Den visar även kategorien base symbols, som
								alltså inte ska användas för att bygga upp ditt gränssnitt.
							</p>
							<h3>Textstilar</h3>
							<p>
								Lägg till fördefinierade texttyper via <strong>Insert</strong> eller{' '}
								<strong>+</strong> under rubriken <strong> Text styles</strong>. Du kan
								även sätta eller byta text style på en text efter att du skrivit den.
							</p>
							<digi-media-image
								afWidth="750"
								afUnlazy
								afSrc={getAssetPath('/assets/images/ui-kit/text-styles.png')}
								afAlt="Textstilar"
							></digi-media-image>
							<p>
								I bildexemplet visas hur en en textstil läggs till från paletten
								Appearances, i detta fall en vänsterställd länk för desktop med
								textstorlekt 16 px.
							</p>
							<h3>Lagerstilar</h3>
							<p>
								Du kan sätta utseende på bakgrundselement, såsom färg eller skuggning,
								genom att använda <strong>Layer Styles</strong> - där hittar du
								UI-kittets lagerstilar som i sin tur använder färgpaletten.
							</p>
							<digi-media-image
								afWidth="750"
								afUnlazy
								afSrc={getAssetPath('/assets/images/ui-kit/layer-styles.png')}
								afAlt="Lagerstilar"
							></digi-media-image>
							<p>
								I bildexemplet visas hur man lägger till en bakgrundsfärg på en
								rektangulär yta med varianten primär bakgrundsfärg.
							</p>
							<h3>Färgpalett</h3>
							<p>
								Färgpaletten är den samling grundfärger som alla andra symboler och
								komponenter och symboler är uppbyggda på. Paletten är baserad på
								myndighetens varumärke och dess färger; med justeringar för toningar,
								gråskalor, funktionsfärger och annat som det finns behov av i digitala
								produkter.
							</p>
							<p>
								Det finns även en färgpalett för{' '}
								<digi-link
									afHref="/grafisk-profil/farger/tryck-farger"
									af-target="_self"
									af-variation="small"
									onAfOnClick={(e) => this.linkHandler(e)}
								>
									tryck
								</digi-link>
								, men den går inte att hantera på samma sätt som den digitala.
							</p>
							<p>
								Du hittar färgpaletten både i Abstract och i Sketch. De visar samma
								färger och samma namn på färgerna, men sättet de visas upp på skiljer
								sig åt.
							</p>
							<digi-media-image
								afWidth="700"
								afUnlazy
								afSrc={getAssetPath('/assets/images/ui-kit/color-palette-abstract.png')}
								afAlt="Färgpaletten i Abstract"
							></digi-media-image>
							<p>Färgpaletten i Abstract</p>
							<digi-media-image
								afWidth="700"
								afUnlazy
								afSrc={getAssetPath('/assets/images/ui-kit/color-palette-sketch.png')}
								afAlt="Färgpaletten i Sketch"
							></digi-media-image>
							<p>Färgpaletten i Sketch</p>
							<digi-info-card
								class="digi-docs-work-with-digi-ui-kit__infocard-narrow"
								afHeading="Observera"
								afHeadingLevel={InfoCardHeadingLevel.H4}
							>
								<p>
									Färgpaletten ska inte användas för att sätta bakgrundsfärger, ramfärger
									eller annat - det ska du använda lagerstilar till.
								</p>
							</digi-info-card>
							<h3>Overrides</h3>
							<p>
								Inställningar som du hittar under <strong>Overrides</strong> på
								komponenten är de inställningar du även kan göra i kod.
							</p>
							<br />
							<digi-info-card
								class="digi-docs-work-with-digi-ui-kit__infocard-narrow"
								afHeading="Gör ej så här"
								afHeadingLevel={InfoCardHeadingLevel.H2}
								afVariation={InfoCardVariation.SECONDARY}
							>
								<p>
									Bryt inte loss symbolen! Om du ändå gör detta kommer designen att
									skilja sig åt när implementationen sker i Digi Core. Dessutom kommer
									uppdateringar för den symbolen <strong>inte längre fungera</strong>.
								</p>
							</digi-info-card>
							<br />
							<br />
							<digi-media-image
								afWidth="700"
								afUnlazy
								afSrc={getAssetPath('/assets/images/ui-kit/customize-component.png')}
								afAlt="Ändra fördefinierade värden i komponenten."
							></digi-media-image>
							<p>
								Bildexemplet visar hur fördefinierade värden i komponenten kan skrivas
								över i gränssnittet i Sketch när symbolen ska användas i design.
							</p>

							<h3>Överblick av komponenter</h3>
							<p>
								Under rubriken Komponenter här på designsystemets dokumentationssida får
								du en bra överblick och indexering över vilka komponenter som finns
								tillgängliga. Här får du också möjlighet att testa komponenten live, se
								varianter, eventuella riktlinjer och designmönster.
							</p>
						</article>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Lägga till nya komponenter </h2>
							<p>
								Tillägg av nya komponenter sker efter behov och görs tillsammans med
								teamet som har behovet. Vi gör det med hjälp av vår samarbetsmodell. Vi
								kartlägger behoven och ser om det finns fler vi bör samarbeta med.
								Designen görs i Sketch och läggs upp i UI-kittet. Utvecklingen sker i
								Digi Core. Målet är alltid att hålla dessa två synkroniserade, eftersom
								de gemensamt utgör en komponent i designsystemet.
							</p>
						</article>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Komplettera eller justera</h2>
							<p>
								Saknar du en färg, storlek eller någon annan del i en komponent och vill
								bidra med en komplettering eller justering? Då kan du själv skapa en
								branch på UI-kittet i Abstract. Gör dina ändringar och markera sedan
								branchen med Request Review så kan någon i designsystemteamet godkänna
								när motsvarande justering är gjord i Digi Core (kod).
							</p>
						</article>
					</digi-layout-container>
					<digi-layout-container af-margin-bottom>
						<article>
							<h2>Har du hittat en bugg?</h2>
							<p>
								Vill du dela med dig om hur du löst en bugg eller annan fråga? <br />Skriv i
								gruppen på intranätet eller kontakta oss via vår funktionsbrevlåda.
							</p>
							<digi-link
								af-variation="small"
								afHref={`mailto:designsystem@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan`}
								class="digi-docs__accessibility-link"
							>
								<digi-icon-envelope slot="icon"></digi-icon-envelope>
								Mejla designsystem@arbetsformedlingen.se
							</digi-link>
							<br />
							<digi-link
								af-target="_blank"
								af-variation="small"
								afHref="https://start.arbetsformedlingen.se/grupper/grupp/designsystem/"
							>
								<digi-icon-external-link-alt slot="icon"></digi-icon-external-link-alt>
								Designsystemet på intranätet
							</digi-link>
						</article>
					</digi-layout-container>
					<digi-layout-block af-variation='symbol' af-container='static'>
						<digi-info-card
							afHeading="Fördjupande information och resurser för dig som jobbar med Arbetsförmedlingens design"
							afHeadingLevel={InfoCardHeadingLevel.H2}
						>
							{/* <h3>Designmönster</h3>
											<p>Vägledning för dig som utformar nya tjänster för Arbetsförmedlingen. </p>
											<digi-link-internal
										afHref=""
										af-target="_self"
										af-variation="large"
										onAfOnClick={(e) => this.linkHandler(e)}
									>
										Länk till Designmönster
									</digi-link-internal> */}
							
							<digi-layout-columns af-variation={LayoutColumnsVariation.THREE} class="digi-docs-work-with-digi-ui-kit__columns">
								<div>
									<h3>Grafiska profilen</h3>
									<p>
										Lär dig mer om varumärket Arbetsförmedlingen. Logotyp, färger och
										typografi. Här finner du också riktlinjer om grafik, illustrationer och
										bildspråk.
									</p>
									<digi-link-internal
										afHref="/grafisk-profil/om-grafisk-profil"
										af-target="_self"
										af-variation="large"
										onAfOnClick={(e) => this.linkHandler(e)}
									>
										Vår grafiska profil
									</digi-link-internal>
								</div>
								<div>
									<h3>Tillgänglighet</h3>
									<p>
										Vilka krav behöver Arbetsförmedlingen uppfylla? Verktyg för att kunna
										sammanställa en tillgänglighetsredogörelse.
									</p>
									<digi-link-internal
										afHref="/tillganglighet-och-design/om-digital-tillganglighet"
										af-target="_self"
										af-variation="large"
										onAfOnClick={(e) => this.linkHandler(e)}
									>
										Om tillgänglighet
									</digi-link-internal>
								</div>
								<div>
									<h3>Språk</h3>
									<p>
										Hur skriver man klarspråk? Hur namnger vi våra tjänster? Microcopy och
										översättningar.
									</p>
									<digi-link-internal
										afHref="/sprak/digi-docs-about-copy-languages"
										af-target="_self"
										af-variation="large"
										onAfOnClick={(e) => this.linkHandler(e)}
									>
										Om vårt språk
									</digi-link-internal>
								</div>
							</digi-layout-columns>
						</digi-info-card>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
