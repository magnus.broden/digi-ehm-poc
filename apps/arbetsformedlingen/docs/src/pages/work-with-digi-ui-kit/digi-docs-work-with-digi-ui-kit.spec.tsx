import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsWorkWithDigiUiKit } from './digi-docs-work-with-digi-ui-kit';

describe('digi-docs-work-with-digi-ui-kit', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsWorkWithDigiUiKit],
      html: '<digi-docs-work-with-digi-ui-kit></digi-docs-work-with-digi-ui-kit>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-work-with-digi-ui-kit>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-work-with-digi-ui-kit>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsWorkWithDigiUiKit],
      html: `<digi-docs-work-with-digi-ui-kit first="Stencil" last="'Don't call me a framework' JS"></digi-docs-work-with-digi-ui-kit>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-work-with-digi-ui-kit first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-work-with-digi-ui-kit>
    `);
  });
});
