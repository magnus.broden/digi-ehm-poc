import { Component, h, State } from '@stencil/core';
import {
	CodeBlockLanguage,
	CodeLanguage,
	CodeVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-docs-work-with-digi-core-react',
	styleUrl: 'digi-docs-work-with-digi-core-react.scss',
	scoped: true
})
export class WorkWithDigiCoreReact {
	@State() pageName = 'Digi Core React';

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/work-with-digi-core-react/digi-docs-work-with-digi-core-react.tsx"
				>
					<span slot="preamble">
						Biblioteket Digi Core React är ett lager på webbkomponenterna i Digi Core
						som förenklar utvecklingen i React. Digi Core React är en så kallad
						output-target från StencilJS som gör så att vi kan använda samma kodbas i
						alla våra kodbibliotek.
					</span>
					<digi-layout-container af-margin-top af-margin-bottom>
						<article>
							<h2>Installera Digi Core React</h2>
							<h3>NPM</h3>
							<p>
								Börja med att konfigurera Nexus. Lägg till en fil som heter
								<digi-code
									afVariation={CodeVariation.LIGHT}
									afCode=".npmrc"
								></digi-code>{' '}
								i samma mapp som din{' '}
								<digi-code
									afVariation={CodeVariation.LIGHT}
									afCode="package.json"
								></digi-code>{' '}
								och lägg till den här raden:
								<digi-code
									afVariation={CodeVariation.LIGHT}
									afCode="@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/"
								></digi-code>
							</p>
							<p>
								Installera Digi Core React genom att köra
								<br />
								<digi-code
									afLanguage={CodeLanguage.BASH}
									afVariation={CodeVariation.LIGHT}
									afCode="npm i @digi/arbetsformedlingen @digi/arbetsformedlingen-react"
								/>
								<br />i roten av ditt react-projekt.
							</p>
							<h3>Importera komponenterna</h3>
							<p>
								För att få med core-komponenterna i react-appen så behöver du importera
								komponenterna så här:{' '}
							</p>
							<digi-code-block
								afLanguage={CodeBlockLanguage.TYPESCRIPT}
								afCode={`import { DigiButton } from '@digi/arbetsformedlingen-react';
import { ButtonVariation } from '@digi/arbetsformedlingen';

export function App() {

	function myFunction(e) {
		console.log(e)
	}

	return (
		<DigiButton onAfOnClick={myFunction} afVariation={ButtonVariation.PRIMARY}>Skicka</DigiButton>
	);
}
export default App;
								`}
							></digi-code-block>
							<h3>Lägg till styles</h3>
							<p>
								Du behöver också lägga till styles i din app för att få rätt utseende.
								Detta gör du genom att importera vår css-fil<br />
								<digi-code
									afLanguage={CodeLanguage.SCSS}
									afVariation={CodeVariation.LIGHT}
									afCode="@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css';"
								/>
							</p>
							<h3>Lägg till typsnitt</h3>
							<p>
								För att läsa in typsnitten behöver man läsa in CSS/SCSS-filen i t.ex.
								din globala SCSS-filen och ändra en variabel så här:
								<digi-code
                  afLanguage={CodeLanguage.SCSS}
									afVariation={CodeVariation.LIGHT}
									af-code={`
$font-path: "~node_modules/@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/assets/fonts/" !default;

@import '@digi/arbetsformedlingen/dist/digi-arbetsformedlingen/fonts/src/fonts.scss';`}
								/>
							</p>
						</article>
					</digi-layout-container>

					<digi-layout-container af-margin-bottom>
						<article>
							<h3>Använda enums</h3>
							<p>
								Du finner alla enums under Digi Core och kan referera dom såhär:
								<br />
								<digi-code
									afLanguage={CodeLanguage.TYPESCRIPT}
									afVariation={CodeVariation.LIGHT}
									afCode="import { EnumName1, EnumName2 } from '@digi/arbetsformedlingen';"
								/>
							</p>
						</article>
					</digi-layout-container>
				</digi-docs-page-layout>
			</host>
		);
	}
}
