import { Component, h } from '@stencil/core';

import { InfoCardHeadingLevel, InfoCardVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-about-plain-language',
  styleUrl: 'digi-docs-about-plain-language.scss',
  scoped: true
})
export class DigiDocsAboutPlainLanguage {
  

  render() {
    return (
      <div class="digi-docs-about-plain-language">
        <digi-docs-page-layout
          af-edit-href="pages/digi-docs-about-plain-language/digi-docs-about-plain-language.tsx"
        >
          <digi-typography>
            <digi-layout-block>
              <digi-typography-heading-jumbo af-text="Namngivning och klarspråk"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Att använda enhetliga, tydliga och begripliga namn, benämningar
                och termer är viktigt för att alla ska förstå vad
                Arbetsförmedlingen erbjuder och vad vi kan hjälpa till med. Det
                är också viktigt för förtroendet för oss och för vårt varumärke.
              </digi-typography-preamble>
              <br />
              <p>
                Våra riktlinjer beskriver hur vi använder namn, benämningar och
                termer. Riktlinjerna omfattar också organisationsnamn, titlar,
                mejlsignaturer och namn på funktionsbrevlådor.
              </p>
              <digi-info-card
                afHeading="Riktlinjer"
                afVariation={InfoCardVariation.PRIMARY}
                afHeadingLevel={InfoCardHeadingLevel.H2}
              >
                <digi-list>
                  <li>
                    Välj konkreta och tydliga namn, benämningar och termer som
                    beskriver verksamheten eller tjänsternas och produkternas
                    innehåll eller funktion.
                  </li>
                  <li>
                    Fokus på kunden eller intressenten, det vill säga ta fram
                    namn, benämningar och termer utifrån kundens perspektiv.
                  </li>
                  <li>
                    Välj namn, benämningar och termer som ger en sammanhållen
                    kundupplevelse och följer samma logik inom hela verksamheten.
                  </li>
                  <li>
                    Välj svenska namn, benämningar och termer när det är möjligt.
                  </li>
                  <li>Var konsekvent med stora och små bokstäver.</li>
                  <li>Använd ett så naturligt språk som möjligt.</li>
                </digi-list>
              </digi-info-card>
              <br />
              <digi-info-card
                afHeading="Då blir det fel"
                afVariation={InfoCardVariation.SECONDARY}
                afHeadingLevel={InfoCardHeadingLevel.H2}
              >
                <digi-list>
                  <li>
                    Ett enskilt substantiv som namn på en tjänst (ex. Ella,
                    Albin, Päron, Klockan, Vinden).
                  </li>
                  <li>Versaler i namnet på en tjänst (ex. FÖRMÅNSPORTALEN).</li>
                  <li>
                    Förkortningar och facktermer som inte är lätta att förstå
                    för utomstående användare (ex. EYS, DS handläggare).
                  </li>
                  <li>
                    En underdomän och namn på tjänsten har olika namn (ex.
                    future.arbetsformedlingen.se med namnet Nya jobb).
                  </li>
                  <li>Utländska namn på tjänster (ex. Come and go).</li>
                  <li>
                    Blanda små och stora bokstäver (ex. MiA, Distans eller På
                    kontoret).
                  </li>
                  <li>
                    Konstlat språk (ex. Verifieringhandläggning av kontroll i
                    arbetsgivartjänstfunktion).
                  </li>
                </digi-list>
              </digi-info-card>
              <h2>Namnforum</h2>
              <p>
                När verksamheten behöver ett nytt namn, en benämning eller en
                term som är kopplad till verksamheten och som påverkar
                myndighetens varumärke ska ärendet beredas i myndighetens
                namnforum innan namnet börjar användas. Ärendet kan gälla vad vi
                ska kalla ett erbjudande, en tjänst, en aktivitet, ett program,
                en metod eller en produkt. När namnforum har berett ärendet
                fattar kommunikations­avdelningen beslut. Forumet består av
                representanter från olika delar av verksamheten och tillsätts av
                kommunikations­direktören.
              </p>
              <p>
                Vid behov kan forumet även behöva justera namn, benämningar och
                termer som har börjat användas utan att namnforum har godkänt
                namnen. Läs mer på intranätet om namnforum och hur du väljer
                rätt namn för verksamheten.
              </p>
              <digi-link
                af-variation="small"
                afHref={`mailto:namnforum@arbetsformedlingen.se`}
              >
                <digi-icon-envelope
                  slot="icon"
                  aria-hidden="true"
                ></digi-icon-envelope>
                Kontakta namnforum via e-post
              </digi-link>
              <h2>Utländska termer och uttryck</h2>
              <p>
                I språklagen står det att svenska är huvudspråk i Sverige.
                Använd därför svenska förklarande termer så långt det går.
                Undvik att fastna i våra interna begrepp som läsaren inte
                förstår. Det är viktigt inte minst ur ett
                tillgänglighetsperspektiv. Inom många privata branscher skapas
                trender bland termer och uttryck, och många lockas av att haka
                på dem för att hänga med i utvecklingen. Dessa termer är dock
                exkluderande för dem som inte förstår dem. Dessutom passar de
                oftast inte in i det svenska böjningsmönstret.
              </p>
              <p>
                Det går naturligtvis inte alltid att hitta svenska termer, men
                på Arbetsförmedlingen undviker vi utländska ord när det redan
                finns svenska ord som fungerar. Kontakta myndighetens namnforum
                innan du börjar använda en ny term som är kopplad till
                verksamheten och som påverkar myndighetens varumärke.
              </p>
              <p>
                <strong>Tre enkla exempel:</strong>
              </p>
              <p>
                Skriv innehåll i stället för content, publicera i stället för gå
                live och talanghantering i stället för talent management.
              </p>
              <h2>Organisationsnamn</h2>
              <p>
                När du kommunicerar externt använder du namnet
                Arbetsförmedlingen, kort och gott, utan tillägg. Det gäller alla
                externa texter och trycksaker samt skyltar, visitkort och texter
                på webben.
              </p>
              <p>
                <strong>
                  Använd aldrig förkortningen Af eller AF i skrift.
                </strong>
              </p>
              <p>
                Externt kommunicerar du avdelningar, enheter och sektioner
                endast när det är relevant för mottagaren, till exempel för att
                beskriva för medierna var någon jobbar. Läs mer om
                organisationsnamn myndighetens namnhandbok.
              </p>

              <digi-link
                af-variation="small"
                // afHref={`https://start.arbetsformedlingen.se/download/18.33d6694417a06a264632034/1625145891979/Namn%20och%20termer%20inom%20Arbetsf%C3%B6rmedlingen.pdf`}
                afHref={`https://space.arbetsformedlingen.se/sites/byggstenendesignsystem/Delade%20dokument/Resurser/Namn%20och%20termer%20inom%20Arbetsf%C3%B6rmedlingen.pdf?d=w5729249f0137477fbf050035977e4b18&csf=1&e=lfD6La`}
                afTarget={`_blank`}
              >
                <digi-icon-file-pdf
                  slot="icon"
                  aria-hidden="true"
                ></digi-icon-file-pdf>
                Handbok: Namn och termer inom Arbetsförmedlingen (PDF)
              </digi-link>
            </digi-layout-block>
          </digi-typography>
          <br />
        </digi-docs-page-layout>
      </div>
    );
  }
}
