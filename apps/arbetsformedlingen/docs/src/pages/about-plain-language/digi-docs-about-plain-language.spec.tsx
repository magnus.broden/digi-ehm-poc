import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutPlainLanguage } from './digi-docs-about-plain-language';

describe('digi-docs-about-plain-language', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutPlainLanguage],
      html: '<digi-docs-about-plain-language></digi-docs-about-plain-language>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-plain-language>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-plain-language>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutPlainLanguage],
      html: `<digi-docs-about-plain-language first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-plain-language>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-plain-language first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-plain-language>
    `);
  });
});
