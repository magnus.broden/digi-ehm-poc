import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';
import state from '../../store/store';
@Component({
	tag: 'digi-docs-about-design-pattern-forms-validation',
	styleUrl: 'digi-docs-about-design-pattern-forms-validation.scss',
	scoped: true
})
export class AboutDesignPatternFormsValidation {
	Router = router;
	@State() pageName = 'Validering i formulär';

	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<host>
				<br />
				<br />
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-forms-validation/digi-docs-about-design-pattern-forms-validation.tsx"
				>
					<br />
					<br />
					<span slot="preamble">
						Validering i formulärfält identifierar och hjälper till att korrigera
						eventuella hinder för användarens inmatning av data.
					</span>
					<br />
					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<br />
						<h3>Validering av formulärkomponenter.</h3>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									Validering ska hjälpa användaren att snabbt och smidigt dela rätt
									information till oss. Vi guidar användaren med väl avvägda
									valideringsmeddelanden. Vi tar en sak i taget fram tills att vi lotsat
									kunden till avslut. Inmatningsfältet kan ha fyra olika statusar.
									Neutral (som är den förvalda valideringstypen), felaktig, varning
									(används sällan) och godkänd.
								</p>
							</div>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern_image_wrapper">
										<digi-media-image
											afUnlazy
											afSrc="/assets/images/pattern/validation/validation-pattern-example-1.svg"
											afAlt="Exempel på fält som validerar korrekt enligt krav på format."
										></digi-media-image>
									</div>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<br />
						<br />
						<h3> Validering i det enskilda fältet. (Inline validation) </h3>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									När vi kan identifiera ett fel, dvs när ett fält validerar mot en
									databas, eller ett fält som har en formaterings-regel validerar vi
									”felaktig” när användaren lämnar fältet. Vi validerar företrädelsevis
									enbart ”godkänt” i ett fält, om det tidigare validerat ”felaktigt”. Om
									ett fält är korrekt ifyllt på första försöket, visar vi inte någon
									visuell validering alls, fältet förblir neutralt. Vi gör det detta av
									två anledningar. För att inte få ”information overload” med redundanta
									”godkända” valideringar i ett formulär. Och för att många fält inte kan
									validera grönt, då det inte går att sätta några (relevanta) regler på
									fältet (utöver att det är ifyllt).
								</p>
							</div>
							<div>
								<digi-layout-block
									af-variation="primary"
									afVerticalPadding
									afMarginBottom
								>
									<div class="pattern_image_wrapper">
										<digi-media-image
											afUnlazy
											afSrc="/assets/images/pattern/validation/validation-pattern-example-2.svg"
											afAlt="Exempel på obligatisk fält som valideras felaktigt enligt krav på format och ett felmeddelande visas."
										></digi-media-image>
									</div>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<br />
						<br />
						{/* <digi-layout-columns af-variation="two"> */}
						<h3> Fel-validering efter skickat formulär. (Post submit validation)</h3>
						<p>
							Obligatoriska fält som användaren lämnat utan att fylla i, validerar vi
							företrädelsevis efter att användaren skickat in formuläret. Då samlas
							alla ”rester” i en felmeddelanderuta högst upp i formuläret (vilket är
							tillgänglighets-praxis). Rutan har ankarlänkar ner till varje enskilt
							fält som behöver justeras. De enskilda felaktiga fälten är markerade
							”röda” för att de ska hittas snabbt. När fältet är ifyllt går de över
							till ”neutral”.
						</p>

						{/* </digi-layout-columns> */}
						<br />
						<br />
						<h3>Kvittens efter inskickat formulär. (Post submit validation)</h3>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									{' '}
									Använd en kvittens för att bekräfta ett korrekt slutfört formulär
									(eller annat flöde). T.ex så kan användaren ha genomfört en inskrivning
									eller annat viktigt formulär-flöde som kan vara en obligatorisk uppgift
									för hen att utföra. Då är det mycket viktigt att tydligt signalera att
									uppgiften är klar. En mycket stor andel av våra inkommande samtal
									handlar om att vi ska bekräfta att vår kund lyckats skicka in rätt
									uppgifter till oss.
									<digi-link
										afHref="/komponenter/digi-form-receipt/oversikt"
										afTarget="self"
										af-variation="small"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Se kvittenskomponent
									</digi-link>
								</p>
							</div>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern_image_wrapper">
										<digi-media-image
											afUnlazy
											afSrc="/assets/images/pattern/validation/validation-pattern-example-3.svg"
											afAlt="Exempel på kvittens efter lyckad inskickning."
										></digi-media-image>
									</div>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<br />
						<br />
						<h3> Valideringstext och språk</h3>
						<p>
							{' '}
							När användarens input i fältet inte är rätt, ger vi feedback med kort,
							beskrivande återkoppling. Skriv inte längre än en mening. Var så specifik
							som möjligt. Undvik tekninsk jargong.
						</p>
						<br />
						<br />
						<h3>Olika sorters fält (Field restrictions)</h3>
						<p>
							Se nedan <a href="#validation__table">flödesschema</a> för olika sorters
							fälts, och dess olika förutsättningar att validera eller att inte
							validera. Denna tabell är inte fullständig, ej heller så är föreslagna
							regler allomfattande för alla tänkbara olika scenarior. Vissa edge-cases
							mår bättre av att välja en justerad validerings-lösning.
						</p>
						<br />
						<br />
						<h3>Liveexempel i PoC</h3>
						<p>
							Testa en PoC och liveexempel på formulär med validering. Här har vi ett
							exempel på olika sorters fält, och hur de fungerar ihop med validering.
						</p>
						<br />
						<digi-link-button
							afHref="https://validering-poc.netlify.app/validation-poc/poc-v5"
							af-target="_blank"
							af-size="medium"
							af-variation="primary"
						>
							PoC med validering
						</digi-link-button>
						{/* </div> */}

						<div>
							{/* <digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern_image_wrapper">
										<digi-media-image
											afUnlazy
											afSrc="/assets/images/pattern/validation/validation-pattern-example-1.svg"
											afAlt="Exempel på fält som validerar korrekt enligt krav på format."
										></digi-media-image>
									</div>
								</digi-layout-block> */}
							<br />

							{/* <digi-layout-block
									af-variation="primary"
									afVerticalPadding
									afMarginBottom
								>
									<div class="pattern_image_wrapper">
										<digi-media-image
											afUnlazy
											afSrc="/assets/images/pattern/validation/validation-pattern-example-2.svg"
											afAlt="Exempel på obligatisk fält som valideras felaktigt enligt krav på format och ett felmeddelande visas."
										></digi-media-image>
									</div>
								</digi-layout-block> */}

							{/* <digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern_image_wrapper">
										<digi-media-image
											afUnlazy
											afSrc="/assets/images/pattern/validation/validation-pattern-example-3.svg"
											afAlt="Exempel på kvittens efter lyckad inskickning."
										></digi-media-image>
									</div>
								</digi-layout-block> */}
							<br />
						</div>
						{/* </digi-layout-columns> */}
						<br />
						<br />
						<br />
						<digi-layout-container>
							<div class="validation__table__title">
								{' '}
								<h3>Typer av fält och förenklat flödesschema</h3>
							</div>

							<div class="validation__table__container" id="validation__table">
								<div class="validation__table__item">
									<div class="validation__table__item__text">
										<div class="validation__table__item__text_title">
											<h4>Frivilligt fält</h4>
										</div>
										<div class="validation__table__item__text_paragraph">
											<p>
												Fält som inte har några regler, varken att det ska vara ifyllt eller
												har några formateringsregler etc.
											</p>
										</div>
									</div>
									<div class="validation__table__item__image">
										<digi-media-image
											afUnlazy
											afFullwidth
											// afWidth="400"
											afSrc="/assets/images/pattern/validation/Frivilligt-fält-exemple.svg"
											afAlt="Friviligt fält som ger positiv kvittens efter inskickning."
										></digi-media-image>
									</div>
								</div>

								<div class="validation__table__item">
									<div class="validation__table__item__text">
										<div class="validation__table__item__text_title">
											{' '}
											<h4>Obligatoriskt fält</h4>
										</div>
										<div class="validation__table__item__text_paragraph">
											<p>
												Bekräftar att fältet är ifyllt, men som ej kollar av någon
												formateringsregler etc.
											</p>
										</div>
									</div>
									<div class="validation__table__item__image">
										<digi-media-image
											afUnlazy
											afFullwidth
											// afWidth="600"
											afSrc="/assets/images/pattern/validation/Obligatoriskt-fält-exemple.svg"
											afAlt="Obligatorisk fält som valideras felaktigt ger ett felmeddelande vid inskickning och behöver rätt inmatning för validering."
										></digi-media-image>
										{/* <digi-media-image
											afUnlazy
											 
											afWidth="600"
											afSrc="/assets/images/pattern/validation/Obligatoriskt-fält-exemple-2.svg"
											afAlt="..."
										></digi-media-image> */}
									</div>
								</div>

								<div class="validation__table__item">
									<div class="validation__table__item__text">
										<div class="validation__table__item__text_title">
											<h4>Bekräftar mot formatering</h4>
										</div>
										<div class="validation__table__item__text_paragraph">
											<p>
												Fältet hjälper användaren att formatera informationen som vi önskar.
												Vi checkar t.ex att ”@-tecken” används och att domänen är med
												(.se/.com/.nu). Eller har koll på antal siffror i ett nummer, etc. )
											</p>
										</div>
									</div>
									<div class="validation__table__item__image">
										<digi-media-image
											afUnlazy
											afFullwidth
											// afWidth="600"
											afSrc="/assets/images/pattern/validation/Bekräftar-mot-formatering-exemple.svg"
											afAlt="Fältet ger ett felaktigt valideringsmedelande tills inmatningen följer kravet på format."
										></digi-media-image>
										{/* <digi-media-image
											afUnlazy
											 
											afWidth="600"
											afSrc="/assets/images/pattern/validation/Bekräftar-mot-formatering-exemple-2.svg"
											afAlt="..."
										></digi-media-image> */}
									</div>
								</div>
								<div class="validation__table__item">
									<div class="validation__table__item__text">
										<div class="validation__table__item__text_title">
											<h4>Bekräftar mot någon form av databas </h4>
										</div>
										<div class="validation__table__item__text_paragraph">
											<p>
												Fält som bekräftar mot ett externt eller internt system eller
												register, etc
											</p>
										</div>
									</div>
									<div class="validation__table__item__image">
										<digi-media-image
											afUnlazy
											afFullwidth
											// afWidth="600"
											afSrc="/assets/images/pattern/validation/Bekräftar-mot-databas-exemple.svg"
											afAlt="Fältet validerar felaktigt så länge inmatingen inte bekräftas i en databas innan inskickningen."
										></digi-media-image>
										{/* <digi-media-image
											afUnlazy
										 
											afWidth="600"
											afSrc="/assets/images/pattern/validation/Bekräftar-mot-databas-exemple-2.svg"
											afAlt="..."
										></digi-media-image> */}
									</div>
								</div>
								<div class="validation__table__item">
									<div class="validation__table__item__text">
										<div class="validation__table__item__text_title">
											<h4>GDPR</h4>
										</div>
										<div class="validation__table__item__text_paragraph">
											<p>
												Tänk på att vissa fält som kan validera mot en databas inte alltid
												ska göra det, med tanke på dataskyddsskäl.
											</p>
										</div>
									</div>
									<div class="validation__table__item__image">
										<digi-media-image
											afUnlazy
											// afWidth="300"
											afFullwidth
											afSrc="/assets/images/pattern/validation/GDPR.svg"
											afAlt="Exempel på valideringmedelande av fält när det inte är lämpligt av dataskyddsskäl."
										></digi-media-image>
									</div>
								</div>
								<div class="validation__table__item">
									<div class="validation__table__item__text">
										<div class="validation__table__item__text_title">
											<h4>Varning </h4>
										</div>
										<div class="validation__table__item__text_paragraph">
											<p>
												{' '}
												Ett fält som inte kan stoppa ett genomfört flöde, men som vi vill
												råda en användare att skriva/klicka i.
											</p>
										</div>
									</div>
									<div class="validation__table__item__image">
										<digi-media-image
											afUnlazy
											afFullwidth
											// afWidth="auto"

											afSrc="/assets/images/pattern/validation/Varning-exemple.svg"
											afAlt="Fält som rekommenderas att fyllas i och lämnats tomt pausar inskickningen, och får ett varningmeddelande."
										></digi-media-image>
										{/* <digi-media-image
											afUnlazy
											afWidth="auto"
											afSrc="/assets/images/pattern/validation/Varning-exemple-2.svg"
											afAlt="..."
										></digi-media-image> */}
									</div>
								</div>
							</div>
							<br />
							<br />
							<br />

							<h3>Bakgrund till lösning</h3>
							<p>
								Tidigare iteration av valideringslösning påvisade problem kring hur en
								grön validering fungerade. Den blev fort mycket påträngande och
								överanvänd. En annan utmaning är att olika fält, som har mycket olika
								förutsättningar för validering ska hanteras när de visas tillsammans.
								Lösningen ovan är tänkt att ge ett mer homogent intryck för användaren
								trots att fälten fungerar olika på ”baksidan”.{' '}
							</p>
						</digi-layout-container>
						<br />
						<br />
						<br />
						<br />
						<hr></hr>
						<br />
						<br />
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<h4> Designmönster</h4>
								<p>Se relaterade designmönster</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/formular"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Formulär
									</digi-link-button>
								</digi-docs-related-links>
							</div>
							<div>
								<h4>Komponenter</h4>
								<p>Se relaterade komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/komponenter/digi-form-input/oversikt"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Inmatningsfält komponent
									</digi-link-button>
								</digi-docs-related-links>{' '}
								{''}
								<br />
								<digi-docs-related-links>
									<digi-link-button
										afHref="/komponenter/digi-form-validation-message/oversikt"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Valideringsmeddelande komponent
									</digi-link-button>
								</digi-docs-related-links>
								{''}
								<br />
								<digi-docs-related-links>
									<digi-link-button
										afHref="/komponenter/digi-form-error-list/oversikt"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Felmeddelandelista komponent
									</digi-link-button>
								</digi-docs-related-links>
							</div>
							<br />
						</digi-layout-columns>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
