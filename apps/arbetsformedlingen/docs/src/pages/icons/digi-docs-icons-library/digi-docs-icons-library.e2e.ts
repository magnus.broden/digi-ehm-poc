import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-icons-library', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-docs-icons-library></digi-docs-icons-library>'
    );

    const element = await page.find('digi-docs-icons-library');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-docs-icons-library></digi-docs-icons-library>'
    );

    const element = await page.find('digi-docs-icons-library >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
