import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'digi-docs-icons-example',
  styleUrl: 'digi-docs-icons-example.scss'
})
export class DigiDocsIconstExample {
  @State() componentName: string;

  render() {
    return (
      <div class="digi-docs-icons-example">
        <digi-docs-page-layout >
          <digi-layout-block>
            <digi-typography>
              <h3>Standard</h3>
            </digi-typography>
            <digi-code-example
              af-code={`<digi-icon-archive 
            class="digi-icon-archive" 
            slot="icon"
            ></digi-icon-archive>`}
            >
              <digi-icon-archive
                class="digi-icon-archive"
                slot="icon"
              ></digi-icon-archive>
            </digi-code-example>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
