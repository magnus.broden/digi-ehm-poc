import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsIcons } from './digi-docs-icons';

describe('digi-docs-icons', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsIcons],
      html: '<digi-docs-icons></digi-docs-icons>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-icons>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-icons>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsIcons],
      html: `<digi-docs-icons first="Stencil" last="'Don't call me a framework' JS"></digi-docs-icons>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-icons first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-icons>
    `);
  });
});
