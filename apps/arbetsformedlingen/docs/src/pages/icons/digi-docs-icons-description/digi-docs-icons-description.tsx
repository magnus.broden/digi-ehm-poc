import { Component, h, State, getAssetPath } from '@stencil/core';
import { LayoutBlockContainer, InfoCardVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-icons-description',
  styleUrl: 'digi-docs-icons-description.scss'
})
export class DigiDocsIconstDescription {
  @State() componentName: string;

  render() {
    return (
      <div class="digi-docs-icons-description">
        <digi-docs-page-layout >
          <digi-layout-block afMarginBottom af-container={LayoutBlockContainer.STATIC}>
            <digi-info-card
              afHeading="Riktlinjer"
              afHeadingLevel={`h2` as any}
              class="digi-docs-icons-description__info"
            >
              <digi-media-image
                afUnlazy
                af-src={getAssetPath('/assets/images/illustration-heart.png')}
                afAlt=""
                af-alt=""
                class="digi-docs-icons-description__illustration"
              ></digi-media-image>
              <digi-list>
                <li>
                  Använd endast ikoner från detta ikonbibliotek eftersom de
                  uppfyller kraven på tillgänglighet och följer
                  Arbetsförmedlingens varumärkesriktlinjer.
                </li>
                <li>
                  Använd dig av
                  Arbetsförmedlingens färgpalett om du behöver ändra färg på
                  ikonen. Primärt används profilfärgen #0000A5 eller textfärgen
                  #000333.
                </li>
                <li>
                  Tänk på att ikonen ska ha god kontrast.
                </li>
                <li>
                  Ikon används
                  primärt ihop med en förklarande text. I de fall det inte är
                  möjligt är det viktigt att det finns en alt-text eller div-tag
                  som förklarar dess funktion.
                </li>
              </digi-list>
            </digi-info-card>

            <digi-info-card
              afHeading="Så här bör du inte göra"
              afHeadingLevel={`h2` as any}
              af-variation={InfoCardVariation.SECONDARY}
            >
              <digi-list>
                <li>
                  Du får inte ändra eller modifiera
                  en ikon.
                </li>
                <li>
                  Valet av ikon skall vara starkt visuellt kopplat till
                    dess funktion. Det är exempelvis inte lämpligt att ha en ikon
                    med en penna för att symbolisera en länk.
                </li>
                <li>
                  Ikoner skall inte
                    användas som illustration eller informationsgrafik. Vi har andra
                    bibliotek för detta.
                </li>
                <li>
                  Ikonbiblioteket är att betrakta som ett
                    typsnitt eller visuell berättare och är inte kopplat till
                    Arbetsförmedlingens varumärke och visuella utryck.
                </li>
              </digi-list>
            </digi-info-card>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
