import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-icons-api', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-docs-icons-api></digi-docs-icons-api>'
    );

    const element = await page.find('digi-docs-icons-api');
    expect(element).toHaveClass('hydrated');
  });

  it('displays the specified name', async () => {
    const page = await newE2EPage({ url: '/profile/joseph' });

    const profileElement = await page.find(
      'app-root >>> digi-docs-icons-api'
    );
    const element = profileElement.shadowRoot.querySelector('div');
    expect(element.textContent).toContain('Hello! My name is Joseph.');
  });

  // it('includes a div with the class "digi-docs-icons-api"', async () => {
  //   const page = await newE2EPage({ url: '/profile/joseph' });

  // I would like to use a selector like this above, but it does not seem to work
  //   const element = await page.find('app-root >>> digi-docs-icons-api >>> div');
  //   expect(element).toHaveClass('digi-docs-icons-api');
  // });
});
