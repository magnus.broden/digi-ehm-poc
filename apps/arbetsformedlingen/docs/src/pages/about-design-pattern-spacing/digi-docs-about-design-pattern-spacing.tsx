import { Component, h, State } from '@stencil/core';

import designTokens from '../../../../../../dist/libs/arbetsformedlingen/design-tokens/styleguide/web.json';
import { router } from '../../global/router';

type Tokens = typeof designTokens;
enum AliasType {
	PADDING = 'padding',
	MARGIN = 'margin',
	GUTTER = 'gutter'
}

@Component({
	tag: 'digi-docs-about-design-pattern-spacing',
	styleUrl: 'digi-docs-about-design-pattern-spacing.scss',
	scoped: true
})
export class AboutDesignPatternSpacing {
	Router = router;
	@State() pageName = 'Spacing';
	@State() aliasType: AliasType = AliasType.PADDING;

	private _layoutTokens: Tokens;
	private _spacingGlobalTokens: Tokens;
	private _paddingTokens: Tokens;
	private _marginTokens: Tokens;
	private _gutterTokens: Tokens;

	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	componentWillLoad() {
		this._layoutTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/layout')
			)
		];

		this._spacingGlobalTokens = [
			...this._layoutTokens.filter((token) =>
				this.isSubset(token.attributes, {
					category: 'global',
					type: 'spacing'
				})
			)
		];

		this._paddingTokens = [
			...this._layoutTokens.filter((token) =>
				this.isSubset(token.attributes, {
					category: 'padding'
				})
			)
		];

		this._marginTokens = [
			...this._layoutTokens.filter((token) =>
				this.isSubset(token.attributes, {
					category: 'margin'
				})
			)
		];

		this._gutterTokens = [
			...this._layoutTokens.filter((token) =>
				this.isSubset(token.attributes, {
					category: 'gutter'
				})
			)
		];
	}

	isSubset(superObj, subObj) {
		return Object.keys(subObj).every((ele) => {
			if (typeof subObj[ele] == 'object') {
				return this.isSubset(superObj[ele], subObj[ele]);
			}
			return subObj[ele] === superObj[ele];
		});
	}

	groupTokens(arr = []) {
		const res = [];
		const map = {};
		let i, j, curr;

		for (i = 0, j = arr.length; i < j; i++) {
			curr = arr[i];
			if (!(curr.attributes.subitem in map)) {
				map[curr.attributes.subitem] = {
					name: curr.attributes.subitem,
					variations: []
				};
				res.push(map[curr.attributes.subitem]);
			}
			map[curr.attributes.subitem].variations.push(curr);
		}
		return res;
	}

	aliasTypeChangeHandler(e) {
		this.aliasType = e.detail.target.value;
	}

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-spacing/digi-docs-about-design-pattern-spacing.tsx"
				>
					<span slot="preamble">
						Spacing är en av Designsystemets grundstenar för att enkelt att sätta
						harmoniska och förutsägbara avstånd mellan komponenter och andra objekt i
						en design. Nedanstående värden är basen till marginaler, kolumnmellanrum,
						yttermarginaler, spacing mm. Dessa tal går att dela för att få ut jämna
						tal i enheten pixlar. I designtokens är dessa omräknade till rem som
						konsekvent är baserade på hela pixlar.
					</span>
					<br />

					<digi-layout-block af-variation="secondary" afVerticalPadding afMarginTop>
						<h3>Horisontal och vertikal spacing</h3>
						<p>
							Våra komponenter och övriga delar av designsystemet är baserade på nedan
							visade grundmått. Som designer använder du pixelmåtten för att skapa en
							konsekvent och homogen spacing för design, layout och nya komponenter.
							<br />
							<br />
							Vissa äldre komponenter kan ha annan spacing än vad som är etablerat i
							designmönstret. Denna legacy är något som vi arbetar löpande med att fasa
							ut. Därför uppmanar vi att du använder de värden som finns på denna sida.
						</p>
						<p>
							<div class="digi-docs-about-design-pattern-spacing__token-format">
								<digi-form-select
									afLabel="Token-format"
									afId="tokenFormat"
									afDisableValidation={true}
									onAfOnChange={(e) => this.aliasTypeChangeHandler(e)}
								>
									<option value={AliasType.PADDING}>Padding </option>
									<option value={AliasType.MARGIN}>Margin </option>
									<option value={AliasType.GUTTER}>Gutter </option>
								</digi-form-select>
							</div>
						</p>
						<digi-table af-size="medium" af-variation="primary">
							<table>
								<caption>Horisontal och vertikal spacing</caption>
								<thead>
									<tr>
										<th scope="col">Storlek</th>
										<th scope="col">rem</th>
										<th scope="col">global token</th>
										<th scope="col">alias token</th>
									</tr>
								</thead>
								<tbody>
									{this._spacingGlobalTokens
										.filter((token) => !token?.attributes?.['hidden-patterns'])
										.sort((a, b) => {
											if (
												Number(a.value.replace('rem', '')) >
												Number(b.value.replace('rem', ''))
											)
												return 1;
											if (
												Number(a.value.replace('rem', '')) <
												Number(b.value.replace('rem', ''))
											)
												return -1;
											return 0;
										})
										.map((token) => {
											var paddingToken = '';
											var marginToken = '';
											var gutterToken = '';

											this._paddingTokens.forEach((aliasToken) => {
												var path = [
													...aliasToken.original.value
														.replace('{', '')
														.replace('}', '')
														.split('.')
												];
												path.pop();
												const aliasName = `digi--${path.join('--')}`;
												if (aliasName == token.name) {
													paddingToken = aliasToken.name;
												}
											});

											this._marginTokens.forEach((aliasToken) => {
												var path = [
													...aliasToken.original.value
														.replace('{', '')
														.replace('}', '')
														.split('.')
												];
												path.pop();
												const aliasName = `digi--${path.join('--')}`;
												if (aliasName == token.name) {
													marginToken = aliasToken.name;
												}
											});

											this._gutterTokens.forEach((aliasToken) => {
												var path = [
													...aliasToken.original.value
														.replace('{', '')
														.replace('}', '')
														.split('.')
												];
												path.pop();
												const aliasName = `digi--${path.join('--')}`;
												if (aliasName == token.name) {
													gutterToken = aliasToken.name;
												}
											});

											return (
												<tr>
													<td>
														<div class="spacing-box-wrapper">
															<span
																class="spacing-box"
																style={{ '--SPACING-BOX--SIZE': `${token.value}` }}
															></span>
															{token.original.value}
														</div>
													</td>
													<td>{token.value}</td>
													<td>
														<digi-code afCode={`--${token.name}`}></digi-code>
													</td>
													<td>
														{paddingToken !== '' && this.aliasType == AliasType.PADDING && (
															<digi-code afCode={`--${paddingToken}`}></digi-code>
														)}
														{marginToken !== '' && this.aliasType == AliasType.MARGIN && (
															<digi-code afCode={`--${marginToken}`}></digi-code>
														)}
														{gutterToken !== '' && this.aliasType == AliasType.GUTTER && (
															<digi-code afCode={`--${gutterToken}`}></digi-code>
														)}
													</td>
												</tr>
											);
										})}
								</tbody>
							</table>
						</digi-table>
					</digi-layout-block>

					<digi-layout-block af-variation="primary" afVerticalPadding afMarginTop>
						<digi-layout-media-object af-alignment="start">
							<digi-media-image
								afUnlazy
								afHeight="72.22"
								afWidth="76"
								slot="media"
								afSrc="/assets/images/pattern/designprogram.svg"
								afAlt="Illustration av en penna och en vinkellinjal."
							></digi-media-image>

							<div>
								<h3 class="media-object__heading">
									Designprogram använder sig av pixlar för att ange mått i en design
								</h3>

								<p>
									Som designer använder du dig av pixlar, efter spacing angiven i
									tabellen ovanför. I koden omvandlar vi ofta värden till rem. 1 rem
									motsvarar 16px som är grundvärdet. Det är utvecklarens uppgift att
									omsätta design till kod och rem om inte en specifik tokens redan löser
									uppgiften, här är en
									<digi-link
										afHref="https://pixelsconverter.com/px-to-rem"
										afTarget="_blank"
									>
										lathund för spacing i px till rem
									</digi-link>
								</p>
							</div>
						</digi-layout-media-object>
					</digi-layout-block>
					<digi-layout-block af-variation="secondary" afVerticalPadding>
						<digi-layout-columns af-variation="two">
							<div>
								<h4>Formulär</h4>
								<p>Se relaterade designmönster och komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/formular"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Formulär
									</digi-link-button>
								</digi-docs-related-links>
							</div>
							<div>
								<h4>Grid och brytpunkter</h4>
								<p>Se relaterade designmönster och komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/grid-och-brytpunkter"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Grid och brytpunkter
									</digi-link-button>
								</digi-docs-related-links>
							</div>
						</digi-layout-columns>
						<br />
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
