import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternSpacing } from './digi-docs-about-design-pattern-spacing';

describe('digi-docs-about-design-pattern-spacing', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternSpacing],
			html:
				'<digi-docs-about-design-pattern-spacing></digi-docs-about-design-pattern-spacing>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-spacing>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-spacing>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternSpacing],
			html: `<digi-docs-about-design-pattern-spacing first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-spacing>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-spacing first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-spacing>
    `);
	});
});
