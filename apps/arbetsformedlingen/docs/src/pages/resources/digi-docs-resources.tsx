import { Component, h } from '@stencil/core';


@Component({
  tag: 'digi-docs-resources',
  styleUrl: 'digi-docs-resources.css'
})
export class DigiDocsResources {
  

  render() {
    return (
      <div class="digi-docs-resources">
        <digi-docs-page-layout
          af-edit-href="pages/digi-docs-resources/digi-docs-resources.tsx"
        >
          <digi-layout-block afMarginBottom>
            <digi-typography>
              <digi-typography-heading-jumbo af-text="Resurser"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Här finner du olika typer av resurser och filer som du kan
                behöva i ditt arbete.
              </digi-typography-preamble>
              <h2>Grafiska manualen</h2>
              <digi-link-external
                af-variation="small"
                afHref="https://space.arbetsformedlingen.se/sites/byggstenendesignsystem/Delade%20dokument/Resurser/grafisk-manual.pdf"
                af-target="_blank"
              >
               Grafiska manualen som PDF
              </digi-link-external>
              <p>
                Arbetsförmedlingens grafiska manual är en del av vårt varumärke.
                Den hjälper oss att vara tydliga och enhetliga för att på så
                sätt uppnå igenkänning och en stark känsla av de värden vi står
                för. Både intern och extern kommunikation ska följa manualen och
                varumärket.
              </p>
              <h2>Designfiler</h2>
              <h3>Digi UI Kit</h3>
              <digi-link-external
                af-variation="small"
                afHref="https://share.goabstract.com/75221e6d-d3b2-4235-a9f7-1729afe025c1"
                af-target="_blank"
              >
                UI-kittet i Abstract
              </digi-link-external>
              <p>
                UI-kittet är ett designverktyg för att effektivt och enhetligt
                kunna bygga skisser och prototyper som följer varumärket.
                UI-kittet består av Sketch-bibliotek och speglar innehåller i
                Digi Core.
              </p>
              <h2>Typsnitt</h2>
              <h3>OpenSans</h3>
              <digi-link-external
                af-variation="small"
                afHref="https://space.arbetsformedlingen.se/sites/designpaarbetsformedlingen/Delade%20dokument/open-sans.zip?csf=1&e=FBgguD"
              >
                OpenSans (open-sans.zip)
              </digi-link-external>
              <p>
                Typsnittet OpenSans är en specialversion av Open Sans som
                används på Arbetsförmedlingen. Ladda ner detta typsnitt för att
                ha rätt font i t.ex. Sketch.
              </p>

            </digi-typography>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
