import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-resources', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-resources></digi-docs-resources>');

    const element = await page.find('digi-docs-resources');
    expect(element).toHaveClass('hydrated');
  });
});
