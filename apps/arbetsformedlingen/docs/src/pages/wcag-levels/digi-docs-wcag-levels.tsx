import { Component, h } from '@stencil/core';


@Component({
  tag: 'digi-docs-wcag-levels',
  styleUrl: 'digi-docs-wcag-levels.scss'
})
export class DigiDocsWcagLevels {
  

  render() {
    return (
      <div class="digi-docs-wcag-levels">
        <digi-docs-page-layout
          af-edit-href="pages/about-digi/digi-docs-wcag-levels/digi-docs-wcag-levels.tsx"
        >
          <digi-layout-block>
            <digi-typography>
              <digi-typography-heading-jumbo af-text="Vad betyder A*, A och AA?"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                I tillgänglighetslistan är punkter markerade med A*, A eller AA. WCAG
                (Web Content Accessibility Guidelines) baseras på tre nivåer av
                överensstämmelse av kraven vilka utgörs av A (enkel-A), AA
                (dubbel-A) och AAA (trippel-A).
              </digi-typography-preamble>
              <br/>
              <p>
                Enligt lagen om tillgänglighet till digital offentlig service
                (DOS-lagen), och lagen om offentlig upphandling (LOU), krävs att
                en offentlig webbplats, eller ett system som omfattas av
                offentlig upphandling, uppnår WCAG 2.1 AA-nivå. Men vad betyder
                det i praktiken? Nedan följer en kort beskrivning av hur
                nivåerna i WCAG förhåller sig till varandra.
              </p>
              <digi-list>
                <li>
                  För att uppfylla nivå A krävs att allt på nivå A följs
                  korrekt.
                </li>
                <li>
                  För att uppfylla nivå AA krävs att allt på nivå A och AA följs
                  korrekt.
                </li>
                <li>
                  För att uppfylla nivå AAA krävs att allt på nivå A, AA och AAA
                  följs korrekt.
                </li>
              </digi-list>
              <p>
                Nivåerna är beroende av varandra. För att uppfylla nivå AA och AAA krävs
                alltså att de undre nivåerna också uppfylls. Vi kan därför inte välja en
                nivå och bortse från kraven på de undre nivåerna. När vi på myndigheten
                säger att vi strävar efter att följa nivå AA i våra webbaserade tjänster,
                appar och dokument betyder det alltså att vi ska följa nivå A och AA.
              </p>
              <p>
                Eftersom kraven på AAA-nivå inte omfattas av DOS-lagen och LOU, krävs inte
                att vi följer dem. Därför finns inga krav på den nivån med i tillgänglighetslistan.
              </p>
              <p>
                A* är inte en nivå i WCAG utan används endast här för att markera några
                specifika punkter i tillgänglighetslistan. Avsteg från dessa punkter kan vara
                särskilt hindrande för användare, oavsett i vilken omfattning som en webbsida
                i sin helhet uppfyller kraven på nivåerna A och AA.
              </p>
              <p>
                Observera att siffrorna framför varje punkt nedan är de unika nummer som punkterna
                har i tillgänglighetslistan.
              </p>
                <digi-list>
                  <li>
                  1. Inget störigt ljud (1.4.2)
                  </li>
                  <li>
                  2. Inget rör sig automatiskt  (2.2.2)
                  </li>
                  <li>
                  3. Automatiska uppdateringar (2.2.2)
                  </li>
                  <li>
                  4. Max tre blinkningar per sekund (2.3.1 och 2.3.2)
                  </li>
                  <li>
                  22. Ingen tangentbordsfälla (2.1.2)
                  </li>
                </digi-list>
              <digi-layout-block af-no-gutter af-margin-bottom>
              <h2>Var kan jag lära mig mer?</h2>
              <p>
                  <digi-link af-target="_blank" af-variation="small"  afHref="https://www.w3.org/TR/WCAG21/">
                  <digi-icon-external-link-alt slot="icon"></digi-icon-external-link-alt>
                  Web Content Accessibility Guidelines (WCAG) 2.1 at W3C</digi-link>
                <br/>
                  <digi-link af-target="_blank" af-variation="small"  afHref="https://www.w3.org/TR/WCAG21/#conformance">
                  <digi-icon-external-link-alt slot="icon"></digi-icon-external-link-alt>
                  Conformance to WCAG 2.1 at W3C</digi-link>
                <br/>
                  <digi-link af-target="_blank" af-variation="small"  afHref="https://webbriktlinjer.se/lagkrav/folj-standarder-tillganglighet/">
                  <digi-icon-external-link-alt slot="icon"></digi-icon-external-link-alt>
                  Följ standarder för tillgänglighet hos Webbriktlinjer.se</digi-link>
              </p>
              </digi-layout-block>
            </digi-typography>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
