import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-wcag-levels', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-wcag-levels></digi-docs-wcag-levels>');

    const element = await page.find('digi-docs-wcag-levels');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-wcag-levels></digi-docs-wcag-levels>');

    const element = await page.find('digi-docs-wcag-levels >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
