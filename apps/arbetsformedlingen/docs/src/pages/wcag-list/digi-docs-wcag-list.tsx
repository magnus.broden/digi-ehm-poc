import { Component, h, Listen, State } from '@stencil/core';

import {
	LayoutContainerVariation,
	LayoutBlockContainer,
	LayoutBlockVariation,
	ButtonSize,
	LinkInternalVariation,
  ExpandableAccordionHeaderLevel
} from '@digi/arbetsformedlingen';

import {
	A11yCategories,
	A11yLevels,
	A11yRoles,
	A11yStatus
} from '@digi/taxonomies';

import { createLocalStore } from '../../store/local-session-store';
import { router } from '../../global/router';

import { DirectusApi } from '../../services/directus.service';

import { marked } from 'marked';
import Papa from 'papaparse';

const FILTERS_INIT_OBJ = {
	filteredWcagCategories: [],
	filteredWcagLevels: [],
	filteredWcagRoles: [],
	filteredWcagStatus: [],
	_filterTags: []
};

const LIST_INIT_OBJ = {
	wcagList: []
};

const filterStore = createLocalStore('wcag-list-filters', FILTERS_INIT_OBJ);
const wcagStore = createLocalStore('wcag-list', LIST_INIT_OBJ);

@Component({
	tag: 'digi-docs-wcag-list',
	styleUrl: 'digi-docs-wcag-list.scss',
	scoped: true
})
export class DigiDocsWcagList {
	A11yCategories = A11yCategories;
	A11yLevels = A11yLevels;
	A11yRoles = A11yRoles;
	A11yStatus = A11yStatus;
	Router = router;

	@State() wcagList = [];
	@State() wcagListFilters = [];
	@State() savedWcagList = [];
	@State() resetValidation: boolean;
	@State() fileUploadValidation: boolean;
	@State() showResults: boolean = true;
	@State() fileName: string = '';
	@State() pageName = 'Tillgänglighetslistan';
	@State() wcagListError: boolean = false;

	fixStore() {
		const localStorageFilterKeys = Object.keys(filterStore.state);
		const localStorageListKeys = Object.keys(wcagStore.state);

		const initFilterKeys = Object.keys(FILTERS_INIT_OBJ);
		const initListKeys = Object.keys(LIST_INIT_OBJ);

		const resetKeys = (store, initKeys, storageKeys) => {
			initKeys.forEach((key) => {
				if (!storageKeys.includes(key)) {
					store.set(key as any, []);
					return;
				}
			});
		};

		resetKeys(filterStore, initFilterKeys, localStorageFilterKeys);
		resetKeys(wcagStore, initListKeys, localStorageListKeys);
	}

	levelsClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	wcagItemStatusHandler(e, id) {
		if (this.resetValidation) this.resetValidation = !this.resetValidation;

		let list = [...this.wcagList];
		const listItemIndex = list.findIndex((item) => item.id == id);

		const listItem = {
			...list[listItemIndex],
			status: e.target.value
		};

		list[listItemIndex] = listItem;

		this.wcagList = list;

		this.setFilterList();
	}

	wcagItemCommentHandler(e, id) {
		if (this.resetValidation) this.resetValidation = !this.resetValidation;

		let list = [...this.wcagList];
		const listItemIndex = list.findIndex((item) => item.id == id);

		const listItem = {
			...list[listItemIndex],
			comment: e.detail.target.value
		};

		list[listItemIndex] = listItem;

		this.wcagList = list;

		this.setFilterList();
	}

	async initWcagList() {
		try {
			const wcagListResp = await DirectusApi.getWcagItems();

			if (wcagStore.get('wcagList').length > 0) {
				this.wcagList = wcagStore.get('wcagList');
				this.wcagListFilters = this.wcagList;
			} else {
				this.wcagList = wcagListResp.data.data;
				this.wcagListFilters = wcagListResp.data.data;
			}

			this.setFilterList();
		} catch (error) {
			this.wcagListError = error.response.data.error.message;
		}
	}

	async resetWcagList() {
		if (
			window.confirm('Bekräfta att du vill återställa listan och alla filter')
		) {
			try {
				filterStore
					.get('_filterTags')
					.forEach((item) => this.uncheckCheckbox(item.category, item.text));
				const wcagListResp = await DirectusApi.getWcagItems();
				this.wcagList = wcagListResp.data.data;
				this.wcagListFilters = wcagListResp.data.data;
				this.setFilterList();
				this.resetValidation = true;
				this.fileUploadValidation = false;
				this.fileName = '';
			} catch (error) {
				this.wcagListError = error.response.data.error.message;
			}
		}
	}

	@Listen('input')
	changeHandler(e) {
		if (e.target.type === 'file') {
			this.fileName = e.target.files[0].name;
			const reader = new FileReader();

			reader.onload = () => {
				const lines = reader.result;
				const result = this.csvToJson(lines);
				this.wcagFileUpload(result);
			};

			reader.readAsText(e.target.files[0]);
			e.target.value = null;
		}
	}

	filterSubmitHandler(e) {
		let items = [];
		for (const value of e.detail.items) {
			items.push(value.text);
		}

		switch (e.detail.filter) {
			case 'Kategorier':
				filterStore.set('filteredWcagCategories', [...items]);
				break;
			case 'Nivåer':
				filterStore.set('filteredWcagLevels', [...items]);
				break;
			case 'Roller':
				filterStore.set('filteredWcagRoles', [...items]);
				break;
			case 'Krav':
				filterStore.set('filteredWcagStatus', [...items]);
				break;
		}

		this.setFilterList();
	}

	setFilterList() {
		const newList = [...this.wcagList]
			.filter((item) => {
				let check: boolean;

				if (!filterStore.get('filteredWcagCategories').length) {
					check = true;
				} else {
					filterStore.get('filteredWcagCategories').forEach((c) => {
						if (
							c.toLowerCase().split(' ').join('') ===
							item.category.toLowerCase().split(' ').join('')
						) {
							check = true;
						}
					});
				}
				return check;
			})
			.filter((item) => {
				let check: boolean;

				if (!filterStore.get('filteredWcagLevels').length) {
					check = true;
				} else {
					filterStore.get('filteredWcagLevels').forEach((c) => {
						if (
							c.toLowerCase().split(' ').join('') ===
							item.level.toLowerCase().split(' ').join('')
						) {
							check = true;
						}
					});
				}
				return check;
			})
			.filter((item) => {
				let check: boolean;

				if (!filterStore.get('filteredWcagRoles').length) {
					check = true;
				} else {
					filterStore.get('filteredWcagRoles').forEach((c) => {
						const text = c.toLocaleLowerCase();
						const role = item.role.toLocaleLowerCase();
						const roles = role.split(' ').join('').split('/');

						if (roles.includes(text)) {
							check = true;
						}
					});
				}
				return check;
			})
			.filter((item) => {
				let check: boolean;

				if (!filterStore.get('filteredWcagStatus').length) {
					check = true;
				} else {
					filterStore.get('filteredWcagStatus').forEach((c) => {
						if (
							item.status &&
							c.toLowerCase().split(' ').join('') ===
								item.status.toLowerCase().split(' ').join('')
						) {
							check = true;
						}
					});
				}
				return check;
			});
		if (newList.length != this.wcagListFilters.length) {
			this.showResults = false;
		}

		setTimeout(() => {
			this.wcagListFilters = newList;
			wcagStore.set('wcagList', this.wcagListFilters);
			this.setFilterTags();
			this.showResults = true;
		}, 0);
	}

	setFilterTags() {
		const categories = [];
		filterStore
			.get('filteredWcagCategories')
			.forEach((item) => categories.push({ category: 'Kategori', text: item }));

		const levels = [];
		filterStore
			.get('filteredWcagLevels')
			.forEach((item) => levels.push({ category: 'Nivå', text: item }));

		const roles = [];
		filterStore
			.get('filteredWcagRoles')
			.forEach((item) => roles.push({ category: 'Roll', text: item }));

		const status = [];
		filterStore
			.get('filteredWcagStatus')
			.forEach((item) => status.push({ category: 'Status', text: item }));

		const _filterTags = [...categories, ...levels, ...roles, ...status];
		filterStore.set('_filterTags', _filterTags);
	}

	get filterLength() {
		return `<strong>Visar ${this.wcagListFilters.length}</strong> av ${this.wcagList.length} riktlinjer`;
	}

	componentWillLoad() {
		this.fixStore();
		this.initWcagList();
	}

	uncheckCheckbox(category: string, text: string) {
		const el = `wcagCheckbox-${category}${text}`;
		const checkbox = document.getElementById(el) as HTMLInputElement;

		if (checkbox.checked) {
			checkbox.click();
			let filterStoreString;
			switch (category) {
				case 'Kategori':
					filterStoreString = 'filteredWcagCategories';
					break;
				case 'Nivå':
					filterStoreString = 'filteredWcagLevels';
					break;
				case 'Roll':
					filterStoreString = 'filteredWcagRoles';
					break;
				case 'Status':
					filterStoreString = 'filteredWcagStatus';
					break;
			}
			const arrIndex = filterStore.get(filterStoreString).indexOf(text);
			let arr = [...filterStore.get(filterStoreString)];
			arr.splice(arrIndex, 1);
			filterStore.set(filterStoreString, [...arr]);
		}
		this.setFilterList();
	}

	getwcagList() {
		const wcagList = wcagStore.get('wcagList');

		const wcagReport = wcagList.map((item) => {
			const { sort, comment, howToTest, ...rest } = item;

			const newObject = {
				...rest,
				howToTest: howToTest?.content ?? '',
				comment: comment
			};

			return newObject;
		});
		const csvData = this.objectToCsv(wcagReport);

		this.downloadCsv(csvData);
	}

	objectToCsv(data) {
		const csvRows = [];

		const headers = Object.keys(data[0]);
		csvRows.push(headers.join(','));

		for (const row of data) {
			const values = Object.values(row).map((value) => JSON.stringify(value));
			csvRows.push(values.join(','));
		}
		return csvRows.join('\n');
	}

	downloadCsv(data) {
		const blob = new Blob(['\uFEFF' + data], { type: 'text/csv;charset=utf8' });
		const url = window.URL.createObjectURL(blob);
		const link = document.createElement('a');
		link.setAttribute('href', url);
		if (this.fileName.length > 0) {
			link.setAttribute('download', `${this.fileName}`);
		} else {
			link.setAttribute('download', 'tillgänglighet-rapport.csv');
		}
		link.click();
	}

	csvToJson(csv) {
		const lines = Papa.parse(csv);

		const result = [];
		const headers = lines.data[0];

		for (let i = 1; i < lines.data.length; i++) {
			let obj = {};
			let currentLine = lines.data[i]

			for (let j = 0; j < headers.length; j++) {
				obj[headers[j]] = currentLine[j];
			}
			result.push(obj);
			for (let i in result) {
				result[i].id = parseInt(result[i].id);

				if (result[i].comment != undefined) {
					result[i].comment = result[i].comment.replaceAll('"', '');
				}
			}
		}
		return result;
	}

	wcagFileUpload(result) {
		const restructuredData = result.map((item) => {
			const { howToTest, ...rest } = item;

			const newObject = {
				...rest,
				howToTest: {
					content: howToTest
				}
			};

			return newObject;
		});

		this.wcagList = [...restructuredData];

		this.wcagListFilters = [...restructuredData];
		wcagStore.set('wcagList', this.wcagListFilters);

		this.fileUploadValidation = true;
	}

	openFile() {
		if (
			window.confirm('Bekräfta att du vill ersätta listan med filens innehåll')
		) {
			const inputFile = document.getElementById('inputFile');
			inputFile.click();
			filterStore
				.get('_filterTags')
				.forEach((item) => this.uncheckCheckbox(item.category, item.text));
			this.setFilterList();
		}
	}

	render() {
		return (
			<div class="digi-docs-wcag-list">
				<digi-docs-page-layout af-page-heading={this.pageName}>
					<digi-layout-block af-container={LayoutBlockContainer.STATIC}>
						<span slot="preamble">
							Tillgänglighetslistan är ett stöd för dig som vill säkerställa att en
							produkt eller tjänst uppfyller krav på tillgänglighet. Listan baseras på
							Web Accessibility Guidelines (WCAG) 2.1. Genom att använda punkterna
							nedan som underlag för kravställning minskar vi risken för att det
							uppstår tillgänglighetsbrister i våra tjänster.
						</span>
						<digi-typography>
							<br />
							<p>
								Förutom kravställning kan du också använda listan för att lära dig mer
								om hur du bygger tillgängliga tjänster. Om din tjänst uppfyller
								kriterierna undviker du de mest grundläggande tillgänglighetsbristerna.
								Kom ihåg att också göra användningstester för att upptäcka brister som
								inte täcks av WCAG-krav.
							</p>
							<p>
								I tillgänglighetslistan har du även möjlighet att fylla i svar på hur
								väl du tycker att en tjänst uppfyller kriterierna och exportera dem som
								underlag för vidare utveckling eller arbete med
								tillgänglighetsredogörelsen. <br />
                Med tillgänglighetslistan kan du göra en självskattning av
								tillgängligheten i din tjänst och spara det för framtida arbete. Du kan
								också exportera innehållet och använda det senare, men om du ändrar i
								csv-filen på din dator kan det hända att importen inte fungerar.
							</p>
							<br />
							<digi-link-internal
								af-variation={LinkInternalVariation.SMALL}
								afHref="/tillganglighet-checklista/wcag-levels"
								onAfOnClick={(e) => this.levelsClickHandler(e)}
							>
								Förklaring till nivåerna A*, A och AA
							</digi-link-internal>
						</digi-typography>

						<digi-typography>
							<h2>Filtrera</h2>
						</digi-typography>

						<div class="wcag-filters">
							<digi-form-filter
								class="from-filter"
								af-id="wcagCategories"
								af-name="Kategorier"
								afFilterButtonText="Kategori"
								afSubmitButtonText="Visa riktlinjer"
								onAfOnSubmitFilters={(e) => this.filterSubmitHandler(e)}
								onAfOnFilterClosed={(e) => this.filterSubmitHandler(e)}
							>
								{Object.values(A11yCategories).map((item) => {
									return (
										<digi-form-checkbox
											af-id={`wcagCheckbox-Kategori${item}`}
											af-label={item}
											afLabel={item}
											af-variation="primary"
											af-checked={
												!!filterStore.get('filteredWcagCategories').find((c) => c == item)
											}
										></digi-form-checkbox>
									);
								})}
							</digi-form-filter>

							<digi-form-filter
								af-id="wcagLevels"
								af-name="Nivåer"
								afFilterButtonText="Nivå"
								afSubmitButtonText="Visa riktlinjer"
								onAfOnSubmitFilters={(e) => this.filterSubmitHandler(e)}
								onAfOnFilterClosed={(e) => this.filterSubmitHandler(e)}
							>
								{Object.values(A11yLevels).map((item) => {
									return (
										<digi-form-checkbox
											af-id={`wcagCheckbox-Nivå${item}`}
											af-label={item}
											afLabel={item}
											af-variation="primary"
											af-checked={
												!!filterStore.get('filteredWcagLevels').find((c) => c == item)
											}
										></digi-form-checkbox>
									);
								})}
							</digi-form-filter>

							<digi-form-filter
								af-id="wcagRoles"
								af-name="Roller"
								afFilterButtonText="Roll"
								afSubmitButtonText="Visa riktlinjer"
								onAfOnSubmitFilters={(e) => this.filterSubmitHandler(e)}
								onAfOnFilterClosed={(e) => this.filterSubmitHandler(e)}
							>
								{Object.values(A11yRoles).map((item) => {
									return (
										<digi-form-checkbox
											af-id={`wcagCheckbox-Roll${item}`}
											af-label={item}
											afLabel={item}
											af-variation="primary"
											af-checked={
												!!filterStore.get('filteredWcagRoles').find((c) => c == item)
											}
										></digi-form-checkbox>
									);
								})}
							</digi-form-filter>
							<digi-form-filter
								afId="wcagStatus"
								af-name="Krav"
								afFilterButtonText="Krav"
								afSubmitButtonText="Visa krav"
								onAfOnSubmitFilters={(e) => this.filterSubmitHandler(e)}
								onAfOnFilterClosed={(e) => this.filterSubmitHandler(e)}
							>
								{Object.values(A11yStatus).map((item) => {
									return (
										<digi-form-checkbox
											af-id={`wcagCheckbox-Status${item}`}
											af-label={item}
											afLabel={item}
											af-variation="primary"
											af-checked={
												!!filterStore.get('filteredWcagStatus').find((c) => c == item)
											}
										></digi-form-checkbox>
									);
								})}
							</digi-form-filter>
						</div>

						{filterStore.get('_filterTags').length > 0 && (
							<div class="wcag-tags">
								{filterStore.get('_filterTags').map((item) => {
									return (
										<digi-tag
											afText={`${item.category}: ${item.text}`}
											af-size="small"
											af-aria-label={`${item.category}: ${item.text}, ta bort`}
											onAfOnClick={() => this.uncheckCheckbox(item.category, item.text)}
										></digi-tag>
									);
								})}
							</div>
						)}

						{/* <digi-info-card af-heading={`OBS!`} af-heading-level={`h2`}>
              <p>
                Detta är bara en representation av riktlinjerna från Web Content
                Accessibility Guidelines (WCAG) 2
              </p>
            </digi-info-card> */}
					</digi-layout-block>
					<digi-layout-container
						af-variation={LayoutContainerVariation.FLUID}
						af-no-gutter={true}
					>
						<digi-layout-block
							af-variation={LayoutBlockVariation.SECONDARY}
							af-container={LayoutBlockContainer.STATIC}
						>
							<div
								class={{
									'wcag-list-info__container': true,
									'wcag-list-info__container--loading': !this.showResults
								}}
							>
								<div>
									{this.wcagList.length > 0 && (
										<h2
											class="wcag-list-info"
											aria-live="polite"
											innerHTML={this.filterLength}
										></h2>
									)}
								</div>
								<div class="wcag-list-buttons">
									<digi-button
										af-variation="function"
										aria-live="polite"
										afSize={ButtonSize.SMALL}
										onAfOnClick={() => this.getwcagList()}
									>
										<digi-icon-download slot="icon" aria-hidden></digi-icon-download>
										Exportera csv-fil
									</digi-button>
									<input
										type="file"
										id="inputFile"
										name="inputFile"
										hidden
										accept=".csv"
									/>
									<digi-button
										aria-live="polite"
										af-variation="function"
										afSize={ButtonSize.SMALL}
										onAfOnClick={() => this.openFile()}
									>
										<digi-icon-file-document
											slot="icon"
											aria-hidden
										></digi-icon-file-document>
										Importera csv-fil
									</digi-button>
									<digi-button
										aria-live="polite"
										af-variation="function"
										afSize={ButtonSize.SMALL}
										onAfOnClick={() => this.resetWcagList()}
									>
										<digi-icon-redo slot="icon" aria-hidden></digi-icon-redo>
										Återställ listan
									</digi-button>
								</div>
							</div>
							{this.resetValidation && (
								<digi-notification-alert
									af-size="medium"
									af-variation="info"
									afCloseable={true}
									af-close-aria-label
									class="wcag-list__reset-notification"
									onAfOnClose={() => (this.resetValidation = false)}
								>
									<p>Listan och filtreringen är återställda</p>
								</digi-notification-alert>
							)}
							{this.fileUploadValidation && (
								<digi-notification-alert
									af-size="medium"
									af-variation="info"
									afCloseable={true}
									af-close-aria-label
									class="wcag-list__reset-notification"
									onAfOnClose={() => (this.fileUploadValidation = false)}
								>
									<p>{this.fileName} är importerad och listan är uppdaterad </p>
								</digi-notification-alert>
							)}
							{this.showResults && this.wcagList.length > 0 && (
								<ul class="wcag-list">
									{this.wcagListFilters.map((item) => {
										return (
											<li class="wcag-item">
												<digi-typography>
													<div class="wcag-item_header">
														<div class="wcag-item_column wcag-item_header-title">
															<digi-typography-meta af-variation="secondary">
																<h3 class="wcag-item__heading">
																	<span class="wcag-item_header-id">{item.id}</span>
																	{item.topic}
																</h3>
															</digi-typography-meta>
														</div>

														<div class="wcag-item__categories wcag-item_column">
															<p>
																<strong>Kategori:</strong>
																<span
																	class={{
																		'wcag-item__category': true
																	}}
																>
																	{item.category}
																</span>
															</p>
															<p>
																<strong>Nivå:</strong>
																<span
																	class={{
																		'wcag-item__level': true,
																		'wcag-item__level--a': item.level.toLowerCase() === 'a',
																		'wcag-item__level--aa': item.level.toLowerCase() === 'aa',
																		'wcag-item__level--aaa': item.level.toLowerCase() === 'aaa'
																	}}
																>
																	{item.level}
																</span>
															</p>
															<p>
																<strong>Roll: </strong>
																<span class="wcag-item__role">{item.role}</span>
															</p>
														</div>
													</div>
													<div class="wcag-item_content">
														<div class="wcag-item_column">
															<p>
																<strong>Påstående: </strong> {item.statement}
															</p>

															<p>
																<strong>Syfte: </strong> {item.why}
															</p>
															<digi-expandable-accordion
																afAnimation={false}
																afHeading="Lagkrav och standarder"
																afHeadingLevel={ExpandableAccordionHeaderLevel.H4}
															>
																<div class="wcag-item__principles">
																	<p slot="secondary" class="wcag-item__meta">
																		{item.wcag}
																	</p>
																	<p>
																		<strong>Princip: </strong>
																		{item.principle}
																	</p>
																	<p>
																		<strong>Riktlinje: </strong>
																		{item.guideline}
																	</p>
																	<p>
																		<strong>Kriterium: </strong>
																		{item.criteria}
																	</p>
																</div>
															</digi-expandable-accordion>
															{item.howToTest && (
																<digi-expandable-accordion
																	afAnimation={false}
																	afHeading="Hur du kan testa"
																	afHeadingLevel={ExpandableAccordionHeaderLevel.H4}
																>
																	<p>
																		<span innerHTML={marked.parse(item.howToTest.content)} />
																	</p>
																</digi-expandable-accordion>
															)}
														</div>
														<div class="wcag-item_column">
															<div class="wcag-item__option-select">
																<digi-form-fieldset>
																	<digi-form-select
																		afLabel="Är kravet uppfyllt?"
																		onAfOnChange={(e) => this.wcagItemStatusHandler(e, item.id)}
																		af-value={
																			item.status
																				? item.status
																				: (item.status = 'Kravet är ej bedömt')
																		}
																		af-variation="large"
																	>
																		<option af-id={item.id} value="Kravet är ej bedömt">
																			Kravet är ej bedömt
																		</option>
																		<option af-id={item.id + 1} value="Kravet är ej relevant">
																			Kravet är ej relevant
																		</option>
																		<option af-id={item.id + 2} value="Kravet uppfylls">
																			Kravet uppfylls
																		</option>
																		<option af-id={item.id + 3} value="Kravet uppfylls delvis">
																			Kravet uppfylls delvis
																		</option>
																		<option af-id={item.id + 4} value="Kravet uppfylls inte">
																			Kravet uppfylls inte
																		</option>
																	</digi-form-select>
																</digi-form-fieldset>
															</div>
															<div class="wcag-item__form-comment_form">
																<digi-form-textarea
																	afValue={item.comment}
																	onAfOnInput={(e) => this.wcagItemCommentHandler(e, item.id)}
																	class="wcag-item__form-comment"
																	afLabel="Egen kommentar"
																	aria-label={`Kommentar ${item.topic}`}
																	af-variation="small"
																	af-name={item.id}
																	af-id={`comment-${item.id}`}
																></digi-form-textarea>
															</div>
														</div>
													</div>
												</digi-typography>
											</li>
										);
									})}
								</ul>
							)}
							{this.wcagListFilters.length === 0 && (
								<div class="wcag-no-hits">
									<digi-typography>
										<h2 class="wcag-no-hits__heading">Inga träffar</h2>
										<p>
											Vi kan inte hitta några riktlinjer som passar in på din filtrering.
											Försök att ändra din filtrering.
										</p>
									</digi-typography>
								</div>
							)}
						</digi-layout-block>
					</digi-layout-container>
				</digi-docs-page-layout>
			</div>
		);
	}
}
