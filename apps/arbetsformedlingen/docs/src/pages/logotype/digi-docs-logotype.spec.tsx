import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsLogotype } from './digi-docs-logotype';

describe('digi-docs-logotype', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsLogotype],
      html: '<digi-docs-logotype></digi-docs-logotype>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-logotype>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-logotype>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsLogotype],
      html: `<digi-docs-logotype first="Stencil" last="'Don't call me a framework' JS"></digi-docs-logotype>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-logotype first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-logotype>
    `);
  });
});
