import { Component, h, State } from '@stencil/core';

@Component({
	tag: 'digi-docs-about-digi-ui-kit',
	styleUrl: 'digi-docs-about-digi-ui-kit.scss',
	scoped: true
})
export class DigiDocsAboutDigiUiKit {
	@State() pageName = 'Digi UI Kit';

	render() {
		return (
			<div class="digi-docs-about-digi-ui-kit">
				<digi-docs-page-layout
					af-edit-href="pages/digi-docs-about-digi-ui-kit/digi-docs-about-digi-ui-kit.tsx"
					af-page-heading={this.pageName}
				>
					<span slot="preamble">
						Ett UI-kit är ett verktyg som hjälper användaren att minimera antalet
						arbetsuppgifter som görs gång på gång, som till exempel skapa knappar,
						formulär, färgpalett, typografi och så vidare. Dessa element samlas i en
						eller flera filer och fungerar som återanvändbara komponenter.
					</span>
					<digi-layout-block afMarginBottom>
						<digi-typography>
							<br />
							<p>
								Arbetsförmedlingens UI-kit heter Digi UI Kit och består av ett antal
								designfiler som versionshanteras i Abstract. Komponenterna, som är
								indelade i kategorier, är skapade i Sketch enligt Arbetsförmedlingens
								grafiska profil och tillgänglighetsriktlinjer. Komponenterna är även
								framtagna för att spegla Digi Core, som är ett komponentbibliotek för
								kod. Digi UI Kit och Digi Core är ihopkopplade med hjälp av
								designtokens.
							</p>
							<digi-link-internal
								afHref="/om-designsystemet/digi-core"
								af-target="_blank"
								af-variation="small"
							>
								Läs mer om Digi Core
							</digi-link-internal>
							<br />
							<digi-link-internal
								afHref="/om-designsystemet/digi-tokens"
								af-target="_blank"
								af-variation="small"
							>
								Läs mer om designtokens
							</digi-link-internal>

							<h2>Vilka kan och bör använda ett Digi UI Kit?</h2>
							<p>
								Alla som arbetar med design i någon form kan använda UI-kittet. I
								praktiken innebär det oftast att du arbetar med ett eller flera av
								följande områden:
							</p>
							<digi-list>
								<li>
									<span class="digi-docs__bold-text">Konceptarbete: </span>
									När du vill skissa på idéer och koncept till nya produkter och
									tjänster.
								</li>
								<li>
									<span class="digi-docs__bold-text">Prototyper: </span>
									När du snabbt vill skissa upp användarflöden eller liknande.
								</li>
								<li>
									<span class="digi-docs__bold-text">Slutlig design: </span>
									När du vill skapa underlag för implementering hos utvecklare.
								</li>
							</digi-list>
							<p>
								För att arbeta med Arbetsförmedlingens Digi UI Kit behöver du Sketch och
								Abstract.
							</p>
							{/* <digi-link-internal afHref="/kom-i-gang/jobba-med-digi-ui-kit">
                Kom igång med UI Kit
              </digi-link-internal> */}
							<h2>Fördelar</h2>
							<digi-list>
								<li>
									<span class="digi-docs__bold-text">Enhetlighet: </span>
									Utvecklade produkter och tjänster följer den grafiska profilen och
									andra framtagna riktlinjer.
								</li>
								<li>
									<span class="digi-docs__bold-text">Snabbare designprocess: </span>
									Enklare och mer effektivt att ta fram koncept, prototyper och skisser
									som sedan blir till kod och färdig produkt.
								</li>
								<li>
									<span class="digi-docs__bold-text">Bäst lösning: </span>
									Komponenterna är uppbyggda enligt praxis på marknaden.
								</li>

								<li>
									<span class="digi-docs__bold-text">Gemensam bild: </span>
									Lättare att få överblick över vilka komponenter som finns tillgängliga,
									hur de ser ut och fungerar.
								</li>
								<li>
									<span class="digi-docs__bold-text">Gemensamt ägande: </span>
									Enkelt för teamen att bidra till vidareutveckling då UI-kittet och
									annat som ingår i designsystemet ägs gemensamt av alla på myndigheten.
								</li>
							</digi-list>
							<h2>Nackdelar</h2>
							<digi-list>
								<li>
									<span class="digi-docs__bold-text">Tar längre tid i början: </span>
									Det tar längre tid i början att bygga upp. Därför är det viktigt att
									utvecklingsteamen själva bidrar in i det gemensamma biblioteket.
								</li>
								<li>
									<span class="digi-docs__bold-text">Begränsat antal komponenter: </span>
									Behovet av nya komponenter och utveckling av befintliga kommer alltid
									att vara större än vad som kommer ut i designsystemet. Därför är det
									viktigt att alla bidrar.
								</li>
								<li>
									<span class="digi-docs__bold-text">
										Anpassningar till befintlig design:{' '}
									</span>
									Det är lättare att använda detta till utveckling av nya produkter och
									tjänster, svårare att anpassa redan befintliga.
								</li>

								<li>
									<span class="digi-docs__bold-text">Kräver ständig utveckling: </span>
									Blir “aldrig klart” och kräver därför en långsiktig plan och
									investering från alla berörda parter, såsom ledninggrupper, chefer,
									produktansvariga, produktägare och utvecklingsteam.
								</li>
								<li>
									<span class="digi-docs__bold-text">Kan begränsa kreativiteten: </span>
									Designers kan känna sig låsta av riktlinjer och färdiga komponenter.
								</li>
							</digi-list>
						</digi-typography>
					</digi-layout-block>
				</digi-docs-page-layout>
			</div>
		);
	}
}
