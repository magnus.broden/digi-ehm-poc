import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutDigiUiKit } from './digi-docs-about-digi-ui-kit';

describe('digi-docs-about-digi-ui-kit', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutDigiUiKit],
      html: '<digi-docs-about-digi-ui-kit></digi-docs-about-digi-ui-kit>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-digi-ui-kit>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-digi-ui-kit>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutDigiUiKit],
      html: `<digi-docs-about-digi-ui-kit first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-digi-ui-kit>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-digi-ui-kit first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-digi-ui-kit>
    `);
  });
});
