import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsReleaseNotes } from './digi-docs-release-notes';

describe('digi-docs-release-notes', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsReleaseNotes],
      html: '<digi-docs-release-notes></digi-docs-release-notes>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-release-notes>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-release-notes>
    `);
  });
});
