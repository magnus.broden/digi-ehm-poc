import { Component, getAssetPath, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-illustrations',
  styleUrl: 'digi-docs-illustrations.scss',
  scoped: true
})
export class DigiDocsIllustrations {
  

  render() {
    return (
      <div class="digi-docs-illustrations">
        <digi-docs-page-layout
          af-edit-href="pages/digi-docs-logotype/digi-docs-illustrations.tsx"
        >
          <digi-typography>
            <digi-layout-block>
              <digi-typography-heading-jumbo af-text="Illustrationer"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Ibland vill vi förstärka en känsla eller beskriva en händelse.
                De gånger det inte är möjligt eller passar att använda foton så
                använder vi våra egna illustrationer. Det är lika viktigt här
                precis som i vår bildpolicy att de representerar ett tvärsnitt
                av befolkningen och kommunicerar verkligheten utan klichéer och
                inte speglar kön eller klasstillhörighet.
              </digi-typography-preamble>
              <p>
                Vi använder oss av ett eget manér där illustrationerna kan
                användas på fler olika sätt. Antingen i sin ursprungsform med
                endast de tecknade linjerna eller ihop med “färgklickar” som
                signalerar eller harmoniserar med övrigt innehåll i dess
                kontext. För “färgklickarna” använder vi våra profilfärger samt
                komplementfärger i dess original eller ljusare variant. Vi är
                sparsamma hur vi använder dessa illustrationer och de är främst
                framtagna för externa webben och tryck material.
              </p>
              <h2>Illustrationer som beskriver en händelse</h2>
              <p>
                Används för att förstärka texten rent visuellt. Exempel kan vara
                en text om hur du deltar i Arbetsförmedlingens webinarier.
              </p>
              <div class="digi-docs__webinar-image">
                <digi-media-image 
                  afUnlazy
                  afSrc={getAssetPath('/assets/images/people-in-webinar-illustrations.png')}
                  afAlt="Illustrationer på hur man kan deltar i Arbetsförmedlingens webinarium"
                ></digi-media-image>
              </div>
                
              <h2>Illustrationer som beskriver en känsla</h2>
              <p>
                Används för att inspirera och komplettera innehåll. Med
                människan i centrum som även kan omges av intryck och verktyg
                som symboliserar en helhet.
              </p>
              <div class="digi-docs__people-image">
                <digi-media-image 
                  afUnlazy
                  afSrc={getAssetPath('/assets/images/people-illustrations.png')}
                  afAlt="Illustrationer på människor med olika intryck"
                ></digi-media-image>
              </div>
            </digi-layout-block>
             <br />
          </digi-typography>
        </digi-docs-page-layout>
       
      </div>
    );
  }
}
