import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutDigiCore } from './digi-docs-about-digi-core';

describe('digi-docs-about-digi-core', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutDigiCore],
      html: '<digi-docs-about-digi-core></digi-docs-about-digi-core>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-digi-core>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-digi-core>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutDigiCore],
      html: `<digi-docs-about-digi-core first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-digi-core>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-digi-core first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-digi-core>
    `);
  });
});
