import { Component, h, State } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'digi-docs-about-digital-accessibility',
	styleUrl: 'digi-docs-about-digital-accessibility.scss',
	scoped: true
})
export class DigiDocsAboutDigitalAccessibility {
	@State() pageName = 'Om digital tillgänglighet';

	render() {
		return (
			<div class="digi-docs-about-digital-accessibility">
        <Helmet>
					<meta
						property="rek:pubdate"
						content="Thu Nov 29 2022 08:06:16 GMT+0100 (CET)"
					/>
				</Helmet>
				<digi-typography>
					<digi-docs-page-layout
						af-page-heading={this.pageName}
						af-edit-href="pages/digi-docs-about-digital-accessibility/digi-docs-about-digital-accessibility.tsx"
					>
						<span slot="preamble">
							Digital tillgänglighet betyder att Arbetsförmedlingens digitala tjänster
							ska vara tillgängliga för alla samt vara lätta att använda. Det spelar
							ingen roll om våra användare har en dator eller mobil, om användaren har
							någon funktionsnedsättning, om användaren är ny i Sverige eller om
							användaren upplever att “datorer är allmänt knepigt”.
						</span>
						<br />
						<digi-layout-block>
							<p>
								Digital tillgänglighet innebär dessutom att alla som jobbar hos oss ska
								ha en digital arbetsmiljö där det är möjligt att utföra sitt arbete
								smidigt och effektivt. Målet är att Arbetsförmedlingen ska vara en
								inkluderande arbetsgivare. Följer vi kraven på digital tillgänglighet
								kan vi dessutom rekrytera fler medarbetare med den kompetens
								Arbetsförmedlingen behöver.
							</p>
						</digi-layout-block>
						<digi-layout-block>
							<h2>Varför?</h2>
							<p>
								Vi vill att våra kunder ska kunna använda våra digitala kanaler i första
								hand. Digital tillgänglighet innebär att vi kan minska belastningen på
								våra kontor och det stöd vi ger på telefon. Dessutom bidrar god
								användbarhet till att såväl kunder som medarbetare oftare gör rätt när
								de ska använda våra digitala tjänster. På så sätt skapar vi även
								enhetlighet och förtroende för våra digitala tjänster genom att vara
								digitalt tillgängliga.
							</p>
						</digi-layout-block>
						<digi-layout-block>
							<h2>Hur?</h2>
							<p>
								Vi vill göra det lätt för dig som arbetar med våra digitala tjänster och
								vår digitala information. I designsystemet har vi därför samlat alla
								resurser du behöver för att kunna ta fram digitala tjänster som följer
								aktuella riktlinjer och lagkrav gällande digital tillgänglighet. Vi
								säkerställer tillgängligheten i våra tjänster genom research kring
								användbarhet, användarcentrerad design, utveckling med tillgängliga
								komponenter men även genom att genomföra användningstester med användare
								som har varierande förutsättningar.
							</p>
						</digi-layout-block>
						<digi-docs-accessibility-matrix></digi-docs-accessibility-matrix>
					</digi-docs-page-layout>
				</digi-typography>
			</div>
		);
	}
}
