import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutDigitalAccessibility } from './digi-docs-about-digital-accessibility';

describe('digi-docs-about-digital-accessibility', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutDigitalAccessibility],
      html: '<digi-docs-about-digital-accessibility></digi-docs-about-digital-accessibility>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-digital-accessibility>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-digital-accessibility>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutDigitalAccessibility],
      html: `<digi-docs-about-digital-accessibility first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-digital-accessibility>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-digital-accessibility first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-digital-accessibility>
    `);
  });
});
