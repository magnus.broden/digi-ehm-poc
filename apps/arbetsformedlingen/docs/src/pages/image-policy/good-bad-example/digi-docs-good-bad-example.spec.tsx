import { newSpecPage } from '@stencil/core/testing';
import { GoodBadExample } from './digi-docs-good-bad-example';

describe('digi-docs-good-bad-example', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [GoodBadExample],
			html: '<digi-docs-good-bad-example></digi-docs-good-bad-example>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-good-bad-example>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-good-bad-example>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [GoodBadExample],
			html: `<digi-docs-good-bad-example first="Stencil" last="'Don't call me a framework' JS"></digi-docs-good-bad-example>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-good-bad-example first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-good-bad-example>
    `);
	});
});
