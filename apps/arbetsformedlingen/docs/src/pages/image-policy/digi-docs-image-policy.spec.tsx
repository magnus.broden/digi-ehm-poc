import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsImagePolicy } from './digi-docs-image-policy';

describe('digi-docs-image-policy', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsImagePolicy],
      html: '<digi-docs-image-policy></digi-docs-image-policy>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-image-policy>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-image-policy>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsImagePolicy],
      html: `<digi-docs-image-policy first="Stencil" last="'Don't call me a framework' JS"></digi-docs-image-policy>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-image-policy first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-image-policy>
    `);
  });
});
