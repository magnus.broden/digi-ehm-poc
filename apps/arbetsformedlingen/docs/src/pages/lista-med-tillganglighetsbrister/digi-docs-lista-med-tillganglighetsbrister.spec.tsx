import { newSpecPage } from '@stencil/core/testing';
import { ListaMedTillganglighetsbrister } from './digi-docs-lista-med-tillganglighetsbrister';

describe('digi-docs-lista-med-tillganglighetsbrister', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [ListaMedTillganglighetsbrister],
			html:
				'<digi-docs-lista-med-tillganglighetsbrister></digi-docs-lista-med-tillganglighetsbrister>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-lista-med-tillganglighetsbrister>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-lista-med-tillganglighetsbrister>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [ListaMedTillganglighetsbrister],
			html: `<digi-docs-lista-med-tillganglighetsbrister first="Stencil" last="'Don't call me a framework' JS"></digi-docs-lista-med-tillganglighetsbrister>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-lista-med-tillganglighetsbrister first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-lista-med-tillganglighetsbrister>
    `);
	});
});
