import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsNews } from './digi-docs-news';

describe('digi-docs-news', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsNews],
      html: '<digi-docs-news></digi-docs-news>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-news>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-news>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsNews],
      html: `<digi-docs-news first="Stencil" last="'Don't call me a framework' JS"></digi-docs-news>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-news first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-news>
    `);
  });
});
