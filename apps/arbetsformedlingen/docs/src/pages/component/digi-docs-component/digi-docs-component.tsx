import { Component, h, State } from '@stencil/core';
import state from '../../../store/store';
import { router } from '../../../global/router';
// import { InternalRouterState } from 'stencil-router-v2/dist/types';
import { match, Route } from 'stencil-router-v2';

import { setDocumentTitle } from '../../../global/documentTitle';

@Component({
  tag: 'digi-docs-component',
  styleUrl: 'digi-docs-component.scss',
  scoped: true
})
export class DigiDocsComponent {
  Router = router;

  @State() componentName: string;
  @State() componentTag: string;
  @State() activeTab: number;

  routeChange() {
    if (
      state.router.newPaths &&
      state.router.oldPaths &&
      state.router.newPaths[0] == 'komponenter' &&
      state.router.newPaths[state.router.newPaths.length - 2] !== state.router.oldPaths[state.router.oldPaths.length - 2]
    ) {
      (async () => {
        await customElements.whenDefined('digi-navigation-tabs');
        const navigationTabs = document.querySelector('digi-navigation-tabs');
        await navigationTabs.afMSetActiveTab(0);
      })();
    }
  }

  componentWillLoad() {
    this.setActiveComponent();
  }

  componentWillUpdate() {
    this.routeChange();
    this.setActiveComponent();
  }

  componentDidLoad() {
    setDocumentTitle(this.Router.activePath);
  }

  componentDidUpdate() {
    setDocumentTitle(this.Router.activePath);
  }

  getTab() {
    switch (state.router.newPaths[state.router.newPaths.length-1]) {
      case 'oversikt':
        return 0;
      case 'api':
        return 1;
      default:
        0;
    }
  }

  setActiveComponent() {
    if (state.router.newPaths[0] == 'komponenter') {
      this.activeTab = this.getTab();

      state.activeComponent = state.components.find(
        (c) => c.tag === state.router.newPaths[state.router.newPaths.length-2]
      );

      this.componentName =
        state.activeComponent.docsTags?.find(
          (tag) => tag.name === 'swedishName'
        )?.text || state.activeComponent.tag;

      this.componentTag = state.activeComponent.tag;
    }
  }

  toggleNavTabHandler(e) {
    const tag = e.target.dataset.tag;
    // Nested tabs in documentation page triggers this function
    // Need to check if tag exists before changing route
    if (tag) {
      this.Router.push(
        `${state.routerRoot}komponenter/${this.componentTag}/${tag}`
      );
    }
  }

  render() {
    return (
      <div class="digi-docs-component">
        <digi-docs-page-layout afPageHeading={this.componentName}>
          <digi-layout-block>
            <p class="digi-subheading">{this.componentTag}</p>
          </digi-layout-block>
          <digi-layout-block>

            <digi-navigation-tabs
              af-aria-label="Innehåll för komponent"
              af-init-active-tab={this.activeTab}
            >

              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="oversikt"
                afAriaLabel="Översikt"
              ></digi-navigation-tab>

              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="api"
                afAriaLabel="Kod"
              ></digi-navigation-tab>

            </digi-navigation-tabs>
            
            <this.Router.Switch>
              <Route
                path={match("/komponenter/:tag/oversikt")}
                render={({tag}) => (
                  <digi-docs-component-details componentTag={tag} />
                )}
              />
              <Route
                path={match("/komponenter/:tag/api")}
                render={({tag}) => (
                  <digi-docs-component-api componentTag={tag} />
                )}
              />
            </this.Router.Switch>

          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
