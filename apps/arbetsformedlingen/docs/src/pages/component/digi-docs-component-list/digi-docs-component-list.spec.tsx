import { newSpecPage } from '@stencil/core/testing';
import { ComponentList } from './digi-docs-component-list';

describe('digi-docs-component-list', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [ComponentList],
			html: '<digi-docs-component-list></digi-docs-component-list>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-component-list>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-component-list>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [ComponentList],
			html: `<digi-docs-component-list first="Stencil" last="'Don't call me a framework' JS"></digi-docs-component-list>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-component-list first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-component-list>
    `);
	});
});
