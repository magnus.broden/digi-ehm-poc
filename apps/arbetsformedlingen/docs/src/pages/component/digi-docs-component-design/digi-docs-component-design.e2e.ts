import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-component-design', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-docs-component-design></digi-docs-component-design>'
    );

    const element = await page.find('digi-docs-component-design');
    expect(element).toHaveClass('hydrated');
  });

  it('displays the specified name', async () => {
    const page = await newE2EPage({ url: '/profile/joseph' });

    const profileElement = await page.find(
      'app-root >>> digi-docs-component-design'
    );
    const element = profileElement.shadowRoot.querySelector('div');
    expect(element.textContent).toContain('Hello! My name is Joseph.');
  });

  // it('includes a div with the class "digi-docs-component-design"', async () => {
  //   const page = await newE2EPage({ url: '/profile/joseph' });

  // I would like to use a selector like this above, but it does not seem to work
  //   const element = await page.find('app-root >>> digi-docs-component-design >>> div');
  //   expect(element).toHaveClass('digi-docs-component-design');
  // });
});
