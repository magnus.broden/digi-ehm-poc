import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternGrids } from './digi-docs-about-design-pattern-grids';

describe('digi-docs-about-design-pattern-grids', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternGrids],
			html:
				'<digi-docs-about-design-pattern-grids></digi-docs-about-design-pattern-grids>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-grids>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-grids>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternGrids],
			html: `<digi-docs-about-design-pattern-grids first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-grids>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-grids first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-grids>
    `);
	});
});
