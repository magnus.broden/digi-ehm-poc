import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';

import designTokens from '../../../../../../dist/libs/arbetsformedlingen/design-tokens/styleguide/web.json';

type Tokens = typeof designTokens;

const fontSizeBase = 16;

@Component({
	tag: 'digi-docs-about-design-pattern-grids',
	styleUrl: 'digi-docs-about-design-pattern-grids.scss',
	scoped: true
})
export class AboutDesignPatternGrids {
	@State() pageName = 'Grid och brytpunkter';
	Router = router;
	private _layoutTokens: Tokens;
	private _gridGlobalTokens: Tokens;
	private _gridAliasTokens: Tokens;

	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}
	componentWillLoad() {
		this._layoutTokens = [
			...designTokens.filter((token) =>
				token.filePath.includes('src/styles/layout')
			)
		];

		this._gridGlobalTokens = [
			...this._layoutTokens.filter((token) =>
				this.isSubset(token.attributes, {
					category: 'global',
					type: 'breakpoint'
				})
			)
		];

		this._gridAliasTokens = [
			...this._layoutTokens.filter((token) =>
				this.isSubset(token.attributes, {
					category: 'breakpoint'
				})
			)
		];
	}

	isSubset(superObj, subObj) {
		return Object.keys(subObj).every((ele) => {
			if (typeof subObj[ele] == 'object') {
				return this.isSubset(superObj[ele], subObj[ele]);
			}
			return subObj[ele] === superObj[ele];
		});
	}

	groupTokens(arr = []) {
		const res = [];
		const map = {};
		let i, j, curr;

		for (i = 0, j = arr.length; i < j; i++) {
			curr = arr[i];
			if (!(curr.attributes.subitem in map)) {
				map[curr.attributes.subitem] = {
					name: curr.attributes.subitem,
					variations: []
				};
				res.push(map[curr.attributes.subitem]);
			}
			map[curr.attributes.subitem].variations.push(curr);
		}
		return res;
	}

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-grids/digi-docs-about-design-pattern-grids.tsx"
				>
					<span slot="preamble">
						Här beskriver vi brytpunkter och responsiv bakgrundsgrid som vi
						rekommenderar att använda för att få ett homogent uttryck på våra webbar
						och tjänster. Gridden ska hjälpa oss att använda konsekventa
						yttermarginaler, kolumnmellanrum och brytpunkter. Gridden är skelettet som
						vi sedan bygger en mängd olika layouter på, för att passa med det
						specifika innehåll och budskap som ska visas.
					</span>
					<br />
					<digi-layout-block af-variation="secondary" afVerticalPadding afMarginTop>
						<digi-layout-columns af-variation="two">
							<div>
								{/* <digi-layout-block
									af-variation="secondary"
									afVerticalPadding
									afMarginTop
								> */}
								<digi-media-image
									afUnlazy
									afHeight="289"
									afWidth="646"
									afSrc="/assets/images/pattern/Rityta-Satsyta.svg"
									afAlt="..."
								></digi-media-image>
								<h3>Rityta</h3>
								<p>
									Ritytan är den maximala bredden på sidans innehåll, det är ytan som vi
									lägger upp vår design på. Yttermarginaler tillkommer och ligger utanför
									ritytan. (Om vi inte gör utfallande block, som till exempel herobilder
									på startsidor). Maxiamala bredden förändras vid varje responsiv
									brytpunkt
								</p>
								{/* </digi-layout-block> */}
							</div>

							<div>
								{/* <digi-layout-block
									af-variation="secondary"
									afVerticalPadding
									afMarginTop
								> */}
								<digi-media-image
									afUnlazy
									afHeight="289"
									afWidth="629"
									afSrc="/assets/images/pattern/Yttermarginaler.svg"
									afAlt="..."
								></digi-media-image>
								<h3>Yttermarginaler</h3>
								<p>
									Yttermarginaler är de yttre marginalerna på gridden. Dessa ligger
									utanför ritytans bredd.
								</p>
								{/* </digi-layout-block> */}
							</div>

							<div>
								{/* <digi-layout-block
									af-variation="secondary"
									afVerticalPadding
									afMarginTop
								> */}
								<digi-media-image
									afUnlazy
									afHeight="289"
									afWidth="646"
									afSrc="/assets/images/pattern/kolumner.svg"
									afAlt="..."
								></digi-media-image>
								<h3>Kolumner</h3>
								<p>
									Vid 1200px ner till ≥ 768 px har gridden 12 responsiva kolumner.
									Kolumnernas bredd är elastisk och förändras beroende på bredden av
									webbsidan. Och kolumnerna blir färre desto mindre skärm som designen
									och visas på.
								</p>
								{/* </digi-layout-block> */}
							</div>

							<div>
								{/* <digi-layout-block
									af-variation="secondary"
									afVerticalPadding
									afMarginTop
								> */}
								<digi-media-image
									afUnlazy
									afHeight="289"
									afWidth="646"
									afSrc="/assets/images/pattern/Gutters.svg"
									afAlt="..."
								></digi-media-image>
								<h3> Gutters (kolumnmellanrum)</h3>
								<p>
									Gutters är mellanrummet mellan griddens kolumner. Gutter bredd har
									fasta värden (16 px, 24 px, etc.) beroende på skärmbredd och vald
									brytpunkt.
								</p>
							 
								{/* </digi-layout-block> */}
							</div>

							<div>
								{/* <digi-layout-block
									af-variation="secondary"
									afVerticalPadding
									afMarginTop
								> */}
								<digi-media-image
									afUnlazy
									afHeight="320"
									afWidth="630"
									afSrc="/assets/images/pattern/Layouter.svg"
									afAlt="..."
								></digi-media-image>
								<h3>Layouter</h3>
								<p>
									En mängd olika layouter är möjliga på samma bakgrundgrid, som exemplet
									ovan visar. Fullbredd eller fyrspalt, eller något däremellan är möjligt
									att designa på en responsiv grid. Konekventa kolumnmellanrum och
									förutbestämda brytpunkter och yttermarginaler hjälper till att skapa
									ett homogent uttryck.
								</p>
								{/* </digi-layout-block> */}
								<br />
							</div>

							<div>
								<digi-layout-block af-variation="secondary" afVerticalPadding>
									<h3>POC och liveexempel på responsiv grid</h3>
									<p>
										Här har vi ett exempel på hur den responsiva gridden beter sig vid
										olika brytpunkter
									</p>
									<digi-link-internal
										afHref="/designmonster/grid-och-brytpunkter/grid-poc"
										af-target="_blank"
										af-variation="small"
									>
										Se liveexempel på resonsiv grid/poc
									</digi-link-internal>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<div>
							{/* <digi-layout-block af-variation="secondary" afVerticalPadding afMarginTop> */}
							<h3>Grundläggande brytpunkter för Arbetsförmedlingen webbar</h3>
							<p>
								Dessa brytpunkter är det primära valet för att skapa en enhetlighet och
								ett samanhängande visuellt uttryck. Undantag är tillåtna för specifika
								tjänster eller behov.
							</p>
							{/* </digi-layout-block> */}
						</div>
						<digi-table af-size="medium" af-variation="primary">
							<table>
								<caption>Grundläggande brytpunkter för Arbetsförmedlingen webbar</caption>
								<thead>
									<tr>
										<th scope="col">Brytpunkt</th>
										<th scope="col">Rityta</th>
										<th scope="col">Yttermarginal</th>
										<th scope="col">Kolumner</th>
										<th scope="col">Gutter</th>
										<th scope="col">Sass-variabel</th>
									</tr>
								</thead>
								<tbody>
									{this._gridAliasTokens
										.filter((token) => !token.name.includes('xx-large'))
										.filter(
											(token) =>
												token.name.includes('small-below') || !token.name.includes('below')
										)
										.sort((a, b) => {
											if (a.value < b.value) return 1;
											if (a.value > b.value) return -1;
											return 0;
										})
										.map((token) => {
											let globalPath = token?.original?.value
												.replace('{', '')
												.replace('}', '')
												.split('.');

											const filteredGlobalToken = this._gridGlobalTokens.filter((token) =>
												this.isSubset(token.attributes, {
													category: globalPath[0],
													type: globalPath[1],
													item: globalPath[2]
												})
											);
											const globalToken = { ...filteredGlobalToken[0] };

											function getRelation() {
												return token.name.includes('below') ? '<' : '≥';
											}

											function containerValue() {
                        let container = token.attributes?.['container-width'];
												if (!container.includes('rem')) {
													return container;
												}
												let value = Number(container.replace('rem', ''));
                        let gutter = Number(token.attributes?.['container-gutter'].replace('rem', ''));
												return `${(value - (gutter*2)) * fontSizeBase} px`;
											}

                      function containerGutterValue() {
                        let gutter = token.attributes?.['container-gutter'];
												if (!gutter.includes('rem')) {
													return gutter;
												}
												let value = Number(gutter.replace('rem', ''));
												return `${value * fontSizeBase} px`;
											}

											return (
												<tr>
													<th scope="row">
														{getRelation()} {globalToken.original.value.replace('px', ' px')}
													</th>
													<td>{containerValue()}</td>
													<td>{containerGutterValue()}</td>
													<td>{token.attributes?.['columns']}</td>
													<td>24 px (1.5 rem)</td>
													<td>${token.name}</td>
												</tr>
											);
										})}
								</tbody>
							</table>
						</digi-table>
						<br />
						{/* <digi-layout-block af-variation="secondary" afVerticalPadding afMarginTop> */}
						<div>
							<h3>Det finn en extra bred skärmbredd tillgänglig.</h3>
							<p>
								Den är tänk för interna webbar som tex Intranätet eller andra interna
								verktyg som behöver mer utrymme för mer komplext innehåll, där vi vet
								att huvuddelen av användarna sitter på desktop-storlekar på sina
								skärmar. Denna extra breda skärmbredd är baserad på samma grundtal som
								ovan angivna tabell.
							</p>
						</div>
						{/* </digi-layout-block> */}
						<br />
						<digi-table af-size="medium" af-variation="primary">
							<table>
								<caption>Det finn en extra bred skärmbredd tillgänglig</caption>
								<thead>
									<tr>
										<th scope="col">Brytpunkt</th>
										<th scope="col">Rityta</th>
										<th scope="col">Yttermarginal</th>
										<th scope="col">Kolumner</th>
										<th scope="col">Gutter</th>
										<th scope="col">Sass-variabel</th>
									</tr>
								</thead>
								<tbody>
									{this._gridAliasTokens
										.filter((token) => token.name.includes('xx-large'))
										.filter((token) => !token.name.includes('below'))
										.map((token) => {
											let globalPath = token?.original?.value
												.replace('{', '')
												.replace('}', '')
												.split('.');

											const filteredGlobalToken = this._gridGlobalTokens.filter((token) =>
												this.isSubset(token.attributes, {
													category: globalPath[0],
													type: globalPath[1],
													item: globalPath[2]
												})
											);
											const globalToken = { ...filteredGlobalToken[0] };

											function getRelation() {
												return token.name.includes('below') ? '<' : '≥';
											}

											function containerValue(val) {
												if (!val.includes('rem')) {
													return val;
												}
												let value = Number(val.replace('rem', ''));

												// return `${value} rem (${value * fontSizeBase} px)`;
												return `${value * fontSizeBase} px`;
											}

											function containerOuterSpacing() {
												var container = token.attributes?.['container-width'];
												var containerValue =
													Number(container.replace('rem', '')) * fontSizeBase;
												var breakpoint = Number(
													globalToken.original.value.replace('px', '')
												);
												return (breakpoint - containerValue) / 2 + ' px';
											}

											return (
												<tr>
													<th scope="row">
														{getRelation()} {globalToken.original.value.replace('px', ' px')}
													</th>
													<td>{containerValue(token.attributes?.['container-width'])}</td>
													<td>{containerOuterSpacing()}</td>
													<td>{token.attributes?.['columns']}</td>
													<td>24px (1.5rem)</td>
													<td>${token.name}</td>
												</tr>
											);
										})}
								</tbody>
							</table>
						</digi-table>
						<br />
						<br />
					</digi-layout-block>

					<digi-layout-block afVerticalPadding>
						<digi-layout-columns af-variation="two">
							<div>
								<h3>Designunderlag</h3>
								<p>
									Vi gör först och främst designunderlag för mobilskärmar (375 px) och
									desktop-bredden 1200 px (med rityta 1140 px). Stötta även utvecklaren
									med tablet-design om resurser finnes. Specifika tjänster kan ha
									användardata som visar på andra skärmbredder, var agil och stötta
									eventuella specialfall
								</p>
								<digi-media-image
									afUnlazy
									afHeight="289"
									afWidth="500"
									afSrc="/assets/images/pattern/Designunderlag.svg"
									afAlt="..."
								></digi-media-image>
							</div>

							<div>
								<h3>Statistik</h3>
								<p></p>
								<digi-link-external
									afHref="https://gs.statcounter.com/os-market-share/mobile/sweden/#monthly-202105-202205"
									af-target="_blank"
									af-variation="small"
								>
									Läs på gs.statcounter.com om skrämstorlekar och andel användare på
									svenska marknaden
								</digi-link-external>
							</div>
						</digi-layout-columns>
					</digi-layout-block>

					<digi-layout-block af-variation="secondary" afVerticalPadding>
						<digi-layout-columns af-variation="two">
							<div>
								<h4>Formulär</h4>
								<p>Se relaterade designmönster och komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/formular"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Formulär
									</digi-link-button>
								</digi-docs-related-links>
							</div>
							<div>
								<h4>Spacing</h4>
								<p>Se relaterade designmönster och komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/spacing"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Spacing
									</digi-link-button>
								</digi-docs-related-links>
							</div>
						</digi-layout-columns>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
