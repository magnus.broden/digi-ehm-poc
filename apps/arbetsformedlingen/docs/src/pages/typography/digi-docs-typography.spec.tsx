import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsTypography } from './digi-docs-typography';

describe('digi-docs-typography', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsTypography],
      html: '<digi-docs-typography></digi-docs-typography>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-typography>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-typography>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsTypography],
      html: `<digi-docs-typography first="Stencil" last="'Don't call me a framework' JS"></digi-docs-typography>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-typography first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-typography>
    `);
  });
});
