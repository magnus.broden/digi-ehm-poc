import { Component, h } from '@stencil/core';


@Component({
  tag: 'digi-docs-typography',
  styleUrl: 'digi-docs-typography.scss',
  scoped: true
})
export class DigiDocsTypography {
  

  render() {
    return (
      <div class="digi-docs-about-typography">
        <digi-docs-page-layout
          af-edit-href="pages/digi-docs-about-typography/digi-docs-about-typography.tsx"
        >
          <digi-layout-block afMarginBottom>
            <digi-typography-heading-jumbo af-text="Typografi"></digi-typography-heading-jumbo>
            
            <digi-typography>
              <digi-typography-preamble>
                Open Sans är Arbetsförmedlingens identitetsbärande
                typsnittsfamilj. Den kompletteras med typsnittfamiljerna
                Georgia, Verdana och Arial för olika ändamål.
              </digi-typography-preamble>
              <p>
                Läsbarhet är en av de viktigaste aspekterna när det kommer till
                att välja rätt typsnitt. Därför är det viktigt att du använder
                dem på rätt sätt enligt de riktlinjer vi presenterar här. Ur ett
                tillgänglighetsperspektiv och för läsbarhetens skull så undviker
                vi varianterna light (tunn) och italic (kursiv) i löptext. Vi
                skriver inte heller rubriker, eller längre stycken med versaler
                (stora bokstäver).
              </p>
              <h2>Webb</h2>
              <p>
                På webben och för alla digitala tjänster som vi producerar
                själva använder vi uteslutande Open Sans. Det gäller rubriker,
                ingress och brödtext samt komponenter och övriga funktioner där
                det förekommer text. Vi använder tre fördefinierade
                teckenstorlekuppsättningar för våra egenutvecklade system och
                tjänster. “Large” används främst för huvudwebbplatsen
                (arbetsformedlingen.se). Finns det gott om utrymme fungerar det
                även för landningssidor och artikelsidor. “Small” lämpar sig för
                olika typer av tjänster där det förekommer mycket komponenter.
                Tex. formulär eller interna verktyg där det finns många
                interaktionsmöjligheter. “Mobil” är en teckenstorlekuppsättning
                helt anpassad för mobila enheter med mindre skärm.
              </p>
              <digi-link-internal
                af-variation="small"
                afHref="/komponenter/digi-typography/oversikt"
              >
                Här hittar du typografikomponenten
              </digi-link-internal>
              <h2>E-post/externa system och tjänster </h2>
              <p>
                I e-postklienter saknas oftast möjligheten att avläsa andra
                typsnitt än de som är standard för operativsystemet. Därför
                använder vi i första hand Verdana och i andra hand Arial. Dessa
                är snarlika men Verdana är att föredra för att typsnittet är
                utvecklat för digitala skärmar och läsbarheten är bättre.
              </p>
              <p>
                För externa system och tjänster som Arbetsförmedlingen köpt in
                eller nyttjar. Om möjligheten finns att implementera Open Sans
                bör det göras. I andra Verdana och tredje hand Arial.{' '}
              </p>
              <h2>Trycksaker</h2>
              <p>
                I tryckt material använder vi Open Sans till rubriker och
                ingresser. För löpande brödtext använder vi Georgia som är ett
                typsnitt med seriffer (klackar). Det ökar läsbarheten när det är
                större textmassor eftersom det skapar en tydligare linje att
                följa när du läser. Även i te.x eventmaterial använder vi Open
                Sans.{' '}
              </p>
              <h2>Microsoft Office</h2>
              <p>
                I office mallarna använder vi Arial som huvudtypsnitt då det är
                mest kompatibelt med vårt arbete i myndigheten. I löpande text,
                brödtext använder vi Georgia.
              </p>
              <h2>Om typsnitten</h2>
              <h3>Open Sans</h3>
              <p>
                Är precis som det låter en sans serif och open
                source-fontfamilj. Designad 2011 av Steve Matteson.
              </p>
              <digi-info-card afHeading="Webb" afHeadingLevel={`h1` as any}>
              <div class="digi-docs__typography-info-card">
                <div class="digi-docs__typography-info-card-left">
                  <digi-typography-preamble>
                    Lorem ipsum dolor sit amet, consec tetur adipiscing elit.
                    Vestibulum elit dui, suscipit at luctus ut.
                  </digi-typography-preamble>
                  <br/>
                  <p>
                    Aliquam erat volutpat. Curabitur eu consequat erat. Vivamus
                    quam nisi, hendrerit vel ligula sed, iaculis arcu. Vivamus
                    libero odio, consectetur ac bibendum nec, gravida at urna.{' '}
                  </p>
                  <h3>Praesent</h3>
                  <p>
                    Sed luctus dui turpis, in rhoncus elit venenatis a. Quisque
                    sit amet scelerisque dolor.
                  </p>
                </div>
                <div class="digi-docs__typography-info-card-right">
                  <ul>
                  <li class="digi-docs__typography-font-weight-light">Open Sans Light 123?!%=*&</li>
                  <li class="digi-docs__typography-font-weight-light digi-docs__typography-font-style-italic">Open Sans Light Italic 123?!%=*&</li>
                  <li>Open Sans Regular 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic" >Open Sans Italic 123?!%=*&</li>
                  <li class="digi-docs__typography-font-weight-semibold">Open Sans Semibold 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic digi-docs__typography-font-weight-semibold">Open Sans Semibold Italic 123?!%=*&</li>
                  <li class="digi-docs__typography-font-weight-bold">Open Sans Bold 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic digi-docs__typography-font-weight-bold">Open Sans Bold Italic 123?!%=*&</li>
                  </ul>
                </div>
              </div>
            </digi-info-card>
              <h3>Georgia</h3>
              <p>Är en serif designad 1993 av Matthew Carter. </p>
              <digi-info-card afHeading="Tryck" afHeadingLevel={`h1` as any}>
              <div class="digi-docs__typography-info-card digi-docs__typography-font-georgia">
                <div class="digi-docs__typography-info-card-left">
                  <p class="digi-docs__typography-font-weight-bold">
                    Lorem ipsum dolor sit amet, consec tetur adipiscing elit.
                    Vestibulum elit dui, suscipit at luctus ut.
                  </p>
                  <p>
                    Aliquam erat volutpat. Curabitur eu consequat erat. Vivamus
                    quam nisi, hendrerit vel ligula sed, iaculis arcu. Vivamus
                    libero odio, consectetur ac bibendum nec, gravida at urna.{' '}
                  </p>
                  <h3>Praesent</h3>
                  <p>
                    Sed luctus dui turpis, in rhoncus elit venenatis a. Quisque
                    sit amet scelerisque dolor.
                  </p>
                </div>
                <div class="digi-docs__typography-info-card-right">
                  <ul>
                  <li >Georgia Regular 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic">Georgia Italic 123?!%=*&</li>
                  <li class="digi-docs__typography-font-weight-bold">Georgia Bold 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic digi-docs__typography-font-weight-bold">Georgia Bold Italic 123?!%=*&</li>
                  </ul>
                </div>
              </div>
            </digi-info-card>
              <h3>Verdana</h3>
              <p>Är en san serif designad 1996 av Matthew Carter. </p>
              <digi-info-card afHeading="Standardsystem" afHeadingLevel={`h1` as any}>
              <div class="digi-docs__typography-info-card digi-docs__typography-font-verdana">
                <div class="digi-docs__typography-info-card-left">
                  <p class="digi-docs__typography-font-weight-bold">
                    Lorem ipsum dolor sit amet, consec tetur adipiscing elit.
                    Vestibulum elit dui, suscipit at luctus ut.
                  </p>
                  <p>
                    Aliquam erat volutpat. Curabitur eu consequat erat. Vivamus
                    quam nisi, hendrerit vel ligula sed, iaculis arcu. Vivamus
                    libero odio, consectetur ac bibendum nec, gravida at urna.{' '}
                  </p>
                  <h3>Praesent</h3>
                  <p>
                    Sed luctus dui turpis, in rhoncus elit venenatis a. Quisque
                    sit amet scelerisque dolor.
                  </p>
                </div>
                <div class="digi-docs__typography-info-card-right">
                  <ul>
                  <li >Verdana Regular 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic">Verdana Italic 123?!%=*&</li>
                  <li class="digi-docs__typography-font-weight-bold">Verdana Bold 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic digi-docs__typography-font-weight-bold">Verdana Bold Italic 123?!%=*&</li>
                  </ul>
                </div>
              </div>
            </digi-info-card>
              <h3>Arial</h3>
              <p>
                Är en san serif designat 1982 av Robin Nicholas och Patricia
                Saunders.</p>
                <digi-info-card afHeading="Standardsystem" afHeadingLevel={`h1` as any}>
                <div class="digi-docs__typography-info-card digi-docs__typography-font-arial">
                <div class="digi-docs__typography-info-card-left">
                  <p class="digi-docs__typography-font-weight-bold">
                    Lorem ipsum dolor sit amet, consec tetur adipiscing elit.
                    Vestibulum elit dui, suscipit at luctus ut.
                  </p>
                  <p>
                    Aliquam erat volutpat. Curabitur eu consequat erat. Vivamus
                    quam nisi, hendrerit vel ligula sed, iaculis arcu. Vivamus
                    libero odio, consectetur ac bibendum nec, gravida at urna.{' '}
                  </p>
                  <h3>Praesent</h3>
                  <p>
                    Sed luctus dui turpis, in rhoncus elit venenatis a. Quisque
                    sit amet scelerisque dolor.
                  </p>
                </div>
                <div class="digi-docs__typography-info-card-right">
                  <ul>
                  <li>Arial Regular 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic">Arial Italic 123?!%=*&</li>
                  <li class="digi-docs__typography-font-weight-bold" >Arial Bold 123?!%=*&</li>
                  <li class="digi-docs__typography-font-style-italic digi-docs__typography-font-weight-bold">Arial Bold Italic 123?!%=*&</li>
                  </ul>
                </div>
              </div>
            </digi-info-card>
              
            </digi-typography>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
