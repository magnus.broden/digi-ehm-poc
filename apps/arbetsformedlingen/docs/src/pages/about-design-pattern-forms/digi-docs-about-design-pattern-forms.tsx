import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';
import state from '../../store/store';

@Component({
	tag: 'digi-docs-about-design-pattern-forms',
	styleUrl: 'digi-docs-about-design-pattern-forms.scss',
	scoped: true
})
export class AboutDesignPatternForms {
	Router = router;
	@State() pageName = 'Formulär';

	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-forms/digi-docs-about-design-pattern-forms.tsx"
				>
					<span slot="preamble">
						Formulär är en grupp komponenter som ger användaren möjlighet att skicka
						in information till oss. För att få den bästa användarupplevelsen är det
						viktigt att de arrangeras rätt. Vi har grupperat dem med tanke på behov
						och innehållet på informationen som användaren skickar.
					</span>
					<br />

					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<br />
						<h2>Inmatningsfält och andra komponenter </h2>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									Inmatningsfält, väljare, datumväljare, kryssruta, radioknapp m.fl, är
									till för att våra användare ska kunna skicka in data till oss med
									minimal insats.
								</p>
							</div>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern">
										<digi-form-input
											afLabel="Efternamn"
											af-variation="medium"
											af-type="text"
											af-validation="neutral"
										></digi-form-input>
									</div>
									<br />
									<div class="pattern">
										<digi-form-select
											afLabel="Arbetslivserfarenhet"
											af-placeholder="Välj antal år"
											af-variation="medium"
											af-validation="neutral"
										>
											<option value="0-2">0-2 år</option>
											<option value="2-5">2-5 år</option>
											<option value="5">Över 5 år</option>
										</digi-form-select>
									</div>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<br />
						<h2>Etikett </h2>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									Beskrivande etiketter hjälper användarna att förstå
									formulärkomponenters innehåll och syfte. Gör texten kort. Ofta kan ett
									enda ord vara tillräckligt för att beskriva innehållet, längre texter
									kan göra att formuläret upplevs som rörigt och komplicerat. (Var därför
									återhållsam vi användandet av två-radiga etikettkomponenter). Etiketten
									ska även hjälpa skärmläsare att beskriva komponenten då den läser upp
									etiketten när komponenten får fokus. För personer som navigerar med
									tangentbordet och/eller använder skärmläsare ger det en möjlighet att
									hoppa till olika delar av formulärets innehåll.
								</p>
							</div>
							<div>
								<digi-layout-block
									af-variation="primary"
									afVerticalPadding
									afMarginBottom
								>
									<div class="pattern">
										<digi-form-fieldset
											afLegend="Sysselsättningsstatus"
											class="myradiobutton"
										>
											<digi-form-radiobutton
												afLabel="Föräldraledig mer ersättning från Försäkringskassan"
												af-variation="primary"
												afName="Myradiobuttons"
											></digi-form-radiobutton>
											<digi-form-radiobutton
												afLabel="Studerande med studiebidrag"
												af-variation="primary"
												afName="Myradiobuttons"
											></digi-form-radiobutton>
											<digi-form-radiobutton
												afLabel="Arbetssökande inskriven hos Arbetsförmedlingen"
												af-variation="primary"
												afName="Myradiobuttons"
											></digi-form-radiobutton>
											<digi-form-radiobutton
												afLabel="Annat (beskriv i fältet nedan)"
												af-variation="primary"
												afName="Myradiobuttons"
											></digi-form-radiobutton>
										</digi-form-fieldset>
									</div>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<br />
						<h2>Obligatoriska fält</h2>
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<p>
									I ett formulär finns det obligatoriska fält som användaren måste fylla
									i. Det kan även finnas fält som är frivilliga. Det måste framgå tydligt
									i formuläret vilka fält som är obligatoriska och frivilliga att fylla
									i. Fälten ska därför vara uppmärkta med antingen "(obligatoriskt)" i
									etiketten eller med "(frivilligt)" i etiketten. Märk upp de fält som
									förekommer minst frekvent i formuläret för att inte belasta användaren
									med för mycket information. Är alla fält obligatoriska krävs ingen
									uppmärkning i etiketten, men beskriv tidigt i formuläret att alla
									fälten är obligatoriska. Om frivilliga fält eller kryssrutor är viktiga
									för beslut och domstolsbeslut, kan "(frivilligt)" i etiketten lätt
									missförstås. Vi rekommenderar då att du tar bort markeringen
									"(frivilligt)".
								</p>
							</div>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern">
										<digi-form-input
											afLabel="Ange E-postadress (obligatorisk)"
											// af-label-description="greta@gmail.see"
											af-variation="medium"
											af-type="text"
											af-validation="error"
											af-validation-text="E-postadressen verkar inte vara korrekt"
										></digi-form-input>
									</div>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<br />
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<h2>Validering och bekräftelsemeddelande</h2>
								<p>
									Använd validationsmeddelanden för att snabbt och effektiv ge feedback i
									ett formulär. Gör det för att först hjälpa användaren att förstå och
									identifiera eventuella felaktigheter, och för att sedan guida till hur
									hen ska lösa problemet. Vanliga fel kan vara felformaterad data,
									obligatoriska fält som är tomma, eller fält som är ofullständigt
									ifyllda. Tänk på att använda validering med måtta, till exempel kan
									positiva valideringsmeddelande bli mycket påträngande om de alltid
									används i alla samanhang.
								</p>
								<br />
								<h2>Knappar ihop med formulär </h2>
								<p>
									Knappar ligger sist i formuläret. Vi använder en tydlig skillnad mellan
									primär och sekundär knapp. Placera alltid den primära knappen till
									höger och den sekundära till vänster. I desktop och i tablet är
									knapparna dessutom vänsterställda. I mobilt gränssnitt bör knapparna
									ligga på full bredd på sidan. Ibland är det svårt att döpa knappar på
									ett bra och konsekvent sätt. Försök att använda en så enkel och kort
									benämning på knappen som möjligt.
								</p>
							</div>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern">
										<digi-form-textarea
											afLabel="Fritext (frivilligt)"
											af-label-description="Beskrivande text"
											af-variation="medium"
											af-validation="neutral"
										></digi-form-textarea>
										<br />
										<digi-button af-size="medium" af-variation="secondary">
											Spara utkast
										</digi-button>{' '}
										<digi-button af-size="medium" af-variation="primary">
											Skicka
										</digi-button>
									</div>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<div>
							{/* <digi-layout-block af-variation="primary" afVerticalPadding>
									
									<div class="pattern">
										<digi-form-input
											afLabel="Efternamn"
											af-variation="medium"
											af-type="text"
											af-validation="neutral"
										></digi-form-input>
									</div>
									<br />
									<div class="pattern">
										<digi-form-select
											afLabel="Arbetslivserfarenhet"
											af-placeholder="Välj antal år"
											af-variation="medium"
											af-validation="neutral"
										>
											<option value="0-2">0-2 år</option>
											<option value="2-5">2-5 år</option>
											<option value="5">Över 5 år</option>
										</digi-form-select>
									</div>
								</digi-layout-block> */}
							<br />
							{/* <digi-layout-block
									af-variation="primary"
									afVerticalPadding
									afMarginBottom
								>
									<div class="pattern">
										<digi-form-fieldset
											afLegend="Sysselsättningsstatus"
											class="myradiobutton"
										>
											<digi-form-radiobutton
												afLabel="Föräldraledig mer ersättning från Försäkringskassan"
												af-variation="primary"
												afName="Myradiobuttons"
											></digi-form-radiobutton>
											<digi-form-radiobutton
												afLabel="Studerande med studiebidrag"
												af-variation="primary"
												afName="Myradiobuttons"
											></digi-form-radiobutton>
											<digi-form-radiobutton
												afLabel="Arbetssökande inskriven hos Arbetsförmedlingen"
												af-variation="primary"
												afName="Myradiobuttons"
											></digi-form-radiobutton>
											<digi-form-radiobutton
												afLabel="Annat (beskriv i fältet nedan)"
												af-variation="primary"
												afName="Myradiobuttons"
											></digi-form-radiobutton>
										</digi-form-fieldset>
									</div>
								</digi-layout-block> */}

							{/* <digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern">
										<digi-form-input
											afLabel="Ange E-postadress (obligatorisk)"
											// af-label-description="greta@gmail.see"
											af-variation="medium"
											af-type="text"
											af-validation="error"
											af-validation-text="E-postadressen verkar inte vara korrekt"
										></digi-form-input>
									</div>
								</digi-layout-block> */}
							<br />
							{/* <digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern">
										<digi-form-textarea
											afLabel="Fritext (frivilligt)"
											af-label-description="Beskrivande text"
											af-variation="medium"
											af-validation="neutral"
										></digi-form-textarea>
										<br />
										<digi-button af-size="medium" af-variation="secondary">
											Spara utkast
										</digi-button>{' '}
										<digi-button af-size="medium" af-variation="primary">
											Skicka
										</digi-button>
								</div>
								</digi-layout-block> */}
						</div>
						{/* </digi-layout-columns> */}
						<br />
						<hr></hr>
						<br />
						<br />
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern">
										<h3 class="pattern__heading">Kontaktformulär</h3>
										<p>Det är obligatoriskt att ange informationen nedan.</p>
										<digi-form-input
											afLabel="Förnamn"
											af-variation="medium"
											af-type="text"
											af-validation="neutral"
										></digi-form-input>
										<digi-form-input
											afLabel="Efternamn"
											af-variation="medium"
											af-type="text"
											af-validation="neutral"
										></digi-form-input>
										<br />
										<br />
										<digi-form-input
											afLabel="Adress"
											af-variation="medium"
											af-type="text"
											af-validation="neutral"
										></digi-form-input>
										<br />
										<digi-form-input
											afLabel="Ort"
											af-variation="medium"
											af-type="text"
											af-validation="neutral"
										></digi-form-input>
										<br />
										<digi-form-input
											afLabel="Telefonnummer (frivilligt)"
											af-variation="medium"
											af-type="text"
											af-validation="neutral"
										></digi-form-input>
										<br />
										<digi-button af-variation="secondary">Rensa</digi-button>{' '}
										<digi-button af-variation="primary">Skicka</digi-button>
									</div>
								</digi-layout-block>
							</div>
							<div>
								<digi-layout-container>
									<h2>Layout</h2>
									<p>
										För formulär som inte är så komplicerade, förespråkar vi att använda
										en en-kolumns-layout istället för flera kolumner. Det gör det enklare
										för användaren att slutföra fälten ett och ett, när hen följer en rak
										linje nedåt jämnfört med att hoppa mellan flera kolumner. Då risken är
										att användaren missar eller hoppar över vissa fält. Mobila enheter är
										smala och har per automatik inte utrymme för fler kolumner om hela ord
										ska få plats i fälten. Denna design hjälper oss att samordna det
										visuella uttrycket för formulärfält i mobil och desktop- storlekar.
										Det blir även enklare för skärmläsare att navigera genom en sådan
										struktur. Komponenter som checkboxar och radiobuttons är ofta korta
										och då är det frestande att lägga fler val på samma rad, men undvik
										det då det av tillgänglighetsskäl är viktigt att lägga varje val på
										egen rad.
									</p>
									<p>
										För mer komplicerade formulär med många inmatningsfält kan det dock
										att vara bättre att använda en två-kolumns-lösning, för att korta ner
										sidan längd och göra den mer kompakt och översiktlig.
									</p>
									<br />
									<h2>Gruppera fält och komponenter i grupper</h2>
									<p>
										När formulär har fält eller komponenter som relaterar till varandra
										kontextuellt, ska du överväga att gruppera denna information i grupper
										för att användaren enkelt ska kunna avkoda de relaterade sektionerna.
										Överväg att göra avståndet mellan relaterade objekt jämförelsevis
										mindre för att peka på samhörighet. Ge sedan användaren en visuell
										paus med ett större avstånd till nästa sektion för att förenkla
										avkodning av hierarkier.
									</p>
									<br />
									<h2>Logiskt flöde och multistegsindikator</h2>
									<p>
										Ibland behöver våra användare ange en mängd information till oss via
										formulär-komponenterna. Detta kan t.ex vara ett flöde i en
										inskrivning, eller ett längre frågeformulär eller liknande. Då är det
										bättre att dela upp flödet med hjälp av fler sidor. Då kan du behöva
										använda en komponent med förloppssteg. Genom att visa på flera steg,
										guidar vi användaren att bryta ner uppgiften till mindre hanterbara
										delmoment. Så att hen förstår vad som redan är gjort och vad som är
										kvar att göra. Detta kan även vara hjälp för användaren att komma
										igång även om hela flödet ej går att slutföra vid första tillfället.{' '}
										<digi-link
											afHref="/komponenter/digi-expandable-faq/oversikt"
											afTarget="_blank"
											af-variation="small"
											onAfOnClick={(e) => this.linkClickHandler(e)}
										>
											Se förloppssteg -FAQ komponenten
										</digi-link>
									</p>
									<br />
									<h2>Formulär i modaler</h2>
									<p>
										Använda formulär i modaler med eftertanke, vi vill helst undvika
										modaler då den tar bort användaren från den kontext hen är i. Av
										tillgänglighetskäl är det mycket viktigt att skriva kod för modalen
										korrekt, så att en skärmläsare får rätt läsordning. Om du använder
										formulärkomponenter i en modal, så tänk på att det passar korta/enkla
										uppgifter, som tillexempel en epostadress eller kortare enkätsvar.
										Längre formulär och flöden bör ligga på ”egna sidor”.
									</p>
								</digi-layout-container>
							</div>
						</digi-layout-columns>
						<br />
						<br />
						<hr></hr>
						<br />
						<br />
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<h4>Grid och brytpunkter</h4>
								<p>Se relaterade designmönster och komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/grid-och-brytpunkter"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Grid och brytpunkter
									</digi-link-button>
								</digi-docs-related-links>
							</div>
							<div>
								<h4>Spacing</h4>
								<p>Se relaterade designmönster och komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/spacing"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Spacing
									</digi-link-button>
								</digi-docs-related-links>
							</div>
						</digi-layout-columns>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
