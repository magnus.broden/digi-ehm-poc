import { Component, getAssetPath, h } from '@stencil/core';

@Component({
	tag: 'digi-docs-about-collabration',
	styleUrl: 'digi-docs-about-collabration.scss',
	scoped: true
})
export class DigiDocsAboutCollabration {
	render() {
		return (
			<div class="digi-docs-about-collabration">
				<digi-docs-page-layout af-edit-href="pages/digi-docs-about-collabration/digi-docs-about-collabration.tsx">
					<digi-typography>
						<digi-layout-block>
							<digi-typography-heading-jumbo af-text="Samarbetsmodell"></digi-typography-heading-jumbo>
							<digi-typography-preamble>
								Utvecklingen av ett bra och hållbart designsystem bygger helt och hållet
								på samarbetet mellan designsystemet och dess intressenter. En viktig
								stöttepelare i det arbetet är den väletablerade samarbetsmodellen som
								gör det enkelt att både komma igång och hitta sätt att bidra in till
								designsystemet.
							</digi-typography-preamble>
							<br />
							<div class="digi-docs__image-wrapper">
								<digi-media-image
									afUnlazy
									af-src={getAssetPath(
										'/assets/images/Designsystemets-samarbetsmodell.svg'
									)}
									afAlt="Infografik över samarbetsmodellens olika steg."
								></digi-media-image>
							</div>
							<br />

							<digi-layout-block>
								<h2>1. Starta upp och identifiera behov</h2>
								Arbetet som sker med våra intressenters behov behöver ske i den mån vi
								kan takta arbetet ihop med de produkter och team som uttryckt behovet.
								Det betyder att vi sporadiskt kan sätta igång iterationer av samarbete
								med intressenterna, där iterationerna kan vara korta eller långa,
								beroende på behovets natur. Ofta får designsystemet in behovet ganska
								tätt inpå när arbetet behövs utföras och en del av byggstenens kapacitet
								måste därför vara dynamiskt och adaptivt.
								<h2>2. Samarbete med intressenterna</h2>
								Samarbetet med intressenterna sker strukturerat men kan skilja sig i
								natur. I den cykel som skapas där vi bjuder in till samarbete så sätter
								vi upp ett startup-möte där vi går igenom behoven. I samarbetet kan vi
								bryta ner behovet, analysera det och iterera fram lösningar i kortare
								eller längre designsprintar. Arbetet stäms av med intressenterna
								kontinuerligt, till exempel i kortare demo-möten.
								<ul>
									<li>
										<span class="digi-docs__bold-text">Deltagare: </span>
										Representanter från teamet och intressenterna
									</li>
									<li>
										<span class="digi-docs__bold-text"> Mål: </span>
										Starta upp samarbete
									</li>
									<li>
										<span class="digi-docs__bold-text">Input: </span>
										Behov, önskemål, förslag
									</li>
									<li>
										<span class="digi-docs__bold-text">Output: </span>
										Stories i backloggen
									</li>
								</ul>
								<h3>2.a Designsprint och workshop</h3>
								<ul>
									<li>
										<span class="digi-docs__bold-text">Deltagare: </span>
										Teamet, intressenter, experter, användare
									</li>
									<li>
										<span class="digi-docs__bold-text">Mål: </span>
										Formulera problemställning utifrån behov och ta fram MVP
									</li>
									<li>
										<span class="digi-docs__bold-text">Input: </span>
										Research, behov, designförslag, mål, m.m.
									</li>
									<li>
										<span class="digi-docs__bold-text">Output: </span>
										Prototyp, MVP, leveransplan, produktdefinition
									</li>
								</ul>
								<h3>2.b Visa och presentera</h3>
								<ul>
									<li>
										<span class="digi-docs__bold-text">Deltagare: </span>
										Representanter från teamet och intressenterna
									</li>
									<li>
										<span class="digi-docs__bold-text"> Mål: </span>
										Stämma av progress, feedback
									</li>
									<li>
										<span class="digi-docs__bold-text">Input: </span>
										Behov, önskemål, förslag
									</li>
									<li>
										<span class="digi-docs__bold-text">Output: </span>
										Stories i backloggen
									</li>
								</ul>
							</digi-layout-block>
						</digi-layout-block>
					</digi-typography>
				</digi-docs-page-layout>
			</div>
		);
	}
}
