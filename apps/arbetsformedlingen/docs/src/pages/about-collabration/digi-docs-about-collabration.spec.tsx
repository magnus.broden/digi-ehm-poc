import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutCollabration } from './digi-docs-about-collabration';

describe('digi-docs-about-collabration', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutCollabration],
      html: '<digi-docs-about-collabration></digi-docs-about-collabration>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-collabration>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-collabration>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutCollabration],
      html: `<digi-docs-about-collabration first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-collabration>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-collabration first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-collabration>
    `);
  });
});
