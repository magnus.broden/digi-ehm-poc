import { newSpecPage } from '@stencil/core/testing';
import { AboutAnimations } from './digi-docs-about-animations';

describe('digi-docs-about-animations', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutAnimations],
			html: '<digi-docs-about-animations></digi-docs-about-animations>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-animations>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-animations>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutAnimations],
			html: `<digi-docs-about-animations first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-animations>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-animations first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-animations>
    `);
	});
});
