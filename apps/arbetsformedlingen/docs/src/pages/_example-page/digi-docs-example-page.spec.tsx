import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsExamplePage } from './digi-docs-example-page';

describe('digi-docs-example-page', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsExamplePage],
      html: '<digi-docs-example-page></digi-docs-example-page>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-example-page>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-example-page>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsExamplePage],
      html: `<digi-docs-example-page first="Stencil" last="'Don't call me a framework' JS"></digi-docs-example-page>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-example-page first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-example-page>
    `);
  });
});
