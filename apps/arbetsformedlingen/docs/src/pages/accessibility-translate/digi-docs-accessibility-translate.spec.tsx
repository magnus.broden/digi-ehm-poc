import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAccessibilityTranslate } from './digi-docs-accessibility-translate';

describe('digi-docs-accessibility-translate', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAccessibilityTranslate],
      html: '<digi-docs-accessibility-translate></digi-docs-accessibility-translate>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-accessibility-translate>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-accessibility-translate>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAccessibilityTranslate],
      html: `<digi-docs-accessibility-translate first="Stencil" last="'Don't call me a framework' JS"></digi-docs-accessibility-translate>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-accessibility-translate first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-accessibility-translate>
    `);
  });
});
