import { Component, h, State } from '@stencil/core';
import { InfoCardHeadingLevel } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-docs-accessibility-translate',
	styleUrl: 'digi-docs-accessibility-translate.scss',
	scoped: true
})
export class DigiDocsAccessibilityTranslate {
	@State() pageName = 'Översättningar';

	render() {
		return (
			<digi-docs-page-layout
				af-page-heading={this.pageName}
				af-edit-href="pages/digi-docs-accessibility-translate.tsx"
			>
				<digi-layout-block>
					<digi-typography-preamble>
						Det finns enligt Språklagen inga exakta gränser för när svenska är
						obligatoriskt eller när det går att använda andra språk, det behövs en
						bedömning för varje enskilt fall.{' '}
					</digi-typography-preamble>
					<p>
						När Arbetsförmedlingen bedömer vad som kan översättas och inte är det
						viktigt att förstå om det rör sig om:
					</p>
					<digi-list>
						<li>Ärendehandläggning.</li>
						<li>
							Annan förvaltningsverksamhet som att lämna upplysningar och/eller
							informera.
						</li>
					</digi-list>
					<p>
						En del av vår verksamhet kan resultera i stora konsekvenser för en
						arbetssökande eller arbetsgivare och då är det särskilt viktigt att det vi
						förmedlar verkligen är begripligt. Om en översatt text är en del av
						ärendehandläggningen är det extra viktigt att den språkliga kvaliteten är
						mycket hög.
					</p>
					<p>
						Myndigheten DIGG rekommenderar att myndigheter bör översätta delar av sin
						information till andra språk och ger följande rekommendationer: Välj vad
						som behöver översättas utifrån ert uppdrag och vad målgrupperna antas
						vilja göra i tjänsten.
					</p>
					<p> Information som kan behöva översättas:</p>
					<digi-list>
						<li>Grundläggande samhällsinformation för nyanlända.</li>
						<li>
							Om möjligt viktiga e-tjänster, blanketter, checklistor och så vidare.
						</li>
						<li>Information om organisationen och dess verksamhet.</li>
						<li>
							Hur man kontaktar organisationen i olika ärenden och får information och
							service på olika språk.
						</li>
					</digi-list>
					<p>
						I vissa situationer kan hela tjänster behöva översättas medan det i andra
						situationer räcker att ge språkstöd i vissa delar exempelvis genom
						hjälptexter, checklistor, instruktionsfilm och så vidare.
					</p>
					<p>
						Mer info finns på DIGG:s sida{' '}
						<digi-link
							af-target="_blank"
							af-variation="small"
							afHref="https://webbriktlinjer.se/"
						>
							Vägledning för webbutveckling
						</digi-link>
						.
					</p>
					<p>
						När en analys visar att det finns behov av att översätta så är det
						lämpligt att följande frågor diskuteras och besvaras som underlag till
						produktplanen:
					</p>
					<digi-list>
						<li>Vilken är målgruppen och vad är behovet?</li>
						<li>
							Vad i tjänsten behöver översättas? Behöver allt översättas eller bara
							vissa delar?
						</li>
						<li>Vilka argument finns för val av språk för översättning?</li>
						<li>Hur kvalitetssäkras översättningen?</li>
						<li>Under vilken tidsperiod är översättningen giltig?</li>
						<li>Vem som ansvarar för och förvaltar översättningen?</li>
						<li>Finns det eventuella framtida kostnader för språklig förvaltning?</li>
						<li>
							På vilket sätt ska översättningen avvecklas när behovet inte längre
							finns?
						</li>
					</digi-list>
					<h2>Till vilka språk översätter vi?</h2>
					<p>
						Vilka språk, och hur många, vi väljer att översätta till behöver
						utvärderas regelbundet. Servicenyttan måste, utifrån aktuell målgrupp och
						behovsbild, ställas mot kostnader och förändringar i förvaltning som
						krävs.
					</p>
					<p>
						Antalet språk där det efterfrågas översättning och tolk är stort inom
						myndigheten. Men ett fåtal språk är mer frekventa. Vi ser därför att det
						går att prioritera vissa språk framför andra och detta är:
					</p>
					<digi-list>
						<li>
							Arabiska – utmärker sig som ett språk många av våra tolkanvändare och
							webbplatsbesökare föredrar.
						</li>
						<li>
							Engelska – är ett prioriterat språk att översätta till inom EU och
							utmärker sig som ett språk många av våra tolkanvändare och
							webbplatsbesökare föredrar. Inom myndigheten använder vi brittisk
							engelska som standard.
						</li>
					</digi-list>
					<p>
						En fördel med att översätta till engelska är kunder som själva väljer att
						använda automatiserade maskinöversättningstjänster får förbättrad kvalitén
						på översättningen om ursprungstexten är översatt till engelska.
					</p>
					<p>
						Det finns situationer där vi behöver informera utöver de språk som vi nu
						nämnt. Sådant material behöver en tydlig förvaltningsplan. Detta kan vara
						en del av produktplanen som ägs av respektive produkt.
					</p>
					<p>
						Sverige har även fem nationella minoritetsspråk. De utgörs av samiska,
						meänkieli, finska, jiddisch och romani chib. De som använder dessa språk
						har särskilda rättigheter. Användare av teckenspråk är inte en kulturell
						minoritet på det sätt som regleras i språklagen och minoritetslagen men
						teckenspråk har en särskild ställning i Sverige. Hur dessa språk ska
						hanteras beskrivs i ”Handbok - Kommunikation på andra språk än svenska i
						digitala kanaler och personliga distansmöten.”
					</p>
					<h3>
						Vem får översätta texter i digitala tjänster i ett utvecklingsteam?
					</h3>
					<p>
						De medarbetare i teamet som arbeta med att översätta i tjänsteutvecklingen
						måste ha en certifiering som säkerställer färdigheter inom ett visst språk
						baserat på hör-, skriv-, tal- och läsfärdigheter. Medarbetaren ska ha
						godkänts med uppnått testresultat CEFR B2 enligt den internationella
						standarden Common European Framework of Reference for Languages. Det är
						viktigt att säkerställa kvalitetssäkringsrutin av översättningar.
					</p>

					<digi-layout-block af-container="none">
						<h3>Att tänka på vid översättning</h3>
						<digi-expandable-accordion afHeading="Tips gällande språk för utvecklare utveckling">
							<p>
								Observera att rätt språkkod behöver anges i html-koden för att
								informationen ska läsas upp korrekt av skärmläsare, avstavas rätt och så
								vidare. Det räcker inte att följa ISO 639 som är en internationell
								standard för språkkoder. Läs mer om språkkoderna i Anpassa webbplatsen
								för flerspråkighet.
							</p>
						</digi-expandable-accordion>
					</digi-layout-block>
					<digi-layout-block af-container="none">
						<digi-expandable-accordion afHeading="Tips för klarspråk">
							<p>
								Undvik förkortningar. Förkortningar är svårare att ta till sig än hela
								uttryck när man läser. De är också svåra att tolka för exempelvis
								talsyntestjänster. Om du måste använda förkortningar se till att även
								skriva ut hela uttrycket. Det är lättare att ta till sig än
								förkortningen.
							</p>
							<p>
								Kontrollera att rätt termer, förkortningar och uttryck används. Var
								konsekvent i användningen av ord och termer. Det gäller såväl
								verksamhetstermer som ord som används för navigation och funktioner.
							</p>
							<p>
								Förklara ord och uttryck som inte är självklart begripliga för alla,
								inom parentes direkt efter ordet eller, om det rör många uttryck, i en
								separat ordlista. Förklara också ord och uttryck som är centrala för er
								tjänst.
							</p>
						</digi-expandable-accordion>
					</digi-layout-block>
					<digi-layout-block af-container="none">
						<digi-expandable-accordion afHeading="Generella tips när du skriver">
							<p>
								Undersök om andra team har översatt relevant material som ni kan
								återanvända eller länka till. Samarbeta med andra och utnyttja
								gemensamma översatta källtexter.
							</p>
							<p>
								Anpassa texterna till målgruppen innan de översätts, både vad gäller
								språknivå och innehåll.
							</p>
							<p>
								Användartesta att texten fungerar på det sätt som tänkts, ta hjälp av
								användartestare.
							</p>
							<p>
								Använd gärna standardtexter, till exempel ansvarsfriskrivningar
								(disclaimers) och beskrivningar av begrepp på olika språk.
							</p>
						</digi-expandable-accordion>
					</digi-layout-block>
					<digi-layout-block af-container="none">
						<digi-expandable-accordion afHeading="Tips vid översättning">
							<p>
								Texterna som ska översättas kan behöva skrivas på ett sätt som gör att
								de håller längre än de svenskspråkiga texterna som kanske uppdateras
								oftare. Det gäller även underlag för teckenspråksfilmer.
							</p>
							<p>
								Om endast delar av information på en webbplats eller i en tjänst
								översätts ska det tydligt framgå att all information inte är tillgänglig
								på valt språk.
							</p>
							<p>
								Låt en annan person med dokumenterad kompetens på språket korrekturläsa
								och språkgranska översättningarna.
							</p>
							<p>
								Ge information i andra format än enbart text. Även för främmande språk
								kan man behöva kombinera skrift med ljud, bild och film eller informera
								om att man kan ringa någon som talar det egna språket.
							</p>
							<p>
								Använd i största utsträckning svenska termer och begrepp Myndigheter är
								enligt § 12 i språklagen skyldiga att utveckla och tillgängliggöra
								svenska termer för sitt verksamhetsområde. När översättning behövs
								använd om möjligt både det svenska begreppet och komplettera med
								översättning.
							</p>
							<p>
								Kontrollera att den automatiska textuppläsningen fungerar också för
								andra språk än svenska.
							</p>
							<p>
								Låt gärna rubriker på webben, blanketter och liknande stå både på
								svenska och på ett annat språk. Det underlättar för den svenskspråkiga
								personal som ska hjälpa personer med annat modersmål.
							</p>
							<p>Ange språkriktning för innehåll som läses från höger till vänster.</p>
							<p>
								Teckenspråkstolk och syntolkning kan vara aktuellt i exempelvis film.
							</p>
						</digi-expandable-accordion>
					</digi-layout-block>
					<br />
					<digi-layout-block af-container="none">
						<digi-info-card
							afHeading="Handbok"
							afHeadingLevel={InfoCardHeadingLevel.H3}
						>
							<p>
								I Arbetsförmedlingens handbok för kommunikation på andra språk hjälper
								verksamheten att öka och utveckla språkstödet. Den kan du hitta här:
								<br />
								<digi-link-external
									af-target="_blank"
									af-variation="small"
									afHref="https://start.arbetsformedlingen.se/var-myndighet/styrande-dokument/handbocker/kommunikation-pa-andra-sprak-an-svenska-i-digitala-kanaler-och-personliga-distansmoten/ "
								>
									Kommunikation på andra språk än svenska i digitala kanaler och
									personliga distansmöten
								</digi-link-external>
							</p>
						</digi-info-card>
					</digi-layout-block>
					<br />
					{/* <digi-layout-block af-container='none'>
            <digi-info-card
              afHeading="Guide"
              af-type= 'info'
              afHeadingLevel={InfoCardHeadingLevel.H3}>
              <p>
                Avdelningen Digitala tjänster har tagit fram en intern Guide - översättningar i digitala tjänster som ska ge kunskap, stöd och vägledning och konkreta verktyg som underlättar i det praktiska arbetet.
              </p>
            </digi-info-card>
          </digi-layout-block> */}
				</digi-layout-block>
			</digi-docs-page-layout>
		);
	}
}
