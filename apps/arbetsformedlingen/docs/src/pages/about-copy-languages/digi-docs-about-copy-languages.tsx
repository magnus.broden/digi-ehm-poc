import { Component, h, State } from '@stencil/core';

@Component({
	tag: 'digi-docs-about-copy-languages',
	styleUrl: 'digi-docs-about-copy-languages.scss',
	scoped: true
})
export class AboutCopyLanguages {
	@State() pageName = 'Om vårt språk';

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-copy-languages/digi-docs-about-copy-languages.tsx"
				>
					<span slot="preamble">
						Vårt språk är viktigt. Hur vi utrycker oss i våra tjänster påverkar hur vi
						uppfattas. På Arbetsförmedlingen använder vi klarspråk och det innebär att
						vi som myndighet är skyldiga att skriva begripliga texter ur mottagarens
						perspektiv. Oavsett om du möter oss på webben eller i våra digitala
						tjänster.
					</span>
					<br />
					<digi-layout-container>
						<article>
							<p>
								Microcopy är ord eller begrepp i våra tjänster som tagits fram för att
								följa de upplevelseprinciperna vi har på Arbetsförmedlingen:
								professionellt, inspirerande och förtroendeingivande.
							</p>
							<p>
								När det kommer till översättningar arbetar vi lite olika på olika delar
								av Arbetsförmedlingen. Det finns inga exakta regler för vad som måste
								vara översatt men vi arbetar löpande översätta till fler språk.
							</p>
						</article>
					</digi-layout-container>
				</digi-docs-page-layout>
			</host>
		);
	}
}
