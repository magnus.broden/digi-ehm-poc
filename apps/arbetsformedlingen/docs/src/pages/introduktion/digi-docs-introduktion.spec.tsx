import { newSpecPage } from '@stencil/core/testing';
import { Introduktion } from './digi-docs-introduktion';

describe('digi-docs-introduktion', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [Introduktion],
      html: '<digi-docs-introduktion></digi-docs-introduktion>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-introduktion>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-introduktion>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [Introduktion],
      html: `<digi-docs-introduktion first="Stencil" last="'Don't call me a framework' JS"></digi-docs-introduktion>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-introduktion first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-introduktion>
    `);
  });
});
