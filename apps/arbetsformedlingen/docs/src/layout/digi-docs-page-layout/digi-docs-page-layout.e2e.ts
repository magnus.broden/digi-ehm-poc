import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-page-layout', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-page-layout></digi-docs-page-layout>');

    const element = await page.find('digi-docs-page-layout');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-page-layout></digi-docs-page-layout>');

    const element = await page.find('digi-docs-page-layout >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
