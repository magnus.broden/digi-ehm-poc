import { Component, Element, h, Prop, State } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'digi-docs-page-layout',
	styleUrl: 'digi-docs-page-layout.scss'
})
export class DigiDocsPageLayout {
	@Element() hostElement: HTMLStencilElement;

	@Prop() afEditBaseUrl: string =
		'https://bitbucket.arbetsformedlingen.se/projects/DIGI/repos/digi-monorepo/browse/apps/docs/src/';
	@Prop() afEditHref: string;

	@Prop() afPageHeading: string; // @Prop() afPageHeading !: string;
	@State() hasPreamble: boolean;
	@State() preamble: string;

	setHasPreamble() {
		this.hasPreamble = !!this.hostElement.querySelector('[slot="preamble"]');

		if (this.hasPreamble) {
			this.preamble =
				this.hostElement.querySelector('[slot="preamble"]').innerHTML;
		} else {
			this.preamble =
				'Välkommen till Arbetsförmedlingen Designsystem! Här finner du komponenter i både kod och design, designmönster, riktlinjer och mer som gäller för hela Arbetsförmedlingen.';
		}
	}

	componentWillLoad() {
		this.setHasPreamble();
	}

	componentWillUpdate() {
		this.setHasPreamble();
	}

	render() {
		return (
			<host>
				<digi-typography>
					<Helmet>
						<meta name="description" content={this.preamble} />
					</Helmet>
					<section class="digi-docs-page-layout__content">
						<digi-layout-container>
							{this.afPageHeading && (
								<digi-typography-heading-jumbo af-text={this.afPageHeading} />
							)}
							{this.hasPreamble && (
								<digi-typography-preamble>
									<slot name="preamble"></slot>
								</digi-typography-preamble>
							)}
						</digi-layout-container>
						<slot></slot>
					</section>
				</digi-typography>
			</host>
		);
	}
}
