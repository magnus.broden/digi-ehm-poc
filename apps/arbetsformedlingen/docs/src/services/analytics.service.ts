export function setAnalytics() {

  //const id: string = "54ec8ad8-bfb9-470d-9bd7-fc80034fb519"; // For acc testing
	const id: string = "79af4693-613c-4b70-820c-a351f27c3c00"; // For prod
  const dataLayerName: string = "_mtm";
	const baseURL: string = 'designsystem.arbetsformedlingen.se';
	const config: object = { subtree: true, childList: true };
	const isAFDomain: any = () => window.location.href.match(/designsystem.arbetsformedlingen/gi) ? true : false;

	let prevURL = "";
	
  (function(window, document, dataLayerName, id) {
		if (!isAFDomain()) { return; }
		window[dataLayerName]=window[dataLayerName]||[],window[dataLayerName].push({start:(new Date).getTime(),event:"stg.start"});var scripts=document.getElementsByTagName('script')[0],tags=document.createElement('script');
		function stgCreateCookie(a,b,c){var d="";if(c){var e=new Date;e.setTime(e.getTime()+24*c*60*60*1e3),d=`; expires=${e.toUTCString()}`}document.cookie=`${a}=${b}${d}; path=/`}
		var isStgDebug=(window.location.href.match("stg_debug")||document.cookie.match("stg_debug"))&&!window.location.href.match("stg_disable_debug");stgCreateCookie("stg_debug",isStgDebug?1:"",isStgDebug?14:-1);
		var qP=[];dataLayerName!=="dataLayer"&&qP.push(`data_layer_name=${dataLayerName}`),isStgDebug&&qP.push("stg_debug");var qPString=qP.length>0?(`?${qP.join("&")}`):"";
		tags.async=!0,tags.src=`https://af.containers.piwik.pro/${id}.js${qPString}`,scripts.parentNode.insertBefore(tags,scripts);
		!function(a,n,i):any{a[n]=a[n]||{};
		for(var c=0;c<i.length;c++)!function(i):any{a[n][i]=a[n][i]||{},a[n][i].api=a[n][i].api||
		function(){var a=[].slice.call(arguments,0);"string"==typeof a[0]&&
		window[dataLayerName].push({event:`${n}.${i}:${a[0]}`,parameters:
		[].slice.call(arguments,1)})}}(i[c])}(window,"ppms",["tm","cm"]);
	})(window, document, dataLayerName, id);

	const observer = new MutationObserver((): void => {
		if (isAFDomain() && window.location.href !== prevURL) {
			prevURL = window.location.href;
			
			let pathname = window.location.pathname;
			
			const trimmedPageTitle = (title: string) => title.split('/').filter(str => str !== '').join(' - ');
			window[dataLayerName]=window[dataLayerName]||[],
			window[dataLayerName].push({event: 'virtualPageView',
				pageInfo: {
					pageUri: baseURL,
					pageTitle: `${trimmedPageTitle(pathname) || 'Arbetsförmedlingen Designsystem'}`,
					applicationName: 'Designsystem'
				}
			});
		}
	});
	observer.observe(document, config);
}