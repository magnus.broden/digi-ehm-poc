import http from './http-common';

class DirectusController {
  constructor() {}
    
    getWcagItems = async () => {
        //return http.get('/items/tillganglighetslistan?fields=*.*');
        return http.get('/assets/data/tillganglighetslistan.json');
    }
}

export const DirectusApi = new DirectusController();