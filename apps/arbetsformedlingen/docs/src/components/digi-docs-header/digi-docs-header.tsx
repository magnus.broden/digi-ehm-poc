import { Component, h, Listen, State } from '@stencil/core';
import { LayoutBlockContainer, LinkVariation, UtilBreakpointObserverBreakpoints } from '@digi/arbetsformedlingen';
import { href } from 'stencil-router-v2';
import { router } from '../../global/router';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-header',
  styleUrl: 'digi-docs-header.scss',
  scoped: true,
})
export class DigiDocsHeader {
  Router = router;

  @State() isMobile: boolean;

  @Listen('afOnChange')
  breakpointChange(e: any) {
    const type = e.target.nodeName;
    if (type.toLowerCase() === 'digi-util-breakpoint-observer') {
      this.isMobile =
        e.detail.value === UtilBreakpointObserverBreakpoints.SMALL
    }
  }

  releaseNotesClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <header class="digi-docs__header">
        <digi-layout-block afContainer={LayoutBlockContainer.NONE}>
          <digi-util-breakpoint-observer>
            <div class="digi-docs__header-inner">
              <a aria-label='Designsystem startsida' {...href('/')} 
                class="digi-docs__logo-link"
              >
                <digi-logo 
                  af-system-name="Designsystem" 
                  afSvgAriaHidden={true}
                ></digi-logo>
              </a>
              <div class="digi-docs__header-end">
                {!this.isMobile && (
                  <digi-link 
                    afVariation={LinkVariation.SMALL}
                    afHref={`${state.routerRoot}release-notes`}
                    onAfOnClick={(e) => this.releaseNotesClickHandler(e)}
                    class="digi-docs__release-notes-link"
                  >
                    <digi-icon-book slot="icon"></digi-icon-book>
                    Release notes
                  </digi-link>
                )}
                {this.isMobile && (
                  <digi-navigation-sidebar-button
                    af-text="Meny"
                    class="digi-docs__sidebar-toggle"
                  ></digi-navigation-sidebar-button>
                )}
              </div>
            </div>
          </digi-util-breakpoint-observer>
        </digi-layout-block>
      </header>
    );
  }
}
