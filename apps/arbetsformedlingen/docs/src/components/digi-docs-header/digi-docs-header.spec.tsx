import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsHeader } from './digi-docs-header';

describe('digi-docs-header', () => {
  it('renders', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsHeader],
      html: '<digi-docs-header></digi-docs-header>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-header>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-header>
    `);
  });

  it('renders with values', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsHeader],
      html: `<digi-docs-header first="Stencil" last="'Don't call me a framework' JS"></digi-docs-header>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-header first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-header>
    `);
  });
});
