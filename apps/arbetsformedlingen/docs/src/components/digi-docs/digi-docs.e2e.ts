import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs', () => {
  it('renders', async () => {
    const page = await newE2EPage({ url: '/' });

    const element = await page.find('digi-docs');
    expect(element).toHaveClass('hydrated');
  });

  it('renders the title', async () => {
    const page = await newE2EPage({ url: '/' });

    const element = await page.find('digi-docs >>> h1');
    expect(element.textContent).toEqual('Stencil App Starter');
  });
});
