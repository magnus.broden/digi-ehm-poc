import { Component, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-issue-collector',
  styleUrl: 'digi-docs-issue-collector.scss',
})
export class DigiDocsIssueCollector {
  render() {
    return [
      <digi-button id="issueCollectorButton">Skapa ett ärende</digi-button>,
    ];
  }
}
