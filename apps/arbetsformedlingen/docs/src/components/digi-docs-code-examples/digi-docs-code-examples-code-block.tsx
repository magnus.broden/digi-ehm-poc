import { Component, Prop, h, State, Watch } from '@stencil/core';


import Prism from 'prismjs';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-git';
import 'prismjs/components/prism-jsx';
import 'prismjs/components/prism-tsx';

import { CodeBlockLanguage, CodeBlockVariation } from 'libs/core/package/src';


/**
 * @enums CodeBlockLanguage - code-block-language.enum.ts
 * @enums CodeBlockVariation - code-block-variation.enum.ts
 * @swedishName Kodblock i docs kodexempel
 */
@Component({
	tag: 'digi-docs-code-examples-code-block',
	styleUrls: ['digi-docs-code-examples-code-block.scss']
})
export class CodeExamplesTabsCodeBlock {
	@State() highlightedCode;

	/**
	 * En sträng med den kod som du vill visa upp.
	 * @en A string of code to use in the code block.
	 */
	@Prop() afCode: string;

	/**
	 * Sätt färgtemat. Kan vara ljust eller mörkt.
	 * @en Set code block variation. This defines the syntax highlighting and can be either light or dark.
	 */
	@Prop() afVariation: CodeBlockVariation = CodeBlockVariation.DARK;

	/**
	 * Sätt programmeringsspråk. 'html' är förvalt.
	 * @en Set code language. Defaults to html.
	 */
	@Prop() afLanguage: CodeBlockLanguage = CodeBlockLanguage.HTML;



	

	componentWillLoad() {
		Prism.manual = true;
		this.formatCode();
	}

	@Watch('afCode')
	formatCode() {
		this.highlightedCode = Prism.highlight(
			this.afCode,
			Prism.languages[this.afLanguage],
			this.afLanguage
		);
	}


	get cssModifiers() {
		return {
			'digi-code-block--light': this.afVariation === CodeBlockVariation.LIGHT,
			'digi-code-block--dark': this.afVariation === CodeBlockVariation.DARK
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-code-block': true,
					...this.cssModifiers
				}}
			>
				<pre class={`digi-code-block__pre language-${this.afLanguage}`}>
					<code
						class="digi-code-block__code"
						lang="en"
						innerHTML={this.highlightedCode}
					></code>
				</pre>
			</div>
		);
	}
}
