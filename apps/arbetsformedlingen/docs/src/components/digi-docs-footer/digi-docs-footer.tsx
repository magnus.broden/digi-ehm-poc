import { Component, h, Listen } from '@stencil/core';
import { LayoutBlockVariation, LayoutBlockContainer, LogoVariation, LogoColor, LinkVariation } from '@digi/arbetsformedlingen';
import { router } from '../../global/router';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-footer',
  styleUrl: 'digi-docs-footer.scss',
  scoped: true
})
export class DigiDocsFooter {
  Router = router;

  @Listen('afOnClick')
  clickHandler(e: any) {
    const href = e.detail.target.getAttribute('href');
    if (e.target.matches('digi-link') && href[0] === '/') {
      e.detail.preventDefault();
      this.Router.push(href);
    }
  }

  render() {
    return (
      <footer class="digi-docs__footer">
        <digi-layout-block
          af-variation={LayoutBlockVariation.PROFILE}
          af-container={LayoutBlockContainer.FLUID}
        >
          <section class="digi-docs__footer-inner">
            <div class="digi-docs__footer-logo">
              <digi-logo
                af-variation={LogoVariation.LARGE}
                af-color={LogoColor.SECONDARY}
              ></digi-logo>
            </div>

            <div>
              <digi-link
                afVariation={LinkVariation.SMALL}
                afHref={`${state.routerRoot}tillganglighetsredogorelse`}
                class="digi-docs__footer-link"
              >
                <digi-icon-accessibility-universal slot="icon"></digi-icon-accessibility-universal>
                Tillgänglighetsredogörelse
              </digi-link>
            </div>

            <div>
              <digi-link
                afVariation={LinkVariation.SMALL}
                afHref={`mailto:designsystem@arbetsformedlingen.se`}
                class="digi-docs__footer-link"
              >
                <digi-icon-envelope slot="icon"></digi-icon-envelope>
                Kontakta vår funktionsbrevlåda
              </digi-link>
            </div>

            <div>
              <digi-link
                afVariation={LinkVariation.SMALL}
                afHref={`https://gitlab.com/arbetsformedlingen/designsystem`}
                class="digi-docs__footer-link"
              >
                <digi-icon-open-source slot="icon"></digi-icon-open-source>
                Designsystemet är licenserat under Apache License 2.0
              </digi-link>
            </div>
          </section>
        </digi-layout-block>
      </footer>
    );
  }
}
