import { newSpecPage } from '@stencil/core/testing';
import { TokenDemo } from './digi-docs-token-demo';

describe('digi-docs-token-demo', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [TokenDemo],
			html: '<digi-docs-token-demo></digi-docs-token-demo>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-token-demo>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-token-demo>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [TokenDemo],
			html: `<digi-docs-token-demo first="Stencil" last="'Don't call me a framework' JS"></digi-docs-token-demo>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-token-demo first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-token-demo>
    `);
	});
});
