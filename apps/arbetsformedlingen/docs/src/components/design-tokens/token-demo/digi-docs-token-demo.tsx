import { Component, h, Prop, } from '@stencil/core';

@Component({
	tag: 'digi-docs-token-demo',
	styleUrl: 'digi-docs-token-demo.scss',
	shadow: true
})
export class TokenDemo {
	@Prop() afRadius: string;
	@Prop() afWidth: string;
	@Prop() afStyle: string;
	@Prop() afSize: string;
	
	@Prop() afFontSize: string;
	@Prop() afTextDecoration: string;
	@Prop() afFontWeight: string;
	@Prop() afLineHeight: string;

	@Prop() afAnimationDuration: string;
	@Prop() afAnimationRotation: string;
	
	get isTypoLayout(){
		return !!this.afFontSize || !!this.afFontWeight || !!this.afLineHeight || !!this.afTextDecoration;
	}

	get isAnimationLayout(){
		return !!this.afAnimationDuration || !!this.afAnimationRotation;
	}

	

	get cssModifiers() {
		return {
			'digi-docs-token-demo--border-layout': !!this.afWidth || !!this.afStyle,
			'digi-docs-token-demo--size-layout': !!this.afSize,
			'digi-docs-token-demo--typo-layout': this.isTypoLayout,
			'digi-docs-token-demo--animation-layout': this.isAnimationLayout
		}
	}
 

	get styleModifiers() {
		var myObj = {};

		myObj = {
			'--digi-docs-token-demo--border-radius': !!this.afRadius ? this.afRadius : null,
			'--digi-docs-token-demo--border-width': !!this.afWidth ? this.afWidth : null,
			'--digi-docs-token-demo--border-style': !!this.afStyle ? this.afStyle : null,
			'--digi-docs-token-demo--size': !!this.afSize ? this.afSize : null,
			
			'--digi-docs-token-demo--font-size': !!this.afFontSize ? this.afFontSize : null,
			'--digi-docs-token-typo-demo--text-decoration': !!this.afTextDecoration? this.afTextDecoration: null,
			'--digi-docs-token-typo-demo--font-weight': !!this.afFontWeight? this.afFontWeight: null,
			'--digi-docs-token-typo-demo--line-height': !!this.afLineHeight? this.afLineHeight: null,
			
			'--digi-docs-token-typo-demo--animation--duration':!!this.afAnimationDuration? this.afAnimationDuration: null,
			'--digi-docs-token-typo-demo--animation--rotation':!!this.afAnimationRotation? this.afAnimationRotation: null
		}
			
		return {
			...myObj
		}
	}

	render() {
		return (
			<host>
				<div 
					class={{
						'digi-docs-token-demo': true,
						...this.cssModifiers
					}}
					style={{
						...this.styleModifiers,
						
					}}
				>
					 <slot></slot>
					
				</div>
				
			</host>
		);
	}
}
