import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsColorSwatch } from './digi-docs-color-swatch';

describe('digi-docs-color-swatch', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorSwatch],
      html: '<digi-docs-color-swatch></digi-docs-color-swatch>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-color-swatch>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-color-swatch>
    `);
  });
});
