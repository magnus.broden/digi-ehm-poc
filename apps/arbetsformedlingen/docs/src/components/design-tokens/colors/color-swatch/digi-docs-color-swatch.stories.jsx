import { h } from '@stencil/core';

export default {
  title: 'DigiDocsColorSwatch',
  args: {
    first: 'John',
    middle: 'S',
    last: 'Doe'
  }
};

export const Primary = (args) => {
  return <digi-docs-color-swatch {...args}></digi-docs-color-swatch>;
};
