export enum DocsColorSwatchVariation {
    RECTANGLE = 'rectangle',
    CIRCLE = 'circle',
    FULL = 'full'
}