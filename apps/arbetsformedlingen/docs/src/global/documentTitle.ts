export function setDocumentTitle(activePath = '/') {
  const documentTitle = 'Arbetsförmedlingen Designsystem';

  let h1;

  if(document.querySelector('h1')) {
    h1 = document.querySelector('h1');
  } else {
    const shadowPath = document.querySelector('.digi-docs__main > digi-util-mutation-observer > *');
    h1 = shadowPath && shadowPath.shadowRoot.querySelector('h1');
  }

  if(!h1) {
    document.title = documentTitle;
    return;
  }

  const h1Text = (h1 as HTMLElement).innerText;

  document.title = (activePath == '/') 
    ? `Startsida - ${documentTitle}`
    : (h1Text == '') 
      ? `${documentTitle}`
      : `${h1Text} - ${documentTitle}`;
  
}