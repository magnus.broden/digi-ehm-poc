import { Env } from '@stencil/core';

const resources = {
  dev: {
    routerRoot: `/`,
  },
  prod: {
    routerRoot: `/mvp/komponenter/`,
  }
};

export const apis = resources[Env.apiEnv];