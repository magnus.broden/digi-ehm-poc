// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DigiPage, DigiPageBlockHero } from 'skolverket-react-dist';

export function App() {
	return (
		<DigiPage>
			<DigiPageBlockHero>
				<h1>Test</h1>
				<p>Lorem ipsum doler siit amet</p>
			</DigiPageBlockHero>
		</DigiPage>
	);
}

export default App;
