import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { DigiSkolverketAngularModule } from 'skolverket-angular-dist';

@NgModule({
	declarations: [AppComponent],
	imports: [BrowserModule, ReactiveFormsModule, DigiSkolverketAngularModule],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
