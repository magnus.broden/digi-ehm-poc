import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';
// import { string } from 'rollup-plugin-string';
import typescript from 'rollup-plugin-typescript';
import nodeResolve from 'rollup-plugin-node-resolve';
// https://stenciljs.com/docs/config

const dev: boolean = process.argv && process.argv.indexOf('--dev') > -1;
const apiEnv: string = dev ? 'dev' : 'prod';

console.log('busting cache: 2303283');

export const config: Config = {
	namespace: 'digi-skolverket-docs',
	globalStyle: 'src/global/app.scss',
	globalScript: 'src/global/app.ts',
	taskQueue: 'async',
	env: {
		apiEnv: apiEnv
	},
	devServer: {
		reloadStrategy: 'pageReload',
		port: 4444
	},
	// enableCache: false,
	outputTargets: [
		{
			type: 'dist',
			esmLoaderPath: '../loader'
		},
		{
			type: 'www',
			// comment the following line to disable service workers in production
			// serviceWorker: null,
			baseUrl: 'http://.local/',
			serviceWorker: {
				swSrc: './src/service-worker.js',
				swDest: '../../../../dist/apps/skolverket/docs/www/service-worker.js'
			},
			copy: [
				{
					src: '../../../../libs/skolverket/fonts/src/assets/fonts/**/*',
					dest: './assets/fonts'
				},
				{
					src: '../../../../libs/skolverket/fonts/src/assets/fonts/**/*',
					dest: './build/assets/fonts'
				},
				{
					src: '../../../../libs/skolverket/package/src/public',
					dest: './build/public'
				}
			]
		},
		{
			type: 'www',
			dir: 'www-netlify',
			// comment the following line to disable service workers in production
			serviceWorker: null,
			baseUrl: 'http://.local/',
			copy: [
				{ src: 'netlify.toml', dest: 'netlify.toml' },
				{
					src: '../../../../libs/skolverket/fonts/src/assets/fonts/**/*',
					dest: './assets/fonts'
				},
				{
					src: '../../../../libs/skolverket/fonts/src/assets/fonts/**/*',
					dest: './build/assets/fonts'
				},
				{
					src: '../../../../libs/skolverket/package/src/public',
					dest: './build/public'
				}
			]
		}
	],
	plugins: [sass()],
	rollupPlugins: {
		before: [
			// string({
			// 	// Required to be specified
			// 	include: '**/*.md',

			// 	// Undefined by default
			// 	exclude: ['**/index.html']
			// })
			nodeResolve({ browser: true }),
			typescript({ tsconfig: `${__dirname}/tsconfig.json` })
		],
		after: []
	}
};
