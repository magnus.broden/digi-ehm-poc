import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-logo',
	scoped: true
})
export class PageCategoryLogo {
	render() {
		return (
			<CategoryPage
				category={Category.LOGO}
				preamble="Skolverkets logotyp i olika varianter."
			></CategoryPage>
		);
	}
}
