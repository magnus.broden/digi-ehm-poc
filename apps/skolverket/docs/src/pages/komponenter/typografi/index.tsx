import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-typografi',
	scoped: true
})
export class PageCategoryTypografi {
	render() {
		return (
			<CategoryPage
				category={Category.TYPOGRAPHY}
				preamble="Grundläggande typografi och typografiska element."
			></CategoryPage>
		);
	}
}
