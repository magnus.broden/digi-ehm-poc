import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-dialog',
	scoped: true
})
export class PageCategoryDialog {
	render() {
		return (
			<CategoryPage
				category={Category.DIALOG}
				preamble="Modaler och dialoger."
			></CategoryPage>
		);
	}
}
