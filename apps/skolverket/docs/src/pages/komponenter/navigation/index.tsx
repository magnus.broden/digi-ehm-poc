import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-navigation',
	scoped: true
})
export class PageCategoryNavigation {
	render() {
		return (
			<CategoryPage
				category={Category.NAVIGATION}
				preamble="Menyer, brödsmulor och andra typer av navigationskomponenter."
			></CategoryPage>
		);
	}
}
