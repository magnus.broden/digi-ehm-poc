import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-laddningsindikatorer',
	scoped: true
})
export class PageCategoryLaddningsindikatorer {
	render() {
		return (
			<CategoryPage
				category={Category.LOADER}
				preamble="Komponenter för att visa pågående processer, nedladdningar och dylikt."
			></CategoryPage>
		);
	}
}
