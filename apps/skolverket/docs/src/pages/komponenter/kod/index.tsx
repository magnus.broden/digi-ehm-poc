import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-kod',
	scoped: true
})
export class PageCategoryKod {
	render() {
		return (
			<CategoryPage
				category={Category.CODE}
				preamble="Kodblock, kodelement och kodexempel."
			></CategoryPage>
		);
	}
}
