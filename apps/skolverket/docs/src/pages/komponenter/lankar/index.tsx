import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-lankar',
	scoped: true
})
export class PageCategoryLankar {
	render() {
		return (
			<CategoryPage
				category={Category.LINK}
				preamble="Interna och externa länkar."
			></CategoryPage>
		);
	}
}
