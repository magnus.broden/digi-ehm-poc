import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-verktyg',
	scoped: true
})
export class PageCategoryVerktyg {
	render() {
		return (
			<CategoryPage
				category={Category.UTIL}
				preamble="Verktyg för att hantera fokus, klick, förändring av skärmstorlekar och dylikt. Flera komponenter i kategorin är abstraktioner runt javascript-api:er som Intersection Observer och liknande."
			></CategoryPage>
		);
	}
}
