import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-media',
	scoped: true
})
export class PageCategoryMedia {
	render() {
		return (
			<CategoryPage
				category={Category.MEDIA}
				preamble="Bilder, videor och ljud."
			></CategoryPage>
		);
	}
}
