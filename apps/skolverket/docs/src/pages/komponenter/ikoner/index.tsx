import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-ikoner',
	scoped: true
})
export class PageCategoryIkoner {
	render() {
		return (
			<CategoryPage
				category={Category.ICON}
				preamble="Skolverkets ikonsystem i komponentform."
			></CategoryPage>
		);
	}
}
