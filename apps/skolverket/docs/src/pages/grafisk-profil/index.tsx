import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, h } from '@stencil/core';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'page-grafisk-profil',
	scoped: true
})
export class PageGrafiskProfil {
	render() {
		return (
			<ArticlePage
				head={<title>Om vår grafiska profil</title>}
				heading="Om vår grafiska profil"
				preamble="Skolverkets kommunikation ska vara tydlig och garantera god tillgänglighet. Det ska vara lätt att känna igen Skolverket som avsändare oavsett vilken kanal som används för kommunikation - våra webbplatser, sociala medier, film eller trycksaker."
			>
				<p>
					Logotyp, typografi, färger och grafiska element utgör byggstenarna i den
					grafiska profilen. Tillsammans bildar de en enhetlig visuell identitet.
				</p>
				<h2>Den grafiska manualen är ditt bästa verktyg</h2>
				<p>
					Den grafiska manualen beskriver hur vår grafiska profil ser ut och ger
					praktiska anvisningar för hur de olika grafiska elementen får användas. Med
					manualens hjälp får du snabbt koll på:
				</p>
				<p
					style={{
						'max-width': 'none',
						'padding-block-start': 'var(--MARGIN--BODY)'
					}}
				>
					<digi-layout-stacked-blocks>
						<digi-card-link>
							<h3 slot="link-heading">
								<a {...href('/grafisk-profil/logotyp')}>Logotyp</a>
							</h3>
							<p>
								Allt du behöver veta om hur och när Skolverket logotyp skall stå som
								avsändare.
							</p>
						</digi-card-link>
						<digi-card-link>
							<h3 slot="link-heading">
								<a {...href('/grafisk-profil/farger')}>Färger</a>
							</h3>
							<p>
								Profilfärger och funktionsfärger. Förklaringar för när de används och
								verktyg för att hitta rätt färg för rätt tillfälle. Här finns också
								färgkartor för våra färgtokens och tryckfärger.
							</p>
						</digi-card-link>
						<digi-card-link>
							<h3 slot="link-heading">
								<a {...href('/grafisk-profil/typografi')}>Typografi</a>
							</h3>
							<p>
								När och hur vi använder våra olika typsnitt. Nerladdning av
								typsnittsfiler.
							</p>
						</digi-card-link>
						<digi-card-link>
							<h3 slot="link-heading">
								<a {...href('/grafisk-profil/grafik')}>Grafiska element</a>
							</h3>
							<p>
								Vi har fler olika typer av grafik. Förklaringar på när och hur de skall
								användas. Informationsgrafik, diagram och funktionsikoner.
							</p>
						</digi-card-link>
						<digi-card-link>
							<h3 slot="link-heading">
								<a {...href('/grafisk-profil/illustrationer')}>
									Illustrationer, diagram och bilder
								</a>
							</h3>
							<p>
								Guide till hur vi använder illustrationer för att förstärka känslor
								eller händelser.
							</p>
						</digi-card-link>
						<digi-card-link>
							<h3 slot="link-heading">
								<a {...href('/grafisk-profil/bildsprak')}>Bilder och bildspråk</a>
							</h3>
							<p>Genomgång av bildpolicy och hur du hittar rätt foto i mediabanken.</p>
						</digi-card-link>
					</digi-layout-stacked-blocks>
				</p>
			</ArticlePage>
		);
	}
}
