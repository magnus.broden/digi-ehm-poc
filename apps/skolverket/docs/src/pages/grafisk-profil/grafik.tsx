import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, getAssetPath, h } from '@stencil/core';

@Component({
	tag: 'page-grafik',
	scoped: true
})
export class PageGrafik {
	render() {
		return (
			<ArticlePage
				head={<title>Grafiska element</title>}
				heading="Grafiska element"
				preamble="Som ytterligare verktyg i vår kommunikation finns två typer av grafiska element – mönster och bildsymboler. De möjliggör ett mer varierat uttryck, samt tillför färg och energi."
			>
				<h2>Mönster</h2>
				<p>
					Grundmönstren är uppbyggda av linjer och enkla genometiska former. Eftersom
					mönstren huvudsakligen har en dekorativ funktion, finns inte samma krav på
					kontrast som när det gäller text. Här kan färgen på linjen och bakgrunden
					kombineras fritt. Var dock vaksam vid placering av logo på mönster – som
					huvudregel placeras logotypen i ett helt mönsterfritt område. Annars får
					mönstret tonas ner.
				</p>
				<h2>Mönster som bakgrunder på webbplats</h2>
				<p>
					På webbplatsen kan du använda mönster för att markera avsnitt och för att
					hjälpa användaren orientera sig mellan undersidor och sektioner. Mönstren
					du får använda för webbplatsen är linjerat, rutigt och prickigt. Dessa
					mönster i olika toningar av bakgrundsfärgerna har en låg kontrast för att
					inte skapa för start visuellt brus och text kan användas direkt på dessa.
				</p>
				<h3>Följande mönster får användas som bakgrund på webbplats</h3>
				<digi-layout-stacked-blocks>
					<digi-media-figure afFigcaption={'Linjerat 1'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/striped_1.svg')}
							afAlt={'Linjerad bakgrund 1'}
						/>
					</digi-media-figure>
					<digi-media-figure afFigcaption={'Linjerat 2'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/striped_2.svg')}
							afAlt={'Linjerad bakgrund 2'}
						/>
					</digi-media-figure>
					<digi-media-figure afFigcaption={'Linjerat 3'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/striped_3.svg')}
							afAlt={'Linjerad bakgrund 3'}
						/>
					</digi-media-figure>
					<digi-media-figure afFigcaption={'Rutat 1'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/square_1.svg')}
							afAlt={'Rutad bakgrund 1'}
						/>
					</digi-media-figure>
					<digi-media-figure afFigcaption={'Rutat 2'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/square_2.svg')}
							afAlt={'Rutad bakgrund 2'}
						/>
					</digi-media-figure>
					<digi-media-figure afFigcaption={'Rutat 3'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/square_3.svg')}
							afAlt={'Rutad bakgrund 3'}
						/>
					</digi-media-figure>
					<digi-media-figure afFigcaption={'Prickat 1'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/dotted_1.svg')}
							afAlt={'Prickad bakgrund 1'}
						/>
					</digi-media-figure>
					<digi-media-figure afFigcaption={'Prickat 2'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/dotted_2.svg')}
							afAlt={'Prickad bakgrund 2'}
						/>
					</digi-media-figure>
					<digi-media-figure afFigcaption={'Prickat 3'}>
						<digi-media-image
							afSrc={getAssetPath('/assets/images/dotted_3.svg')}
							afAlt={'Prickad bakgrund 3'}
						/>
					</digi-media-figure>
				</digi-layout-stacked-blocks>
				<h2>Bildsymboler</h2>
				<p>
					Vi kan även dekorera och/eller förstärka innehåll med våra bildsymboler,
					enkla symboliska linjeteckningar placerade på en rund färgplatta. De kan
					fungera både som ett dekorativt element och som en betydelsebärande signal.
					Till exempel kan de användas på ett rapportomslag för att signalera en viss
					publikationsserie.
				</p>
				<p>
					Den konsekventa utformningen av bildsymbolerna med en linjeteckning
					placerad på en rund färgplatta gör att innehållet/symbolen kan varieras men
					ändå behålla en tydlig form som kan kännas igen som Skolverket.
				</p>
				<p>
					För bildsymbolerna gäller att alla färger i Skolverkets palett kan användas
					som bakgrunds- eller linjefärg.
				</p>
				<p>
					Skolverket har tagit fram ett antal olika bildsymboler som får användas.
					Vissa av dessa får användas fritt medan vissa är dedikerade till ett
					särskilt område eller publikation.
				</p>
				<h3>Exempel på bildsymboler för allmän användning</h3>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/symboler-exempel-1.png')}
						afAlt={'Olika symboler för allmän användning'}
					/>
				</p>
				<h3>Exempel på bildsymboler dedikerade till område</h3>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/symboler-exempel-2.png')}
						afAlt={'Olika symboler för dedikerade områden'}
					/>
				</p>
				<h3>Bildsymboler på webben</h3>
				<p>
					På webben ska bildsymbolerna användas med sparsamhet då de kan
					sammanblandas med funktionsikoner.
				</p>
				<h2>Mer om mönster och bildsymboler</h2>
				<p>
					Du hittar mer information och kan se hela biblioteket av mönster och
					bildsymboler i vår grafiska manual.
					{/* TODO: Länk här */}
				</p>
			</ArticlePage>
		);
	}
}
