import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, h } from '@stencil/core';

@Component({
	tag: 'page-typografi',
	scoped: true
})
export class PageTypografi {
	render() {
		return (
			<ArticlePage head={<title>Typografi</title>} heading="Typografi">
				<h2>Tryckt material och digitala gränssnitt</h2>
				<p>
					I våra externt producerade material, till exempel publikationer så använder
					vi två typsnittsfamiljer - Source Sans Pro och Mercury Text G1. I trycka
					produktioner används Source Sans Pro i rubriker medan Mercury Text G1
					används i brödtext.
				</p>
				<p>
					I digitala gränssnitt används Source Sans Pro uteslutande för både rubriker
					och brödtext.
				</p>
				<h3>Source Sans Pro</h3>
				<p>
					Source Sans Pro ritades för att fungera optimalt i framför allt
					användargränssnitt. Det har en mjuk karaktär, och ger därför ett mer
					humanistiskt intryck. Det är tydligt, framför allt i mycket små storlekar.
				</p>
				<p>
					Source Sans Pro används i rubriker, bildtexter, tabeller, grafik och
					kortare faktatexter. Det används också genomgående på våra webbplatser.
				</p>
				<h3>Mercury text G1</h3>
				<p>
					Mercury Text G1 är ett stabilt, rent och klassiskt typsnitt med moderna
					former. Typsnittet är lättläst även i stora textmängder och fungerar även i
					små storlekar.
				</p>
				<p>Mercury Text G1 används i brödtext för tryck, aldrig i rubriker.</p>
				<h2>Internt material och dokument</h2>
				<p>
					I våra interna dokument och när vi tar fram kommunikationsmaterial på egen
					hand finns det ett antal mallar som stöd i det arbetet. Mallarna i Office
					bygger på typsnitten Arial och Times New Roman som finns på våra datorer.
					Se till att alltid använda Skolverkets dokumentmallar även för internt
					arbetsmaterial. På så sätt undviker du att format och andra teckensnitt
					smyger sig med när du till exempel kopierar text mellan olika dokument.
				</p>
				<p>
					Source Sans Pro och Mercury Text G1 är de typsnitt som i första hand ska
					användas i de material vi producerar. Om dessa typsnitt av tekniska skäl
					inte kan användas ska Source Sans Pro bytas ut mot Arial och Mercury Text
					G1 bytas ut mot Times New Roman.
				</p>
				<h3>Alternativt typsnitt i illustrationer</h3>
				<p>
					I undantagsfall kan ett typsnitt av en mer handskriven karaktär användas
					till illustrationer för såväl webb som tryck. Det ska dock användas med
					sparsamhet. Det utvalda typsnittet Atma går att ladda ner här:{' '}
					<a href="https://fonts.google.com/specimen/Atma">
						https://fonts.google.com/specimen/Atma
					</a>
				</p>
			</ArticlePage>
		);
	}
}
