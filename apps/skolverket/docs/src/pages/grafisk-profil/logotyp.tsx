import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';
import { Component, getAssetPath, h } from '@stencil/core';
import {
	LayoutMediaObjectAlignment,
	NotificationDetailVariation
} from '@digi/skolverket';

@Component({
	tag: 'page-logotyp',
	scoped: true
})
export class PageLogotyp {
	render() {
		return (
			<ArticlePage
				head={<title>Logotyp</title>}
				heading="Logotyp"
				preamble="Logotypen är vårt främsta och viktigaste verktyg för att lyfta fram Skolverket som avsändare. Vår logotyp består av en så kallad ordbild - där Skolverkets namn är skrivet i skrivstil"
			>
				<p>
					Skolverkets logotyp finns i flera varianter, både som positiv och negativ
					logotyp.
				</p>
				<ul>
					<li>Positiv logotyp: Färgad text mot ljus bakgrund</li>
					<li>Negativ logotyp: Vit text mot färgad bakgrund</li>
				</ul>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/logotyp-positiv-negativ.png')}
						afAlt={'Skolverkets logotyp i olika varianter'}
						afWidth={'615'}
						afHeight={'102'}
					/>
				</p>
				<h2>Användning</h2>
				<p>
					Du får använda logotypen i sammanhang där vi är avsändare och i
					redaktionella sammanhang, men inte i reklam eller marknadsföring. Använd i
					första hand logotypen i profilfärgen, primärfärg 1. Logotypen finns även i
					svart och i vitt för användning på foto eller färgad bakgrund.
				</p>
				<p>
					Skolverkets logotyp ska finnas med som avsändare på alla Skolverkets
					webbplatser och digitala tjänster. På{' '}
					<a href="https://www.skolverket.se/">skolverket.se</a> används den i
					sidhuvudet. På syskonwebbplatser och på e-tjänster och verktyg återfinns
					den i sidfoten.
				</p>
				<h3>Storlek och proportioner</h3>
				<p>
					Logotypens grundstorlek är 37 mm bred, men storleken kan anpassas till
					materialet.
				</p>
				<p>
					Dock får logotypen aldrig vara mindre än 24 mm bred och proportionerna får
					aldrig förändras.
				</p>
				<h3>Frizon</h3>
				<p>
					Runt logotypen ska det alltid finnas en frizon. Den fria ytan runt
					logotypen ska motsvara minst logotypens versalhöjd.
				</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/logotyp-frizon.png')}
						afAlt={'Visualisering av logotypens frizon'}
						afWidth={'226'}
						afHeight={'128'}
					/>
				</p>
				<h3>Användning av logotyp på färgad bakgrund</h3>
				<p>
					Logotypen kan vid behov placeras på en färgad bakgrund, men då bara i en
					kombination som garanterar god kontrast.
				</p>
				<p>
					<digi-notification-detail>
						<h4 slot="heading">Tumregel</h4>
						<ul>
							<li>Ljus bakgrund - Använd alltid logotyp med färgad text (positiv)</li>
							<li>Mörk bakgrund - Använd alltid logotyp med vit text (negativ)</li>
						</ul>
					</digi-notification-detail>
				</p>
				<h4>Logotypen mot bakgrundsfärger</h4>
				<p>Så här kan logotypen användas mot olika bakgrundsfärger.</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/logotyp-fargad-bakgrund.png')}
						afAlt={'Logotypen på olika bakgrundsfärger'}
						afWidth={'1110'}
						afHeight={'475'}
					/>
				</p>
				<h3>Placering av logotypen på foto</h3>
				<p>
					Logotypen bör finnas med på framsidan av alla trycksaker (med undantag för
					när externa leverantörer står för innehållet - då placeras logon på
					baksidan).
				</p>
				<p>Runt logotypen ska det alltid finnas en frizon.</p>
				<p>
					Logotypen får vid behov placeras på ett foto. Det är dock mycket viktigt
					att se till att logotypen placeras mot en lugn bakgrund och att det är god
					kontrast mellan logotyp och bakgrund.
				</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/logotyp-foto.jpg')}
						afAlt={'Logotypen på olika fotografier'}
						afWidth={'739'}
						afHeight={'716'}
					/>
				</p>
				<h3>Användning av logotypen på grafiska mönster</h3>
				<p>
					Logotypen kan - precis som i Skolverkets Powerpointmallar - placeras på
					våra grafiska mönster. Men det är då viktigt att mönstret inte stör
					logotypen visuellt på samma sätt som vid placering på foton. Även här är
					lugn och god kontrast mycket viktigt. Dämpa därför kontrasten genom att
					exempelvis tona ner mönstret till under 50 procents transparens eller vid
					vitt mönster tona bakgrundsfärgen eller placera logotypen i ett helt
					mönsterfritt område.
				</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/logotyp-grafiskt-monster-ja.png')}
						afAlt={'Bra exempel på logotyp på grafiskt mönster'}
						afWidth={'297'}
						afHeight={'297'}
					/>
				</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/logotyp-grafiskt-monster-nej.png')}
						afAlt={'Dåligt exempel på logotyp på grafiskt mönster'}
						afWidth={'297'}
						afHeight={'297'}
					/>
				</p>
				<h2>Varianter av logotyp</h2>
				<p>Logotypen finns även i fler varianter:</p>
				<ul>
					<li>Svensk variant med integrerad webbadress</li>
					<li>Engelsk variant för användning i internationella sammanhang</li>
					<li>Engelsk variant med integrerad webbadress</li>
				</ul>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/logotyp-varianter.png')}
						afAlt={'Logotypen i olika varianter'}
						afWidth={'603'}
						afHeight={'229'}
					/>
				</p>
				<h3>Favikon</h3>
				<p>
					Skolverkets favikon bygger på versala bokstaven ”S” från logotypen i vitt
					och har en cirkel i Primärfärg 1 som bakgrund.
				</p>
				<p>
					Favikonen får användas i sidhuvudet webbplatser, e-tjänster och i sociala
					medier för att markera Skolverket som avsändare. Favikonen får aldrig
					användas som ensam avsändare utan används alltid i kombination med
					skolverkets logotyp i sidfoten.
				</p>
				<p>
					Den kan också användas i andra sammanhang, till exempel på rygg eller
					baksida på olika trycksaker. Den finns även med i vår PowerPoint-mall.
				</p>
				<p>
					Favikonen får ses som en mer lättplacerad avsändarmarkör än logotypen,
					vilken den dock inte ersätter. Logotypen ska för tydlighetens skull alltid
					finnas med eller - som i sociala medier - texten "Skolverket".
				</p>
				<p>
					I tryck är den minsta storleken som favikonen får användas i 7 x 7 mm.
				</p>
				<p>
					<digi-layout-media-object afAlignment={LayoutMediaObjectAlignment.CENTER}>
						<digi-media-image
							slot="media"
							afSrc={getAssetPath('/assets/images/logotyp-favikon-vanlig.png')}
							afAlt={'Logotypen som favikon'}
							afWidth={'93'}
							afHeight={'92'}
						/>
						<p>
							Favikonen i sin grundform, med ett vitt ”S” i en cirkel med Primärfärg 1
							som bakgrund.
						</p>
					</digi-layout-media-object>
				</p>
				<p>
					<digi-layout-media-object afAlignment={LayoutMediaObjectAlignment.CENTER}>
						<digi-media-image
							slot="media"
							afSrc={getAssetPath('/assets/images/logotyp-favikon-alternativ.png')}
							afAlt={'Logotypen som favikon i alternativ form'}
							afWidth={'93'}
							afHeight={'92'}
						/>
						<p>
							Favikonen finns även i en version med ”S” i Primärfärg 1 på vit botten.
							Denna version kan användas till exempel på bokryggar.
						</p>
					</digi-layout-media-object>
				</p>
				<h2>Filformat för logotyp</h2>
				<ul>
					<li>För tryck används vektoriserade format (ai- eller eps-fil)</li>
					<li>På webben används gif- eller svg-fil</li>
					<li>För Word och PowerPoint används png-format</li>
				</ul>
				<p>
					<digi-notification-detail>
						<h3 slot="heading">Gör så här...</h3>
						<ul>
							<li>Behåll logotypens proportioner</li>
							<li>Se till att skapa god kontrast mot bakgrunden</li>
							<li>Använd rätt filformat anpassad för ändamålet</li>
							<li>
								Skriv ”Skolverket”, istället för att använda logotypen, i löptext
							</li>
						</ul>
					</digi-notification-detail>
				</p>
				<p>
					<digi-notification-detail afVariation={NotificationDetailVariation.DANGER}>
						<h3 slot="heading">...men inte så här</h3>
						<ul>
							<li>Ändra logotypens utformning eller proportioner</li>
							<li>Addera effekter eller annan grafik på logotypen</li>
							<li>Använd logotypen i löpande text</li>
							<li>
								Lägg logotypen på en bakgrund med dålig kontrast mot texten i logotypen
							</li>
							<li>
								Använd webbfiler för tryckproduktion eller tvärtom då de är optimerade
								efter användningsområde
							</li>
						</ul>
					</digi-notification-detail>
				</p>
				<h2>Logotypen i komponentbiblioteket</h2>
				<p>
					Komponentbiblioteket innehåller flera olika logotyper för olika ändamål. Se
					hur du kan använda dem här:
				</p>
				<digi-list-link>
					<li>
						<ComponentLink tag="digi-logo" />
					</li>
					<li>
						<ComponentLink tag="digi-logo-sister" />
					</li>
					<li>
						<ComponentLink tag="digi-logo-service" />
					</li>
				</digi-list-link>
			</ArticlePage>
		);
	}
}
