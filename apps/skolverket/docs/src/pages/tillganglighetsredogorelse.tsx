import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-tillganglighetsredogorelse',
	scoped: true
})
export class PageTillganglighetsredogorelse {
	render() {
		return (
			<article>
				<Helmet>
					<title>Om tillgänglighetsredogörelse</title>
				</Helmet>
				<h1>om tillgänglighetsredogörelse</h1>
				<p>Så här gör man med tillgänglighetsredogörelse</p>
			</article>
		);
	}
}
