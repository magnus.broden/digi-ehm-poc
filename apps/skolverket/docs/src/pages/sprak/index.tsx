import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-sprak',
	scoped: true
})
export class PageSprak {
	render() {
		return (
			<article>
				<Helmet>
					<title>Om språk</title>
				</Helmet>
				<h1>om språk</h1>
				<p>Så här gör man med språk</p>
			</article>
		);
	}
}
