import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, h } from '@stencil/core';

@Component({
	tag: 'page-oversattningar',
	scoped: true
})
export class PageOversattningar {
	render() {
		return (
			<ArticlePage
				head={<title>Översättningar</title>}
				heading="Översättningar"
				preamble="På Skolverket kommunicerar vi i huvudsak på svenska, men måste från fall till fall bedöma vad vi ska översätta och till vilka språk. Detta gör vi utifrån våra målgruppers behov."
			>
				<h2>Vägledning för översättningar</h2>
				<p>
					Vi har en vägledning för översättningar, som ska vara ett stöd för dig som
					planerar att översätta skrivet, talat eller filmat innehåll inom ramen för
					Skolverkets verksamhet. Det kan till exempel handla om innehåll i form av
					webbtexter, publikationer, filmer, kampanjer, rapporter, stödmaterial eller
					underlag i arbetet med internationella kontakter. I vägledningen kan du
					även se en rutin och checklista för översättningar.
				</p>
				<p>
					<a href="https://kanalen.skolverket.se/stod-i-arbetet/kommunikation/interna-styrdokument-for-kommunikation/vagledning-for-oversattningar-1.76046">
						Vägledning för översättningar, Skolverkets intranät
					</a>
				</p>
				<h2>Målgruppernas behov vägleder oss</h2>
				<p>
					Vi kommunicerar i huvudsak på svenska som dels är huvudspråk i Sverige,
					dels arbetsspråk i den svenska skolan. Målgrupper med annat modersmål än
					svenska kan däremot behöva information om skola och utbildning på sitt eget
					modersmål eller på ett annat språk än svenska.
				</p>
				<p>
					Beslut om vad som ska översättas ska i första hand utgå från målgruppernas
					behov, förutsatt att innehållet inte omfattas av språklagen eller lagen om
					nationella minoriteter och minoritetsspråk.
				</p>
				<p>
					Vi kan bland annat stämma av målgruppernas behov med hjälp av:
					<ul>
						<li>
							statistik över nedladdning och försäljning av översatta publikationer och
							pdf:er
						</li>
						<li>
							analyser av efterfrågan som myndigheten har fått via vår
							upplysningstjänst, mejl eller på annat sätt
						</li>
						<li>Språkrådet och Tolk- och översättningsinstitutet</li>
						<li>uppgifter och statistik från andra myndigheter</li>
						<li>referensgrupper inom området eller samråd.</li>
					</ul>
				</p>
				<h2>Myndighetens översättningsgrupp</h2>
				<p>
					Skolverkets har en översättningsgrupp, som kan vara ett bollplank när du
					står inför att översätta till ett annat språk. Syftet med
					översättningsgruppen är att ha samlad kunskap och ansvar för samordning i
					myndigheten kring frågor som berör översättning.
				</p>
				<p>
					Kommunikationsavdelningen har ett övergripande ansvar för
					översättningsgruppen och vägledningen. Gruppen består av medlemmar från de
					flesta av myndighetens avdelningar.
				</p>
				<p>
					<a href="https://kanalen.skolverket.se/pagaende-arbete-och-resultat/myndighetens-oversattningsgrupp/myndighetens-oversattningsgrupp-1.76047">
						Översättningsgruppen, Skolverkets intranät
					</a>
				</p>
				<h2>Vad ska vi översätta?</h2>
				<p>
					Vilket innehåll som ska översättas avgörs utifrån vilken målgrupp som
					innehållet riktar sig till och målgruppens behov. Du kan läsa mer om hur du
					kan tänka vid funderingar om ett material ska översättas eller inte i
					vägledningen för översättningar.
				</p>
				<h2>Vilka språk ska vi översätta till?</h2>
				<h3>De officiella minoritetsspråken</h3>
				<p>
					Skolverket är skyldig att ge grundläggande information om sin verksamhet på
					de fem nationella minoritetsspråken.
				</p>
				<h3>Svenskt teckenspråk</h3>
				<p>
					Den som är döv eller hörselskadad har rätt att lära sig, utveckla och
					använda det svenska teckenspråket. Om målgruppen har behov av genomgående
					syntolkning, teckenspråkstolkning eller information på fler språk än
					svenska behöver du anpassa film- och ljudproduktionen efter deras behov.
					Mer om syntolkning och teckenspråkstolkning för film- eller
					ljudproduktioner hittar du i vägledning för film och ljud.
				</p>
				<p>
					<a href="https://kanalen.skolverket.se/stod-i-arbetet/kommunikation/interna-styrdokument-for-kommunikation/vagledning-for-film-och-ljud/tillganglighet-1.65235">
						Tillgänglighet och vägledningen för film och ljud, Skolverkets intranät
					</a>
				</p>
				<h2>Tips för utvecklare - rätt språkkod måste anges</h2>
				<p>
					Rätt språkkod behöver anges i html-koden för att informationen ska läsas
					upp korrekt av skärmläsare, avstavas rätt och så vidare. Det räcker inte
					att följa ISO 639 som är en internationell standard för språkkoder.
				</p>
				<p>
					<a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/lang">
						Om språkattribut i html på MDN Web Docs
					</a>
				</p>
				<h2>Lagar</h2>
				<a
					style={{ display: 'block' }}
					href="https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/spraklag-2009600_sfs-2009-600"
				>
					Språklagen (2009:600)
				</a>
				<a href="https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/lag-2009724-om-nationella-minoriteter-och_sfs-2009-724">
					Lag (2009:724) om nationella minoriteter och minoritetsspråk
				</a>
			</ArticlePage>
		);
	}
}
