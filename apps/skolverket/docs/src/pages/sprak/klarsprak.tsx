import { Component, getAssetPath, h } from '@stencil/core';
import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';

@Component({
	tag: 'page-klarsprak',
	scoped: true
})
export class PageKlarsprak {
	render() {
		return (
			<ArticlePage
				head={<title>Klarspråk</title>}
				heading="Klarspråk"
				preamble="På Skolverket skriver och kommunicerar vi på klarspråk. Det betyder att språket som vi använder ska vara vårdat, enkelt och begripligt för mottagaren."
			>
				<h2>Vad är klarspråk?</h2>
				<p>
					Klarspråk står för ett vårdat, enkelt och begripligt myndighetsspråk. I
					Sverige har vi en språklag som säger att språket i offentlig verksamhet ska
					följa klarspråksprinciperna. Klarspråk omfattar alla delar av en text -
					från informationsurvalet, strukturen och rubrikerna till meningsbyggnad och
					ordval.
				</p>
				<p>
					<a href="https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/spraklag-2009600_sfs-2009-600">
						Språklagen (2009:600)
					</a>
				</p>
				<h2>Tydliga texter sparar tid och skapar förtroende</h2>
				<p>
					<ul>
						<li>
							Ett språk som våra mottagare förstår främjar demokratin och stärker
							förtroendet för oss som myndighet.
						</li>
						<li>
							Klarspråk är också ett sätt att värna om både vår egen och andras tid.
							När du skriver enkelt och begripligt går det snabbare att läsa texten och
							förstå budskapet.
						</li>
						<li>
							Texter som är begripliga leder även till färre frågor som tar tid för oss
							att svara på.
						</li>
					</ul>
				</p>
				<h2>Använd Skolverkets 14 klarspråksprinciper</h2>
				<p>
					På Skolverket har vi en lathund som innehåller 14 frågor som du kan ställa
					dig när du ska klarspråksgranska en befintlig text eller när du planerar
					att ta fram en ny text.
				</p>
				<p>
					<ol>
						<li>Är texten skriven ur mottagarens perspektiv?</li>
						<li>Har texten ett tydligt syfte och budskap?</li>
						<li>Kommer det viktigaste först?</li>
						<li>Är det tydligt hur texten ska läsas?</li>
						<li>Tilltalas läsaren?</li>
						<li>Finns tydliga rubriker som vägleder läsaren?</li>
						<li>Behövs en ingress eller sammanfattning i början?</li>
						<li>Är layouten luftig?</li>
						<li>Är meningarna enkla och lagom långa?</li>
						<li>Kommer verben tidigt i meningarna?</li>
						<li>Är det tydligt vem det är som gör något?</li>
						<li>Är orden begripliga?</li>
						<li>Finns sambandsord som skapar den röda tråden?</li>
						<li>Finns det språkfel och stavfel?</li>
					</ol>
				</p>
				<p>
					Läs om de olika punkterna i{' '}
					<a
						href={getAssetPath('/assets/documents/lathund_klarsprak_v1.2.pdf')}
						download="lathund_klarsprak_v1-2"
					>
						Skolverkets lathund för att klarspråksgranska din text
					</a>
					.
				</p>
				<h2>Jobba mer med klarspråk</h2>
				<p>
					På Skolverkets intranät finns mer information om klarspråk och stöd i hur
					du kan jobba med det.
				</p>
				<a href="https://kanalen.skolverket.se/stod-i-arbetet/kommunikation/klarsprak/klarsprak-1.15174">
					Klarspråk, Skolverkets intranät
				</a>
			</ArticlePage>
		);
	}
}
