import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, h } from '@stencil/core';
import { CodeBlockLanguage, CodeLanguage } from 'libs/skolverket/package/src';

@Component({
	tag: 'page-jobba-med-kod',
	scoped: true
})
export class PageJobbaMedKod {
	render() {
		return (
			<ArticlePage
				head={<title>Jobba med kod</title>}
				heading="Jobba med kod"
				preamble="Som utvecklare på Skolverket så utgår vi från vårt gemensamma komponentbibliotek Digi Skolverket. Digi Skolverket är ett komponentbibliotek, design tokens, funktionalitet, api:er och annat som hjälper dig att bygga digitala tjänster som följer våra gemensamma riktlinjer."
			>
				<h2>Om kodpaketen</h2>
				<p>
					Koden är fördelad på ett par olika paket (ett grundpaket och ett anpassat
					för Angular), som distribueras med NPM. I och med att designsystemet är ett
					samarbete med Arbetsförmedlingen så sker distributionen över deras publika
					Nexus.
				</p>
				<h2>@digi/skolverket</h2>
				<p>
					Det här är grundbiblioteket med Skolverkets komponenter, css, design tokens
					och typsnitt. Komponenterna bygger på web componentsstandarden och kan
					därför användas oavsett teknikstack och ramverk.
				</p>
				<h3>Installera @digi/skolverket</h3>
				<p>
					Om du vill lägga till koden i Angular kan du hoppa direkt till
					@digi/skolverket-angular nedan.
				</p>
				<h4>1. Konfigurera NPM</h4>
				<p>
					Börja med att konfigurera NPM så att paketen hämtas från rätt källa. Lägg
					till en fil som heter{' '}
					<digi-code afLanguage={CodeLanguage.BASH} afCode={`.npmrc`} /> i samma mapp
					som <digi-code afLanguage={CodeLanguage.BASH} afCode={`package.json`} />,
					och lägg till följande innehåll:
				</p>
				<p>
					<digi-code-block
						afCode={`@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/

						`}
					/>
				</p>
				<h4>2. Installera paketet</h4>
				<p>
					<digi-code-block
						afLanguage={CodeBlockLanguage.BASH}
						afCode={`npm i @digi/skolverket`}
					/>
				</p>
				<h4>3. Lägg till javascript, typsnitt och css</h4>
				<p>
					Här länkar vi in javascript, css och fontdeklarationer. Sedan ska det gå
					att använda komponenterna i din HTML.
				</p>
				<p>
					<digi-code-block
						afCode={`<script
  type="module"
  src="node_modules/@digi/skolverket/dist/digi-skolverket/digi-skolverket.esm.js"
></script>
<script
  nomodule
  src="node_modules/@digi/skolverket/dist/digi-skolverket/digi-skolverket.js"
></script>
<link rel="stylesheet" href="node_modules/@digi/skolverket/dist/digi-skolverket/fonts/fonts.css" />
<link
  rel="stylesheet"
  href="node_modules/@digi/skolverket/dist/digi-skolverket/digi-skolverket.css"
/>
`}
					/>
				</p>
				<h2>@digi/skolverket-angular</h2>
				<p>
					Detta är en Angularifierad version av komponentbiblioteket. Det använder
					fortfarande web components, men har bindningar för reactive forms som borde
					göra det lite lättare att arbeta med.
				</p>
				<h3>Installera @digi/skolverket-angular</h3>
				<h4>1. Konfigurera NPM</h4>
				<p>
					Börja med att konfigurera NPM så att paketen hämtas från rätt källa. Lägg
					till en fil som heter{' '}
					<digi-code afLanguage={CodeLanguage.BASH} afCode={`.npmrc`} /> i samma mapp
					som <digi-code afLanguage={CodeLanguage.BASH} afCode={`package.json`} />,
					och lägg till följande innehåll:
				</p>
				<p>
					<digi-code-block
						afCode={`@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/

						`}
					/>
				</p>
				<h4>2. Installera paketen</h4>
				<p>
					<digi-code-block
						afLanguage={CodeBlockLanguage.BASH}
						afCode={`npm i @digi/skolverket-angular`}
					/>
				</p>
				<p>
					Notera att <digi-code afCode={`@digi/skolverket`} /> är en peer dependency
					till Reactbiblioteket och därför kommer med på köpet.
				</p>
				<h4>3. Lägg till modulen</h4>
				<p>
					<digi-code-block
						afCode={`import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { DigiSkolverketAngularModule } from "@digi/skolverket-angular";
import { AppComponent } from "./app.component";
						
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, DigiSkolverketAngularModule], // <--- Lägg till importen
  providers: [],
  bootstrap: [AppComponent],
})

export class AppModule {}`}
						afLanguage={CodeBlockLanguage.TYPESCRIPT}
					/>
				</p>
				<h4>4. Lägg till css</h4>
				<p>
					Både fontdeklarationer och annan css finns i{' '}
					<digi-code
						afCode={`@digi/skolverket`}
						afLanguage={CodeLanguage.JAVASCRIPT}
					/>
					:
				</p>
				<ul>
					<li>
						<digi-code
							afCode={`@digi/skolverket/dist/digi-skolverket/fonts/src/fonts.css`}
							afLanguage={CodeLanguage.BASH}
						/>
					</li>
					<li>
						<digi-code
							afCode={`@digi/skolverket/dist/digi-skolverket/digi-skolverket.css`}
							afLanguage={CodeLanguage.BASH}
						/>
					</li>
				</ul>
				<p>
					Om du använder sass så kan du lägga till dem som importer på följande vis
					(sökvägarna kan behöva ändras så att de blir relativa till din
					node_modules):
				</p>
				<p>
					<digi-code-block
						afCode={`@import "node_modules/@digi/skolverket/dist/digi-skolverket/fonts/src/fonts.css";
@import "node_modules/@digi/skolverket/dist/digi-skolverket/digi-skolverket.css";


`}
						afLanguage={CodeBlockLanguage.SCSS}
					/>
				</p>
				<p>
					Eller i{' '}
					<digi-code afCode={`angular.json`} afLanguage={CodeLanguage.BASH} /> såhär:
				</p>
				<p>
					<digi-code-block
						afLanguage={CodeBlockLanguage.JSON}
						afCode={`{
...
"styles": [
  "@digi/skolverket/dist/digi-skolverket/fonts/src/fonts.css",
  "@digi/skolverket/dist/digi-skolverket/digi-skolverket.css"
],
...
}
`}
					/>
				</p>
				<h2>@digi/skolverket-react</h2>
				<p>
					Detta är en Reactifierad version av komponentbiblioteket. Det använder
					fortfarande web components, men är skapat för att lättare integrera i
					Reactmiljöer.
				</p>
				<h3>Installera @digi/skolverket-react</h3>
				<h4>1. Konfigurera NPM</h4>
				<p>
					Börja med att konfigurera NPM så att paketen hämtas från rätt källa. Lägg
					till en fil som heter{' '}
					<digi-code afLanguage={CodeLanguage.BASH} afCode={`.npmrc`} /> i samma mapp
					som <digi-code afLanguage={CodeLanguage.BASH} afCode={`package.json`} />,
					och lägg till följande innehåll:
				</p>
				<p>
					<digi-code-block
						afCode={`@digi:registry=https://nexus.jobtechdev.se/repository/arbetsformedlingen-npm/

						`}
					/>
				</p>
				<h4>2. Installera paketet</h4>
				<p>
					<digi-code-block
						afLanguage={CodeBlockLanguage.BASH}
						afCode={`npm i @digi/skolverket-react`}
					/>
				</p>
				<p>
					Notera att <digi-code afCode={`@digi/skolverket`} /> är en peer dependency
					till Reactbiblioteket och därför kommer med på köpet.
				</p>
				<h4>3. Använd komponenter</h4>
				<p>
					<digi-code-block
						afCode={`import { DigiButton } from "@digi/skolverket-react";
import type { ButtonVariation } from "@digi/skolverket";
						
export const App = () => {
	const [variation, setVariation] = useState(ButtonVariation.PRIMARY);
	return (
		<DigiButton afVariation={variation} onAfOnClick={() => setVariation(ButtonVariation.SECONDARY)}>
			Ändra mig!
		</DigiButton>
	);
}`}
						afLanguage={CodeBlockLanguage.TYPESCRIPT}
					/>
				</p>
				<h4>4. Lägg till css</h4>
				<p>
					Både fontdeklarationer och annan css finns i{' '}
					<digi-code
						afCode={`@digi/skolverket`}
						afLanguage={CodeLanguage.JAVASCRIPT}
					/>
					:
				</p>
				<ul>
					<li>
						<digi-code
							afCode={`@digi/skolverket/dist/digi-skolverket/fonts/src/fonts.css`}
							afLanguage={CodeLanguage.BASH}
						/>
					</li>
					<li>
						<digi-code
							afCode={`@digi/skolverket/dist/digi-skolverket/digi-skolverket.css`}
							afLanguage={CodeLanguage.BASH}
						/>
					</li>
				</ul>
				<p>
					Om du använder sass så kan du lägga till dem som importer på följande vis
					(sökvägarna kan behöva ändras så att de blir relativa till din
					node_modules):
				</p>
				<p>
					<digi-code-block
						afCode={`@import "node_modules/@digi/skolverket/dist/digi-skolverket/fonts/src/fonts.css";
@import "node_modules/@digi/skolverket/dist/digi-skolverket/digi-skolverket.css";


`}
						afLanguage={CodeBlockLanguage.SCSS}
					/>
				</p>
			</ArticlePage>
		);
	}
}
