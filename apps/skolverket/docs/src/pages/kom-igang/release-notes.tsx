import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, h } from '@stencil/core';
import { ExpandableAccordionVariation } from '@digi/skolverket';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'page-release-notes',
	scoped: true
})
export class PageReleaseNotes {
	render() {
		return (
			<ArticlePage
				head={<title>Release notes</title>}
				heading="Release notes"
				preamble="Här hittar du information om de senaste uppdateringarna i designsystemet."
			>
				<digi-layout-rows>
					<digi-expandable-accordion
						afVariation={ExpandableAccordionVariation.SECONDARY}
						afHeading={'[17.5.1] - 2023-03-23'}
						afExpanded={true}
					>
						<h3>Nytt</h3>
						<h4>@digi/skolverket-react</h4>
						<ul>
							<li>
								Skapat Reactbibliotek för komponenterna. Läs mer om hur du använder
								designsystemet i React på{' '}
								<a {...href('/kom-igang/jobba-med-kod')}>kom igång-sidan</a>.
							</li>
						</ul>
						<h3>Ändrat</h3>
						<h4>@digi/skolverket</h4>
						<ul>
							<li>
								<digi-code afCode={`digi-loader-spinner`} />
								<ul>
									<li>
										Åtgärdat bugg som uppstod vid behov av laddningsindikatorer i{' '}
										<digi-code afCode={`digi-navigation-tabs`} />
									</li>
								</ul>
							</li>
						</ul>
						<h4>@digi/skolverket-angular</h4>
						<ul>
							<li>
								<digi-code afCode={`digi-link`} />
								<ul>
									<li>
										För bättre stöd med routerlinks i Angular så har vi lagt in stöd för
										att kunna använda komponenten som en container för ett vanligt
										länkelement. Detta behövs för att kunna stödja tangentbordsnavigation
										på korrekt vis.
									</li>
								</ul>
							</li>
						</ul>
					</digi-expandable-accordion>
					<digi-expandable-accordion
						afVariation={ExpandableAccordionVariation.SECONDARY}
						afHeading={'[17.5.0] - 2023-03-13'}
					>
						<h3>Ändrat</h3>
						<h4>@digi/skolverket</h4>
						<ul>
							<li>Uppdaterat till Stencil 3</li>
							<li>
								<digi-code afCode={`digi-page-header`} />
								<ul>
									<li>
										Åtgärdat bugg där toppdelen syntes trots att den inte hade något
										innehåll.
									</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-form-checkbox`} />,{' '}
								<digi-code afCode={`digi-form-radiobutton`} />
								<ul>
									<li>Åtgärdat layoutbuggar.</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-form-radiogroup`} />
								<ul>
									<li>Återgärdat bugg där komponenten triggade changeventet på init.</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-form-fieldset`} />
								<ul>
									<li>Justerat padding.</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-form-input`} />
								<ul>
									<li>
										Lagt till nytt attribut för inputmode:{' '}
										<digi-code afCode={`af-inputmode`} />.
									</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-typography`} />
								<ul>
									<li>
										Ändrat standardstorleken på länkar till 1em, det vill säga samma
										storlek som texten den befinner sig i.
									</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-link`} />
								<ul>
									<li>Åtgärdat bugg där det ej gick att ändra textstorlek på länken.</li>
								</ul>
							</li>
						</ul>
					</digi-expandable-accordion>
					<digi-expandable-accordion
						afVariation={ExpandableAccordionVariation.SECONDARY}
						afHeading={'[17.4.3] - 2023-02-21'}
					>
						<h3>Ändrat</h3>
						<h4>@digi/skolverket</h4>
						<ul>
							<li>
								<digi-code afCode={`digi-navigation-main-menu`} />,{' '}
								<digi-code afCode={`digi-navigation-main-menu-panel`} />
								<ul>
									<li>Tagit bort felaktig vertikal yttre padding på små skärmar.</li>
									<li>Åtgärdat bugg där panelens innehåll kunde överflöda panelen.</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-form-checkbox`} />
								<ul>
									<li>Åtgärdat layoutbuggar.</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-typography`} />
								<ul>
									<li>
										Ändrat standardstorleken på länkar till 1em, det vill säga samma
										storlek som texten den befinner sig i.
									</li>
								</ul>
							</li>
						</ul>
					</digi-expandable-accordion>
					<digi-expandable-accordion
						afVariation={ExpandableAccordionVariation.SECONDARY}
						afHeading={'[17.4.2] - 2023-02-20'}
					>
						<h3>Ändrat</h3>
						<h4>@digi/skolverket</h4>
						<ul>
							<li>
								<digi-code afCode={`digi-navigation-main-menu`} />,{' '}
								<digi-code afCode={`digi-navigation-main-menu-panel`} />
								<ul>
									<li>
										Åtgärdat bugg där länkar i menyn fick samma färg som bakgrunden om de
										var <digi-code afCode={`:visited`} />.
									</li>
									<li>
										Rehydrering av slottad html vid dynamisk uppdatering av aktiv sida
										(med hjälp av 'aria-current="page"'). Dvs, nu uppdateras attribut och
										annat som behövs för att markera vilken sida som är aktiv vid dynamisk
										routing.
									</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-form-checkbox`} />
								<ul>
									<li>Åtgärdat layoutbuggar.</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-typography`} />
								<ul>
									<li>
										Ändrat standardstorleken på länkar till 1em, det vill säga samma
										storlek som texten den befinner sig i.
									</li>
								</ul>
							</li>
						</ul>
					</digi-expandable-accordion>
					<digi-expandable-accordion
						afVariation={ExpandableAccordionVariation.SECONDARY}
						afHeading={'[17.4.1] - 2023-02-20'}
					>
						<h3>Ändrat</h3>
						<h4>@digi/skolverket</h4>
						<ul>
							<li>
								Lagt till <digi-code afCode={`d3`} /> som peer dependency.
							</li>
						</ul>
					</digi-expandable-accordion>
					<digi-expandable-accordion
						afVariation={ExpandableAccordionVariation.SECONDARY}
						afHeading={'[17.4.0] - 2023-02-17'}
					>
						<h3>Ändrat</h3>
						<h4>@digi/skolverket</h4>
						<ul>
							<li>
								<digi-code afCode={`digi-form-file-upload`} />
								<ul>
									<li>Lagt till stöd för att ladda upp flera filer på en gång.</li>
									<li>Små visuella justeringar.</li>
								</ul>
							</li>
							<li>
								<digi-code afCode={`digi-icon`} />
								<ul>
									<li>
										Ändrat ikon för <digi-code afCode={`validation-error`} />.
									</li>
								</ul>
							</li>
						</ul>
					</digi-expandable-accordion>
				</digi-layout-rows>
			</ArticlePage>
		);
	}
}
