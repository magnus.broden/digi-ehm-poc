import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-tillganglighetslista',
	scoped: true
})
export class PageTillganglighetslista {
	render() {
		return (
			<article>
				<Helmet>
					<title>Tillgänglighetslista</title>
				</Helmet>
				<digi-docs-wcag-list />
			</article>
		);
	}
}
