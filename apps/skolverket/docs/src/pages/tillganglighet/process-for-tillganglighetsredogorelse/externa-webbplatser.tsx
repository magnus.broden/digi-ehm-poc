import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-externa-webbplatser',
	scoped: true
})
export class PageExternaWebbplatser {
	render() {
		return (
			<article>
				<Helmet>
					<title>Externa webbplatser</title>
				</Helmet>
				<h1>Externa webbplatser</h1>
				<p>Så här gör man med externa webbplatser</p>
			</article>
		);
	}
}
