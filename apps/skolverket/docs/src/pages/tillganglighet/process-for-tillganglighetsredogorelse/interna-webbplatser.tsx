import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-interna-webbplatser',
	scoped: true
})
export class PageInternaWebbplatser {
	render() {
		return (
			<article>
				<Helmet>
					<title>Interna webbplatser</title>
				</Helmet>
				<h1>Interna webbplatser</h1>
				<p>Så här gör man med interna webbplatser</p>
			</article>
		);
	}
}
