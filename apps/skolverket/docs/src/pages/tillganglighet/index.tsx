import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, h } from '@stencil/core';

@Component({
	tag: 'page-tillganglighet',
	scoped: true
})
export class PageTillganglighet {
	render() {
		return (
			<ArticlePage
				head={<title>Om digital tillgänglighet</title>}
				heading="Om digital tillgänglighet"
				preamble="Digital tillgänglighet betyder att Skolverkets digitala tjänster ska vara tillgängliga för alla samt vara lätta att använda. Det spelar ingen roll om våra användare har en dator eller mobil, om användaren har någon funktionsnedsättning, om användaren är ny i Sverige eller om användaren upplever att “datorer är allmänt knepigt”."
			>
				<p>
					Digital tillgänglighet innebär dessutom att alla som jobbar hos oss ska ha
					en digital arbetsmiljö där det är möjligt att utföra sitt arbete smidigt
					och effektivt. Målet är att Skolverket ska vara en inkluderande
					arbetsgivare. Följer vi kraven på digital tillgänglighet kan vi dessutom
					rekrytera fler medarbetare med den kompetens Skolverket behöver.
				</p>
				<h2>Varför?</h2>
				<p>
					Vi vill att våra kunder ska kunna använda våra digitala kanaler i första
					hand. God användbarhet leder till att såväl kunder som medarbetare oftare
					gör rätt när de ska använda våra digitala tjänster. På så sätt skapar vi
					även enhetlighet och förtroende för våra digitala tjänster genom att vara
					digitalt tillgängliga.
				</p>
				<h2>Hur?</h2>
				<p>
					Vi vill göra det lätt för dig som arbetar med våra digitala tjänster och
					vår digitala information. I designsystemet har vi därför samlat alla
					resurser du behöver för att kunna ta fram digitala tjänster som följer
					aktuella riktlinjer och lagkrav gällande digital tillgänglighet. Vi
					säkerställer tillgängligheten i våra tjänster genom research kring
					användbarhet, användarcentrerad design, utveckling med tillgängliga
					komponenter men även genom att genomföra användningstester med användare
					som har varierande förutsättningar.
				</p>
			</ArticlePage>
		);
	}
}
