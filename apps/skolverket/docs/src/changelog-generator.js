'use strict';
(function () {
	const { marked } = require('marked');
	const { parse } = require('node-html-parser');
	const fs = require('fs');
	const path = require('path');

	console.log('\nExporting changelog');
	const data = fs.readFileSync(
		path.resolve(__dirname, '../../../../CHANGELOG.md'),
		{ encoding: 'utf8' }
	);
	let root = marked(data);
	root = parse(root);
	root.getElementsByTagName('code').forEach((el) => {
		const newCode = `<digi-code af-code="${el.text}"></digi-code>`;
		el.replaceWith(newCode);
	});
	root.getElementsByTagName('breaking').forEach((el) => {
		const newCode = `<span class="breaking-tag">Breaking</span>`;
		el.replaceWith(newCode);
	});
	root.getElementsByTagName('pre').forEach((el) => {
		const newCode = el.text.replace('<code>', '').replace('</code>', '').trim();
		el.replaceWith(`<digi-code-block af-code="${newCode}"></digi-code-block>`);
	});
	const headings = root.getElementsByTagName('h2');
	const contentBetweenHeadings = root
		.toString()
		.match(/(?<=<\/h2>\s)\S.*?(?=\s*<h2|$)/gs);
	let stringRoot = '';

	for (let i = 0; i < headings.length; i++) {
		stringRoot += `<digi-expandable-accordion af-heading="${headings[i].text}"\
    ${i < 3 ? ' af-expanded="true"' : ''}>\n${
			contentBetweenHeadings[i]
		}\n</digi-expandable-accordion af-heading>\n`;
	}

	const file = fs.createWriteStream(__dirname + '/data/changelogData.ts');
	file.on('error', function (err) {
		console.error(err);
	});
	file.write('export const changelogData = `' + stringRoot + '`');
	file.end();
	console.log(
		'CHANGELOG.md successfully exported to apps/docs/src/data/changelog.ts\n'
	);
})();
