import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, FormRadiobuttonLayout } from '@digi/skolverket';
import { router } from '@digi/skolverket-docs/global/routes/router';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';

@Component({
	tag: 'digi-form-radiobutton-details',
	scoped: true
})
export class DigiFormRadiobuttontDetails {
	Router = router;

	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formRadiobuttonLayout: FormRadiobuttonLayout = FormRadiobuttonLayout.BLOCK;
	@State() validation: boolean = false;

	get radiobuttonCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
    <digi-form-radiobutton 
      af-label="Kryssruta"
      af-layout="${this.formRadiobuttonLayout}"\
      ${this.validation ? `\n \t  af-validation="error"` : ''}\n\t>
    </digi-form-radiobutton>`,
			[CodeExampleLanguage.ANGULAR]: `\
    <digi-form-radiobutton
      [attr.af-label]="'Kryssruta'"
      [attr.af-layout]="FormRadiobuttonLayout.${Object.keys(FormRadiobuttonLayout).find((key) => FormRadiobuttonLayout[key] === this.formRadiobuttonLayout)}"\
      ${this.validation ? `\n\t [attr.af-validation]="FormCheckboxValidation.ERROR"` : ''}\n\t>
    </digi-form-radiobutton>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.radiobuttonCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Layout"
							afLegend="Layout"
							onChange={(e) => (this.formRadiobuttonLayout = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Layout"
								afLabel="Block"
								afValue={FormRadiobuttonLayout.BLOCK}
								afChecked={this.formRadiobuttonLayout === FormRadiobuttonLayout.BLOCK}
							/>
							<digi-form-radiobutton
								afName="Layout"
								afLabel="Inline"
								afValue={FormRadiobuttonLayout.INLINE}
								afChecked={this.formRadiobuttonLayout === FormRadiobuttonLayout.INLINE}
							/>
						</digi-form-fieldset>

						<digi-form-fieldset afLegend="Validering">
							<digi-form-checkbox
								afLabel="Error"
								afChecked={this.validation}
								onAfOnChange={() => (this.validation = !this.validation)}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}

				<digi-form-radiobutton
					afLabel="Kryssruta"
					afLayout={this.formRadiobuttonLayout}
					af-validation={this.validation ? 'error' : ''}
					afChecked={!this.validation}
				/>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Används för att välja ett enda alternativ av flera fördefinierade val."
				example={this.example()}
				description={
					<Fragment>
						<h3>Layouter</h3>
						<p>
							Radioknappen finns i två layouter, block och inline. Block är den som bör
							användas i första hand och innebär att komponenten tar upp en hel rad där
							hela är klickbar.
						</p>
						<h3>Validering</h3>
						<p>
							Kryssrutan kan ha två olika valideringsstatus: Felaktig eller ingen alls.
						</p>
						<h3>ControlValueAccessor</h3>
						<p>
							För att kunna knyta radioknappar mot{' '}
							<digi-code afCode="ControlValueAccessor"></digi-code> i Angular så
							behöver du använda denna komponent tillsammans med{' '}
							<ComponentLink
								tag="digi-form-radiogroup"
								text="radiogruppskomponenten"
							/>
						</p>
					</Fragment>
				}
			/>
		);
	}
}
