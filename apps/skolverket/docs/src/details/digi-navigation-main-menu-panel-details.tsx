import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-navigation-main-menu-panel-details',
	scoped: true
})
export class DigiNavigationMainMenuPanelDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() totalPages: number = 6;
	@State() showResultName: boolean = false;

	get navigationMainMenuPanelCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-main-menu-panel>
  <a href="#" slot="main-link">Huvudingång 1</a>
  <ul>
    <li>
      <a href="#">Ingång 1 nivå 2.1</a>
    </li>
    <li>
      <a href="#">Ingång 1 nivå 2.2</a>
      <ul>
        <li>
          <a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
          </li>
          <li>
            <a href="#">Ingång 1 nivå 2.2 nivå 3.2</a>
            <ul>
              <li>
                <a href="#">Ingång 1 nivå 3.2 nivå 4.1</a>
              </li>
              <li>
                <a href="#" aria-current="page">
                  Ingång 1 nivå 3.2 nivå 4.2
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li>
        <a href="#">Ingång 1 nivå 2.3</a>
      </li>
    </ul>
 </digi-navigation-main-menu-panel>
          `,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-main-menu-panel>
  <a href="#" slot="main-link">Huvudingång 1</a>
  <ul>
    <li>
      <a href="#">Ingång 1 nivå 2.1</a>
    </li>
    <li>
      <a href="#">Ingång 1 nivå 2.2</a>
      <ul>
        <li>
          <a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
          </li>
          <li>
            <a href="#">Ingång 1 nivå 2.2 nivå 3.2</a>
            <ul>
              <li>
                <a href="#">Ingång 1 nivå 3.2 nivå 4.1</a>
              </li>
              <li>
                <a href="#" aria-current="page">
                  Ingång 1 nivå 3.2 nivå 4.2
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li>
        <a href="#">Ingång 1 nivå 2.3</a>
      </li>
    </ul>
 </digi-navigation-main-menu-panel>
          `
		};
	}

	example() {
		return (
			<Fragment>
				<digi-code-example
					af-code={JSON.stringify(this.navigationMainMenuPanelCode)}
					af-controls-position="end"
					af-hide-controls={this.afHideControls ? 'true' : 'false'}
					af-hide-code={this.afHideCode ? 'true' : 'false'}
				>
					<digi-navigation-main-menu-panel
						style={{
							display: 'block',
							background: 'var(--digi--color--background--secondary)'
						}}
					>
						<a href="#" slot="main-link">
							Huvudingång 1
						</a>
						<ul>
							<li>
								<a href="#">Ingång 1 nivå 2.1</a>
							</li>
							<li>
								<a href="#">Ingång 1 nivå 2.2</a>
								<ul>
									<li>
										<a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
									</li>
									<li>
										<a href="#">Ingång 1 nivå 2.2 nivå 3.2</a>
										<ul>
											<li>
												<a href="#">Ingång 1 nivå 3.2 nivå 4.1</a>
											</li>
											<li>
												<a href="#" aria-current="page">
													Ingång 1 nivå 3.2 nivå 4.2
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Ingång 1 nivå 2.3</a>
							</li>
						</ul>
					</digi-navigation-main-menu-panel>
				</digi-code-example>
			</Fragment>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Huvudmenyspanelen används för undermenyer i huvudmenyn."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Huvudmenyspanelen ska alltid användas i{' '}
							<ComponentLink tag="digi-navigation-main-menu" text="huvudmenyn" />. Se
							där för vidare information.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
