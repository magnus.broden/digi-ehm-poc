import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'digi-logo-details',
	scoped: true
})
export class DigiLogoDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasVariant: boolean = true;
	@State() hasTitle: boolean = false;

	get logoCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-logo></digi-logo>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-logo></digi-logo>`
		};
	}

	@State() showSystemName: boolean;

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.logoCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{<digi-logo></digi-logo>}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Det här är Skolverkets huvudlogotyp."
				example={this.example()}
				description={
					<Fragment>
						<h3>När ska logotypen användas?</h3>
						<p>
							Huvudlogotypen används i sidhuvudet på skolverket.se, och i sidfoten
							överallt. Här finns{' '}
							<ComponentLink
								tag="digi-logo-sister"
								text="logotypen för syskonwebbplatser"
							/>{' '}
							och här finns{' '}
							<ComponentLink
								tag="digi-logo-service"
								text="logotypen för tjänster och verktyg"
							/>
							.
						</p>
						<p>
							För vidare riktlinjer se{' '}
							<a {...href('/grafisk-profil/logotyp')}>
								vår grafiska profil om logotyper
							</a>
						</p>
					</Fragment>
				}
			/>
		);
	}
}
