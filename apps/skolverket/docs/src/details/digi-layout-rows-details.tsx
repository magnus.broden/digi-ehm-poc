import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { LayoutComponentGuidelines } from '../components/LayoutComponentGuidelines';

@Component({
	tag: 'digi-layout-rows-details',
	scoped: true,
	styles: `
		digi-layout-rows div {
			background: var(--digi--color--background--tertiary);
			height: 100px;
			display: flex;
			align-items: center;
			justify-content: center;
		}
	`
})
export class DigiLayoutRowsDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get layoutRowsCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-layout-rows>
  <div>1</div>
  <div>2</div>
  <div>3</div>
  <div>4</div>
  <div>5</div>
</digi-layout-rows>
				`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-layout-rows>
  <div>1</div>
  <div>2</div>
  <div>3</div>
  <div>4</div>
  <div>5</div>
</digi-layout-rows>
				`
		};
	}

	example() {
		return (
			<digi-layout-rows>
				<digi-code-example
					af-code={JSON.stringify(this.layoutRowsCode)}
					af-hide-controls={this.afHideControls ? 'true' : 'false'}
					af-hide-code={this.afHideCode ? 'true' : 'false'}
				>
					<digi-layout-rows>
						<div>1</div>
						<div>2</div>
						<div>3</div>
						<div>4</div>
						<div>5</div>
					</digi-layout-rows>
				</digi-code-example>
			</digi-layout-rows>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="Standardkomponent för jämna rader."
				description={
					<Fragment>
						<p>
							Rader är en standardkomponent för att skapa jämna rader med en responsiv
							marginal mellan. Mycket praktisk för att placera ut sidinnehåll och annat
							som flödar vertikalt. Komponenten följer samma regler för marginaler som
							basgriden och övriga layoutkomponenter gör, så i kombination kan de skapa
							avancerade layouter som ändå följer rätt designmönster.
						</p>
					</Fragment>
				}
				guidelines={<LayoutComponentGuidelines />}
			/>
		);
	}
}
