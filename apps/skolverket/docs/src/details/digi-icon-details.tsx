import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeBlockLanguage, CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import {
	IconName,
	iconNames
} from 'libs/skolverket/package/src/global/icon/icon';

@Component({
	tag: 'digi-icon-details',
	scoped: true,
	styles: `
		.icon-list {
			--digi--icon--width: 32px; 
			--digi--icon--height: 32px; 
			display: flex; 
			gap: var(--digi--responsive-grid-gutter);
			flex-direction: column;
			flex-wrap: wrap;
			margin: 0;
			padding: 0;
			list-style: none; 
		}
	`
})
export class DigiIconDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() iconName: IconName = iconNames[0] as IconName;
	@State() hasDescription: boolean = false;

	get textareaCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-icon af-name="${this.iconName}"></digi-icon>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-icon af-name="${this.iconName}"></digi-icon>`
		};
	}

	example() {
		return (
			<div>
				<digi-code-example
					af-code={JSON.stringify(this.textareaCode)}
					af-hide-controls={this.afHideControls ? 'true' : 'false'}
					af-hide-code={this.afHideCode ? 'true' : 'false'}
				>
					<digi-icon afName={this.iconName}></digi-icon>

					{!this.afHideControls && (
						<div class="slot__controls" slot="controls">
							<digi-form-select
								afLabel="Namn"
								onAfOnChange={(e) => (this.iconName = (e.target as any).value)}
								af-variation="small"
								af-value="medium"
							>
								{iconNames.map((name) => (
									<option value={name}>{name}</option>
								))}
							</digi-form-select>
						</div>
					)}
				</digi-code-example>
			</div>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Textarean används för att låta användare ange en mängd text som är
			längre än en rad."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Välj vilken ikon du vill använda genom att skicka in en prop med namnet{' '}
							<digi-code af-code="afName" />. Testa att byta i exemplet ovan, eller{' '}
							<a href="#alla-ikoner">
								se listan med alla tillgängliga ikoner här nedan.
							</a>
						</p>
						<h3>Storlekar</h3>
						<p>
							Ikoner ska användas i storlekarna 24px, 32px eller 48px. Storlekar kan
							sättas genom att ändra ett par av komponentens css-variabler
						</p>
						<p>
							<digi-code-block
								afCode={`
--digi--icon--width: 2rem; // <-- 2rem motsvarar 32px
--digi--icon--height: 2rem;
							`}
								afLanguage={CodeBlockLanguage.CSS}
							/>
						</p>
						<h3>Färger</h3>
						<p>
							Komponenten får samma färg som dess föräldrar, så genom att byta färg på
							omslutande element så ändrar den sig också.
						</p>
						<p>
							<p>
								<digi-code-block
									afCode={`
<p>
  <digi-icon af-name=${iconNames[0]}></digi-icon>
</p>
<style>
  /* Det här ärvs av ikonen */
  p {
    color: var(--digi--color--text--secondary);
  }
  
  /* Det här fungerar också */
  digi-icon {
    color: var(--digi--color--text--secondary);
  }
</style>
							`}
									afLanguage={CodeBlockLanguage.HTML}
								/>
							</p>
						</p>
						<h3 id="alla-ikoner">Alla ikoner</h3>
						<ul class="icon-list">
							{iconNames.map((name) => (
								<li>
									<digi-layout-media-object>
										<digi-icon slot="media" afName={name} />
										<p>{name}</p>
									</digi-layout-media-object>
								</li>
							))}
						</ul>
					</Fragment>
				}
			/>
		);
	}
}
