import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	ListLinkLayout,
	ListLinkType,
	ListLinkVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-list-link-details',
	scoped: true
})
export class DigiListLinkDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() variation: ListLinkVariation = ListLinkVariation.REGULAR;
	@State() type: ListLinkType = ListLinkType.UNORDERED;
	@State() layout: ListLinkLayout = ListLinkLayout.BLOCK;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-list-link
  af-variation="${this.variation}"
  af-layout="${this.layout}"
  af-type="${this.type}"
>
  <h3 slot="heading">En rubrik</h3>
  <li><a href="#">Om webbplatsen</a></li>
  <li><a href="#">Vår verksamhet</a></li>
  <li><a href="#">Organisation</a></li>
  <li><a href="#">Jobba hos oss</a></li>
</digi-list-link>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-list-link
  [attr.af-variation]="ListLinkVariation.${Object.keys(ListLinkVariation).find(
			(key) => ListLinkVariation[key] === this.variation
		)}"
  [attr.af-layout]="ListLinkLayout.${Object.keys(ListLinkLayout).find(
			(key) => ListLinkLayout[key] === this.layout
		)}"
  [attr.af-type]="ListLinkType.${Object.keys(ListLinkType).find(
			(key) => ListLinkType[key] === this.type
		)}"
>
  <h3 slot="heading">En rubrik</h3>
  <li><a href="#">Om webbplatsen</a></li>
  <li><a href="#">Vår verksamhet</a></li>
  <li><a href="#">Organisation</a></li>
  <li><a href="#">Jobba hos oss</a></li>
</digi-list-link>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variation"
							onChange={(e) => (this.variation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Standard"
								afValue={ListLinkVariation.REGULAR}
								afChecked={this.variation === ListLinkVariation.REGULAR}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Kompakt"
								afValue={ListLinkVariation.COMPACT}
								afChecked={this.variation === ListLinkVariation.COMPACT}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset
							afName="Typ"
							afLegend="Typ"
							onChange={(e) => (this.type = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Typ"
								afLabel="UL"
								afValue={ListLinkType.UNORDERED}
								afChecked={this.type === ListLinkType.UNORDERED}
							/>
							<digi-form-radiobutton
								afName="Typ"
								afLabel="OL"
								afValue={ListLinkType.ORDERED}
								afChecked={this.type === ListLinkType.ORDERED}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset
							afName="Layout"
							afLegend="Layout"
							onChange={(e) => (this.layout = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Layout"
								afLabel="Block"
								afValue={ListLinkLayout.BLOCK}
								afChecked={this.layout === ListLinkLayout.BLOCK}
							/>
							<digi-form-radiobutton
								afName="Layout"
								afLabel="Inline"
								afValue={ListLinkLayout.INLINE}
								afChecked={this.layout === ListLinkLayout.INLINE}
							/>
						</digi-form-fieldset>
					</div>
				)}
				<digi-list-link
					af-variation={this.variation}
					af-layout={this.layout}
					af-type={this.type}
				>
					<h3 slot="heading">En rubrik</h3>
					<li>
						<a href="#">Om webbplatsen</a>
					</li>
					<li>
						<a href="#">Vår verksamhet</a>
					</li>
					<li>
						<a href="#">Organisation</a>
					</li>
					<li>
						<a href="#">Jobba hos oss</a>
					</li>
				</digi-list-link>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Listor med länkar."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Den kompakta versionen är främst lämplig att använda i sidfoten. Annars
							använder vi oss av den vanliga versionen i så stor utsträckning som
							möjligt.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
