import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormCheckboxVariation,
	LinkVariation,
	NotificationDetailVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-link-details',
	scoped: true
})
export class DigiLinkDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasIcon: boolean = false;
	@State() linkVariation: LinkVariation = LinkVariation.SMALL;

	get linkCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-link
  af-href="#"
  af-target="_blank"
  af-variation="${Object.keys(LinkVariation)
			.find((key) => LinkVariation[key] === this.linkVariation)
			.toLocaleLowerCase()}"
> ${
				this.hasIcon ? '\n <digi-icon af-name="copy" slot="icon"></digi-icon>' : ' '
			}\n  Jag är en länk
</digi-link>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-link
  [attr.af-link-container]="true"
  [attr.af-variation]="LinkVariation.${Object.keys(LinkVariation).find((key) => LinkVariation[key] === this.linkVariation)}"
>
  <a href="/" [routerLink]="/">
    ${
				this.hasIcon ? '\n <digi-icon af-name="copy" slot="icon"></digi-icon>' : ' '
			}\n  Jag är en länk
  </a>
</digi-link>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.linkCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Storlek"
							afLegend="Storlek"
							onChange={(e) => (this.linkVariation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Storlek"
								afLabel="Liten"
								afValue={LinkVariation.SMALL}
								afChecked={this.linkVariation === LinkVariation.SMALL}
							/>
							<digi-form-radiobutton
								afName="Storlek"
								afLabel="Stor"
								afValue={LinkVariation.LARGE}
								afChecked={this.linkVariation === LinkVariation.LARGE}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset>
							<digi-form-checkbox
								afLabel="Icon"
								afVariation={FormCheckboxVariation.PRIMARY}
								afChecked={this.hasIcon}
								onAfOnChange={() =>
									this.hasIcon ? (this.hasIcon = false) : (this.hasIcon = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}

				{this.hasIcon && (
					<digi-link afHref="#" af-target="_blank" af-variation={this.linkVariation}>
						<digi-icon afName={`copy`} slot="icon"></digi-icon>
						Jag är en länk
					</digi-link>
				)}
				{!this.hasIcon && (
					<digi-link afHref="#" af-target="_blank" af-variation={this.linkVariation}>
						Jag är en länk
					</digi-link>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Länkens syfte är att förflytta användaren ifrån aktuell sida till en
			annan sida. En länk kan även användas för att förflytta användaren till
			en annan plats på samma sida. Länkar skall aldrig användas för att
			genomföra en åtgärd. Vid dessa situationer skall istället en knapp
			användas."
				example={this.example()}
				description={
					<Fragment>
						<h3>Varianter</h3>
						<p>
							Länk-komponenten finns i två storlekar, varav den lilla är standard.
							Använd <digi-code af-code="af-variation"></digi-code> för att välja
							vilken du vill ha, t.ex.{' '}
							<digi-code af-code="af-variation='large'"></digi-code>.
						</p>
						<h3>Med ikon</h3>
						<p>
							Du kan använda digi-link med en ikon genom att använda{' '}
							<digi-code af-code="slot='icon'"></digi-code> i ikon-taggen.
						</p>
						<digi-notification-detail
							afVariation={NotificationDetailVariation.WARNING}
						>
							<h3 slot="heading">Om Angular och routerLink</h3>
							Ska du använda exempelvis routerLink i Angular så behöver du sätta
							attributet afOverrideLink till true.
						</digi-notification-detail>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Tillgängliget</h3>
						<ul>
							<li>
								En länktext ska vara tydlig så att användaren förstår vart en länk leder
								även om länken lyfts ur sitt sammanhang. Undvik länkar som endast heter
								"Tillbaka" eller "Läs mer".
							</li>
							<li>Skriv länktexten så kort som möjligt.</li>
							<li>
								Undvik fraser som "Gå till...", "Till sidan..." i länken, då det är
								självklart att länken leder till en annan del av webbplatsen.
							</li>
							<li>
								Navigering på webbplatsen och inom tjänsterna sker primärt via
								brödsmulor. Tillbakalänkar används endast i undantagsfall.
							</li>
							<li>
								Alla ikoner som utgör en länk och som definieras inom &lt;i&gt; skall ha
								attributet <digi-code af-code='aria-hidden="true"'></digi-code>.
							</li>
							<li>
								Behöver du förtydliga länkens syfte eller mål så kan attributet{' '}
								<digi-code af-code="aria-label"></digi-code> användas. Det är svårt att
								säga när attributet behövs, men om flera länkar i t.ex. ett sökresultat
								är identiska så kan aria-label användas.
								<br />
								<br />
								Kom ihåg att attributet aria-label:
								<ul>
									<li>endast används av skärmläsare och andra liknande verktyg.</li>
									<li>skriver över länktexten för användare av dessa verktyg.</li>
								</ul>
							</li>
              <li>
                För att kunna använda Angulars <digi-code af-code="routerLink"></digi-code> med vår komponent behöver man använd attributet <digi-code af-code="af-container-link"></digi-code> och istället använda komponenten som en hållare av ett vanligt länkelement.<br /><br />
                Använder man Angular RouterLink direkt på vårt element så får man tillgänglighetsproblem då Angular lägger till tabindex i koden som gör att man får fokus på fel element när man navigerar med tangentbord.
                Använder man @digi/arbetsformedlingen-angular i tidigare version än 19.0.0 så rekommenderar vi att använda <digi-code af-code="af-override-link"></digi-code> och istället sköta routing programmatisk.
              </li>
						</ul>
					</digi-notification-detail>
				}
			/>
		);
	}
}
