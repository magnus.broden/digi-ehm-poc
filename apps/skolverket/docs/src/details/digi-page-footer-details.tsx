import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

const variation = (angular = false) =>
	angular
		? `[attr.af-variation]="ListLinkVariation.COMPACT"`
		: `af-variation="compact"`;

const layout = (angular = false) =>
	angular ? `[attr.af-layout]="ListLinkLayout.INLINE"` : `af-layout="inline"`;

const pageFooterExampleMarkup = (
	angular = false
) => /*html*/ `<digi-list-link ${variation(angular)} slot="top-first">
      <h3 slot="heading">Kontakt</h3>
      <li><a href="#">Alla kontaktuppgifter</a></li>
      <li><a href="#">Lämna en synpunkt</a></li>
      <li><a href="#">Skolverket på sociala medier</a></li>
    </digi-list-link>
    <digi-list-link ${variation(angular)} slot="top">
      <h3 slot="heading">Om oss</h3>
      <li><a href="#">Om webbplatsen</a></li>
      <li><a href="#">Vår verksamhet</a></li>
      <li><a href="#">Organisation</a></li>
      <li><a href="#">Jobba hos oss</a></li>
    </digi-list-link>
    <digi-list-link ${variation(angular)} slot="top">
      <h3 slot="heading">Nyheter och media</h3>
      <li><a href="#">Publikationer & nyhetsbrev</a></li>
      <li><a href="#">Press</a></li>
      <li><a href="#">Kalender</a></li>
      <li><a href="#">Öppna data</a></li>
      <li><a href="#">Jobba hos oss</a></li>
    </digi-list-link>
    <digi-list-link ${variation(angular)} ${layout(angular)} slot="bottom">
      <li><a href="#">Behandling av personuppgifter</a></li>
      <li><a href="#">Kakor</a></li>
      <li><a href="#">Tillgänglighet</a></li>
    </digi-list-link>`;

@Component({
	tag: 'digi-page-footer-details',
	scoped: true
})
export class DigiPageFooterDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<footer>
  <digi-page-footer>
    ${pageFooterExampleMarkup()}
  </digi-page-footer>
</footer>`,
			[CodeExampleLanguage.ANGULAR]: `\
<footer>
  <digi-page-footer>
    ${pageFooterExampleMarkup(true)}
  </digi-page-footer>
</footer>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-page-footer>
					<digi-list-link af-variation="compact" slot="top-first">
						<h3 slot="heading">Kontakt</h3>
						<li>
							<a href="#">Alla kontaktuppgifter</a>
						</li>
						<li>
							<a href="#">Lämna en synpunkt</a>
						</li>
						<li>
							<a href="#">Skolverket på sociala medier</a>
						</li>
					</digi-list-link>
					<digi-list-link af-variation="compact" slot="top">
						<h3 slot="heading">Om oss</h3>
						<li>
							<a href="#">Om webbplatsen</a>
						</li>
						<li>
							<a href="#">Vår verksamhet</a>
						</li>
						<li>
							<a href="#">Organisation</a>
						</li>
						<li>
							<a href="#">Jobba hos oss</a>
						</li>
					</digi-list-link>
					<digi-list-link af-variation="compact" slot="top">
						<h3 slot="heading">Nyheter och media</h3>
						<li>
							<a href="#">Publikationer & nyhetsbrev</a>
						</li>
						<li>
							<a href="#">Press</a>
						</li>
						<li>
							<a href="#">Kalender</a>
						</li>
						<li>
							<a href="#">Öppna data</a>
						</li>
						<li>
							<a href="#">Jobba hos oss</a>
						</li>
					</digi-list-link>
					<digi-list-link af-variation="compact" af-layout="inline" slot="bottom">
						<li>
							<a href="#">Behandling av personuppgifter</a>
						</li>
						<li>
							<a href="#">Kakor</a>
						</li>
						<li>
							<a href="#">Tillgänglighet</a>
						</li>
					</digi-list-link>
				</digi-page-footer>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Sidfot att använda på alla sidor."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Sidfoten på skolverket.se hänvisar till de viktigaste kontaktvägarna så
							att de finns tillgängligt över hela webbplatsen. Den innehåller även
							länkar till innehåll som förklarar mer om Skolverket och webbplatsen.
						</p>
						<p>
							Sidfoten på syskonwebbplatser och på E-tjänster och verktyg är mindre
							omfattande än skolverket.se. Det finns alltid en syftesbeskrivning och
							kontaktuppgifter om användaren behöver support.
						</p>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Tillgänglighet</h3>
						Notera att komponenten inte innehåller något{' '}
						<digi-code af-code="<footer>" />
						-element, så glöm inte att använda ett sådant runt komponenten.
					</digi-notification-detail>
				}
			/>
		);
	}
}
