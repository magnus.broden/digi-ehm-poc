import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { ButtonSize, ButtonVariation } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-button-details',
	scoped: true
})
export class DigiButtonDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() buttonExampleData: { [key: string]: any } = {
		size: ButtonSize.LARGE,
		variation: ButtonVariation.PRIMARY,
		icon: false,
		iconSecondary: false,
		fullWidth: false
	};

	get buttonExampleCode() {
		return {
			HTML: `\
<digi-button
  af-size="${this.buttonExampleData.size}"
  af-variation="${this.buttonExampleData.variation}"
  af-full-width="${this.buttonExampleData.fullWidth}"\>${
				this.buttonExampleData.variation === ButtonVariation.FUNCTION
					? `\n<digi-icon-envelope slot="icon" />`
					: ''
			}\
${
	this.buttonExampleData.icon ? `\n<digi-icon af-name="copy" slot="icon" />` : ''
}
  En knapp\
${
	this.buttonExampleData.iconSecondary
		? `\n<digi-icon af-name="chevron-down" slot="icon-secondary" />`
		: ''
}\n</digi-button>`,
			Angular: `\
<digi-button
  [attr.af-size]="ButtonSize.${Object.keys(ButtonSize).find(
			(key) => ButtonSize[key] === this.buttonExampleData.size
		)}"
  [attr.af-variation]="ButtonVariation.${Object.keys(ButtonVariation).find(
			(key) => ButtonVariation[key] === this.buttonExampleData.variation
		)}"
  [attr.af-full-width]="${this.buttonExampleData.fullWidth}"
  \>${
			this.buttonExampleData.variation === ButtonVariation.FUNCTION
				? `\n<digi-icon-envelope slot="icon" />`
				: ''
		}\
${
	this.buttonExampleData.icon ? `\n<digi-icon af-name="copy" slot="icon" />` : ''
}
  En knapp\
${
	this.buttonExampleData.iconSecondary
		? `\n<digi-icon af-name="chevron-down" slot="icon-secondary" />`
		: ''
}
</digi-button>`
		};
	}

	@State() showExample: boolean = true;

	changeButtonExampleData(newData: Object) {
		this.showExample = false;
		setTimeout(() => (this.showExample = true), 0);

		this.buttonExampleData = newData;
	}

	changeSizeHandler(e) {
		this.changeButtonExampleData({
			...this.buttonExampleData,
			size: e.target.value
		});
	}

	changeVariationHandler(e) {
		let newData: any = {
			...this.buttonExampleData,
			variation: e.target.value
		};

		if (e.target.value === ButtonVariation.FUNCTION) {
			newData = {
				...newData,
				icon: false,
				iconSecondary: false
			};
		}

		this.changeButtonExampleData(newData);
	}

	changeIconHandler(e) {
		const pos = e.target.value;

		let newData: any = {
			...this.buttonExampleData,
			icon: false,
			iconSecondary: false
		};

		if (pos === 'first') {
			newData = {
				...newData,
				icon: true
			};
		} else if (pos === 'last') {
			newData = {
				...newData,
				iconSecondary: true
			};
		}

		this.changeButtonExampleData(newData);
	}

	changeFullWidthHandler(e) {
		console.log(e);
		this.changeButtonExampleData({
			...this.buttonExampleData,
			fullWidth: e.target.checked
		});
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.buttonExampleCode)}
				af-controls-position="end"
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							af-legend="Varianter"
							af-name="Varianter"
							onChange={(e) => this.changeVariationHandler(e)}
						>
							<digi-form-radiobutton
								afName="Varianter"
								afLabel="Primär"
								afValue={ButtonVariation.PRIMARY}
								afChecked={true}
							/>
							<digi-form-radiobutton
								afName="Varianter"
								afLabel="Sekundär"
								afValue={ButtonVariation.SECONDARY}
							/>
							<digi-form-radiobutton
								afName="Varianter"
								afLabel="Funktion"
								afValue={ButtonVariation.FUNCTION}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset
							af-legend="Storlekar"
							af-name="Storlekar"
							onChange={(e) => this.changeSizeHandler(e)}
						>
							<digi-form-radiobutton
								afName="Storlekar"
								afLabel="Stor"
								afValue={ButtonSize.LARGE}
								afChecked
							/>
							<digi-form-radiobutton
								afName="Storlekar"
								afLabel="Liten"
								afValue={ButtonSize.SMALL}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset
							af-legend="Ikon"
							af-name="Ikon"
							onChange={(e) => this.changeIconHandler(e)}
						>
							{this.buttonExampleData.variation !== ButtonVariation.FUNCTION && (
								<span>
									<digi-form-radiobutton
										afName="Ikon"
										afLabel="Ingen ikon"
										afChecked={
											!this.buttonExampleData.icon && !this.buttonExampleData.iconSecondary
										}
										afValue={''}
									/>
									<digi-form-radiobutton
										afName="Ikon"
										afLabel="Ikon (före)"
										afChecked={this.buttonExampleData.icon}
										afValue={'first'}
									/>
									<digi-form-radiobutton
										afName="Ikon"
										afLabel="Ikon (efter)"
										afChecked={this.buttonExampleData.iconSecondary}
										afValue={'last'}
									/>
								</span>
							)}
							{this.buttonExampleData.variation === ButtonVariation.FUNCTION && (
								<p>
									<em>Ikonalternativen är inte tillgängliga för funktionsknappen.</em>
								</p>
							)}
						</digi-form-fieldset>
						<digi-form-checkbox
							afLabel="100% i bredd"
							onChange={(e) => this.changeFullWidthHandler(e)}
						></digi-form-checkbox>
					</div>
				)}
				{this.showExample && (
					<digi-button
						af-size={this.buttonExampleData.size}
						af-variation={this.buttonExampleData.variation}
						af-full-width={this.buttonExampleData.fullWidth}
					>
						{this.buttonExampleData.variation === ButtonVariation.FUNCTION && (
							<digi-icon afName={'copy'} slot="icon" />
						)}
						{this.buttonExampleData.icon && <digi-icon afName={'copy'} slot="icon" />}
						En knapp
						{this.buttonExampleData.iconSecondary && (
							<digi-icon afName={'copy'} slot="icon-secondary" />
						)}
					</digi-button>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Knappar används för att utföra en handling eller exekvera en funktion. Till
				skillnad från länkar så används de ej för att navigera inom eller mellan
				sidor."
				example={this.example()}
				description={
					<Fragment>
						<h3>Storlekar</h3>
						<p>
							Knapparna finns även i två storlekar: stor och liten. Liten är förvald.
							Storlek ställs in med <digi-code af-code="af-size" />.
						</p>
						<h3>Varianter</h3>
						<p>
							Knappar finns i tre olika varianter: Primära, sekundära och
							funktionsknappar. De sistnämnda måste ha en ikon. Variant ställs in med{' '}
							<digi-code af-code="af-variation" />.{' '}
						</p>
						<h4>Primär</h4>
						<ul>
							<li>Används alltid om det endast finns en knapp på sidan.</li>
							<li>Ska alltid vara mest framträdande i relation till andra knappar.</li>
							<li>
								Ska placeras så att den är det naturliga förstahandsvalet i
								interaktionen.
							</li>
						</ul>

						<h4>Sekundär</h4>
						<ul>
							<li>
								En sekundär knapp får endast användas om det finns mer än en knapp på
								sidan.
							</li>
							<li>
								En sekundär knapp används för den sekundära interaktionen, som till
								exempel att avbryta ett flöde, säga nej tack eller liknande.
							</li>
							<li>
								En sekundär knapp får i vissa fall ersättas med en funktionsknapp, men
								endast om det gör interaktionen tydligare för användare.
							</li>
						</ul>
						<h4>Funktionsknapp</h4>
						<ul>
							<li>
								Funktionsknappar är knappar men ser ut som länkar. Dessa används för att
								utföra en handling, till exempel Rensa eller Ändra.
							</li>
						</ul>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Riktlinjer</h3>
						<ul>
							<li>
								Inaktiv knapp ska inte användas. Alla knappar ska alltid gå att
								interagera med.
							</li>
						</ul>
					</digi-notification-detail>
				}
			/>
		);
	}
}
