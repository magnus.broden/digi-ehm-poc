import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-navigation-pagination-details',
	scoped: true
})
export class DigiNavigationPaginationDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() totalPages: number = 6;
	@State() showResultName: boolean = false;

	get navigationPaginationCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-pagination
${
	this.showResultName
		? `\
   af-total-pages="6"
   af-init-active-page="1"
   af-current-result-start="1"
   af-current-result-end="25"
   af-total-results="10"
   af-result-name="dokument"`
		: `\
   af-total-pages="10"
   af-init-active-page="1"`
}
  >
</digi-navigation-pagination>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-pagination
${
	this.showResultName
		? `\
  [attr.af-total-pages]="6"
  [attr.af-init-active-page]="1"
  [attr.af-current-result-start]="1"
  [attr.af-current-result-end]="25"
  [attr.af-total-results]="10"
  [attr.af-result-name]="'dokument'"`
		: `\
  [attr.af-total-pages]="10"
  [attr.af-init-active-page]="1"`
}
  >
</digi-navigation-pagination>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.navigationPaginationCode)}
				af-controls-position="end"
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset>
							<digi-form-checkbox
								afLabel="Resultatblock"
								afChecked={this.showResultName}
								onAfOnChange={() =>
									this.showResultName
										? (this.showResultName = false)
										: (this.showResultName = true)
								}
							/>
						</digi-form-fieldset>
					</div>
				)}
				{this.showResultName && (
					<digi-navigation-pagination
						afTotalPages={10}
						afInitActivePage={1}
						afCurrentResultStart={1}
						afCurrentResultEnd={4}
						afTotalResults={10}
						afResultName={'dokument'}
					></digi-navigation-pagination>
				)}
				{!this.showResultName && (
					<digi-navigation-pagination
						afTotalPages={10}
						afInitActivePage={1}
					></digi-navigation-pagination>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Paginering hjälper användaren att hålla sig orienterad i längre listningar av innehåll som sökträffar och filtreringar. Pagineringen placeras i slutet av en listning och håller samma bredd i layouten för att göra det tydligt vilken del av innehållet på sidan som är paginerat."
				example={this.example()}
				description={
					<Fragment>
						<h3>Varianter</h3>
						<h4>Med resultatblock</h4>
						<p>
							<digi-code af-code="af-total-pages" /> anger hur många sidor pagineringen
							innehåller och
							<digi-code af-code="af-init-active-page" /> anger vilken sida som ska
							visas som förvald. Om inget anges är första (1) sidan vald.
						</p>
						<h4>Utan resultatblock</h4>
						<p>
							Genom att inte använda <digi-code af-code="af-total-results" /> renderas
							inte resultatfältet och då behövs inte heller
							<digi-code af-code="af-result-name" />,{' '}
							<digi-code af-code="af-current-result-start" /> eller{' '}
							<digi-code af-code="af-current-result-end" /> skickas med.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
