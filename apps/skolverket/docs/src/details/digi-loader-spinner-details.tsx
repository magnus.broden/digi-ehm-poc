import { Component, Fragment, h, Prop, State } from '@stencil/core';
import { LoaderSpinnerSize } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-loader-spinner-details',
	scoped: true
})
export class DigiLoaderSpinner {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() spinnerExampleData: { [key: string]: any } = {
		size: LoaderSpinnerSize.MEDIUM
	};

	get spinnerExampleCode() {
		return {
			HTML: `
	<digi-loader-spinner 
	af-size=${this.spinnerExampleData.size}
	></digi-loader-spinner>
			`,
			Angular: `
	<digi-loader-spinner 
	[attr.af-size]="LoaderSpinnerSize.${Object.keys(LoaderSpinnerSize).find(
		(key) => LoaderSpinnerSize[key] === this.spinnerExampleData.size
	)}"
	></digi-loader-spinner>
			`
		};
	}

	@State() showExample: boolean = true;

	changeLoaderSpinnerExampleData(newData: Object) {
		this.showExample = false;
		setTimeout(() => (this.showExample = true), 0);

		this.spinnerExampleData = newData;
	}

	changeSizeHandler(e) {
		this.changeLoaderSpinnerExampleData({
			...this.spinnerExampleData,
			size: e.target.value
		});
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.spinnerExampleCode)}
				af-controls-position="end"
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							af-legend="Storlekar"
							af-name="Storlekar"
							onChange={(e) => this.changeSizeHandler(e)}
						>
							<digi-form-radiobutton
								af-name="Storlekar"
								afLabel="Small"
								afValue={LoaderSpinnerSize.SMALL}
							/>
							<digi-form-radiobutton
								af-name="Storlekar"
								afLabel="Medium"
								afChecked={true}
								afValue={LoaderSpinnerSize.MEDIUM}
							/>
							<digi-form-radiobutton
								af-name="Storlekar"
								afLabel="Large"
								afValue={LoaderSpinnerSize.LARGE}
							/>
						</digi-form-fieldset>
					</div>
				)}
				{this.showExample && (
					<digi-loader-spinner
						af-size={this.spinnerExampleData.size}
						af-text={this.spinnerExampleData.size !== 'small' ? 'Laddar' : ''}
					></digi-loader-spinner>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Används för att upplysa användaren om att vi väntar på till exempel ett svar från systemet."
				example={this.example()}
				description={
					<Fragment>
						<h3>Storlekar</h3>
						<p>
							Spinnern finns i tre storlekar, small, medium och large, som du kan välja
							med <digi-code af-variation="light" af-code="af-size" />.
						</p>
						<p>Den minsta storleken är till för ikoner</p>
						<h3>Användning</h3>
						<ul>
							<li>
								Ge komponenten ett id genom att använda{' '}
								<digi-code af-variation="light" af-code=" af-id" />, annars kommer ett
								autogeneras.
							</li>
							<li>
								Ange texten med attributet{' '}
								<digi-code af-variation="light" af-code=" af-text" />{' '}
							</li>
						</ul>
					</Fragment>
				}
			/>
		);
	}
}
