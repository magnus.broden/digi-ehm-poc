import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	LayoutColumnsElement,
	LayoutColumnsVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-layout-columns-details',
	scoped: true,
	styles: `
		ol, ul, li { list-style: none; margin: 0; }
		digi-layout-columns div { background: var(--digi--color--background--tertiary); height: 100px; }
		`
})
export class DigiLayoutColumnsDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() layoutColumnsVariation: any = 'none';
	@State() layoutColumnsElement: LayoutColumnsElement = LayoutColumnsElement.DIV;

	get layoutColumnCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
  <digi-layout-columns
    af-element="${this.layoutColumnsElement}"
  ${
			this.layoutColumnsVariation !== 'none'
				? `  af-variation="${this.layoutColumnsVariation}"\n  >`
				: '>'
		}
    ${
					this.layoutColumnsElement === LayoutColumnsElement.DIV
						? '<div></div>'
						: '<li><div></div></li>'
				}
    ...  
  </digi-layout-columns>`,
			[CodeExampleLanguage.ANGULAR]: `\
  <digi-layout-columns
    [attr.af-element]="LayoutColumnsVariation.${Object.keys(
					LayoutColumnsElement
				).find((key) => LayoutColumnsElement[key] === this.layoutColumnsElement)}"
  ${
			this.layoutColumnsVariation !== 'none'
				? `  [attr.af-variation]="LayoutColumnsVariation.${Object.keys(
						LayoutColumnsVariation
				  ).find(
						(key) => LayoutColumnsVariation[key] === this.layoutColumnsVariation
				  )}"\n  >`
				: '>'
		}
    ${
					this.layoutColumnsElement === LayoutColumnsElement.DIV
						? '<div></div>'
						: '<li><div></div></li>'
				}
    ...  
  </digi-layout-columns>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.layoutColumnCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afLegend="Antal kolumner"
							afName="variant"
							onChange={(e) => (this.layoutColumnsVariation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="variant"
								afLabel="Ingen"
								afValue="none"
								afChecked={this.layoutColumnsVariation === 'none'}
							/>
              <digi-form-radiobutton
								afName="variant"
								afLabel="En"
								afValue="one"
								afChecked={this.layoutColumnsVariation === LayoutColumnsVariation.ONE}
							/>
							<digi-form-radiobutton
								afName="variant"
								afLabel="Två"
								afValue="two"
								afChecked={this.layoutColumnsVariation === LayoutColumnsVariation.TWO}
							/>
							<digi-form-radiobutton
								afName="variant"
								afLabel="Tre"
								afValue="three"
								afChecked={this.layoutColumnsVariation === 'three'}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset
							afLegend="Elementtyp"
							afName="element"
							onChange={(e) => (this.layoutColumnsElement = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="element"
								afLabel="<div>"
								afValue={LayoutColumnsElement.DIV}
								afChecked={this.layoutColumnsElement === LayoutColumnsElement.DIV}
							/>
							<digi-form-radiobutton
								afName="element"
								afLabel="<ul>"
								afValue={LayoutColumnsElement.UL}
								afChecked={this.layoutColumnsElement === LayoutColumnsElement.UL}
							/>
							<digi-form-radiobutton
								afName="element"
								afLabel="<ol>"
								afValue={LayoutColumnsElement.OL}
								afChecked={this.layoutColumnsElement === LayoutColumnsElement.OL}
							/>
						</digi-form-fieldset>
					</div>
				)}

				{this.layoutColumnsElement === LayoutColumnsElement.DIV && (
					<digi-layout-columns
						afElement={this.layoutColumnsElement}
						afVariation={
							this.layoutColumnsVariation === 'none'
								? this.layoutColumnsVariation
								: null
						}
					>
						<div></div>

						<div></div>

						{!this.afShowOnlyExample && <div></div>}

						{!this.afShowOnlyExample && <div></div>}
					</digi-layout-columns>
				)}
				{this.layoutColumnsElement === LayoutColumnsElement.DIV &&
					this.layoutColumnsVariation !== 'none' && (
						<digi-layout-columns
							afElement={this.layoutColumnsElement}
							afVariation={this.layoutColumnsVariation}
						>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
						</digi-layout-columns>
					)}
				{this.layoutColumnsElement !== LayoutColumnsElement.DIV &&
					this.layoutColumnsVariation === 'none' && (
						<digi-layout-columns
							afElement={this.layoutColumnsElement}
							afVariation={
								this.layoutColumnsVariation === 'none'
									? this.layoutColumnsVariation
									: null
							}
						>
							<li>
								<div></div>
							</li>
							<li>
								<div></div>
							</li>
							<li>
								<div></div>
							</li>
							<li>
								<div></div>
							</li>
						</digi-layout-columns>
					)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Lägg innehåll i jämna kolumner. Kan radbryta vid satt minimibredd."
				example={this.example()}
				description={
					<Fragment>
						<h3>Bredd och gap</h3>
						<p>
							Som standard kommer den att placera alla underordnade element på samma
							rad, men genom att ställa in ett{' '}
							<digi-code af-code="--digi-layout-columns--min-width" /> önskat värde
							kommer det att börja flöda till ny rad när underordnade element når sin
							minsta bredd. Kolumngapet kan anpassas med{' '}
							<digi-code af-code="--digi--layout-columns--gap--column" />. Det går även
							att anpassa radgapet med{' '}
							<digi-code af-code="--digi--layout-columns--gap--row" />.
						</p>
						<h3>Antal kolumner</h3>
						<p>
							Med hjälp av <digi-code af-code="af-variation" /> går det att ställa in
							om man vill ändra till att ha en, två eller tre kolumner.
						</p>
						<h3>Element</h3>
						<p>
							Det går att ändra elementtyp som ska wrappa de underordnade elementen med
							hjälp av <digi-code af-code="af-element" />. Som standard är det{' '}
							<digi-code af-code="div" /> men det går också att välja{' '}
							<digi-code af-code="ol" /> och <digi-code af-code="ul" /> väljer man
							någon av listtaggarna så måste också underliggande element ligga i{' '}
							<digi-code af-code="li" />
							-taggar.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
