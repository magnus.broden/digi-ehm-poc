import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '../components/ComponentLink';

const pageHeaderExampleMarkup =
	() => /*html*/ `<p slot="top">Tillbaka till <a href="#">Skolverket.se</a></p>
    <a href="#" slot="logo"><digi-logo-sister af-name="Logotitel"></digi-logo-sister></a>
    <ul slot="eyebrow">
      <li><digi-link-icon>...</digi-link-icon></li>
      <li><digi-link-icon>...</digi-link-icon></li>
      <li><digi-link-icon>...</digi-link-icon></li>
      <li><digi-link-icon>...</digi-link-icon></li>
    </ul>
    <nav slot="nav">
	    ...huvudnavigationsinnehåll
    </nav>`;

@Component({
	tag: 'digi-page-header-details',
	scoped: true
})
export class DigiPageHeaderDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<header>
  <digi-page-header>
    ${pageHeaderExampleMarkup()}
  </digi-page-header>
</header>`,
			[CodeExampleLanguage.ANGULAR]: `\
<footer>
  <digi-page-header>
    ${pageHeaderExampleMarkup()}
  </digi-page-header>
</footer>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-page-header>
					<p slot="top">slot="top"</p>
					<a href="#" slot="logo" style={{ 'text-decoration': 'none' }}>
						<digi-logo-sister afName={`Logotitel`} />
					</a>
					<ul slot="eyebrow">
						<li>
							<digi-link-icon>
								<a href="#">
									<digi-icon af-name="search" aria-hidden="true"></digi-icon>Sök
								</a>
							</digi-link-icon>
						</li>
						<li>
							<digi-link-icon>
								<a href="#">
									<digi-icon af-name="accessibility-deaf" aria-hidden="true"></digi-icon>
									Lyssna
								</a>
							</digi-link-icon>
						</li>
						<li>
							<digi-link-icon>
								<a href="#">
									<digi-icon af-name="lattlast" aria-hidden="true"></digi-icon>Lättläst
								</a>
							</digi-link-icon>
						</li>
						<li>
							<digi-link-icon>
								<a href="#">
									<digi-icon af-name="globe" aria-hidden="true"></digi-icon>Languages
								</a>
							</digi-link-icon>
						</li>
					</ul>
					<p slot="nav" style={{ color: 'white' }}>
						slot="nav"
					</p>
				</digi-page-header>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Sidhuvud att använda på alla sidor."
				example={this.example()}
				description={
					<Fragment>
						<h3>Olika typer av sidor och tjänster</h3>
						<p>
							För att skapa ett pålitligt och konsekvent system där Skolverket är en
							tydlig avsändare samtidigt som tjänster och sajter särskiljande kommer
							till sin rätt har ett system i tre nivåer tagits fram.
						</p>
						<p>
							Sidhuvudet på e-tjänster och verktyg använder en avsändare med favicon.
							Undermenyn har likt menyn på skolverket.se primärfärg som bakgrund.
							Avsändaren länkar till första sidan för tjänsten, skolverket.se nås genom
							syftesraden ovanför.
						</p>
						<p>
							Sidhuvudet på syskonwebbplatser använder en avsändare med favicon. För en
							starkare avskiljning från skolverket.se används en vit bakgrund för
							huvudmenyn här istället. Avsändaren länkar till första sidan för
							webbplatsen och skolverket.se nås genom syftesraden ovanför.
						</p>
						<h3>Förväntat innehåll</h3>
						<p>
							Det finns fyra olika slottar i sidhuvudet: top, logo, eyebrow och nav
						</p>
						<ul>
							<li>
								<digi-code af-code="top" /> - Här kan man placera genväg till mina
								sidor, tillbaka till skolverket.se och så vidare
							</li>
							<li>
								<digi-code af-code="logo" /> - Här placerar man relevant logotyp
								beroende på vilken typ av sida det gäller. Logotyperna finns i
								versionerna <ComponentLink tag="digi-logo" />,{' '}
								<ComponentLink tag="digi-logo-sister" /> och{' '}
								<ComponentLink tag="digi-logo-service" />
							</li>
							<li>
								<digi-code af-code="eyebrow" /> - Här placerar man funktioner som sök
								och stödverktyg. Förväntar sig <digi-code af-code="<li>" />
								-element med <ComponentLink
									tag="digi-link-icon"
									text="länkikoner"
								/>{' '}
								inuti (se kodexepel ovan)
							</li>
							<li>
								<digi-code af-code="nav" /> - Innehållet här blir omslutet av{' '}
								<ComponentLink
									tag="digi-navigation-main-menu"
									text="huvudmenyskomponenten"
								/>{' '}
								och förväntar sig därmed samma innehåll som huvudmenyn.
							</li>
						</ul>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Tillgänglighet</h3>
						Notera att komponenten inte innehåller något{' '}
						<digi-code af-code="<header>" />
						-element, så glöm inte att använda ett sådant runt komponenten.
					</digi-notification-detail>
				}
			/>
		);
	}
}
