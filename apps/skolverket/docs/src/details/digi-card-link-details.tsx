import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { CardLinkImage } from '../components/CardLinkImage';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-card-link-details',
	scoped: true
})
export class DigiCardLinkDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get cardLinkCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-layout-stacked-blocks>
  <digi-card-link>
    <img
      slot="image"
      src="/images/image.jpg"
      alt="Bild på en bild"
      width="392"
      height="220"
      loading="lazy"
    />
    <h2 slot="link-heading">
      <a href="#">Beredskap vid ett förändrat omvärldsläge</a>
    </h2>
    <p>
      Förskola och skola är samhällsviktig verksamhet som behöver fungera även
      vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
      verksamhet vid olika händelser av kris.
    </p>
  </digi-card-link>
  <digi-card-link>
    <div
      slot="image"
    ></div>
    <h2 slot="link-heading">
      <a href="#">Kort med bildplaceholder</a>
    </h2>
    <p>
      Förskola och skola är samhällsviktig verksamhet som behöver fungera även
      vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
      verksamhet vid olika händelser av kris.
    </p>
  </digi-card-link>
  <digi-card-link>
    <h2 slot="link-heading">
      <a href="#">Kort utan bild</a>
    </h2>
    <p>
      Förskola och skola är samhällsviktig verksamhet som behöver fungera även
      vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
      verksamhet vid olika händelser av kris.
    </p>
  </digi-card-link>
</digi-layout-stacked-blocks>
				`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-card-link>
  <img
    slot="link-image"
    src="/images/image.jpg"
    alt="Bild på en bild"
    width="392"
    height="220"
    loading="lazy"
  />
  <h2 slot="link-heading">
    <a href="#">Beredskap vid ett förändrat omvärldsläge</a>
  </h2>
  <p>
    Förskola och skola är samhällsviktig verksamhet som behöver fungera även
    vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
    verksamhet vid olika händelser av kris.
  </p>
</digi-card-link>
				`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.cardLinkCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-layout-stacked-blocks>
					<digi-card-link>
						<CardLinkImage />
						<h2 slot="link-heading">
							<a href="#">Beredskap vid ett förändrat omvärldsläge</a>
						</h2>
						<p>
							Förskola och skola är samhällsviktig verksamhet som behöver fungera även
							vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
							verksamhet vid olika händelser av kris.
						</p>
					</digi-card-link>
					<digi-card-link>
						<div slot="image"></div>
						<h2 slot="link-heading">
							<a href="#">Kort med bildplaceholder</a>
						</h2>
						<p>
							Förskola och skola är samhällsviktig verksamhet som behöver fungera även
							vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
							verksamhet vid olika händelser av kris.
						</p>
					</digi-card-link>
					<digi-card-link>
						<h2 slot="link-heading">
							<a href="#">Kort utan bild</a>
						</h2>
						<p>
							Förskola och skola är samhällsviktig verksamhet som behöver fungera även
							vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
							verksamhet vid olika händelser av kris.
						</p>
					</digi-card-link>
				</digi-layout-stacked-blocks>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="Länkkort används när en hel puff är länkad till en sida."
				description={
					<Fragment>
						<h3>Bildhantering</h3>
						<p>
							Kortets bild ska ha proportionerna 16:9. Komponenten i sig gör inget för
							att säkerställa detta, så se till att använda bilder med rätt format.
							Bilden bör även vara{' '}
							<digi-code af-code="display: block" af-language="css" /> eller
							motsvarande.
						</p>
						<p>
							Om man inte vill ha en bild i kortet, men ändå låta den ha samma layout
							som omkringliggande kort så går det att använda en tom div i bildslotten
							(se exempel ovan)
						</p>
						<h3>Layout</h3>
						<p>
							Kortet bör användas i sammanhängande listor tillsammans med andra kort,
							och alltid i en passande layoutkomponent (annars expanderar kortet ut
							till 100% bredd, vilket förmodligen inte är önskvärt). Mest troligt är
							att kortet används i{' '}
							<ComponentLink tag="digi-page-block-cards" lowercase /> eller{' '}
							<ComponentLink tag="digi-layout-stacked-blocks" lowercase />. I exemplet
							ovan använder vi oss av{' '}
							<ComponentLink tag="digi-layout-stacked-blocks" lowercase />.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
