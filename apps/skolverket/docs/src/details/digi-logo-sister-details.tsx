import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { href } from 'stencil-router-v2';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-logo-sister-details',
	scoped: true
})
export class DigiLogoSisterDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get logoCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-logo-sister af-name="Syskonwebbplats"></digi-logo-sister>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-logo-sister [attr.af-name]="'Syskonwebbplats'"></digi-logo-sister>`
		};
	}

	@State() showSystemName: boolean;

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.logoCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{<digi-logo-sister afName={`Syskonwebbplats`}></digi-logo-sister>}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Det här är Skolverkets logotyp för syskonwebbplatser."
				example={this.example()}
				description={
					<Fragment>
						<h3>När ska logotypen användas?</h3>
						<p>
							Logotypen används i sidhuvudet på syskonwebbplatser (webbplatser med
							subdomän). Här finns{' '}
							<ComponentLink tag="digi-logo" text="logotypen för huvudwebbplatsen" />{' '}
							och här finns{' '}
							<ComponentLink
								tag="digi-logo-service"
								text="logotypen för tjänster och verktyg"
							/>
							.
						</p>
						<p>
							För vidare riktlinjer se{' '}
							<a {...href('/grafisk-profil/logotyp')}>
								vår grafiska profil om logotyper
							</a>
						</p>
					</Fragment>
				}
			/>
		);
	}
}
