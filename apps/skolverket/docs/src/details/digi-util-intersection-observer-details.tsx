import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, h, Fragment } from '@stencil/core';

@Component({
	tag: 'digi-util-intersection-observer-details',
	scoped: true
})
export class DigiUtilIntersectionObserverDetails {
	render() {
		return (
			<ComponentDetails
				showOnlyExample={false}
				preamble="Övervakar när element kommer in i och lämnar webbläsarfönstret."
				description={
					<Fragment>
						<p>
							Kan till exempel användas för att lazy loada tunga element som iframes,
							videor och så vidare, eller prefetcha länkar då användaren närmar sig
							dem.
						</p>
						<p>
							Komponenten implementerar{' '}
							<a href="https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API">
								Intersection Observer API
							</a>
							, och accepterar dess inställningar som ett attribut.
						</p>
						<p>Pseudokodsexempel i Angularformat nedan:</p>
						<digi-code-block
							afCode={`<digi-util-intersection-observer (afOnIntersect)="isVisible === true">
  <a href="/mina-sidor" [attr.rel]="isVisible ? 'prefetch' : null">Mina sidor</a>
</digi-util-intersection-observer>`}
						/>
					</Fragment>
				}
			/>
		);
	}
}
