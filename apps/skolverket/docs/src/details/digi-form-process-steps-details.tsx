import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';

@Component({
	tag: 'digi-form-process-steps-details',
	scoped: true
})
export class DigiFormProcessStepsDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get formProcessStepsCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-process-steps>
  <li>
    <digi-form-process-step
      af-label="Tillfällig lagring"
    ></digi-form-process-step>
  </li>
  <li>
    <digi-form-process-step 
      af-href="/en-lank"
      af-label="Användare"
    ></digi-form-process-step>
   </li>
  <li>
    <digi-form-process-step
      af-label="Kontaktuppgifter"
    ></digi-form-process-step>
  </li>
  <li>
    <digi-form-process-step
      af-label="Hantera ärende"
    ></digi-form-process-step>
  </li>
</digi-form-process-steps>
				`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-process-steps>
  <li>
    <digi-form-process-step
      af-label="Tillfällig lagring"
    ></digi-form-process-step>
  </li>
  <li>
    <digi-form-process-step 
      af-href="/en-lank"
      af-label="Användare"
    ></digi-form-process-step>
   </li>
  <li>
    <digi-form-process-step
      af-label="Kontaktuppgifter"
    ></digi-form-process-step>
  </li>
  <li>
    <digi-form-process-step
      af-label="Hantera ärende"
    ></digi-form-process-step>
  </li>
</digi-form-process-steps>
				`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.formProcessStepsCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-form-process-steps afCurrentStep={3}>
					<li>
						<digi-form-process-step
							afLabel={'Tillfällig lagring'}
						></digi-form-process-step>
					</li>
					<li>
						<digi-form-process-step
							af-href="#"
							afLabel={'Användare'}
						></digi-form-process-step>
					</li>
					<li>
						<digi-form-process-step
							afLabel={'Kontaktuppgifter'}
						></digi-form-process-step>
					</li>
					<li>
						<digi-form-process-step
							afLabel={'Hantera ärende'}
						></digi-form-process-step>
					</li>
				</digi-form-process-steps>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="För större formulär används en paginering som ger användaren en guidning kring var hen befinner sig i processen och en uppfattning om hur mycket arbete som återstår. Alla stegade formulär och processer bör ha ett sista steg som sammanställer allt för granskning innan formuläret skickas in."
				description={
					<Fragment>
						<p>
							Aktuellt steg markeras med en fylld cirkel. Passerade steg är klickbara
							för möjligheten att gå tillbaka och redigera eller se över ett tidigare
							steg. Nuvarande och kommande steg är ej klickbara.
						</p>
						<p>
							<digi-notification-detail>
								<h3 slot="heading">Knappar eller länkar</h3>
								<p>
									Beroende på applikationens behov så kan de passerade stegen antingen
									behöva vara knappar eller länkar. Om steget får attributet{' '}
									<digi-code af-code="af-href" /> så kommer det skapa länkar, annars
									knappar. Alla klick kan hanteras genom att lyssna på eventet{' '}
									<digi-code af-code="afOnClick" />
								</p>
							</digi-notification-detail>
						</p>
						<p>
							Processpagineringen är responsiv, så när fler steg behövs än vad som är
							möjligt att visa på en rad så används en responsiv fallback som stödjer
							fler steg. Beteendet är inbyggt i komponenten och sker per automatik.
						</p>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Tillgänglighet</h3>
						<p>
							Vi ska helst undvika att skapa formulär och processer som kräver fler än
							fem steg. Fundera på om det går att dela upp processen i flera mindre
							delar som är lättare för användaren att genomföra.
						</p>
					</digi-notification-detail>
				}
			/>
		);
	}
}
