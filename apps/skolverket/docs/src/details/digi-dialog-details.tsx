import { Component, Fragment, h, Prop, State } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-dialog-details',
	scoped: true
})
export class DigiDialog {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() isActive: boolean;

	get dialogCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-dialog 
	af-open="${this.isActive ? 'true' : 'false'}"
></digi-dialog>
			`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-dialog 
	[attr.af-open]="${this.isActive ? 'true' : 'false'}"
></digi-dialog>
			`
		};
	}

	example() {
		return (
			<digi-code-example
				afCode={JSON.stringify(this.dialogCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-button
					af-variation="primary"
					onAfOnClick={() => (this.isActive = !this.isActive)}
				>
					Öppna dialogen
				</digi-button>
				<digi-dialog
					afOpen={this.isActive}
					onAfOnClose={() => (this.isActive = false)}
				>
					<h2 slot="heading">En rubrik</h2>
					<p>
						Det här är bara ord för att illustrera hur det ser ut med text inuti.
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
						commodo egestas elit in consequat. Proin in ex consectetur, laoreet augue
						sit amet, malesuada tellus.
					</p>
					<digi-button slot="actions" onAfOnClick={() => (this.isActive = false)}>
						Stäng
					</digi-button>
				</digi-dialog>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Den här komponenten används när man behöver öppna upp ett modalt fönster
			ovanför huvudflödet för att utföra en avgränsad uppgift."
				example={this.example()}
				description={
					<Fragment>
						<h3>Tillgänglighet</h3>
						<p>
							Dialogen använder sig internt av HTML-elementet{' '}
							<digi-code af-code="dialog" />, vilket innebär att fokus automatiskt
							sätts tillbaka på rätt element när den stängs och att fokus blir inlåst i
							modalen då den är öppen. Fokus kommer även att hamna på det första
							fokusbara elementet i dialogen då den öppnas. Vill du sätta det på
							rubriken så kan du lägga till <digi-code af-code="tabindex='-1'" /> på
							rubriken.
						</p>
						<h3>Formulär</h3>
						<p>
							Om formulär inuti modalen har attributet{' '}
							<digi-code af-code="method='dialog'" /> så kommer modalen att stängas när
							formuläret skickas in eller avbryts. För att få knapparna i modalen att
							kopplas ihop med formuläret så kan du använda attributet{' '}
							<digi-code af-code="af-form='formulärets-id'" /> på knapparna.
						</p>
						<p>
							<digi-code-block
								af-code='
<digi-dialog>
	<h2 slot="heading">En rubrik</h2>
	<form
		id="form-id"
		method="dialog"
	>/** Formulärsfält etc **/ </form>
	<digi-button 
		af-type="submit"
		af-form="form-id"
		slot="actions"
	>Skicka in</digi-button>
</digi-dialog>'
							/>
						</p>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Tänk på det här</h3>
						<ul>
							<li>Man får endast visa en modal åt gången</li>
							<li>
								Texten bör vara kort men innehålla den information som användaren
								behöver för att kunna utföra den avgränsade uppgiften
							</li>
						</ul>
					</digi-notification-detail>
				}
			/>
		);
	}
}
