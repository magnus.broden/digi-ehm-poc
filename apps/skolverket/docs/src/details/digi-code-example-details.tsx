import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleVariation,
	ButtonSize,
	ButtonVariation,
	CodeBlockVariation,
	CodeExampleLanguage
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-code-example-details',
	scoped: true
})
export class DigiCodeExampleDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() buttonVariation: ButtonVariation = ButtonVariation.PRIMARY;
	@State() buttonSize: ButtonSize = ButtonSize.LARGE;
	@State() codeBlockVariation: CodeBlockVariation = CodeBlockVariation.DARK;
	@State() exampleVariation: CodeExampleVariation = CodeExampleVariation.LIGHT;

	get buttonCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
  <digi-button
    af-size="${this.buttonSize}"
    af-variation="${this.buttonVariation}"
  >
    <digi-icon-arrow-right slot="icon" />
    Jag är en knapp
  </digi-button>`,
			[CodeExampleLanguage.ANGULAR]: `\
  <digi-button
    [attr.af-size]="ButtonSize.${Object.keys(ButtonSize).find(
					(key) => ButtonSize[key] === this.buttonSize
				)}"
    [attr.af-variation]="ButtonVariation.${Object.keys(ButtonVariation).find(
					(key) => ButtonVariation[key] === this.buttonVariation
				)}"
  >
    <digi-icon-arrow-right slot="icon" />
    Jag är en knapp
  </digi-button>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.buttonCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variation"
							onChange={(e) => (this.buttonVariation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Primär"
								afValue={ButtonVariation.PRIMARY}
								afChecked={this.buttonVariation === ButtonVariation.PRIMARY}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Sekundär"
								afValue={ButtonVariation.SECONDARY}
								afChecked={this.buttonVariation === ButtonVariation.SECONDARY}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Funktion"
								afValue={ButtonVariation.FUNCTION}
								afChecked={this.buttonVariation === ButtonVariation.FUNCTION}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset
							afName="Storlek"
							afLegend="Storlek"
							onChange={(e) => (this.buttonSize = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Storlek"
								afLabel="Liten"
								afValue={ButtonSize.SMALL}
								afChecked={this.buttonSize === ButtonSize.SMALL}
							/>
							<digi-form-radiobutton
								afName="Storlek"
								afLabel="Stor"
								afValue={ButtonSize.LARGE}
								afChecked={this.buttonSize === ButtonSize.LARGE}
							/>
						</digi-form-fieldset>
					</div>
				)}

				<digi-button afVariation={this.buttonVariation} afSize={this.buttonSize}>
					Jag är en knapp
				</digi-button>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Med hjälp av kodexempelkomponenten går det att visa live-exempel av
					kodsnuttar. I högerspalten kan formlärkomponenter läggas in, t.ex.
					radioknappar, för att kunna växla mellan olika varianter och kodspråk.
					Det finns också möjlighet att kopiera koden som genererats."
				example={this.example()}
				description={
					<Fragment>
						<h3>Kod</h3>
						<h4>Fler än ett språk</h4>
						<p>
							<digi-code-block
								af-language="javascript"
								af-code='get buttonCode() {
    return {
      HTML:`\
      <digi-button
        af-size="${this.buttonSize}"
        af-variation="${this.buttonVariation}"
      >
        <digi-icon af-name="chevron-right" slot="icon" />
          Jag är en knapp
        </digi-button>`,
      Angular:`\
      <digi-button
        [attr.af-size]="ButtonSize.${Object.keys(ButtonSize).find(key => 
          ButtonSize[key] === this.buttonSize)}"
        [attr.af-variation]="ButtonVariation.${Object.keys(ButtonVariation).find(key => 
          ButtonVariation[key] === this.buttonVariation)}"
      >
        <digi-icon af-name="chevron-right" slot="icon" />
        Jag är en knapp
      </digi-button>`
          }
        }'
							/>
						</p>
						<p>
							Lägg in den kod som ska visas i <digi-code af-code="af-code" />. Används
							flera språk så kan en geter skapas som i exemplet ovan och då genereras
							radioknappar för det längst ner i högerspalten. Är det bara ett språk kan
							det läggas in direkt.
						</p>
						<p>
							Lägg sedan in en div i sloten 'controls' och ge den klassen
							"slot__controls". Efter det kan en eller flera{' '}
							<digi-code af-code="digi-form-fieldset" /> med de formulärselement, t.ex.{' '}
							<digi-code af-code="digi-form-radiobutton" />, som du behöver läggas in.
						</p>
						<p>
							Efter div:en för kontroller läggs sedan koden för exemplet som ska visas
							in.
						</p>
						<p>
							<digi-code-block
								af-code='
             <digi-code-example 
             af-code={JSON.stringify(this.buttonCode)}
           >
             <div class="slot__controls" slot="controls">
                <digi-form-fieldset afName="Variation" afLegend="Variation" 
                onChange={e => this.buttonVariation = (e.target as any).value}>
                  <digi-form-radiobutton
                  afName="Variation"
                  afLabel="Primär"
                  afValue={ButtonVariation.PRIMARY}
                  afChecked={this.buttonVariation === ButtonVariation.PRIMARY}
                  />
                  (...)
                  />
                </digi-form-fieldset>
                <digi-form-fieldset afName="Storlek" afLegend="Storlek" 
                onChange={e => this.buttonSize = (e.target as any).value}>
                  <digi-form-radiobutton
                  afName="Storlek"
                  afLabel="Liten"
                  afValue={ButtonSize.SMALL}
                  afChecked={this.buttonSize === ButtonSize.LARGE}
                  />
                  (...)
                </digi-form-fieldset>
             </div>
             <digi-button  
             afVariation={this.buttonVariation} 
             afSize={this.buttonSize}>
                Jag är en knapp
              </digi-button>
            </digi-code-example>'
							></digi-code-block>
						</p>
						<h3>Varianter</h3>
						<p>
							Det går att ändra exemplets bakgrund med hjälp av{' '}
							<digi-code af-code="af-example-variation" />. Alternativen är ljus
							(light) och mörkt (dark), varav ljus är standard. Det går även att ändra
							bakgrunden på kodblocket med hjälp av{' '}
							<digi-code af-code="af-code-block-variation" />. Det går att välja mellan
							ett mörkt färgschema (dark) och ett ljust (light), varav ljus är
							standard.
						</p>

						<digi-code-example
							af-code="<digi-button>
  Jag är en knapp
</digi-button>"
							afCodeBlockVariation={this.codeBlockVariation}
							afExampleVariation={this.exampleVariation}
						>
							<div class="slot__controls" slot="controls">
								<digi-form-fieldset
									afName="bgVariation"
									afLegend="Bakgrund på exemplet"
									onChange={(e) => (this.exampleVariation = (e.target as any).value)}
								>
									<digi-form-radiobutton
										afName="bgVariation"
										afLabel="Ljus"
										afValue={CodeExampleVariation.LIGHT}
										afChecked={this.exampleVariation === CodeExampleVariation.LIGHT}
									/>
									<digi-form-radiobutton
										afName="bgVariation"
										afLabel="Mörk"
										afValue={CodeExampleVariation.DARK}
										afChecked={this.exampleVariation === CodeExampleVariation.DARK}
									/>
								</digi-form-fieldset>
								<digi-form-fieldset
									afName="colorScheme"
									afLegend="Färgschema på kodblocket"
									onChange={(e) => (this.codeBlockVariation = (e.target as any).value)}
								>
									<digi-form-radiobutton
										afName="colorScheme"
										afLabel="Mörk"
										afValue={CodeBlockVariation.DARK}
										afChecked={this.codeBlockVariation === CodeBlockVariation.DARK}
									/>
									<digi-form-radiobutton
										afName="colorScheme"
										afLabel="Ljus"
										afValue={CodeBlockVariation.LIGHT}
										afChecked={this.codeBlockVariation === CodeBlockVariation.LIGHT}
									/>
								</digi-form-fieldset>
							</div>
							<digi-button>Jag är en knapp</digi-button>
						</digi-code-example>
					</Fragment>
				}
			/>
		);
	}
}
