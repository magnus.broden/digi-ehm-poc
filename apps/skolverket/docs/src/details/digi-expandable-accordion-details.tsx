import { Component, Prop, h, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	ExpandableAccordionVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-expandable-accordion-details',
	scoped: true
})
export class DigiExpandableAccordionDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get faqAccordionCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-expandable-accordion
  af-heading="Utfällbar rubrik" 
  af-variation="secondary" 
>
<p>Ea nulla enim enim voluptate mollit proident.</p>
</digi-expandable-accordion>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-expandable-accordion
  [attr.af-heading]="Utfällbar rubrik" 
  [attr.af-variation]="ExpandableAccordionVariation.SECONDARY"" 
>
<p>Ea nulla enim enim voluptate mollit proident.</p>
</digi-expandable-accordion>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.faqAccordionCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-expandable-accordion
					afHeading="Utfällbar rubrik"
					afExpanded={false}
					afVariation={ExpandableAccordionVariation.SECONDARY}
				>
					<p>Ea nulla enim enim voluptate mollit proident.</p>
				</digi-expandable-accordion>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Den här komponenten kan användas för att minska vertikalt utrymme på en
			sida med stora mängder information. Den är komprimerad som standard och
			expanderas genom att klicka på rubrikytan."
				example={this.example()}
				description={
					<Fragment>
						<h3>Varianter</h3>
						<p>
							På Skolverket använder vi den sekundära varianten, det justeras enligt
							kodexmplet ovan <digi-code af-code="af-variation='secondary'" />.
						</p>
						<h3>Rubriknivå</h3>
						<p>
							Rubriken läggs in med{' '}
							<digi-code af-variation="light" af-code="af-heading" /> och rubriknivån
							är <digi-code af-variation="light" af-code="h2" /> som standard. Vill du
							ändra rubriknivå kan du använda{' '}
							<digi-code af-variation="light" af-code="af-heading-level" /> .
							Typescriptanvändare bör importera och använda
							<digi-code
								af-variation="light"
								af-code="ExpandableFaqItemHeadingLevel"
							/>
							. Stylingen på kommer se likadan ut även om du ändrar till en lägre
							rubriknivå.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
