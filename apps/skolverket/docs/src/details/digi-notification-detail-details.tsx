import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	NotificationDetailVariation,
	CodeExampleLanguage
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';

@Component({
	tag: 'digi-notification-detail-details',
	scoped: true
})
export class DiginNotificationDetailDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() notificationDetailVariation: NotificationDetailVariation =
		NotificationDetailVariation.INFO;

	get notificationDetailCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-notification-detail af-variation="${this.notificationDetailVariation}">
  <h2 slot="heading">En rubrik</h2>
  Meddelandebeskrivning
</digi-notification-detail>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-notification-detail [attr.af-variation]="NotificationDetailVariation.${Object.keys(
				NotificationDetailVariation
			).find(
				(key) =>
					NotificationDetailVariation[key] === this.notificationDetailVariation
			)}">
  <h2 slot="heading">En rubrik</h2>
  Meddelandebeskrivning
</digi-notification-detail>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.notificationDetailCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variant"
							onChange={(e) =>
								(this.notificationDetailVariation = (e.target as any).value)
							}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Info"
								afValue={NotificationDetailVariation.INFO}
								afChecked={
									this.notificationDetailVariation === NotificationDetailVariation.INFO
								}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Fara"
								afValue={NotificationDetailVariation.DANGER}
								afChecked={
									this.notificationDetailVariation === NotificationDetailVariation.DANGER
								}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Varning"
								afValue={NotificationDetailVariation.WARNING}
								afChecked={
									this.notificationDetailVariation ===
									NotificationDetailVariation.WARNING
								}
							/>
						</digi-form-fieldset>
					</div>
				)}
				<digi-notification-detail af-variation={this.notificationDetailVariation}>
					<h2 slot="heading">En rubrik</h2>
					<p>Meddelandebeskrivning</p>
				</digi-notification-detail>
				`
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Informationsmeddelande används för att lyfta fram viktig
			information till användaren."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Detaljmeddelanden används i artikelinnehåll för att lyfta fram viktig
							information. Meddelandet går ej att stänga.
						</p>
						<h3>Globala och lokala meddelanden</h3>
						<p>
							Se{' '}
							<ComponentLink
								tag="digi-notification-alert"
								text="informationsmeddelande"
							/>{' '}
							för globala och lokala meddelanden.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
