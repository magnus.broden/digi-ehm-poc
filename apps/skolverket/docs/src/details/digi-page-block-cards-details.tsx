import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, PageBlockCardsVariation } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '../components/ComponentLink';
import { CardLinkImage } from '../components/CardLinkImage';

@Component({
	tag: 'digi-page-block-cards-details',
	scoped: true
})
export class DigiPageBlockCardsDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() variation: PageBlockCardsVariation = PageBlockCardsVariation.START;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-page-block-cards
  af-variation="${this.variation}"
>
  <h2 slot="heading">Undervisning</h2>
  <digi-card-link>...</digi-card-link>
  <digi-card-link>...</digi-card-link>
  <digi-card-link>...</digi-card-link>
  <digi-card-link>...</digi-card-link>
  <digi-card-link>...</digi-card-link>
</digi-page-block-cards>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-page-block-cards
  [attr.af-variation]="PageBlockCardsVariation.${Object.keys(
			PageBlockCardsVariation
		).find((key) => PageBlockCardsVariation[key] === this.variation)}"
>
  <h2 slot="heading">Undervisning</h2>
  <digi-card-link>...</digi-card-link>
  <digi-card-link>...</digi-card-link>
  <digi-card-link>...</digi-card-link>
  <digi-card-link>...</digi-card-link>
  <digi-card-link>...</digi-card-link>
</digi-page-block-cards>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variation"
							onChange={(e) => (this.variation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Startsida"
								afValue={PageBlockCardsVariation.START}
								afChecked={this.variation === PageBlockCardsVariation.START}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Undersida"
								afValue={PageBlockCardsVariation.SUB}
								afChecked={this.variation === PageBlockCardsVariation.SUB}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Sektionssida"
								afValue={PageBlockCardsVariation.SECTION}
								afChecked={this.variation === PageBlockCardsVariation.SECTION}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Processida"
								afValue={PageBlockCardsVariation.PROCESS}
								afChecked={this.variation === PageBlockCardsVariation.PROCESS}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Artikelsida"
								afValue={PageBlockCardsVariation.ARTICLE}
								afChecked={this.variation === PageBlockCardsVariation.ARTICLE}
							/>
						</digi-form-fieldset>
					</div>
				)}
				<digi-page-block-cards af-variation={this.variation}>
					<h2 slot="heading">Undervisning</h2>
					<digi-card-link>
						<CardLinkImage />
						<h2 slot="link-heading">
							<a href="#">Beredskap vid ett förändrat omvärldsläge</a>
						</h2>
						<p>
							Förskola och skola är samhällsviktig verksamhet som behöver fungera även
							vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
							verksamhet vid olika händelser av kris.
						</p>
					</digi-card-link>
					<digi-card-link>
						<CardLinkImage />
						<h2 slot="link-heading">
							<a href="#" slot="link-heading">
								Nyanlända barn och elever från Ukraina
							</a>
						</h2>
						<p>
							Här hittar du som tar emot barn och elever från Ukraina information vad
							som gäller, det stöd vi erbjuder för mottagande och kartläggning och om
							skolsystemet och de olika skolformerna på engelska.
						</p>
					</digi-card-link>
					<digi-card-link>
						<h2 slot="link-heading">
							<a href="#">Ett kort utan bild</a>
						</h2>
						<p>Etiam porta sem malesuada magna mollis euismod.</p>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
					</digi-card-link>
					<digi-card-link>
						<CardLinkImage />
						<h2 slot="link-heading">
							<a href="#">Och så en länkad rubrik</a>
						</h2>
					</digi-card-link>
					<digi-card-link>
						<div slot="image"></div>
						<h2 slot="link-heading">
							<a href="#">Och så en länkad rubrik</a>
						</h2>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
					</digi-card-link>
				</digi-page-block-cards>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Sidblockskomponent med länkade kort."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Blocket förväntar sig flera{' '}
							<ComponentLink tag="digi-card-link" text="länkade kort" /> som barn.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
