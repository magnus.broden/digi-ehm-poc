import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-link-icon-details',
	scoped: true
})
export class DigiLinkIconDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-link-icon>
  <a href="#"><digi-icon af-name="search" aria-hidden="true"></digi-icon>Sök</a>
</digi-link-icon>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-link-icon>
  <a href="#"><digi-icon af-name="search" aria-hidden="true"></digi-icon>Sök</a>
</digi-link-icon>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-link-icon>
					<a href="#">
						<digi-icon af-name="search" aria-hidden="true"></digi-icon>Sök
					</a>
				</digi-link-icon>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Länk med ikon och text."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Ikonlänken är främst avsedd för verktygs- och funktionslänkar i
							sidhuvudet.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
