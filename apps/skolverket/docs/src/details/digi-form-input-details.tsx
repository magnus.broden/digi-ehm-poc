import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormInputButtonVariation,
	FormInputType,
	FormInputMode,
	FormInputValidation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-input-details',
	scoped: true
})
export class DigiFormInputDetails {
	@State() isSubmitted: boolean = false;
	@State() hasSubmitted: boolean = false;
	@State() formState = {
		firstName: {
			valid: false,
			errorText: 'Ange ditt förnamn'
		},
		lastName: {
			valid: false,
			errorText: 'Ange ditt efternamn'
		}
	};
	@State() errorList = [];

	@State() formInputType: FormInputType = FormInputType.TEXT;
	@State() formInputButtonVariation: FormInputButtonVariation = FormInputButtonVariation.SECONDARY;
	@State() formInputValidation: FormInputValidation = FormInputValidation.NEUTRAL;
	@State() formInputMode: FormInputMode = FormInputMode.DEFAULT;
	@State() hasDescription: boolean = false;
	@State() hasButton: boolean = false;

	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	submitHandler(e) {
		e.detail.preventDefault();
		this.validateForm();
		this.hasSubmitted = true;
	}

	validateForm() {
		const formElements = [...Object.keys(this.formState)];
		const tempState = this.formState;
		let errorList = [];

		formElements.forEach((element) => {
			const el = document.getElementById(element) as HTMLFormElement;
			tempState[element].valid = el.value === '' ? false : true;
		});

		Object.entries(tempState).forEach(([key, value]) => {
			!!value.valid || errorList.push({ id: key, message: value.errorText });
		});

		this.formState = { ...tempState };
		this.errorList = [...errorList];

		if (this.errorList.length === 0) {
			this.isSubmitted = true;
		}
	}
	get inputCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-input
	af-label="Etikett"
	af-label-description="Beskrivande text"
	af-button-variation="${this.formInputButtonVariation}"
	af-type="${this.formInputType}"
	af-validation="${this.formInputValidation}"\
	${this.hasDescription ? '\n\taf-label-description="Beskrivande text"\t ' : ''}\
	${this.formInputMode ? `\n\taf-inputmode="${this.formInputMode}"\t ` : ''}\
	${this.formInputValidation !== FormInputValidation.NEUTRAL ? '\n\taf-validation-text="Det här är ett valideringsmeddelande"\n>' : '\n>'}
	${this.hasButton ? `<digi-button slot="button">Skicka</digi-button>` : ''}
</digi-form-input>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-input
	[attr.af-label]="'Etikett'"
	[attr.af-label-description]="'Beskrivande text'"
	[attr.af-button-variation]="FormInputButtonVariation.${Object.keys(FormInputButtonVariation).find((key) => FormInputButtonVariation[key] === this.formInputButtonVariation)}"
	[attr.af-type]="FormInputType.${Object.keys(FormInputType).find((key) => FormInputType[key] === this.formInputType)}"
	[attr.af-validation]="FormInputValidation.${Object.keys(FormInputValidation).find((key) => FormInputValidation[key] === this.formInputValidation)}"\
	${this.hasDescription ? `\n\t[attr.af-label-description]="'Beskrivande text'"\t ` : ''}\
	${this.formInputMode ? `\n\t[attr.af-inputmode]="FormInputMode.${Object.keys(FormInputMode).find((key) => FormInputMode[key] === this.formInputMode)}"\t ` : ''}\
	${this.formInputValidation !== FormInputValidation.NEUTRAL ? `\n\t[attr.af-validation-text]="'Det här är ett valideringsmeddelande'"\n>` : '\n>'}
	${this.hasButton ? `<digi-button slot="button">Skicka</digi-button>` : ''}
</digi-form-input>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.inputCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<div style={{ width: '400px' }}>
					<digi-form-input
						afLabel="Etikett"
						afLabelDescription={this.hasDescription ? 'Beskrivande text' : null}
						af-validation-text="Det här är ett valideringsmeddelande"
						afButtonVariation={this.formInputButtonVariation}
						af-type={this.formInputType}
						afValidation={this.formInputValidation}
					>
						{this.hasButton && <digi-button slot="button">Skicka</digi-button>}
					</digi-form-input>
				</div>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-select
							afLabel="Validering"
							onAfOnChange={(e) =>
								(this.formInputValidation = (e.target as any).value)
							}
							af-variation="small"
						>
							<option value="neutral">Ingen</option>
							<option value="error">Felaktig</option>
							<option value="success">Godkänd</option>
							<option value="warning">Varning</option>
						</digi-form-select>
						<digi-form-select
							afLabel="Typ"
							onAfOnChange={(e) => (this.formInputType = (e.target as any).value)}
							af-variation="small"
							af-start-selected="10"
						>
							<option value="color">Färg</option>
							<option value="date">Datum</option>
							<option value="datetime-local">Lokalt datum</option>
							<option value="email">Epostadress</option>
							<option value="hidden">Gömd</option>
							<option value="month">Månad</option>
							<option value="number">Nummer</option>
							<option value="password">Lösenord</option>
							<option value="search">Sök</option>
							<option value="tel">Telefonnummer</option>
							<option value="text">Text</option>
							<option value="time">Tid</option>
							<option value="url">URL</option>
							<option value="week">Vecka</option>
						</digi-form-select>


						<digi-form-select
							afLabel="Inmatningsläge"
							onAfOnChange={(e) =>
								(this.formInputMode = (e.target as any).value)
							}
							af-variation="small"
							af-start-selected="10"
						>
							<option value="">Standardläge</option>
							<option value="none">Inget</option>
							<option value="text">Text</option>
							<option value="decimal">Decimaler</option>
							<option value="numeric">Numerisk</option>
							<option value="tel">Telefon</option>
							<option value="search">Sök</option>
							<option value="email">E-post</option>
							<option value="url">Url</option>
						</digi-form-select>

						<digi-form-fieldset afLegend="Beskrivande text">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasDescription}
								onAfOnChange={() => (this.hasDescription = !this.hasDescription)}
							></digi-form-checkbox>
						</digi-form-fieldset>
						<digi-form-fieldset afLegend="Med knapp">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasButton}
								onAfOnChange={() => (this.hasButton = !this.hasButton)}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Inmatningsfält används för att användaren ska kunna fylla i information
			när det inte finns färdiga alternativ att välja mellan."
				example={this.example()}
				description={
					<Fragment>
						<h3>Validering</h3>
						<p>
							Inmatningsfältet kan ha fyra olika valideringsstatus: Godkänd, felaktig,
							varning och neutral (som är den förvalda valideringstypen).
							Valideringsmeddelanden kan visas vid alla statusar, men kommer endast att
							synas om användaren har interagerat med fältet.
						</p>
						<digi-notification-detail>
							<h4 slot="heading">Bra att tänka på</h4>
							<ul>
								<li>Validering görs direkt när användaren lämnar inmatningsfältet.</li>
								<li>
									Tänk på att formulera felmeddelandet kort och koncist. Användaren ska
									förstå vad som gick fel och vad som behöver göras för att rätta till
									felet. Var konsekvent i formuleringarna.
								</li>
								<li>Rensa inte informationen i fältet vid validering.</li>
								<li>
									Inmatningsfält kan också validera bekräftande. Används främst för då
									datat i fältet hämtar information i ett annat system eller liknande.
								</li>
							</ul>
						</digi-notification-detail>

						<h3>Typer</h3>
						<p>
							Inmatningsfältet stödjer de flesta fälttyper och mappar direkt
							till inputelementets <digi-code af-code="type"></digi-code>
							-attribut.
						</p>
						<h3>Inmatningsläge</h3>
						<p>
							Inmatningsfältet stödjer de flesta inmatningslägena och mappar direkt till
							inputelementets <digi-code af-code="inputmode"></digi-code>
							-attribut.
						</p>
						<p>
							Inmatningsläge är till för att ändra funktionaliteten för enheter med virtuella tangentbord.
							Detta kan bland annat användas till att skapa textfält med inmatning för siffror på mobila enheter.
						</p>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Övriga riktlinjer</h3>
						<ul>
							<li>
								Skriv korta och tydliga etiketter. Det ska vara enkelt för användaren
								att förstå vilken information som fältet ska fyllas med.
							</li>
							<li>Är det möjligt ska vi tillåta inmatning i olika format.</li>
							<li>
								Visa vilket format som ska användas i de fall det krävs, användaren ska
								inte behöva gissa vilket format som gäller.
							</li>
							<li>
								Anpassa storleken på inmatningsfältet för att passa för ett normalt svar
								i sammanhanget. Men ta samtidigt hänsyn till helheten, så att inte
								formuläret upplevs som rörigt.
							</li>
							<li>
								Fundera på ordningsföljden och grupperingen av inmatningsfält, så att
								den följer största möjliga logik för användaren.
							</li>
							<li>
								Är det möjligt att förpopulera inmatningsfält med information ska vi
								göra det.
							</li>
						</ul>
					</digi-notification-detail>
				}
			/>
		);
	}
}
