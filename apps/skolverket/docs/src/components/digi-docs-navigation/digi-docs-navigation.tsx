import { Component, Element, h, Listen } from '@stencil/core';
import { HTMLStencilElement, State } from '@stencil/core/internal';
import { Category } from '@digi/taxonomies';
import {
	UtilBreakpointObserverBreakpoints,
	NavigationSidebarVariation,
	NavigationSidebarPosition,
	NavigationVerticalMenuActiveIndicatorSize
} from '@digi/skolverket';

import state from '../../store/store';

import { router } from '../../global/routes/router';
import { InternalRouterState } from 'stencil-router-v2/dist/types';

@Component({
	tag: 'digi-docs-navigation',
	styleUrl: 'digi-docs-navigation.scss',
	scoped: true
})
export class DigiDocsNavigation {
	Router = router;

	@Element() el: HTMLStencilElement;

	private _initiated: boolean;

	@State() _activePath: string;
	@State() isMobile: boolean;
	@State() isOpen: boolean;

	@Listen('click')
	clickHandler(e: any) {
		if (e.target.matches('.digi-navigation-vertical-menu-item__link')) {
			if (e.target.getAttribute('href')[0] === '/') {
				this.Router.push(e.target.getAttribute('href'));
			}
			// If target is combined link and toggle button,
			// open up subnav when clicking on the link
			if (
				e.target.parentElement.matches(
					'.digi-navigation-vertical-menu-item__inner--combined'
				)
			) {
				let el = e.target.closest('digi-navigation-vertical-menu-item');
				el.setAttribute('af-active-subnav', 'true');
			}

			if (this.isMobile) {
				state.activeNav = false;
			}
		}
	}

	get wrapperElement() {
		return this.isMobile ? 'digi-navigation-sidebar' : 'div';
	}

	getComponentName(component: any) {
		const componentName =
			component.docsTags?.find((tag) => tag.name === 'swedishName')?.text ||
			component.tag;

		return componentName;
	}

	componentWillLoad() {
		this.Router.onChange(
			'activePath',
			(_activePath: InternalRouterState['activePath']) => {
				this._activePath = _activePath;

				if (this._initiated) {
					this.setActiveSubnavs();
				}
			}
		);
	}

	componentDidLoad() {
		this._initiated = true;
		this.setActiveSubnavs();
	}

	setActiveSubnavs() {
		let activeEl = null;

		if (state.router.newPaths.length > 1) {
			let path = [...state.router.newPaths];
			activeEl = document.querySelector(
				`digi-navigation-vertical-menu-item[af-href="/${path.join('/')}"]`
			);
		}

		if (activeEl) {
			let parentUl = activeEl.closest('ul');
			let parentToggle = parentUl.parentElement.querySelector(
				'digi-navigation-vertical-menu-item'
			);

			function setActiveAttribute() {
				if (
					!parentUl.matches('.digi-docs-main-nav__root') &&
					parentToggle.getAttribute('af-active-subnav')
				) {
					parentToggle.setAttribute('af-active-subnav', 'true');
				}
				if (activeEl.getAttribute('af-active-subnav') === 'false') {
					activeEl.setAttribute('af-active-subnav', 'true');
				}
			}
			setActiveAttribute();

			let items = 1;
			let i = 0;
			while (i < items) {
				const newParentUl = parentUl.parentElement.closest('ul');
				if (newParentUl) {
					parentUl = newParentUl;
					parentToggle = newParentUl.parentElement.querySelector(
						'digi-navigation-vertical-menu-item'
					);
					setActiveAttribute();
					items++;
				}
				i++;
			}
		}
	}

	checkActive(level: number, linkItem: string, exact: boolean = true) {
		return String(
			state.router.newPaths[level] === linkItem &&
				(exact ? state.router.newPaths.length === level + 1 : true)
		);
	}

	render() {
		return (
			<digi-util-breakpoint-observer
				id="sideNavObserver"
				onAfOnChange={(e) =>
					(this.isMobile =
						e.detail.value === UtilBreakpointObserverBreakpoints.SMALL)
				}
			>
				<digi-navigation-sidebar
					id="sideNav"
					class="digi-docs-navigation__sidebar"
					afActive={this.isMobile ? state.activeNav : true}
					afVariation={NavigationSidebarVariation.STATIC}
					afPosition={NavigationSidebarPosition.END}
					afHideHeader={this.isMobile ? false : true}
					afStickyHeader={true}
					afCloseButtonText="Stäng"
					onAfOnClose={() => (state.activeNav = false)}
					onAfOnEsc={() => (state.activeNav = false)}
					onAfOnBackdropClick={() => (state.activeNav = false)}
				>
					<digi-navigation-vertical-menu
						afId="digi-docs-main-nav"
						afAriaLabel="Huvudmeny"
						afActiveIndicatorSize={
							NavigationVerticalMenuActiveIndicatorSize.SECONDARY
						}
					>
						<ul class="digi-docs-main-nav__root">
							<li>
								<digi-navigation-vertical-menu-item
									af-text="Aktuellt"
									af-active-subnav="false"
								></digi-navigation-vertical-menu-item>
								<ul>
									<li>
										<digi-navigation-vertical-menu-item
											af-text="Pågående arbete"
											af-href={`${state.routerRoot}aktuellt/pagaende-arbete`}
											af-override-link="true"
											af-active={this.checkActive(1, 'pagaende-arbete')}
										></digi-navigation-vertical-menu-item>
									</li>
									<li>
										<digi-navigation-vertical-menu-item
											af-text="Release notes"
											af-href={`${state.routerRoot}aktuellt/release-notes`}
											af-override-link="true"
											af-active={this.checkActive(1, 'release-notes')}
										></digi-navigation-vertical-menu-item>
									</li>
								</ul>
							</li>
							<li>
								<digi-navigation-vertical-menu-item
									af-text="Om designsystemet"
									af-active-subnav="false"
								></digi-navigation-vertical-menu-item>
								<ul>
									<li>
										<digi-navigation-vertical-menu-item
											af-text="Introduktion"
											af-href={`${state.routerRoot}om-designsystemet/introduktion`}
											af-override-link="true"
											af-active={this.checkActive(1, 'introduktion')}
										></digi-navigation-vertical-menu-item>
									</li>
								</ul>
							</li>
							<li>
								<digi-navigation-vertical-menu-item
									af-text="Komponenter"
									af-active-subnav="false"
								></digi-navigation-vertical-menu-item>

								<ul>
									<li>
										<digi-navigation-vertical-menu-item
											af-text="Om vårt komponentbibliotek"
											af-href={`${state.routerRoot}komponenter/om-komponenter`}
											af-override-link="true"
											af-active={this.checkActive(1, 'om-komponenter')}
										></digi-navigation-vertical-menu-item>
									</li>
									{Object.values(Category)
										.sort()
										.filter(
											(category) =>
												category !== Category.ICON && category !== Category.UNCATEGORIZED
										)
										.map((category) => {
											return (
												<li>
													<digi-navigation-vertical-menu-item
														af-text={category}
														af-active-subnav="false"
													></digi-navigation-vertical-menu-item>
													<ul>
														{state.components
															.filter((component) => component.category === category)
															.filter((component) => !component.tag.includes('digi-icon-'))
															.map((component) => {
																return (
																	<li>
																		<digi-navigation-vertical-menu-item
																			id={`komponent-${component.tag}`}
																			af-href={`${state.routerRoot}komponenter/${component.tag}/oversikt`}
																			af-override-link="true"
																			af-text={this.getComponentName(component)}
																			af-active={this.checkActive(1, component.tag, false)}
																		></digi-navigation-vertical-menu-item>
																	</li>
																);
															})}
													</ul>
												</li>
											);
										})}
								</ul>
							</li>
							<li>
								<digi-navigation-vertical-menu-item
									af-text="Design tokens"
									af-active-subnav="false"
								></digi-navigation-vertical-menu-item>
								<ul>
									<li>
										<digi-navigation-vertical-menu-item
											af-text="Färger"
											af-href={`${state.routerRoot}design-tokens/farger`}
											af-override-link="true"
											af-active={this.checkActive(1, 'farger', false)}
										></digi-navigation-vertical-menu-item>
									</li>
								</ul>
							</li>
							<li>
								<digi-navigation-vertical-menu-item
									af-text="Tillgänglighet"
									af-active-subnav="false"
								></digi-navigation-vertical-menu-item>
								<ul>
									<li>
										<digi-navigation-vertical-menu-item
											af-text="Tillgänglighetslista"
											af-href={`${state.routerRoot}tillganglighet-och-design/tillganglighet-checklista`}
											af-override-link="true"
											af-active={this.checkActive(1, 'tillganglighet-checklista')}
										></digi-navigation-vertical-menu-item>

										<li>
											<digi-navigation-vertical-menu-item
												af-text="Nivåer i WCAG"
												af-href={`${state.routerRoot}tillganglighet-checklista/wcag-levels`}
												af-override-link="true"
												af-active={this.checkActive(1, 'wcag-levels')}
											></digi-navigation-vertical-menu-item>
										</li>
									</li>
								</ul>
							</li>
						</ul>
					</digi-navigation-vertical-menu>
				</digi-navigation-sidebar>
			</digi-util-breakpoint-observer>
		);
	}
}
