import { FunctionalComponent, h } from '@stencil/core';
import { href } from 'stencil-router-v2';
import {
	ComponentTag,
	componentTagsAndNames
} from '../global/routes/componentTagsAndNames.generated';
import { getComponentRoute } from '../global/routes/helpers';

export interface ComponentLinkProps {
	tag: ComponentTag;
	text?: string;
	lowercase?: boolean;
	openingSlash?: boolean;
}

const getText = (text: string, lowercase?: boolean) =>
	lowercase ? text.toLowerCase() : text;

export const ComponentLink: FunctionalComponent<ComponentLinkProps> = ({
	tag,
	text,
	lowercase,
	openingSlash = true
}) => (
	<a {...href(getComponentRoute(tag, openingSlash))}>
		{getText(
			text || componentTagsAndNames.find((c) => c.tag === tag)?.name,
			lowercase
		)}
	</a>
);
