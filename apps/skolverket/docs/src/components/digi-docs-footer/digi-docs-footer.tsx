import { Component, h } from '@stencil/core';

@Component({
	tag: 'digi-docs-footer',
	scoped: true
})
export class DigiDocsFooter {
	render() {
		return (
			<footer>
				<digi-page-footer>
					<digi-list-link af-variation="compact" slot="top-first">
						<div slot="heading">
							<h3>Kontakt</h3>
							<p>
								Upplysningtjänst och växel <br />
								<a
									style={{
										'font-size': 'var(--digi--global--typography--font-size--larger)',
										'font-weight':
											'var(--digi--global--typography--font-weight--semibold)',
										color: 'currentColor',
										'text-decoration': 'none'
									}}
									href="tel:08-52733200"
								>
									08-527 332 00
								</a>
							</p>
						</div>
						<li>
							<a href="https://www.skolverket.se/om-oss/kontakta-oss">
								Kontakta Skolverket
							</a>
						</li>
					</digi-list-link>
					<digi-list-link af-variation="compact" slot="top">
						<h3 slot="heading">Om oss</h3>
						<li>
							<a href="https://www.skolverket.se/om-oss/var-verksamhet/webbplatser-och-sociala-kanaler/om-skolverket.se">
								Om webbplatsen
							</a>
						</li>
					</digi-list-link>
					<digi-list-link af-variation="compact" af-layout="inline" slot="bottom">
						<li>
							<a href="https://gitlab.com/arbetsformedlingen/designsystem/digi">
								Designsystemet är licenserat under Apache License 2.0
							</a>
						</li>
					</digi-list-link>
				</digi-page-footer>
			</footer>
		);
	}
}
