import { FunctionalComponent, h, VNode } from '@stencil/core';

interface ComponentDetailsProps {
	example?: VNode[];
	description?: VNode[];
	guidelines?: VNode[];
	showOnlyExample?: boolean;
	preamble?: string;
}

export const ComponentDetails: FunctionalComponent<ComponentDetailsProps> = ({
	example,
	showOnlyExample,
	preamble,
	description,
	guidelines
}) =>
	showOnlyExample ? (
		<div>{example}</div>
	) : (
		<digi-typography
			style={{
				'margin-block-end': 'var(--digi--responsive-grid-gutter)',
				display: 'block'
			}}
		>
			<digi-layout-container>
				<digi-layout-rows>
					{preamble && (
						<digi-typography-preamble>
							{preamble}
							{!preamble.endsWith('.') && '.'}
						</digi-typography-preamble>
					)}
					<digi-navigation-toc>
						<h2 slot="heading">Innehåll på denna sida</h2>
						{example && (
							<li>
								<a href="#exempel">Exempel</a>
							</li>
						)}
						{description && (
							<li>
								<a href="#beskrivning">Beskrivning</a>
							</li>
						)}
						{guidelines && (
							<li>
								<a href="#riktlinjer">Riktlinjer</a>
							</li>
						)}
					</digi-navigation-toc>
					{example && (
						<article>
							<h2 id="exempel">Exempel</h2>
							{example}
						</article>
					)}
					{description && (
						<article>
							<h2 id="beskrivning">Beskrivning</h2>
							{description}
						</article>
					)}
					{guidelines && (
						<article>
							<h2 id="riktlinjer">Riktlinjer</h2>
							<digi-layout-rows>{guidelines}</digi-layout-rows>
						</article>
					)}
				</digi-layout-rows>
			</digi-layout-container>
		</digi-typography>
	);
