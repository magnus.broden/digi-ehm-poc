import { FunctionalComponent, h, VNode } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { TypographyVariation } from '@digi/skolverket';
import { Tocify } from '@digi/skolverket-docs/components/Tocify';

interface ArticlePageProps {
	heading: string;
	preamble?: string;
	head?: VNode[];
	extras?: VNode[];
}

export const ArticlePage: FunctionalComponent<ArticlePageProps> = (
	{ heading, preamble, head },
	children
) => (
	<article
		style={{ 'padding-block': 'calc(var(--digi--responsive-grid-gutter) * 2)' }}
	>
		{head && <Helmet>{head}</Helmet>}
		<digi-layout-container>
			<digi-typography afVariation={TypographyVariation.LARGE}>
				<h1>{heading}</h1>
				{preamble && (
					<digi-typography-preamble>
						{preamble}
						{!preamble.endsWith('.') && '.'}
					</digi-typography-preamble>
				)}
				<Tocify>{children}</Tocify>
			</digi-typography>
		</digi-layout-container>
	</article>
);
