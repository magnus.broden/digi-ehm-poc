export enum TokenFormat {
  SASS = 'sass',
  CUSTOM_PROPERTY = 'custom-property',
  RGB = 'rgb'
}