import { newSpecPage } from '@stencil/core/testing';
import { ColorTokenCards } from './digi-docs-color-token-cards';

describe('digi-docs-color-token-cards', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [ColorTokenCards],
      html: '<digi-docs-color-token-cards></digi-docs-color-token-cards>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-color-token-cards>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-color-token-cards>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [ColorTokenCards],
      html: `<digi-docs-color-token-cards first="Stencil" last="'Don't call me a framework' JS"></digi-docs-color-token-cards>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-color-token-cards first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-color-token-cards>
    `);
  });
});
