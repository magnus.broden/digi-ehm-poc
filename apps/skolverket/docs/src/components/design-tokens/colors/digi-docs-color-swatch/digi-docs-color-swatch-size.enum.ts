export enum DocsColorSwatchSize {
    S = 's',
    M = 'm',
    L = 'l'
}