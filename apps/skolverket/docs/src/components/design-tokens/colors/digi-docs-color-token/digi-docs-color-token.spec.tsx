import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsColorToken } from './digi-docs-color-token';

describe('digi-docs-color-token', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorToken],
      html: '<digi-docs-color-token></digi-docs-color-token>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-color-token>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-color-token>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorToken],
      html: `<digi-docs-color-token first="Stencil" last="'Don't call me a framework' JS"></digi-docs-color-token>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-color-token first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-color-token>
    `);
  });
});
