import { FunctionalComponent, h } from '@stencil/core';
import { slugify } from 'libs/shared/utils/src';

/**
 * Creates a Navigation TOC (Table of Contents) component from article markup, and adds ids to the headings.
 * NOTE: This only works on headings with a single text node inside. That is, no nested elements.
 */
export const Tocify: FunctionalComponent = (_, children, utils) => {
	let headings = [];

	utils.forEach(children, (child, i) =>
		child.vtag === 'h2'
			? utils.forEach([child.vchildren[0]], (subchild) => {
					headings = [...headings, { id: i, text: subchild.vtext }];
			  })
			: null
	);

	return [
		<Toc headings={headings} />,
		utils.map(children, (child, i) => ({
			...child,
			vattrs:
				child.vtag === 'h2'
					? {
							...child.vattrs,
							id: slugify(headings.find((heading) => heading.id === i).text)
					  }
					: child.vattrs
		}))
	];
};

const Toc = ({ headings }) => {
	return headings.length > 0 ? (
		<digi-navigation-toc>
			<h2 slot="heading">Innehåll på denna sida</h2>
			{headings.map((heading) => (
				<li>
					<a href={`#${slugify(heading.text)}`}>{heading.text}</a>
				</li>
			))}
		</digi-navigation-toc>
	) : null;
};
