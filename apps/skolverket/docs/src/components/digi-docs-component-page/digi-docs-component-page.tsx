import { Component, h, Prop, State, Watch } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { href } from 'stencil-router-v2';
import { getComponentRoute } from '@digi/skolverket-docs/global/routes/helpers';
import { LayoutBlockVariation, ListLinkLayout } from '@digi/skolverket';
import { ComponentTag } from '@digi/skolverket-docs/global/routes/componentTagsAndNames.generated';

@Component({
	tag: 'digi-docs-component-page',
	scoped: true,
	styles: `
		.digi-docs-component-page-details {
			display: block;
			padding-block-end: var(--digi--responsive-grid-gutter);
		}
	`
})
export class DigiDocsComponentPage {
	@Prop() tag: ComponentTag;
	@Prop() category: string;
	@Prop() name: string;
	@Prop() tab: 'oversikt' | 'api' = 'oversikt';
	@State() componentDetailTag: string;

	componentWillLoad() {
		this.setComponentDetailTag();
	}

	@Watch('tag')
	setComponentDetailTag() {
		this.componentDetailTag = `${this.tag}-details`;
	}

	render() {
		return (
			<article class="digi-docs-component-page">
				<Helmet>
					<title>{this.name}</title>
				</Helmet>
				<digi-layout-rows>
					<digi-layout-block afMarginTop>
						<digi-typography>
							<h1
								style={{ 'padding-block-start': 'var(--digi--responsive-grid-gutter)' }}
							>
								{this.name} ({this.tag})
							</h1>
						</digi-typography>
					</digi-layout-block>
					<digi-layout-block
						afVerticalPadding
						afVariation={LayoutBlockVariation.SECONDARY}
					>
						<digi-list-link af-layout={ListLinkLayout.INLINE}>
							<li>
								<a
									{...href(getComponentRoute(this.tag, true))}
									aria-current={this.tab === 'oversikt' ? 'page' : null}
								>
									Översikt
								</a>
							</li>
							<li>
								<a
									{...href(`${getComponentRoute(this.tag, true)}/api`)}
									aria-current={this.tab === 'api' ? 'page' : null}
								>
									Kod
								</a>
							</li>
						</digi-list-link>
					</digi-layout-block>
					{this.tab === 'oversikt' && <this.componentDetailTag />}
					{this.tab === 'api' && <digi-docs-component-api tag={this.tag} />}
				</digi-layout-rows>
			</article>
		);
	}
}
