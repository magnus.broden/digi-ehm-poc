import { Component, Element, h, Prop, State } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';

@Component({
	tag: 'digi-docs-page-layout',
	styleUrl: 'digi-docs-page-layout.scss'
})
export class DigiDocsPageLayout {
	@Element() hostElement: HTMLStencilElement;

	@Prop() afEditBaseUrl: string =
		'https://bitbucket.arbetsformedlingen.se/projects/DIGI/repos/digi-monorepo/browse/apps/docs/src/';
	@Prop() afEditHref: string;

	@Prop() afPageHeading: string; // @Prop() afPageHeading !: string;
	@State() hasPreamble: boolean;

	setHasPreamble() {
		this.hasPreamble = !!this.hostElement.querySelector('[slot="preamble"]');
	}

	componentWillLoad() {
		this.setHasPreamble();
	}

	componentWillUpdate() {
		this.setHasPreamble();
	}

	render() {
		return (
			<digi-typography>
				<section class="digi-docs-page-layout__content">
					{this.afPageHeading && <h1>{this.afPageHeading}</h1>}
					{this.hasPreamble && (
						<digi-typography-preamble>
							<slot name="preamble"></slot>
						</digi-typography-preamble>
					)}

					<slot></slot>
				</section>
			</digi-typography>
		);
	}
}
