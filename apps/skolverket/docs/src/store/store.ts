import { createStore } from '@stencil/store';
import { components } from '@digi/skolverket-docs/data/componentData';

const { state, onChange } = createStore({
	components: components,
	routerRoot: '/',
	activeComponent: null,
	activeNav: false,
	router: {
		activePath: '',
		oldPaths: [],
		newPaths: []
	}
});

onChange('routerRoot', (value) => {
	// console.log('change routerRoot', value);
	state.routerRoot = value;
});

onChange('activeComponent', (value) => {
	console.log('new active component', value);
});

onChange('activeNav', (/*value*/) => {
	// console.log('Nav has changed', value)
});

export default state;
