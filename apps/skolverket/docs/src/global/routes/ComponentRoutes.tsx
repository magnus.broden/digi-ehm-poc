import { Fragment, h } from '@stencil/core';
import { Route } from 'stencil-router-v2';
import { componentTagsAndNames } from './componentTagsAndNames.generated';
import { componentCategoryMap } from '@digi/skolverket-docs/data/categoryData';
import { getComponentRoute } from './helpers';

/**
 * Create predefined routes for all components in the project.
 */
export const ComponentRoutes = () => {
	return componentTagsAndNames.map(({ tag, name }) => (
		<Fragment>
			<Route
				path={getComponentRoute(tag, true)}
				render={(_) => (
					<digi-docs-component-page
						tag={tag}
						name={name}
						tab="oversikt"
						category={componentCategoryMap[tag] || 'ovrigt'}
					/>
				)}
			/>
			<Route
				path={`${getComponentRoute(tag, true)}/api`}
				render={(_) => (
					<digi-docs-component-page
						tag={tag}
						name={name}
						tab="api"
						category={componentCategoryMap[tag] || 'ovrigt'}
					/>
				)}
			/>
		</Fragment>
	));
};
