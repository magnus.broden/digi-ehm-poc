import { h } from '@stencil/core';
import { match, Route } from 'stencil-router-v2';
import { pageRoutes } from './pageRoutes.generated';

export const PageRoutes = () => {
	return pageRoutes.map(({ route, tag: PageComponent }) =>
		route.includes(':') ? (
			<Route
				path={match(route)}
				render={(props) => <PageComponent {...props} />}
			/>
		) : (
			<Route path={route}>
				<PageComponent />
			</Route>
		)
	);
};
