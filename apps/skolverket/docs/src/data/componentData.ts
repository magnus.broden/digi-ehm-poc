import { componentCategoryMap } from './categoryData';
import { Category } from '@digi/taxonomies';
import _componentData from '../../../../../dist/libs/skolverket/docs.json';

// Core components that are not yet skolverketified
const blacklist = [
	'digi-calendar',
	'digi-calendar-week-view',
	'digi-form-filter',
	'digi-link-internal',
	'digi-link-external',
	'digi-navigation-context-menu',
	'digi-navigation-context-menu-item',
	'digi-navigation-sidebar',
	'digi-navigation-sidebar-button',
	'digi-navigation-vertical-menu',
	'digi-navigation-vertical-menu-item',
	'digi-progress-step',
	'digi-progress-steps',
	'digi-progressbar',
	'digi-tag',
	'digi-typography-meta'
];

export const componentData = _componentData;
export const components = componentData.components
	.filter((component) => !blacklist.includes(component.tag))
	.map((component) => {
		return {
			...component,
			category: component.tag.includes('digi-icon')
				? Category.ICON
				: componentCategoryMap[component.tag] || Category.UNCATEGORIZED
		};
	});
