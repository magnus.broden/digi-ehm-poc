# Changelog - Digi Docs

- `@digi/skolverket-angular`
  - Uppdaterat peer-dependencies till Angular 15

## 2022-02-17

### Ändrat

- `Texter`
  - Mindre uppdateringar i texter och rubriker för Tillgänglighetsredogörelsen och Process för tillgänglighetsredogörelse.

## 2022-02-02

### Lagt till

- `Lagkrav och riktlinjer`
  - Ny sida för Lagkrav och riktlinjer under Tillgänglighet och design.

### Ändrat

- `assetPath`

  - Lagt till assetPath för bilder så att de laddas korrekt i sitevision.

- `Font weights`

  - Lagt till att ladda typsnitt Open Sans med font weights så att de syns korrekt i sitevision.

- `Texter`

  - Ändrat diverse texter på sidorna: Grafisk profil > Grafik.

- `Tillgänglighetsredogörelsen`
  - Uppdaterat tillgänglighetsredogörelsen efter ny granskning.

## 2022-01-20

### Ändrat

- `Digi Core`
  - Uppdaterat dokumentationen för att koppla till Digi Core Angular. Har uppdaterat enums m.m.

## 2022-01-13

### Ändrat

- `footer`
  - Footer tar nu hela bredden, lagt till mail-länk.

## 2022-01-12

### Ändrat

- `layout`
  - Ändrat layout på docs så att center har en maxbredd enlight container och att aside ligger helt till vänster. För bästa layout använd inte layout-block och layout-container i onödan. Layout-container används för innehåll på en sida för att få bredd, layout-block kan användas för att få fullbredd på infokort.

## 2022-01-11

### Ändrat

- `Digi Core Angular`
  - Dokumentationen är uppdaterad efter ändringar i bl.a. Digi Core och enums.

## 2021-12-23

### Nytt

- `Digi Core Angular`
  - Ny dokumentation för biblioteket Digi Core Angular.

## 2021-06-11

### Nytt

- `digi-calendar`
  - Ny dokumentation
- `digi-navigation-pagination`
  - Ny dokumentation
- `digi-navigation-context-menu`
  - Ny dokumentation
- `digi-navigation-context-menu-item`
  - Ny dokumentation
- `digi-navigation-tabs`
  - Ny dokumentation
- `digi-navigation-tab`
  - Ny dokumentation
- `digi-typography-time`
  - Ny dokumentation
- `digi-navigation-breadcrumbs`
  - Ny dokumentation

### Ändrat

- Lagt till `digi-navigation-tabs` för navigering mellan flikarna på komponentsidorna
