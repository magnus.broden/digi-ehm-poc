# Skolverket

Digi är Skolverkets och Skolverkets gemensamma designsystem. Här finns bland annat komponentbibliotek, design tokens, funktionalitet, api:er och annat som hjälper dig att bygga digitala tjänster som följer våra gemensamma riktlinjer. Repot är ett monorepo genererat med [Nx](https://nx.dev). Besök [Skolverkets dokumentationssida](https://designsystem.skolverket.se/) för att se vilka komponenter som ingår.

## Kom igång

### Förutsättningar

- [node (och npm)](https://nodejs.org/en/) i din utvecklingsmiljö. Aktuell nodeversion finns i `.nvmrc`

### Installera

- `nvm use` om du använder nvm. Annars får byta till rätt nodeversion på annat sätt.
- `npm i` i roten av projektet.

### Skolverket

Skolverket bygger på @digi/core som är den centrala delen i designsystemet. I @digi/core skapas alla komponenter som delas mellan de olika projekten, skolverket, arbetsförmedlingen etc.

#### Börja jobba i Skolverkets dokumentationsapp

För att bygga dokumentationsappen.

- kör `npm run build skolverket-docs`.

När du bygger projektet så läggs den i `dist/apps/skolverket/docs`.

För att starta dokumentationen.

- kör `npm run start skolverket-docs`.

#### Skapa ny sida i dokumentationen

Alla sidor förutom detaljsidorna för komponenterna använder sig av en filbaserad routing (i stil med Nextjs och liknande ramverk), det vill säga namnen på mappar och filer kommer att generera motsvarande sökvägar i url:en. Det går även att använda sig av klamrar i filnamnet för att skapa dynamiska routes.

Gör såhär:

- Skapa mappar och filer i `/pages` som motsvarar den url du vill ha till sidan. Alla `.tsx`-filer kommer antas vara sidkomponenter. Om komponenten heter `index.tsx` så slutar routen vid närmast överliggande mapp (`sida/undersida/index.tsx` blir `/sida/undersida/`), om filen heter något annat så blir det en del av routen (`sida/undersida.tsx` blir också `/sida/undersida`).
- Kör `npm run generate-page-routes:skolverket` för att generera nya routes
- Klart

Några exempel:

- Filen `pages/kom-igang/jobba-med-kod/index.tsx` skulle motsvara routen `/kom-igang/jobba-med-kod`
- Filen `pages/kom-igang/jobba-med-kod.tsx` skulle också motsvara routen `kom-igang/jobba-med-kod`
- Filen `pages/nyheter/[id].tsx` skulle motsvara routen `/nyheter/:id`
- Filen `pages/nyheter/[category]/[title].tsx` skulle motsvara routen `/nyheter/:category/:title`
- Filen `pages/nyheter/[category]/[title]/index.tsx` skulle också motsvara routen `/nyheter/:category/:title`
- Filen `pages/nyheter/[category][title].tsx` skulle även den motsvara routen `/nyheter/:category/:title`

#### Skapa en dokumentationsida för en komponent

Alla detaljsidor för komponenterna finns i mappen `/details`. Varje komponent har en motsvarande fil här.

- Skapa en ny fil som heter `digi-{komponentnamn}-details.tsx` (en bra idé är att utgå från någon av de existerande)
- Lägg till komponenten i `/data/categoryData.ts` så att den syns i huvudmenyn och sidan får rätt route
- Kör `npm run generate-component-routes:skolverket` för att generera nya routes

[Skolverket libs](../../libs/skolverket/README.md)
