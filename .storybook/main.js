module.exports = {
  stories: [],
  addons: ['@storybook/addon-knobs/dist/register'],
  core: {
    builder: "webpack5"
  },
  managerWebpack: (config, options) => {
    options.cache.set = () => Promise.resolve();
    return config;
},
  webpackFinal: async config => {
    config.module.rules.push({
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
      include: path.resolve(__dirname, '../')
    });
    return config;
  }
};