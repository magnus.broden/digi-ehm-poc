import { sortStories } from './util/story-helpers';
import { addParameters } from '@storybook/html';
import '@storybook/addon-console';

import { setStencilDocJson } from '@pxtrn/storybook-addon-docs-stencil';
import docJson from '../../../../dist/libs/arbetsformedlingen/docs.json';

// @ts-ignore
if (docJson) setStencilDocJson(docJson);

export const parameters = {
	controls: { hideNoControlsWarning: true }
};

const SORT_ORDER = {
	Documentation: ['Welcome']
};

addParameters({
	options: {
		storySort: sortStories(SORT_ORDER)
	}
});
