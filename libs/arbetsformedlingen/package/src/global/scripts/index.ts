import { setMode } from "@stencil/core";

export default () => {
  const themeResolver = (elm: HTMLElement) => {
    // const theme = (elm as any).theme || elm.getAttribute('theme');
    const theme = document.querySelector('body').getAttribute('theme')
    console.log('themeResolver', theme)
    if (theme) {
      return theme;
    } else if (elm.parentElement) {
      console.log('else if', elm.parentElement)
      return 'core';
    } else {
      console.log('else', elm.parentElement)
      return themeResolver(elm.parentElement);
    }
  };

  setMode((elm: HTMLElement) => {
    return themeResolver(elm);
  })
};