# digi-expandable-faq

This is a expandable faq component.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import {  ExpandableFaqHeadingLevel} from '@digi/core/dist/enum/expandable-faq-heading-level.enum';
import {  ExpandableFaqHeadingVariation} from '@digi/core/dist/enum/expandable-faq-heading-variation.enum';
```

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                          | Type                                                                                                                                                                                           | Default                          |
| ---------------- | ------------------ | ---------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| `afHeading`      | `af-heading`       | Rubrikens vanliga frågor                             | `string`                                                                                                                                                                                       | `undefined`                      |
| `afHeadingLevel` | `af-heading-level` | Sätter rubriknivå. 'h2' är förvalt.                  | `ExpandableFaqHeadingLevel.H1 \| ExpandableFaqHeadingLevel.H2 \| ExpandableFaqHeadingLevel.H3 \| ExpandableFaqHeadingLevel.H4 \| ExpandableFaqHeadingLevel.H5 \| ExpandableFaqHeadingLevel.H6` | `ExpandableFaqHeadingLevel.H2`   |
| `afVariation`    | `af-variation`     | Sätter variant. Kan vara 'Primary' eller 'Secondary' | `ExpandableFaqVariation.PRIMARY \| ExpandableFaqVariation.SECONDARY \| ExpandableFaqVariation.TERTIARY`                                                                                        | `ExpandableFaqVariation.PRIMARY` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
