import { newSpecPage } from '@stencil/core/testing';
import { ExpandableFaqItem } from './expandable-faq-item';


describe('digi-expandable-faq-item', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [ExpandableFaqItem],
      html: '<digi-expandable-faq-item></digi-expandable-faq-item>'
    });
    expect(root).toEqualHtml(`
      <digi-expandable-faq-item>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-expandable-faq-item>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [ExpandableFaqItem],
      html: `<digi-expandable-faq-item first="Stencil" last="'Don't call me a framework' JS"></digi-expandable-faq-item>`
    });
    expect(root).toEqualHtml(`
      <digi-expandable-faq-item first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-expandable-faq-item>
    `);
  });
});
