# digi-expandable-faq-item

This is a expandable faq component.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import {   ExpandableFaqItemHeadingLevel } from '@digi/core/dist/enum/expandable-faq-item-heading-level.enum';
import {   ExpandableFaqItemVariation} from '@digi/core/dist/enumexpandable-faq-item-variation.enum.enum';
```

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                                                          | Type                                                                                                                                                                                                                   | Default                                         |
| ---------------- | ------------------ | ---------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------- |
| `afAnimation`    | `af-animation`     | Ställer in ifall komponenten ska ha en animation vid utfällning. Animation är påslagen som standard. | `boolean`                                                                                                                                                                                                              | `true`                                          |
| `afExpanded`     | `af-expanded`      | Sätter läge på FAQ-frågan om den ska vara expanderad eller ej.                                       | `boolean`                                                                                                                                                                                                              | `false`                                         |
| `afHeading`      | `af-heading`       | Rubrikens vanliga frågor                                                                             | `string`                                                                                                                                                                                                               | `undefined`                                     |
| `afHeadingLevel` | `af-heading-level` | Sätter rubriknivå. 'h3' är förvalt.                                                                  | `ExpandableFaqItemHeadingLevel.H1 \| ExpandableFaqItemHeadingLevel.H2 \| ExpandableFaqItemHeadingLevel.H3 \| ExpandableFaqItemHeadingLevel.H4 \| ExpandableFaqItemHeadingLevel.H5 \| ExpandableFaqItemHeadingLevel.H6` | `ExpandableFaqItemHeadingLevel.H3`              |
| `afId`           | `af-id`            | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                | `string`                                                                                                                                                                                                               | `randomIdGenerator('digi-expandable-faq-item')` |
| `afVariation`    | `af-variation`     | Sätter variant. Kan vara 'Primary' eller 'Secondary'                                                 | `ExpandableFaqItemVariation.PRIMARY \| ExpandableFaqItemVariation.SECONDARY \| ExpandableFaqItemVariation.TERTIARY`                                                                                                    | `ExpandableFaqItemVariation.PRIMARY`            |


## Events

| Event       | Description                       | Type                      |
| ----------- | --------------------------------- | ------------------------- |
| `afOnClick` | Buttonelementets 'onclick'-event. | `CustomEvent<MouseEvent>` |


## CSS Custom Properties

| Name                                                               | Description                                                |
| ------------------------------------------------------------------ | ---------------------------------------------------------- |
| `--digi--expandable-faq-item--border-bottom-width`                 | var(--digi--border-width--secondary);                      |
| `--digi--expandable-faq-item--border-width`                        | var(--digi--border-width--secondary);                      |
| `--digi--expandable-faq-item--content--padding`                    | var(--digi--gutter--largest-2);                            |
| `--digi--expandable-faq-item--content--transition`                 | ease-in-out var(--digi--animation--duration--base) height; |
| `--digi--expandable-faq-item--header--background-color--primary`   | var(--digi--color--background--neutral-5);                 |
| `--digi--expandable-faq-item--header--background-color--secondary` | var(--digi--color--background--secondary);                 |
| `--digi--expandable-faq-item--header--background-color--tertiary`  | var(--digi--color--background--primary);                   |
| `--digi--expandable-faq-item--header--font-size`                   | var(--digi--typography--preamble--font-size--desktop);     |
| `--digi--expandable-faq-item--header--font-weight`                 | var(--digi--typography--preamble--font-weight--desktop);   |
| `--digi--expandable-faq-item--header--toggle-icon--transition`     | ease-in-out var(--digi--animation--duration--base) all;    |
| `--digi--expandable-faq-item--icon--margin-right`                  | var(--digi--margin--medium);                               |
| `--digi--expandable-faq-item--icon--size`                          | 0.875rem;                                                  |


## Dependencies

### Depends on

- [digi-icon](../../../__core/_icon/icon)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-expandable-faq-item --> digi-icon
  digi-expandable-faq-item --> digi-typography
  style digi-expandable-faq-item fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
