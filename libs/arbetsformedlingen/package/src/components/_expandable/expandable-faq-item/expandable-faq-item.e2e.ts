import { newE2EPage } from '@stencil/core/testing';

describe('digi-expandable-faq-item', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<digi-expandable-faq-item></digi-expandable-faq-item>');
    const element = await page.find('digi-expandable-faq-item');
    expect(element).toHaveClass('hydrated');
  });

  it('renders changes to the name data', async () => {
    const page = await newE2EPage();

    await page.setContent('<digi-expandable-faq-item></digi-expandable-faq-item>');
    const component = await page.find('digi-expandable-faq-item');
    const element = await page.find('digi-expandable-faq-item>>> div');
    expect(element.textContent).toEqual(`Hello, World! I'm `);

    component.setProperty('first', 'James');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James`);

    component.setProperty('last', 'Quincy');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

    component.setProperty('middle', 'Earl');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
  });
});
