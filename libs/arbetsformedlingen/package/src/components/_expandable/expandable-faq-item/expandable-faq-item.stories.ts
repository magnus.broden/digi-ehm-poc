import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ExpandableFaqItemHeadingLevel } from './expandable-faq-item-heading-level.enum';
import { ExpandableFaqItemVariation } from './expandable-faq-item-variation.enum';

export default {
	title: 'expandable/digi-expandable-faq-item',
	argTypes: {
		'af-heading-level': enumSelect(ExpandableFaqItemHeadingLevel),
		'af-variation': enumSelect(ExpandableFaqItemVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-expandable-faq-item',
	'af-heading': '',
	'af-heading-level': ExpandableFaqItemHeadingLevel.H3,
	'af-expanded': false,
	'af-variation': ExpandableFaqItemVariation.PRIMARY,
	'af-id': null,
	/* html */
	children: '<p>FAQ content</p>'
};
