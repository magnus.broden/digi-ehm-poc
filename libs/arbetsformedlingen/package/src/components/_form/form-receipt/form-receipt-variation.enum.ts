export enum FormReceiptVariation {
  FULLWIDTH = 'fullwidth',
  GRID = 'grid'
}
