import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-receipt', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-receipt></digi-form-receipt>');

    const element = await page.find('digi-form-receipt');
    expect(element).toHaveClass('hydrated');
  });
});
