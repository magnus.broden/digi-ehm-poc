# digi-form-receipt

This is a receipt card. It exists in a number of different variations.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormReceipVariation, FormReceiptType, FormReceiptHeadingLevel } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                      | Type                                                                                                                                                                               | Default                          |
| ---------------- | ------------------ | ---------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| `afHeadingLevel` | `af-heading-level` | Sätter textens vikt (h1-h6)                                      | `FormReceiptHeadingLevel.H1 \| FormReceiptHeadingLevel.H2 \| FormReceiptHeadingLevel.H3 \| FormReceiptHeadingLevel.H4 \| FormReceiptHeadingLevel.H5 \| FormReceiptHeadingLevel.H6` | `FormReceiptHeadingLevel.H2`     |
| `afText`         | `af-text`          | Sätter texten som ska visas                                      | `string`                                                                                                                                                                           | `""`                             |
| `afType`         | `af-type`          | Sätter textsnittsjusteringen. Kan vara 'center' eller 'start'.   | `FormReceiptType.CENTER \| FormReceiptType.START`                                                                                                                                  | `FormReceiptType.CENTER`         |
| `afVariation`    | `af-variation`     | Sätter variationen på kortet. Kan vara 'fullwidth' eller 'grid'. | `FormReceiptVariation.FULLWIDTH \| FormReceiptVariation.GRID`                                                                                                                      | `FormReceiptVariation.FULLWIDTH` |


## CSS Custom Properties

| Name                                             | Description                                                   |
| ------------------------------------------------ | ------------------------------------------------------------- |
| `--digi--receipt--background-color`              | var(--digi--color--background--neutral-5);                    |
| `--digi--receipt--heading--color`                | var(--digi--color--text--primary);                            |
| `--digi--receipt--heading--font-size`            | var(--digi--typography--heading-2--font-size--mobile);        |
| `--digi--receipt--heading--font-size--desktop`   | var(--digi--typography--heading-1--font-size--desktop-large); |
| `--digi--receipt--heading--font-weight`          | var(--digi--typography--heading-2--font-weight--mobile);      |
| `--digi--receipt--heading--font-weight--desktop` | var(--digi--typography--heading-1--font-weight--desktop);     |
| `--digi--receipt--margin`                        | var(--digi--padding--small);                                  |
| `--digi--receipt--padding-bottom`                | var(--digi--padding--largest);                                |
| `--digi--receipt--padding-bottom--desktop`       | var(--digi--padding--largest-3);                              |
| `--digi--receipt--padding-horizontal`            | var(--digi--padding--medium);                                 |
| `--digi--receipt--padding-top`                   | var(--digi--padding--large);                                  |
| `--digi--receipt--padding-top--desktop`          | var(--digi--padding--largest-2);                              |


## Dependencies

### Depends on

- [digi-layout-block](../../../__core/_layout/layout-block)

### Graph
```mermaid
graph TD;
  digi-form-receipt --> digi-layout-block
  digi-layout-block --> digi-layout-container
  style digi-form-receipt fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
