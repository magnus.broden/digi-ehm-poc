import {
  Component,
  Prop,
  h,
} from '@stencil/core';
import { FormReceiptHeadingLevel } from './form-receipt-heading-level.enum';
import { FormReceiptVariation } from './form-receipt-variation.enum';
import { FormReceiptType } from './form-receipt-type.enum';
import { LayoutBlockVariation } from '../../../enums-core';

/**
 * @enums FormReceiptVariation - receipt-variation.enum.ts
 * @enums FormReceiptType - receipt-type.enum.ts
 * @enums FormReceiptHeadingLevel - receipt-heading-level.enum.ts
 * @swedishName Kvittens
 */
@Component({
  tag: 'digi-form-receipt',
  styleUrls: ['form-receipt.scss'],
  scoped: true,
})
export class FormReceipt {

  /**
   * Sätter texten som ska visas
   * @en Set text to be shown
   */
  @Prop() afText: string = "";

  /**
   * Sätter variationen på kortet. Kan vara 'fullwidth' eller 'grid'.
   * @en Set card variation. Can be 'fullwidth' or 'grid'.
   */
  @Prop() afVariation: FormReceiptVariation = FormReceiptVariation.FULLWIDTH;

  /**
   * Sätter textsnittsjusteringen. Kan vara 'center' eller 'start'.
   * @en Set font alignment. Can be 'center' or 'start'.
   */
  @Prop() afType: FormReceiptType = FormReceiptType.CENTER;

  /**
   * Sätter textens vikt (h1-h6)
   * @en Set text level (h1-h6)
   */
  @Prop() afHeadingLevel: FormReceiptHeadingLevel = FormReceiptHeadingLevel.H2;

  get cssModifiers() {
    return {
      'digi-form-receipt--fullwidth': this.afVariation === FormReceiptVariation.FULLWIDTH,
      'digi-form-receipt--grid': this.afVariation === FormReceiptVariation.GRID,
      'digi-form-receipt--center': this.afType === FormReceiptType.CENTER,
      'digi-form-receipt--start': this.afType === FormReceiptType.START,
    };
  }

  get content() {
    return (
      <div class="digi-form-receipt__content">
        <svg class="digi-form-receipt__icon" aria-hidden="true" version="1.1" viewBox="0 0 35 29">
          <title>Check mark</title>
          <g fill="none" fill-rule="evenodd">
            <g transform="translate(-170 -32)">
              <g transform="translate(170 32.325)">
                <g transform="translate(0 .77495)">
                  <mask id="b" fill="white">
                    <polygon points="0 0 26.685 0 26.685 27.689 0 27.689"/>
                  </mask>
                  <path d="m13.711 27.689c-7.5602 0-13.711-6.2103-13.711-13.844 0-7.6345 6.151-13.845 13.711-13.845 3.1693 0 6.2611 1.1179 8.7048 3.1478l-1.5894 1.9509c-1.9972-1.6592-4.5241-2.5724-7.1155-2.5724-6.181 0-11.209 5.0773-11.209 11.319 0 6.2406 5.0283 11.318 11.209 11.318 4.7924 0 9.0551-3.0733 10.607-7.6478l2.3668 0.81853c-1.8983 5.5958-7.1124 9.3556-12.974 9.3556" fill="#333" mask="url(#b)"/>
                </g>
                <g transform="translate(4.2177)">
                  <mask id="a" fill="white">
                    <polygon points="0 0 30.425 0 30.425 23.496 0 23.496"/>
                  </mask>
                  <path d="m30.191 3.2545-19.95 20.007c-0.31227 0.31253-0.81758 0.31253-1.1292 0l-8.878-8.9033c-0.31164-0.31316-0.31164-0.81992 0-1.1324l1.8818-1.8878c0.31164-0.31253 0.81758-0.31253 1.1292 0l6.4315 6.4499 17.504-17.553c0.31164-0.31316 0.81695-0.31316 1.1292 0l1.8818 1.8872c0.31164 0.31253 0.31164 0.81929 0 1.1324" fill="#95C13E" mask="url(#a)"/>
                </g>
              </g>
            </g>
          </g>
        </svg>
        <this.afHeadingLevel class="digi-form-receipt__heading">
          { this.afText }
        </this.afHeadingLevel>
      </div>
    );
  }

  render() {
    return (
      <div
        class={{
          'digi-form-receipt': true,
          ...this.cssModifiers,
        }}
      >
        { this.afVariation === FormReceiptVariation.FULLWIDTH &&
          <digi-layout-block afVariation={LayoutBlockVariation.TRANSPARENT}>
            { this.content }
          </digi-layout-block>
        }
        { this.afVariation !== FormReceiptVariation.FULLWIDTH &&
          this.content
        }
      </div>
    );
  }
}
