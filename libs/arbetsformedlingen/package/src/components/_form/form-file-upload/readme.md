# digi-form-file-upload

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute                      | Description                                                                       | Type                                                                                                                                                                                                 | Default                                      |
| -------------------------- | ------------------------------ | --------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| `afAnnounceIfOptional`     | `af-announce-if-optional`      | Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria. | `boolean`                                                                                                                                                                                            | `false`                                      |
| `afAnnounceIfOptionalText` | `af-announce-if-optional-text` | Sätter text för afAnnounceIfOptional.                                             | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileMaxSize`            | `af-file-max-size`             | Sätter en maximal filstorlek i MB, 10MB är standard.                              | `number`                                                                                                                                                                                             | `10`                                         |
| `afFileTypes` _(required)_ | `af-file-types`                | Sätter attributet 'accept'. Använd för att limitera accepterade filtyper          | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afHeadingFiles`           | `af-heading-files`             | Sätt rubrik för uppladdade filer.                                                 | `string`                                                                                                                                                                                             | `'Uppladdade filer'`                         |
| `afHeadingLevel`           | `af-heading-level`             | Sätt rubrikens vikt. 'h2' är förvalt.                                             | `FormFileUploadHeadingLevel.H1 \| FormFileUploadHeadingLevel.H2 \| FormFileUploadHeadingLevel.H3 \| FormFileUploadHeadingLevel.H4 \| FormFileUploadHeadingLevel.H5 \| FormFileUploadHeadingLevel.H6` | `FormFileUploadHeadingLevel.H2`              |
| `afId`                     | `af-id`                        | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.             | `string`                                                                                                                                                                                             | `randomIdGenerator('digi-form-file-upload')` |
| `afLabel`                  | `af-label`                     | Texten till labelelementet                                                        | `string`                                                                                                                                                                                             | `'Ladda upp fil'`                            |
| `afLabelDescription`       | `af-label-description`         | Valfri beskrivande text                                                           | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afMaxFiles`               | `af-max-files`                 | Sätter maximalt antal filer man kan ladda upp                                     | `number`                                                                                                                                                                                             | `undefined`                                  |
| `afName`                   | `af-name`                      | Sätter attributet 'name'.                                                         | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afRequired`               | `af-required`                  | Sätter attributet 'required'.                                                     | `boolean`                                                                                                                                                                                            | `undefined`                                  |
| `afRequiredText`           | `af-required-text`             | Sätter text för afRequired.                                                       | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afUploadBtnText`          | `af-upload-btn-text`           | Sätter ladda upp knappens text.                                                   | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afValidation`             | `af-validation`                | Sätter antivirus scan status. Kan vara 'enable' eller 'disabled.                  | `FormFileUploadValidation.DISABLED \| FormFileUploadValidation.ENABLED`                                                                                                                              | `FormFileUploadValidation.ENABLED`           |
| `afVariation`              | `af-variation`                 | Sätter variant. Kan vara 'primary', 'secondary' eller 'tertiary'.                 | `FormFileUploadVariation.PRIMARY \| FormFileUploadVariation.SECONDARY \| FormFileUploadVariation.TERTIARY`                                                                                           | `FormFileUploadVariation.PRIMARY`            |


## Events

| Event            | Description                                                                     | Type               |
| ---------------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnCancelFile` | Sänder ut vilken fil som har avbrutis uppladdning                               | `CustomEvent<any>` |
| `afOnReady`      | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |
| `afOnRemoveFile` | Sänder ut vilken fil som tagits bort                                            | `CustomEvent<any>` |
| `afOnRetryFile`  | Sänder ut vilken fil som försöker laddas upp igen                               | `CustomEvent<any>` |
| `afOnUploadFile` | Sänder ut fil vid uppladdning                                                   | `CustomEvent<any>` |


## Methods

### `afMGetAllFiles() => Promise<File[]>`

Få ut alla uppladdade filer

#### Returns

Type: `Promise<File[]>`



### `afMGetFormControlElement() => Promise<HTMLInputElement>`



#### Returns

Type: `Promise<HTMLInputElement>`



### `afMImportFiles(files: File[]) => Promise<void>`

Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt måste innehålla id, status och filen själv.

#### Returns

Type: `Promise<void>`



### `afMValidateFile(scanInfo: any) => Promise<void>`

Validera filens status.

#### Returns

Type: `Promise<void>`




## CSS Custom Properties

| Name                                                              | Description                                                      |
| ----------------------------------------------------------------- | ---------------------------------------------------------------- |
| `--digi--form-file-upload--border-radius--primary--default`       | var(--digi--border-radius--secondary);                           |
| `--digi--form-file-upload--border-radius--secondary--default`     | var(--digi--border-radius--secondary);                           |
| `--digi--form-file-upload--border-radius--tertiary--default`      | var(--digi--border-radius--secondary);                           |
| `--digi--form-file-upload--border-style--primary--default`        | solid;                                                           |
| `--digi--form-file-upload--border-style--secondary--default`      | dashed;                                                          |
| `--digi--form-file-upload--border-style--tertiary--default`       | dashed;                                                          |
| `--digi--form-file-upload--border-width--primary--default`        | var(--digi--border-width--secondary);                            |
| `--digi--form-file-upload--border-width--primary--hover`          | var(--digi--border-width--primary);                              |
| `--digi--form-file-upload--border-width--secondary--default`      | var(--digi--border-width--secondary);                            |
| `--digi--form-file-upload--border-width--secondary--hover`        | var(--digi--border-width--primary);                              |
| `--digi--form-file-upload--border-width--tertiary--default`       | var(--digi--border-width--secondary);                            |
| `--digi--form-file-upload--border-width--tertiary--hover`         | var(--digi--border-width--primary);                              |
| `--digi--form-file-upload--button-text--color--default`           | var(--digi--global--color--cta--link--base);                     |
| `--digi--form-file-upload--button-text--color--hover`             | var(--digi--global--color--function--info--light);               |
| `--digi--form-file-upload--color--background--primary--default`   | var(--digi--color--background--secondary);                       |
| `--digi--form-file-upload--color--background--primary--hover`     | var(--digi--global--color--cta--hover--base);                    |
| `--digi--form-file-upload--color--background--secondary--default` | var(--digi--color--background--secondary);                       |
| `--digi--form-file-upload--color--background--secondary--hover`   | var(--digi--global--color--cta--hover--base);                    |
| `--digi--form-file-upload--color--background--tertiary--default`  | var(--digi--color--background--secondary);                       |
| `--digi--form-file-upload--color--background--tertiary--hover`    | var(--digi--global--color--cta--hover--base);                    |
| `--digi--form-file-upload--color--border--primary--default`       | var(--digi--global--color--neutral--grayscale--darkest);         |
| `--digi--form-file-upload--color--border--primary--hover`         | var(--digi--global--color--cta--hover--base);                    |
| `--digi--form-file-upload--color--border--secondary--default`     | var(--digi--global--color--neutral--grayscale--darkest);         |
| `--digi--form-file-upload--color--border--secondary--hover`       | var(--digi--global--color--cta--hover--base);                    |
| `--digi--form-file-upload--color--border--tertiary--default`      | var(--digi--global--color--neutral--grayscale--darkest);         |
| `--digi--form-file-upload--color--border--tertiary--hover`        | var(--digi--global--color--cta--hover--base);                    |
| `--digi--form-file-upload--font-family`                           | var(--digi--global--typography--font-family--default);           |
| `--digi--form-file-upload--font-size--large`                      | var(--digi--global--typography--font-size--large);               |
| `--digi--form-file-upload--font-size--medium`                     | var(--digi--global--typography--font-size--base);                |
| `--digi--form-file-upload--font-size--small`                      | var(--digi--global--typography--font-size--small);               |
| `--digi--form-file-upload--font-weight`                           | var(--digi--typography--form-file-upload--font-weight--desktop); |
| `--digi--form-file-upload--heading--font-family`                  | var(--digi--global--typography--font-family--default);           |
| `--digi--form-file-upload--heading--font-size`                    | var(--digi--typography--heading-4--font-size--desktop);          |
| `--digi--form-file-upload--heading--font-size--large`             | var(--digi--typography--heading-4--font-size--desktop-large);    |
| `--digi--form-file-upload--heading--font-weight`                  | var(--digi--typography--heading-4--font-weight--desktop);        |
| `--digi--form-file-upload--padding--primary`                      | var(--digi--gutter--medium) var(--digi--gutter--largest);        |
| `--digi--form-file-upload--padding--secondary`                    | var(--digi--gutter--largest) var(--digi--gutter--largest);       |
| `--digi--form-file-upload--padding--tertiary`                     | var(--digi--gutter--largest) var(--digi--gutter--largest-3);     |
| `--digi--form-file-upload--width--primary`                        | max-content;                                                     |
| `--digi--form-file-upload--width--secondary`                      | 300px;                                                           |
| `--digi--form-file-upload--width--tertiary`                       | 100%;                                                            |


## Dependencies

### Depends on

- [digi-form-label](../../../__core/_form/form-label)
- [digi-icon-upload](../../_icon/icon-upload)
- [digi-button](../../../__core/_button/button)
- [digi-icon-paperclip](../../../__core/_icon/icon-paperclip)
- [digi-form-validation-message](../../../__core/_form/form-validation-message)
- [digi-icon-spinner](../../../__core/_icon/icon-spinner)
- [digi-icon-x-button-outline](../../_icon/icon-x-button-outline)
- [digi-icon-notification-succes](../../../__core/_icon/icon-notification-succes)
- [digi-icon-trash](../../../__core/_icon/icon-trash)
- [digi-icon-danger-outline](../../../__core/_icon/icon-danger-outline)
- [digi-icon-update](../../_icon/icon-update)

### Graph
```mermaid
graph TD;
  digi-form-file-upload --> digi-form-label
  digi-form-file-upload --> digi-icon-upload
  digi-form-file-upload --> digi-button
  digi-form-file-upload --> digi-icon-paperclip
  digi-form-file-upload --> digi-form-validation-message
  digi-form-file-upload --> digi-icon-spinner
  digi-form-file-upload --> digi-icon-x-button-outline
  digi-form-file-upload --> digi-icon-notification-succes
  digi-form-file-upload --> digi-icon-trash
  digi-form-file-upload --> digi-icon-danger-outline
  digi-form-file-upload --> digi-icon-update
  digi-form-validation-message --> digi-icon
  style digi-form-file-upload fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
