import {
	Component,
	h,
	Prop,
	State,
	Element,
	Method,
	EventEmitter,
	Event
} from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';

import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormFileUploadVariation } from './form-file-upload-variation.enum';
import { FormFileUploadValidation } from './form-file-upload-validation.enum';
import { ButtonType, ButtonVariation } from '../../../enums-core';
import { FormFileUploadHeadingLevel } from './form-file-upload-heading-level.enum';

/**
 *	@enums FormInputFileType - form-input-file-type.enum.ts
 *  @enums FormInputFileVariation - form-input-file-variation.enum.ts
 * 	@swedishName Filuppladdare
 */
@Component({
	tag: 'digi-form-file-upload',
	styleUrl: 'form-file-upload.scss',
	scoped: true
})
export class FormFileUpload {
	@Element() hostElement: HTMLStencilElement;

	@State() _input: HTMLInputElement;

	@State() files: File[] = [];

	@State() error: boolean = false;

	@State() errorMessage: string;

	@State() fileHover: boolean = false;

	/**
	 * Texten till labelelementet
	 * @en The label text
	 */
	@Prop() afLabel: string = 'Ladda upp fil';

	/**
	 * Valfri beskrivande text
	 * @en A description text
	 */
	@Prop() afLabelDescription: string;

	/**
	 * Sätter variant. Kan vara 'primary', 'secondary' eller 'tertiary'.
	 * @en Set input variation.
	 */
	@Prop() afVariation: FormFileUploadVariation = FormFileUploadVariation.PRIMARY;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-file-upload');

	/** Sätter attributet 'accept'. Använd för att limitera accepterade filtyper
	 * @en Set input field's accept attribute. Use this to limit accepted file types.
	 */
	@Prop() afFileTypes!: string;

	/**
	 * Sätter en maximal filstorlek i MB, 10MB är standard.
	 * @en Set file max size in MB
	 */
	@Prop() afFileMaxSize: number = 10;

	/**
	 * Sätter maximalt antal filer man kan ladda upp
	 * @en Set max number of files you can upload
	 */
	@Prop() afMaxFiles: number;

	/**
	 * Sätter attributet 'name'.
	 * @en Input name attribute
	 */
	@Prop() afName: string;

	/**
	 * Sätter ladda upp knappens text.
	 * @en Set upload buttons text
	 */
	@Prop() afUploadBtnText: string;

	/**
	 * Sätter attributet 'required'.
	 * @en Input required attribute
	 */
	@Prop() afRequired: boolean;

	/**
	 * Sätter text för afRequired.
	 * @en Set text for afRequired.
	 */
	@Prop() afRequiredText: string;

	/**
	 * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
	 * @en Set this to true if the form contains more required fields than optional fields.
	 */
	@Prop() afAnnounceIfOptional = false;

	/**
	 * Sätt rubrik för uppladdade filer.
	 * @en Set heading for uploaded files.
	 */
	@Prop() afHeadingFiles = 'Uppladdade filer';

	/**
	 * Sätter text för afAnnounceIfOptional.
	 * @en Set text for afAnnounceIfOptional.
	 */
	@Prop() afAnnounceIfOptionalText: string;

	/**
	 * Sätter antivirus scan status. Kan vara 'enable' eller 'disabled.
	 * @en Set antivirus scan status. Enable is default.
	 */
	@Prop() afValidation: FormFileUploadValidation =
		FormFileUploadValidation.ENABLED;

	/**
	 * Sätt rubrikens vikt. 'h2' är förvalt.
	 * @en Set heading level. Default is 'h2'
	 */
	@Prop() afHeadingLevel: FormFileUploadHeadingLevel =
		FormFileUploadHeadingLevel.H2;

	/**
	 * Sänder ut fil vid uppladdning
	 * @en Emits file on upload
	 */
	@Event() afOnUploadFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som tagits bort
	 * @en Emits which file that was deleted.
	 */
	@Event() afOnRemoveFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som har avbrutis uppladdning
	 * @en Emits which file that was canceled
	 */
	@Event() afOnCancelFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som försöker laddas upp igen
	 * @en Emits which file is trying to retry its upload
	 */
	@Event() afOnRetryFile: EventEmitter;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	/**
	 * Validera filens status.
	 * @en Validate file status
	 */
	@Method()
	async afMValidateFile(scanInfo: any) {
		this.validateFile(scanInfo);
	}

	/**
	 * Få ut alla uppladdade filer
	 * @en Get all uploaded files
	 */
	@Method()
	async afMGetAllFiles() {
		if (this.afValidation === 'enabled') {
			const validatedFiles = this.files.filter((file) => file['status'] === 'OK');
			return validatedFiles;
		} else {
			return this.files;
		}
	}

	/**
	 * Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt måste innehålla id, status och filen själv.
	 * @en Import an array with files without triggering upload event. A file object needs to contain an id, status and the file itself.
	 */
	@Method()
	async afMImportFiles(files: File[]) {
		for (const importedFile of files) {
			let duplicate = false;
			for (const item of this.files) {
				if (item.name == importedFile.name) {
					this.error = true;
					this.errorMessage = `Det finns redan en fil med filnamnet (${importedFile.name})`;
					duplicate = true;
				}
			}

			if (!duplicate) {
				this.files = [...this.files, importedFile];
			}
		}
	}

	@Method()
	async afMGetFormControlElement() {
		return this._input;
	}

	get fileMaxSize() {
		// 1MB is 1048576 in Bytes
		return this.afFileMaxSize * 1048576;
	}

  checkAcceptedTypes(file) {
    const acceptedTypes = this.afFileTypes.replace(/\s/g, '').split(',')
    const fileMimeType = file.type;
    const fileNameExtension = file.name.split('.').slice(-1).map((item) => '.' + item.toLowerCase()).toString()

    return acceptedTypes.some((item) => {

      // Checking against group of mime types
      if(item.includes('/*')) {
        const mimeTypeCategory = item.split('/').slice(0, 1).toString()
        const fileMimeTypeCategory = fileMimeType.split('/').slice(0, 1).toString() 
        return mimeTypeCategory == fileMimeTypeCategory
      }

      // Checking against mimetype and extension
      return item === fileMimeType || item === fileNameExtension || item === '*'
    })
  }

	validateFile(scanInfo: any) {
		const index = this.files.findIndex(
			(file) => file['name'] == scanInfo['name'] || file['id'] == scanInfo['id']
		);

		if (index === -1) {
			return { message: 'Something went wrong' };
		}

		let file = this.files[index];

		if (scanInfo.status === 'OK') {
			file['status'] = 'OK';
			delete file['error'];

			this.removeFileFromList(file['id']);

			this.files = [...this.files, file];
		} else {
			file['status'] = 'error';

			file['error'] =
				scanInfo.error === undefined ? 'Fil bedömd som osäker' : scanInfo.error;

			this.removeFileFromList(file['id']);

			this.files = [...this.files, file];
		}
	}

	emitFile(incomingFile: File) {
		const index = this.getIndexOfFile(incomingFile['id']);

		if (this.afValidation === 'enabled') {
			this.afOnUploadFile.emit(this.files[index]);
		} else {
			this.files[index]['status'] = 'OK';
			this.afOnUploadFile.emit(this.files[index]);
		}
	}

	onRetryFileHandler(e: Event, id: string) {
		e.preventDefault();

		const fileFromList = this.removeFileFromList(id);
		fileFromList['status'] = 'pending';
		delete fileFromList['error'];

		this.files = [...this.files, fileFromList];

		this.afOnRetryFile.emit(fileFromList);
	}

	onCancelFileHandler(e: Event, id: string) {
		e.preventDefault();

		const fileFromList = this.removeFileFromList(id);
		fileFromList['status'] = 'error';
		fileFromList['error'] = 'Uppladdning avbruten';

		this.files = [...this.files, fileFromList];

		this.afOnCancelFile.emit(fileFromList);
	}

	removeFileFromList(id: string) {
		const index = this.getIndexOfFile(id);

		if (index === -1) {
			return;
		}

		const removedFile = this.files[index];
		this.files = [...this.files.slice(0, index), ...this.files.slice(index + 1)];
		return removedFile;
	}

	getIndexOfFile(id: string) {
		const index = this.files.findIndex((file) => file['id'] == id);
		return index;
	}

	onButtonUploadFileHandler(e) {
		e.preventDefault();

		if (!e.target.files) return;

		const file = e.target.files[0];

    if(this.checkAcceptedTypes(file)) {
      this.addUploadedFiles(file);
    } else {
      this.error = true;
      this.errorMessage = 'Filtypen tillåts inte att ladda upp';
    }

	}

	onRemoveFileHandler(e: Event, id: any) {
		e.preventDefault();

		const fileToRemove = this.removeFileFromList(id);

		this.afOnRemoveFile.emit(fileToRemove);

		this._input.value = '';

		if (this.files.length < 1) this.error = false;
	}

	async addUploadedFiles(file: File) {
		this.error = false;

		if (this.files.length >= this.afMaxFiles) {
			this.error = true;
			this.errorMessage = 'Max antal filer uppladdade!';
			return;
		} else if (file.size > this.fileMaxSize) {
			this.error = true;
			this.errorMessage = 'Filens storlek är för stor!';
			return;
		} else {
			for (const item of this.files) {
				if (item.name == file.name) {
					this.error = true;
					this.errorMessage = 'Det finns redan en fil med det filnamnet!';
					return;
				}
			}
		}

		file['id'] = randomIdGenerator('file');
		file['status'] = 'pending';
		file['base64'] = await this.toBase64(file);

		this.files = [...this.files, file];
		this.emitFile(file);
	}

	toBase64 = (file: Blob) =>
		new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => {
				let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
				if (encoded.length % 4 > 0) {
					encoded += '='.repeat(4 - (encoded.length % 4));
				}
				resolve(encoded);
			};
			reader.onerror = (error) => reject(error);
		});

	handleDrop(e: DragEvent) {
		e.stopPropagation();
		e.preventDefault();
		this.fileHover = false;

		if (e.dataTransfer.files) {
			const file = e.dataTransfer.files[0];

			if (this.checkAcceptedTypes(file)) {
				this.addUploadedFiles(file);
			} else {
				this.error = true;
				this.errorMessage = 'Filtypen tillåts inte att ladda upp';
			}
		}
	}

	handleAllowDrop(e: DragEvent) {
		if (this.afVariation === 'secondary') return;
		e.stopPropagation();
		e.preventDefault();
	}

	handleOnDragEnter(e: DragEvent) {
		this.handleAllowDrop(e);
		this.fileHover = !this.fileHover;
	}

	handleOnDragLeave(e: DragEvent) {
		this.handleAllowDrop(e);
		this.fileHover = !this.fileHover;
	}

	handleInputClick(e: Event) {
		e.cancelBubble = true;
		this._input.click();
	}

	componentWillLoad() { }
	componentDidLoad() {
		this.afOnReady.emit();
	}
	componentWillUpdate() { }

	get cssModifiers() {
		return {
			'digi-form-file-upload--primary': this.afVariation == 'primary',
			'digi-form-file-upload--secondary': this.afVariation == 'secondary',
			'digi-form-file-upload--tertiary': this.afVariation == 'tertiary'
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-form-file-upload': true,
					...this.cssModifiers
				}}
			>
				<digi-form-label
					afFor={this.afId}
					afLabel={this.afLabel}
					afDescription={this.afLabelDescription}
					afRequired={this.afRequired}
					afAnnounceIfOptional={this.afAnnounceIfOptional}
					afRequiredText={this.afRequiredText}
					afAnnounceIfOptionalText={this.afAnnounceIfOptionalText}
				></digi-form-label>
				<div
					class={{
						'digi-form-file-upload__upload-area': true,
						'digi-form-file-upload__upload-area--hover': this.fileHover
					}}
					onDrop={(e) => this.handleDrop(e)}
					onDragOver={(e) => this.handleAllowDrop(e)}
					onDragEnter={(e) => this.handleOnDragEnter(e)}
					onDragLeave={(e) => this.handleOnDragLeave(e)}
					onClick={(e) => this.handleInputClick(e)}
				>
					{this.fileHover && this.afVariation != 'secondary' && (
						<div class="digi-form-file-upload__upload-area-overlay"></div>
					)}
					{this.afVariation == 'tertiary' && (
						<digi-icon-upload
							style={{ '--digi--icon--height': '40px' }}
							class="digi-form-file-upload__icon"
							aria-hidden="true"
						></digi-icon-upload>
					)}
					<div class="digi-form-file-upload__text-area">
						<digi-button
							afVariation={ButtonVariation.FUNCTION}
							afType={ButtonType.BUTTON}
							onClick={(e) => this.handleInputClick(e)}
						>
							<digi-icon-paperclip
								aria-hidden="true"
								slot="icon"
							></digi-icon-paperclip>
							{this.afUploadBtnText ? this.afUploadBtnText : 'Välj fil'}
						</digi-button>
						<input
							ref={(el) => {
								this._input = el as HTMLInputElement;
							}}
							onChange={(e) => this.onButtonUploadFileHandler(e)}
							class="digi-form-file-upload__input"
							multiple
							type="file"
							id={this.afId}
							accept={this.afFileTypes}
							name={this.afName ? this.afName : null}
							required={this.afRequired ? this.afRequired : null}
							onDrop={(e) => this.handleDrop(e)}
							onDragOver={(e) => this.handleAllowDrop(e)}
							aria-errormessage="digi-form-file-upload__error"
							aria-describedBy="digi-form-file-upload__error"
						/>
						{this.afVariation != FormFileUploadVariation.SECONDARY &&
							'eller dra och släpp en fil inuti det streckade området'}
					</div>
				</div>
				{this.error && (
					<digi-form-validation-message
						id="digi-form-file-upload__error"
						class="digi-form-file-upload__error"
						role="alert"
						aria-label={this.errorMessage}
						af-variation="error"
					>
						{this.errorMessage}
					</digi-form-validation-message>
				)}
				{this.files.length > 0 && (
					<div class="digi-form-file-upload__files">
						<this.afHeadingLevel class="digi-form-file-upload__files-heading">
							{this.afHeadingFiles}
						</this.afHeadingLevel>
						<ul aria-live="assertive">
							{this.files.map((file) => {
								return (
									<li class="digi-form-file-upload__file-container">
										{file['status'] == 'pending' && (
											<div class="digi-form-file-upload__file">
												<div class="digi-form-file-upload__file-header">
													<digi-icon-spinner class="digi-form-file-upload__spinner hidden-mobile"></digi-icon-spinner>
													<span>
														Laddar upp... <span class={'hidden-mobile'}>|</span>
													</span>
													<p class="digi-form-file-upload__file-name hidden-mobile">
														{file.name}
													</p>
													<button
														type="button"
														onClick={(e) => this.onCancelFileHandler(e, file['id'])}
														class="digi-form-file-upload__button hidden-mobile"
														aria-label={`Avbryt uppladdningen av ${file.name}`}
													>
														Avbryt uppladdning
													</button>
													<button
														type="button"
														onClick={(e) => this.onCancelFileHandler(e, file['id'])}
														class="digi-form-file-upload__button digi-form-file-upload__button--mobile"
														aria-label={`Avbryt uppladdningen av ${file.name}`}
													>
														<digi-icon-x-button-outline
															style={{
																'--digi--icon--height': '20px',
																'--digi--icon--color':
																	'var(--digi--global--color--function--info--base)'
															}}
															aria-hidden="true"
														></digi-icon-x-button-outline>
													</button>
												</div>
											</div>
										)}
										{file['status'] == 'OK' && (
											<div class="digi-form-file-upload__file">
												<div class="digi-form-file-upload__file-header">
													<digi-icon-notification-succes
														style={{
															'--digi--icon--height': '20px',
															'--digi--icon--color': 'green'
														}}
														class={'hidden-mobile'}
													></digi-icon-notification-succes>
													<p class="digi-form-file-upload__file-name">{file.name}</p>
													<button
														type="button"
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														class="digi-form-file-upload__button hidden-mobile"
														aria-label={`Ta bort ${file.name}`}
													>
														Ta bort fil
													</button>
													<button
														type="button"
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														class="digi-form-file-upload__button digi-form-file-upload__button--mobile"
														aria-label={`Ta bort ${file.name}`}
													>
														<digi-icon-trash
															style={{
																'--digi--icon--height': '20px',
																'--digi--icon--color':
																	'var(--digi--global--color--function--info--base)'
															}}
														></digi-icon-trash>
													</button>
												</div>
											</div>
										)}
										{file['status'] == 'error' && (
											<div class="digi-form-file-upload__file">
												<div class="digi-form-file-upload__file-header">
													<digi-icon-danger-outline
														style={{
															'--digi--icon--height': '20px',
															'--digi--icon--color': 'red'
														}}
														aria-hidden="true"
														class={'hidden-mobile'}
													></digi-icon-danger-outline>
													<span
														class="digi-form-file-upload__file--error"
														aria-label={`${file['name']} gick inte att ladda upp. ${file['error']}`}
													>
														Avbruten <span>|</span>
													</span>
													<p
														class="digi-form-file-upload__file-name
													digi-form-file-upload__file--error hidden-mobile
													"
													>
														{file.name}
													</p>
													{this.afValidation === 'disabled' ||
														(file['error'] === 'Uppladdning avbruten' && (
															<div style={{ display: 'flex' }}>
																<button
																	type="button"
																	onClick={(e) => this.onRetryFileHandler(e, file['id'])}
																	class="digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile"
																	aria-label={`Misslyckad uppladdning, försök ladda upp filen igen ${file.name}`}
																>
																	Försök igen
																	<span>|</span>
																</button>
																<button
																	type="button"
																	onClick={(e) => this.onRetryFileHandler(e, file['id'])}
																	class="digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file"
																	aria-label={`Misslyckad uppladdning, försök ladda upp filen igen ${file.name}`}
																	style={{
																		'margin-right': '10px'
																	}}
																>
																	<digi-icon-update
																		style={{
																			'--digi--icon--height': '20px',
																			'--digi--icon--color':
																				'var(--digi--global--color--function--info--base)'
																		}}
																	></digi-icon-update>
																</button>
															</div>
														))}
													<button
														type="button"
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														class="digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile"
														aria-label={`Misslyckad uppladdning, ta bort ${file.name} från fillistan`}
													>
														Ta bort fil
													</button>
													<button
														type="button"
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														class="digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file"
														aria-label={`Misslyckad uppladdning, ta bort ${file.name} från fillistan`}
													>
														<digi-icon-x-button-outline
															style={{
																'--digi--icon--height': '20px',
																'--digi--icon--color':
																	'var(--digi--global--color--function--info--base)'
															}}
														></digi-icon-x-button-outline>
													</button>
												</div>
												<div class="digi-form-file-upload__file-footer">
													<p
														class={'hidden-mobile'}
														role="status"
														aria-label={`${file['name']} gick inte att ladda upp. ${file['error']}`}
													>
														{`Filen gick inte att ladda upp. ${file['error']}`}
													</p>
												</div>
											</div>
										)}
										{/* {(file['status'] == undefined ||
											!file['status'] ||
											file['status']) && (
											<div class="digi-form-file-upload__file">
												<div class="digi-form-file-upload__file-header">
													<p
														class="digi-form-file-upload__file-name
														digi-form-file-upload__file--error hidden-mobile
														"
													>
														{file.name ?? 'otillgänglig fil'}
													</p>

													<button
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														type="button"
														class="digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile"
														af-aria-label={`Misslyckad uppladdning, ta bort ${
															file.name ?? 'otillgänglig fil'
														} från fillistan`}
													>
														Ta bort fil
													</button>
													<button
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														type="button"
														class="digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file"
														af-aria-label={`Misslyckad uppladdning, ta bort ${
															file.name ?? 'otillgänglig fil'
														} från fillistan`}
													>
														<digi-icon-x-button-outline
															style={{
																'--digi--icon--height': '20px',
																'--digi--icon--color':
																	'var(--digi--global--color--function--info--base)'
															}}
														></digi-icon-x-button-outline>
													</button>
												</div>
											</div>
										)} */}
									</li>
								);
							})}
						</ul>
					</div>
				)}
			</div>
		);
	}
}
