# digi-form-error-list

Displays a list of errors. The component is meant to be used at the top of a form.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormErrorListHeadingLevel } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                 | Description                                                                                                                                                                        | Type                                                                                                                                                                                           | Default                                     |
| ---------------------- | ------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------- |
| `afDescription`        | `af-description`          | En valfri beskrivande text.                                                                                                                                                        | `string`                                                                                                                                                                                       | `undefined`                                 |
| `afEnableHeadingFocus` | `af-enable-heading-focus` | Används för att aktivera automatisk fokusering på rubrik när komponenten initieras.                                                                                                | `boolean`                                                                                                                                                                                      | `true`                                      |
| `afHeading`            | `af-heading`              | Rubrikens text                                                                                                                                                                     | `string`                                                                                                                                                                                       | `undefined`                                 |
| `afHeadingLevel`       | `af-heading-level`        | Sätt rubrikens vikt. 'h2' är förvalt.                                                                                                                                              | `FormErrorListHeadingLevel.H1 \| FormErrorListHeadingLevel.H2 \| FormErrorListHeadingLevel.H3 \| FormErrorListHeadingLevel.H4 \| FormErrorListHeadingLevel.H5 \| FormErrorListHeadingLevel.H6` | `FormErrorListHeadingLevel.H2`              |
| `afId`                 | `af-id`                   | Sätter attributet 'id' på det omslutande elementet. Ges inget värde så genereras ett slumpmässigt.                                                                                 | `string`                                                                                                                                                                                       | `randomIdGenerator('digi-form-error-list')` |
| `afOverrideLink`       | `af-override-link`        | Kringgår felmeddelandenas ankarlänkar och sätter istället fokus programmatiskt. Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande. | `boolean`                                                                                                                                                                                      | `false`                                     |


## Events

| Event           | Description                                                                     | Type               |
| --------------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnClickLink` | Länkelementens 'onclick'-event.                                                 | `CustomEvent<any>` |
| `afOnReady`     | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |


## Methods

### `afMSetHeadingFocus() => Promise<void>`

Sätter fokus på rubriken.

#### Returns

Type: `Promise<void>`




## CSS Custom Properties

| Name                                     | Description                          |
| ---------------------------------------- | ------------------------------------ |
| `--digi--form-error-list--item--margin`  | 0;                                   |
| `--digi--form-error-list--item--padding` | var(--digi--padding--smaller) 0 0 0; |
| `--digi--form-error-list--list--margin`  | 0;                                   |
| `--digi--form-error-list--list--padding` | var(--digi--gutter--smaller) 0 0 0;  |


## Dependencies

### Depends on

- [digi-notification-alert](../../_notification/notification-alert)
- [digi-typography](../../../__core/_typography/typography)
- [digi-link-internal](../../../__core/_link/link-internal)
- [digi-util-mutation-observer](../../../__core/_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-form-error-list --> digi-notification-alert
  digi-form-error-list --> digi-typography
  digi-form-error-list --> digi-link-internal
  digi-form-error-list --> digi-util-mutation-observer
  digi-notification-alert --> digi-layout-media-object
  digi-notification-alert --> digi-icon
  digi-notification-alert --> digi-button
  digi-notification-alert --> digi-typography
  digi-link-internal --> digi-link
  digi-link-internal --> digi-icon
  style digi-form-error-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
