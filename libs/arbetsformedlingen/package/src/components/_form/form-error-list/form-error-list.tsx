import {
	Component,
	Element,
	Event,
	EventEmitter,
	Prop,
	State,
	h
} from '@stencil/core';
import { HTMLStencilElement, Listen, Method } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormErrorListHeadingLevel } from './form-error-list-heading-level.enum';
import { logger } from '../../../global/utils/logger';
import {
	TypographyVariation,
	LinkInternalVariation
} from '../../../enums-core';
import { NotificationAlertVariation } from '../../_notification/notification-alert/notification-alert-variation.enum';

/**
 * @enums FormErrorListHeadingLevel - form-error-list-heading-level.enum.ts
 * @swedishName Felmeddelandelista
 */
@Component({
	tag: 'digi-form-error-list',
	styleUrls: ['form-error-list.scss'],
	scoped: true
})
export class FormErrorList {
	@Element() hostElement: HTMLStencilElement;

	private _notificationAlert;
	private _observer;

	@State() linkItems: any[] = [];

	/**
	 * Rubrikens text
	 * @en The heading text
	 */
	@Prop() afHeading: string;

	/**
	 * Sätt rubrikens vikt. 'h2' är förvalt.
	 * @en Set heading level. Default is 'h2'
	 */
	@Prop() afHeadingLevel: FormErrorListHeadingLevel =
		FormErrorListHeadingLevel.H2;

	/**
	 * En valfri beskrivande text.
	 * @en An optional description text.
	 */
	@Prop() afDescription: string;

	/**
	 * Sätter attributet 'id' på det omslutande elementet. Ges inget värde så genereras ett slumpmässigt.
	 * @en Label id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-error-list');

	/**
	 * Kringgår felmeddelandenas ankarlänkar och sätter istället fokus programmatiskt.
	 * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
	 * @en Override default anchor link behavior and sets focus programmatically. Should only be used if default anchor link behaviour is a problem with e.g. routing
	 */
	@Prop() afOverrideLink: boolean = false;

	/**
	 * Används för att aktivera automatisk fokusering på rubrik när komponenten initieras.
	 * @en Used to enable automatic focus on header when the component is initialized.
	 */
	@Prop() afEnableHeadingFocus: boolean = true;

	/**
	 * Länkelementens 'onclick'-event.
	 * @en The link elements' 'onclick' event.
	 */
	@Event() afOnClickLink: EventEmitter;

	/**
	 * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	 * @en When the component and slots are loaded and initialized this event will trigger.
	 */
	@Event({
		bubbles: false,
		cancelable: true
	})
	afOnReady: EventEmitter;

	/**
	 * Sätter fokus på rubriken.
	 * @en Sets focus on the heading.
	 */
	@Method()
	async afMSetHeadingFocus() {
		this.setHeadingFocus();
	}

	componentWillLoad() {
		this.getLinks();
	}

	componentWillUpdate() {
		this.getLinks(true);
	}
	componentDidLoad() {
		this.setHeadingFocus();
		this.afOnReady.emit();
	}

	getLinks(update?: boolean) {
		let elements = this.hostElement.children;

		if (!elements || elements.length <= 0) {
			logger.warn(`The slot contains no link elements`, this.hostElement);
			return;
		}

		let items: any;

		if (update) {
			items = this._observer.children;
		} else {
			items = elements;
		}

		this.linkItems = [...items]
			.filter((element) => element.tagName.toLowerCase() === 'a')
			.map((element) => {
				return {
					text: element.textContent,
					href: element.getAttribute('href') || ''
				};
			});
	}

	setHeadingFocus() {
		if (this.afEnableHeadingFocus) {
			setTimeout(async () => {
				const el = await this._notificationAlert.afMGetHeadingElement();
				!!el && el.focus();
			}, 0);
		}
	}

	@Listen('afOnClick')
	clickResponse(e: any): void {
		if (this.afOverrideLink) {
			e.detail.preventDefault();
		}
		this.afOnClickLink.emit(e);
	}

	render() {
		return (
			<div class="digi-form-error-list" id={this.afId}>
				<digi-notification-alert
					ref={(el) => (this._notificationAlert = el)}
					af-heading={this.afHeading}
					af-heading-level={this.afHeadingLevel}
					af-variation={NotificationAlertVariation.DANGER}
				>
					{this.afDescription && (
						<digi-typography af-variation={TypographyVariation.SMALL}>
							<p class="digi-form-error-list__description">{this.afDescription}</p>
						</digi-typography>
					)}
					<ul class="digi-form-error-list__list">
						{this.linkItems.map((item) => {
							return (
								<li class="digi-form-error-list__item">
									<digi-link-internal
										class="digi-form-error-list__link"
										afVariation={LinkInternalVariation.SMALL}
										afHref={item.href}
									>
										{item.text}
									</digi-link-internal>
								</li>
							);
						})}
					</ul>
				</digi-notification-alert>
				<div hidden class="digi-form-error-list__slot">
					<digi-util-mutation-observer
						afOptions={{
							attributes: false,
							childList: true,
							subtree: true,
							characterData: true
						}}
						ref={(el) => (this._observer = el)}
						onAfOnChange={() => this.getLinks()}
					>
						<slot></slot>
					</digi-util-mutation-observer>
				</div>
			</div>
		);
	}
}
