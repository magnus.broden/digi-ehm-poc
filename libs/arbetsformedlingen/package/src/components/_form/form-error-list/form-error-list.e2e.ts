import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-error-list', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-error-list></digi-form-error-list>');

    const element = await page.find('digi-form-error-list');
    expect(element).toHaveClass('hydrated');
  });
});
