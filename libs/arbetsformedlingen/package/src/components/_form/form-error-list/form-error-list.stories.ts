import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormErrorListHeadingLevel } from './form-error-list-heading-level.enum';

export default {
	title: 'form/digi-form-error-list',
	parameters: {
		actions: {
			handles: ['afOnClickLink']
		}
	},
	argTypes: {
		'af-heading-level': enumSelect(FormErrorListHeadingLevel)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-error-list',
	'af-heading': '',
	'af-heading-level': FormErrorListHeadingLevel.H2,
	'af-description': '',
	'af-id': null,
	'af-override-link': false,
	/* html */
	children: `
    <a href="#testInput1">Obligatoriskt fält</a>
    <a href="#testInput2">Felaktigt format</a>`
};
