import { newSpecPage } from '@stencil/core/testing';
import { FormCategoryFilter } from './form-category-filter';

describe('form-category-filter', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [FormCategoryFilter],
			html: '<form-category-filter></form-category-filter>'
		});
		expect(root).toEqualHtml(`
      <form-category-filter>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </form-category-filter>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [FormCategoryFilter],
			html: `<form-category-filter first="Stencil" last="'Don't call me a framework' JS"></form-category-filter>`
		});
		expect(root).toEqualHtml(`
      <form-category-filter first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </form-category-filter>
    `);
	});
});
