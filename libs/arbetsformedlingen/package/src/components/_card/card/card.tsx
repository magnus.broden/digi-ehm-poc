import { Component, Element, h, Prop, State } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { logger } from '../../../global/utils/logger';
import { CardBorderRadius } from './card-border-radius.enum';
import { CardBorder } from './card-border.enum';
import { CardFooterBorder } from './card-footer-border.enum';

/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Kort (Under utveckling)
 */
@Component({
  tag: 'digi-card',
  styleUrl: 'card.scss',
  scoped: true
})
export class Card {
  @Element() hostElement: HTMLStencilElement;

  @State() hasFooter:boolean;

  /**
   * Sätter ramlinje runt kort. Standard är utan ramlinje.
   * @en Sets borders to card. Defaults to 'none'.
   */
  @Prop() afBorder: CardBorder = CardBorder.NONE;

  /**
   * Sätter rundade hörn på kort. Standard är utan rundade hörn.
   * @en Sets cards border radius. Defaults to 'none'.
   */
  @Prop() afBorderRadius: CardBorderRadius = CardBorderRadius.NONE;

  /**
   * Sätter ramlinje uppåt i kortets bottendel. Standard är utan ramlinje.
   * @en Sets borders to cards footer. Default to 'none'.
   */
  @Prop() afFooterBorder: CardFooterBorder = CardFooterBorder.NONE;

  componentWillLoad() {
    logger.warn(`Digi-card is still work in progress.`, this.hostElement);
    this.setHasFooter();
  }

  setHasFooter() {
    this.hasFooter = !!this.hostElement.querySelector('[slot="footer"]');
  }

  get cssModifiers() {
    return {
      'digi-card--border--primary': this.afBorder == CardBorder.PRIMARY,
      'digi-card--border-radius--primary': this.afBorderRadius == CardBorderRadius.PRIMARY,
      'digi-card--has-footer': this.hasFooter,
      'digi-card--footer--border': this.afFooterBorder == CardFooterBorder.PRIMARY
    };
  }

  render() {
    return (
      <digi-typography>
        <div
          class={{
            'digi-card': true,
            ...this.cssModifiers
          }}
        >
          <div class="digi-card__content">
            <slot></slot>
          </div>
          {this.hasFooter && (
            <div class="digi-card__footer">
              <slot name="footer"></slot>
            </div>
          )}
        </div>
      </digi-typography>
    );
  }
}
