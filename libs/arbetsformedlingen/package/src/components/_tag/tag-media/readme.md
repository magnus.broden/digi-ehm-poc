# digi-tag-media

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute | Description                         | Type                                                                                                                                    | Default             |
| --------------------- | --------- | ----------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- | ------------------- |
| `afIcon`              | `af-icon` | Sätter en specifik icon för taggen. | `TagMediaIcon.FILM \| TagMediaIcon.NEWS \| TagMediaIcon.PLAYLIST \| TagMediaIcon.PODCAST \| TagMediaIcon.WEBINAR \| TagMediaIcon.WEBTV` | `TagMediaIcon.NEWS` |
| `afText` _(required)_ | `af-text` | Sätter taggens text.                | `string`                                                                                                                                | `undefined`         |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


## CSS Custom Properties

| Name                                   | Description                                                 |
| -------------------------------------- | ----------------------------------------------------------- |
| `--digi--tag-media--background-color`  | var(--digi--color--background--inverted-3);                 |
| `--digi--tag-media--border-radius`     | var(--digi--border-radius--tertiary);                       |
| `--digi--tag-media--color--icon`       | var(--digi--color--text--inverted);                         |
| `--digi--tag-media--color--text`       | var(--digi--color--text--inverted);                         |
| `--digi--tag-media--height`            | 1.875rem;                                                   |
| `--digi--tag-media--margin`            | var(--digi--gutter--small);                                 |
| `--digi--tag-media--padding`           | 0 var(--digi--gutter--small);                               |
| `--digi--tag-media--text--font-size`   | var(--digi--typography--tag--font-size--desktop);           |
| `--digi--tag-media--text--font-weight` | var(--digi--typography--description--font-weight--desktop); |
| `--digi--tag-media--width--icon`       | var(--digi--typography--body--font-size--desktop-large);    |


## Dependencies

### Used by

 - [digi-info-card-multi](../../_info-card/info-card-multi)

### Depends on

- [digi-icon-news](../../_icon/icon-news)
- [digi-icon-list-ul](../../_icon/icon-list-ul)
- [digi-icon-media-podcast](../../_icon/icon-media-podcast)
- [digi-icon-film](../../_icon/icon-film)
- [digi-icon-web-tv](../../_icon/icon-web-tv)
- [digi-icon-webinar](../../_icon/icon-webinar)

### Graph
```mermaid
graph TD;
  digi-tag-media --> digi-icon-news
  digi-tag-media --> digi-icon-list-ul
  digi-tag-media --> digi-icon-media-podcast
  digi-tag-media --> digi-icon-film
  digi-tag-media --> digi-icon-web-tv
  digi-tag-media --> digi-icon-webinar
  digi-info-card-multi --> digi-tag-media
  style digi-tag-media fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
