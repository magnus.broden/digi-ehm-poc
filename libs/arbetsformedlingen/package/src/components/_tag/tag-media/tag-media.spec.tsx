import { newSpecPage } from '@stencil/core/testing';
import { TagMedia } from './tag-media';

describe('tag-media', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [TagMedia],
			html: '<tag-media></tag-media>'
		});
		expect(root).toEqualHtml(`
      <tag-media>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </tag-media>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [TagMedia],
			html: `<tag-media first="Stencil" last="'Don't call me a framework' JS"></tag-media>`
		});
		expect(root).toEqualHtml(`
      <tag-media first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </tag-media>
    `);
	});
});
