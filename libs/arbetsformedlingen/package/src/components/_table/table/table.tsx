import { Component, Prop, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { TableSize } from './table-size.enum';
import { TableVariation } from './table-variation.enum';

/**
 * @slot default - Ska innehålla ett table-element
 *
 * @enums TableSize - table-size.enum.ts
 * @enums TableVariation - table-variation.enum.ts
 * @swedishName Tabell
 */
@Component({
	tag: 'digi-table',
	styleUrls: ['table.scss'],
	scoped: false
})
export class Table {
	/**
	 * Sätter variant. Kan vara 'primary', 'secondary' eller 'tertiary'.
	 * @en Set variation. Can be 'primary', 'secondary' or 'tertiary'.
	 */
	@Prop() afVariation: TableVariation = TableVariation.PRIMARY;

	/**
	 * Sätter storlek. Kan vara 'small' eller 'medium'.
	 * @en Set size. Can be 'small' or 'medium'.
	 */
	@Prop() afSize: TableSize = TableSize.MEDIUM;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-breadcrumbs');

	get cssModifiers() {
		return {
			'digi-table--primary': this.afVariation === TableVariation.PRIMARY,
			'digi-table--secondary': this.afVariation === TableVariation.SECONDARY,
			'digi-table--tertiary': this.afVariation === TableVariation.TERTIARY,
			'digi-table--small': this.afSize === TableSize.SMALL
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-table': true,
					...this.cssModifiers
				}}
			>
				<slot></slot>
			</div>
		);
	}
}
