import { newE2EPage } from '@stencil/core/testing';

describe('info-card-multi', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<info-card-multi></info-card-multi>');
		const element = await page.find('info-card-multi');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<info-card-multi></info-card-multi>');
		const component = await page.find('info-card-multi');
		const element = await page.find('info-card-multi >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
