# digi-info-card-multi

<!-- Auto Generated Below -->


## Properties

| Property                      | Attribute          | Description                                | Type                                                                                                                                                                                           | Default                   |
| ----------------------------- | ------------------ | ------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------- |
| `afAlt`                       | `af-alt`           | Sätter attributet 'alt'.                   | `string`                                                                                                                                                                                       | `undefined`               |
| `afHeading` _(required)_      | `af-heading`       | Rubrikens text                             | `string`                                                                                                                                                                                       | `undefined`               |
| `afHeadingLevel` _(required)_ | `af-heading-level` | Sätter rubrikens vikt (h1-h6)              | `InfoCardMultiHeadingLevel.H1 \| InfoCardMultiHeadingLevel.H2 \| InfoCardMultiHeadingLevel.H3 \| InfoCardMultiHeadingLevel.H4 \| InfoCardMultiHeadingLevel.H5 \| InfoCardMultiHeadingLevel.H6` | `undefined`               |
| `afLinkHref`                  | `af-link-href`     | Sätter attributet 'href' på länken         | `string`                                                                                                                                                                                       | `undefined`               |
| `afSrc`                       | `af-src`           | Sätter attributet 'src'.                   | `string`                                                                                                                                                                                       | `undefined`               |
| `afTagIcon`                   | `af-tag-icon`      | Sätter ikon för taggen                     | `TagMediaIcon.FILM \| TagMediaIcon.NEWS \| TagMediaIcon.PLAYLIST \| TagMediaIcon.PODCAST \| TagMediaIcon.WEBINAR \| TagMediaIcon.WEBTV`                                                        | `TagMediaIcon.NEWS`       |
| `afTagText`                   | `af-tag-text`      | Sätter Taggtext på bild                    | `string`                                                                                                                                                                                       | `undefined`               |
| `afType`                      | `af-type`          | Sätter typ. Kan vara 'media' eller 'text'. | `InfoCardMultiType.ENTRY \| InfoCardMultiType.MEDIA \| InfoCardMultiType.RELATED`                                                                                                              | `InfoCardMultiType.MEDIA` |


## Events

| Event           | Description                    | Type               |
| --------------- | ------------------------------ | ------------------ |
| `afOnClickLink` | Länkelementets 'onclick'-event | `CustomEvent<any>` |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


## CSS Custom Properties

| Name                                                           | Description                                                                                                         |
| -------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| `--digi--info-card--background-color`                          | var(--digi--color--background--primary);                                                                            |
| `--digi--info-card-multi--border`                              | var(--digi--border-width--complementary-2) solid var(--digi--color--border--secondary);                             |
| `--digi--info-card-multi--color--heading`                      | var(--digi--color--text--primary);                                                                                  |
| `--digi--info-card-multi--entry-heading--font-size--desktop`   | var(--digi--typography--heading-2--font-size--desktop);                                                             |
| `--digi--info-card-multi--entry-heading--font-size--mobile`    | var(--digi--typography--heading-2--font-size--mobile);                                                              |
| `--digi--info-card-multi--heading--font-weight`                | var(--digi--typography--heading-3--font-weight--desktop);                                                           |
| `--digi--info-card-multi--media-heading--font-size--desktop`   | var(--digi--typography--heading-3--font-size--desktop);                                                             |
| `--digi--info-card-multi--media-heading--font-size--mobile`    | var(--digi--typography--heading-3--font-size--mobile);                                                              |
| `--digi--info-card-multi--padding--image`                      | var(--digi--padding--medium) var(--digi--padding--medium) var(--digi--padding--large) var(--digi--padding--medium); |
| `--digi--info-card-multi--padding--image--desktop`             | var(--digi--padding--medium) var(--digi--padding--medium) var(--digi--padding--large) var(--digi--padding--medium); |
| `--digi--info-card-multi--padding--text`                       | var(--digi--padding--medium) var(--digi--padding--medium) var(--digi--padding--large) var(--digi--padding--medium); |
| `--digi--info-card-multi--padding--text--desktop`              | var(--digi--padding--medium) var(--digi--padding--medium) var(--digi--padding--large) var(--digi--padding--medium); |
| `--digi--info-card-multi--related-heading--font-size--desktop` | var(--digi--typography--heading-3--font-size--desktop);                                                             |
| `--digi--info-card-multi--related-heading--font-size--mobile`  | var(--digi--typography--heading-3--font-size--mobile);                                                              |


## Dependencies

### Depends on

- [digi-media-image](../../../__core/_media/media-image)
- [digi-tag-media](../../_tag/tag-media)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-info-card-multi --> digi-media-image
  digi-info-card-multi --> digi-tag-media
  digi-info-card-multi --> digi-typography
  digi-media-image --> digi-util-intersection-observer
  digi-tag-media --> digi-icon-news
  digi-tag-media --> digi-icon-list-ul
  digi-tag-media --> digi-icon-media-podcast
  digi-tag-media --> digi-icon-film
  digi-tag-media --> digi-icon-web-tv
  digi-tag-media --> digi-icon-webinar
  style digi-info-card-multi fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
