export enum InfoCardMultiType {
  MEDIA = 'media',
  RELATED = 'related',
  ENTRY = 'entry',
}
