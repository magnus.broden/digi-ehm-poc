import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { InfoCardVariation } from './info-card-variation.enum';
import { InfoCardType } from './info-card-type.enum';
import { InfoCardHeadingLevel } from './info-card-heading-level.enum';

export default {
	title: 'info-card/digi-info-card',
	parameters: {
		actions: {
			handles: ['afOnClickLink']
		}
	},
	argTypes: {
		'af-type': enumSelect(InfoCardType),
		'af-variation': enumSelect(InfoCardVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-info-card',
	'af-heading': 'A heading is required',
	'af-heading-level': null,
	'af-type': InfoCardType.TIP,
	'af-variation': InfoCardVariation.PRIMARY,
	'af-link-text': '',
	'af-link-href': '',
	/* html */
	children: `
    <p>
        These are just words to illustrate how it looks like with text inside. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse commodo egestas elit in
        consequat. Proin in ex consectetur, laoreet augue sit amet, malesuada tellus.
    </p>`
};
