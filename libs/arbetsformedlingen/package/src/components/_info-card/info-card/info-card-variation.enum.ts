export enum InfoCardVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
