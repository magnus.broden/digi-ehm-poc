import { Component, Prop, h } from '@stencil/core';
import { InfoCardMultiContainerHeadingLevel } from './info-card-multi-container-heading-level.enum';

/**
 * @slot default - Ska vara flera digi-info-card med af-type 'multi'
 *
 * @enums InfoCardMultiContainerHeadingLevel - info-card-multi-container-heading-level.enum.ts
 * @swedishName Infokortsbehållare
 */
@Component({
  tag: 'digi-info-card-multi-container',
  styleUrls: ['info-card-multi-container.scss'],
  scoped: true,
})
export class InfoCardMultiContainer {
  /**
   * Rubrikens text
   * @en The heading text
   */
  @Prop() afHeading: string;

  /**
   * Sätt rubrikens vikt (h1-h6)
   * @en Set heading level (h1-h6)
   */
  @Prop() afHeadingLevel: InfoCardMultiContainerHeadingLevel;

  render() {
    return (
      <div class="digi-info-card-multi-container">
        {this.afHeading && this.afHeadingLevel && (
          <this.afHeadingLevel class="digi-info-card-multi-container__heading">
            {this.afHeading}
          </this.afHeadingLevel>
        )}
        <digi-layout-columns af-variation="three">
          <slot></slot>
        </digi-layout-columns>
      </div>
    );
  }
}
