import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { InfoCardMultiContainerHeadingLevel } from './info-card-multi-container-heading-level.enum';
import { InfoCardHeadingLevel } from '../info-card/info-card-heading-level.enum';
import { InfoCardType } from '../info-card/info-card-type.enum';

export default {
	title: 'info-card/digi-info-card-multi-container',
	argTypes: {
		'af-heading-level': enumSelect(InfoCardMultiContainerHeadingLevel)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-info-card-multi-container',
	'af-heading': '',
	'af-heading-level': null,
	/* html */
	children: `
    <digi-info-card
        af-heading="I am an info card with type multi"
        af-heading-level="${InfoCardHeadingLevel.H3}"
        af-type="${InfoCardType.MULTI}"
        af-link-text="I am a required link"
        af-link-href="#"
    >
        <p>These are just words to illustrate how it looks like with text inside. These are just words to illustrate how it looks like with text inside.</p>
    </digi-info-card>
    <digi-info-card
        af-heading="I am an info card with type multi"
        af-heading-level="${InfoCardHeadingLevel.H3}"
        af-type="${InfoCardType.MULTI}"
        af-link-text="I am a required link"
        af-link-href="#"
    >
        <p>These are just words to illustrate how it looks like with text inside. These are just words to illustrate how it looks like with text inside.</p>
    </digi-info-card>
    <digi-info-card
        af-heading="I am an info card with type multi"
        af-heading-level="${InfoCardHeadingLevel.H3}"
        af-type="${InfoCardType.MULTI}"
        af-link-text="I am a required link"
        af-link-href="#"
    >
        <p>These are just words to illustrate how it looks like with text inside. These are just words to illustrate how it looks like with text inside.</p>
    </digi-info-card>`
};
