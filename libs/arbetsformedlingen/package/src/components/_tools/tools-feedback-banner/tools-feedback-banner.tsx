import { Component, h, Prop, Element, State, Watch } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { ToolsFeedbackBannerHeadingLevel } from './tools-feedback-banner-heading-level.enum';
import { ToolsFeedbackBannerType } from './tools-feedback-banner-type.enum';
import { LayoutBlockVariation } from '../../../__core/_layout/layout-block/layout-block-variation.enum';

/**
 * @enums FeedbackBannerHeadingLevel - feedback-banner-heading-level.enum.ts
 * @enums FeedbackBannerType - feedback-banner-type.enum.ts
 *
 * @swedishName Feedback-banner
 */

@Component({
	tag: 'digi-tools-feedback-banner',
	styleUrl: 'tools-feedback-banner.scss',
	scoped: true
})
export class FeedbackBanner {
	@Element() hostElement: HTMLStencilElement;

	private _observer;

	/**
	 * Sätter rubriken
	 * @en Sets the heading
	 */
	@Prop() afHeading: string = 'Hjälp oss att bli bättre';

	/**
	 * Sätt rubrikens vikt. 'h2' är förvalt.
	 * @en Set heading level. Default is 'h2'
	 */
	@Prop() afHeadingLevel: ToolsFeedbackBannerHeadingLevel =
	ToolsFeedbackBannerHeadingLevel.H2;

	/**
	 * Sätter brödtexten
	 * @en Sets the text
	 */
	@Prop() afText: string =
		'Skicka in dina synpunkter och förslag på förbättringar på (tjänst)';

	/**
	 * Sätter typ på kortet. Kan vara 'fullwidth' eller 'grid'.
	 * @en Set card type. Can be 'fullwidth' or 'grid'.
	 */
	@Prop() afType: ToolsFeedbackBannerType = ToolsFeedbackBannerType.FULLWIDTH;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-feedback-banner');

	/**
	 * Används för att inte rendera länkarna om något a-element saknar href eller text
	 * @en Is used for not rendering the links if some a-element lacks href or text
	 */
	@State() validLinks: boolean = true;

	@State() validContent: boolean = true;

	/**
	 * Listan för alla länkobjekt
	 * @en The list for all link objects
	 */
	@State() linkItems: any[] = [];

	/**
	 * Kontrollerar så att man inte matar in en tom rubrik och paragraf
	 * @en Control for empty heading and paragraph
	 */
	@Watch('afHeading')
	@Watch('afText')
	validateHeading() {
		if (!this.afHeading || !this.afText) {
			logger.warn(
				`digi-feedback-banner must have a heading and text. Please add af-heading and af-text`,
				this.hostElement
			);
			this.validContent = false;
			return false;
		}
	}

	componentWillLoad() {
		this.validateLinks();
		this.validateHeading();
	}

	componentWillUpdate() {
		this.validateLinks(true);
		this.validateHeading();
	}

	validateLinks(update?: boolean) {
		let elements = this.hostElement.children;

		if (elements.length < 1 || elements.length > 3) {
			logger.warn(
				`The slot contains no link elements or there are more than 3 links specified (only 3 can be showed)`,
				this.hostElement
			);
			return;
		}

		let items: any;

		if (update) {
			items = this._observer.children;
		} else {
			items = elements;
		}

		this.linkItems = [...items]
			.filter((element) => element.tagName.toLowerCase() === 'a')
			.filter((element) => {
				const text = element.textContent;
				const href = element.getAttribute('href');
				if (!text || !href) {
					this.validLinks = false;
					logger.warn(
						`No links in digi-feedback-banner are rendered because a specified <a>-element must have both a text and href`,
						this.hostElement
					);
					return false;
				}
				return true;
			})
			.map((element) => {
				return {
					text: element.text,
					href: element.href
				};
			});
	}

	get cssModifiers() {
		return {
			'digi-tools-feedback-banner--fullwidth':
				this.afType === ToolsFeedbackBannerType.FULLWIDTH,
			'digi-tools-feedback-banner--grid': this.afType === ToolsFeedbackBannerType.GRID
		};
	}

	get icon() {
		return (
			<svg
				width="70px"
				height="55px"
				viewBox="0 -1 70 57"
				version="1.1"
				xmlns="http://www.w3.org/2000/svg"
			>
				<g
					id="Page-1"
					stroke="none"
					stroke-width="1"
					fill="none"
					fill-rule="evenodd"
				>
					<g id="Artboard" transform="translate(-1.000000, -1.000000)">
						<g
							id="Illustration"
							transform="translate(36.000000, 28.500000) scale(-1, 1) translate(-36.000000, -28.500000) translate(1.000000, 1.000000)"
						>
							<path
								d="M42.6854848,46.8545136 L52.1772369,46.8545136 L60.0989602,53.7853167 L60.0989602,46.8545136 L63.5554881,46.8545136 C66.2322456,46.8545136 68.4223676,44.6442917 68.4223676,41.9430542 L68.4223676,21.4518253 C68.4223676,18.7503271 66.2322456,16.5401053 63.5554881,16.5401053 L45.2209187,16.5401053 C45.2209187,16.5401053 45.2972177,30.5151432 45.2972177,32.3423051 C45.2972177,34.1692063 43.6708169,38.4874313 37.8183432,38.4874313 C37.7811114,41.1448606 37.8183432,41.9430542 37.8183432,41.9430542 C37.8183432,44.6442917 40.0084652,46.8545136 42.6854848,46.8545136 Z"
								id="Fill-1"
								fill="#DEE9B7"
							></path>
							<path
								d="M46.0035734,16.5771857 L65.0615411,16.5771857 C67.777628,16.5771857 70,18.7874075 70,21.488645 L70,43.1577375 C70,45.858975 67.777628,48.0691969 65.0615411,48.0691969 L61.5544094,48.0691969 L61.5544094,55 L53.5160089,48.0691969 L43.8850309,48.0691969 C41.1686818,48.0691969 38.9463098,45.858975 38.9463098,43.1577375 L38.9463098,38.8848853"
								id="Stroke-3"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></path>
							<g id="Group-8">
								<path
									d="M46.0035734,16.5771857 L46.0035734,6.19362792 C46.0035734,2.81701593 43.2256083,0.0542385739 39.8304342,0.0542385739 L39.8579648,0 L6.17313916,0 C2.77796506,0 0,2.76277736 0,6.13938934 L0,32.7454959 C0,36.1221079 2.77796506,38.8848853 6.17313916,38.8848853 L11.926765,38.8848853 L11.926765,51.8799782 L24.212476,38.8848853 L39.8304342,38.8848853 C43.2256083,38.8848853 46.0035734,36.1221079 46.0035734,32.7454959 L46.0035734,16.5172103"
									id="Fill-5"
									fill="#FFFFFF"
								></path>
								<path
									d="M46.0035734,16.5771857 L46.0035734,6.19362792 C46.0035734,2.81701593 43.2256083,0.0542385739 39.8304342,0.0542385739 L39.8579648,0 L6.17313916,0 C2.77796506,0 0,2.76277736 0,6.13938934 L0,32.7454959 C0,36.1221079 2.77796506,38.8848853 6.17313916,38.8848853 L11.926765,38.8848853 L11.926765,51.8799782 L24.212476,38.8848853 L39.8304342,38.8848853 C43.2256083,38.8848853 46.0035734,36.1221079 46.0035734,32.7454959 L46.0035734,16.5172103"
									id="Stroke-7"
									stroke="#333333"
									stroke-width="1.5552"
									stroke-linejoin="round"
								></path>
							</g>
							<line
								x1="34.3569122"
								y1="10.998957"
								x2="11.7267882"
								y2="10.998957"
								id="Stroke-9"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
							<line
								x1="61.6306297"
								y1="27.8791224"
								x2="57.7779239"
								y2="27.8791224"
								id="Stroke-11"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
							<line
								x1="56.1242808"
								y1="27.8791224"
								x2="52.271575"
								y2="27.8791224"
								id="Stroke-13"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
							<line
								x1="50.6179582"
								y1="27.8791224"
								x2="46.7652523"
								y2="27.8791224"
								id="Stroke-15"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
							<line
								x1="61.6306297"
								y1="33.1988052"
								x2="56.1142912"
								y2="33.1988052"
								id="Stroke-17"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
							<line
								x1="52.1812485"
								y1="33.1988052"
								x2="46.4129397"
								y2="33.1988052"
								id="Stroke-19"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
							<line
								x1="34.3569122"
								y1="16.6256614"
								x2="11.7267882"
								y2="16.6256614"
								id="Stroke-21"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
							<line
								x1="34.3569122"
								y1="22.2523919"
								x2="11.7267882"
								y2="22.2523919"
								id="Stroke-23"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
							<line
								x1="34.3569122"
								y1="27.8791224"
								x2="20.5357822"
								y2="27.8791224"
								id="Stroke-25"
								stroke="#333333"
								stroke-width="1.5552"
								stroke-linejoin="round"
							></line>
						</g>
					</g>
				</g>
			</svg>
		);
	}

	get content() {
		return (
			<section class="digi-tools-feedback-banner__container">
				{this.validContent && (
					<div class="digi-tools-feedback-banner__main-content">
						<div class="digi-tools-feedback-banner__text-content">
							<this.afHeadingLevel
								class="digi-tools-feedback-banner__heading"
								id={this.afId + '-heading'}
							>
								{this.afHeading}
							</this.afHeadingLevel>
							<p>{this.afText}</p>
						</div>
						<span class="digi-tools-feedback-banner__illustration">{this.icon}</span>
					</div>
				)}
				<div class="digi-tools-feedback-banner__links">
					{this.validLinks &&
						this.linkItems.map((item) => {
							return (
								<digi-link-external
									afHref={item.href}
									afTarget="_blank"
									afVariation="small"
									afDescribedby={this.afId + '-heading'}
								>
									{item.text}
								</digi-link-external>
							);
						})}
					<div hidden class="digi-tools-feedback-banner__slot">
						<digi-util-mutation-observer
							ref={(el) => (this._observer = el)}
							onAfOnChange={() => this.validateLinks()}
						>
							<slot></slot>
						</digi-util-mutation-observer>
					</div>
				</div>
			</section>
		);
	}

	render() {
		return (
			<div
				class={{
					'digi-tools-feedback-banner': true,
					...this.cssModifiers
				}}
			>
				{this.afType === ToolsFeedbackBannerType.FULLWIDTH && (
					<digi-layout-block afVariation={LayoutBlockVariation.TRANSPARENT}>
						{this.content}
					</digi-layout-block>
				)}
				{this.afType !== ToolsFeedbackBannerType.FULLWIDTH && this.content}
			</div>
		);
	}
}
