# digi-tools-feedback-banner



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                           | Type                                                                                                                                                                                                                               | Default                                                                |
| ---------------- | ------------------ | --------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| `afHeading`      | `af-heading`       | Sätter rubriken                                                       | `string`                                                                                                                                                                                                                           | `'Hjälp oss att bli bättre'`                                           |
| `afHeadingLevel` | `af-heading-level` | Sätt rubrikens vikt. 'h2' är förvalt.                                 | `ToolsFeedbackBannerHeadingLevel.H1 \| ToolsFeedbackBannerHeadingLevel.H2 \| ToolsFeedbackBannerHeadingLevel.H3 \| ToolsFeedbackBannerHeadingLevel.H4 \| ToolsFeedbackBannerHeadingLevel.H5 \| ToolsFeedbackBannerHeadingLevel.H6` | `ToolsFeedbackBannerHeadingLevel.H2`                                   |
| `afId`           | `af-id`            | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string`                                                                                                                                                                                                                           | `randomIdGenerator('digi-feedback-banner')`                            |
| `afText`         | `af-text`          | Sätter brödtexten                                                     | `string`                                                                                                                                                                                                                           | `'Skicka in dina synpunkter och förslag på förbättringar på (tjänst)'` |
| `afType`         | `af-type`          | Sätter typ på kortet. Kan vara 'fullwidth' eller 'grid'.              | `ToolsFeedbackBannerType.FULLWIDTH \| ToolsFeedbackBannerType.GRID`                                                                                                                                                                | `ToolsFeedbackBannerType.FULLWIDTH`                                    |


## Dependencies

### Depends on

- [digi-link-external](../../../__core/_link/link-external)
- [digi-util-mutation-observer](../../../__core/_util/util-mutation-observer)
- [digi-layout-block](../../../__core/_layout/layout-block)

### Graph
```mermaid
graph TD;
  digi-tools-feedback-banner --> digi-link-external
  digi-tools-feedback-banner --> digi-util-mutation-observer
  digi-tools-feedback-banner --> digi-layout-block
  digi-link-external --> digi-link
  digi-link-external --> digi-icon
  digi-layout-block --> digi-layout-container
  style digi-tools-feedback-banner fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
