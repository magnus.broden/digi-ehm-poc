# tool-languagepicker



<!-- Auto Generated Below -->


## Properties

| Property                        | Attribute                          | Description                                                           | Type                                                                     | Default                                          |
| ------------------------------- | ---------------------------------- | --------------------------------------------------------------------- | ------------------------------------------------------------------------ | ------------------------------------------------ |
| `afId`                          | `af-id`                            | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string`                                                                 | `randomIdGenerator('digi-tools-languagepicker')` |
| `afLanguagepickerHide`          | `af-languagepicker-hide`           | Dölj språkväljaren                                                    | `boolean`                                                                | `false`                                          |
| `afLanguagepickerItems`         | `af-languagepicker-items`          | Sätter alla valbara språk.                                            | `INavigationContextMenuItem[] \| string`                                 | `undefined`                                      |
| `afLanguagepickerStartSelected` | `af-languagepicker-start-selected` | Sätter ett förvalt värde.                                             | `number`                                                                 | `1`                                              |
| `afLanguagepickerText`          | `af-languagepicker-text`           | Texten på språkväljaren                                               | `string`                                                                 | `undefined`                                      |
| `afLanguagepickerVariation`     | `af-languagepicker-variation`      | Sätter variant. Kan vara 'default' eller 'googleTranslate'            | `LanguagepickerVariation.DEFAULT \| LanguagepickerVariation.READSPEAKER` | `LanguagepickerVariation.DEFAULT`                |
| `afListenHide`                  | `af-listen-hide`                   | Dölj "Lyssna"-knappen                                                 | `boolean`                                                                | `false`                                          |
| `afListenText`                  | `af-listen-text`                   | Texten på "Lyssna"-kanppen                                            | `string`                                                                 | `'Lyssna'`                                       |
| `afSignlangHide`                | `af-signlang-hide`                 | Dölj "Teckenspråk"-knappen                                            | `boolean`                                                                | `false`                                          |
| `afSignlangText`                | `af-signlang-text`                 | Texten på "Teckenspråk"-kanppen                                       | `string`                                                                 | `'Teckenspråk'`                                  |


## Events

| Event          | Description                                | Type               |
| -------------- | ------------------------------------------ | ------------------ |
| `afOnActive`   | När komponenten öppnas                     | `CustomEvent<any>` |
| `afOnBlur`     | När fokus sätts utanför komponenten        | `CustomEvent<any>` |
| `afOnChange`   | Vid navigering till nytt listobjekt        | `CustomEvent<any>` |
| `afOnClick`    | 'onclick'-event på någon av knapparna      | `CustomEvent<any>` |
| `afOnFocus`    | När fokus sätts på komponenten             | `CustomEvent<any>` |
| `afOnInactive` | När komponenten stängs                     | `CustomEvent<any>` |
| `afOnSelect`   | 'onclick'-event på knappelementen i listan | `CustomEvent<any>` |
| `afOnToggle`   | Toggleknappens 'onclick'-event             | `CustomEvent<any>` |


## CSS Custom Properties

| Name                                                              | Description                                                                         |
| ----------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| `--digi--tools-languagepicker--button--border-radius`             | var(--digi--border-radius--button);                                                 |
| `--digi--tools-languagepicker--button--border-radius--active`     | var(--digi--border-radius--button);                                                 |
| `--digi--tools-languagepicker--button--border-radius--focus`      | var(--digi--border-radius--button);                                                 |
| `--digi--tools-languagepicker--button--border-radius--hover`      | var(--digi--border-radius--button);                                                 |
| `--digi--tools-languagepicker--button--border-style`              | var(--digi--border-style--primary);                                                 |
| `--digi--tools-languagepicker--button--border-style--active`      | var(--digi--border-style--primary);                                                 |
| `--digi--tools-languagepicker--button--border-style--focus`       | var(--digi--border-style--primary);                                                 |
| `--digi--tools-languagepicker--button--border-style--hover`       | var(--digi--border-style--primary);                                                 |
| `--digi--tools-languagepicker--button--border-width`              | var(--digi--border-width--button);                                                  |
| `--digi--tools-languagepicker--button--border-width--active`      | var(--digi--border-width--button);                                                  |
| `--digi--tools-languagepicker--button--border-width--focus`       | var(--digi--border-width--button);                                                  |
| `--digi--tools-languagepicker--button--border-width--hover`       | var(--digi--border-width--button);                                                  |
| `--digi--tools-languagepicker--button--color--background`         | var(--digi--global--color--neutral--grayscale--lightest-4);                         |
| `--digi--tools-languagepicker--button--color--background--active` | var(--digi--global--color--cta--blue--dark);                                        |
| `--digi--tools-languagepicker--button--color--background--focus`  | var(--digi--global--color--cta--blue--dark);                                        |
| `--digi--tools-languagepicker--button--color--background--hover`  | var(--digi--global--color--cta--blue--dark);                                        |
| `--digi--tools-languagepicker--button--color--border`             | var(--digi--global--color--neutral--grayscale--lightest-4);                         |
| `--digi--tools-languagepicker--button--color--border--active`     | var(--digi--color--border--secondary);                                              |
| `--digi--tools-languagepicker--button--color--border--focus`      | var(--digi--color--border--secondary);                                              |
| `--digi--tools-languagepicker--button--color--border--hover`      | var(--digi--color--border--secondary);                                              |
| `--digi--tools-languagepicker--button--color--text`               | var(--digi--global--color--neutral--grayscale--darkest-5);                          |
| `--digi--tools-languagepicker--button--color--text--active`       | var(--digi--color--text--secondary);                                                |
| `--digi--tools-languagepicker--button--color--text--focus`        | var(--digi--color--text--secondary);                                                |
| `--digi--tools-languagepicker--button--color--text--hover`        | var(--digi--color--text--secondary);                                                |
| `--digi--tools-languagepicker--button--font-size`                 | var(--digi--global--typography--font-size--interaction-medium);                     |
| `--digi--tools-languagepicker--button--font-size--desktop`        | var(--digi--global--typography--font-size--interaction-medium);                     |
| `--digi--tools-languagepicker--button--padding`                   | var(--digi--gutter--button-block-medium) var(--digi--gutter--button-inline-medium); |


## Dependencies

### Depends on

- [digi-icon-globe](../../_icon/icon-globe)
- [digi-icon-language-outline](../../_icon/icon-language-outline)
- [digi-navigation-context-menu](../../../__core/_navigation/navigation-context-menu)
- [digi-button](../../../__core/_button/button)
- [digi-icon-volume](../../_icon/icon-volume)
- [digi-icon-sign](../../_icon/icon-sign)

### Graph
```mermaid
graph TD;
  digi-tools-languagepicker --> digi-icon-globe
  digi-tools-languagepicker --> digi-icon-language-outline
  digi-tools-languagepicker --> digi-navigation-context-menu
  digi-tools-languagepicker --> digi-button
  digi-tools-languagepicker --> digi-icon-volume
  digi-tools-languagepicker --> digi-icon-sign
  digi-navigation-context-menu --> digi-util-keydown-handler
  digi-navigation-context-menu --> digi-button
  digi-navigation-context-menu --> digi-icon
  digi-navigation-context-menu --> digi-util-mutation-observer
  style digi-tools-languagepicker fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
