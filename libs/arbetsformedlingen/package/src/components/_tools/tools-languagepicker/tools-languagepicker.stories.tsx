import { Template } from '../../../../../../shared/utils/src';
import { LanguagepickerVariation } from './tools-languagepicker-variation.enum';

export default {
	title: 'tools/digi-tools-languagepicker'
};

export const Primary = Template.bind({});
Primary.args = {
	component: 'digi-tools-languagepicker',
	'af-languagepicker-variation': LanguagepickerVariation.READSPEAKER,
	'af-languagepicker-text': 'Languages',
	'af-languagepicker-id': null,
	'af-languagepicker-start-selected': 1,
	'af-languagepicker-items': `[{"text":"العربية (Arabiska)","lang":"ar","value":"ar","dir":"rtl"},{"text":"دری (Dari)","lang":"prs","value":"prs","dir":"rtl"},{"text":"به پارسی (Persiska)","lang":"fa","value":"fa","dir":"rtl"},{"text":"English (Engelska)","lang":"en","value":"en","dir":"ltr"},{"text":"Русский (Ryska)","lang":"ru","value":"ru","dir":"ltr"},{"text":"Af soomaali (Somaliska)","lang":"so","value":"so","dir":"ltr"},{"text":"Svenska","lang":"sv","value":"sv","dir":"ltr"},{"text":"ትግርኛ (Tigrinska)","lang":"ti","value":"ti","dir":"ltr"}]`,
	'af-languagepicker-hide': false,
	'af-listen-hide': false,
	'af-signlang-hide': false,
	'af-listen-text': 'Lyssna',
	'af-signlang-text': 'Teckenspråk'
};
