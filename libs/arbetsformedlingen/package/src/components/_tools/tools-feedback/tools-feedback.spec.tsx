import { newSpecPage } from '@stencil/core/testing';
import { ToolsFeedback } from './tools-feedback';

describe('tools-feedback', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [ToolsFeedback],
			html: '<tools-feedback></tools-feedback>'
		});
		expect(root).toEqualHtml(`
      <tools-feedback>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </tools-feedback>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [ToolsFeedback],
			html: `<tools-feedback first="Stencil" last="'Don't call me a framework' JS"></tools-feedback>`
		});
		expect(root).toEqualHtml(`
      <tools-feedback first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </tools-feedback>
    `);
	});
});
