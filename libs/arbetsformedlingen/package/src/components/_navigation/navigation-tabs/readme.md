# digi-navigation-tabs

This is the navigation tabs component using digi-navigation-tab for tabs.

<!-- Auto Generated Below -->


## Properties

| Property                 | Attribute                    | Description                                         | Type      | Default                                     |
| ------------------------ | ---------------------------- | --------------------------------------------------- | --------- | ------------------------------------------- |
| `afAriaLabel`            | `af-aria-label`              | Sätter attributet 'aria-label' på tablistelementet. | `string`  | `''`                                        |
| `afId`                   | `af-id`                      | Input id attribute. Defaults to random string.      | `string`  | `randomIdGenerator('digi-navigation-tabs')` |
| `afInitActiveTab`        | `af-init-active-tab`         | Sätter initial aktiv tabb                           | `number`  | `0`                                         |
| `afPreventScrollOnFocus` | `af-prevent-scroll-on-focus` | Förhindra skroll på fokus                           | `boolean` | `false`                                     |


## Events

| Event           | Description                                                                                        | Type                      |
| --------------- | -------------------------------------------------------------------------------------------------- | ------------------------- |
| `afOnChange`    | Vid byte av flik. Returnerar indexet på aktiv flik.                                                | `CustomEvent<number>`     |
| `afOnClick`     | Vid klick på en flik-knapp.                                                                        | `CustomEvent<MouseEvent>` |
| `afOnFocus`     | Vid fokus på en flik-knapp.                                                                        | `CustomEvent<FocusEvent>` |
| `afOnReady`     | När komponenten och slotsen är laddade och initierade så skickas detta eventet.                    | `CustomEvent<any>`        |
| `afOnTabsReady` | När <digi-navigation-tab> är initierad och laddad i komponenten. Returnerar indexet på aktiv flik. | `CustomEvent<number>`     |


## Methods

### `afMSetActiveTab(tabIndex: number) => Promise<void>`

Sätter om aktiv flik.

#### Returns

Type: `Promise<void>`




## Slots

| Slot        | Description                             |
| ----------- | --------------------------------------- |
| `"default"` | Ska innehålla flera digi-navigation-tab |


## CSS Custom Properties

| Name                                                   | Description                                                        |
| ------------------------------------------------------ | ------------------------------------------------------------------ |
| `--digi--navigation-tabs--background-color--active`    | var(--digi--color--background--neutral-1);                         |
| `--digi--navigation-tabs--background-color--indicator` | var(--digi--navigation-tabs--background-color--active);            |
| `--digi--navigation-tabs--box-shadow--selected`        | inset 0 -4px 0 0 var(--digi--color--border--secondary);            |
| `--digi--navigation-tabs--color`                       | var(--digi--color--text--primary);                                 |
| `--digi--navigation-tabs--divider`                     | 1px solid var(--digi--navigation-tabs--divider--color--indicator); |
| `--digi--navigation-tabs--divider--color--indicator`   | var(--digi--global--color--neutral--grayscale--darkest-2);         |
| `--digi--navigation-tabs--padding`                     | var(--digi--padding--medium) var(--digi--padding--largest);        |
| `--digi--navigation-tabs--tab--background-color`       | transparent;                                                       |
| `--digi--navigation-tabs--tab--border`                 | none;                                                              |


## Dependencies

### Depends on

- [digi-util-keydown-handler](../../../__core/_util/util-keydown-handler)
- [digi-util-mutation-observer](../../../__core/_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-navigation-tabs --> digi-util-keydown-handler
  digi-navigation-tabs --> digi-util-mutation-observer
  style digi-navigation-tabs fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
