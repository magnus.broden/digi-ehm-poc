import {
	Component,
	Element,
	Event,
	EventEmitter,
	Prop,
	State,
	Watch,
	Method,
	h
} from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { NavigationTab } from '../navigation-tab/navigation-tab';
import { DigiUtilMutationObserverCustomEvent } from '../../../components';

/**
 * @slot default - Ska innehålla flera digi-navigation-tab
 * @swedishName Flikfält
 */
@Component({
	tag: 'digi-navigation-tabs',
	styleUrls: ['navigation-tabs.scss'],
	scoped: true
})
export class NavigationTabs {
	@Element() hostElement: HTMLStencilElement;

	@State() tabPanels = [];
	@State() activeTab: number = 0;
	@State() currentTabIndex: number = this.activeTab;

	/**
	 * Sätter attributet 'aria-label' på tablistelementet.
	 * @en Set aria-label attribute on the tablist elementet.
	 */
	@Prop() afAriaLabel: string = '';

	/**
	 * Sätter initial aktiv tabb
	 * @en Set initial active tab index. Default to 0.
	 */
	@Prop() afInitActiveTab: number = 0;

	/**
	 * Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-tabs');

	/**
	 * Förhindra skroll på fokus
	 * @en Prevent scroll on focus
	 */
	@Prop() afPreventScrollOnFocus: boolean = false;

	/**
	 * Vid byte av flik. Returnerar indexet på aktiv flik.
	 * @en When switching tabs. Returns the index of active tab.
	 */
	@Event() afOnChange: EventEmitter<number>;
	/**
	 * Vid klick på en flik-knapp.
	 * @en When clicking a tab button.
	 */
	@Event() afOnClick: EventEmitter<MouseEvent>;
	/**
	 * Vid fokus på en flik-knapp.
	 * @en When focusing a tab button.
	 */
	@Event() afOnFocus: EventEmitter<FocusEvent>;
	/**
	 * När <digi-navigation-tab> är initierad och laddad i komponenten. Returnerar indexet på aktiv flik.
	 * @en When <digi-navigation-tab> is initialized and loaded into the component. Returns the index of active tab.
	 */
	@Event() afOnTabsReady: EventEmitter<number>;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	/**
	 * Sätter om aktiv flik.
	 * @en Sets the active tab.
	 */
	@Method()
	async afMSetActiveTab(tabIndex: number) {
		this.setActiveTab(tabIndex);
	}

	@Watch('activeTab')
	changeHandler(activeTabIndex: number) {
		this.afOnChange.emit(activeTabIndex);
	}

	setTabFocus(id): void {
		id = this.tabId(this.tabPanels[id].afId);
		if (document.getElementById(`${id}`)) {
			document
				.getElementById(`${id}`)
				.focus({ preventScroll: this.afPreventScrollOnFocus });
		}
	}

	clickHandler(e: MouseEvent, i: number) {
		this.setActiveTab(i);
		this.afOnClick.emit(e);
	}

	focusHandler(e: FocusEvent) {
		const isActiveTab =
			this.tabPanels.findIndex(
				(i) => this.tabId(i.afId) === document.activeElement.id
			) === this.activeTab;
		this.currentTabIndex = isActiveTab ? this.activeTab : this.currentTabIndex;
		this.afOnFocus.emit(e);
	}

	leftHandler() {
		this.decrementCurrentTabIndex();
		this.setTabFocus(this.currentTabIndex);
	}

	rightHandler() {
		this.incrementCurrentTabIndex();
		this.setTabFocus(this.currentTabIndex);
	}

	homeHandler() {
		this.setTabFocus('0');
	}

	endHandler() {
		this.setTabFocus(this.tabPanels.length - 1);
	}

	tabId(prefix: string): string {
		return `${prefix}-tab`;
	}

	decrementCurrentTabIndex() {
		this.currentTabIndex > 0
			? (this.currentTabIndex = this.currentTabIndex - 1)
			: (this.currentTabIndex = this.tabPanels.length - 1);
	}

	incrementCurrentTabIndex() {
		this.currentTabIndex < this.tabPanels.length - 1
			? (this.currentTabIndex = this.currentTabIndex + 1)
			: (this.currentTabIndex = 0);
	}

	componentDidLoad() {
		this.getTabs();
		this.afOnReady.emit(); 
	}

	getTabs(e: DigiUtilMutationObserverCustomEvent<any> = null) {
		let tablist: any;

		tablist = this.hostElement.querySelectorAll(
			`#${this.afId}-observer > digi-navigation-tab`
		);

		if (!tablist) {
			logger.warn(
				`navigation-tabs tablist is empty, have you missed anything?`,
				this.hostElement
			);
			return;
		}

		this.tabPanels = [...tablist];

		let activeTabIndex = this.afInitActiveTab ? this.afInitActiveTab : 0;

		// If tabs are added or removed
		if (e) {
			if (!e.detail.addedNodes || !e.detail.removedNodes) {
				return;
			}
			const added = e.detail.addedNodes!.item(0),
				removed = e.detail.removedNodes!.item(0);
			activeTabIndex = this.currentTabIndex;

			if (added) {
				// If added tab is same position or before current tab, jump to the right
				Object.values(e.target.children).indexOf(added) <= this.currentTabIndex &&
					(activeTabIndex += 1);
			} else if (removed) {
				// If removed tab is before current tab or is last, jump to the left
				if (
					(removed.dataset.position == e.target.children.length &&
						removed.dataset.position == this.currentTabIndex) ||
					removed.dataset.position < this.currentTabIndex
				) {
					activeTabIndex -= 1;
				}
			}
		}

		this.setActiveTab(activeTabIndex);
		this.afOnTabsReady.emit(activeTabIndex);
		
	}

	setActiveTab(newTabIndex: number) {
		this.activeTab = newTabIndex;
		this.currentTabIndex = this.activeTab;

		this.tabPanels.forEach((tab, i: number) => {
			tab.setAttribute('af-active', i === newTabIndex);
			tab.setAttribute('data-position', i);
		});
	}

	render() {
		return (
			<div class="digi-navigation-tabs">
				<digi-util-keydown-handler
					onAfOnLeft={() => this.leftHandler()}
					onAfOnRight={() => this.rightHandler()}
					onAfOnHome={() => this.homeHandler()}
					onAfOnEnd={() => this.endHandler()}
				>
					<div
						class="digi-navigation-tabs__tablist"
						role="tablist"
						aria-label={this.afAriaLabel}
					>
						{this.tabPanels.map((tab: NavigationTab, i: number) => {
							return (
								<button
									class="digi-navigation-tabs__tab"
									role="tab"
									type="button"
									aria-selected={this.activeTab === i ? 'true' : null}
									aria-controls={tab.afId}
									tabindex={this.activeTab !== i ? '-1' : null}
									id={`${this.tabId(tab.afId)}`}
									onClick={(e: MouseEvent) => this.clickHandler(e, i)}
									onFocus={(e: FocusEvent) => this.focusHandler(e)}
								>
									{tab.afAriaLabel}
								</button>
							);
						})}
					</div>
				</digi-util-keydown-handler>
				<digi-util-mutation-observer
					onAfOnChange={(e: DigiUtilMutationObserverCustomEvent<any>) => this.getTabs(e)}
					id={`${this.afId}-observer`}
				>
					<slot></slot>
				</digi-util-mutation-observer>
			</div>
		);
	}
}
