import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-tabs', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-tabs></digi-navigation-tabs>');

    const element = await page.find('digi-navigation-tabs');
    expect(element).toHaveClass('hydrated');
  });
});
