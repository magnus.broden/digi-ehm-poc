import {
	Component,
	Event,
	EventEmitter,
	Prop,
	State,
	h,
	Method
} from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ButtonVariation } from '../../../enums-core';
import { DigiUtilResizeObserverCustomEvent } from '../../../components';

/**
 * @swedishName Paginering
 */

@Component({
	tag: 'digi-navigation-pagination',
	styleUrls: ['navigation-pagination.scss'],
	scoped: true
})
export class NavigationPagination {
	@State() currentPage: number;
	@State() twoRows: boolean;
	@State() limit: number = 6;

	setTwoRows(value: boolean) {
		this.twoRows = value;
	}
	setLimit(value: number) {
		this.limit = value;
	}


	/**
	 * Sätter initiala aktiva sidan
	 * @en Set initial start page
	 */
	@Prop() afInitActivePage: number = 1;

	/**
	 * Sätter totala mängden sidor
	 * @en Set total pages that exists
	 */
	@Prop() afTotalPages: number;

	/**
	 * Sätter startvärdet för nuvarande resultat
	 * @en Set start value of current results
	 */
	@Prop() afCurrentResultStart: number;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-pagination');

	/**
	 * Sätter slutvärdet för nuvarande resultat
	 * @en Set end value of current results
	 */
	@Prop() afCurrentResultEnd: number;

	/**
	 * Sätter totala mängden resultat
	 * @en Set total results
	 */
	@Prop() afTotalResults: number = 0;

	/**
	 * Sätter ett resultatnamn. Det används för att kommunicera vad som listas. T.ex. om värdet är 'användare' så kan det stå 'Visar 23-138 användare'.
	 * @en Set a result name. This is used to communicate what is being listed. For example, if the value is 'users', the component might say 'Showing 23-138 users'.
	 */
	@Prop() afResultName: string = '';


	/**
	* Sätt max antal sidor att visa i menyn. Anpassar sig efter komponentens container bredd.
	* @en Set max number of pages to show in menu. Adjust automatically down to fit its container width.
	*/
	@Prop({ mutable: true }) afLimit: number = 6;

	/**
	 * Vid byte av sida
	 * @en When page changes
	 */
	@Event() afOnPageChange: EventEmitter<number>;

	/**
	 * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	 * @en When the component and slots are loaded and initialized this event will trigger.
	 */
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	/**
	 * Kan användas för att manuellt sätta om den aktiva sidan.
	 * @en Can be used to set the active page.
	 */
	@Method()
	async afMSetCurrentPage(pageNumber: number) {
		this.setCurrentPage(pageNumber, false);
	}



	/**
	 * Handles which pages to display when the total amount of pages exceeds 6
	 */
	getPagination(currentPage, numPages, maxLength) {
		var startPage = Math.max(currentPage - Math.floor(maxLength / 2), 1);
		var endPage = startPage + maxLength - 1;

		if (endPage > numPages) {
			endPage = numPages;
			startPage = Math.max(endPage - maxLength + 1, 1);
		}

		var pages = [];
		for (var i = startPage; i <= endPage; i++) {
			pages.push(i);
		}

		if (startPage > 1) {
			pages.shift(); //remove first array item
			pages.unshift("1…"); //add first

		}

		if (endPage < numPages) {
			pages.pop(); //remove last array item
			pages.push("…" + numPages); //add last

		}

		return pages;
	}
	get currentInterval() {
		return this.getPagination(this.currentPage, this.afTotalPages, this.limit);
	}

	prevPage() {
		this.setCurrentPage((this.currentPage - 1), true);
	}

	nextPage() {
		this.setCurrentPage((this.currentPage + 1), true);
	}

	setCurrentPage(pageToSet = this.currentPage, emitEvent = true) {
		if (pageToSet === this.currentPage) {
			return;
		}

		this.currentPage =
			pageToSet <= 1
				? 1
				: pageToSet <= this.afTotalPages
					? pageToSet
					: this.afTotalPages;

		if (emitEvent) {
			this.afOnPageChange.emit(this.currentPage);
		}
	}

	buttonAriaLabel(pageNumber) {
		return `${pageNumber}`;
	}

	handleResizeObserver(e: DigiUtilResizeObserverCustomEvent<ResizeObserverEntry>) {

		var w = Math.round(e.detail.contentRect.width);
		if (!isNaN(w)) {

			let buttonWidth = 52;
			let minNavWidth = 570;

			if (w > minNavWidth) {
				const diff = Math.floor((w - minNavWidth) / buttonWidth);
				const minLimit = 6;
				this.setLimit(Math.min(minLimit + diff, this.afLimit));
				this.setTwoRows(false);
			} else {
				this.setTwoRows(true);
			}
		}
	}
	componentDidLoad() {
		this.afOnReady.emit();
	}
	componentWillLoad() {
		this.setCurrentPage(this.afInitActivePage, false);
	}



	isCurrentPage(currentPage: number, page: number) {
		if (currentPage === page || null) {
			return 'page';
		}
	}

	labelledby() {
		if (this.afTotalResults > 0) {
			return `
        ${this.afId}-aria-label
        ${this.afId}-result-show
        ${this.afId}-result-current
        ${this.afId}-result-of
        ${this.afId}-result-total
        ${this.afId}-result-name
      `;
		} else {
			return `${this.afId}-aria-label`;
		}
	}

	render() {
		return (
			<div class="digi-navigation-pagination">

				<digi-util-resize-observer onAfOnChange={(e) => { this.handleResizeObserver(e) }}></digi-util-resize-observer>
				<span
					id={`${this.afId}-aria-label`}
					aria-hidden="true"
					class="digi-navigation-pagination__aria-label"
				>
					Paginering
				</span>

				<nav
					aria-labelledby={this.labelledby()}
					class={`digi-navigation-pagination__pagination ${this.twoRows ? 'digi-navigation-pagination--two-rows' : ''}`}
				>

					<digi-button
						onClick={() => this.prevPage()}
						class={{
							'digi-navigation-pagination__button digi-navigation-pagination__button--previous':
								true,
							'digi-navigation-pagination__button--hidden': this.currentPage === 1
						}}
						afVariation="secondary"
						afAriaLabel='Föregående'
					>


						<digi-icon slot="icon" afName={`chevron-left`}></digi-icon>
						<span>Föregående</span>
					</digi-button>
					<div class="digi-navigation-pagination__pages">

						<ol class="digi-navigation-pagination__page-list">
							{this.currentInterval.map((pageValue) => {
								const page = isNaN(pageValue) ? parseInt(pageValue.replace('…', '')) : pageValue;
								return (
									<li class={`digi-navigation-pagination__page-list-item ${isNaN(pageValue) ? page === 1 ? "ellipsis-first" : "ellipsis-last" : ""}`}>
										<digi-button
											onClick={() => this.setCurrentPage(page)}

											afAriaLabel={this.buttonAriaLabel(page)}
											afAriaCurrent={this.isCurrentPage(this.currentPage, page)}

											class={{
												'digi-navigation-pagination__button': true,
												'digi-navigation-pagination__page-button': true,
												'digi-navigation-pagination__page-button--active':
													this.currentPage === page,

											}}
											afVariation={
												this.currentPage === page
													? ButtonVariation.PRIMARY
													: ButtonVariation.SECONDARY
											}
										>
											<span class={`digi-navigation-pagination__page-text`}>
												{page}
											</span>

										</digi-button>
									</li>
								);
							})}
						</ol>
					</div>
					<digi-button
						onClick={() => this.nextPage()}
						class={{
							'digi-navigation-pagination__button digi-navigation-pagination__button--next':
								true,
							'digi-navigation-pagination__button--hidden':
								this.currentPage === this.afTotalPages
						}}
						afVariation="secondary"
						afAriaLabel='Nästa'
					>
						<digi-icon slot="icon-secondary" afName={`chevron-right`}></digi-icon>
						<span>Nästa</span>
					</digi-button>
				</nav>

				{
					this.afTotalResults > 0 && (
						<div
							class={{
								'digi-navigation-pagination__result': true,
								'digi-navigation-pagination__result--pages': this.afTotalPages > 1
							}}
							aria-hidden={this.afTotalPages > 1 ? 'true' : 'false'}
							id={`${this.afId}-result`}
						>
							<digi-typography>
								<span id={`${this.afId}-result-show`}>Visar </span>
								<strong id={`${this.afId}-result-current`}>
									{this.afCurrentResultStart}-{this.afCurrentResultEnd}
								</strong>
								<span id={`${this.afId}-result-of`}> av </span>
								<strong id={`${this.afId}-result-total`}>{this.afTotalResults}</strong>
								{this.afResultName && (
									<span id={`${this.afId}-result-name`}>{` ${this.afResultName}`}</span>
								)}
							</digi-typography>
						</div>
					)
				}
			</div >
		);
	}
}
