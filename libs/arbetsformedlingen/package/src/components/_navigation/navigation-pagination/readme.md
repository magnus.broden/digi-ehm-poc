# digi-navigation-pagination

A pagination component.

<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                 | Description                                                                                                                                      | Type     | Default                                           |
| ---------------------- | ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | -------- | ------------------------------------------------- |
| `afCurrentResultEnd`   | `af-current-result-end`   | Sätter slutvärdet för nuvarande resultat                                                                                                         | `number` | `undefined`                                       |
| `afCurrentResultStart` | `af-current-result-start` | Sätter startvärdet för nuvarande resultat                                                                                                        | `number` | `undefined`                                       |
| `afId`                 | `af-id`                   | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                                            | `string` | `randomIdGenerator('digi-navigation-pagination')` |
| `afInitActivePage`     | `af-init-active-page`     | Sätter initiala aktiva sidan                                                                                                                     | `number` | `1`                                               |
| `afLimit`              | `af-limit`                | Sätt max antal sidor att visa i menyn. Anpassar sig efter komponentens container bredd.                                                          | `number` | `6`                                               |
| `afResultName`         | `af-result-name`          | Sätter ett resultatnamn. Det används för att kommunicera vad som listas. T.ex. om värdet är 'användare' så kan det stå 'Visar 23-138 användare'. | `string` | `''`                                              |
| `afTotalPages`         | `af-total-pages`          | Sätter totala mängden sidor                                                                                                                      | `number` | `undefined`                                       |
| `afTotalResults`       | `af-total-results`        | Sätter totala mängden resultat                                                                                                                   | `number` | `0`                                               |


## Events

| Event            | Description                                                                     | Type                  |
| ---------------- | ------------------------------------------------------------------------------- | --------------------- |
| `afOnPageChange` | Vid byte av sida                                                                | `CustomEvent<number>` |
| `afOnReady`      | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>`    |


## Methods

### `afMSetCurrentPage(pageNumber: number) => Promise<void>`

Kan användas för att manuellt sätta om den aktiva sidan.

#### Returns

Type: `Promise<void>`




## CSS Custom Properties

| Name                                                                      | Description                                                                            |
| ------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| `--digi--navigation-pagination--button--display`                          | flex;                                                                                  |
| `--digi--navigation-pagination--button--icon--width`                      | var(--digi--gutter--medium);                                                           |
| `--digi--navigation-pagination--button--justify-content--default`         | center;                                                                                |
| `--digi--navigation-pagination--button--keep-right--margin`               | 8.375rem;                                                                              |
| `--digi--navigation-pagination--button--margin`                           | 0.125rem;                                                                              |
| `--digi--navigation-pagination--button--next-button--border-radius`       | 0 var(--digi--global--border-radius--base) var(--digi--global--border-radius--base) 0; |
| `--digi--navigation-pagination--button--padding`                          | calc(var(--digi--gutter--small) + 1px) var(--digi--gutter--largest-2);                 |
| `--digi--navigation-pagination--button--page-button--border-radius`       | 0;                                                                                     |
| `--digi--navigation-pagination--button--page-button--height`              | 2.750rem; //44px                                                                       |
| `--digi--navigation-pagination--button--page-button--margin`              | 0 2px;                                                                                 |
| `--digi--navigation-pagination--button--page-button--padding`             | calc(var(--digi--gutter--medium) - 1px) calc(var(--digi--gutter--largest) - 1px);      |
| `--digi--navigation-pagination--button--page-button--padding--compressed` | calc(var(--digi--gutter--medium) - 1px) calc(var(--digi--gutter--medium) - 1px);       |
| `--digi--navigation-pagination--button--page-button--width`               | 2.750rem; //44px                                                                       |
| `--digi--navigation-pagination--button--previous-button--border-radius`   | var(--digi--global--border-radius--base) 0 0 var(--digi--global--border-radius--base); |
| `--digi--navigation-pagination--button--width`                            | 8.750rem;                                                                              |
| `--digi--navigation-pagination--page-list--margin`                        | 0;                                                                                     |
| `--digi--navigation-pagination--page-list--padding`                       | 0;                                                                                     |
| `--digi--navigation-pagination--pages--margin`                            | 0 0 var(--digi--global--spacing--smallest-4) 0; //4 eller 6px                          |
| `--digi--navigation-pagination--result--margin`                           | 0;                                                                                     |
| `--digi--navigation-pagination--result--pages--margin`                    | var(--digi--margin--smaller) 0 0 0;                                                    |
| `--digi--navigation-pagination--result--strong--font-weight`              | 600;                                                                                   |
| `--digi--navigation-pagination--result--text-align`                       | center;                                                                                |


## Dependencies

### Depends on

- [digi-util-resize-observer](../../../__core/_util/util-resize-observer)
- [digi-button](../../../__core/_button/button)
- [digi-icon](../../../__core/_icon/icon)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-navigation-pagination --> digi-util-resize-observer
  digi-navigation-pagination --> digi-button
  digi-navigation-pagination --> digi-icon
  digi-navigation-pagination --> digi-typography
  style digi-navigation-pagination fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
