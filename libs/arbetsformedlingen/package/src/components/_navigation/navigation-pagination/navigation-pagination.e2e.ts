import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-pagination', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-pagination></digi-navigation-pagination>');

    const element = await page.find('digi-navigation-pagination');
    expect(element).toHaveClass('hydrated');
  });
});
