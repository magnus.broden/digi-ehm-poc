import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-pagination',
	parameters: {
		actions: {
			handles: ['afOnPageChange']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-pagination',
	'af-init-active-page': 1,
	'af-total-pages': 6,
	'af-current-result-start': null,
	'af-id': null,
	'af-current-result-end': null,
	'af-total-results': 0,
	'af-result-name': '',
	children: ''
};
