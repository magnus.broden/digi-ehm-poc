import { Component, Event, EventEmitter, Prop, Watch, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Flik
 */
@Component({
  tag: 'digi-navigation-tab',
  styleUrls: ['navigation-tab.scss'],
  scoped: true,
})
export class NavigationTab {
  /**
   * Sätter attributet 'aria-label'
   * @en Set aria-label attribute
   */
  @Prop() afAriaLabel!: string;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-navigation-tab');

  /**
   * Sätter aktiv tabb. Detta sköts av digi-navigation-tabs som ska omsluta denna komponent.
   * @en Sets active tab (this is handled by digi-navigation-tabs which should wrap this component)
   */
  @Prop() afActive: boolean;

  /**
   * När tabben växlar mellan aktiv och inaktiv
   * @en When the tab toggles between active and inactive
   */
  @Event() afOnToggle: EventEmitter<boolean>;

  @Watch('afActive')
  toggleHandler(activeTab: boolean) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }

  render() {
    return (
      <div
        class="digi-navigation-tab"
        tabindex="0"
        role="tabpanel"
        id={this.afId}
        aria-label={this.afAriaLabel}
        hidden={!this.afActive}
      >
        <slot></slot>
      </div>
    );
  }
}
