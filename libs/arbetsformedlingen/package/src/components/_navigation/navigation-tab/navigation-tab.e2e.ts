import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-tab', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-tab></digi-navigation-tab>');

    const element = await page.find('digi-navigation-tab');
    expect(element).toHaveClass('hydrated');
  });
});
