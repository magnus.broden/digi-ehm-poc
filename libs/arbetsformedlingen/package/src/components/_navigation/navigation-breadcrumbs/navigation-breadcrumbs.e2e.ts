import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-breadcrumbs', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-breadcrumbs></digi-navigation-breadcrumbs>');

    const element = await page.find('digi-navigation-breadcrumbs');
    expect(element).toHaveClass('hydrated');
  });
});
