import {
	Component,
	Prop,
	Event,
	EventEmitter,
	Element,
	h,
	Watch
} from '@stencil/core';
import { HTMLStencilElement, State } from '@stencil/core/internal';
import { logger } from '../../../global/utils/logger';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { _t } from '@digi/shared/text';

/**
 * @slot default - Ska inehålla länkelement med href-attribut.
 * @swedishName Brödsmulor
 */
@Component({
	tag: 'digi-navigation-breadcrumbs',
	styleUrls: ['navigation-breadcrumbs.scss'],
	scoped: true
})
export class NavigationBreadcrumbs {
	@Element() hostElement: HTMLStencilElement;

	private _observer;

	@State() linkItems = [];

	@State() currentPage!: string;

	/**
	 * Sätter attributet 'aria-label' på navelementet.
	 * @en Set `aria-label` attribute on the nav element.
	 */
	@Prop() afAriaLabel: string = _t.breadcrumbs;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-breadcrumbs');

	/**
	 * Länkelementens 'onclick'-event.
	 * @en The link elements' 'onclick' events.
	 */
	@Event() afOnClick: EventEmitter;

	/**
	 * Sätter texten för den aktiva sidan
	 * @en Set text for the active page
	 */
	@Prop() afCurrentPage!: string;
	@Watch('afCurrentPage')
	onAfCurrentPage(afCurrentPage) {
		this.currentPage = afCurrentPage;
	}

	componentDidLoad() {
		this.getBreadcrumbs();
	}

	getBreadcrumbs() {
		const elements = this._observer.children;

		if (!elements || elements?.length <= 0) {
			logger.warn(`The slot contains no link elements.`, this.hostElement);
			return;
		}

		this.linkItems = [...elements]
			.filter((element) => element.tagName.toLowerCase() === 'a')
			.map((element) => {
				return {
					text: element.textContent,
					href: element.getAttribute('href') || ''
				};
			});
	}

	clickHandler(e: any) {
		this.afOnClick.emit(e);
	}

	render() {
		return (
			<nav class="digi-navigation-breadcrumbs" aria-label={this.afAriaLabel}>
				<ol class="digi-navigation-breadcrumbs__items">
					{this.linkItems.map((item) => {
						return (
							<li class="digi-navigation-breadcrumbs__item">
								<a
									class="digi-navigation-breadcrumbs__link"
									onClick={(e) => this.clickHandler(e)}
									href={item.href}
								>
									{item.text}
								</a>
							</li>
						);
					})}
					<li class="digi-navigation-breadcrumbs__item" aria-current="page">
						{this.currentPage ?? this.afCurrentPage}
					</li>
					<digi-util-mutation-observer
						onAfOnChange={() => this.getBreadcrumbs()}
						ref={(el) => (this._observer = el)}
						hidden
					>
						<slot></slot>
					</digi-util-mutation-observer>
				</ol>
			</nav>
		);
	}
}
