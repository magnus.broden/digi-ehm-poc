import { Component, Event, EventEmitter, Prop, h, Element } from '@stencil/core';
import { Host, HTMLStencilElement } from '@stencil/core/internal';
import { LinkButtonSize } from './link-button-size.enum';
import { LinkButtonVariation } from './link-button-variation.enum';

/**
 * @slot default - Ska vara en textnod
 * @enums LinkButtonVariation - link-button-variation.enum.ts
 * @enums LinkButtonSize - link-button-size.enum.ts
 * @swedishName Länkknapp
 */
@Component({
  tag: 'digi-link-button',
  styleUrls: ['link-button.scss'],
  scoped: true
})
export class LinkButton {
  @Element() hostElement: HTMLStencilElement;

  private _link: any;

  /**
   * Sätter attributet 'href'
   * @en Set `href` attribute
   */
  @Prop() afHref!: string;

  /**
   * Sätter variant. Kan vara 'primary', 'secondary', 'primary-alt' eller 'secondary-alt'
   * @en Set variation. Can be 'primary', 'secondary', 'primary-alt' or 'secondary-alt'
   */
  @Prop() afVariation: LinkButtonVariation = LinkButtonVariation.PRIMARY;

  /**
   * Sätter storlek. Kan vara 'medium' eller 'large'
   * @en Set size. Can be 'medium' or 'large'
   */
  @Prop() afSize: LinkButtonSize = LinkButtonSize.MEDIUM;

  /**
   * Kringgår länkens vanliga beteende.
   * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
   * @en Override default link behavior. Should only be used if default link behaviour is a problem with e.g. routing
   */
  @Prop() afOverrideLink: boolean = false;

  /**
   * Sätt till true om du använder Angular, se exempelkod under 'Översikt'
   * @en Set to true if using Angular
   */
  @Prop() afLinkContainer: boolean = false;

  /**
   * Länkelementets 'onclick'-event.
   * @en The link element's 'onclick' event.
   */
  @Event() afOnClick: EventEmitter<MouseEvent>;

  /**
   * Dölj ikonen.
   * @en Hide icon.
   */
  @Prop() afHideIcon: boolean = false;

  /**
   * Fullbredd.
   * @en Fullwidth.
   */
  @Prop() afFullwidth: boolean = false;

  clickLinkHandler(e: MouseEvent) {
    if (this.afOverrideLink) {
      e.preventDefault();
    }
    this.afOnClick.emit(e);
  }

  initRouting() {
    const tabIndex = this.hostElement.getAttribute('tabIndex');
    !!tabIndex && tabIndex === '0' && this._link.setAttribute('tabIndex', '-1');
  }

  componentDidLoad() {
    this.initRouting();
  }

  get cssModifiers() {
    return {
      'digi-link-button--primary':
        this.afVariation === LinkButtonVariation.PRIMARY,
      'digi-link-button--secondary':
        this.afVariation === LinkButtonVariation.SECONDARY,
      'digi-link-button--primary-alt':
        this.afVariation === LinkButtonVariation.PRIMARY_ALT,
      'digi-link-button--secondary-alt':
        this.afVariation === LinkButtonVariation.SECONDARY_ALT,
      'digi-link-button--medium': this.afSize === LinkButtonSize.MEDIUM,
      'digi-link-button--medium-large': this.afSize === LinkButtonSize.MEDIUMLARGE,
      'digi-link-button--large': this.afSize === LinkButtonSize.LARGE,
      'digi-link-button--hide-icon': this.afHideIcon,
      'digi-link-button--fullwidth': this.afFullwidth
    };
  }

  render() {
    return (
      <Host
        class={{
          'digi-link-button': true,
          ...this.cssModifiers
        }}
      >
        {!this.afLinkContainer && (
          <a
            href={this.afHref}
            onClick={(e) => this.clickLinkHandler(e)}
            ref={(el) => this._link = el}
          >
            <span class="digi-link-button__text">
              <slot></slot>
            </span>
            <digi-icon-chevron-right class="digi-link-button__icon"></digi-icon-chevron-right>
          </a>
          )}
          {this.afLinkContainer && (
            <slot></slot>
          )}
      </Host>
    );
  }
}
