import { LinkButtonSize } from './link-button-size.enum';
import { LinkButtonVariation } from './link-button-variation.enum';
import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'link/digi-link-button',
	parameters: {
		actions: {
			handles: ['afOnClick']
		}
	},
	argTypes: {
		'af-variation': enumSelect(LinkButtonVariation),
		'af-size': enumSelect(LinkButtonSize)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-link-button',
	'af-href': 'A link is required',
	'af-variation': LinkButtonVariation.PRIMARY,
	'af-size': LinkButtonSize.MEDIUM,
	/* html */
	children: 'I am a button link'
};
