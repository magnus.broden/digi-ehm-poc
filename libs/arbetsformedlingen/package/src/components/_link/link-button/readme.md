# digi-link-button

WORK IN PROGRESS

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute           | Description                                                                                                                           | Type                                                                                                                                   | Default                       |
| --------------------- | ------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| `afFullwidth`         | `af-fullwidth`      | Fullbredd.                                                                                                                            | `boolean`                                                                                                                              | `false`                       |
| `afHideIcon`          | `af-hide-icon`      | Dölj ikonen.                                                                                                                          | `boolean`                                                                                                                              | `false`                       |
| `afHref` _(required)_ | `af-href`           | Sätter attributet 'href'                                                                                                              | `string`                                                                                                                               | `undefined`                   |
| `afLinkContainer`     | `af-link-container` | Sätt till true om du använder Angular, se exempelkod under 'Översikt'                                                                 | `boolean`                                                                                                                              | `false`                       |
| `afOverrideLink`      | `af-override-link`  | Kringgår länkens vanliga beteende. Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande. | `boolean`                                                                                                                              | `false`                       |
| `afSize`              | `af-size`           | Sätter storlek. Kan vara 'medium' eller 'large'                                                                                       | `LinkButtonSize.LARGE \| LinkButtonSize.MEDIUM \| LinkButtonSize.MEDIUMLARGE`                                                          | `LinkButtonSize.MEDIUM`       |
| `afVariation`         | `af-variation`      | Sätter variant. Kan vara 'primary', 'secondary', 'primary-alt' eller 'secondary-alt'                                                  | `LinkButtonVariation.PRIMARY \| LinkButtonVariation.PRIMARY_ALT \| LinkButtonVariation.SECONDARY \| LinkButtonVariation.SECONDARY_ALT` | `LinkButtonVariation.PRIMARY` |


## Events

| Event       | Description                     | Type                      |
| ----------- | ------------------------------- | ------------------------- |
| `afOnClick` | Länkelementets 'onclick'-event. | `CustomEvent<MouseEvent>` |


## Slots

| Slot        | Description         |
| ----------- | ------------------- |
| `"default"` | Ska vara en textnod |


## CSS Custom Properties

| Name                                                        | Description                                                  |
| ----------------------------------------------------------- | ------------------------------------------------------------ |
| `--digi--link-button--background--primary--default`         | var(--digi--color--background--inverted-1);                  |
| `--digi--link-button--background--primary--hover`           | var(--digi--color--background--inverted-6);                  |
| `--digi--link-button--background--primary-alt--default`     | var(--digi--color--background--neutral-2);                   |
| `--digi--link-button--background--primary-alt--hover`       | var(--digi--color--background--inverted-4);                  |
| `--digi--link-button--background--secondary--default`       | var(--digi--color--background--primary);                     |
| `--digi--link-button--background--secondary--hover`         | var(--digi--color--background--neutral-8);                   |
| `--digi--link-button--background--secondary-alt--default`   | var(--digi--color--background--inverted-1);                  |
| `--digi--link-button--background--secondary-alt--hover`     | var(--digi--color--background--inverted-4);                  |
| `--digi--link-button--border`                               | var(--digi--border-width--secondary) solid;                  |
| `--digi--link-button--border-color--primary--default`       | var(--digi--color--background--inverted-1);                  |
| `--digi--link-button--border-color--primary--hover`         | var(--digi--color--background--inverted-6);                  |
| `--digi--link-button--border-color--primary-alt--default`   | var(--digi--color--background--neutral-2);                   |
| `--digi--link-button--border-color--primary-alt--hover`     | var(--digi--color--background--inverted-4);                  |
| `--digi--link-button--border-color--secondary--default`     | var(--digi--color--border--secondary);                       |
| `--digi--link-button--border-color--secondary--hover`       | var(--digi--color--border--secondary);                       |
| `--digi--link-button--border-color--secondary-alt--default` | var(--digi--color--border--tertiary);                        |
| `--digi--link-button--border-color--secondary-alt--hover`   | var(--digi--color--background--inverted-4);                  |
| `--digi--link-button--border-radius`                        | var(--digi--global--border-radius--largest-2);               |
| `--digi--link-button--color--primary--default`              | var(--digi--color--text--inverted);                          |
| `--digi--link-button--color--primary--hover`                | var(--digi--color--text--inverted);                          |
| `--digi--link-button--color--primary-alt--default`          | var(--digi--color--text--secondary);                         |
| `--digi--link-button--color--primary-alt--hover`            | var(--digi--color--text--secondary);                         |
| `--digi--link-button--color--secondary--default`            | var(--digi--color--text--secondary);                         |
| `--digi--link-button--color--secondary--hover`              | var(--digi--color--text--secondary);                         |
| `--digi--link-button--color--secondary-alt--default`        | var(--digi--color--background--neutral-2);                   |
| `--digi--link-button--color--secondary-alt--hover`          | var(--digi--color--text--secondary);                         |
| `--digi--link-button--font-family`                          | var(--digi--global--typography--font-family--default);       |
| `--digi--link-button--font-size--large`                     | var(--digi--typography--button--font-size--desktop-large);   |
| `--digi--link-button--font-size--medium`                    | var(--digi--typography--button--font-size--desktop);         |
| `--digi--link-button--font-weight`                          | var(--digi--typography--button--font-weight--desktop);       |
| `--digi--link-button--icon--margin-left`                    | var(--digi--margin--large);                                  |
| `--digi--link-button--padding--large`                       | var(--digi--gutter--largest) var(--digi--gutter--largest-3); |
| `--digi--link-button--padding--medium`                      | var(--digi--gutter--medium) var(--digi--gutter--largest);    |
| `--digi--link-button--padding--medium-large`                | var(--digi--gutter--larger) var(--digi--gutter--largest-3);  |


## Dependencies

### Depends on

- [digi-icon-chevron-right](../../../__core/_icon/icon-chevron-right)

### Graph
```mermaid
graph TD;
  digi-link-button --> digi-icon-chevron-right
  style digi-link-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
