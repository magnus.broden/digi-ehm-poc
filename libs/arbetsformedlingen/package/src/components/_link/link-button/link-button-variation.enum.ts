export enum LinkButtonVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  PRIMARY_ALT = 'primary-alt',
  SECONDARY_ALT = 'secondary-alt'
}
