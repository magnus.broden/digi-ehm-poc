import { newE2EPage } from '@stencil/core/testing';

describe('digi-link-button', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-link-button></digi-link-button>');

    const element = await page.find('digi-link-button');
    expect(element).toHaveClass('hydrated');
  });
});
