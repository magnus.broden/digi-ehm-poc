import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-list-ul',
	styleUrls: ['icon-list-ul.scss'],
	scoped: true
})
export class IconListUl {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-list-ul"
				width="26"
				height="21"
				viewBox="0 0 26 21"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-list-ul__shape"
					d="M4.875 2.423a2.43 2.43 0 01-2.438 2.423A2.43 2.43 0 010 2.423 2.43 2.43 0 012.438 0a2.43 2.43 0 012.437 2.423zM2.437 8.077A2.43 2.43 0 000 10.5a2.43 2.43 0 002.438 2.423A2.43 2.43 0 004.874 10.5a2.43 2.43 0 00-2.438-2.423zm0 8.077A2.43 2.43 0 000 18.577 2.43 2.43 0 002.438 21a2.43 2.43 0 002.437-2.423 2.43 2.43 0 00-2.438-2.423zM7.314 4.24h17.875A.81.81 0 0026 3.433v-2.02a.81.81 0 00-.813-.807H7.313a.81.81 0 00-.812.807v2.02a.81.81 0 00.813.807zm0 8.077h17.875A.81.81 0 0026 11.51V9.49a.81.81 0 00-.813-.807H7.313a.81.81 0 00-.812.807v2.02a.81.81 0 00.813.807zm0 8.077h17.875a.81.81 0 00.812-.807v-2.02a.81.81 0 00-.813-.807H7.313a.81.81 0 00-.812.807v2.02a.81.81 0 00.813.807z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
