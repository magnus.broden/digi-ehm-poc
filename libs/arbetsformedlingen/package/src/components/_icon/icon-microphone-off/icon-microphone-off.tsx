import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-microphone-off',
	styleUrls: ['icon-microphone-off.scss'],
	scoped: true
})
export class IconMicrophoneOff {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	


	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-microphone-off"
				width="48"
				height="48"
				viewBox="0 0 33.9992407 47.999"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				fill="currentColor"
				id="Icon"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<g id="Pågående" stroke="none" stroke-width="1" fill-rule="evenodd">
					<g id="Artboard" transform="translate(-1651.000000, -853.000000)" fill-rule="nonzero">
						<g id="mic-bred-off-v2" transform="translate(1651.000000, 853.000000)">
							<path d="M14.8144981,32.556 L7.52549813,22.356 L7.52549813,26.497 C7.52549813,29.838 10.2244981,32.556 13.5414981,32.556 L14.8144981,32.556 Z" id="Path"></path>
							<path d="M10.3634981,34.398 C7.34749813,34.398 4.89349813,31.926 4.89349813,28.888 L4.89349813,18.118 C4.89349813,17.035 4.02149813,16.157 2.94649813,16.157 C1.87149813,16.157 0.999498134,17.035 0.999498134,18.118 L0.999498134,28.888 C0.999498134,34.089 5.20049813,38.319 10.3634981,38.319 L15.0524981,38.319 L15.0524981,46.039 C15.0524981,47.122 15.9244981,47.999 16.9994981,47.999 C18.0744981,47.999 18.9464981,47.122 18.9464981,46.039 L18.9464981,38.338 L16.1314981,34.398 L10.3634981,34.398 L10.3634981,34.398 Z" id="Path"></path>
							<path d="M33.6524981,44.013 L28.5614981,36.889 C31.2204981,35.227 32.9994981,32.268 32.9994981,28.889 L32.9994981,18.119 C32.9994981,17.036 32.1274981,16.158 31.0524981,16.158 C29.9774981,16.158 29.1054981,17.036 29.1054981,18.119 L29.1054981,28.889 C29.1054981,30.959 27.9654981,32.764 26.2854981,33.705 L24.3984981,31.065 C25.6684981,29.956 26.4724981,28.321 26.4724981,26.498 L26.4724981,6.059 C26.4724981,2.718 23.7734981,0 20.4564981,0 L13.5414981,0 C10.2234981,0 7.52549813,2.718 7.52549813,6.059 L7.52549813,7.454 L3.49949813,1.819 C2.88949813,0.954 1.69149813,0.741 0.820498134,1.34 C0.484498134,1.57 0.246498134,1.888 0.116498134,2.242 C-0.0905018659,2.805 -0.0265018659,3.456 0.347498134,3.987 L30.4994981,46.181 C30.7344981,46.515 31.0574981,46.751 31.4154981,46.881 C31.9854981,47.089 32.6434981,47.027 33.1774981,46.66 C33.5134981,46.429 33.7514981,46.111 33.8814981,45.757 C34.0904981,45.195 34.0254981,44.544 33.6524981,44.013 Z M11.4194981,6.059 C11.4194981,4.88 12.3714981,3.922 13.5414981,3.922 L20.4564981,3.922 C21.6264981,3.922 22.5784981,4.881 22.5784981,6.059 L22.5784981,26.498 C22.5784981,27.009 22.3924981,27.473 22.0944981,27.841 L11.4194981,12.903 L11.4194981,6.059 Z" id="Shape"></path>
						</g>
					</g>
				</g></svg>
		);
	}
}
