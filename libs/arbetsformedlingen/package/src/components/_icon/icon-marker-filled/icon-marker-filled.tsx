import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-marker-filled',
	styleUrl: 'icon-marker-filled.scss',
	scoped: true
})
export class IconMarkerFilled {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-marker-filled"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-marker-filled__shape"
					d="M22.150125,47.031551 C8.5284375,27.2841496 6,25.2574626 6,18 C6,8.05884178 14.0588438,0 24,0 C33.9411562,0 42,8.05884178 42,18 C42,25.2574626 39.4715625,27.2841496 25.849875,47.031551 C24.9559688,48.3228632 23.0439375,48.3227695 22.150125,47.031551 Z M24,25.4999974 C28.1421562,25.4999974 31.5,22.1421508 31.5,17.9999974 C31.5,13.8578404 28.1421562,10.4999974 24,10.4999974 C19.8578437,10.4999974 16.5,13.8578404 16.5,17.9999974 C16.5,22.1421508 19.8578437,25.4999974 24,25.4999974 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
