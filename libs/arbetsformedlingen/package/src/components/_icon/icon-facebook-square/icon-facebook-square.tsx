import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-facebook-square',
	styleUrls: ['icon-facebook-square.scss'],
	scoped: true
})
export class IconFacebookSquare {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-facebook-square"
				width="22"
				height="22"
				viewBox="0 0 22 22"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-facebook-square__shape"
					d="M22 2.357v17.286A2.358 2.358 0 0119.643 22h-4.189v-8.702h2.976l.427-3.32h-3.403v-2.12c0-.963.265-1.616 1.645-1.616h1.758V3.275a23.636 23.636 0 00-2.563-.132c-2.534 0-4.273 1.547-4.273 4.39v2.45H9.036v3.32h2.99V22H2.357A2.358 2.358 0 010 19.643V2.357A2.358 2.358 0 012.357 0h17.286A2.358 2.358 0 0122 2.357z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
