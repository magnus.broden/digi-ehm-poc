import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-lightbulb',
	styleUrls: ['icon-lightbulb.scss'],
	scoped: true
})
export class IconLightbulb {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	
	
	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-lightbulb"
				width="18"
				height="26"
				viewBox="0 0 18 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-lightbulb__shape"
					d="M13.09 21.734v1.422c0 .53-.34.982-.817 1.15v.475c0 .673-.55 1.219-1.228 1.219h-4.09a1.223 1.223 0 01-1.228-1.219v-.476a1.22 1.22 0 01-.818-1.149v-1.422c0-.336.275-.609.614-.609h6.954c.34 0 .614.273.614.61zM5.774 19.5c-.507 0-.963-.309-1.144-.778C2.644 13.586 0 14.104 0 8.937 0 4.003 4.03 0 9 0s9 4.002 9 8.938c0 5.166-2.644 4.648-4.629 9.784-.181.47-.637.778-1.144.778H5.773zM4.909 8.937c0-2.24 1.835-4.062 4.091-4.062a.815.815 0 00.818-.813A.815.815 0 009 3.25c-3.158 0-5.727 2.551-5.727 5.688 0 .448.366.812.818.812a.815.815 0 00.818-.813z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
