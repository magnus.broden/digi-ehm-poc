import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-screensharing-off',
	styleUrls: ['icon-screensharing-off.scss'],
	scoped: true
})
export class IconScreensharingOff {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-screensharing-off"
				width="48"
				height="48"
				fill="currentColor"
				viewBox="0 2 50 47"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				{/* <path
					class="digi-icon-videocamera__shape"
					d="m43.993 26.226-9.856-6.671v-7.11l9.856-6.67v20.45ZM3.964 28.022h26.209V3.979H3.964v24.043ZM46.935.232a2.006 2.006 0 0 0-2.062.113L34.138 7.612V2.448A2.429 2.429 0 0 0 31.709.025H2.429A2.428 2.428 0 0 0 0 2.448v27.106a2.429 2.429 0 0 0 2.429 2.423h29.28a2.428 2.428 0 0 0 2.429-2.423v-5.165l10.735 7.267A2.002 2.002 0 0 0 48 30V2.003c0-.742-.41-1.422-1.065-1.77Z"
					fill="currentColor"
					fill-rule="evenodd"
					
				/> */}
    <g id="dela-basic-disabl" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g transform="translate(-1644.000000, -943.000000)">
            <g transform="translate(1644.000000, 943.000000)">
                <path d="M3.773,38.0037 L3.773,12.9057 L8.224,12.9057 L5.56,8.9997 L2.327,8.9997 C1.044,8.9997 0,10.0807 0,11.4087 L0,39.5007 C0,40.8287 1.044,41.9097 2.327,41.9097 L28,41.9097 L25.336,38.0037 L3.773,38.0037 Z" id="Fill-1"></path>
                <g transform="translate(5.902100, 0.000000)">
                    <path d="M38.2229002,38.0037 L29.5449002,38.0037 L20.0789002,24.4987 L20.0789002,21.3337 L23.0639002,25.3257 C23.4399002,25.8297 24.0169002,26.0957 24.6019002,26.0957 C25.0009002,26.0957 25.4049002,25.9707 25.7489002,25.7127 C26.5979002,25.0787 26.7699002,23.8757 26.1359002,23.0267 L20.1659002,15.0447 C20.1109002,14.9717 20.0519002,14.9017 19.9859002,14.8377 C19.5319002,14.3827 18.9469002,14.1447 18.3509002,14.0997 C18.2869002,14.0937 18.2249002,14.0807 18.1589002,14.0807 C18.0959002,14.0807 18.0369002,14.0937 17.9759002,14.0997 C17.3769002,14.1417 16.7889002,14.3807 16.3329002,14.8377 C16.2719002,14.8987 16.2159002,14.9627 16.1639002,15.0297 L14.7449002,16.8897 L11.9519002,12.9057 L38.2229002,12.9057 L38.2229002,38.0037 Z M39.7079002,8.9997 L9.21390024,8.9997 L3.47890024,0.8187 C2.87290024,-0.0463 1.68190024,-0.2593 0.815900243,0.3397 C0.481900243,0.5697 0.245900243,0.8877 0.115900243,1.2417 C-0.0900997574,1.8047 -0.0260997574,2.4557 0.344900243,2.9867 L31.3239002,47.1807 C31.5579002,47.5147 31.8789002,47.7507 32.2349002,47.8807 C32.8009002,48.0887 33.4559002,48.0267 33.9869002,47.6597 C34.3209002,47.4287 34.5569002,47.1107 34.6869002,46.7567 C34.8929002,46.1947 34.8289002,45.5427 34.4579002,45.0117 L32.2829002,41.9087 L39.7079002,41.9087 C41.0269002,41.9087 42.0999002,40.8277 42.0999002,39.4997 L42.0999002,11.4077 C42.0999002,10.0807 41.0269002,8.9997 39.7079002,8.9997 L39.7079002,8.9997 Z" id="Fill-3"></path>
                </g>
                <path d="M18.7141,25.6577 L16.4371,22.4087 L15.9771,23.0127 C15.3341,23.8557 15.4971,25.0597 16.3381,25.7017 C17.0571,26.2507 18.0411,26.2087 18.7141,25.6577" id="Fill-6"></path>
                <path d="M25.536,35.3897 L22.142,30.5477 L22.142,34.1757 C22.142,35.2357 23.001,36.0947 24.061,36.0947 C24.658,36.0947 25.184,35.8167 25.536,35.3897" id="Fill-8"></path>
            </g>
        </g>
    </g>
		</svg>);
	}
}
