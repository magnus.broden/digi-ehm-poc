import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-building-outline',
	styleUrl: 'icon-building-outline.scss',
	scoped: true
})
export class IconBuildingOutline {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-building-outline"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-building-outline__shape"
					d="M15.52,13.875 L15.52,10.125 C15.52,9.50625 16.006,9 16.6,9 L20.2,9 C20.794,9 21.28,9.50625 21.28,10.125 L21.28,13.875 C21.28,14.49375 20.794,15 20.2,15 L16.6,15 C16.006,15 15.52,14.49375 15.52,13.875 Z M28.12,15 L31.72,15 C32.314,15 32.8,14.49375 32.8,13.875 L32.8,10.125 C32.8,9.50625 32.314,9 31.72,9 L28.12,9 C27.526,9 27.04,9.50625 27.04,10.125 L27.04,13.875 C27.04,14.49375 27.526,15 28.12,15 Z M16.6,24 L20.2,24 C20.794,24 21.28,23.49375 21.28,22.875 L21.28,19.125 C21.28,18.50625 20.794,18 20.2,18 L16.6,18 C16.006,18 15.52,18.50625 15.52,19.125 L15.52,22.875 C15.52,23.49375 16.006,24 16.6,24 Z M28.12,24 L31.72,24 C32.314,24 32.8,23.49375 32.8,22.875 L32.8,19.125 C32.8,18.50625 32.314,18 31.72,18 L28.12,18 C27.526,18 27.04,18.50625 27.04,19.125 L27.04,22.875 C27.04,23.49375 27.526,24 28.12,24 Z M21.28,31.875 L21.28,28.125 C21.28,27.50625 20.794,27 20.2,27 L16.6,27 C16.006,27 15.52,27.50625 15.52,28.125 L15.52,31.875 C15.52,32.49375 16.006,33 16.6,33 L20.2,33 C20.794,33 21.28,32.49375 21.28,31.875 Z M28.12,33 L31.72,33 C32.314,33 32.8,32.49375 32.8,31.875 L32.8,28.125 C32.8,27.50625 32.314,27 31.72,27 L28.12,27 C27.526,27 27.04,27.50625 27.04,28.125 L27.04,31.875 C27.04,32.49375 27.526,33 28.12,33 Z M44.32,44.625 L44.32,48 L4,48 L4,44.625 C4,44.00625 4.486,43.5 5.08,43.5 L6.835,43.5 L6.835,2.25 C6.835,1.003125 7.798,0 8.995,0 L39.325,0 C40.522,0 41.485,1.003125 41.485,2.25 L41.485,43.5 L43.24,43.5 C43.834,43.5 44.32,44.00625 44.32,44.625 Z M11.155,43.40625 L21.28,43.40625 L21.28,37.125 C21.28,36.50625 21.766,36 22.36,36 L25.96,36 C26.554,36 27.04,36.50625 27.04,37.125 L27.04,43.40625 L37.165,43.40625 L37.165,4.59375 L11.2,4.5 L11.155,43.40625 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
