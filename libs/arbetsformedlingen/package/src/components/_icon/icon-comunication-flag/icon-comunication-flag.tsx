import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-comunication-flag',
	styleUrl: 'icon-comunication-flag.scss',
	scoped: true
})
export class IconComunicationFlag {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-comunication-flag"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-comunication-flag__shape"
					d="M31.5163125,7.5 C26.9101875,7.5 22.7689687,4.5 16.3369687,4.5 C13.4025,4.5 10.8710625,5.10769191 8.769375,5.92200433 C8.97767241,5.29677849 9.04622225,4.63345798 8.9701875,3.97884828 C8.72503125,1.835161 6.95315625,0.149442424 4.80009375,0.00975493793 C2.1789375,-0.160307545 0,1.91541099 0,4.5 C0,6.1653793 0.90534375,7.61831666 2.25,8.39644157 L2.25,46.5 C2.25,47.3284688 2.92153125,48 3.75,48 L5.25,48 C6.07846875,48 6.75,47.3284688 6.75,46.5 L6.75,38.6772197 C10.3002187,37.0575011 13.4305312,36 18.7336875,36 C23.3398125,36 27.4810312,39 33.9130313,39 C39.3954375,39 43.4729063,36.8796574 45.9644063,35.2517826 C47.2355625,34.4213451 48,33.0047828 48,31.4864079 L48,8.99409776 C48,5.76356685 44.694,3.57834832 41.72775,4.85812944 C38.3618437,6.31022304 34.8413438,7.5 31.5163125,7.5 Z M43.5,31.500004 C41.4578437,32.9448765 37.79775,34.500004 33.9130313,34.500004 C28.2931875,34.500004 24.3503438,31.500004 18.7336875,31.500004 C14.6685938,31.500004 9.69815625,32.3815329 6.75,33.7500015 L6.75,12.000004 C8.79225,10.5551289 12.45225,9.00000401 16.3369688,9.00000401 C21.9568125,9.00000401 25.8996563,12.000004 31.5163125,12.000004 C35.5729688,12.000004 40.5463125,10.3719414 43.5,9.00000401 L43.5,31.500004 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
