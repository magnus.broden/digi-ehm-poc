import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-phone-hangup',
	styleUrls: ['icon-phone-hangup.scss'],
	scoped: true
})
export class IconPhoneHangup {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-phone-hangup"
				width="48"
				height="48"
				viewBox="0 0 48.0003198 19.9993764"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				fill="currentColor"
				id="Icon"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<g id="Pågående" stroke="none" stroke-width="1" fill-rule="evenodd">
					<g id="Artboard" transform="translate(-1209.000000, -1002.000000)" fill-rule="nonzero">
						<g id="phone-hangup" transform="translate(1209.000000, 1002.000000)">
							<path d="M23.9487661,7.11703244 C20.2367661,7.06703244 16.2697661,8.48203244 15.1917661,9.78403244 L15.2687661,16.1310324 C15.2517661,16.2680324 15.5617661,17.0400324 11.3617661,18.3660324 C11.3617661,18.3660324 4.31776613,21.2270324 3.24376613,19.3760324 L0.211766132,13.0690324 C-1.46223387,9.91303244 6.82776613,-0.11696756 23.6837661,0.0010324397 L24.3167661,0.0010324397 C41.1727661,-0.11596756 49.4617661,9.91403244 47.7887661,13.0690324 L44.7567661,19.3760324 C43.6817661,21.2270324 36.6387661,18.3660324 36.6387661,18.3660324 C32.4377661,17.0390324 32.7487661,16.2680324 32.7317661,16.1310324 L32.8087661,9.78403244 C31.7307661,8.48303244 27.7637661,7.06803244 24.0517661,7.11703244" id="Path"></path>
						</g>
					</g>
				</g></svg>
		);
	}
}
