import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-update',
	styleUrl: 'icon-update.scss',
	scoped: true
})
export class IconUpdate {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-update"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-update__shape"
					d="M46.0175806,1.98241935 L40.9693548,7.03064516 C36.6266129,2.68712903 30.6275806,0 24,0 C11.1510968,0 0.660387097,10.0972258 0.03,22.7896452 C-0.00280645161,23.4494516 0.52983871,24 1.19041935,24 L3.90445161,24 C4.51925806,24 5.02829032,23.5207742 5.06312903,22.9070323 C5.62848387,12.9337742 13.8825484,5.03225806 24,5.03225806 C29.2415806,5.03225806 33.9829355,7.15383871 37.4142581,10.5857419 L32.1760645,15.8240323 C31.4444516,16.5556452 31.9625806,17.8064516 32.9971935,17.8064516 L46.8387097,17.8064516 C47.4800323,17.8064516 48,17.2864839 48,16.6451613 L48,2.80364516 C48,1.76903226 46.7490968,1.25090323 46.0175806,1.98241935 Z M46.8095806,24 L44.0955484,24 C43.4807419,24 42.9717097,24.4792258 42.936871,25.0929677 C42.3715161,35.0662258 34.1174516,42.9677419 24,42.9677419 C18.7584194,42.9677419 14.0170645,40.8461613 10.5857419,37.4142581 L15.8239355,32.1759677 C16.5555484,31.4443548 16.0374194,30.1935484 15.0028065,30.1935484 L1.16129032,30.1935484 C0.519967742,30.1935484 0,30.7135161 0,31.3548387 L0,45.1963548 C0,46.2309677 1.25090323,46.7490968 1.98241935,46.0174839 L7.03064516,40.9693548 C11.3733871,45.312871 17.3725161,48 24,48 C36.849,48 47.3396129,37.9027742 47.97,25.2103548 C48.0028065,24.5505484 47.4701613,24 46.8095806,24 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
