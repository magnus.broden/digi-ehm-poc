import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-screensharing',
	styleUrls: ['icon-screensharing.scss'],
	scoped: true
})
export class IconScreensharing {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-screensharing"
				width="48"
				height="48"
				fill="currentColor"
				viewBox="0 0 50 32.91"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				id="Icon"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
    <g id="Pågående" stroke="none" stroke-width="1">
        <g id="Artboard" transform="translate(-1583.000000, -952.000000)">
            <g id="screensharing" transform="translate(1583.000000, 952.000000)">
                <path d="M31.909,14.027 L25.939,6.045 C25.884,5.972 25.825,5.902 25.759,5.838 C25.305,5.383 24.72,5.145 24.124,5.1 C24.06,5.094 23.998,5.081 23.932,5.081 C23.869,5.081 23.81,5.094 23.749,5.1 C23.15,5.142 22.562,5.381 22.106,5.838 C22.045,5.899 21.989,5.963 21.937,6.03 L15.847,14.012 C15.204,14.855 15.367,16.059 16.208,16.701 C17.049,17.343 18.253,17.183 18.897,16.34 L22.012,12.257 L22.012,25.176 C22.012,26.236 22.871,27.095 23.931,27.095 C24.991,27.095 25.85,26.236 25.85,25.176 L25.85,12.333 L28.835,16.325 C29.211,16.829 29.788,17.095 30.373,17.095 C30.772,17.095 31.176,16.97 31.52,16.712 C32.371,16.079 32.544,14.876 31.909,14.027" id="Path"></path>
                <path d="M3.877,29.004 L44.122,29.004 L44.122,3.906 L3.877,3.906 L3.877,29.004 Z M45.608,32.91 L2.392,32.91 C1.073,32.91 0,31.829 0,30.501 L0,2.409 C0,1.081 1.073,0 2.392,0 L45.608,0 C46.927,0 48,1.081 48,2.409 L48,30.501 C48,31.829 46.927,32.91 45.608,32.91 L45.608,32.91 Z" id="Path"></path>
            </g>
        </g>
    </g>
		</svg>);
	}
}
