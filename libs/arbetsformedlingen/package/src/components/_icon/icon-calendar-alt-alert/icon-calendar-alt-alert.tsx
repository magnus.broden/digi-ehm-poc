import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-calendar-alt-alert',
	styleUrl: 'icon-calendar-alt-alert.scss',
	scoped: true
})
export class IconCalendarAltAlert {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-calendar-alt-alert"
				width="48px" height="48px" viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<g id="Symbols" stroke="none" stroke-width="1" fill="currentColor" fill-rule="nonzero">
					<g id="Group-3">
						<path d="M19.0887669,17.9999463 C17.7631122,20.1885257 17,22.7554943 17,25.5 C17,33.5099052 23.4924335,40 31.5,40 C34.8186995,40 37.8771521,38.8852287 40.3210376,37.0095068 L40.32,43.5 C40.32,45.984375 38.385,48 36,48 L4.32,48 C1.935,48 2.27373675e-13,45.984375 2.27373675e-13,43.5 L2.27373675e-13,19.125 C2.27373675e-13,18.50625 0.486,18 1.08,18 Z M13.32,0 C13.914,0 14.4,0.50625 14.4,1.125 L14.4,6 L25.92,6 L25.92,1.125 C25.92,0.50625 26.406,0 27,0 L30.6,0 C31.194,0 31.68,0.50625 31.68,1.125 L31.68,6 L36,6 C38.385,6 40.32,8.015625 40.32,10.5 L40.32,13.875 L40.3150424,13.987304 C37.8721511,12.1137514 34.8159863,11 31.5,11 C27.6238392,11 24.1026987,12.5218309 21.5011222,15.0001531 L1.08,15 C0.486,15 2.27373675e-13,14.49375 2.27373675e-13,13.875 L2.27373675e-13,10.5 C2.27373675e-13,8.015625 1.935,6 4.32,6 L8.64,6 L8.64,1.125 C8.64,0.50625 9.126,0 9.72,0 L13.32,0 Z" id="Combined-Shape"></path>
						<path d="M31.5,13.8064516 C37.9601593,13.8064516 43.1935484,19.037619 43.1935484,25.5 C43.1935484,31.9624395 37.9647198,37.1935484 31.5,37.1935484 C25.0374435,37.1935484 19.8064516,31.9647198 19.8064516,25.5 C19.8064516,19.0396069 25.0376774,13.8064516 31.5,13.8064516 Z M31.5,28.6572581 C30.1459456,28.6572581 29.0443548,29.7588488 29.0443548,31.1129032 C29.0443548,32.4669577 30.1459456,33.5685484 31.5,33.5685484 C32.8540544,33.5685484 33.9556452,32.4669577 33.9556452,31.1129032 C33.9556452,29.7588488 32.8540544,28.6572581 31.5,28.6572581 Z M33.1011391,18.016129 L29.8988609,18.016129 C29.4975968,18.016129 29.1780706,18.3520262 29.198125,18.7527641 L29.5957056,26.704377 C29.6143569,27.0778105 29.9225988,27.3709677 30.2964415,27.3709677 L32.7035585,27.3709677 C33.0774012,27.3709677 33.3856431,27.0778105 33.4042944,26.704377 L33.801875,18.7527641 C33.8219294,18.3520262 33.5024032,18.016129 33.1011391,18.016129 Z" id="Combined-Shape"></path>
					</g>
				</g>
			</svg>
		);
	}
}
