import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-lattlast',
	styleUrls: ['icon-lattlast.scss'],
	scoped: true
})
export class IconLattlast {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-lattlast"
				width="21"
				height="27"
				viewBox="0 0 21 27"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-lattlast__shape"
					d="M1.657 7.364c.915 0 1.657.72 1.657 1.607v14.814h8.76c.915 0 1.657.72 1.657 1.607 0 .888-.742 1.608-1.657 1.608H1.657C.742 27 0 26.28 0 25.392V8.972c0-.889.742-1.608 1.657-1.608zM8.927 0c.915 0 1.657.72 1.657 1.608V16.42h8.759c.915 0 1.657.72 1.657 1.608 0 .888-.742 1.607-1.657 1.607H8.926c-.915 0-1.657-.72-1.657-1.607V1.608C7.27.72 8.011 0 8.926 0z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
