import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-bell',
	styleUrls: ['icon-bell.scss'],
	scoped: true
})
export class IconBell {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-bell"
				width="22"
				height="26"
				viewBox="0 0 22 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-bell__shape"
					d="M20.89 16.805c-.834-.852-1.696-1.733-1.696-5.894 0-4.217-2.994-7.723-6.898-8.367.18-.27.276-.59.275-.919C12.571.728 11.868 0 11 0S9.429.728 9.429 1.625c0 .328.095.649.275.919-3.904.644-6.898 4.15-6.898 8.367 0 4.16-.862 5.042-1.696 5.894-2.362 2.413-.632 5.945 2.176 5.945h4.571C7.857 24.545 9.264 26 11 26c1.736 0 3.143-1.455 3.143-3.25h4.57c2.81 0 4.539-3.534 2.177-5.945zM11 23.97c-.65 0-1.179-.547-1.179-1.219h2.358c0 .672-.53 1.219-1.179 1.219zm7.714-3.657H3.285c-.823 0-1.234-1.035-.655-1.634 1.404-1.452 2.533-2.83 2.533-7.767 0-3.328 2.619-6.036 5.837-6.036 3.218 0 5.837 2.708 5.837 6.036 0 4.958 1.14 6.326 2.532 7.767.582.602.165 1.634-.655 1.634z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
