import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-heart-solid',
	styleUrl: 'icon-heart-solid.scss',
	scoped: true
})
export class IconHeartSolid {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-heart-solid"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-heart-solid__shape"
					d="M 34.181818,3 C 42.518182,3 48,8.1142224 48,16.649995 c 0,4.706895 -2.890909,9.368531 -6.190909,12.554737 l -0.03636,0.03621 c -3.121827,3.046127 -5.463197,5.330722 -7.024111,6.853786 -1.768379,1.725498 -4.420948,4.313746 -7.957707,7.764744 -1.547184,1.520708 -4.034634,1.520708 -5.581818,0 L 6.2272727,29.213784 C 5.5818182,28.61637 0,23.284907 0,16.649995 0,8.4400843 5.1636364,3 13.818182,3 17.581818,3 21.145454,5.0637924 24,7.5077571 26.845454,5.0637924 30.418182,3 34.181818,3 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
