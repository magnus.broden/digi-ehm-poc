import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-arrow-back',
	styleUrl: 'icon-arrow-back.scss',
	scoped: true
})
export class IconArrowBack {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-arrow-back"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-arrow-back__shape"
					d="M23.910365,9.36383059 L23.910365,9.36383059 L22.0978102,9.36383059 L20.8832117,9.36383059 L20.8832117,2.6525574 C20.8832117,0.298049958 18.0055474,-0.888473479 16.3237956,0.780075104 L5.78481752,11.2363129 C4.73839416,12.2745209 4.73839416,13.9430695 5.78481752,14.9812775 L16.3237956,25.4375153 C18.0055474,27.1060639 20.8832117,25.9195404 20.8832117,23.565033 L20.8832117,16.5571289 L22.0978102,16.5571289 L23.910365,16.5571289 L23.910365,16.5571289 C30.6560584,16.5571289 36.1310949,22.007721 36.1310949,28.6819153 C36.1310949,35.3746491 30.6373723,40.8067017 23.910365,40.8067017 C23.910365,40.8067017 23.8543066,40.8067017 23.8356204,40.8067017 L8.66248175,40.8067017 C8.06452555,40.8067017 7.56,41.3072662 7.56,41.900528 L7.56,46.8690948 C7.56,47.4623566 8.06452555,47.9629211 8.66248175,47.9629211 L23.8169343,48 C23.8543066,48 23.8729927,48 23.910365,48 C34.6548905,48 43.4,39.3235474 43.4,28.6633759 C43.4,18.0217438 34.6548905,9.36383059 23.910365,9.36383059 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
