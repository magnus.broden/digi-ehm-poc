import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-chart',
	styleUrl: 'icon-chart.scss',
	scoped: true
})
export class IconChart {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				width="56"
				height="62"
				viewBox="0 0 56 62"
				version="1.1"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				<title>Chart</title>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<defs>
					<polygon
						id="path-1"
						points="19.8389256 11.2318841 34.4403749 11.2318841 34.4403749 42.2318841 19.8389256 42.2318841"
					></polygon>
					<polygon
						id="path-3"
						points="-0.846460828 -19.7681159 53.908974 -19.7681159 53.908974 -0.393115942 -0.846460828 -0.393115942"
					></polygon>
				</defs>
				<g
					id="Ikonguide"
					stroke="none"
					stroke-width="1"
					fill="none"
					fill-rule="evenodd"
				>
					<g transform="translate(-1012.000000, -804.000000)" id="Chart">
						<g transform="translate(1013.063248, 823.768116)">
							<g id="Group" transform="translate(-0.000000, 0.000000)">
								<path
									d="M11.2871933,17.0724638 L3.19179715,17.0724638 C1.47642907,17.0724638 0.0891563449,18.5157634 0.0891563449,20.3004089 L0,39.003939 C0,40.7885845 1.38727273,42.2318841 3.1026408,42.2318841 L11.1980369,42.2318841 C12.913405,42.2318841 14.3006777,40.7885845 14.3006777,39.003939 L14.3898341,20.3004089 C14.3898341,18.5157634 12.9989951,17.0724638 11.2871933,17.0724638"
									id="Fill-1"
									fill="currentColor"
								></path>
								<path
									d="M31.292107,11.2318841 L23.077661,11.2318841 C21.3370669,11.2318841 19.9293931,12.6247 19.9293931,14.3469222 L19.8389256,39.1168459 C19.8389256,40.8354877 21.2465994,42.2318841 22.9871935,42.2318841 L31.2016396,42.2318841 C32.9422337,42.2318841 34.3499075,40.8354877 34.3499075,39.1168459 L34.4403749,14.3469222 C34.4403749,12.6247 33.0290824,11.2318841 31.292107,11.2318841"
									id="Fill-3"
									fill="currentColor"
								></path>
								<path
									d="M51.0899571,0 L43.0444095,0 C41.339604,0 39.9608736,1.42791855 39.9608736,3.19354534 L39.7836589,39.0420095 C39.7836589,40.8039655 41.1659336,42.2318841 42.8671948,42.2318841 L50.9162867,42.2318841 C52.6175479,42.2318841 53.9962783,40.8039655 53.9962783,39.0420095 L54.173493,3.19354534 C54.173493,1.42791855 52.7912183,0 51.0899571,0"
									id="Fill-6"
									fill="currentColor"
								></path>
								<polygon
									id="path-1"
									points="19.8389256 11.2318841 34.4403749 11.2318841 34.4403749 42.2318841 19.8389256 42.2318841"
								></polygon>
								<g id="Clip-4"></g>
								<polygon
									id="path-3"
									points="-0.846460828 -19.7681159 53.908974 -19.7681159 53.908974 -0.393115942 -0.846460828 -0.393115942"
								></polygon>
								<g id="Clip-9"></g>
								<path
									d="M2.64246183,-0.393115942 L24.287738,-0.393115942 C24.783165,-0.393115942 25.2716142,-0.501190406 25.7216852,-0.713736852 L51.853716,-12.8829215 C53.6086441,-13.6970825 54.3936517,-15.8297519 53.5981773,-17.6454029 C52.8027029,-19.4610539 50.7372607,-20.2680099 48.9858215,-19.4466439 L23.5376196,-7.59808021 L2.64246183,-7.59808021 C0.716576525,-7.59808021 -0.846460828,-5.98777069 -0.846460828,-3.99559807 C-0.846460828,-2.00342546 0.716576525,-0.393115942 2.64246183,-0.393115942"
									id="Fill-8"
									fill="currentColor"
									mask="url(#mask-4)"
								></path>
							</g>
						</g>
					</g>
				</g>
			</svg>
		);
	}
}
