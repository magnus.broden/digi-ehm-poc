import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-user-alt',
	styleUrls: ['icon-user-alt.scss'],
	scoped: true
})
export class IconUserAlt {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-user-alt"
				width="22"
				height="26"
				viewBox="0 0 22 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-user-alt__shape"
					d="M4.714 6.5C4.714 2.91 7.53 0 11 0c3.471 0 6.286 2.91 6.286 6.5S14.47 13 11 13c-3.471 0-6.286-2.91-6.286-6.5zm12.572 8.125h-2.683a8.39 8.39 0 01-7.206 0H4.714C2.111 14.625 0 16.808 0 19.5v5.281C0 25.454.528 26 1.179 26H20.82c.651 0 1.179-.546 1.179-1.219V19.5c0-2.692-2.11-4.875-4.714-4.875z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
