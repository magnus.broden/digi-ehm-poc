import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-notification-info',
	styleUrls: ['icon-notification-info.scss'],
	scoped: true
})
export class IconNotificationInfo {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-notification-info"
				width="42"
				height="42"
				viewBox="0 0 42 42"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<g
					class="digi-icon-notification-info__shape"
					fill="currentColor"
					fill-rule="nonzero"
				>
					<path d="M21 39c9.941 0 18-8.059 18-18S30.941 3 21 3 3 11.059 3 21s8.059 18 18 18zm0 3C9.402 42 0 32.598 0 21S9.402 0 21 0s21 9.402 21 21-9.402 21-21 21z" />
					<path d="M22 10c2.071 0 3.75 1.485 3.75 3.316 0 1.831-1.679 3.316-3.75 3.316-2.071 0-3.75-1.485-3.75-3.316C18.25 11.485 19.929 10 22 10zm5 20.053c0 .523-.48.947-1.071.947H18.07C17.48 31 17 30.576 17 30.053v-1.895c0-.523.48-.947 1.071-.947h1.072v-5.053H18.07c-.591 0-1.071-.424-1.071-.947v-1.895c0-.523.48-.948 1.071-.948h5.715c.591 0 1.071.425 1.071.948v7.895h1.072c.591 0 1.071.424 1.071.947v1.895z" />
				</g>
			</svg>
		);
	}
}
