import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-arrow-right',
	styleUrl: 'icon-arrow-right.scss',
	scoped: true
})
export class IconArrowRight {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-arrow-right"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-arrow-right__shape"
					d="M0.381032171,24.6320096 L2.52712589,26.7533334 C3.03506039,27.2554054 3.85863836,27.2554054 4.36668125,26.7533334 L21.1817412,10.1322435 L21.1817412,46.7142843 C21.1817412,47.4243208 21.7641425,48 22.4824698,48 L25.5175031,48 C26.2358304,48 26.8182317,47.4243208 26.8182317,46.7142843 L26.8182317,10.1322435 L43.6334,26.7533334 C44.1413345,27.2554054 44.9649125,27.2554054 45.4729554,26.7533334 L47.6190491,24.6320096 C48.1269836,24.1299376 48.1269836,23.3158653 47.6190491,22.8136862 L24.9197099,0.376553992 C24.4117754,-0.125517997 23.5881975,-0.125517997 23.0801546,0.376553992 L0.381032171,22.8137933 C-0.127010724,23.3158653 -0.127010724,24.1299376 0.381032171,24.6320096 Z"
					id="Icon"
					transform="translate(24.000000, 24.000000) rotate(90.000000) translate(-24.000000, -24.000000) "
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
