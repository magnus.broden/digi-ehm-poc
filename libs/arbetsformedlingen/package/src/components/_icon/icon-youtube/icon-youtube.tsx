import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-youtube',
	styleUrls: ['icon-youtube.scss'],
	scoped: true
})
export class IconYoutube {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-youtube"
				width="27"
				height="19"
				viewBox="0 0 27 19"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-youtube__shape"
					d="M26.392 2.973a3.388 3.388 0 00-2.38-2.405C21.915 0 13.5 0 13.5 0S5.086 0 2.987.568A3.388 3.388 0 00.608 2.973C.046 5.093.046 9.519.046 9.519s0 4.426.562 6.547a3.338 3.338 0 002.38 2.366C5.085 19 13.5 19 13.5 19s8.414 0 10.513-.568a3.338 3.338 0 002.379-2.366c.562-2.121.562-6.547.562-6.547s0-4.425-.562-6.546zM10.748 13.537V5.501l7.033 4.018-7.033 4.018z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
