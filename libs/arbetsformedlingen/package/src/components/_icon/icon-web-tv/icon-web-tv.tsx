import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-web-tv',
	styleUrl: 'icon-web-tv.scss',
	scoped: true
})
export class IconWebTv {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-web-tv"
				width="18"
				height="15"
				viewBox="0 0 18 15"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-web-tv__shape"
					d="M17.1346154,3.45 L11.9423077,3.45 C11.464369,3.45 11.0769231,3.8361544 11.0769231,4.3125 L11.0769231,12.9375 C11.0769231,13.4138456 11.464369,13.8 11.9423077,13.8 L17.1346154,13.8 C17.6125541,13.8 18,13.4138456 18,12.9375 L18,4.3125 C18,3.8361544 17.6125541,3.45 17.1346154,3.45 Z M17.3076923,13.11 L11.7692308,13.11 L11.7692308,4.14 L17.3076923,4.14 L17.3076923,13.11 Z M2.72596154,0.8625 L13.6298077,0.8625 L13.6298077,2.5875 L14.5384615,2.5875 L14.5384615,0.8625 C14.5384615,0.386154403 14.1316434,0 13.6298077,0 L2.72596154,0 C2.22412588,0 1.81730769,0.386154403 1.81730769,0.8625 L1.81730769,7.7625 L0.454326923,7.7625 C0.203409092,7.7625 0,7.9555772 0,8.19375 L0,8.625 C0.00218016205,9.57588663 0.812985966,10.3465925 1.8147521,10.35 L9.99519231,10.35 L9.99519231,9.4875 L1.8147521,9.4875 C1.31391475,9.48616294 0.908651865,9.10039988 0.908653846,8.625 L9.99519231,8.625 L9.99519231,7.7625 L2.72596154,7.7625 L2.72596154,0.8625 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
