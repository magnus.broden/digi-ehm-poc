import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-globe',
	styleUrls: ['icon-globe.scss'],
	scoped: true
})
export class IconGlobe {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-globe"
				width="25"
				height="25"
				viewBox="0 0 25 25"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-globe__shape"
					d="M12.5 0C5.596 0 0 5.596 0 12.5S5.596 25 12.5 25 25 19.404 25 12.5 19.404 0 12.5 0zm9.057 8.065h-4.24c-.37-1.886-.973-3.646-1.781-5.182a10.127 10.127 0 016.021 5.182zm.823 6.451h-4.724c.11-1.306.118-2.647 0-4.032h4.724a10.22 10.22 0 010 4.032zm-12.605 0a21.098 21.098 0 01-.002-4.032h5.454a21.098 21.098 0 01-.002 4.032h-5.45zm5.068 2.42c-.472 2.12-1.275 4.027-2.343 5.506-1.068-1.479-1.871-3.387-2.343-5.507h4.686zM2.62 10.483h4.724a23.69 23.69 0 000 4.032H2.62a10.22 10.22 0 010-4.032zm7.53-2.42c.468-2.115 1.27-4.014 2.35-5.507 1.08 1.493 1.882 3.392 2.35 5.508h-4.7zm-.686-5.181c-.808 1.536-1.412 3.296-1.781 5.182h-4.24a10.127 10.127 0 016.02-5.182zM3.443 16.935h4.236c.368 1.89.97 3.645 1.78 5.18a10.127 10.127 0 01-6.016-5.18zm12.099 5.18c.81-1.535 1.411-3.29 1.78-5.18h4.235a10.127 10.127 0 01-6.015 5.18z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
