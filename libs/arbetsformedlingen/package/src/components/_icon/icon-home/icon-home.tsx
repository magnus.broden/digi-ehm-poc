import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-home',
	styleUrl: 'icon-home.scss',
	scoped: true
})
export class IconHome {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-home"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-home__shape"
					d="M23.3636147,15.3393459 L8.00063412,27.5429401 L8.00063412,40.7141422 C8.00063412,41.4243019 8.5975417,42 9.33386452,42 L18.6714769,41.9766938 C19.4051951,41.9731555 19.9980504,41.3984924 19.9980412,40.690836 L19.9980412,32.9989954 C19.9980412,32.2888358 20.5949488,31.7131376 21.3312716,31.7131376 L26.6641932,31.7131376 C27.400516,31.7131376 27.9974236,32.2888358 27.9974236,32.9989954 L27.9974236,40.6852104 C27.9963164,41.0269368 28.1362924,41.355033 28.3864417,41.5970479 C28.636591,41.8390628 28.9763363,41.9750882 29.330654,41.9750865 L38.6649334,42 C39.4012562,42 39.9981638,41.4243019 39.9981638,40.7141422 L39.9981638,27.5340998 L24.6385163,15.3393459 C24.2664583,15.0501041 23.7356727,15.0501041 23.3636147,15.3393459 Z M47.6309078,23.6339324 L40.664779,18.0959036 L40.664779,6.96439335 C40.664779,6.43177361 40.2170983,6 39.6648562,6 L34.9985498,6 C34.4463076,6 33.998627,6.43177361 33.998627,6.96439335 L33.998627,12.7997768 L26.5383696,6.88000893 C25.0618773,5.70817817 22.9319209,5.70817817 21.4554287,6.88000893 L0.362890451,23.6339324 C0.15840013,23.7969442 0.0294595071,24.0316513 0.0044543242,24.2863839 C-0.0205508587,24.5411165 0.0604293198,24.7949916 0.229567411,24.9921197 L2.35440336,27.4834691 C2.5230839,27.6812885 2.7664259,27.8062674 3.03076068,27.8308419 C3.29509545,27.8554165 3.55870881,27.777568 3.76346125,27.6144659 L23.3636147,12.0443353 C23.7356727,11.7550935 24.2664583,11.7550935 24.6385163,12.0443353 L44.239503,27.6144659 C44.4438935,27.7775941 44.7071217,27.8556969 44.9712389,27.8315802 C45.2353562,27.8074635 45.4787102,27.6831045 45.6477276,27.4858801 L47.7725636,24.9945306 C47.9415298,24.7962543 48.0215807,24.5411986 47.9949936,24.2858308 C47.9684065,24.0304629 47.8373748,23.7958496 47.6309078,23.6339324 L47.6309078,23.6339324 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
