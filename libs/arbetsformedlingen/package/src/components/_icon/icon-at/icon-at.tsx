import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-at',
	styleUrl: 'icon-at.scss',
	scoped: true
})
export class IconAt {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

  

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-at"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-at__shape"
					d="M48,21.6774194 C48,8.49203226 37.3599677,0 24,0 C10.736129,0 0,10.7340968 0,24 C0,37.2637742 10.7340968,48 24,48 C29.121871,48 34.1304194,46.3471935 38.2350968,43.3237742 C38.7674516,42.9316452 38.8566774,42.1698387 38.4378387,41.6581935 L36.9645484,39.8584839 C36.572129,39.3790645 35.8759355,39.2969032 35.3747419,39.6609677 C32.0851935,42.0500323 28.0875484,43.3548387 24,43.3548387 C13.3277419,43.3548387 4.64516129,34.6722581 4.64516129,24 C4.64516129,13.3277419 13.3277419,4.64516129 24,4.64516129 C34.5937742,4.64516129 43.3548387,10.9374194 43.3548387,21.6774194 C43.3548387,27.7844516 39.2440645,31.1893548 35.3206452,31.1893548 C33.4330645,31.1893548 33.3722903,29.9680645 33.7368387,28.1445484 L36.5066129,13.7680645 C36.6446129,13.0517419 36.0958065,12.3870968 35.3663226,12.3870968 L31.5751935,12.3870968 C31.0184291,12.3873023 30.5400838,12.782473 30.4348065,13.3291935 C30.3281613,13.8830323 30.2740645,14.1359032 30.2141613,14.6833548 C29.0603226,12.7916129 26.7420968,11.6767742 23.9042903,11.6767742 C16.9753548,11.6767742 10.8387097,17.7240968 10.8387097,26.4706452 C10.8387097,32.3892581 14.0203548,36.3522581 19.856129,36.3522581 C22.7402903,36.3522581 25.7926452,34.7232581 27.5737742,32.2646129 C27.9749032,35.2497097 30.3315484,35.943 33.3194516,35.943 C42.9161613,35.9429032 48,29.7857419 48,21.6774194 Z M21.4935484,31.0983871 C18.7381935,31.0983871 17.0932258,29.2153548 17.0932258,26.0612903 C17.0932258,20.4954194 20.9216129,17.0216129 24.3135484,17.0216129 C27.2283871,17.0216129 28.7139677,19.1053548 28.7139677,22.0132258 C28.7139677,26.5484516 25.5032903,31.0983871 21.4935484,31.0983871 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
