import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-accessibility-universal',
	styleUrls: ['icon-accessibility-universal.scss'],
	scoped: true
})
export class IconAccessibilityUniversal {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
	@Prop() afSvgAriaLabelledby: string;
	@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-accessibility-universal"
				width="48px"
				height="48px"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-accessibility-universal__shape"
					d="M24,0 C10.745129,0 0,10.745129 0,24 C0,37.254871 10.745129,48 24,48 C37.254871,48 48,37.254871 48,24 C48,10.745129 37.254871,0 24,0 Z M24,44.1290323 C12.8755161,44.1290323 3.87096774,35.1262258 3.87096774,24 C3.87096774,12.8755161 12.8737742,3.87096774 24,3.87096774 C35.1244839,3.87096774 44.1290323,12.8737742 44.1290323,24 C44.1290323,35.1244839 35.1262258,44.1290323 24,44.1290323 Z M37.8961935,16.3877419 C38.1418065,17.4280645 37.4975806,18.4705161 36.4572581,18.7162258 C33.5670968,19.3985806 30.8599355,20.0019677 28.1736774,20.3372903 C28.2393871,31.1111613 29.3982581,32.7685161 30.7835806,36.3162581 C31.2503226,37.5110323 30.6601935,38.8580323 29.4653226,39.3246774 C28.271129,39.7914194 26.9237419,39.2017742 26.4569032,38.0064194 C25.5318387,35.6386452 24.7063548,33.8500645 24.1531935,29.769 L23.847,29.769 C23.2933548,33.8534516 22.466129,35.6437742 21.5432903,38.0064194 C21.0772258,39.1995484 19.7315806,39.792 18.534871,39.3247742 C17.3400968,38.8580323 16.749871,37.511129 17.2165161,36.3162581 C18.6032903,32.766 19.7607097,31.1062258 19.8264194,20.3372903 C17.1401613,20.0019677 14.433,19.3986774 11.5428387,18.7162258 C10.5025161,18.4706129 9.85829032,17.4280645 10.1039032,16.3877419 C10.3496129,15.3474194 11.3921613,14.7033871 12.4323871,14.9488065 C22.5712258,17.3426129 25.4433871,17.3392258 35.5678065,14.9488065 C36.6080323,14.7031935 37.6505806,15.3473226 37.8961935,16.3877419 Z M20.3038065,12.2123226 C20.3038065,10.1709677 21.9586452,8.51612903 24,8.51612903 C26.0413548,8.51612903 27.6961935,10.1709677 27.6961935,12.2123226 C27.6961935,14.2536774 26.0413548,15.9085161 24,15.9085161 C21.9586452,15.9085161 20.3038065,14.2535806 20.3038065,12.2123226 L20.3038065,12.2123226 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
