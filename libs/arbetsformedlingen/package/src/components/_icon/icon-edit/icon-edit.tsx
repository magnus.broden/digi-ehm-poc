import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-edit',
	styleUrls: ['icon-edit.scss'],
	scoped: true
})
export class IconEdit {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-edit"
				width="29"
				height="26"
				viewBox="0 0 29 26"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-edit__shape"
					d="M20.27 4.225l4.541 4.58a.5.5 0 010 .701l-10.996 11.09-4.672.524a.983.983 0 01-1.082-1.092l.518-4.712L19.575 4.225a.49.49 0 01.695 0zm8.156-1.163L25.969.584a1.957 1.957 0 00-2.78 0l-1.781 1.798a.5.5 0 000 .7l4.541 4.58a.49.49 0 00.695 0l1.782-1.797a1.998 1.998 0 000-2.803zM19.333 17.58v5.17H3.223V6.5h11.569a.616.616 0 00.428-.178l2.014-2.031c.382-.386.11-1.041-.428-1.041H2.416A2.428 2.428 0 000 5.688v17.875A2.428 2.428 0 002.417 26h17.722a2.428 2.428 0 002.417-2.438V15.55a.605.605 0 00-1.033-.431l-2.013 2.03a.627.627 0 00-.177.432z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
