import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-comunication-case',
	styleUrl: 'icon-comunication-case.scss',
	scoped: true
})
export class IconComunicationCase {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-comunication-case"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-comunication-case__shape"
					d="M24.28,0 C27.4561,0 30.04,2.6915625 30.04,6 L37.24,6 C39.6259,6 41.56,8.0146875 41.56,10.5 L41.56,43.5 C41.56,45.9853125 39.6259,48 37.24,48 L11.32,48 C8.9341,48 7,45.9853125 7,43.5 L7,10.5 C7,8.0146875 8.9341,6 11.32,6 L18.52,6 C18.52,2.6915625 21.1039,0 24.28,0 Z M35.08,11.52 L13.48,11.52 C13.082355,11.52 12.76,12.0035325 12.76,12.6 L12.76,16.2 C12.76,16.7964675 13.082355,17.28 13.48,17.28 L35.08,17.28 C35.477645,17.28 35.8,16.7964675 35.8,16.2 L35.8,12.6 C35.8,12.0035325 35.477645,11.52 35.08,11.52 Z M24.28,1.92 C22.1592,1.92 20.44,3.6392 20.44,5.76 C20.44,7.8808 22.1592,9.6 24.28,9.6 C26.4008,9.6 28.12,7.8808 28.12,5.76 C28.12,3.6392 26.4008,1.92 24.28,1.92 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
