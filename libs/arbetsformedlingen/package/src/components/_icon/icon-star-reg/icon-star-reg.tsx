import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-star-reg',
	styleUrls: ['icon-star-reg.scss'],
	scoped: true
})
export class IconStarReg {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-star-reg"
				width="27"
				height="26"
				viewBox="0 0 27 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-star-reg__shape"
					d="M25.595 8.726l-7.36-1.078-3.29-6.697c-.589-1.194-2.296-1.209-2.89 0l-3.29 6.697-7.36 1.078c-1.32.192-1.848 1.826-.891 2.762l5.324 5.21-1.26 7.36c-.226 1.33 1.17 2.326 2.338 1.704l6.584-3.475 6.584 3.475c1.168.617 2.564-.374 2.337-1.704l-1.26-7.36 5.325-5.21c.957-.936.428-2.57-.891-2.762zm-7.027 7.122l1.193 7-6.261-3.302-6.261 3.303 1.193-7.001-5.067-4.957 7.002-1.022L13.5 3.495l3.133 6.374 7.002 1.022-5.067 4.957z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
