import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-job-suggestion',
	styleUrl: 'icon-job-suggestion.scss',
	scoped: true
})
export class IconJobSuggestion {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-job-suggestion"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-job-suggestion__shape"
					d="M27.8528015,8.20954973 C28.6291494,6.6392705 30.8136122,6.6071672 31.676309,8.05938247 L31.7580716,8.20954973 L36.1976079,17.1958706 L46.1352881,18.6418694 C47.8627434,18.8931734 48.5885556,20.9620543 47.4530422,22.2311036 L47.3411084,22.3476538 L40.1508461,29.3399491 L41.8495808,39.217639 C42.1484484,40.9510454 40.3775543,42.2545248 38.8310739,41.5694809 L38.6952605,41.5038357 L29.8046095,36.8406548 L20.9156126,41.5038357 C19.3827108,42.3152174 17.5594804,41.0766577 17.7370643,39.3694509 L17.7579842,39.217639 L19.460027,29.3399491 L12.2697647,22.3476538 C11.0170789,21.1311501 11.649473,19.0330207 13.3148059,18.6706742 L13.473931,18.6418694 L23.4116112,17.1958706 L27.8528015,8.20954973 Z M12.5450108,26.3103448 C13.0972955,26.3103448 13.5450108,26.7580601 13.5450108,27.3103448 L13.5450108,29.1724138 C13.5450108,29.7246985 13.0972955,30.1724138 12.5450108,30.1724138 L1,30.1724138 C0.44771525,30.1724138 -3.55271368e-15,29.7246985 -3.55271368e-15,29.1724138 L-3.55271368e-15,27.3103448 C-3.55271368e-15,26.7580601 0.44771525,26.3103448 1,26.3103448 L12.5450108,26.3103448 Z M8.6750077,18.5862069 C9.22729245,18.5862069 9.6750077,19.0339221 9.6750077,19.5862069 L9.6750077,21.4482759 C9.6750077,22.0005606 9.22729245,22.4482759 8.6750077,22.4482759 L1,22.4482759 C0.44771525,22.4482759 -3.55271368e-15,22.0005606 -3.55271368e-15,21.4482759 L-3.55271368e-15,19.5862069 C-3.55271368e-15,19.0339221 0.44771525,18.5862069 1,18.5862069 L8.6750077,18.5862069 Z M22.2200185,10.862069 C22.7723032,10.862069 23.2200185,11.3097842 23.2200185,11.862069 L23.2200185,13.7241379 C23.2200185,14.2764227 22.7723032,14.7241379 22.2200185,14.7241379 L1,14.7241379 C0.44771525,14.7241379 -3.55271368e-15,14.2764227 -3.55271368e-15,13.7241379 L-3.55271368e-15,11.862069 C-3.55271368e-15,11.3097842 0.44771525,10.862069 1,10.862069 L22.2200185,10.862069 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
