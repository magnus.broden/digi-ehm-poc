import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-caret-up',
	styleUrls: ['icon-caret-up.scss'],
	scoped: true
})
export class IconCaretUp {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-caret-up"
				width="12"
				height="7"
				viewBox="0 0 12 7"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-caret-up__shape"
					d="M11.777 5.65A.608.608 0 0112 6.12c0 .18-.074.337-.223.469a.768.768 0 01-.527.198H.75a.768.768 0 01-.527-.198A.608.608 0 010 6.119c0-.18.074-.337.223-.469L5.473.984A.768.768 0 016 .786c.203 0 .379.066.527.198l5.25 4.666z"
					fill="currentColor"
					fill-rule="evenodd"
				/>
			</svg>
		);
	}
}
