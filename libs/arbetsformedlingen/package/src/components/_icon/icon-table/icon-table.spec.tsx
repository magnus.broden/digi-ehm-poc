import { newSpecPage } from '@stencil/core/testing';
import { IconTable } from './icon-table';

describe('icon-table', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [IconTable],
			html: '<icon-table></icon-table>'
		});
		expect(root).toEqualHtml(`
      <icon-table>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </icon-table>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [IconTable],
			html: `<icon-table first="Stencil" last="'Don't call me a framework' JS"></icon-table>`
		});
		expect(root).toEqualHtml(`
      <icon-table first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </icon-table>
    `);
	});
});
