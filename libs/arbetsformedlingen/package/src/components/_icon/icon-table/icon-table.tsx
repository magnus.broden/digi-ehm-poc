import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-table',
	styleUrl: 'icon-table.scss',
	scoped: true
})
export class IconTable {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-table"
				height="54"
				width="62"
				viewBox="0 0 62 54"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<defs>
					<polygon id="path-1" points="0 0 62 0 62 11.25 0 11.25"></polygon>
				</defs>
				<g
					id="Ikonguide"
					stroke="none"
					stroke-width="1"
					fill="none"
					fill-rule="evenodd"
				>
					<g transform="translate(-1093.000000, -812.000000)" id="Table">
						<g transform="translate(1093.000000, 812.750000)" id="Group">
							<path
								d="M13.1595059,16.9067797 L4.31083309,16.9067797 C2.43584999,16.9067797 0.919491525,18.3742233 0.919491525,20.1887231 L0.919491525,28.7519549 C0.919491525,30.5664546 2.43584999,32.0338983 4.31083309,32.0338983 L13.1595059,32.0338983 C15.0305909,32.0338983 16.5508475,30.5664546 16.5508475,28.7519549 L16.5508475,20.1887231 C16.5508475,18.3742233 15.0305909,16.9067797 13.1595059,16.9067797"
								id="Fill-1"
								fill="currentColor"
							></path>
							<path
								d="M36.146794,16.9067797 L27.2981212,16.9067797 C25.4231381,16.9067797 23.9067797,18.3742233 23.9067797,20.1887231 L23.9067797,28.7519549 C23.9067797,30.5664546 25.4231381,32.0338983 27.2981212,32.0338983 L36.146794,32.0338983 C38.017879,32.0338983 39.5381356,30.5664546 39.5381356,28.7519549 L39.5381356,20.1887231 C39.5381356,18.3742233 38.017879,16.9067797 36.146794,16.9067797"
								id="Fill-3"
								fill="currentColor"
							></path>
							<path
								d="M58.2145906,16.9067797 L49.3659178,16.9067797 C47.4909347,16.9067797 45.9745763,18.3742233 45.9745763,20.1887231 L45.9745763,28.7519549 C45.9745763,30.5664546 47.4909347,32.0338983 49.3659178,32.0338983 L58.2145906,32.0338983 C60.0856756,32.0338983 61.6059322,30.5664546 61.6059322,28.7519549 L61.6059322,20.1887231 C61.6059322,18.3742233 60.0856756,16.9067797 58.2145906,16.9067797"
								id="Fill-5"
								fill="currentColor"
							></path>
							<path
								d="M13.1595059,37.3728814 L4.31083309,37.3728814 C2.43584999,37.3728814 0.919491525,38.840325 0.919491525,40.6548248 L0.919491525,49.2180566 C0.919491525,51.0325563 2.43584999,52.5 4.31083309,52.5 L13.1595059,52.5 C15.0305909,52.5 16.5508475,51.0325563 16.5508475,49.2180566 L16.5508475,40.6548248 C16.5508475,38.840325 15.0305909,37.3728814 13.1595059,37.3728814"
								id="Fill-10"
								fill="currentColor"
							></path>
							<path
								d="M36.146794,37.3728814 L27.2981212,37.3728814 C25.4231381,37.3728814 23.9067797,38.840325 23.9067797,40.6548248 L23.9067797,49.2180566 C23.9067797,51.0325563 25.4231381,52.5 27.2981212,52.5 L36.146794,52.5 C38.017879,52.5 39.5381356,51.0325563 39.5381356,49.2180566 L39.5381356,40.6548248 C39.5381356,38.840325 38.017879,37.3728814 36.146794,37.3728814"
								id="Fill-12"
								fill="currentColor"
							></path>
							<path
								d="M58.2145906,37.3728814 L49.3659178,37.3728814 C47.4909347,37.3728814 45.9745763,38.840325 45.9745763,40.6548248 L45.9745763,49.2180566 C45.9745763,51.0325563 47.4909347,52.5 49.3659178,52.5 L58.2145906,52.5 C60.0856756,52.5 61.6059322,51.0325563 61.6059322,49.2180566 L61.6059322,40.6548248 C61.6059322,38.840325 60.0856756,37.3728814 58.2145906,37.3728814"
								id="Fill-14"
								fill="currentColor"
							></path>

							<polygon id="path-1" points="0 0 62 0 62 11.25 0 11.25"></polygon>

							<g id="Clip-8"></g>
							<path
								d="M58.6255865,0 L3.37441351,0 C1.51266813,0 0,1.96895726 0,4.39802874 L0,11.25 L62,11.25 L62,4.39802874 C62,1.96895726 60.4873319,0 58.6255865,0"
								id="Fill-7"
								fill="currentColor"

								mask="url(#mask-2)"
							></path>
						</g>
					</g>
				</g>
			</svg>
		);
	}
}
