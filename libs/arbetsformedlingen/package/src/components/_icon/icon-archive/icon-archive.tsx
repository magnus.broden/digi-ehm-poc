import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-archive',
	styleUrls: ['icon-archive.scss'],
	scoped: true
})
export class IconArchive {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-archive"
				width="26"
				height="22"
				viewBox="0 0 26 22"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-archive__shape"
					d="M24.781 4.714H1.22C.546 4.714 0 4.187 0 3.536V1.179C0 .528.546 0 1.219 0H24.78C25.454 0 26 .528 26 1.179v2.357c0 .65-.546 1.178-1.219 1.178zm-.406 16.107V7.464c0-.65-.546-1.178-1.219-1.178H2.844c-.673 0-1.219.527-1.219 1.178v13.357c0 .651.546 1.179 1.219 1.179h20.312c.673 0 1.219-.528 1.219-1.179zM15.641 11h-5.282a.6.6 0 01-.609-.59v-.392a.6.6 0 01.61-.59h5.28a.6.6 0 01.61.59v.393a.6.6 0 01-.61.589z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
