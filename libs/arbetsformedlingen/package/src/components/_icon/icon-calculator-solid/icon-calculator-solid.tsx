import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-calculator-solid',
	styleUrl: 'icon-calculator-solid.scss',
	scoped: true
})
export class IconCalculatorSolid {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-calculator-solid"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-calculator-solid__shape"
					d="M40.012987,0 L7.44155844,0 C5.07272727,0 3,2.1 3,4.5 L3,43.5 C3,45.9 5.07272727,48 7.44155844,48 L40.012987,48 C42.3818182,48 44.4545455,45.9 44.4545455,43.5 L44.4545455,4.5 C44.4545455,2.1 42.3818182,0 40.012987,0 Z M14.8441558,40.8 C14.8441558,41.4 14.2519481,42 13.6597403,42 L10.1064935,42 C9.51428571,42 8.92207792,41.4 8.92207792,40.8 L8.92207792,37.2 C8.92207792,36.6 9.51428571,36 10.1064935,36 L13.6597403,36 C14.2519481,36 14.8441558,36.6 14.8441558,37.2 L14.8441558,40.8 L14.8441558,40.8 Z M14.8441558,28.8 C14.8441558,29.4 14.2519481,30 13.6597403,30 L10.1064935,30 C9.51428571,30 8.92207792,29.4 8.92207792,28.8 L8.92207792,25.2 C8.92207792,24.6 9.51428571,24 10.1064935,24 L13.6597403,24 C14.2519481,24 14.8441558,24.6 14.8441558,25.2 L14.8441558,28.8 L14.8441558,28.8 Z M26.6883117,40.8 C26.6883117,41.4 26.0961039,42 25.5038961,42 L21.9506494,42 C21.3584416,42 20.7662338,41.4 20.7662338,40.8 L20.7662338,37.2 C20.7662338,36.6 21.3584416,36 21.9506494,36 L25.5038961,36 C26.0961039,36 26.6883117,36.6 26.6883117,37.2 L26.6883117,40.8 Z M26.6883117,28.8 C26.6883117,29.4 26.0961039,30 25.5038961,30 L21.9506494,30 C21.3584416,30 20.7662338,29.4 20.7662338,28.8 L20.7662338,25.2 C20.7662338,24.6 21.3584416,24 21.9506494,24 L25.5038961,24 C26.0961039,24 26.6883117,24.6 26.6883117,25.2 L26.6883117,28.8 Z M38.5324675,40.8 C38.5324675,41.4 37.9402597,42 37.3480519,42 L33.7948052,42 C33.2025974,42 32.6103896,41.4 32.6103896,40.8 L32.6103896,25.2 C32.6103896,24.6 33.2025974,24 33.7948052,24 L37.3480519,24 C37.9402597,24 38.5324675,24.6 38.5324675,25.2 L38.5324675,40.8 L38.5324675,40.8 Z M38.5324675,16.8 C38.5324675,17.4 37.9402597,18 37.3480519,18 L10.1064935,18 C9.51428571,18 8.92207792,17.4 8.92207792,16.8 L8.92207792,7.2 C8.92207792,6.6 9.51428571,6 10.1064935,6 L37.3480519,6 C37.9402597,6 38.5324675,6.6 38.5324675,7.2 L38.5324675,16.8 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
