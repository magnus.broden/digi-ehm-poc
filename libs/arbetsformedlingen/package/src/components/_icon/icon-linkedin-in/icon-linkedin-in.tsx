import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-linkedin-in',
	styleUrls: ['icon-linkedin-in.scss'],
	scoped: true
})
export class IconLinkedinIn {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-linkedin-in"
				width="22"
				height="20"
				viewBox="0 0 22 20"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-linkedin-in__shape"
					d="M22 20h-4.862v-7.03c0-1.841-.76-3.097-2.433-3.097-1.28 0-1.991.847-2.322 1.664-.124.294-.105.702-.105 1.11V20H7.461s.062-12.454 0-13.586h4.817v2.132c.285-.933 1.824-2.263 4.28-2.263 3.048 0 5.442 1.954 5.442 6.162V20zM2.59 4.714h-.031C1.006 4.714 0 3.675 0 2.36 0 1.017 1.036 0 2.62 0 4.2 0 5.173 1.014 5.204 2.355c0 1.317-1.004 2.36-2.615 2.36zm-2.035 1.7h4.288V20H.555V6.414z"
					fill="currentColor"
					fill-rule="evenodd"
				/>
			</svg>
		);
	}
}
