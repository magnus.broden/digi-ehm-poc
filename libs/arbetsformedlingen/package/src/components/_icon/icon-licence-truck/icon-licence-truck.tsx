import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-licence-truck',
	styleUrl: 'icon-licence-truck.scss',
	scoped: true
})
export class IconLicenceTruck {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-licence-truck"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-licence-truck__shape"
					d="M46.5975,22.7117347 L42.21,18.2346939 C41.31,17.3163265 40.0875,16.8035714 38.8125,16.8035714 L36,16.8035714 L36,9.44897959 C36,8.09438776 34.9275,7 33.6,7 L2.4,7 C1.0725,7 0,8.09438776 0,9.44897959 L0,35.1632653 C0,38.5459184 2.685,41.2857143 6,41.2857143 C7.9725,41.2857143 9.705,40.2984694 10.8,38.8061224 C11.895,40.3061224 13.6275,41.2857143 15.6,41.2857143 C18.915,41.2857143 21.6,38.5459184 21.6,35.1632653 C21.6,34.7423469 21.555,34.3367347 21.48,33.9387755 L33.72,33.9387755 C33.6375,34.3367347 33.6,34.7423469 33.6,35.1632653 C33.6,38.5459184 36.285,41.2857143 39.6,41.2857143 C42.915,41.2857143 45.6,38.5459184 45.6,35.1632653 C45.6,34.7423469 45.555,34.3367347 45.48,33.9387755 L46.8,33.9387755 C47.46,33.9387755 48,33.3877551 48,32.7142857 L48,26.1709184 C48,24.869898 47.4975,23.630102 46.5975,22.7117347 Z M6,37.6122449 C4.68,37.6122449 3.6,36.5102041 3.6,35.1632653 C3.6,33.8163265 4.68,32.7142857 6,32.7142857 C7.32,32.7142857 8.4,33.8163265 8.4,35.1632653 C8.4,36.5102041 7.32,37.6122449 6,37.6122449 Z M15.6,37.6122449 C14.28,37.6122449 13.2,36.5102041 13.2,35.1632653 C13.2,33.8163265 14.28,32.7142857 15.6,32.7142857 C16.92,32.7142857 18,33.8163265 18,35.1632653 C18,36.5102041 16.92,37.6122449 15.6,37.6122449 Z M36,20.4693878 L38.8125,20.4693878 C39.135,20.4693878 39.435,20.5994898 39.66,20.8290816 L42.9075,24.1428571 L36,24.1428571 L36,20.4693878 Z M39.6,37.6122449 C38.28,37.6122449 37.2,36.5102041 37.2,35.1632653 C37.2,33.8163265 38.28,32.7142857 39.6,32.7142857 C40.92,32.7142857 42,33.8163265 42,35.1632653 C42,36.5102041 40.92,37.6122449 39.6,37.6122449 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
