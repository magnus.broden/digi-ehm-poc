import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-book',
	styleUrls: ['icon-book.scss'],
	scoped: true
})
export class IconBook {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-book"
				width="22"
				height="26"
				viewBox="0 0 22 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-book__shape"
					d="M22 18.281V1.22C22 .543 21.475 0 20.821 0H4.714C2.112 0 0 2.184 0 4.875v16.25C0 23.816 2.112 26 4.714 26h16.107c.654 0 1.179-.543 1.179-1.219v-.812c0-.381-.172-.726-.437-.95-.206-.782-.206-3.011 0-3.793.265-.219.437-.564.437-.945zM6.286 6.805A.3.3 0 016.58 6.5h10.411a.3.3 0 01.295.305V7.82a.3.3 0 01-.295.305H6.581a.3.3 0 01-.295-.305V6.805zm0 3.25a.3.3 0 01.294-.305h10.411a.3.3 0 01.295.305v1.015a.3.3 0 01-.295.305H6.581a.3.3 0 01-.295-.305v-1.015zM18.729 22.75H4.714c-.869 0-1.571-.726-1.571-1.625 0-.894.707-1.625 1.571-1.625H18.73c-.093.868-.093 2.382 0 3.25z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
