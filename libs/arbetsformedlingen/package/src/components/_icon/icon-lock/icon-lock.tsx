import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-lock',
	styleUrls: ['icon-lock.scss'],
	scoped: true
})
export class IconLock {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-lock"
				width="22"
				height="26"
				viewBox="0 0 22 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-lock__shape"
					d="M19.643 11.375h-1.179V7.719C18.464 3.463 15.115 0 11 0S3.536 3.463 3.536 7.719v3.656H2.357C1.056 11.375 0 12.467 0 13.813v9.75C0 24.907 1.056 26 2.357 26h17.286C20.944 26 22 24.908 22 23.562v-9.75c0-1.345-1.056-2.437-2.357-2.437zm-5.107 0H7.464V7.719c0-2.016 1.586-3.657 3.536-3.657s3.536 1.64 3.536 3.657v3.656z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
