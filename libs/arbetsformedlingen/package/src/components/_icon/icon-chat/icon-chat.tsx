import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-chat',
	styleUrl: 'icon-chat.scss',
	scoped: true
})
export class IconChat {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-chat"
				width="75"
				height="59"
				viewBox="0 0 75 59"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-chat__shape"
					d="M58.6018878,18.4166667 C68.2482164,21.350936 75,28.1169117 75,35.9986344 C75,40.5627858 72.7313949,44.7405697 68.9575824,48.0256244 C68.1256385,52.1022455 73.7379969,56.7951496 73.791053,56.8411327 C74.0242559,57.0527644 74.0879232,57.3656685 73.9500993,57.6416766 C73.8122754,57.9176847 73.5154053,58.0833333 73.1762122,58.0833333 C66.1479271,58.0833333 61.8551407,55.1571657 59.2473425,53.3535333 C55.7808895,54.4853745 51.9327985,55.1387725 47.8621159,55.1387725 C35.5982316,55.1387725 25.2379022,49.4034753 21.875,41.5273362 C23.7223276,41.7734552 25.5979518,41.8989234 27.4934568,41.8989234 C35.7104422,41.8989234 43.4577288,39.6267013 49.3079248,35.5008126 C55.3238753,31.257886 58.6371365,25.5702142 58.6371365,19.4852264 C58.6371365,19.1275435 58.6246958,18.7713935 58.6018878,18.4166667 Z M27.0263672,0 C41.9541534,0 54.0527344,8.67559709 54.0527344,19.3826957 C54.0527344,30.0897944 41.9541534,38.7655023 27.0263672,38.7655023 C22.9724121,38.7655023 19.140134,38.1038202 15.6879255,36.9576289 C13.0908435,38.7841288 8.81569731,41.7473958 1.81629334,41.7473958 C1.47849412,41.7473958 1.18284388,41.5797579 1.04558638,41.3001395 C0.908450338,41.0206321 0.971734551,40.7037608 1.20397911,40.4894459 C1.25681717,40.4428798 6.84611284,35.6905875 6.01758767,31.5621701 C2.25928283,28.2354657 0,24.0048247 0,19.3826957 C0,8.67559709 12.098581,0 27.0263672,0 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
