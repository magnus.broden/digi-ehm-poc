import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-eye-slash',
	styleUrls: ['icon-eye-slash.scss'],
	scoped: true
})
export class IconEyeSlash {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-eye-slash"
				width="29"
				height="26"
				viewBox="0 0 29 26"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-eye-slash__shape"
					d="M13.73 18.202c-4.052-.457-6.858-4.404-5.887-8.466l5.887 8.466zm.77 1.665a13.974 13.974 0 01-12.083-6.892 13.926 13.926 0 014.448-4.646L5.478 6.335A16.353 16.353 0 00.335 11.74a2.447 2.447 0 000 2.47A16.39 16.39 0 0014.5 22.3c.667 0 1.334-.04 1.996-.12l-1.612-2.318c-.128.003-.256.005-.384.005zm14.165-5.657a16.375 16.375 0 01-7.227 6.563l2.422 3.484a.61.61 0 01-.148.847l-1.044.735a.602.602 0 01-.842-.15L5.14 1.694a.61.61 0 01.148-.847L6.332.11a.602.602 0 01.842.15l2.795 4.02c1.44-.41 2.96-.63 4.531-.63a16.39 16.39 0 0114.165 8.09 2.447 2.447 0 010 2.47zm-2.082-1.235A13.965 13.965 0 0015.7 6.132a2.838 2.838 0 00-.797 1.977 2.829 2.829 0 002.82 2.838c1.556 0 2.819-1.27 2.819-2.838v-.002c1.542 2.903.842 6.602-1.86 8.703l1.344 1.933a13.951 13.951 0 006.557-5.768z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
