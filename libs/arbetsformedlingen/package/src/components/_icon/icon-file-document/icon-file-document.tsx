import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-file-document',
	styleUrl: 'icon-file-document.scss',
	scoped: true
})
export class IconFileDocument {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-file-document"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-file-document__shape"
					d="M27.16,12.75 L27.16,0 L9.16,0 C7.963,0 7,1.003125 7,2.25 L7,45.75 C7,46.996875 7.963,48 9.16,48 L39.4,48 C40.597,48 41.56,46.996875 41.56,45.75 L41.56,15 L29.32,15 C28.132,15 27.16,13.9875 27.16,12.75 Z M32.92,34.875 C32.92,35.49375 32.434,36 31.84,36 L16.72,36 C16.126,36 15.64,35.49375 15.64,34.875 L15.64,34.125 C15.64,33.50625 16.126,33 16.72,33 L31.84,33 C32.434,33 32.92,33.50625 32.92,34.125 L32.92,34.875 Z M32.92,28.875 C32.92,29.49375 32.434,30 31.84,30 L16.72,30 C16.126,30 15.64,29.49375 15.64,28.875 L15.64,28.125 C15.64,27.50625 16.126,27 16.72,27 L31.84,27 C32.434,27 32.92,27.50625 32.92,28.125 L32.92,28.875 Z M32.92,22.125 L32.92,22.875 C32.92,23.49375 32.434,24 31.84,24 L16.72,24 C16.126,24 15.64,23.49375 15.64,22.875 L15.64,22.125 C15.64,21.50625 16.126,21 16.72,21 L31.84,21 C32.434,21 32.92,21.50625 32.92,22.125 Z M41.56,11.428125 L41.56,12 L30.04,12 L30.04,0 L30.589,0 C31.165,0 31.714,0.234375 32.119,0.65625 L40.93,9.84375 C41.335,10.265625 41.56,10.8375 41.56,11.428125 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
