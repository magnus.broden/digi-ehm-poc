import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-calendar',
	styleUrls: ['icon-calendar.scss'],
	scoped: true
})
export class IconCalendar {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-calendar"
				width="22"
				height="26"
				viewBox="0 0 22 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-calendar__shape"
					d="M.59 9.75h20.82c.325 0 .59.274.59.61v13.203C22 24.907 20.944 26 19.643 26H2.357C1.056 26 0 24.908 0 23.562V10.36c0-.335.265-.609.59-.609zM22 7.516V5.687c0-1.345-1.056-2.437-2.357-2.437h-2.357V.61a.601.601 0 00-.59-.61h-1.964a.601.601 0 00-.59.61v2.64H7.858V.61a.601.601 0 00-.59-.61H5.305a.601.601 0 00-.59.61v2.64H2.357C1.056 3.25 0 4.342 0 5.688v1.828c0 .335.265.609.59.609h20.82c.325 0 .59-.274.59-.61z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
