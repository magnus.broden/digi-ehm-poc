import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-star',
	styleUrls: ['icon-star.scss'],
	scoped: true
})
export class IconStar {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-star"
				width="27"
				height="26"
				viewBox="0 0 27 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-star__shape"
					d="M12.03.901L8.74 7.598 1.38 8.676C.06 8.868-.468 10.502.49 11.438l5.324 5.21-1.26 7.36c-.226 1.33 1.17 2.326 2.338 1.704l6.584-3.475 6.584 3.475c1.168.617 2.564-.374 2.337-1.704l-1.26-7.36 5.325-5.21c.957-.936.428-2.57-.892-2.762l-7.36-1.078L14.922.901c-.59-1.194-2.297-1.209-2.892 0z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
