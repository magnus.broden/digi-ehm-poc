import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-videocamera',
	styleUrls: ['icon-videocamera.scss'],
	scoped: true
})
export class IconVideocamera {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-videocamera"
				width="48"
				height="48"
				viewBox="0 0 48 32.001"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-videocamera__shape"
					d="m43.993 26.226-9.856-6.671v-7.11l9.856-6.67v20.45ZM3.964 28.022h26.209V3.979H3.964v24.043ZM46.935.232a2.006 2.006 0 0 0-2.062.113L34.138 7.612V2.448A2.429 2.429 0 0 0 31.709.025H2.429A2.428 2.428 0 0 0 0 2.448v27.106a2.429 2.429 0 0 0 2.429 2.423h29.28a2.428 2.428 0 0 0 2.429-2.423v-5.165l10.735 7.267A2.002 2.002 0 0 0 48 30V2.003c0-.742-.41-1.422-1.065-1.77Z"
					fill="currentColor"
					fill-rule="evenodd"
					id="Icon"
				/>
			</svg>
		);
	}
}
