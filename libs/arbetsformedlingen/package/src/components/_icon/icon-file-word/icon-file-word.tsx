import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-file-word',
	styleUrl: 'icon-file-word.scss',
	scoped: true
})
export class IconFileWord {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-file-word"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-file-word__shape"
					d="M27.28,12.75 L27.28,0 L8.28,0 C7.0165,0 6,1.003125 6,2.25 L6,45.75 C6,46.996875 7.0165,48 8.28,48 L40.2,48 C41.4635,48 42.48,46.996875 42.48,45.75 L42.48,15 L29.56,15 C28.306,15 27.28,13.9875 27.28,12.75 Z M32.7045,24 L34.975,24 C35.7065,24 36.248,24.665625 36.0865,25.378125 L32.4765,41.128125 C32.3625,41.64375 31.897,42 31.365,42 L27.755,42 C27.2325,42 26.7765,41.64375 26.653,41.146875 C24.202,31.44375 24.677,33.534375 24.221,30.7875 L24.1735,30.7875 C24.069,32.128125 23.9455,32.41875 21.7415,41.146875 C21.618,41.64375 21.162,42 20.6395,42 L17.115,42 C16.583,42 16.1175,41.634375 16.0035,41.11875 L12.4125,25.36875 C12.251,24.665625 12.7925,24 13.524,24 L15.8515,24 C16.393,24 16.868,24.375 16.9725,24.909375 C18.4545,32.221875 18.882,35.175 18.9675,36.365625 C19.1195,35.409375 19.661,33.3 21.7605,24.8625 C21.884,24.35625 22.34,24.009375 22.872,24.009375 L25.6365,24.009375 C26.1685,24.009375 26.6245,24.365625 26.748,24.871875 C29.028,34.284375 29.484,36.496875 29.56,37.003125 C29.541,35.953125 29.313,35.334375 31.612,24.890625 C31.707,24.365625 32.1725,24 32.7045,24 L32.7045,24 Z M42.48,11.428125 L42.48,12 L30.32,12 L30.32,0 L30.8995,0 C31.5075,0 32.087,0.234375 32.5145,0.65625 L41.815,9.84375 C42.2425,10.265625 42.48,10.8375 42.48,11.428125 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
