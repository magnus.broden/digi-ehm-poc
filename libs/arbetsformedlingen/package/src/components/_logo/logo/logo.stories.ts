import { Template, enumSelect } from '../../../../../../shared/utils/src';
import { LogoColor } from './logo-color.enum';
import { LogoVariation } from './logo-variation.enum';

export default {
	title: 'logo/digi-logo',
	argTypes: {
		'af-color': enumSelect(LogoColor),
		'af-variation': enumSelect(LogoVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-logo',
	'af-color': LogoColor.PRIMARY,
	'af-variation': LogoVariation.LARGE,
	'af-system-name': '',
	'af-title': 'base',
	'af-desc': '',
	'af-title-id': null,
	children: ''
};
