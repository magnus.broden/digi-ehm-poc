# digi-logo

The Swedish Public Employment Service's logotype.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { LogoColor, LogoVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property          | Attribute            | Description                                                                                                  | Type                                         | Default                                 |
| ----------------- | -------------------- | ------------------------------------------------------------------------------------------------------------ | -------------------------------------------- | --------------------------------------- |
| `afColor`         | `af-color`           | Sätter variation. kan vara 'primary' eller 'secondary'.                                                      | `LogoColor.PRIMARY \| LogoColor.SECONDARY`   | `LogoColor.PRIMARY`                     |
| `afDesc`          | `af-desc`            | Lägger till ett descelement i svg:n                                                                          | `string`                                     | `undefined`                             |
| `afSvgAriaHidden` | `af-svg-aria-hidden` | För att dölja logotypen för skärmläsare.                                                                     | `boolean`                                    | `undefined`                             |
| `afSystemName`    | `af-system-name`     | Skriver ut namn på systemet bredvid logosymbolen.                                                            | `string`                                     | `undefined`                             |
| `afTitle`         | `af-title`           | Lägger till ett titleelement i svg:n. Standardtext är 'Arbetsförmedlingen'.                                  | `string`                                     | `'Arbetsförmedlingen'`                  |
| `afTitleId`       | `af-title-id`        | Sätter attributet 'id' på title-elementet inuti svg-elementet. Om inget väljs så skapas ett slumpmässigt id. | `string`                                     | `randomIdGenerator('digi-logo__title')` |
| `afVariation`     | `af-variation`       | Sätter storlek. Kan vara 'small' eller 'large'.                                                              | `LogoVariation.LARGE \| LogoVariation.SMALL` | `LogoVariation.LARGE`                   |


## CSS Custom Properties

| Name                                          | Description                                                    |
| --------------------------------------------- | -------------------------------------------------------------- |
| `--digi--logo--border-width`                  | var(--digi--border-width--primary);                            |
| `--digi--logo--border-width--large`           | var(--digi--border-width--secondary);                          |
| `--digi--logo--color--border--primary`        | var(--digi--color--text--primary);                             |
| `--digi--logo--color--border--secondary`      | var(--digi--color--text--inverted);                            |
| `--digi--logo--color--primary`                | var(--digi--color--text--secondary);                           |
| `--digi--logo--color--secondary`              | var(--digi--color--text--inverted);                            |
| `--digi--logo--font-family`                   | var(--digi--global--typography--font-family--default);         |
| `--digi--logo--font-size`                     | var(--digi--typography--title-logo--font-size--mobile);        |
| `--digi--logo--font-size--large`              | var(--digi--typography--title-logo--font-size--desktop-large); |
| `--digi--logo--font-size--system`             | var(--digi--typography--title-logo--font-size--mobile);        |
| `--digi--logo--font-size--system--large`      | var(--digi--typography--title-logo--font-size--desktop);       |
| `--digi--logo--font-weight`                   | var(--digi--typography--title-logo--font-weight--desktop);     |
| `--digi--logo--img--max-width`                | 312px;                                                         |
| `--digi--logo--img--max-width--large`         | 312px;                                                         |
| `--digi--logo--img--max-width--system`        | 29px;                                                          |
| `--digi--logo--img--max-width--system--large` | 36px;                                                          |
| `--digi--logo--img--min-width`                | 256px;                                                         |
| `--digi--logo--line-height`                   | var(--digi--typography--title-logo--line-height--desktop);     |
| `--digi--logo--margin--system`                | 0 0 0 var(--digi--gutter--medium);                             |
| `--digi--logo--margin--system--large`         | 0 0 0 var(--digi--gutter--largest);                            |
| `--digi--logo--padding`                       | var(--digi--gutter--medium);                                   |
| `--digi--logo--padding--large`                | var(--digi--gutter--medium);                                   |
| `--digi--logo--padding--large--wide`          | var(--digi--gutter--larger);                                   |
| `--digi--logo--padding--system`               | 0 0 0 var(--digi--gutter--medium);                             |
| `--digi--logo--padding--system--large`        | 0 0 0 var(--digi--gutter--largest);                            |
| `--digi--logo--padding--wide`                 | var(--digi--gutter--larger);                                   |
| `--digi--logo--system-name-color--primary`    | var(--digi--color--text--primary);                             |
| `--digi--logo--system-name-color--secondary`  | var(--digi--color--text--inverted);                            |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
