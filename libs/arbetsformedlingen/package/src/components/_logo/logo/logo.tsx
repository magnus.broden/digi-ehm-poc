import { Component, Prop, h } from '@stencil/core';
import { State } from '@stencil/core/internal';
import { LogoColor } from './logo-color.enum';
import { LogoVariation } from './logo-variation.enum';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

/**
 * @enums LogoSize - logo-size.enum.ts
 * @enums LogoVariation - logo-variation.enum.ts
 * @swedishName Logotyp
 */
@Component({
	tag: 'digi-logo',
	styleUrls: ['logo.scss'],
	scoped: true
})
export class Logo {
	@State() relation: string;

	/**
	 * Sätter variation. kan vara 'primary' eller 'secondary'.
	 * @en Sets the variation of the logo.
	 */
	@Prop() afColor: LogoColor = LogoColor.PRIMARY;


	/**
	 * Sätter storlek. Kan vara 'small' eller 'large'.
	 * @en Sets the size of the logo.
	 * @ignore
	 */
	@Prop() afVariation: LogoVariation = LogoVariation.LARGE;

	/**
	 * Skriver ut namn på systemet bredvid logosymbolen.
	 * @en Writes out system name next to the logo symbol.
	 */
	@Prop() afSystemName: string;

	/**
	 * Lägger till ett titleelement i svg:n. Standardtext är 'Arbetsförmedlingen'.
	 * @en Adds a title element inside the svg. Defaults to 'Arbetsförmedlingen'.
	 */
	@Prop() afTitle: string = 'Arbetsförmedlingen';

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja logotypen för skärmläsare.
	 * @en Hides the logotype for screen readers.
	 */
	@Prop() afSvgAriaHidden: boolean;

	/**
	 * Sätter attributet 'id' på title-elementet inuti svg-elementet.
	 * Om inget väljs så skapas ett slumpmässigt id.
	 * @en Id attribute on the title element inside of the svg. Defaults to random string.
	 */
	@Prop() afTitleId: string = randomIdGenerator('digi-logo__title');

	get cssModifiers() {
		return {
			'digi-logo': true,
			'digi-logo--small':
				this.afVariation === LogoVariation.SMALL || !!this.afSystemName,
			'digi-logo--large':
				this.afVariation === LogoVariation.LARGE && !this.afSystemName,
			'digi-logo--primary': this.afColor === LogoColor.PRIMARY,
			'digi-logo--secondary': this.afColor === LogoColor.SECONDARY,
			'digi-logo--system-name': !!this.afSystemName
		};
	}


	render() {
		return (
			<div class={{ ...this.cssModifiers }}>
				<div class="digi-logo__img">
					{(this.afVariation === LogoVariation.SMALL || !!this.afSystemName) && (
						<svg
							aria-labelledby={this.afTitle ? this.afTitleId : null}
							class="digi-logo-small"
							xmlns="http://www.w3.org/2000/svg"
							width="50.7"
							height="51.65"
							viewBox="0 0 50.7 51.65"
							aria-hidden={this.afSvgAriaHidden && 'true'}
							role="img"
						>
							{this.afTitle && <title id={this.afTitleId}>{this.afTitle}</title>}
							{this.afDesc && <desc>{this.afDesc}</desc>}
							<path
								class="digi-logo-small__shape"
								fill="#95C235"
								d="M18.88 18.16a14.13 14.13 0 1013.89 14.37v-.24A14 14 0 0018.9 18.16m0 21.67a7.55 7.55 0 117.42-7.68v.14a7.46 7.46 0 01-7.4 7.54"
							/>
							<path
								class="digi-logo-small__shape"
								fill="#95C235"
								d="M18.88 5v6.6a20.51 20.51 0 0120.33 20.69 20.84 20.84 0 01-2.94 10.77l5.41 3.63a27.46 27.46 0 004-14.36A27.06 27.06 0 0018.86 5"
							/>
						</svg>
					)}
					{this.afVariation === LogoVariation.LARGE && !this.afSystemName && (
						<svg
							aria-labelledby={this.afTitle ? this.afTitleId : null}
							class="digi-logo-large"
							height="51.65"
							viewBox="0 0 359.71 51.65"
							width="359.71"
							xmlns="http://www.w3.org/2000/svg"
							aria-hidden={this.afSvgAriaHidden && 'true'}
							role="img"
						>
							{this.afTitle && <title id={this.afTitleId}>{this.afTitle}</title>}
							{this.afDesc && <desc>{this.afDesc}</desc>}
							<g class="digi-logo__text" fill={this.afColor}>
								<path
									class="digi-logo__shape"
									d="M65.27 21.71h-5.64l-1.79 3.84h-3l7.54-16.27h.08L70 25.55h-3zm-1.1-2.51l-1.72-4.28-1.81 4.3zM81.5 25.28l-5.16-6.76v6.76h-2.82V9.22h5.63A4.69 4.69 0 0184 13.75v.16c0 2.7-2 4.22-4.34 4.49l5.39 6.84zm-5.16-13.36v4.14h2.73A2.1 2.1 0 0081.18 14a2 2 0 00-1.93-2.07h-2.91zM192.91 25.28l-5.16-6.76v6.76h-2.81V9.22h5.63a4.7 4.7 0 014.84 4.54v.15c0 2.7-1.95 4.22-4.34 4.49l5.4 6.84zm-5.16-13.36v4.14h2.74A2.11 2.11 0 00192.6 14a2 2 0 00-1.93-2.07h-2.92zM100.21 20.59c0 2.67-2.11 4.69-5.09 4.69H90V9.22h5.15a4.63 4.63 0 014.77 4.47v.22a3.92 3.92 0 01-1.48 3.26 4 4 0 011.77 3.42zm-7.47-8.67v3.94h2.38a1.87 1.87 0 002-1.71v-.24a1.91 1.91 0 00-1.82-2h-2.56zm4.53 8.67a2.14 2.14 0 00-2.15-2.07h-2.38v4.06h2.38a2 2 0 002.14-1.84.76.76 0 00.01-.15zM108.71 12v3.98h5.75v2.66h-5.75v3.98h5.94v2.66h-8.71V9.26h8.71v2.66h-5.94V12zM123.41 11.92h-4.03V9.26h10.87v2.66h-4.03v13.36h-2.81V11.92zM152.42 11.96v3.98h5.86v2.66h-5.86v6.68h-2.7V9.26h8.72v2.66l-6.02.04zM163.17 17.22a8.44 8.44 0 118.53 8.35h-.09a8.35 8.35 0 01-8.44-8.26v-.09m2-10.5a1.64 1.64 0 111.65 1.63 1.66 1.66 0 01-1.64-1.63m12 10.5a5.59 5.59 0 10-5.59 5.65 5.5 5.5 0 005.59-5.43v-.22m-2.43-10.54a1.64 1.64 0 111.65 1.63 1.63 1.63 0 01-1.64-1.62M217.68 8.9v16.38h-2.81V14.87l-5.32 4.73-5.35-4.78v10.46h-2.82V8.9h.04l8.09 7.04 8.05-7.04h.12zM226.71 12v3.98h5.74v2.66h-5.74v3.98h5.94v2.66h-8.72V9.26h8.72v2.66h-5.94V12zM251.27 17.29c0 4.69-3.56 8-8.41 8h-4.18V9.22h4.18c4.85 0 8.41 3.3 8.41 8m-2.86 0c0-3.18-2.22-5.33-5.55-5.33h-1.4v10.62h1.4c3.33 0 5.55-2.15 5.55-5.29M264.79 22.62v2.66h-8.72V9.26h2.82v13.36h5.9zM269.75 9.26h2.81v16.03h-2.81zM291.64 9.26V25.6h-.04l-10-10.1v9.78h-2.82V8.94h.04L288.83 19V9.26h2.81zM341.82 9.26V25.6h-.04l-10-10.1v9.78h-2.82V8.94h.04L339.05 19V9.26h2.77zM308.68 25.28h-3.87c-4.88 0-8.4-3.34-8.4-8s3.56-8 8.4-8h2.7v2.66h-2.7c-3.32 0-5.55 2.15-5.55 5.33s2.23 5.33 5.55 5.33h1.1v-4h-2.39v-2.66h5.16zM317.34 12v3.98h5.75v2.66h-5.75v3.98h5.94v2.66h-8.71V9.26h8.71v2.66h-5.94V12zM134.63 22.57h5a2.08 2.08 0 002-2.11 2 2 0 00-1.93-2.07h-.77c-2.86 0-4.73-1.52-4.73-4.51s2.19-4.58 4.8-4.58h4.54V12H139a1.89 1.89 0 00-2 1.78v.13a1.79 1.79 0 001.81 1.77h.9c3 0 4.73 1.63 4.73 4.82a4.65 4.65 0 01-4.44 4.78h-5.38zM75.81 35.43l-4.63 10.14h-.04l-2.04-4.48-2.05 4.48-4.63-10.14H64l3.05 6.76 1.28-2.79-1.82-3.97h1.58l1.01 2.24 1-2.24h1.55l-1.82 3.93 1.28 2.83 3.04-6.76h1.66zM79.3 36.77v2.87h3.74v1.41H79.3v2.87h3.82v1.41h-5.29v-9.9h5.29v1.42H79.3v-.08zM94.11 40.38a4.9 4.9 0 01-4.84 5h-2.84v-9.9h2.47A4.9 4.9 0 0194.1 40v.42m-1.47 0a3.46 3.46 0 00-3.34-3.58h-1.35V44h1a3.41 3.41 0 003.69-3.1 3.23 3.23 0 000-.44M96.8 35.43h1.47v9.9H96.8zM117.77 35.43v9.9h-1.47v-4.24h-4.75v4.24h-1.46v-9.9h1.46v4.25h4.75v-4.25h1.47zM132.91 38.22a2.86 2.86 0 01-2.85 2.87h-1.7v4.2h-1.47v-9.9h3a2.86 2.86 0 013 2.72v.11m-1.43 0a1.43 1.43 0 00-1.46-1.45h-1.65v2.91H130a1.42 1.42 0 001.46-1.38v-.08M135.45 42.27v-6.83h1.46v6.83a2 2 0 003.94 0v-6.83h1.47v6.83a3.19 3.19 0 01-3.07 3.3h-.33a3.23 3.23 0 01-3.46-3v-.32M151.65 42.46a2.81 2.81 0 01-2.74 2.88h-3.31v-9.9h3.05a2.78 2.78 0 012.89 2.67v.12a2.68 2.68 0 01-1 2.12 2.71 2.71 0 011.11 2.11zM147 36.81v2.79h1.62a1.4 1.4 0 00.24-2.79 1 1 0 00-.24 0zm3.2 5.65a1.52 1.52 0 00-1.6-1.46H147v2.91h1.62a1.55 1.55 0 001.56-1.45zM159.85 43.92v1.41h-5.17v-9.9h1.47v8.53h3.7v-.04zM162.43 35.43h1.47v9.9h-1.47zM166.57 40.36a4.89 4.89 0 014.84-4.94h1.86v1.41h-1.47a3.54 3.54 0 100 7.08h1.48v1.41h-1.48a4.94 4.94 0 01-5.2-4.66v-.34M183.34 36.77v2.87h3.74v1.41h-3.74v2.87h3.82v1.41h-5.29v-9.9h5.29v1.42h-3.82v-.08zM200.35 35.24v10.1h-1.47v-7.07l-3.44 2.9-3.43-2.9v7.07h-1.43V35.2h.04l4.86 4.13 4.87-4.09zM210 38.22a2.86 2.86 0 01-2.87 2.87h-1.73v4.25h-1.46v-9.89h3a2.84 2.84 0 013 2.65v.13m-1.47 0a1.45 1.45 0 00-1.47-1.45h-1.64v2.91H207a1.4 1.4 0 001.47-1.33v-.13M217.56 43.92v1.41h-5.17v-9.9h1.46v8.53h3.71v-.04zM219.09 40.38a5.21 5.21 0 115.23 5.19 5.19 5.19 0 01-5.21-5.17m9 0a3.75 3.75 0 10-3.75 3.78 3.68 3.68 0 003.75-3.61v-.17M235.1 40.54v4.79h-1.47v-4.79l-2.97-5.11h1.62l2.08 3.58 2.09-3.58h1.62l-2.97 5.11zM250.02 35.24v10.1h-1.46v-7.07l-3.44 2.9-3.43-2.9v7.07h-1.47V35.2h.04l4.86 4.13 4.9-4.09zM255.13 36.77v2.87h3.74v1.41h-3.74v2.87h3.82v1.41h-5.29v-9.9h5.29v1.42h-3.82v-.08zM270.07 35.43v10.1h-.04l-6.18-6.8v6.6h-1.46V35.2h.03l6.18 6.76v-6.53h1.47zM274.99 36.85h-2.54v-1.42h6.52v1.42h-2.51v8.48h-1.47v-8.48z"
								/>
								<path
									class="digi-logo__shape"
									d="M132.84 38.22a2.86 2.86 0 01-2.84 2.87h-1.7v4.2h-1.5v-9.9h3a2.87 2.87 0 013 2.74v.09m-1.43 0a1.45 1.45 0 00-1.37-1.45h-1.64v2.91H130a1.43 1.43 0 001.47-1.39v-.07M151.57 42.46a2.81 2.81 0 01-2.74 2.88h-3.36v-9.9h3.05a2.78 2.78 0 012.89 2.67v.12a2.62 2.62 0 01-1 2.12 2.69 2.69 0 011.16 2.11zm-4.67-5.65v2.79h1.62a1.4 1.4 0 00.24-2.79 1 1 0 00-.24 0zm3.2 5.65a1.52 1.52 0 00-1.58-1.46h-1.62v2.91h1.62a1.55 1.55 0 001.58-1.45zM159.78 43.92v1.41h-5.18v-9.9h1.47v8.53h3.71v-.04zM162.35 35.43h1.47v9.9h-1.47zM166.49 40.36a4.89 4.89 0 014.84-4.94h1.84v1.41h-1.47a3.54 3.54 0 100 7.08h1.48v1.41h-1.48a4.94 4.94 0 01-5.2-4.66v-.34M183.26 36.77v2.87H187v1.41h-3.74v2.87h3.82v1.41h-5.29v-9.9h5.29v1.42h-3.82v-.08zM200.27 35.24v10.1h-1.47v-7.07l-3.43 2.9-3.44-2.9v7.07h-1.43V35.2h.04l4.86 4.13 4.87-4.09zM209.88 38.22a2.86 2.86 0 01-2.88 2.87h-1.71v4.25h-1.46v-9.89h3a2.82 2.82 0 013 2.63.76.76 0 010 .15m-1.47 0a1.43 1.43 0 00-1.36-1.45h-1.66v2.91H207a1.4 1.4 0 001.46-1.34v-.12M217.48 43.92v1.41h-5.17v-9.9h1.47v8.53h3.7v-.04zM219 40.38a5.22 5.22 0 115.25 5.19A5.19 5.19 0 01219 40.4m9 0a3.75 3.75 0 10-3.75 3.78 3.68 3.68 0 003.75-3.63v-.17M235.02 40.54v4.79h-1.47v-4.79l-2.97-5.11h1.62l2.09 3.58 2.08-3.58h1.62l-2.97 5.11zM249.94 35.24v10.1h-1.46v-7.07l-3.44 2.9-3.43-2.9v7.07h-1.47V35.2h.04l4.86 4.13 4.9-4.09zM255.05 36.77v2.87h3.75v1.41h-3.75v2.87h3.82v1.41h-5.29v-9.9h5.29v1.42h-3.82v-.08zM269.99 35.43v10.1h-.04l-6.18-6.8v6.6h-1.46V35.2h.04l6.17 6.76v-6.53h1.47zM274.92 36.85h-2.55v-1.42h6.52v1.42h-2.51v8.48h-1.46v-8.48zM55.11 43.92H58a1.5 1.5 0 100-3h-.3c-2 0-2.94-1.14-2.94-2.75a2.65 2.65 0 012.53-2.76 1.88 1.88 0 01.41 0h2.74v1.42h-2.78a1.34 1.34 0 10-.23 2.67.89.89 0 00.23 0H58a2.7 2.7 0 013 2.37 2.44 2.44 0 010 .54 2.79 2.79 0 01-2.65 2.91h-3.24zM101.49 43.92h2.94a1.5 1.5 0 000-3h-.35a2.59 2.59 0 01-2.93-2.75 2.63 2.63 0 012.51-2.76 2 2 0 01.42 0h2.78v1.42h-2.78a1.34 1.34 0 10-.23 2.67.89.89 0 00.23 0h.35a2.7 2.7 0 013 2.37 2.44 2.44 0 010 .54 2.79 2.79 0 01-2.65 2.91h-3.25v-1.4zM286.63 43.92h2.93a1.5 1.5 0 000-3h-.35a2.59 2.59 0 01-2.92-2.21 2.68 2.68 0 010-.49 2.63 2.63 0 012.51-2.76 2 2 0 01.42 0H292v1.42h-2.8a1.34 1.34 0 10-.23 2.67h.58a2.69 2.69 0 013 2.35 2.62 2.62 0 010 .56 2.79 2.79 0 01-2.65 2.91h-3.23zM296.97 36.77v2.87h3.75v1.41h-3.75v2.87h3.82v1.41h-5.28v-9.9h5.28v1.42h-3.82v-.08zM308.92 45.29L305.53 41v4.39h-1.47v-9.9h3.21a2.8 2.8 0 11.33 5.51h-.33l3.4 4.3h-1.78zm-3.39-8.45v2.86h1.74a1.43 1.43 0 001.43-1.43 1.41 1.41 0 00-1.42-1.43h-1.75zM320.8 35.43l-4.63 10.06h-.04l-4.63-10.06h1.58l3.05 6.71 3.05-6.71h1.62zM322.87 35.43h1.47v9.9h-1.47zM327 40.36a4.89 4.89 0 014.84-4.94h1.83v1.41h-1.43a3.54 3.54 0 100 7.08h1.47v1.41h-1.46a4.94 4.94 0 01-5.2-4.66v-.34M337.94 36.77v2.87h3.74v1.41h-3.74v2.87h3.82v1.41h-5.29v-9.9h5.29v1.42h-3.82v-.08z"
								/>
							</g>
							<g class="digi-logo__symbol" fill="#95C235">
								<path
									class="digi-logo__shape"
									d="M18.88 18.16a14.13 14.13 0 1013.88 14.38v-.25a14 14 0 00-13.87-14.13m0 21.67a7.55 7.55 0 117.42-7.68v.14a7.48 7.48 0 01-7.42 7.54"
								/>
								<path
									class="digi-logo__shape"
									d="M18.88 5v6.6a20.51 20.51 0 0120.33 20.69 20.93 20.93 0 01-3 10.73l5.42 3.63a27.56 27.56 0 004-14.36A27.06 27.06 0 0018.81 5"
								/>
							</g>
						</svg>
					)}
				</div>
				{!!this.afSystemName && (
					<span class="digi-logo__system-name">{this.afSystemName}</span>
				)}
			</div>
		);
	}
}
