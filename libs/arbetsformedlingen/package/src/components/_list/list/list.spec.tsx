import { newSpecPage } from '@stencil/core/testing';
import { List } from './list';

describe('list', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [List],
			html: '<list></list>'
		});
		expect(root).toEqualHtml(`
      <list>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </list>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [List],
			html: `<list first="Stencil" last="'Don't call me a framework' JS"></list>`
		});
		expect(root).toEqualHtml(`
      <list first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </list>
    `);
	});
});
