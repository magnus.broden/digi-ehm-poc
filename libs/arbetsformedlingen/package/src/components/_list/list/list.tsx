import { Component, Prop, Element, State} from '@stencil/core';
import { HTMLStencilElement, h } from '@stencil/core/internal';
import { ListType } from './list-type.enum';
import { logger } from '../../../global/utils/logger';
 

/**
 * @slot default - ska innehålla <li>-element
 *
 * @enums ListType - list-type.enum.ts
 *
 * @swedishName Listor
 */
@Component({
	tag: 'digi-list',
	styleUrls: ['list.scss'],
	scoped: true
})
export class List {
	@Element() hostElement: HTMLStencilElement;
  
	/**
	 * Sätter typ. Kan vara 'bullet', 'numbered', eller 'checked'.
	 * @en Set list type. Can be 'bullet', 'numbered' eller 'checked'.
	 */
	@Prop() afListType: ListType = ListType.BULLET;
	@State() listItems: any[] = [];
	@State() hasSlottedItems: boolean;
	private _observer;

	componentWillLoad() {
		this.getListItems();
	}

	componentWillUpdate() {
		this.getListItems(true);	 
	}
 
	getListItems(update?: boolean) {
		let slotItem: any;

		if (update) {
			slotItem = this._observer.children;
		} else {
			slotItem = this.hostElement.children;
		}

		if (!slotItem || slotItem.length <= 0) {
			logger.warn(`The slot contains no children elements.`, this.hostElement);
			return;
		}

	 
		if (slotItem.length > 0) {
			this.hasSlottedItems = true;

			this.listItems = [...slotItem]
				.filter((element) => element.tagName.toLowerCase() === 'li')
				.map((element:HTMLElement) => {
					return {
						text: element.innerHTML
					};
				});
		}
	}
	get listElement() {
		return this.afListType === ListType.NUMBERED ? 'ol' : 'ul';
	}

	get cssModifiers() {
		return {
			'digi-list--numbered': this.afListType === ListType.NUMBERED,
			'digi-list--bullet': this.afListType === ListType.BULLET,
			'digi-list--checked': this.afListType === ListType.CHECKED
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-list': true,
					...this.cssModifiers
				}}
			>
        <digi-typography>
          <this.listElement class="digi-list__list">
            {this.listItems.map((item) => {
              return (
                <li class="digi-list__item">
                  {this.afListType === ListType.CHECKED && (
                    <digi-icon-check class="digi-list__icon"></digi-icon-check>
                  )}
                  <span innerHTML={item.text}></span>
                </li>
              );
            })}
          </this.listElement>
        </digi-typography>
				<div hidden class="digi-list__slot">
					<digi-util-mutation-observer
						ref={(el) => (this._observer = el)}
						onAfOnChange={() => this.getListItems()}
					>
						<slot></slot>
					</digi-util-mutation-observer>
				</div>
			</div>
		);
	}
}
