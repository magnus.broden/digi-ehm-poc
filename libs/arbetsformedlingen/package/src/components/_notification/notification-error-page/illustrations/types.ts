import { notificationIllustrations } from "./illustrations";

export const notificationNamesAsArray = Object.keys(notificationIllustrations);
