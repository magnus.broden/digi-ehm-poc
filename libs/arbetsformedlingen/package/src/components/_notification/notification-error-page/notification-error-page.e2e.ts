import { newE2EPage } from '@stencil/core/testing';

describe('notification-error-page', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<notification-error-page></notification-error-page>');
		const element = await page.find('notification-error-page');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<notification-error-page></notification-error-page>');
		const component = await page.find('notification-error-page');
		const element = await page.find('notification-error-page >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
