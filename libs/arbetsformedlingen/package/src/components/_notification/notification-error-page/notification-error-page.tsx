import { Element, Component, h, Host, Prop, State } from '@stencil/core';
import { ErrorPageStatusCodes } from './notification-error-page-httpstatus.enum';
import { HTMLStencilElement, Watch } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { notificationIllustrations, NotificationIllustrationName } from './illustrations/illustrations';
import { UtilBreakpointObserverBreakpoints } from '../../../enums-core';

/**
 * @slot default - Html-text, använd för att skriva över komponentens default brödtext
 * @slot search - Skicka in en komponent eller kod som har stöd för att skicka vidare besökaren till en söksida
 * @slot links - Ska vara en lista (<ul> eller <ol>) med listobjekt med länkar (<li><a href="/">Start</a></li>)
 *
 * @enums ErrorPageStatusCodes - notification-error-page-httpstatus.enum.ts
 *  
 * @swedishName Felsida
 */
@Component({
	tag: 'digi-notification-error-page',
	styleUrl: 'notification-error-page.scss',
	scoped: true
})
export class NotificationErrorPage {
	@Element() hostElement: HTMLStencilElement;

	@State() hasSearch: boolean;
	@State() hasLinks: boolean;
	@State() hasBodyText: boolean;
  @State() currentIllustration;
  @State() isMobile: boolean;

  setIllustration (name: NotificationIllustrationName) {
    const { NotificationIllustrationComponent } = notificationIllustrations[name];
    this.currentIllustration = NotificationIllustrationComponent;
  }

	/**
 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
 * @en Input id attribute. Defaults to random string.
 */
	@Prop() afId: string = randomIdGenerator('digi-notification-error-page');

	/**
	 * Http status kod som supportas av komponenten
	 * @en Http status code that are supported by our component
	 */
	@Prop() afHttpStatusCode: ErrorPageStatusCodes;

	/**
	 * Använd för egen rubrik
	 * @en Use custom heading
	 */
	@Prop({ mutable: true }) afCustomHeading: string;

	setHasSearch() {
		const searchSlot = !!this.hostElement.querySelector('[slot="search"]');
		if (searchSlot) {
			this.hasSearch = searchSlot;
		}
	}

	setHasLinks() {
		const linksSlot = !!this.hostElement.querySelector('[slot="links"]');
		if (linksSlot) {
			this.hasLinks = linksSlot;
		}
	}
	
	setHasBodyText() {
		const bodyTextSlot = !!this.hostElement.querySelector('[slot="bodytext"]');
		this.hasBodyText = bodyTextSlot;
	}

	componentWillLoad() {
		this.setHasSearch();
		this.setHasLinks();
		this.setHasBodyText();
    this.getImage();
	}

	componentWillUpdate() {
		this.setHasSearch();
		this.setHasLinks();
		this.setHasBodyText();
    this.getImage();
	}

	@Watch('afCustomHeading')
	private getText(): { heading: string, body: string } {
		const customHeading = this.afCustomHeading && this.afCustomHeading !== '' ? this.afCustomHeading : undefined;
		switch (this.afHttpStatusCode) {
			case ErrorPageStatusCodes.FORBIDDEN:
				return { heading: customHeading ?? "Sidan är låst", body: "<p>Det verkar som att du saknar behörighet till den här sidan. Du kanske har glömt att logga in, eller att slå på VPN? Prova det innan du går vidare.</p>" }
			case ErrorPageStatusCodes.GATEWAY_TIMEOUT:
				return { heading: customHeading ?? "Sidan kan inte visas just nu", body: "<p>På grund av ett tillfälligt problem kan sidan inte visas just nu. Du kan prova att ladda om sidan direkt eller om en stund. Om felet upprepar sig, gör en felanmälan.</p>" }
			case ErrorPageStatusCodes.GONE:
				return { heading: customHeading ?? "Sidan är borttagen", body: "<p>Du kan göra en sökning för att komma vidare eller använd de föreslagna länkarna.</p>" }
			case ErrorPageStatusCodes.INTERNAL_SERVER_ERRROR:
				return { heading: customHeading ?? "Sidan kan inte visas just nu", body: "<p>På grund av ett tillfälligt problem kan sidan inte visas just nu.<br>Vi jobbar på att lösa problemet. Prova igen om en stund. Om felet upprepar sig, gör en felanmälan. </p>" }
			case ErrorPageStatusCodes.NOT_FOUND:
				return { heading: customHeading ?? "Sidan kan inte hittas", body: "<p>Det kan bero på att länken hit är felaktig eller att sidan inte finns kvar. Prova att ladda om sidan, felet kan vara tillfälligt. Du kan göra en sökning för att komma vidare eller använd de föreslagna länkarna.</p>" }
			case ErrorPageStatusCodes.UNAUTHORIZED:
				return { heading: customHeading ?? "Sidan kan inte visas", body: "<p>Det verkar som att du saknar behörighet till den här sidan. Du kanske har glömt att logga in? Prova det innan du går vidare.</p>" }
			default: return {
				heading: "",
				body: ""
			};
		}
	}

	private getImage(): void {
		switch (this.afHttpStatusCode) {
      case ErrorPageStatusCodes.UNAUTHORIZED:
        return this.isMobile ? this.setIllustration('ill-401-mob') : this.setIllustration('ill-401-dskt')
			case ErrorPageStatusCodes.FORBIDDEN:
        return this.isMobile ? this.setIllustration('ill-403-mob') : this.setIllustration('ill-403-dskt')
      case ErrorPageStatusCodes.NOT_FOUND:
        return this.isMobile ? this.setIllustration('ill-404-mob') : this.setIllustration('ill-404-dskt')
      case ErrorPageStatusCodes.GONE:
        return this.isMobile ? this.setIllustration('ill-410-mob') : this.setIllustration('ill-410-dskt')
      case ErrorPageStatusCodes.INTERNAL_SERVER_ERRROR:
        return this.isMobile ? this.setIllustration('ill-500-mob') : this.setIllustration('ill-500-dskt')
			case ErrorPageStatusCodes.GATEWAY_TIMEOUT:
        return this.isMobile ? this.setIllustration('ill-504-mob') : this.setIllustration('ill-504-dskt')
		}
	}

	render() {
		return <Host>
			<digi-layout-container class={{
				'digi-notification-error-page': true,
			}}
				id={this.afId}>
				<digi-typography>
          <digi-util-breakpoint-observer
            onAfOnChange={(e) => {
              const val = e.detail.value;
              this.isMobile = val === UtilBreakpointObserverBreakpoints.SMALL || val === UtilBreakpointObserverBreakpoints.MEDIUM
            }}
          ></digi-util-breakpoint-observer>
					<div class="digi-notification-error-page__grid">
            <div class="digi-notification-error-page__picture">
              {this.currentIllustration ? (
                <this.currentIllustration></this.currentIllustration>
              ): null}
            </div>
						<div class="digi-notification-error-page__content">
							<h1 class="digi-notification-error-page__content__heading">{this.getText().heading}</h1>
							{this.hasBodyText ? (
								<div class="digi-notification-error-page__content__bodytext">
									<slot name="bodytext"></slot>
								</div>
							) : (
								<div class="digi-notification-error-page__content__bodytext">
									<div innerHTML={this.getText().body}></div>
								</div>
							)}
							{this.hasSearch && (
								<div class="digi-notification-error-page__content__search">
									<slot name="search"></slot>
								</div>)}
							{this.hasLinks && (
								<div class="digi-notification-error-page__content__links">
									<slot name="links"></slot>
								</div>)}
						</div>
					</div>
				</digi-typography>
			</digi-layout-container>
		</Host>;
	}
}
