import { newSpecPage } from '@stencil/core/testing';
import { NotificationErrorPage } from './notification-error-page';

describe('notification-error-page', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [NotificationErrorPage],
			html: '<notification-error-page></notification-error-page>'
		});
		expect(root).toEqualHtml(`
      <notification-error-page>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </notification-error-page>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [NotificationErrorPage],
			html: `<notification-error-page first="Stencil" last="'Don't call me a framework' JS"></notification-error-page>`
		});
		expect(root).toEqualHtml(`
      <notification-error-page first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </notification-error-page>
    `);
	});
});
