import {
	Component,
	Element,
	Event,
	EventEmitter,
	Prop,
	h
} from '@stencil/core';
import { HTMLStencilElement, Method } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { NotificationAlertSize } from './notification-alert-size.enum';
import { NotificationAlertHeadingLevel } from './notification-alert-heading-level.enum';
import { NotificationAlertVariation } from './notification-alert-variation.enum';
import {
	LayoutMediaObjectAlignment,
	ButtonVariation,
	TypographyVariation
} from '../../../enums-core';
import { IconName } from '../../../global/icon/icon';

/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums NotificationAlertSize - notification-alert-size.enum.ts
 * @enums NotificationAlertHeadingLevel - notification-alert-heading-level.enum.ts
 * @enums NotificationAlertVariation - notification-alert-variation.enum.ts
 * @swedishName Informationsmeddelande
 */
@Component({
	tag: 'digi-notification-alert',
	styleUrls: ['notification-alert.scss'],
	scoped: true
})
export class NotificationAlert {
	@Element() hostElement: HTMLStencilElement;

	public listItems = [];

	private _heading;

	/**
	 * Rubrikens text
	 * @en The heading text
	 */
	@Prop() afHeading: string;

	/**
	 * Sätter rubrikens vikt. 'h2' är förvalt.
	 * @en Set heading level. Default is 'h2'
	 */
	@Prop() afHeadingLevel: `${NotificationAlertHeadingLevel}` =
		NotificationAlertHeadingLevel.H2;

	/**
	 * Sätter storlek. Kan vara 'small', 'medium' eller 'large'.
	 * @en Set size of alert notification. Can be 'small', 'medium' or 'large'.
	 */
	@Prop() afSize: `${NotificationAlertSize}` = NotificationAlertSize.LARGE;

	/**
	 * Sätter variant. Kan vara 'info', 'success', 'warning' eller 'danger'.
	 * @en Set variation of alert notification. Can be 'info', 'success', 'warning' or 'danger'.
	 */
	@Prop() afVariation: `${NotificationAlertVariation}` =
		NotificationAlertVariation.INFO;

	/**
	 * Gör det möjligt att stänga meddelandet med en stängknapp
	 * @en If true, the close button is added to the component
	 */
	@Prop() afCloseable: boolean = false;

	/**
	 * Stängknappens attribut 'aria-label'
	 * @en Close button aria-label attribute
	 */
	@Prop() afCloseAriaLabel: string = 'Stäng meddelandet';

	/**
	 * Sätter attributet 'id'. Ges inget värde så genereras ett slumpmässigt.
	 * @en Label id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-notification-alert');

	/**
	 * Stängknappens 'onclick'-event
	 * @en Close button's 'onclick' event
	 */
	@Event() afOnClose: EventEmitter<MouseEvent>;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	/**
	 * Hämtar en referens till rubrikelementet. Bra för att t.ex. sätta fokus programmatiskt.
	 * @en Returns a reference to the heading element. Handy for setting focus programmatically.
	 */
	@Method()
	async afMGetHeadingElement() {
		return this._heading;
	}

	clickHandler(e) {
		this.afOnClose.emit(e);
	}

	componentDidLoad() {
		this.afOnReady.emit();  
	}

	get cssModifiers() {
		return {
			[`digi-notification-alert--size-${this.afSize}`]: !!this.afSize,
			[`digi-notification-alert--variation-${this.afVariation}`]:
				!!this.afVariation,
			'digi-notification-alert--closeable': this.afCloseable
		};
	}

	get icon(): IconName {
		switch (this.afVariation) {
			case NotificationAlertVariation.SUCCESS:
				return 'notification-succes';
			case NotificationAlertVariation.WARNING:
				return 'notification-warning';
			case NotificationAlertVariation.DANGER:
				return 'notification-error';
			default:
				return 'notification-info';
		}
	}

	render() {
		return (
			<div
				class={{
					'digi-notification-alert': true,
					...this.cssModifiers
				}}
			>
				<digi-layout-media-object
					af-alignment={LayoutMediaObjectAlignment.STRETCH}
					class="digi-notification-alert__layout"
				>
					<div class="digi-notification-alert__icon" slot="media">
						<digi-icon afName={this.icon} />
					</div>
					<div class="digi-notification-alert__wrapper">
						{this.afCloseable && (
							<digi-button
								af-aria-label={this.afCloseAriaLabel}
								af-variation={ButtonVariation.FUNCTION}
								onAfOnClick={(e) => this.clickHandler(e)}
								class="digi-notification-alert__close-button"
							>
								<span class="digi-notification-alert__close-button__text">Stäng</span>
								<digi-icon
									afName={`x`}
									slot="icon-secondary"
									aria-hidden="true"
								></digi-icon>
							</digi-button>
						)}
						<div class="digi-notification-alert__text">
							{this.afHeading && this.afSize === NotificationAlertSize.LARGE && (
								<this.afHeadingLevel
									ref={(el) => (this._heading = el)}
									tabindex="-1"
									class="digi-notification-alert__heading"
								>
									{this.afHeading}
								</this.afHeadingLevel>
							)}
							<div class="digi-notification-alert__content">
								<digi-typography af-variation={TypographyVariation.SMALL}>
									<slot></slot>
								</digi-typography>
							</div>
						</div>
					</div>
				</digi-layout-media-object>
			</div>
		);
	}
}
