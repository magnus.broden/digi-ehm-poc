export enum NotificationAlertSize {
    LARGE = 'large',
    MEDIUM = 'medium',
    SMALL = 'small'
}