import { Component, Element, Prop, h } from '@stencil/core';
import { QuoteSingleVariation } from './quote-single-variation.enum';
import { HTMLStencilElement } from '@stencil/core/internal';

/**
 * @slot default -
 *
 * @enums QuoteSingleVariation - quote-single-variation.enum.ts
 *
 * @swedishName Citat
 */
@Component({
	tag: 'digi-quote-single',
	styleUrls: ['quote.scss'],
	scoped: true
})
export class QuoteSingle {
	@Element() hostElement: HTMLStencilElement;

	/**
	 * Sätter variant. Kontrollerar bakgrundsfärgen.
	 * @en Set variation. Controls background color.
	 */
	@Prop() afVariation: QuoteSingleVariation = QuoteSingleVariation.PRIMARY;

	/**
	 * Innehållet på citatet
	 * @en The quote itself
	 */
	@Prop() afQuoteText: string;

	/**
	 * Namnet på författaren bakom citatet
	 * @en the quote authors
	 */
	@Prop() afQuoteAuthorName: string;

	/**
	 * Beskrivning på författaren
	 * @en Description of the author
	 */
	@Prop() afQuoteAuthorTitle: string;


	get cssModifiers() {
		return {
			'digi-quote-single--primary':
				this.afVariation === QuoteSingleVariation.PRIMARY,
			'digi-quote-single--secondary':
				this.afVariation === QuoteSingleVariation.SECONDARY,
			'digi-quote-single--tertiary':
				this.afVariation === QuoteSingleVariation.TERTIARY
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-quote-single': true,
					...this.cssModifiers
				}}
			>
				<div class="digi-quote-single__content">
					<section
						class= "digi-quote-single__text"
					>
						<digi-typography>
							<svg
                class="digi-quote-single__icon"
                aria-hidden="true"
								width="51px"
								height="40px"
								viewBox="0 0 51 40"
                fill="#00005A"
							>
										<path
											d="M0.602908277,34.1566952 C4.73191648,32.1718898 7.96569724,29.8622982 10.3042506,27.2279202 C12.6428039,24.5935423 13.99478,21.3998101 14.360179,17.6467236 L11.2908277,17.6467236 C9.02535421,17.6467236 7.18008949,17.3941121 5.75503356,16.8888889 C4.32997763,16.3836657 3.17897092,15.679962 2.30201342,14.7777778 C1.46159582,13.9116809 0.867822521,12.9553656 0.520693512,11.9088319 C0.173564504,10.8622982 0,9.77967711 0,8.66096866 C0,6.35137702 0.940902312,4.33048433 2.82270694,2.5982906 C4.70451156,0.866096866 6.92431022,0 9.48210291,0 C13.5014914,0 16.5891126,1.31718898 18.7449664,3.95156695 C20.9008203,6.58594492 21.9787472,10.2307692 21.9787472,14.8860399 C21.9787472,19.2165242 20.1243475,23.5830959 16.4155481,27.985755 C12.7067487,32.3884141 8.23974646,35.7264957 3.01454139,38 L0.602908277,34.1566952 Z M27.6241611,34.1566952 C31.7531693,32.1718898 34.98695,29.8622982 37.3255034,27.2279202 C39.6640567,24.5935423 41.0160328,21.3998101 41.3814318,17.6467236 L38.3120805,17.6467236 C36.046607,17.6467236 34.2013423,17.3941121 32.7762864,16.8888889 C31.3512304,16.3836657 30.2002237,15.679962 29.3232662,14.7777778 C28.4828486,13.9116809 27.8890753,12.9553656 27.5419463,11.9088319 C27.1948173,10.8622982 27.0212528,9.77967711 27.0212528,8.66096866 C27.0212528,6.35137702 27.9621551,4.33048433 29.8439597,2.5982906 C31.7257644,0.866096866 33.945563,0 36.5033557,0 C40.5227442,0 43.6103654,1.31718898 45.7662192,3.95156695 C47.9220731,6.58594492 49,10.2307692 49,14.8860399 C49,19.2165242 47.1456003,23.5830959 43.4368009,27.985755 C39.7280015,32.3884141 35.2609993,35.7264957 30.0357942,38 L27.6241611,34.1566952 Z"
										></path>
							</svg>
							<blockquote><slot></slot></blockquote>
						</digi-typography>
					</section>
					<digi-digi-typography-meta af-variation="primary">
						{this.afQuoteAuthorName && (
							<p class="digi-quote-single__author">{this.afQuoteAuthorName}</p>
					)}
						{this.afQuoteAuthorTitle && (
						<p class="digi-quote-single__author-title" slot="secondary">{this.afQuoteAuthorTitle}</p>
					)}
					</digi-digi-typography-meta>
					{/* {this.afQuoteAuthorName && (
						<div class="digi-quote-single__author">
							<p>{this.afQuoteAuthorName}</p>
						</div>
					)}
					{this.afQuoteAuthorTitle && (
						<p class="digi-quote-single__author-title">{this.afQuoteAuthorTitle}</p>
					)} */}
				</div>
			</div>
		);
	}
}
