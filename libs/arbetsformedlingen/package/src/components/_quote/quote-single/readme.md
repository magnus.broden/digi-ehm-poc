# digi-quote-single

This is a container component meant to wrap 1-3 `digi-info-card` components of type multi.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { InfoCardMultiContainerHeadingLevel } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property             | Attribute               | Description                                   | Type                                                                                              | Default                        |
| -------------------- | ----------------------- | --------------------------------------------- | ------------------------------------------------------------------------------------------------- | ------------------------------ |
| `afQuoteAuthorName`  | `af-quote-author-name`  | Namnet på författaren bakom citatet           | `string`                                                                                          | `undefined`                    |
| `afQuoteAuthorTitle` | `af-quote-author-title` | Beskrivning på författaren                    | `string`                                                                                          | `undefined`                    |
| `afQuoteText`        | `af-quote-text`         | Innehållet på citatet                         | `string`                                                                                          | `undefined`                    |
| `afVariation`        | `af-variation`          | Sätter variant. Kontrollerar bakgrundsfärgen. | `QuoteSingleVariation.PRIMARY \| QuoteSingleVariation.SECONDARY \| QuoteSingleVariation.TERTIARY` | `QuoteSingleVariation.PRIMARY` |


## Slots

| Slot          | Description |
| ------------- | ----------- |
| `"default -"` |             |


## CSS Custom Properties

| Name                                          | Description                                |
| --------------------------------------------- | ------------------------------------------ |
| `--digi--quote-single--background--primary`   | var(--digi--color--background--neutral-5); |
| `--digi--quote-single--background--secondary` | var(--digi--color--background--secondary); |
| `--digi--quote-single--background--tertiary`  | var(--digi--color--background--primary);   |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-quote-single --> digi-typography
  style digi-quote-single fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
