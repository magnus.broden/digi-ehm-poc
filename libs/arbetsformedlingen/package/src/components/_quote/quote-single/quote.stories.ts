import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { QuoteSingleVariation } from './quote-single-variation.enum';
//import { InfoCardType } from '../info-card/info-card-type.enum';

export default {
	title: 'quote/digi-quote',
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-quote-single',
	'af-quote-author': '',
	'af-quote-author-title': '',
    'af-variation': QuoteSingleVariation.PRIMARY,
	/* html */
	children: `
    <digi-quote-single
        af-quote-author="I am the author"
        af-heading-level=""
        af-variation=""
    >
        <p>
        These are just words to illustrate how it looks like with text inside. These are just words to illustrate how it looks like with text inside.
        </p>
    </digi-quote-single>`
};
