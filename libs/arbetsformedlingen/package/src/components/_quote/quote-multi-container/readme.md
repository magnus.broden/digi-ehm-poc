# digi-quote-multi-container

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                 | Type                                                                                                                                                                                                                               | Default     |
| ---------------- | ------------------ | --------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `afHeading`      | `af-heading`       | Rubrikens text              | `string`                                                                                                                                                                                                                           | `undefined` |
| `afHeadingLevel` | `af-heading-level` | Sätt rubrikens vikt (h1-h6) | `QuoteMultiContainerHeadingLevel.H1 \| QuoteMultiContainerHeadingLevel.H2 \| QuoteMultiContainerHeadingLevel.H3 \| QuoteMultiContainerHeadingLevel.H4 \| QuoteMultiContainerHeadingLevel.H5 \| QuoteMultiContainerHeadingLevel.H6` | `undefined` |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
