import { newSpecPage } from '@stencil/core/testing';
import { QuoteMultiContainer } from './quote-multi-container';

describe('quote-multi-container', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [QuoteMultiContainer],
			html: '<quote-multi-container></quote-multi-container>'
		});
		expect(root).toEqualHtml(`
      <quote-multi-container>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </quote-multi-container>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [QuoteMultiContainer],
			html: `<quote-multi-container first="Stencil" last="'Don't call me a framework' JS"></quote-multi-container>`
		});
		expect(root).toEqualHtml(`
      <quote-multi-container first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </quote-multi-container>
    `);
	});
});
