import {
	Component,
	h,
	Prop,
	Element,
} from '@stencil/core';
import { QuoteMultiContainerHeadingLevel } from './quote-multi-container-heading-level.enum';
import { HTMLStencilElement } from '@stencil/core/internal';

/**
 * @slot mySlot - Slot description, if any
 *
 * @enums QuoteMultiContainerHeadingLevel - quote-multi-container-heading-level.enum.ts
 *
 * @swedishName Citatbehållare
 */
@Component({
	tag: 'digi-quote-multi-container',
	styleUrl: 'quote-multi-container.scss',
	scoped: true
})
export class QuoteMultiContainer {
	@Element() hostElement: HTMLStencilElement;

	/**
   * Rubrikens text
   * @en The heading text
   */
  @Prop() afHeading: string;

  /**
   * Sätt rubrikens vikt (h1-h6)
   * @en Set heading level (h1-h6)
   */
  @Prop() afHeadingLevel: QuoteMultiContainerHeadingLevel;

	render() {
		return (
				<div class="digi-quote-multi-container">
        {this.afHeading && this.afHeadingLevel && (
          <this.afHeadingLevel class="digi-quote-multi-container__heading">
            {this.afHeading}
          </this.afHeadingLevel>
        )}
				<div class="digi-quote-multi-container__content">
					<slot></slot>
				</div>
      </div>
		);
	}
}
