import { newE2EPage } from '@stencil/core/testing';

describe('quote-multi-container', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<quote-multi-container></quote-quote-multi-container>'
		);
		const element = await page.find('quote-multi-container');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<quote-multi-container></quote-multi-container>'
		);
		const component = await page.find('quote-multi-container');
		const element = await page.find('quote-multi-container >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
