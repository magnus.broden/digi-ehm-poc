# digi-dialog

<!-- Auto Generated Below -->


## Properties

| Property                    | Attribute                      | Description                                                                                                      | Type                                                                                                                                                 | Default                            |
| --------------------------- | ------------------------------ | ---------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------- |
| `afCloseButtonText`         | `af-close-button-text`         | Visa stäng-knapp                                                                                                 | `string`                                                                                                                                             | `'Stäng'`                          |
| `afFallbackElementSelector` | `af-fallback-element-selector` | Om man vill ha annat element än sista aktiva elementet innan modalen öppnades                                    | `string`                                                                                                                                             | `undefined`                        |
| `afHeading`                 | `af-heading`                   | Text till rubrik                                                                                                 | `string`                                                                                                                                             | `undefined`                        |
| `afHeadingLevel`            | `af-heading-level`             | Sätt rubrikens vikt. 'h2' är förvalt.                                                                            | `DialogHeadingLevel.H1 \| DialogHeadingLevel.H2 \| DialogHeadingLevel.H3 \| DialogHeadingLevel.H4 \| DialogHeadingLevel.H5 \| DialogHeadingLevel.H6` | `DialogHeadingLevel.H2`            |
| `afId`                      | `af-id`                        | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                            | `string`                                                                                                                                             | `randomIdGenerator('digi-dialog')` |
| `afPrimaryButtonText`       | `af-primary-button-text`       | Text på primär knapp                                                                                             | `string`                                                                                                                                             | `undefined`                        |
| `afSecondaryButtonText`     | `af-secondary-button-text`     | Text på sekundär knapp                                                                                           | `string`                                                                                                                                             | `undefined`                        |
| `afShowDialog`              | `af-show-dialog`               | Sätt till true om modalen ska visas                                                                              | `boolean`                                                                                                                                            | `undefined`                        |
| `afSize`                    | `af-size`                      | Sätt storlek på modalen small = max-width: 450px, medium = max-width: 675px (standard), large = max-width: 900px | `DialogSize.LARGE \| DialogSize.MEDIUM \| DialogSize.SMALL`                                                                                          | `DialogSize.MEDIUM`                |
| `afVariation`               | `af-variation`                 | Sätt variation på modalen. OBS! Sekundär variant är endast en beta.                                              | `DialogVariation.PRIMARY \| DialogVariation.SECONDARY`                                                                                               | `DialogVariation.PRIMARY`          |


## Events

| Event                    | Description                        | Type                      |
| ------------------------ | ---------------------------------- | ------------------------- |
| `afOnClose`              | Event när modalen stängs           | `CustomEvent<MouseEvent>` |
| `afPrimaryButtonClick`   | Primära knappens 'onclick'-event   | `CustomEvent<MouseEvent>` |
| `afSecondaryButtonClick` | Sekundära knappens 'onclick'-event | `CustomEvent<MouseEvent>` |


## Slots

| Slot             | Description                                    |
| ---------------- | ---------------------------------------------- |
| `"default"`      | kan innehålla vad som helst                    |
| `"over-heading"` | Innehåll som hamnar över den inbyggda rubriken |


## CSS Custom Properties

| Name                                                  | Description                                                |
| ----------------------------------------------------- | ---------------------------------------------------------- |
| `--digi--dialog--backdrop--align--items`              | center;                                                    |
| `--digi--dialog--backdrop--background--color`         | rgba(0, 0, 0, 0.7);                                        |
| `--digi--dialog--backdrop--display`                   | flex;                                                      |
| `--digi--dialog--backdrop--height`                    | 100vh;                                                     |
| `--digi--dialog--backdrop--justify--content`          | center;                                                    |
| `--digi--dialog--backdrop--left`                      | 0;                                                         |
| `--digi--dialog--backdrop--opacity`                   | 1;                                                         |
| `--digi--dialog--backdrop--position`                  | fixed;                                                     |
| `--digi--dialog--backdrop--top`                       | 0;                                                         |
| `--digi--dialog--backdrop--width`                     | 100vw;                                                     |
| `--digi--dialog--backdrop--z--index`                  | 999;                                                       |
| `--digi--dialog--button--container--display`          | flex;                                                      |
| `--digi--dialog--button--container--flex-wrap`        | wrap;                                                      |
| `--digi--dialog--button--container--gap`              | var(--digi--gutter--large);                                |
| `--digi--dialog--button--container--padding--top`     | var(--digi--gutter--largest-2);                            |
| `--digi--dialog--button--text--align--mobile`         | center;                                                    |
| `--digi--dialog--close--button--display`              | grid;                                                      |
| `--digi--dialog--close--button--justify--content`     | end;                                                       |
| `--digi--dialog--close--button--margin--right`        | -1rem;                                                     |
| `--digi--dialog--content--color--background--primary` | var(--digi--global--color--profile--white--base);          |
| `--digi--dialog--content--color--primary`             | var(--digi--global--color--neutral--grayscale--darkest-6); |
| `--digi--dialog--content--gutter`                     | var(--digi--gutter--largest);                              |
| `--digi--dialog--content--margin`                     | var(--digi--margin--large) auto;                           |
| `--digi--dialog--content--padding`                    | var(--digi--padding--large);                               |
| `--digi--dialog--content--width--large`               | 56.25rem;                                                  |
| `--digi--dialog--content--width--medium`              | 42.1875rem;                                                |
| `--digi--dialog--content--width--small`               | 28.75rem;                                                  |


## Dependencies

### Depends on

- [digi-util-keydown-handler](../../../__core/_util/util-keydown-handler)
- [digi-typography](../../../__core/_typography/typography)
- [digi-button](../../../__core/_button/button)
- [digi-icon](../../../__core/_icon/icon)

### Graph
```mermaid
graph TD;
  digi-dialog --> digi-util-keydown-handler
  digi-dialog --> digi-typography
  digi-dialog --> digi-button
  digi-dialog --> digi-icon
  style digi-dialog fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
