import { enumSelect, parseArgs } from '../../../../../../shared/utils/src';
import { DialogSize } from './dialog-size.enum';
import { DialogVariation } from './dialog-variation.enum';
import { DialogHeadingLevel } from './dialog-heading-level.enum';

export default {
	title: 'dialog/digi-dialog',
	parameters: {
		actions: {
			handles: ['afPrimaryButtonClick', 'afSecondaryButtonClick', 'afOnBlur']
		}
	},
	argTypes: {
		'af-size': enumSelect(DialogSize),
		'af-variation': enumSelect(DialogVariation),
		'af-heading-level': enumSelect(DialogHeadingLevel)
	}
};

const Template = (args) => /*html*/ `
	<script>
		document.addEventListener('afPrimaryButtonClick', (e) => {
			console.log('Primär klick', e.detail)
		})
		document.addEventListener('afSecondaryButtonClick', (e) => {
			console.log('Sekundär klick', e.detail)
		}) 
		document.addEventListener('afOnClick', (e) => {
			if(e.target.getAttribute('id') === 'open-btn'){
				document.getElementById('dialog').setAttribute('af-show-dialog', 'true')
			}
		})
		document.addEventListener('afOnClose', () => {
			document.getElementById('dialog').setAttribute('af-show-dialog', 'false')
		})
  </script>
	<digi-typography>
		<digi-button id="open-btn" af-variation="primary" (afOnClick)="()=>{}">Open dialog</digi-button>
		<digi-dialog ${parseArgs(
			args
		)} (afPrimaryButtonClick)="()=>{}" (afSecondaryButtonClick)="()=> {}" (afOnFocus)="()=>{}">
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			</p>
		</digi-dialog>
	</digi-typography>
`;

export const Standard = Template.bind({});

Standard.args = {
	id: 'dialog',
	'af-variation': DialogVariation.PRIMARY,
	'af-size': DialogSize.MEDIUM,
	'af-heading-level': DialogHeadingLevel.H2,
	'af-show-dialog': false,
	'af-close-button-text': 'Close',
	'af-heading': 'Dialog heading',
	'af-primary-button-text': 'Primary button',
	'af-secondary-button-text': 'Secondary button',
	'af-fallback-element-selector': '',
	children: ''
};
