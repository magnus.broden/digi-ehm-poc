# digi-typography-heading-jumbo

This is a huge heading with a blue line beneath.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { TypographyHeadingJumboLevel } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description                           | Type                                                                                                                                                                                                       | Default                          |
| --------- | ---------- | ------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| `afLevel` | `af-level` | Sätt rubrikens vikt. 'h1' är förvalt. | `TypographyHeadingJumboLevel.H1 \| TypographyHeadingJumboLevel.H2 \| TypographyHeadingJumboLevel.H3 \| TypographyHeadingJumboLevel.H4 \| TypographyHeadingJumboLevel.H5 \| TypographyHeadingJumboLevel.H6` | `TypographyHeadingJumboLevel.H1` |
| `afText`  | `af-text`  | Rubrikens text                        | `string`                                                                                                                                                                                                   | `''`                             |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
