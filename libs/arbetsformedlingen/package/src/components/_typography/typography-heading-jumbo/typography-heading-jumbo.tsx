import { Component, Prop, h } from '@stencil/core';
import { TypographyHeadingJumboLevel } from './typography-heading-jumbo-level.enum';

/**
 * @enums TypographyHeadingJumboLevel - typography-heading-jumbo-level.enum.ts
 * @swedishName Jumborubrik
 */
@Component({
  tag: 'digi-typography-heading-jumbo',
  styleUrls: ['typography-heading-jumbo.scss'],
  scoped: true,
})
export class TypographyHeadingJumbo {
  /**
   * Rubrikens text
   * @en The heading text
   */
  @Prop() afText = '';

  /**
   * Sätt rubrikens vikt. 'h1' är förvalt.
   * @en Set heading level. Default is 'h1'
   */
  @Prop() afLevel: TypographyHeadingJumboLevel = TypographyHeadingJumboLevel.H1;

  render() {
    return (
      <this.afLevel class="digi-typography-heading-jumbo">
        {this.afText}
      </this.afLevel>
    );
  }
}
