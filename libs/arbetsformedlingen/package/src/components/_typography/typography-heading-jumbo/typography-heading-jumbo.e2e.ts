import { newE2EPage } from '@stencil/core/testing';

describe('digi-typography-heading-jumbo', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-typography-heading-jumbo></digi-typography-heading-jumbo>');

    const element = await page.find('digi-typography-heading-jumbo');
    expect(element).toHaveClass('hydrated');
  });
});
