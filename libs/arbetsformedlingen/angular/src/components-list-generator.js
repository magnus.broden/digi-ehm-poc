'use strict';
(function () {
	const glob = require('glob');
	const fs = require('fs');
	const path = require('path');

	const getFlags = function () {
		const args = process.argv.map((val) => val.replace(new RegExp('-', 'g'), ''));
		return args;
	};

	console.log('\nListing components from arbetsformedlingen...');
	const componentsFile = fs.readFileSync(
		__dirname + '/lib/stencil-generated/components.ts',
		{ encoding: 'utf8' }
	);
	const componentsList = componentsFile.match(/Components\.[a-zA-Z]+/gm);

	const data = fs.readFileSync(__dirname + '/lib/digi-arbetsformedlingen-angular.module.ts', {
		encoding: 'utf8'
	});
	const updatedData = data.replace(
		/const COMPONENTS = \[[\s\S][^;]*\];/gm,
		`const COMPONENTS = ${JSON.stringify(componentsList, null, 1).replace(/\"/gm,'')};`
	);

	getFlags().includes('verbose') && console.log(componentsList);

	const file = fs.createWriteStream(
		__dirname + '/lib/digi-arbetsformedlingen-angular.module.ts'
	);
	file.on('error', function (err) {
		console.error(err);
	});
	file.write(updatedData);
	file.end();

	console.log('Components successfully added to digi-arbetsformedlingen-angular.module.ts\n');
})();
