/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';
import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import type { Components } from '@digi/arbetsformedlingen/components';

import { defineCustomElement as defineDigiBarChart } from '@digi/arbetsformedlingen/components/digi-bar-chart.js';
import { defineCustomElement as defineDigiButton } from '@digi/arbetsformedlingen/components/digi-button.js';
import { defineCustomElement as defineDigiCalendar } from '@digi/arbetsformedlingen/components/digi-calendar.js';
import { defineCustomElement as defineDigiCalendarDatepicker } from '@digi/arbetsformedlingen/components/digi-calendar-datepicker.js';
import { defineCustomElement as defineDigiCalendarWeekView } from '@digi/arbetsformedlingen/components/digi-calendar-week-view.js';
import { defineCustomElement as defineDigiCard } from '@digi/arbetsformedlingen/components/digi-card.js';
import { defineCustomElement as defineDigiChartLine } from '@digi/arbetsformedlingen/components/digi-chart-line.js';
import { defineCustomElement as defineDigiCode } from '@digi/arbetsformedlingen/components/digi-code.js';
import { defineCustomElement as defineDigiCodeBlock } from '@digi/arbetsformedlingen/components/digi-code-block.js';
import { defineCustomElement as defineDigiCodeExample } from '@digi/arbetsformedlingen/components/digi-code-example.js';
import { defineCustomElement as defineDigiDialog } from '@digi/arbetsformedlingen/components/digi-dialog.js';
import { defineCustomElement as defineDigiExpandableAccordion } from '@digi/arbetsformedlingen/components/digi-expandable-accordion.js';
import { defineCustomElement as defineDigiExpandableFaq } from '@digi/arbetsformedlingen/components/digi-expandable-faq.js';
import { defineCustomElement as defineDigiExpandableFaqItem } from '@digi/arbetsformedlingen/components/digi-expandable-faq-item.js';
import { defineCustomElement as defineDigiFormCategoryFilter } from '@digi/arbetsformedlingen/components/digi-form-category-filter.js';
import { defineCustomElement as defineDigiFormCheckbox } from '@digi/arbetsformedlingen/components/digi-form-checkbox.js';
import { defineCustomElement as defineDigiFormErrorList } from '@digi/arbetsformedlingen/components/digi-form-error-list.js';
import { defineCustomElement as defineDigiFormFieldset } from '@digi/arbetsformedlingen/components/digi-form-fieldset.js';
import { defineCustomElement as defineDigiFormFileUpload } from '@digi/arbetsformedlingen/components/digi-form-file-upload.js';
import { defineCustomElement as defineDigiFormFilter } from '@digi/arbetsformedlingen/components/digi-form-filter.js';
import { defineCustomElement as defineDigiFormInput } from '@digi/arbetsformedlingen/components/digi-form-input.js';
import { defineCustomElement as defineDigiFormInputSearch } from '@digi/arbetsformedlingen/components/digi-form-input-search.js';
import { defineCustomElement as defineDigiFormLabel } from '@digi/arbetsformedlingen/components/digi-form-label.js';
import { defineCustomElement as defineDigiFormRadiobutton } from '@digi/arbetsformedlingen/components/digi-form-radiobutton.js';
import { defineCustomElement as defineDigiFormRadiogroup } from '@digi/arbetsformedlingen/components/digi-form-radiogroup.js';
import { defineCustomElement as defineDigiFormReceipt } from '@digi/arbetsformedlingen/components/digi-form-receipt.js';
import { defineCustomElement as defineDigiFormSelect } from '@digi/arbetsformedlingen/components/digi-form-select.js';
import { defineCustomElement as defineDigiFormTextarea } from '@digi/arbetsformedlingen/components/digi-form-textarea.js';
import { defineCustomElement as defineDigiFormValidationMessage } from '@digi/arbetsformedlingen/components/digi-form-validation-message.js';
import { defineCustomElement as defineDigiIcon } from '@digi/arbetsformedlingen/components/digi-icon.js';
import { defineCustomElement as defineDigiIconAccessibilityDeaf } from '@digi/arbetsformedlingen/components/digi-icon-accessibility-deaf.js';
import { defineCustomElement as defineDigiIconAccessibilityUniversal } from '@digi/arbetsformedlingen/components/digi-icon-accessibility-universal.js';
import { defineCustomElement as defineDigiIconArchive } from '@digi/arbetsformedlingen/components/digi-icon-archive.js';
import { defineCustomElement as defineDigiIconArchiveOutline } from '@digi/arbetsformedlingen/components/digi-icon-archive-outline.js';
import { defineCustomElement as defineDigiIconArrowBack } from '@digi/arbetsformedlingen/components/digi-icon-arrow-back.js';
import { defineCustomElement as defineDigiIconArrowDown } from '@digi/arbetsformedlingen/components/digi-icon-arrow-down.js';
import { defineCustomElement as defineDigiIconArrowLeft } from '@digi/arbetsformedlingen/components/digi-icon-arrow-left.js';
import { defineCustomElement as defineDigiIconArrowRight } from '@digi/arbetsformedlingen/components/digi-icon-arrow-right.js';
import { defineCustomElement as defineDigiIconArrowSignIn } from '@digi/arbetsformedlingen/components/digi-icon-arrow-sign-in.js';
import { defineCustomElement as defineDigiIconArrowSignOut } from '@digi/arbetsformedlingen/components/digi-icon-arrow-sign-out.js';
import { defineCustomElement as defineDigiIconArrowUp } from '@digi/arbetsformedlingen/components/digi-icon-arrow-up.js';
import { defineCustomElement as defineDigiIconAt } from '@digi/arbetsformedlingen/components/digi-icon-at.js';
import { defineCustomElement as defineDigiIconBars } from '@digi/arbetsformedlingen/components/digi-icon-bars.js';
import { defineCustomElement as defineDigiIconBell } from '@digi/arbetsformedlingen/components/digi-icon-bell.js';
import { defineCustomElement as defineDigiIconBellFilled } from '@digi/arbetsformedlingen/components/digi-icon-bell-filled.js';
import { defineCustomElement as defineDigiIconBook } from '@digi/arbetsformedlingen/components/digi-icon-book.js';
import { defineCustomElement as defineDigiIconBubble } from '@digi/arbetsformedlingen/components/digi-icon-bubble.js';
import { defineCustomElement as defineDigiIconBubbleEllipsis } from '@digi/arbetsformedlingen/components/digi-icon-bubble-ellipsis.js';
import { defineCustomElement as defineDigiIconBuildingOutline } from '@digi/arbetsformedlingen/components/digi-icon-building-outline.js';
import { defineCustomElement as defineDigiIconBuildingSolid } from '@digi/arbetsformedlingen/components/digi-icon-building-solid.js';
import { defineCustomElement as defineDigiIconCalculatorSolid } from '@digi/arbetsformedlingen/components/digi-icon-calculator-solid.js';
import { defineCustomElement as defineDigiIconCalendar } from '@digi/arbetsformedlingen/components/digi-icon-calendar.js';
import { defineCustomElement as defineDigiIconCalendarAlt } from '@digi/arbetsformedlingen/components/digi-icon-calendar-alt.js';
import { defineCustomElement as defineDigiIconCalendarAltAlert } from '@digi/arbetsformedlingen/components/digi-icon-calendar-alt-alert.js';
import { defineCustomElement as defineDigiIconCaretCircleRight } from '@digi/arbetsformedlingen/components/digi-icon-caret-circle-right.js';
import { defineCustomElement as defineDigiIconCaretDown } from '@digi/arbetsformedlingen/components/digi-icon-caret-down.js';
import { defineCustomElement as defineDigiIconCaretLeft } from '@digi/arbetsformedlingen/components/digi-icon-caret-left.js';
import { defineCustomElement as defineDigiIconCaretRight } from '@digi/arbetsformedlingen/components/digi-icon-caret-right.js';
import { defineCustomElement as defineDigiIconCaretUp } from '@digi/arbetsformedlingen/components/digi-icon-caret-up.js';
import { defineCustomElement as defineDigiIconChart } from '@digi/arbetsformedlingen/components/digi-icon-chart.js';
import { defineCustomElement as defineDigiIconChat } from '@digi/arbetsformedlingen/components/digi-icon-chat.js';
import { defineCustomElement as defineDigiIconCheck } from '@digi/arbetsformedlingen/components/digi-icon-check.js';
import { defineCustomElement as defineDigiIconCheckCircleRegAlt } from '@digi/arbetsformedlingen/components/digi-icon-check-circle-reg-alt.js';
import { defineCustomElement as defineDigiIconChevronDown } from '@digi/arbetsformedlingen/components/digi-icon-chevron-down.js';
import { defineCustomElement as defineDigiIconChevronLeft } from '@digi/arbetsformedlingen/components/digi-icon-chevron-left.js';
import { defineCustomElement as defineDigiIconChevronRight } from '@digi/arbetsformedlingen/components/digi-icon-chevron-right.js';
import { defineCustomElement as defineDigiIconChevronUp } from '@digi/arbetsformedlingen/components/digi-icon-chevron-up.js';
import { defineCustomElement as defineDigiIconClock } from '@digi/arbetsformedlingen/components/digi-icon-clock.js';
import { defineCustomElement as defineDigiIconCompressAlt } from '@digi/arbetsformedlingen/components/digi-icon-compress-alt.js';
import { defineCustomElement as defineDigiIconComunicationCase } from '@digi/arbetsformedlingen/components/digi-icon-comunication-case.js';
import { defineCustomElement as defineDigiIconComunicationFlag } from '@digi/arbetsformedlingen/components/digi-icon-comunication-flag.js';
import { defineCustomElement as defineDigiIconComunicationGraduate } from '@digi/arbetsformedlingen/components/digi-icon-comunication-graduate.js';
import { defineCustomElement as defineDigiIconComunicationPlay } from '@digi/arbetsformedlingen/components/digi-icon-comunication-play.js';
import { defineCustomElement as defineDigiIconCooperation } from '@digi/arbetsformedlingen/components/digi-icon-cooperation.js';
import { defineCustomElement as defineDigiIconCopy } from '@digi/arbetsformedlingen/components/digi-icon-copy.js';
import { defineCustomElement as defineDigiIconDangerOutline } from '@digi/arbetsformedlingen/components/digi-icon-danger-outline.js';
import { defineCustomElement as defineDigiIconDownload } from '@digi/arbetsformedlingen/components/digi-icon-download.js';
import { defineCustomElement as defineDigiIconDxa } from '@digi/arbetsformedlingen/components/digi-icon-dxa.js';
import { defineCustomElement as defineDigiIconEdit } from '@digi/arbetsformedlingen/components/digi-icon-edit.js';
import { defineCustomElement as defineDigiIconEllipsis } from '@digi/arbetsformedlingen/components/digi-icon-ellipsis.js';
import { defineCustomElement as defineDigiIconEnvelope } from '@digi/arbetsformedlingen/components/digi-icon-envelope.js';
import { defineCustomElement as defineDigiIconEnvelopeFilled } from '@digi/arbetsformedlingen/components/digi-icon-envelope-filled.js';
import { defineCustomElement as defineDigiIconExclamationCircleFilled } from '@digi/arbetsformedlingen/components/digi-icon-exclamation-circle-filled.js';
import { defineCustomElement as defineDigiIconExclamationTriangle } from '@digi/arbetsformedlingen/components/digi-icon-exclamation-triangle.js';
import { defineCustomElement as defineDigiIconExclamationTriangleFilled } from '@digi/arbetsformedlingen/components/digi-icon-exclamation-triangle-filled.js';
import { defineCustomElement as defineDigiIconExclamationTriangleWarning } from '@digi/arbetsformedlingen/components/digi-icon-exclamation-triangle-warning.js';
import { defineCustomElement as defineDigiIconExpandAlt } from '@digi/arbetsformedlingen/components/digi-icon-expand-alt.js';
import { defineCustomElement as defineDigiIconExternalLinkAlt } from '@digi/arbetsformedlingen/components/digi-icon-external-link-alt.js';
import { defineCustomElement as defineDigiIconEye } from '@digi/arbetsformedlingen/components/digi-icon-eye.js';
import { defineCustomElement as defineDigiIconEyeSlash } from '@digi/arbetsformedlingen/components/digi-icon-eye-slash.js';
import { defineCustomElement as defineDigiIconFacebookSquare } from '@digi/arbetsformedlingen/components/digi-icon-facebook-square.js';
import { defineCustomElement as defineDigiIconFileDocument } from '@digi/arbetsformedlingen/components/digi-icon-file-document.js';
import { defineCustomElement as defineDigiIconFileExcel } from '@digi/arbetsformedlingen/components/digi-icon-file-excel.js';
import { defineCustomElement as defineDigiIconFilePdf } from '@digi/arbetsformedlingen/components/digi-icon-file-pdf.js';
import { defineCustomElement as defineDigiIconFilePowerpoint } from '@digi/arbetsformedlingen/components/digi-icon-file-powerpoint.js';
import { defineCustomElement as defineDigiIconFileWord } from '@digi/arbetsformedlingen/components/digi-icon-file-word.js';
import { defineCustomElement as defineDigiIconFilm } from '@digi/arbetsformedlingen/components/digi-icon-film.js';
import { defineCustomElement as defineDigiIconFilter } from '@digi/arbetsformedlingen/components/digi-icon-filter.js';
import { defineCustomElement as defineDigiIconGlobe } from '@digi/arbetsformedlingen/components/digi-icon-globe.js';
import { defineCustomElement as defineDigiIconGlobeFilled } from '@digi/arbetsformedlingen/components/digi-icon-globe-filled.js';
import { defineCustomElement as defineDigiIconHeadphones } from '@digi/arbetsformedlingen/components/digi-icon-headphones.js';
import { defineCustomElement as defineDigiIconHeart } from '@digi/arbetsformedlingen/components/digi-icon-heart.js';
import { defineCustomElement as defineDigiIconHeartSolid } from '@digi/arbetsformedlingen/components/digi-icon-heart-solid.js';
import { defineCustomElement as defineDigiIconHistory } from '@digi/arbetsformedlingen/components/digi-icon-history.js';
import { defineCustomElement as defineDigiIconHome } from '@digi/arbetsformedlingen/components/digi-icon-home.js';
import { defineCustomElement as defineDigiIconImage } from '@digi/arbetsformedlingen/components/digi-icon-image.js';
import { defineCustomElement as defineDigiIconInfoCircleSolid } from '@digi/arbetsformedlingen/components/digi-icon-info-circle-solid.js';
import { defineCustomElement as defineDigiIconInstagram } from '@digi/arbetsformedlingen/components/digi-icon-instagram.js';
import { defineCustomElement as defineDigiIconJobSuggestion } from '@digi/arbetsformedlingen/components/digi-icon-job-suggestion.js';
import { defineCustomElement as defineDigiIconLanguage } from '@digi/arbetsformedlingen/components/digi-icon-language.js';
import { defineCustomElement as defineDigiIconLanguageOutline } from '@digi/arbetsformedlingen/components/digi-icon-language-outline.js';
import { defineCustomElement as defineDigiIconLattlast } from '@digi/arbetsformedlingen/components/digi-icon-lattlast.js';
import { defineCustomElement as defineDigiIconLicenceBus } from '@digi/arbetsformedlingen/components/digi-icon-licence-bus.js';
import { defineCustomElement as defineDigiIconLicenceCar } from '@digi/arbetsformedlingen/components/digi-icon-licence-car.js';
import { defineCustomElement as defineDigiIconLicenceMotorcycle } from '@digi/arbetsformedlingen/components/digi-icon-licence-motorcycle.js';
import { defineCustomElement as defineDigiIconLicenceTruck } from '@digi/arbetsformedlingen/components/digi-icon-licence-truck.js';
import { defineCustomElement as defineDigiIconLightbulb } from '@digi/arbetsformedlingen/components/digi-icon-lightbulb.js';
import { defineCustomElement as defineDigiIconLinkedinIn } from '@digi/arbetsformedlingen/components/digi-icon-linkedin-in.js';
import { defineCustomElement as defineDigiIconListUl } from '@digi/arbetsformedlingen/components/digi-icon-list-ul.js';
import { defineCustomElement as defineDigiIconLock } from '@digi/arbetsformedlingen/components/digi-icon-lock.js';
import { defineCustomElement as defineDigiIconMarker } from '@digi/arbetsformedlingen/components/digi-icon-marker.js';
import { defineCustomElement as defineDigiIconMarkerFilled } from '@digi/arbetsformedlingen/components/digi-icon-marker-filled.js';
import { defineCustomElement as defineDigiIconMediaCourse } from '@digi/arbetsformedlingen/components/digi-icon-media-course.js';
import { defineCustomElement as defineDigiIconMediaPodcast } from '@digi/arbetsformedlingen/components/digi-icon-media-podcast.js';
import { defineCustomElement as defineDigiIconMicrophone } from '@digi/arbetsformedlingen/components/digi-icon-microphone.js';
import { defineCustomElement as defineDigiIconMicrophoneOff } from '@digi/arbetsformedlingen/components/digi-icon-microphone-off.js';
import { defineCustomElement as defineDigiIconMinus } from '@digi/arbetsformedlingen/components/digi-icon-minus.js';
import { defineCustomElement as defineDigiIconNews } from '@digi/arbetsformedlingen/components/digi-icon-news.js';
import { defineCustomElement as defineDigiIconNotificationError } from '@digi/arbetsformedlingen/components/digi-icon-notification-error.js';
import { defineCustomElement as defineDigiIconNotificationInfo } from '@digi/arbetsformedlingen/components/digi-icon-notification-info.js';
import { defineCustomElement as defineDigiIconNotificationSucces } from '@digi/arbetsformedlingen/components/digi-icon-notification-succes.js';
import { defineCustomElement as defineDigiIconNotificationWarning } from '@digi/arbetsformedlingen/components/digi-icon-notification-warning.js';
import { defineCustomElement as defineDigiIconOpenSource } from '@digi/arbetsformedlingen/components/digi-icon-open-source.js';
import { defineCustomElement as defineDigiIconPaperclip } from '@digi/arbetsformedlingen/components/digi-icon-paperclip.js';
import { defineCustomElement as defineDigiIconPen } from '@digi/arbetsformedlingen/components/digi-icon-pen.js';
import { defineCustomElement as defineDigiIconPhone } from '@digi/arbetsformedlingen/components/digi-icon-phone.js';
import { defineCustomElement as defineDigiIconPhoneHangup } from '@digi/arbetsformedlingen/components/digi-icon-phone-hangup.js';
import { defineCustomElement as defineDigiIconPlus } from '@digi/arbetsformedlingen/components/digi-icon-plus.js';
import { defineCustomElement as defineDigiIconPrint } from '@digi/arbetsformedlingen/components/digi-icon-print.js';
import { defineCustomElement as defineDigiIconRedo } from '@digi/arbetsformedlingen/components/digi-icon-redo.js';
import { defineCustomElement as defineDigiIconScreensharing } from '@digi/arbetsformedlingen/components/digi-icon-screensharing.js';
import { defineCustomElement as defineDigiIconScreensharingOff } from '@digi/arbetsformedlingen/components/digi-icon-screensharing-off.js';
import { defineCustomElement as defineDigiIconSearch } from '@digi/arbetsformedlingen/components/digi-icon-search.js';
import { defineCustomElement as defineDigiIconSettings } from '@digi/arbetsformedlingen/components/digi-icon-settings.js';
import { defineCustomElement as defineDigiIconShareAlt } from '@digi/arbetsformedlingen/components/digi-icon-share-alt.js';
import { defineCustomElement as defineDigiIconSign } from '@digi/arbetsformedlingen/components/digi-icon-sign.js';
import { defineCustomElement as defineDigiIconSlidersH } from '@digi/arbetsformedlingen/components/digi-icon-sliders-h.js';
import { defineCustomElement as defineDigiIconSokkandidat } from '@digi/arbetsformedlingen/components/digi-icon-sokkandidat.js';
import { defineCustomElement as defineDigiIconSort } from '@digi/arbetsformedlingen/components/digi-icon-sort.js';
import { defineCustomElement as defineDigiIconSortDown } from '@digi/arbetsformedlingen/components/digi-icon-sort-down.js';
import { defineCustomElement as defineDigiIconSortUp } from '@digi/arbetsformedlingen/components/digi-icon-sort-up.js';
import { defineCustomElement as defineDigiIconSpinner } from '@digi/arbetsformedlingen/components/digi-icon-spinner.js';
import { defineCustomElement as defineDigiIconStar } from '@digi/arbetsformedlingen/components/digi-icon-star.js';
import { defineCustomElement as defineDigiIconStarReg } from '@digi/arbetsformedlingen/components/digi-icon-star-reg.js';
import { defineCustomElement as defineDigiIconSuitcaseOutline } from '@digi/arbetsformedlingen/components/digi-icon-suitcase-outline.js';
import { defineCustomElement as defineDigiIconSuitcaseSolid } from '@digi/arbetsformedlingen/components/digi-icon-suitcase-solid.js';
import { defineCustomElement as defineDigiIconTable } from '@digi/arbetsformedlingen/components/digi-icon-table.js';
import { defineCustomElement as defineDigiIconToggleOff } from '@digi/arbetsformedlingen/components/digi-icon-toggle-off.js';
import { defineCustomElement as defineDigiIconToggleOn } from '@digi/arbetsformedlingen/components/digi-icon-toggle-on.js';
import { defineCustomElement as defineDigiIconTrash } from '@digi/arbetsformedlingen/components/digi-icon-trash.js';
import { defineCustomElement as defineDigiIconTwitter } from '@digi/arbetsformedlingen/components/digi-icon-twitter.js';
import { defineCustomElement as defineDigiIconTyckTill } from '@digi/arbetsformedlingen/components/digi-icon-tyck-till.js';
import { defineCustomElement as defineDigiIconUnlock } from '@digi/arbetsformedlingen/components/digi-icon-unlock.js';
import { defineCustomElement as defineDigiIconUpdate } from '@digi/arbetsformedlingen/components/digi-icon-update.js';
import { defineCustomElement as defineDigiIconUpload } from '@digi/arbetsformedlingen/components/digi-icon-upload.js';
import { defineCustomElement as defineDigiIconUserAlt } from '@digi/arbetsformedlingen/components/digi-icon-user-alt.js';
import { defineCustomElement as defineDigiIconUsersSolid } from '@digi/arbetsformedlingen/components/digi-icon-users-solid.js';
import { defineCustomElement as defineDigiIconValidationError } from '@digi/arbetsformedlingen/components/digi-icon-validation-error.js';
import { defineCustomElement as defineDigiIconValidationSuccess } from '@digi/arbetsformedlingen/components/digi-icon-validation-success.js';
import { defineCustomElement as defineDigiIconValidationWarning } from '@digi/arbetsformedlingen/components/digi-icon-validation-warning.js';
import { defineCustomElement as defineDigiIconVideocamera } from '@digi/arbetsformedlingen/components/digi-icon-videocamera.js';
import { defineCustomElement as defineDigiIconVideocameraOff } from '@digi/arbetsformedlingen/components/digi-icon-videocamera-off.js';
import { defineCustomElement as defineDigiIconVolume } from '@digi/arbetsformedlingen/components/digi-icon-volume.js';
import { defineCustomElement as defineDigiIconWebTv } from '@digi/arbetsformedlingen/components/digi-icon-web-tv.js';
import { defineCustomElement as defineDigiIconWebinar } from '@digi/arbetsformedlingen/components/digi-icon-webinar.js';
import { defineCustomElement as defineDigiIconX } from '@digi/arbetsformedlingen/components/digi-icon-x.js';
import { defineCustomElement as defineDigiIconXButton } from '@digi/arbetsformedlingen/components/digi-icon-x-button.js';
import { defineCustomElement as defineDigiIconXButtonOutline } from '@digi/arbetsformedlingen/components/digi-icon-x-button-outline.js';
import { defineCustomElement as defineDigiIconYoutube } from '@digi/arbetsformedlingen/components/digi-icon-youtube.js';
import { defineCustomElement as defineDigiInfoCard } from '@digi/arbetsformedlingen/components/digi-info-card.js';
import { defineCustomElement as defineDigiInfoCardMulti } from '@digi/arbetsformedlingen/components/digi-info-card-multi.js';
import { defineCustomElement as defineDigiInfoCardMultiContainer } from '@digi/arbetsformedlingen/components/digi-info-card-multi-container.js';
import { defineCustomElement as defineDigiLayoutBlock } from '@digi/arbetsformedlingen/components/digi-layout-block.js';
import { defineCustomElement as defineDigiLayoutColumns } from '@digi/arbetsformedlingen/components/digi-layout-columns.js';
import { defineCustomElement as defineDigiLayoutContainer } from '@digi/arbetsformedlingen/components/digi-layout-container.js';
import { defineCustomElement as defineDigiLayoutMediaObject } from '@digi/arbetsformedlingen/components/digi-layout-media-object.js';
import { defineCustomElement as defineDigiLink } from '@digi/arbetsformedlingen/components/digi-link.js';
import { defineCustomElement as defineDigiLinkButton } from '@digi/arbetsformedlingen/components/digi-link-button.js';
import { defineCustomElement as defineDigiLinkExternal } from '@digi/arbetsformedlingen/components/digi-link-external.js';
import { defineCustomElement as defineDigiLinkInternal } from '@digi/arbetsformedlingen/components/digi-link-internal.js';
import { defineCustomElement as defineDigiList } from '@digi/arbetsformedlingen/components/digi-list.js';
import { defineCustomElement as defineDigiLoaderSpinner } from '@digi/arbetsformedlingen/components/digi-loader-spinner.js';
import { defineCustomElement as defineDigiLogo } from '@digi/arbetsformedlingen/components/digi-logo.js';
import { defineCustomElement as defineDigiMediaFigure } from '@digi/arbetsformedlingen/components/digi-media-figure.js';
import { defineCustomElement as defineDigiMediaImage } from '@digi/arbetsformedlingen/components/digi-media-image.js';
import { defineCustomElement as defineDigiNavigationBreadcrumbs } from '@digi/arbetsformedlingen/components/digi-navigation-breadcrumbs.js';
import { defineCustomElement as defineDigiNavigationContextMenu } from '@digi/arbetsformedlingen/components/digi-navigation-context-menu.js';
import { defineCustomElement as defineDigiNavigationContextMenuItem } from '@digi/arbetsformedlingen/components/digi-navigation-context-menu-item.js';
import { defineCustomElement as defineDigiNavigationPagination } from '@digi/arbetsformedlingen/components/digi-navigation-pagination.js';
import { defineCustomElement as defineDigiNavigationSidebar } from '@digi/arbetsformedlingen/components/digi-navigation-sidebar.js';
import { defineCustomElement as defineDigiNavigationSidebarButton } from '@digi/arbetsformedlingen/components/digi-navigation-sidebar-button.js';
import { defineCustomElement as defineDigiNavigationTab } from '@digi/arbetsformedlingen/components/digi-navigation-tab.js';
import { defineCustomElement as defineDigiNavigationTabs } from '@digi/arbetsformedlingen/components/digi-navigation-tabs.js';
import { defineCustomElement as defineDigiNavigationVerticalMenu } from '@digi/arbetsformedlingen/components/digi-navigation-vertical-menu.js';
import { defineCustomElement as defineDigiNavigationVerticalMenuItem } from '@digi/arbetsformedlingen/components/digi-navigation-vertical-menu-item.js';
import { defineCustomElement as defineDigiNotificationAlert } from '@digi/arbetsformedlingen/components/digi-notification-alert.js';
import { defineCustomElement as defineDigiNotificationErrorPage } from '@digi/arbetsformedlingen/components/digi-notification-error-page.js';
import { defineCustomElement as defineDigiProgressStep } from '@digi/arbetsformedlingen/components/digi-progress-step.js';
import { defineCustomElement as defineDigiProgressSteps } from '@digi/arbetsformedlingen/components/digi-progress-steps.js';
import { defineCustomElement as defineDigiProgressbar } from '@digi/arbetsformedlingen/components/digi-progressbar.js';
import { defineCustomElement as defineDigiQuoteMultiContainer } from '@digi/arbetsformedlingen/components/digi-quote-multi-container.js';
import { defineCustomElement as defineDigiQuoteSingle } from '@digi/arbetsformedlingen/components/digi-quote-single.js';
import { defineCustomElement as defineDigiTable } from '@digi/arbetsformedlingen/components/digi-table.js';
import { defineCustomElement as defineDigiTag } from '@digi/arbetsformedlingen/components/digi-tag.js';
import { defineCustomElement as defineDigiTagMedia } from '@digi/arbetsformedlingen/components/digi-tag-media.js';
import { defineCustomElement as defineDigiToolsFeedback } from '@digi/arbetsformedlingen/components/digi-tools-feedback.js';
import { defineCustomElement as defineDigiToolsFeedbackBanner } from '@digi/arbetsformedlingen/components/digi-tools-feedback-banner.js';
import { defineCustomElement as defineDigiToolsLanguagepicker } from '@digi/arbetsformedlingen/components/digi-tools-languagepicker.js';
import { defineCustomElement as defineDigiTypography } from '@digi/arbetsformedlingen/components/digi-typography.js';
import { defineCustomElement as defineDigiTypographyHeadingJumbo } from '@digi/arbetsformedlingen/components/digi-typography-heading-jumbo.js';
import { defineCustomElement as defineDigiTypographyMeta } from '@digi/arbetsformedlingen/components/digi-typography-meta.js';
import { defineCustomElement as defineDigiTypographyPreamble } from '@digi/arbetsformedlingen/components/digi-typography-preamble.js';
import { defineCustomElement as defineDigiTypographyTime } from '@digi/arbetsformedlingen/components/digi-typography-time.js';
import { defineCustomElement as defineDigiUtilBreakpointObserver } from '@digi/arbetsformedlingen/components/digi-util-breakpoint-observer.js';
import { defineCustomElement as defineDigiUtilDetectClickOutside } from '@digi/arbetsformedlingen/components/digi-util-detect-click-outside.js';
import { defineCustomElement as defineDigiUtilDetectFocusOutside } from '@digi/arbetsformedlingen/components/digi-util-detect-focus-outside.js';
import { defineCustomElement as defineDigiUtilIntersectionObserver } from '@digi/arbetsformedlingen/components/digi-util-intersection-observer.js';
import { defineCustomElement as defineDigiUtilKeydownHandler } from '@digi/arbetsformedlingen/components/digi-util-keydown-handler.js';
import { defineCustomElement as defineDigiUtilKeyupHandler } from '@digi/arbetsformedlingen/components/digi-util-keyup-handler.js';
import { defineCustomElement as defineDigiUtilMutationObserver } from '@digi/arbetsformedlingen/components/digi-util-mutation-observer.js';
import { defineCustomElement as defineDigiUtilResizeObserver } from '@digi/arbetsformedlingen/components/digi-util-resize-observer.js';


export declare interface DigiBarChart extends Components.DigiBarChart {}

@ProxyCmp({
  defineCustomElementFn: defineDigiBarChart,
  inputs: ['afChartData', 'afHeadingLevel', 'afId', 'afVariation']
})
@Component({
  selector: 'digi-bar-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afChartData', 'afHeadingLevel', 'afId', 'afVariation']
})
export class DigiBarChart {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiButton extends Components.DigiButton {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Buttonelementets 'onfocus'-event. @en The button element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * Buttonelementets 'onblur'-event. @en The button element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiButton,
  inputs: ['afAriaChecked', 'afAriaControls', 'afAriaCurrent', 'afAriaExpanded', 'afAriaHaspopup', 'afAriaLabel', 'afAriaLabelledby', 'afAriaPressed', 'afAutofocus', 'afDir', 'afForm', 'afFullWidth', 'afId', 'afLang', 'afRole', 'afSize', 'afTabindex', 'afType', 'afVariation'],
  methods: ['afMGetButtonElement']
})
@Component({
  selector: 'digi-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaChecked', 'afAriaControls', 'afAriaCurrent', 'afAriaExpanded', 'afAriaHaspopup', 'afAriaLabel', 'afAriaLabelledby', 'afAriaPressed', 'afAutofocus', 'afDir', 'afForm', 'afFullWidth', 'afId', 'afLang', 'afRole', 'afSize', 'afTabindex', 'afType', 'afVariation']
})
export class DigiButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick', 'afOnFocus', 'afOnBlur', 'afOnReady']);
  }
}


export declare interface DigiCalendar extends Components.DigiCalendar {
  /**
   * Sker när ett datum har valts eller avvalts. Returnerar datumen i en array. @en When a date is selected. Return the dates in an array.
   */
  afOnDateSelectedChange: EventEmitter<CustomEvent<any>>;
  /**
   * Sker när fokus flyttas utanför kalendern @en When focus moves out from the calendar
   */
  afOnFocusOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid klick utanför kalendern @en When click outside the calendar
   */
  afOnClickOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Sker när kalenderdatumen har rörts första gången, när värdet på focusedDate har ändrats första gången. @en When the calendar dates are touched the first time, when focusedDate is changed the first time.
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiCalendar,
  inputs: ['afActive', 'afId', 'afInitSelectedDate', 'afInitSelectedMonth', 'afMaxDate', 'afMinDate', 'afMultipleDates', 'afSelectedDate', 'afSelectedDates', 'afShowWeekNumber']
})
@Component({
  selector: 'digi-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afId', 'afInitSelectedDate', 'afInitSelectedMonth', 'afMaxDate', 'afMinDate', 'afMultipleDates', 'afSelectedDate', 'afSelectedDates', 'afShowWeekNumber']
})
export class DigiCalendar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnDateSelectedChange', 'afOnFocusOutside', 'afOnClickOutside', 'afOnDirty']);
  }
}


export declare interface DigiCalendarDatepicker extends Components.DigiCalendarDatepicker {
  /**
   * Sker vid datum uppdatering @en When date is updated
   */
  afOnDateChange: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiCalendarDatepicker,
  inputs: ['afCloseOnSelect', 'afInvalid', 'afLabel', 'afLabelDescription', 'afMaxDate', 'afMinDate', 'afMultipleDates', 'afSelectedDates', 'afShowWeekNumber', 'afValidationDisabledDate', 'afValidationMessage', 'afValidationWrongFormat']
})
@Component({
  selector: 'digi-calendar-datepicker',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCloseOnSelect', 'afInvalid', 'afLabel', 'afLabelDescription', 'afMaxDate', 'afMinDate', 'afMultipleDates', 'afSelectedDates', 'afShowWeekNumber', 'afValidationDisabledDate', 'afValidationMessage', 'afValidationWrongFormat']
})
export class DigiCalendarDatepicker {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnDateChange']);
  }
}


export declare interface DigiCalendarWeekView extends Components.DigiCalendarWeekView {
  /**
   * Vid byte av vecka @en When week changes
   */
  afOnWeekChange: EventEmitter<CustomEvent<string>>;
  /**
   * Vid byte av dag @en When day changes
   */
  afOnDateChange: EventEmitter<CustomEvent<string>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiCalendarWeekView,
  inputs: ['afDates', 'afHeadingLevel', 'afId', 'afMaxWeek', 'afMinWeek'],
  methods: ['afMSetActiveDate']
})
@Component({
  selector: 'digi-calendar-week-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDates', 'afHeadingLevel', 'afId', 'afMaxWeek', 'afMinWeek']
})
export class DigiCalendarWeekView {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnWeekChange', 'afOnDateChange', 'afOnReady']);
  }
}


export declare interface DigiCard extends Components.DigiCard {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCard,
  inputs: ['afBorder', 'afBorderRadius', 'afFooterBorder']
})
@Component({
  selector: 'digi-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afBorder', 'afBorderRadius', 'afFooterBorder']
})
export class DigiCard {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiChartLine extends Components.DigiChartLine {}

@ProxyCmp({
  defineCustomElementFn: defineDigiChartLine,
  inputs: ['afChartData', 'afHeadingLevel', 'afId']
})
@Component({
  selector: 'digi-chart-line',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afChartData', 'afHeadingLevel', 'afId']
})
export class DigiChartLine {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCode extends Components.DigiCode {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCode,
  inputs: ['afCode', 'afLang', 'afLanguage', 'afVariation']
})
@Component({
  selector: 'digi-code',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCode', 'afLang', 'afLanguage', 'afVariation']
})
export class DigiCode {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCodeBlock extends Components.DigiCodeBlock {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCodeBlock,
  inputs: ['afCode', 'afHideToolbar', 'afLang', 'afLanguage', 'afVariation']
})
@Component({
  selector: 'digi-code-block',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCode', 'afHideToolbar', 'afLang', 'afLanguage', 'afVariation']
})
export class DigiCodeBlock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCodeExample extends Components.DigiCodeExample {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCodeExample,
  inputs: ['afCode', 'afCodeBlockVariation', 'afExampleVariation', 'afHideCode', 'afHideControls', 'afLanguage']
})
@Component({
  selector: 'digi-code-example',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCode', 'afCodeBlockVariation', 'afExampleVariation', 'afHideCode', 'afHideControls', 'afLanguage']
})
export class DigiCodeExample {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiDialog extends Components.DigiDialog {
  /**
   * Primära knappens 'onclick'-event @en The primary buttons 'onclick' event
   */
  afPrimaryButtonClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Sekundära knappens 'onclick'-event @en The secondary buttons 'onclick' event
   */
  afSecondaryButtonClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Event när modalen stängs @en Event fired when modal is closed
   */
  afOnClose: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiDialog,
  inputs: ['afCloseButtonText', 'afFallbackElementSelector', 'afHeading', 'afHeadingLevel', 'afId', 'afPrimaryButtonText', 'afSecondaryButtonText', 'afShowDialog', 'afSize', 'afVariation']
})
@Component({
  selector: 'digi-dialog',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCloseButtonText', 'afFallbackElementSelector', 'afHeading', 'afHeadingLevel', 'afId', 'afPrimaryButtonText', 'afSecondaryButtonText', 'afShowDialog', 'afSize', 'afVariation']
})
export class DigiDialog {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afPrimaryButtonClick', 'afSecondaryButtonClick', 'afOnClose']);
  }
}


export declare interface DigiExpandableAccordion extends Components.DigiExpandableAccordion {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiExpandableAccordion,
  inputs: ['afAnimation', 'afExpanded', 'afHeading', 'afHeadingLevel', 'afId', 'afVariation']
})
@Component({
  selector: 'digi-expandable-accordion',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnimation', 'afExpanded', 'afHeading', 'afHeadingLevel', 'afId', 'afVariation']
})
export class DigiExpandableAccordion {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiExpandableFaq extends Components.DigiExpandableFaq {}

@ProxyCmp({
  defineCustomElementFn: defineDigiExpandableFaq,
  inputs: ['afHeading', 'afHeadingLevel', 'afVariation']
})
@Component({
  selector: 'digi-expandable-faq',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHeading', 'afHeadingLevel', 'afVariation']
})
export class DigiExpandableFaq {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiExpandableFaqItem extends Components.DigiExpandableFaqItem {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiExpandableFaqItem,
  inputs: ['afAnimation', 'afExpanded', 'afHeading', 'afHeadingLevel', 'afId', 'afVariation']
})
@Component({
  selector: 'digi-expandable-faq-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnimation', 'afExpanded', 'afHeading', 'afHeadingLevel', 'afId', 'afVariation']
})
export class DigiExpandableFaqItem {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiFormCategoryFilter extends Components.DigiFormCategoryFilter {
  /**
   * Sker vid ändrade valda kategorier @en When selected categories is changed
   */
  afOnSelectedCategoryChange: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormCategoryFilter,
  inputs: ['afAllCategories', 'afAllCategoriesSelected', 'afAllCategoriesText', 'afCategories', 'afMultiselect', 'afStartCollapsed', 'afVisibleCollapsed']
})
@Component({
  selector: 'digi-form-category-filter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAllCategories', 'afAllCategoriesSelected', 'afAllCategoriesText', 'afCategories', 'afMultiselect', 'afStartCollapsed', 'afVisibleCollapsed']
})
export class DigiFormCategoryFilter {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnSelectedCategoryChange']);
  }
}


export declare interface DigiFormCheckbox extends Components.DigiFormCheckbox {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormCheckbox,
  inputs: ['afAriaDescribedby', 'afAriaLabel', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afDescription', 'afId', 'afIndeterminate', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-checkbox',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaDescribedby', 'afAriaLabel', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afDescription', 'afId', 'afIndeterminate', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value']
})
export class DigiFormCheckbox {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormErrorList extends Components.DigiFormErrorList {
  /**
   * Länkelementens 'onclick'-event. @en The link elements' 'onclick' event.
   */
  afOnClickLink: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormErrorList,
  inputs: ['afDescription', 'afEnableHeadingFocus', 'afHeading', 'afHeadingLevel', 'afId', 'afOverrideLink'],
  methods: ['afMSetHeadingFocus']
})
@Component({
  selector: 'digi-form-error-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDescription', 'afEnableHeadingFocus', 'afHeading', 'afHeadingLevel', 'afId', 'afOverrideLink']
})
export class DigiFormErrorList {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClickLink', 'afOnReady']);
  }
}


export declare interface DigiFormFieldset extends Components.DigiFormFieldset {}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormFieldset,
  inputs: ['afForm', 'afId', 'afLegend', 'afName']
})
@Component({
  selector: 'digi-form-fieldset',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afForm', 'afId', 'afLegend', 'afName']
})
export class DigiFormFieldset {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormFileUpload extends Components.DigiFormFileUpload {
  /**
   * Sänder ut fil vid uppladdning @en Emits file on upload
   */
  afOnUploadFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som tagits bort @en Emits which file that was deleted.
   */
  afOnRemoveFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som har avbrutis uppladdning @en Emits which file that was canceled
   */
  afOnCancelFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som försöker laddas upp igen @en Emits which file is trying to retry its upload
   */
  afOnRetryFile: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormFileUpload,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afFileMaxSize', 'afFileTypes', 'afHeadingFiles', 'afHeadingLevel', 'afId', 'afLabel', 'afLabelDescription', 'afMaxFiles', 'afName', 'afRequired', 'afRequiredText', 'afUploadBtnText', 'afValidation', 'afVariation'],
  methods: ['afMValidateFile', 'afMGetAllFiles', 'afMImportFiles', 'afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-file-upload',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afFileMaxSize', 'afFileTypes', 'afHeadingFiles', 'afHeadingLevel', 'afId', 'afLabel', 'afLabelDescription', 'afMaxFiles', 'afName', 'afRequired', 'afRequiredText', 'afUploadBtnText', 'afValidation', 'afVariation']
})
export class DigiFormFileUpload {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnUploadFile', 'afOnRemoveFile', 'afOnCancelFile', 'afOnRetryFile', 'afOnReady']);
  }
}


export declare interface DigiFormFilter extends Components.DigiFormFilter {
  /**
   *  
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Vid uppdatering av filtret @en At filter submit
   */
  afOnSubmitFilters: EventEmitter<CustomEvent<any>>;
  /**
   * När filtret stängs utan att valda alternativ bekräftats @en When the filter is closed without confirming the selected options
   */
  afOnFilterClosed: EventEmitter<CustomEvent<any>>;
  /**
   * Vid reset av filtret @en At filter reset
   */
  afOnResetFilters: EventEmitter<CustomEvent<any>>;
  /**
   * När ett filter ändras @en When a filter is changed
   */
  afOnChange: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormFilter,
  inputs: ['afAlignRight', 'afAutofocus', 'afFilterButtonAriaDescribedby', 'afFilterButtonAriaLabel', 'afFilterButtonText', 'afHideResetButton', 'afId', 'afName', 'afResetButtonText', 'afResetButtonTextAriaLabel', 'afSubmitButtonText', 'afSubmitButtonTextAriaLabel']
})
@Component({
  selector: 'digi-form-filter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlignRight', 'afAutofocus', 'afFilterButtonAriaDescribedby', 'afFilterButtonAriaLabel', 'afFilterButtonText', 'afHideResetButton', 'afId', 'afName', 'afResetButtonText', 'afResetButtonTextAriaLabel', 'afSubmitButtonText', 'afSubmitButtonTextAriaLabel']
})
export class DigiFormFilter {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusout', 'afOnSubmitFilters', 'afOnFilterClosed', 'afOnResetFilters', 'afOnChange']);
  }
}


export declare interface DigiFormInput extends Components.DigiFormInput {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onkeyup'-event. @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormInput,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonVariation', 'afDirname', 'afId', 'afInputmode', 'afLabel', 'afLabelDescription', 'afList', 'afMax', 'afMaxlength', 'afMin', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afStep', 'afType', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-input',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonVariation', 'afDirname', 'afId', 'afInputmode', 'afLabel', 'afLabelDescription', 'afList', 'afMax', 'afMaxlength', 'afMin', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afStep', 'afType', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value']
})
export class DigiFormInput {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormInputSearch extends Components.DigiFormInputSearch {
  /**
   * Vid fokus utanför komponenten. @en When focus is outside component.
   */
  afOnFocusOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid fokus inuti komponenten. @en When focus is inside component.
   */
  afOnFocusInside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid klick utanför komponenten. @en When click outside component.
   */
  afOnClickOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid klick inuti komponenten. @en When click inside component.
   */
  afOnClickInside: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onkeyup'-event. @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Knappelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormInputSearch,
  inputs: ['afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonAriaLabel', 'afButtonAriaLabelledby', 'afButtonText', 'afButtonType', 'afButtonVariation', 'afHideButton', 'afId', 'afLabel', 'afLabelDescription', 'afName', 'afType', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-input-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonAriaLabel', 'afButtonAriaLabelledby', 'afButtonText', 'afButtonType', 'afButtonVariation', 'afHideButton', 'afId', 'afLabel', 'afLabelDescription', 'afName', 'afType', 'afValue', 'afVariation', 'value']
})
export class DigiFormInputSearch {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusOutside', 'afOnFocusInside', 'afOnClickOutside', 'afOnClickInside', 'afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnClick', 'afOnReady']);
  }
}


export declare interface DigiFormLabel extends Components.DigiFormLabel {}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormLabel,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afDescription', 'afFor', 'afId', 'afLabel', 'afRequired', 'afRequiredText']
})
@Component({
  selector: 'digi-form-label',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afDescription', 'afFor', 'afId', 'afLabel', 'afRequired', 'afRequiredText']
})
export class DigiFormLabel {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormRadiobutton extends Components.DigiFormRadiobutton {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormRadiobutton,
  inputs: ['afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afId', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-radiobutton',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afId', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value']
})
export class DigiFormRadiobutton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormRadiogroup extends Components.DigiFormRadiogroup {
  /**
   * Event när värdet ändras @en Event when value changes
   */
  afOnGroupChange: EventEmitter<CustomEvent<string>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormRadiogroup,
  inputs: ['afName', 'afValue', 'value']
})
@Component({
  selector: 'digi-form-radiogroup',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afName', 'afValue', 'value']
})
export class DigiFormRadiogroup {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnGroupChange']);
  }
}


export declare interface DigiFormReceipt extends Components.DigiFormReceipt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormReceipt,
  inputs: ['afHeadingLevel', 'afText', 'afType', 'afVariation']
})
@Component({
  selector: 'digi-form-receipt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHeadingLevel', 'afText', 'afType', 'afVariation']
})
export class DigiFormReceipt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormSelect extends Components.DigiFormSelect {
  /**
   *  
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnSelect: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Sker första gången väljarelementet ändras. @en First time the select element is changed.
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormSelect,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAutofocus', 'afDescription', 'afDisableValidation', 'afId', 'afLabel', 'afName', 'afPlaceholder', 'afRequired', 'afRequiredText', 'afStartSelected', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-select',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAutofocus', 'afDescription', 'afDisableValidation', 'afId', 'afLabel', 'afName', 'afPlaceholder', 'afRequired', 'afRequiredText', 'afStartSelected', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value']
})
export class DigiFormSelect {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnSelect', 'afOnFocus', 'afOnFocusout', 'afOnBlur', 'afOnDirty', 'afOnReady']);
  }
}


export declare interface DigiFormTextarea extends Components.DigiFormTextarea {
  /**
   * Textareatelementets 'onchange'-event. @en The textarea element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onblur'-event. @en The textarea element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onkeyup'-event. @en The textarea element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onfocus'-event. @en The textarea element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onfocusout'-event. @en The textarea element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'oninput'-event. @en The textarea element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid textareaelementets första 'oninput' @en First time the textarea element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid textareaelementets första 'onblur' @en First time the textelement is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormTextarea,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afId', 'afLabel', 'afLabelDescription', 'afMaxlength', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-textarea',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afId', 'afLabel', 'afLabelDescription', 'afMaxlength', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value']
})
export class DigiFormTextarea {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormValidationMessage extends Components.DigiFormValidationMessage {}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormValidationMessage,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-form-validation-message',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiFormValidationMessage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIcon extends Components.DigiIcon {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIcon,
  inputs: ['afDesc', 'afName', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afName', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIcon {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconAccessibilityDeaf extends Components.DigiIconAccessibilityDeaf {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconAccessibilityDeaf,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-accessibility-deaf',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconAccessibilityDeaf {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconAccessibilityUniversal extends Components.DigiIconAccessibilityUniversal {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconAccessibilityUniversal,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-accessibility-universal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconAccessibilityUniversal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArchive extends Components.DigiIconArchive {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArchive,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-archive',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArchive {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArchiveOutline extends Components.DigiIconArchiveOutline {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArchiveOutline,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-archive-outline',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArchiveOutline {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArrowBack extends Components.DigiIconArrowBack {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArrowBack,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-arrow-back',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArrowBack {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArrowDown extends Components.DigiIconArrowDown {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArrowDown,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-arrow-down',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArrowDown {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArrowLeft extends Components.DigiIconArrowLeft {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArrowLeft,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-arrow-left',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArrowLeft {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArrowRight extends Components.DigiIconArrowRight {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArrowRight,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-arrow-right',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArrowRight {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArrowSignIn extends Components.DigiIconArrowSignIn {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArrowSignIn,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-arrow-sign-in',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArrowSignIn {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArrowSignOut extends Components.DigiIconArrowSignOut {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArrowSignOut,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-arrow-sign-out',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArrowSignOut {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconArrowUp extends Components.DigiIconArrowUp {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconArrowUp,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-arrow-up',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconArrowUp {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconAt extends Components.DigiIconAt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconAt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-at',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconAt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBars extends Components.DigiIconBars {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBars,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-bars',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBars {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBell extends Components.DigiIconBell {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBell,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-bell',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBell {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBellFilled extends Components.DigiIconBellFilled {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBellFilled,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-bell-filled',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBellFilled {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBook extends Components.DigiIconBook {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBook,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-book',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBook {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBubble extends Components.DigiIconBubble {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBubble,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-bubble',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBubble {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBubbleEllipsis extends Components.DigiIconBubbleEllipsis {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBubbleEllipsis,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-bubble-ellipsis',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBubbleEllipsis {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBuildingOutline extends Components.DigiIconBuildingOutline {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBuildingOutline,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-building-outline',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBuildingOutline {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBuildingSolid extends Components.DigiIconBuildingSolid {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBuildingSolid,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-building-solid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBuildingSolid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCalculatorSolid extends Components.DigiIconCalculatorSolid {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCalculatorSolid,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-calculator-solid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCalculatorSolid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCalendar extends Components.DigiIconCalendar {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCalendar,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCalendar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCalendarAlt extends Components.DigiIconCalendarAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCalendarAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-calendar-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCalendarAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCalendarAltAlert extends Components.DigiIconCalendarAltAlert {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCalendarAltAlert,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-calendar-alt-alert',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCalendarAltAlert {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCaretCircleRight extends Components.DigiIconCaretCircleRight {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCaretCircleRight,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-caret-circle-right',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCaretCircleRight {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCaretDown extends Components.DigiIconCaretDown {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCaretDown,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-caret-down',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCaretDown {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCaretLeft extends Components.DigiIconCaretLeft {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCaretLeft,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-caret-left',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCaretLeft {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCaretRight extends Components.DigiIconCaretRight {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCaretRight,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-caret-right',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCaretRight {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCaretUp extends Components.DigiIconCaretUp {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCaretUp,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-caret-up',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCaretUp {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChart extends Components.DigiIconChart {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChart,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChart {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChat extends Components.DigiIconChat {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChat,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chat',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChat {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCheck extends Components.DigiIconCheck {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCheck,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-check',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCheck {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCheckCircleRegAlt extends Components.DigiIconCheckCircleRegAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCheckCircleRegAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-check-circle-reg-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCheckCircleRegAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronDown extends Components.DigiIconChevronDown {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronDown,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-down',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChevronDown {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronLeft extends Components.DigiIconChevronLeft {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronLeft,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-left',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChevronLeft {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronRight extends Components.DigiIconChevronRight {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronRight,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-right',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChevronRight {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronUp extends Components.DigiIconChevronUp {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronUp,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-up',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChevronUp {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconClock extends Components.DigiIconClock {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconClock,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-clock',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconClock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCompressAlt extends Components.DigiIconCompressAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCompressAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-compress-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCompressAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconComunicationCase extends Components.DigiIconComunicationCase {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconComunicationCase,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-comunication-case',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconComunicationCase {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconComunicationFlag extends Components.DigiIconComunicationFlag {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconComunicationFlag,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-comunication-flag',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconComunicationFlag {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconComunicationGraduate extends Components.DigiIconComunicationGraduate {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconComunicationGraduate,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-comunication-graduate',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconComunicationGraduate {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconComunicationPlay extends Components.DigiIconComunicationPlay {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconComunicationPlay,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-comunication-play',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconComunicationPlay {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCooperation extends Components.DigiIconCooperation {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCooperation,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-cooperation',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCooperation {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCopy extends Components.DigiIconCopy {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCopy,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-copy',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCopy {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconDangerOutline extends Components.DigiIconDangerOutline {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconDangerOutline,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-danger-outline',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconDangerOutline {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconDownload extends Components.DigiIconDownload {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconDownload,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-download',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconDownload {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconDxa extends Components.DigiIconDxa {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconDxa,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-dxa',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconDxa {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconEdit extends Components.DigiIconEdit {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconEdit,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconEdit {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconEllipsis extends Components.DigiIconEllipsis {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconEllipsis,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-ellipsis',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconEllipsis {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconEnvelope extends Components.DigiIconEnvelope {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconEnvelope,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-envelope',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconEnvelope {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconEnvelopeFilled extends Components.DigiIconEnvelopeFilled {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconEnvelopeFilled,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-envelope-filled',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconEnvelopeFilled {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationCircleFilled extends Components.DigiIconExclamationCircleFilled {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationCircleFilled,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-circle-filled',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExclamationCircleFilled {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationTriangle extends Components.DigiIconExclamationTriangle {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationTriangle,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-triangle',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExclamationTriangle {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationTriangleFilled extends Components.DigiIconExclamationTriangleFilled {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationTriangleFilled,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-triangle-filled',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExclamationTriangleFilled {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationTriangleWarning extends Components.DigiIconExclamationTriangleWarning {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationTriangleWarning,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-triangle-warning',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExclamationTriangleWarning {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExpandAlt extends Components.DigiIconExpandAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExpandAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-expand-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExpandAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExternalLinkAlt extends Components.DigiIconExternalLinkAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExternalLinkAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-external-link-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExternalLinkAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconEye extends Components.DigiIconEye {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconEye,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-eye',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconEye {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconEyeSlash extends Components.DigiIconEyeSlash {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconEyeSlash,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-eye-slash',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconEyeSlash {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconFacebookSquare extends Components.DigiIconFacebookSquare {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconFacebookSquare,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-facebook-square',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconFacebookSquare {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconFileDocument extends Components.DigiIconFileDocument {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconFileDocument,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-file-document',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconFileDocument {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconFileExcel extends Components.DigiIconFileExcel {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconFileExcel,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-file-excel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconFileExcel {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconFilePdf extends Components.DigiIconFilePdf {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconFilePdf,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-file-pdf',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconFilePdf {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconFilePowerpoint extends Components.DigiIconFilePowerpoint {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconFilePowerpoint,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-file-powerpoint',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconFilePowerpoint {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconFileWord extends Components.DigiIconFileWord {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconFileWord,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-file-word',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconFileWord {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconFilm extends Components.DigiIconFilm {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconFilm,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-film',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconFilm {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconFilter extends Components.DigiIconFilter {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconFilter,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-filter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconFilter {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconGlobe extends Components.DigiIconGlobe {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconGlobe,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-globe',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconGlobe {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconGlobeFilled extends Components.DigiIconGlobeFilled {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconGlobeFilled,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-globe-filled',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconGlobeFilled {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconHeadphones extends Components.DigiIconHeadphones {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconHeadphones,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-headphones',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconHeadphones {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconHeart extends Components.DigiIconHeart {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconHeart,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-heart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconHeart {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconHeartSolid extends Components.DigiIconHeartSolid {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconHeartSolid,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-heart-solid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconHeartSolid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconHistory extends Components.DigiIconHistory {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconHistory,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-history',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconHistory {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconHome extends Components.DigiIconHome {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconHome,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-home',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconHome {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconImage extends Components.DigiIconImage {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconImage,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-image',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconImage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconInfoCircleSolid extends Components.DigiIconInfoCircleSolid {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconInfoCircleSolid,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-info-circle-solid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconInfoCircleSolid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconInstagram extends Components.DigiIconInstagram {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconInstagram,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-instagram',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconInstagram {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconJobSuggestion extends Components.DigiIconJobSuggestion {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconJobSuggestion,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-job-suggestion',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconJobSuggestion {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLanguage extends Components.DigiIconLanguage {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLanguage,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-language',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLanguage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLanguageOutline extends Components.DigiIconLanguageOutline {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLanguageOutline,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-language-outline',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLanguageOutline {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLattlast extends Components.DigiIconLattlast {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLattlast,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-lattlast',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLattlast {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLicenceBus extends Components.DigiIconLicenceBus {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLicenceBus,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-licence-bus',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLicenceBus {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLicenceCar extends Components.DigiIconLicenceCar {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLicenceCar,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-licence-car',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLicenceCar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLicenceMotorcycle extends Components.DigiIconLicenceMotorcycle {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLicenceMotorcycle,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-licence-motorcycle',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLicenceMotorcycle {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLicenceTruck extends Components.DigiIconLicenceTruck {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLicenceTruck,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-licence-truck',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLicenceTruck {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLightbulb extends Components.DigiIconLightbulb {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLightbulb,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-lightbulb',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLightbulb {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLinkedinIn extends Components.DigiIconLinkedinIn {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLinkedinIn,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-linkedin-in',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLinkedinIn {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconListUl extends Components.DigiIconListUl {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconListUl,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-list-ul',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconListUl {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconLock extends Components.DigiIconLock {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconLock,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-lock',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconLock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMarker extends Components.DigiIconMarker {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconMarker,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-marker',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconMarker {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMarkerFilled extends Components.DigiIconMarkerFilled {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconMarkerFilled,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-marker-filled',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconMarkerFilled {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMediaCourse extends Components.DigiIconMediaCourse {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconMediaCourse,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-media-course',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconMediaCourse {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMediaPodcast extends Components.DigiIconMediaPodcast {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconMediaPodcast,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-media-podcast',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconMediaPodcast {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMicrophone extends Components.DigiIconMicrophone {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconMicrophone,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-microphone',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconMicrophone {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMicrophoneOff extends Components.DigiIconMicrophoneOff {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconMicrophoneOff,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-microphone-off',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconMicrophoneOff {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMinus extends Components.DigiIconMinus {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconMinus,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-minus',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconMinus {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNews extends Components.DigiIconNews {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconNews,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-news',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconNews {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNotificationError extends Components.DigiIconNotificationError {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconNotificationError,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-notification-error',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconNotificationError {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNotificationInfo extends Components.DigiIconNotificationInfo {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconNotificationInfo,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-notification-info',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconNotificationInfo {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNotificationSucces extends Components.DigiIconNotificationSucces {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconNotificationSucces,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-notification-succes',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconNotificationSucces {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNotificationWarning extends Components.DigiIconNotificationWarning {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconNotificationWarning,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-notification-warning',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconNotificationWarning {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconOpenSource extends Components.DigiIconOpenSource {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconOpenSource,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-open-source',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconOpenSource {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPaperclip extends Components.DigiIconPaperclip {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconPaperclip,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-paperclip',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconPaperclip {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPen extends Components.DigiIconPen {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconPen,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-pen',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconPen {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPhone extends Components.DigiIconPhone {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconPhone,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-phone',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconPhone {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPhoneHangup extends Components.DigiIconPhoneHangup {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconPhoneHangup,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-phone-hangup',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconPhoneHangup {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPlus extends Components.DigiIconPlus {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconPlus,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-plus',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconPlus {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPrint extends Components.DigiIconPrint {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconPrint,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-print',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconPrint {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconRedo extends Components.DigiIconRedo {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconRedo,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-redo',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconRedo {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconScreensharing extends Components.DigiIconScreensharing {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconScreensharing,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-screensharing',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconScreensharing {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconScreensharingOff extends Components.DigiIconScreensharingOff {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconScreensharingOff,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-screensharing-off',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconScreensharingOff {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSearch extends Components.DigiIconSearch {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSearch,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSearch {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSettings extends Components.DigiIconSettings {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSettings,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-settings',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSettings {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconShareAlt extends Components.DigiIconShareAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconShareAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-share-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconShareAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSign extends Components.DigiIconSign {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSign,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-sign',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSign {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSlidersH extends Components.DigiIconSlidersH {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSlidersH,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-sliders-h',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSlidersH {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSokkandidat extends Components.DigiIconSokkandidat {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSokkandidat,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-sokkandidat',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSokkandidat {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSort extends Components.DigiIconSort {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSort,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-sort',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSort {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSortDown extends Components.DigiIconSortDown {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSortDown,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-sort-down',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSortDown {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSortUp extends Components.DigiIconSortUp {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSortUp,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-sort-up',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSortUp {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSpinner extends Components.DigiIconSpinner {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSpinner,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-spinner',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSpinner {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconStar extends Components.DigiIconStar {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconStar,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-star',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconStar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconStarReg extends Components.DigiIconStarReg {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconStarReg,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-star-reg',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconStarReg {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSuitcaseOutline extends Components.DigiIconSuitcaseOutline {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSuitcaseOutline,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-suitcase-outline',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSuitcaseOutline {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSuitcaseSolid extends Components.DigiIconSuitcaseSolid {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSuitcaseSolid,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-suitcase-solid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSuitcaseSolid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconTable extends Components.DigiIconTable {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconTable,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-table',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconTable {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconToggleOff extends Components.DigiIconToggleOff {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconToggleOff,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-toggle-off',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconToggleOff {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconToggleOn extends Components.DigiIconToggleOn {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconToggleOn,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-toggle-on',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconToggleOn {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconTrash extends Components.DigiIconTrash {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconTrash,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-trash',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconTrash {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconTwitter extends Components.DigiIconTwitter {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconTwitter,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-twitter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconTwitter {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconTyckTill extends Components.DigiIconTyckTill {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconTyckTill,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-tyck-till',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconTyckTill {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconUnlock extends Components.DigiIconUnlock {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconUnlock,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-unlock',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconUnlock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconUpdate extends Components.DigiIconUpdate {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconUpdate,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-update',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconUpdate {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconUpload extends Components.DigiIconUpload {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconUpload,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-upload',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconUpload {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconUserAlt extends Components.DigiIconUserAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconUserAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-user-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconUserAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconUsersSolid extends Components.DigiIconUsersSolid {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconUsersSolid,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-users-solid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconUsersSolid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconValidationError extends Components.DigiIconValidationError {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconValidationError,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-validation-error',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconValidationError {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconValidationSuccess extends Components.DigiIconValidationSuccess {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconValidationSuccess,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-validation-success',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconValidationSuccess {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconValidationWarning extends Components.DigiIconValidationWarning {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconValidationWarning,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-validation-warning',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconValidationWarning {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconVideocamera extends Components.DigiIconVideocamera {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconVideocamera,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-videocamera',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconVideocamera {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconVideocameraOff extends Components.DigiIconVideocameraOff {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconVideocameraOff,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-videocamera-off',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconVideocameraOff {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconVolume extends Components.DigiIconVolume {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconVolume,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-volume',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconVolume {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconWebTv extends Components.DigiIconWebTv {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconWebTv,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-web-tv',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconWebTv {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconWebinar extends Components.DigiIconWebinar {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconWebinar,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-webinar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconWebinar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconX extends Components.DigiIconX {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconX,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-x',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconX {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconXButton extends Components.DigiIconXButton {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconXButton,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-x-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconXButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconXButtonOutline extends Components.DigiIconXButtonOutline {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconXButtonOutline,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-x-button-outline',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconXButtonOutline {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconYoutube extends Components.DigiIconYoutube {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconYoutube,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-youtube',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconYoutube {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiInfoCard extends Components.DigiInfoCard {
  /**
   * Länkelementets 'onclick'-event @en Link element's 'onclick' event
   */
  afOnClickLink: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiInfoCard,
  inputs: ['afBorderPosition', 'afHeading', 'afHeadingLevel', 'afLinkHref', 'afLinkText', 'afSize', 'afSvgAriaHidden', 'afType', 'afVariation']
})
@Component({
  selector: 'digi-info-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afBorderPosition', 'afHeading', 'afHeadingLevel', 'afLinkHref', 'afLinkText', 'afSize', 'afSvgAriaHidden', 'afType', 'afVariation']
})
export class DigiInfoCard {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClickLink']);
  }
}


export declare interface DigiInfoCardMulti extends Components.DigiInfoCardMulti {
  /**
   * Länkelementets 'onclick'-event @en Link element's 'onclick' event
   */
  afOnClickLink: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiInfoCardMulti,
  inputs: ['afAlt', 'afHeading', 'afHeadingLevel', 'afLinkHref', 'afSrc', 'afTagIcon', 'afTagText', 'afType']
})
@Component({
  selector: 'digi-info-card-multi',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlt', 'afHeading', 'afHeadingLevel', 'afLinkHref', 'afSrc', 'afTagIcon', 'afTagText', 'afType']
})
export class DigiInfoCardMulti {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClickLink']);
  }
}


export declare interface DigiInfoCardMultiContainer extends Components.DigiInfoCardMultiContainer {}

@ProxyCmp({
  defineCustomElementFn: defineDigiInfoCardMultiContainer,
  inputs: ['afHeading', 'afHeadingLevel']
})
@Component({
  selector: 'digi-info-card-multi-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHeading', 'afHeadingLevel']
})
export class DigiInfoCardMultiContainer {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutBlock extends Components.DigiLayoutBlock {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutBlock,
  inputs: ['afContainer', 'afMarginBottom', 'afMarginTop', 'afVariation', 'afVerticalPadding']
})
@Component({
  selector: 'digi-layout-block',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afContainer', 'afMarginBottom', 'afMarginTop', 'afVariation', 'afVerticalPadding']
})
export class DigiLayoutBlock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutColumns extends Components.DigiLayoutColumns {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutColumns,
  inputs: ['afElement', 'afVariation']
})
@Component({
  selector: 'digi-layout-columns',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afElement', 'afVariation']
})
export class DigiLayoutColumns {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutContainer extends Components.DigiLayoutContainer {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutContainer,
  inputs: ['afMarginBottom', 'afMarginTop', 'afNoGutter', 'afVariation', 'afVerticalPadding']
})
@Component({
  selector: 'digi-layout-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afMarginBottom', 'afMarginTop', 'afNoGutter', 'afVariation', 'afVerticalPadding']
})
export class DigiLayoutContainer {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutMediaObject extends Components.DigiLayoutMediaObject {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutMediaObject,
  inputs: ['afAlignment']
})
@Component({
  selector: 'digi-layout-media-object',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlignment']
})
export class DigiLayoutMediaObject {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLink extends Components.DigiLink {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiLink,
  inputs: ['afDescribedby', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afTarget', 'afVariation'],
  methods: ['afMGetLinkElement']
})
@Component({
  selector: 'digi-link',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDescribedby', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afTarget', 'afVariation']
})
export class DigiLink {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick', 'afOnReady']);
  }
}


export declare interface DigiLinkButton extends Components.DigiLinkButton {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiLinkButton,
  inputs: ['afFullwidth', 'afHideIcon', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afSize', 'afVariation']
})
@Component({
  selector: 'digi-link-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afFullwidth', 'afHideIcon', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afSize', 'afVariation']
})
export class DigiLinkButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiLinkExternal extends Components.DigiLinkExternal {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiLinkExternal,
  inputs: ['afDescribedby', 'afHref', 'afTarget', 'afVariation']
})
@Component({
  selector: 'digi-link-external',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDescribedby', 'afHref', 'afTarget', 'afVariation']
})
export class DigiLinkExternal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiLinkInternal extends Components.DigiLinkInternal {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiLinkInternal,
  inputs: ['afDescribedby', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afVariation']
})
@Component({
  selector: 'digi-link-internal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDescribedby', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afVariation']
})
export class DigiLinkInternal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiList extends Components.DigiList {}

@ProxyCmp({
  defineCustomElementFn: defineDigiList,
  inputs: ['afListType']
})
@Component({
  selector: 'digi-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afListType']
})
export class DigiList {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLoaderSpinner extends Components.DigiLoaderSpinner {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLoaderSpinner,
  inputs: ['afSize', 'afText']
})
@Component({
  selector: 'digi-loader-spinner',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afSize', 'afText']
})
export class DigiLoaderSpinner {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLogo extends Components.DigiLogo {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLogo,
  inputs: ['afColor', 'afDesc', 'afSvgAriaHidden', 'afSystemName', 'afTitle', 'afTitleId', 'afVariation']
})
@Component({
  selector: 'digi-logo',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afColor', 'afDesc', 'afSvgAriaHidden', 'afSystemName', 'afTitle', 'afTitleId', 'afVariation']
})
export class DigiLogo {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiMediaFigure extends Components.DigiMediaFigure {}

@ProxyCmp({
  defineCustomElementFn: defineDigiMediaFigure,
  inputs: ['afAlignment', 'afFigcaption']
})
@Component({
  selector: 'digi-media-figure',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlignment', 'afFigcaption']
})
export class DigiMediaFigure {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiMediaImage extends Components.DigiMediaImage {
  /**
   * Bildlementets 'onload'-event. @en The image element's 'onload' event.
   */
  afOnLoad: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiMediaImage,
  inputs: ['afAlt', 'afAriaLabel', 'afFullwidth', 'afHeight', 'afObserverOptions', 'afSrc', 'afSrcset', 'afTitle', 'afUnlazy', 'afWidth']
})
@Component({
  selector: 'digi-media-image',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlt', 'afAriaLabel', 'afFullwidth', 'afHeight', 'afObserverOptions', 'afSrc', 'afSrcset', 'afTitle', 'afUnlazy', 'afWidth']
})
export class DigiMediaImage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnLoad']);
  }
}


export declare interface DigiNavigationBreadcrumbs extends Components.DigiNavigationBreadcrumbs {
  /**
   * Länkelementens 'onclick'-event. @en The link elements' 'onclick' events.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationBreadcrumbs,
  inputs: ['afAriaLabel', 'afCurrentPage', 'afId']
})
@Component({
  selector: 'digi-navigation-breadcrumbs',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaLabel', 'afCurrentPage', 'afId']
})
export class DigiNavigationBreadcrumbs {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiNavigationContextMenu extends Components.DigiNavigationContextMenu {
  /**
   * När komponenten stängs @en When component gets inactive
   */
  afOnInactive: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten öppnas @en When component gets active
   */
  afOnActive: EventEmitter<CustomEvent<any>>;
  /**
   * När fokus sätts utanför komponenten @en When focus is move outside of component
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Vid navigering till nytt listobjekt @en When navigating to a new list item
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
  /**
   * 'onclick'-event på knappelementen i listan @en List item buttons' 'onclick'-event
   */
  afOnSelect: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationContextMenu,
  inputs: ['afIcon', 'afId', 'afNavigationContextMenuItems', 'afStartSelected', 'afText']
})
@Component({
  selector: 'digi-navigation-context-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afIcon', 'afId', 'afNavigationContextMenuItems', 'afStartSelected', 'afText']
})
export class DigiNavigationContextMenu {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnInactive', 'afOnActive', 'afOnBlur', 'afOnChange', 'afOnToggle', 'afOnSelect']);
  }
}


export declare interface DigiNavigationContextMenuItem extends Components.DigiNavigationContextMenuItem {}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationContextMenuItem,
  inputs: ['afDir', 'afHref', 'afLang', 'afText', 'afType']
})
@Component({
  selector: 'digi-navigation-context-menu-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDir', 'afHref', 'afLang', 'afText', 'afType']
})
export class DigiNavigationContextMenuItem {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiNavigationPagination extends Components.DigiNavigationPagination {
  /**
   * Vid byte av sida @en When page changes
   */
  afOnPageChange: EventEmitter<CustomEvent<number>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationPagination,
  inputs: ['afCurrentResultEnd', 'afCurrentResultStart', 'afId', 'afInitActivePage', 'afLimit', 'afResultName', 'afTotalPages', 'afTotalResults'],
  methods: ['afMSetCurrentPage']
})
@Component({
  selector: 'digi-navigation-pagination',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCurrentResultEnd', 'afCurrentResultStart', 'afId', 'afInitActivePage', 'afLimit', 'afResultName', 'afTotalPages', 'afTotalResults']
})
export class DigiNavigationPagination {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnPageChange', 'afOnReady']);
  }
}


export declare interface DigiNavigationSidebar extends Components.DigiNavigationSidebar {
  /**
   * Stängknappens 'onclick'-event @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter<CustomEvent<any>>;
  /**
   * Stängning av sidofält med esc-tangenten @en At close/open of sidebar using esc-key
   */
  afOnEsc: EventEmitter<CustomEvent<any>>;
  /**
   * Stängning av sidofält med klick på skuggan bakom menyn @en At close of sidebar when clicking on backdrop
   */
  afOnBackdropClick: EventEmitter<CustomEvent<any>>;
  /**
   * Vid öppning/stängning av sidofält @en At close/open of sidebar
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationSidebar,
  inputs: ['afActive', 'afBackdrop', 'afCloseButtonAriaLabel', 'afCloseButtonPosition', 'afCloseButtonText', 'afCloseFocusableElement', 'afFocusableElement', 'afHeading', 'afHeadingLevel', 'afHideHeader', 'afId', 'afMobilePosition', 'afMobileVariation', 'afPosition', 'afStickyHeader', 'afVariation']
})
@Component({
  selector: 'digi-navigation-sidebar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afBackdrop', 'afCloseButtonAriaLabel', 'afCloseButtonPosition', 'afCloseButtonText', 'afCloseFocusableElement', 'afFocusableElement', 'afHeading', 'afHeadingLevel', 'afHideHeader', 'afId', 'afMobilePosition', 'afMobileVariation', 'afPosition', 'afStickyHeader', 'afVariation']
})
export class DigiNavigationSidebar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClose', 'afOnEsc', 'afOnBackdropClick', 'afOnToggle']);
  }
}


export declare interface DigiNavigationSidebarButton extends Components.DigiNavigationSidebarButton {
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationSidebarButton,
  inputs: ['afAriaLabel', 'afId', 'afText']
})
@Component({
  selector: 'digi-navigation-sidebar-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaLabel', 'afId', 'afText']
})
export class DigiNavigationSidebarButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnToggle']);
  }
}


export declare interface DigiNavigationTab extends Components.DigiNavigationTab {
  /**
   * När tabben växlar mellan aktiv och inaktiv @en When the tab toggles between active and inactive
   */
  afOnToggle: EventEmitter<CustomEvent<boolean>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationTab,
  inputs: ['afActive', 'afAriaLabel', 'afId']
})
@Component({
  selector: 'digi-navigation-tab',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afAriaLabel', 'afId']
})
export class DigiNavigationTab {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnToggle']);
  }
}


export declare interface DigiNavigationTabs extends Components.DigiNavigationTabs {
  /**
   * Vid byte av flik. Returnerar indexet på aktiv flik. @en When switching tabs. Returns the index of active tab.
   */
  afOnChange: EventEmitter<CustomEvent<number>>;
  /**
   * Vid klick på en flik-knapp. @en When clicking a tab button.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Vid fokus på en flik-knapp. @en When focusing a tab button.
   */
  afOnFocus: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * När <digi-navigation-tab> är initierad och laddad i komponenten. Returnerar indexet på aktiv flik. @en When <digi-navigation-tab> is initialized and loaded into the component. Returns the index of active tab.
   */
  afOnTabsReady: EventEmitter<CustomEvent<number>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationTabs,
  inputs: ['afAriaLabel', 'afId', 'afInitActiveTab', 'afPreventScrollOnFocus'],
  methods: ['afMSetActiveTab']
})
@Component({
  selector: 'digi-navigation-tabs',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaLabel', 'afId', 'afInitActiveTab', 'afPreventScrollOnFocus']
})
export class DigiNavigationTabs {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnClick', 'afOnFocus', 'afOnTabsReady', 'afOnReady']);
  }
}


export declare interface DigiNavigationVerticalMenu extends Components.DigiNavigationVerticalMenu {
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationVerticalMenu,
  inputs: ['afActiveIndicatorSize', 'afAriaLabel', 'afId', 'afVariation'],
  methods: ['afMSetCurrentActiveLevel']
})
@Component({
  selector: 'digi-navigation-vertical-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActiveIndicatorSize', 'afAriaLabel', 'afId', 'afVariation']
})
export class DigiNavigationVerticalMenu {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnReady']);
  }
}


export declare interface DigiNavigationVerticalMenuItem extends Components.DigiNavigationVerticalMenuItem {
  /**
   * Länken och toggle-knappens 'onclick'-event @en Link and toggle buttons 'onclick' event
   */
  afOnClick: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationVerticalMenuItem,
  inputs: ['afActive', 'afActiveSubnav', 'afAriaCurrent', 'afHasSubnav', 'afHref', 'afId', 'afOverrideLink', 'afText']
})
@Component({
  selector: 'digi-navigation-vertical-menu-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afActiveSubnav', 'afAriaCurrent', 'afHasSubnav', 'afHref', 'afId', 'afOverrideLink', 'afText']
})
export class DigiNavigationVerticalMenuItem {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiNotificationAlert extends Components.DigiNotificationAlert {
  /**
   * Stängknappens 'onclick'-event @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNotificationAlert,
  inputs: ['afCloseAriaLabel', 'afCloseable', 'afHeading', 'afHeadingLevel', 'afId', 'afSize', 'afVariation'],
  methods: ['afMGetHeadingElement']
})
@Component({
  selector: 'digi-notification-alert',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCloseAriaLabel', 'afCloseable', 'afHeading', 'afHeadingLevel', 'afId', 'afSize', 'afVariation']
})
export class DigiNotificationAlert {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClose', 'afOnReady']);
  }
}


export declare interface DigiNotificationErrorPage extends Components.DigiNotificationErrorPage {}

@ProxyCmp({
  defineCustomElementFn: defineDigiNotificationErrorPage,
  inputs: ['afCustomHeading', 'afHttpStatusCode', 'afId']
})
@Component({
  selector: 'digi-notification-error-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCustomHeading', 'afHttpStatusCode', 'afId']
})
export class DigiNotificationErrorPage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiProgressStep extends Components.DigiProgressStep {}

@ProxyCmp({
  defineCustomElementFn: defineDigiProgressStep,
  inputs: ['afHeading', 'afHeadingLevel', 'afId', 'afIsLast', 'afStepStatus', 'afVariation']
})
@Component({
  selector: 'digi-progress-step',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHeading', 'afHeadingLevel', 'afId', 'afIsLast', 'afStepStatus', 'afVariation']
})
export class DigiProgressStep {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiProgressSteps extends Components.DigiProgressSteps {
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiProgressSteps,
  inputs: ['afCurrentStep', 'afHeadingLevel', 'afVariation'],
  methods: ['afMNext', 'afMPrevious']
})
@Component({
  selector: 'digi-progress-steps',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCurrentStep', 'afHeadingLevel', 'afVariation']
})
export class DigiProgressSteps {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnReady']);
  }
}


export declare interface DigiProgressbar extends Components.DigiProgressbar {}

@ProxyCmp({
  defineCustomElementFn: defineDigiProgressbar,
  inputs: ['afCompletedSteps', 'afId', 'afRole', 'afStepsLabel', 'afStepsSeparator', 'afTotalSteps', 'afVariation']
})
@Component({
  selector: 'digi-progressbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCompletedSteps', 'afId', 'afRole', 'afStepsLabel', 'afStepsSeparator', 'afTotalSteps', 'afVariation']
})
export class DigiProgressbar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiQuoteMultiContainer extends Components.DigiQuoteMultiContainer {}

@ProxyCmp({
  defineCustomElementFn: defineDigiQuoteMultiContainer,
  inputs: ['afHeading', 'afHeadingLevel']
})
@Component({
  selector: 'digi-quote-multi-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHeading', 'afHeadingLevel']
})
export class DigiQuoteMultiContainer {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiQuoteSingle extends Components.DigiQuoteSingle {}

@ProxyCmp({
  defineCustomElementFn: defineDigiQuoteSingle,
  inputs: ['afQuoteAuthorName', 'afQuoteAuthorTitle', 'afQuoteText', 'afVariation']
})
@Component({
  selector: 'digi-quote-single',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afQuoteAuthorName', 'afQuoteAuthorTitle', 'afQuoteText', 'afVariation']
})
export class DigiQuoteSingle {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTable extends Components.DigiTable {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTable,
  inputs: ['afId', 'afSize', 'afVariation']
})
@Component({
  selector: 'digi-table',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afId', 'afSize', 'afVariation']
})
export class DigiTable {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTag extends Components.DigiTag {
  /**
   * Taggelementets 'onclick'-event. @en The tag elements 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiTag,
  inputs: ['afAriaLabel', 'afNoIcon', 'afSize', 'afText']
})
@Component({
  selector: 'digi-tag',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaLabel', 'afNoIcon', 'afSize', 'afText']
})
export class DigiTag {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiTagMedia extends Components.DigiTagMedia {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTagMedia,
  inputs: ['afIcon', 'afText']
})
@Component({
  selector: 'digi-tag-media',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afIcon', 'afText']
})
export class DigiTagMedia {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiToolsFeedback extends Components.DigiToolsFeedback {
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiToolsFeedback,
  inputs: ['afCloseButtonText', 'afFeedback', 'afHeadingLevel', 'afId', 'afIsDone', 'afText', 'afType', 'afVariation'],
  methods: ['afMReset', 'afMSetStep']
})
@Component({
  selector: 'digi-tools-feedback',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCloseButtonText', 'afFeedback', 'afHeadingLevel', 'afId', 'afIsDone', 'afText', 'afType', 'afVariation']
})
export class DigiToolsFeedback {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnReady']);
  }
}


export declare interface DigiToolsFeedbackBanner extends Components.DigiToolsFeedbackBanner {}

@ProxyCmp({
  defineCustomElementFn: defineDigiToolsFeedbackBanner,
  inputs: ['afHeading', 'afHeadingLevel', 'afId', 'afText', 'afType']
})
@Component({
  selector: 'digi-tools-feedback-banner',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHeading', 'afHeadingLevel', 'afId', 'afText', 'afType']
})
export class DigiToolsFeedbackBanner {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiToolsLanguagepicker extends Components.DigiToolsLanguagepicker {
  /**
   * När komponenten stängs @en When component gets inactive
   */
  afOnInactive: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten öppnas @en When component gets active
   */
  afOnActive: EventEmitter<CustomEvent<any>>;
  /**
   * När fokus sätts utanför komponenten @en When focus is move outside of component
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * När fokus sätts på komponenten @en When focus is on component
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Vid navigering till nytt listobjekt @en When navigating to a new list item
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
  /**
   * 'onclick'-event på knappelementen i listan @en List item buttons' 'onclick'-event
   */
  afOnSelect: EventEmitter<CustomEvent<any>>;
  /**
   * 'onclick'-event på någon av knapparna @en Button 'onclick'-event
   */
  afOnClick: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiToolsLanguagepicker,
  inputs: ['afId', 'afLanguagepickerHide', 'afLanguagepickerItems', 'afLanguagepickerStartSelected', 'afLanguagepickerText', 'afLanguagepickerVariation', 'afListenHide', 'afListenText', 'afSignlangHide', 'afSignlangText']
})
@Component({
  selector: 'digi-tools-languagepicker',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afId', 'afLanguagepickerHide', 'afLanguagepickerItems', 'afLanguagepickerStartSelected', 'afLanguagepickerText', 'afLanguagepickerVariation', 'afListenHide', 'afListenText', 'afSignlangHide', 'afSignlangText']
})
export class DigiToolsLanguagepicker {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnInactive', 'afOnActive', 'afOnBlur', 'afOnFocus', 'afOnChange', 'afOnToggle', 'afOnSelect', 'afOnClick']);
  }
}


export declare interface DigiTypography extends Components.DigiTypography {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypography,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-typography',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiTypography {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyHeadingJumbo extends Components.DigiTypographyHeadingJumbo {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyHeadingJumbo,
  inputs: ['afLevel', 'afText']
})
@Component({
  selector: 'digi-typography-heading-jumbo',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afLevel', 'afText']
})
export class DigiTypographyHeadingJumbo {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyMeta extends Components.DigiTypographyMeta {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyMeta,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-typography-meta',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiTypographyMeta {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyPreamble extends Components.DigiTypographyPreamble {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyPreamble
})
@Component({
  selector: 'digi-typography-preamble',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiTypographyPreamble {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyTime extends Components.DigiTypographyTime {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyTime,
  inputs: ['afDateTime', 'afVariation']
})
@Component({
  selector: 'digi-typography-time',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDateTime', 'afVariation']
})
export class DigiTypographyTime {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiUtilBreakpointObserver extends Components.DigiUtilBreakpointObserver {
  /**
   * Vid inläsning av sida samt vid uppdatering av brytpunkt @en At page load and at change of breakpoint on page
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'small' @en At breakpoint 'small'
   */
  afOnSmall: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'medium' @en At breakpoint 'medium'
   */
  afOnMedium: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'large' @en At breakpoint 'large'
   */
  afOnLarge: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'xlarge' @en At breakpoint 'xlarge'
   */
  afOnXLarge: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilBreakpointObserver
})
@Component({
  selector: 'digi-util-breakpoint-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiUtilBreakpointObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnSmall', 'afOnMedium', 'afOnLarge', 'afOnXLarge']);
  }
}


export declare interface DigiUtilDetectClickOutside extends Components.DigiUtilDetectClickOutside {
  /**
   * Vid klick utanför komponenten @en When click detected outside of component
   */
  afOnClickOutside: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Vid klick inuti komponenten @en When click detected inside of component
   */
  afOnClickInside: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilDetectClickOutside,
  inputs: ['afDataIdentifier']
})
@Component({
  selector: 'digi-util-detect-click-outside',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDataIdentifier']
})
export class DigiUtilDetectClickOutside {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClickOutside', 'afOnClickInside']);
  }
}


export declare interface DigiUtilDetectFocusOutside extends Components.DigiUtilDetectFocusOutside {
  /**
   * Vid fokus utanför komponenten @en When focus detected outside of component
   */
  afOnFocusOutside: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * Vid fokus inuti komponenten @en When focus detected inside of component
   */
  afOnFocusInside: EventEmitter<CustomEvent<FocusEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilDetectFocusOutside,
  inputs: ['afDataIdentifier']
})
@Component({
  selector: 'digi-util-detect-focus-outside',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDataIdentifier']
})
export class DigiUtilDetectFocusOutside {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusOutside', 'afOnFocusInside']);
  }
}


export declare interface DigiUtilIntersectionObserver extends Components.DigiUtilIntersectionObserver {
  /**
   * Vid statusförändring mellan 'unintersected' och 'intersected' @en On change between unintersected and intersected
   */
  afOnChange: EventEmitter<CustomEvent<boolean>>;
  /**
   * Vid statusförändring till 'intersected' @en On every change to intersected.
   */
  afOnIntersect: EventEmitter<CustomEvent<any>>;
  /**
   * Vid statusförändring till 'unintersected'
On every change to unintersected 
   */
  afOnUnintersect: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilIntersectionObserver,
  inputs: ['afOnce', 'afOptions']
})
@Component({
  selector: 'digi-util-intersection-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afOnce', 'afOptions']
})
export class DigiUtilIntersectionObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnIntersect', 'afOnUnintersect']);
  }
}


export declare interface DigiUtilKeydownHandler extends Components.DigiUtilKeydownHandler {
  /**
   * Vid keydown på escape @en At keydown on escape key
   */
  afOnEsc: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på enter @en At keydown on enter key
   */
  afOnEnter: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på tab @en At keydown on tab key
   */
  afOnTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på space @en At keydown on space key
   */
  afOnSpace: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på skift och tabb @en At keydown on shift and tab key
   */
  afOnShiftTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil upp @en At keydown on up arrow key
   */
  afOnUp: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil ner @en At keydown on down arrow key
   */
  afOnDown: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil vänster @en At keydown on left arrow key
   */
  afOnLeft: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil höger @en At keydown on right arrow key
   */
  afOnRight: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på home @en At keydown on home key
   */
  afOnHome: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på end @en At keydown on end key
   */
  afOnEnd: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på alla tangenter @en At keydown on any key
   */
  afOnKeyDown: EventEmitter<CustomEvent<KeyboardEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilKeydownHandler
})
@Component({
  selector: 'digi-util-keydown-handler',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiUtilKeydownHandler {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnEsc', 'afOnEnter', 'afOnTab', 'afOnSpace', 'afOnShiftTab', 'afOnUp', 'afOnDown', 'afOnLeft', 'afOnRight', 'afOnHome', 'afOnEnd', 'afOnKeyDown']);
  }
}


export declare interface DigiUtilKeyupHandler extends Components.DigiUtilKeyupHandler {
  /**
   * Vid keyup på escape @en At keyup on escape key
   */
  afOnEsc: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på enter @en At keyup on enter key
   */
  afOnEnter: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på tab @en At keyup on tab key
   */
  afOnTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på space @en At keyup on space key
   */
  afOnSpace: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på skift och tabb @en At keyup on shift and tab key
   */
  afOnShiftTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil upp @en At keyup on up arrow key
   */
  afOnUp: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil ner @en At keyup on down arrow key
   */
  afOnDown: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil vänster @en At keyup on left arrow key
   */
  afOnLeft: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil höger @en At keyup on right arrow key
   */
  afOnRight: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på home @en At keyup on home key
   */
  afOnHome: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på end @en At keyup on end key
   */
  afOnEnd: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på alla tangenter @en At keyup on any key
   */
  afOnKeyUp: EventEmitter<CustomEvent<KeyboardEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilKeyupHandler
})
@Component({
  selector: 'digi-util-keyup-handler',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiUtilKeyupHandler {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnEsc', 'afOnEnter', 'afOnTab', 'afOnSpace', 'afOnShiftTab', 'afOnUp', 'afOnDown', 'afOnLeft', 'afOnRight', 'afOnHome', 'afOnEnd', 'afOnKeyUp']);
  }
}


export declare interface DigiUtilMutationObserver extends Components.DigiUtilMutationObserver {
  /**
   * När DOM-element läggs till eller tas bort inuti Mutation Observer:n @en When DOM-elements is added or removed inside the Mutation Observer
   */
  afOnChange: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilMutationObserver,
  inputs: ['afId', 'afOptions']
})
@Component({
  selector: 'digi-util-mutation-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afId', 'afOptions']
})
export class DigiUtilMutationObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange']);
  }
}


export declare interface DigiUtilResizeObserver extends Components.DigiUtilResizeObserver {
  /**
   * När komponenten ändrar storlek @en When the component changes size
   */
  afOnChange: EventEmitter<CustomEvent<ResizeObserverEntry>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilResizeObserver
})
@Component({
  selector: 'digi-util-resize-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiUtilResizeObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange']);
  }
}
