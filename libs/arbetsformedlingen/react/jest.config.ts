/* eslint-disable */
export default {
	displayName: 'arbetsformedlingen-react',
	preset: '../../jest.preset.js',
	transform: {
		'^.+\\.[tj]sx?$': 'babel-jest'
	},
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
	coverageDirectory: '../../coverage/libs/arbetsformedlingen-react'
};
