### Börja jobba i digi

Kör digi projektet.
- Kör kommandot `npm run start arbetsformedlingen` i ett terminalfönster från rotmappen.

Starta storybook i digi projektet.
- Kör kommandot `npm run storybook arbetsformedlingen`.

### Skapa en ny komponent

För att skapa din fösta komponent så behöver du följa beskrivningen nedan.
- Kör kommandot `npm run generate-component` i ett terminalfönster.
- Välj ett namn för din komponent t.ex. `button`.
- Välj sedan en kategori t.ex. `form, navigation etc`.
- Välj sedan en variation eller flera variation, separera med komma t.ex. `primary, secondary, small, medium`
- Ange `arbetsformedlingen` när den frågar vilket projekt du vill använda.
- En komponent ska nu ha skapats under `libs\arbetsformedlingen\package\src\components`.