export const kebabCase = (str: string) =>
	str.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase();

export const slugify = (str: string) =>
	str
		.toLowerCase()
		.trim()
		.replace(/å/g, 'a')
		.replace(/ä/g, 'a')
		.replace(/ö/g, 'o')
		.replace(/[^\w\s-]/g, '')
		.replace(/[\s_-]+/g, '-')
		.replace(/^-+|-+$/g, '');

export const unslugify = (str: string) =>
	str
		.replace(/-/g, ' ')
		.split(' ')
		.map((word, i) =>
			i === 0 ? word.charAt(0).toUpperCase() + word.slice(1) : word
		)
		.join(' ');
