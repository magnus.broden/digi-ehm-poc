# digi-expandable-accordion

This is a expandable accordion component.

## Enums

If used in a Typescript environment, you will need to import enum:

```ts
import {  ExpandableAccordionHeaderLevel} from '@digi/core';

```


<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                                                          | Type                                                                                                                                                                                                                         | Default                                          |
| ---------------- | ------------------ | ---------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------ |
| `afAnimation`    | `af-animation`     | Ställer in ifall komponenten ska ha en animation vid utfällning. Animation är påslagen som standard. | `boolean`                                                                                                                                                                                                                    | `true`                                           |
| `afExpanded`     | `af-expanded`      | Sätter läge på rubriken om den ska vara expanderad eller ej.                                         | `boolean`                                                                                                                                                                                                                    | `false`                                          |
| `afHeading`      | `af-heading`       | Rubrikens Utfällbar yta                                                                              | `string`                                                                                                                                                                                                                     | `undefined`                                      |
| `afHeadingLevel` | `af-heading-level` | Sätter utfällbar rubrik. 'h2' är förvalt.                                                            | `ExpandableAccordionHeaderLevel.H1 \| ExpandableAccordionHeaderLevel.H2 \| ExpandableAccordionHeaderLevel.H3 \| ExpandableAccordionHeaderLevel.H4 \| ExpandableAccordionHeaderLevel.H5 \| ExpandableAccordionHeaderLevel.H6` | `ExpandableAccordionHeaderLevel.H2`              |
| `afId`           | `af-id`            | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                | `string`                                                                                                                                                                                                                     | `randomIdGenerator('digi-expandable-accordion')` |
| `afVariation`    | `af-variation`     | Utfällbara ytans variation                                                                           | `ExpandableAccordionVariation.PRIMARY \| ExpandableAccordionVariation.SECONDARY`                                                                                                                                             | `ExpandableAccordionVariation.PRIMARY`           |


## Events

| Event       | Description                       | Type                      |
| ----------- | --------------------------------- | ------------------------- |
| `afOnClick` | Buttonelementets 'onclick'-event. | `CustomEvent<MouseEvent>` |


## CSS Custom Properties

| Name                                                            | Description                                                |
| --------------------------------------------------------------- | ---------------------------------------------------------- |
| `--digi--expandable-accordion--border-bottom-color`             | var(--digi--color--border--informative);                   |
| `--digi--expandable-accordion--border-bottom-width`             | var(--digi--border-width--secondary);                      |
| `--digi--expandable-accordion--content--padding`                | var(--digi--padding--medium) 0;                            |
| `--digi--expandable-accordion--content--transition`             | ease-in-out var(--digi--animation--duration--base) height; |
| `--digi--expandable-accordion--header--font-size`               | var(--digi--typography--accordion--font-size--desktop);    |
| `--digi--expandable-accordion--header--font-size--small`        | var(--digi--typography--accordion--font-size--mobile);     |
| `--digi--expandable-accordion--header--font-weight`             | var(--digi--typography--accordion--font-weight--desktop);  |
| `--digi--expandable-accordion--header--toggle-icon--transition` | ease-in-out var(--digi--animation--duration--base) all;    |
| `--digi--expandable-accordion--icon--margin-right`              | var(--digi--margin--medium);                               |
| `--digi--expandable-accordion--icon--size`                      | 0.875rem;                                                  |


## Dependencies

### Depends on

- [digi-icon](../../_icon/icon)
- [digi-typography](../../_typography/typography)
- [digi-button](../../_button/button)

### Graph
```mermaid
graph TD;
  digi-expandable-accordion --> digi-icon
  digi-expandable-accordion --> digi-typography
  digi-expandable-accordion --> digi-button
  style digi-expandable-accordion fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
