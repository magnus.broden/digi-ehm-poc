export enum ExpandableAccordionVariation {
	PRIMARY = 'primary',
	SECONDARY = 'secondary'
}
