import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ExpandableAccordionHeaderLevel } from './expandable-accordion-header-level.enum';
import { ExpandableAccordionVariation } from './expandable-accordion-variation.enum';

export default {
	title: 'expandable/digi-expandable-accordion',
	argTypes: {
		'af-heading-level': enumSelect(ExpandableAccordionHeaderLevel),
		'af-variation': enumSelect(ExpandableAccordionVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-expandable-accordion',
	'af-heading': 'A required heading',
	'af-heading-level': ExpandableAccordionHeaderLevel.H2,
	'af-variation': ExpandableAccordionVariation.PRIMARY,
	'af-expanded': false,
	'af-id': null,
	'af-animation': true,
	children: `<p>Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec id elit non mi porta gravida at eget metus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.
	Nullam id dolor id nibh ultricies vehicula ut id elit. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec id elit non mi porta gravida at eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
	Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Curabitur blandit tempus porttitor.</p>`
};
