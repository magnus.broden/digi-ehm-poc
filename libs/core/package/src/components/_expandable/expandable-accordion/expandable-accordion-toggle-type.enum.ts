export enum ExpandableAccordionToggleType {
	IMPLICIT = 'implicit',
	EXPLICIT = 'explicit'
}
