import { newE2EPage } from '@stencil/core/testing';

describe('digi-media-figure', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-media-figure></digi-media-figure>');

    const element = await page.find('digi-media-figure');
    expect(element).toHaveClass('hydrated');
  });
});
