# digi-media-figure

This is a figure component. Wrap this around an image (preferably a `digi-media-image`), and pass an optional figcaption as a prop.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { MediaFigureAlignment } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                        | Type                                                                                    | Default                      |
| -------------- | --------------- | ------------------------------------------------------------------ | --------------------------------------------------------------------------------------- | ---------------------------- |
| `afAlignment`  | `af-alignment`  | Justering av bild och text. Kan vara 'start', 'center' eller 'end' | `MediaFigureAlignment.CENTER \| MediaFigureAlignment.END \| MediaFigureAlignment.START` | `MediaFigureAlignment.START` |
| `afFigcaption` | `af-figcaption` | Lägger till ett valfritt figcaptionelement                         | `string`                                                                                | `undefined`                  |


## Slots

| Slot        | Description                                                              |
| ----------- | ------------------------------------------------------------------------ |
| `"default"` | Ska vara en bild (eller bildlik) komponent. Helst en `digi-media-image`. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
