import { newE2EPage } from '@stencil/core/testing';

describe('digi-media-image', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-media-image></digi-media-image>');

    const element = await page.find('digi-media-image');
    expect(element).toHaveClass('hydrated');
  });
});
