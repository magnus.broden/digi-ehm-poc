# digi-media-image

This is an image component with built in lazy-loading. If width and height-props are provided, it also creates a correctly sized placeholder before load, to prevent ugly repaint of the document.

<!-- Auto Generated Below -->


## Properties

| Property             | Attribute       | Description                                                                                                                                   | Type                                         | Default                                            |
| -------------------- | --------------- | --------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- | -------------------------------------------------- |
| `afAlt` _(required)_ | `af-alt`        | Sätter attributet 'alt'.                                                                                                                      | `string`                                     | `undefined`                                        |
| `afAriaLabel`        | `af-aria-label` | Sätter attributet 'aria-label'.                                                                                                               | `string`                                     | `undefined`                                        |
| `afFullwidth`        | `af-fullwidth`  | Sätter bilden till 100% i bredd och anpassar höjden automatiskt efter bredden.                                                                | `boolean`                                    | `undefined`                                        |
| `afHeight`           | `af-height`     | Sätter attributet 'height'. Om både 'width' och 'height' är satta så kommer komponenten att ha rätt aspektratio även innan bilden laddats in. | `string`                                     | `undefined`                                        |
| `afObserverOptions`  | --              | Skicka options till komponentens interna Intersection Observer. T.ex. för att kontrollera när bilden lazy loadas.                             | `{ rootMargin: string; threshold: number; }` | `{     rootMargin: '200px',     threshold: 0,   }` |
| `afSrc`              | `af-src`        | Sätter attributet 'src'.                                                                                                                      | `string`                                     | `undefined`                                        |
| `afSrcset`           | `af-srcset`     | Sätter attributet 'srcset'.                                                                                                                   | `string`                                     | `undefined`                                        |
| `afTitle`            | `af-title`      | Sätter attributet 'title'.                                                                                                                    | `string`                                     | `undefined`                                        |
| `afUnlazy`           | `af-unlazy`     | Tar bort lazy loading av bilden.                                                                                                              | `boolean`                                    | `false`                                            |
| `afWidth`            | `af-width`      | Sätter attributet 'width'. Om både 'width' och 'height' är satta så kommer komponenten att ha rätt aspektratio även innan bilden laddats in.  | `string`                                     | `undefined`                                        |


## Events

| Event      | Description                   | Type               |
| ---------- | ----------------------------- | ------------------ |
| `afOnLoad` | Bildlementets 'onload'-event. | `CustomEvent<any>` |


## Dependencies

### Depends on

- [digi-util-intersection-observer](../../_util/util-intersection-observer)

### Graph
```mermaid
graph TD;
  digi-media-image --> digi-util-intersection-observer
  style digi-media-image fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
