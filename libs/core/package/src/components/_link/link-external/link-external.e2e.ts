import { newE2EPage } from '@stencil/core/testing';

describe('digi-link-external', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-link-external></digi-link-external>');

    const element = await page.find('digi-link-external');
    expect(element).toHaveClass('hydrated');
  });
});
