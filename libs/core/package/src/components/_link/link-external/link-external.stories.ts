import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { LinkExternalVariation } from './link-external-variation.enum';

export default {
	title: 'link/digi-link-external',
	parameters: {
		actions: {
			handles: ['afOnClick']
		}
	},
	argTypes: {
		'af-variation': enumSelect(LinkExternalVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-link-external',
	'af-href': 'A link is required',
	'af-variation': LinkExternalVariation.SMALL,
	'af-target': '',
	/* html */
	children: 'I am an external link'
};
