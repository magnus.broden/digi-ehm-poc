# digi-link-external

I am an external link in two sizes, size S and L.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { LinkExternalVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute        | Description                                                                                                                           | Type                 | Default                       |
| --------------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------- | -------------------- | ----------------------------- |
| `afDescribedby`       | `af-describedby` | Sätter attributet 'aria-describedby'.                                                                                                 | `string`             | `undefined`                   |
| `afHref` _(required)_ | `af-href`        | Sätter attributet 'href'.                                                                                                             | `string`             | `undefined`                   |
| `afTarget`            | `af-target`      | Sätter attributet 'target'. Om värdet är '_blank' så lägger komponenten automatikt till ett ´ref´-attribut med 'noopener noreferrer'. | `string`             | `undefined`                   |
| `afVariation`         | `af-variation`   | Sätter variant. Kan vara 'small' eller 'large'.                                                                                       | `"large" \| "small"` | `LinkExternalVariation.SMALL` |


## Events

| Event       | Description                     | Type                      |
| ----------- | ------------------------------- | ------------------------- |
| `afOnClick` | Länkelementets 'onclick'-event. | `CustomEvent<MouseEvent>` |


## Slots

| Slot        | Description         |
| ----------- | ------------------- |
| `"default"` | Ska vara en textnod |


## CSS Custom Properties

| Name                                                    | Description                                                |
| ------------------------------------------------------- | ---------------------------------------------------------- |
| `--digi--link-external--color--default`                 | var(--digi--color--text--link);                            |
| `--digi--link-external--color--hover`                   | var(--digi--color--text--link-hover);                      |
| `--digi--link-external--color--visited`                 | var(--digi--color--text--link-visited);                    |
| `--digi--link-external--font-family`                    | var(--digi--global--typography--font-family--default);     |
| `--digi--link-external--font-size--large`               | var(--digi--typography--link--font-size--desktop-large);   |
| `--digi--link-external--font-size--small`               | var(--digi--typography--link--font-size--desktop);         |
| `--digi--link-external--font-weight`                    | var(--digi--typography--link--font-weight--desktop);       |
| `--digi--link-external--gap`                            | var(--digi--gutter--medium);                               |
| `--digi--link-external--text-decoration--default`       | var(--digi--global--typography--text-decoration--default); |
| `--digi--link-external--text-decoration--hover`         | var(--digi--typography--link--text-decoration--desktop);   |
| `--digi--link-external--text-decoration--icon--default` | var(--digi--global--typography--text-decoration--default); |
| `--digi--link-external--text-decoration--icon--hover`   | var(--digi--typography--link--text-decoration--desktop);   |


## Dependencies

### Depends on

- [digi-link](../link)
- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-link-external --> digi-link
  digi-link-external --> digi-icon
  style digi-link-external fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
