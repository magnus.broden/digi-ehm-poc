import { newE2EPage } from '@stencil/core/testing';

describe('digi-link', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-link></digi-link>');

    const element = await page.find('digi-link');
    expect(element).toHaveClass('hydrated');
  });
});
