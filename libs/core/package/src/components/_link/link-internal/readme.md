# digi-link-internal

A component for internal links.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { LinkInternalVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute           | Description                                                                                                                           | Type                 | Default                       |
| --------------------- | ------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | -------------------- | ----------------------------- |
| `afDescribedby`       | `af-describedby`    | Sätter attributet 'aria-describedby'.                                                                                                 | `string`             | `undefined`                   |
| `afHref` _(required)_ | `af-href`           | Sätter attributet 'href'.                                                                                                             | `string`             | `undefined`                   |
| `afLinkContainer`     | `af-link-container` | Sätt till true om du använder Angular, se exempelkod under 'Översikt'                                                                 | `boolean`            | `false`                       |
| `afOverrideLink`      | `af-override-link`  | Kringgår länkens vanliga beteende. Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande. | `boolean`            | `false`                       |
| `afVariation`         | `af-variation`      | Sätter variant. Kan vara 'small' eller 'large'.                                                                                       | `"large" \| "small"` | `LinkInternalVariation.SMALL` |


## Events

| Event       | Description                     | Type                      |
| ----------- | ------------------------------- | ------------------------- |
| `afOnClick` | Länkelementets 'onclick'-event. | `CustomEvent<MouseEvent>` |


## Slots

| Slot        | Description         |
| ----------- | ------------------- |
| `"default"` | Ska vara en textnod |


## CSS Custom Properties

| Name                                                    | Description                                                |
| ------------------------------------------------------- | ---------------------------------------------------------- |
| `--digi--link-internal--color--default`                 | var(--digi--color--text--link);                            |
| `--digi--link-internal--color--hover`                   | var(--digi--color--text--link-hover);                      |
| `--digi--link-internal--color--visited`                 | var(--digi--color--text--link-visited);                    |
| `--digi--link-internal--font-family`                    | var(--digi--global--typography--font-family--default);     |
| `--digi--link-internal--font-size--large`               | var(--digi--typography--link--font-size--desktop-large);   |
| `--digi--link-internal--font-size--small`               | var(--digi--typography--link--font-size--desktop);         |
| `--digi--link-internal--font-weight`                    | var(--digi--typography--link--font-weight--desktop);       |
| `--digi--link-internal--gap`                            | var(--digi--gutter--medium);                               |
| `--digi--link-internal--text-decoration--default`       | var(--digi--global--typography--text-decoration--default); |
| `--digi--link-internal--text-decoration--hover`         | var(--digi--typography--link--text-decoration--desktop);   |
| `--digi--link-internal--text-decoration--icon--default` | var(--digi--global--typography--text-decoration--default); |
| `--digi--link-internal--text-decoration--icon--hover`   | var(--digi--typography--link--text-decoration--desktop);   |


## Dependencies

### Depends on

- [digi-link](../link)
- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-link-internal --> digi-link
  digi-link-internal --> digi-icon
  style digi-link-internal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
