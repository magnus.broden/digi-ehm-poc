import { newE2EPage } from '@stencil/core/testing';

describe('digi-link-internal', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-link-internal></digi-link-internal>');

    const element = await page.find('digi-link-internal');
    expect(element).toHaveClass('hydrated');
  });
});
