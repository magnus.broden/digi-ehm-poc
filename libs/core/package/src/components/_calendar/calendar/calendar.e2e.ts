import { newE2EPage } from '@stencil/core/testing';

describe('digi-calendar', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-calendar></digi-calendar>');

    const element = await page.find('digi-calendar');
    expect(element).toHaveClass('hydrated');
  });
});
