# digi-calendar

This is a calendar component using a slot for displaying elements beneath it, preferable buttons.

By passing in `af-init-selected-month` it is possible to set the start month. The sizes of the navigation buttons and month-selector
can be changed using the enums. The month selector can be removed by usage of the `af-month-selector` and the calendar can be hidden
by using the `af-active` prop.

The calendar uses `util-keyup-handler` for navigation with the up, down, left, right, tab, shift-tab and enter keys.

The focused date is stored when navigating between the months.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { CalendarButtonSize, CalendarSelectSize } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute                | Description                                                                                                                                                                  | Type      | Default                              |
| --------------------- | ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------------------------------------ |
| `afActive`            | `af-active`              | Visar döljer kalendern. Visas som förvalt                                                                                                                                    | `boolean` | `true`                               |
| `afId`                | `af-id`                  | Prefix för komponentens alla olika interna 'id'-attribut på kalendern etc. Ex. 'my-cool-id' genererar 'my-cool-id-calendar' osv. Ges inget värde genereras ett slumpmässigt. | `string`  | `randomIdGenerator('digi-calendar')` |
| `afInitSelectedDate`  | --                       | Valt måndad och år från start. Förvalt är dagens månad och år.                                                                                                               | `Date`    | `new Date()`                         |
| `afInitSelectedMonth` | `af-init-selected-month` | Vald månad att visa från start. Förvalt är den månad som dagens datum har. (Januari = 0, December = 11)                                                                      | `number`  | `new Date().getMonth()`              |
| `afMaxDate`           | --                       | Senaste valbara datumet                                                                                                                                                      | `Date`    | `undefined`                          |
| `afMinDate`           | --                       | Tidigaste valbara datumet                                                                                                                                                    | `Date`    | `undefined`                          |
| `afMultipleDates`     | `af-multiple-dates`      | Sätt denna till true för att kunna markera mer en ett datum i kalendern. Förvalt är false                                                                                    | `boolean` | `false`                              |
| `afSelectedDate`      | --                       | Valt datum i kalendern                                                                                                                                                       | `Date`    | `undefined`                          |
| `afSelectedDates`     | --                       | Valda datum i kalendern                                                                                                                                                      | `Date[]`  | `[]`                                 |
| `afShowWeekNumber`    | `af-show-week-number`    | Visa veckonummer i kalender. Förvalt är false                                                                                                                                | `boolean` | `false`                              |


## Events

| Event                    | Description                                                                                            | Type               |
| ------------------------ | ------------------------------------------------------------------------------------------------------ | ------------------ |
| `afOnClickOutside`       | Sker vid klick utanför kalendern                                                                       | `CustomEvent<any>` |
| `afOnDateSelectedChange` | Sker när ett datum har valts eller avvalts. Returnerar datumen i en array.                             | `CustomEvent<any>` |
| `afOnDirty`              | Sker när kalenderdatumen har rörts första gången, när värdet på focusedDate har ändrats första gången. | `CustomEvent<any>` |
| `afOnFocusOutside`       | Sker när fokus flyttas utanför kalendern                                                               | `CustomEvent<any>` |


## CSS Custom Properties

| Name                                                       | Description                                                                    |
| ---------------------------------------------------------- | ------------------------------------------------------------------------------ |
| `--digi--calendar--date--background-color`                 | var(--digi--color--background--primary);                                       |
| `--digi--calendar--date--background-color--hover`          | var(--digi--global--color--cta--blue--dark);                                   |
| `--digi--calendar--date--border-color`                     | none;                                                                          |
| `--digi--calendar--date--box-shadow`                       | none;                                                                          |
| `--digi--calendar--date--box-shadow--hover`                | inset 0 0 0 var(--digi--border-width--primary) var(--digi--color--text--link); |
| `--digi--calendar--date--color`                            | var(--digi--color--text--primary);                                             |
| `--digi--calendar--date--focused--background-color`        | var(--digi--global--color--cta--blue--dark);                                   |
| `--digi--calendar--date--focused--box-shadow`              | inset 0 0 0 var(--digi--border-width--primary) var(--digi--color--text--link); |
| `--digi--calendar--date--padding`                          | var(--digi--gutter--large);                                                    |
| `--digi--calendar--date--selected--background-color`       | var(--digi--color--background--complementary-1);                               |
| `--digi--calendar--date--selected--color`                  | var(--digi--color--text--inverted);                                            |
| `--digi--calendar--date--today--background-color`          | var(--digi--color--background--neutral-1);                                     |
| `--digi--calendar--date--today--background-color--focused` | var(--digi--global--color--cta--blue--dark);                                   |
| `--digi--calendar--date--today--background-color--hover`   | var(--digi--global--color--cta--blue--dark);                                   |
| `--digi--calendar--date--today--border-color`              | var(--digi--color--background--neutral-1);                                     |
| `--digi--calendar--date--today--box-shadow`                | none;                                                                          |
| `--digi--calendar--date--today--circle-border`             | 2px solid var(--digi--color--text--secondary);                                 |
| `--digi--calendar--date--today--circle-margin`             | 10%;                                                                           |
| `--digi--calendar--date--today--color`                     | var(--digi--color--text--primary);                                             |
| `--digi--calendar--display`                                | inline-block;                                                                  |
| `--digi--calendar--header--align-items`                    | center;                                                                        |
| `--digi--calendar--header--button--background`             | transparent;                                                                   |
| `--digi--calendar--header--button--background--hover`      | var(--digi--global--color--cta--blue--dark);                                   |
| `--digi--calendar--header--button--box-shadow`             | none;                                                                          |
| `--digi--calendar--header--button--padding`                | var(--digi--gutter--medium) var(--digi--gutter--largest);                      |
| `--digi--calendar--header--display`                        | flex;                                                                          |
| `--digi--calendar--header--font-family`                    | var(--digi--global--typography--font-family--default);                         |
| `--digi--calendar--header--font-weight`                    | var(--digi--global--typography--font-weight--semibold);                        |
| `--digi--calendar--header--justify-content`                | space-between;                                                                 |
| `--digi--calendar--header--text-align`                     | center;                                                                        |
| `--digi--calendar--table--border-collapse`                 | collapse;                                                                      |
| `--digi--calendar--table--border-spacing`                  | 0;                                                                             |
| `--digi--calendar--table--font-family`                     | var(--digi--global--typography--font-family--default);                         |
| `--digi--calendar--table--font-weight`                     | var(--digi--global--typography--font-weight--semibold);                        |
| `--digi--calendar--table-header--font-weight-normal`       | var(--digi--typography--body--font-weight--desktop);                           |
| `--digi--calendar--table-header--padding`                  | var(--digi--gutter--larger) 0;                                                 |
| `--digi--calendar--week--border-color`                     | var(--digi--color--background--neutral-3);                                     |
| `--digi--calendar--week--border-width`                     | var(--digi--border-width--primary);                                            |
| `--digi--calendar--week--font-size`                        | var(--digi--global--typography--font-size--small);                             |
| `--digi--calendar--week--font-style`                       | italic;                                                                        |


## Dependencies

### Depends on

- [digi-icon](../../_icon/icon)
- [digi-util-keydown-handler](../../_util/util-keydown-handler)

### Graph
```mermaid
graph TD;
  digi-calendar --> digi-icon
  digi-calendar --> digi-util-keydown-handler
  style digi-calendar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
