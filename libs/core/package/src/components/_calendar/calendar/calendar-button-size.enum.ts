export enum CalendarButtonSize {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large'
}
