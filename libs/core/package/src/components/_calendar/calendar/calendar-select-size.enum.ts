export enum CalendarSelectSize {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large'
}
