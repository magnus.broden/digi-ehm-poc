import { _t } from '@digi/shared/text';
import {
	Component,
	Event,
	EventEmitter,
	Prop,
	State,
	Watch,
	h,
	Method
} from '@stencil/core';
import { format, getISOWeek, addDays, subDays, setDay } from 'date-fns';
import svlocale from 'date-fns/locale/sv';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ButtonVariation } from '../../_button/button/button-variation.enum';
import { CalendarWeekViewHeadingLevel } from './calendar-week-view-heading-level.enum';

/**
 * @enums CalendarWeekViewHeadingLevel - calendar-week-view-heading-level.enum.ts
 * @swedishName Veckovy
 */

@Component({
	tag: 'digi-calendar-week-view',
	styleUrls: ['calendar-week-view.scss'],
	scoped: true
})
export class CalendarWeekView {
	private minDate = new Date();
	private maxDate = new Date();

	@State() dates = [];
	@State() weekDays = [];

	@State() todaysDate = format(new Date(), 'yyyy-MM-dd', { locale: svlocale });

	@State() selectedDate = format(new Date(), 'yyyy-MM-dd', { locale: svlocale });
	@Watch('selectedDate')
	selectedDateChangeHandler() {
		this.afOnDateChange.emit(this.selectedDate);
	}

	@State() currentDate;
	@Watch('currentDate')
	currentDateChangeHandler() {
		this.setWeekDates();
	}
	setWeekDates() {
		let newDates = [];
		for (let i = 1; i < 6; i++) {
			const day = setDay(new Date(this.currentDate), i);
			const date = format(new Date(day), 'yyyy-MM-dd', { locale: svlocale });
			newDates.push(date);
		}
		this.weekDays = [...newDates];
	}

	@Prop({ mutable: true }) afMinWeek: number;
	@Prop({ mutable: true }) afMaxWeek: number;

	/**
	 * Lista över valbara datum
	 * @en List of selectable dates
	 */
	@Prop({ mutable: true }) afDates: string = '';
	@Watch('afDates')
	afDatesUpdate() {
		const dateList = [...JSON.parse(this.afDates)];
		this.dates = [
			...dateList.sort((a, b) => {
				var c: any = new Date(a);
				var d: any = new Date(b);
				return c - d;
			})
		];
	}

	/**
	 * Sätt rubrikens vikt. 'h2' är förvalt.
	 * @en Set heading level. Default is 'h2'
	 */
	@Prop() afHeadingLevel: CalendarWeekViewHeadingLevel =
		CalendarWeekViewHeadingLevel.H2;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-calendar-week-view');

	/**
	 * Vid byte av vecka
	 * @en When week changes
	 */
	@Event() afOnWeekChange: EventEmitter<string>;

	/**
	 * Vid byte av dag
	 * @en When day changes
	 */
	@Event() afOnDateChange: EventEmitter<string>;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	/**
	 * Kan användas för att manuellt sätta om det aktiva datumet. Formatet måste vara 'yyyy-MM-dd'.
	 * @en Can be used to set the active date. The format needs to be 'yyy-MM-dd'.
	 */
	@Method()
	async afMSetActiveDate(activeDate: string) {
		this.selectedDate = format(new Date(activeDate), 'yyyy-MM-dd', {
			locale: svlocale
		});
		this.currentDate = this.selectedDate;
	}

	prevWeek() {
		this.currentDate = subDays(new Date(this.currentDate), 7);
		this.afOnWeekChange.emit('previous');
	}

	nextWeek() {
		this.currentDate = addDays(new Date(this.currentDate), 7);
		this.afOnWeekChange.emit('next');
	}

	initDates() {
		this.minDate = new Date(this.dates[0]);
		this.maxDate = new Date(this.dates[this.dates.length - 1]);
		this.selectedDate = format(new Date(this.dates[0]), 'yyyy-MM-dd', {
			locale: svlocale
		});
		this.currentDate = format(new Date(this.selectedDate), 'yyyy-MM-dd', {
			locale: svlocale
		});
	}

	componentWillLoad() {
		this.afDatesUpdate();
		this.initDates();
	}
	componentDidLoad(){
		this.afOnReady.emit(); 
	}

	get prevWeekText() {
		const nextWeekDate = subDays(new Date(this.currentDate), 7);
		return `${_t.calendar.week} ${getISOWeek(new Date(nextWeekDate))}`;
	}

	get nextWeekText() {
		const nextWeekDate = addDays(new Date(this.currentDate), 7);
		return `${_t.calendar.week} ${getISOWeek(new Date(nextWeekDate))}`;
	}

	get currentWeek() {
		return getISOWeek(new Date(this.currentDate));
	}

	get currentMonth() {
		const firstDate = new Date(this.weekDays[0]);
		const lastDate = new Date(this.weekDays[this.weekDays.length - 1]);
		return firstDate.getMonth() == lastDate.getMonth()
			? format(new Date(firstDate), 'MMMM yyyy', { locale: svlocale })
			: format(new Date(firstDate), 'MMMM yyyy', { locale: svlocale }) +
			' - ' +
			format(new Date(lastDate), 'MMMM yyyy', { locale: svlocale });
	}

	render() {
		return (
			<div class="digi-calendar-week-view">
				<nav class="digi-calendar-week-view__timepicker">
					<div class="digi-calendar-week-view__dates">
						<div class="digi-calendar-week-view__flex">
							<digi-typography>
								<this.afHeadingLevel
									role="status"
									class="digi-calendar-week-view__heading"
								>
									<span class="digi-calendar-week-view__month">{this.currentMonth}</span>
									<span class="digi-calendar-week-view__week">
										{' '}
										v.{this.currentWeek}
									</span>
								</this.afHeadingLevel>
							</digi-typography>
							<ul class="digi-calendar-week-view__date-list">
								{this.weekDays.map((dateItem) => {
									const date = format(new Date(dateItem), 'd', {
										locale: svlocale
									});

									const day = format(new Date(dateItem), 'EEE', {
										locale: svlocale
									});

									const fullDay = format(new Date(dateItem), 'EEEE ', {
										locale: svlocale
									});

									return (
										<li
											class="digi-calendar-week-view__date-list-item"
											aria-current={dateItem === this.todaysDate && 'date'}
										>
											<digi-button
												onClick={() => (this.selectedDate = dateItem)}
												afAriaLabel={`${fullDay} ${date}`}
												aria-hidden={!this.dates.includes(dateItem) ? 'true' : 'false'}
												af-tabindex={!this.dates.includes(dateItem) ? -1 : null}
												aria-pressed={this.selectedDate == dateItem ? 'true' : 'false'}
												class={{
													'digi-calendar-week-view__date-button': true,
													'digi-calendar-week-view__date-button--active':
														this.selectedDate == dateItem,
													'digi-calendar-week-view__date-button--no-value':
														!this.dates.includes(dateItem)
												}}
												af-variation={ButtonVariation.SECONDARY}
											>
												<span
													class="digi-calendar-week-view__date-button-content"
													aria-hidden="true"
												>
													<span
														class={{
															'digi-calendar-week-view__date-button-day':
																this.selectedDate != dateItem,
															'digi-calendar-week-view__date-button-day--active':
																this.selectedDate == dateItem
														}}
													>
														{day}
													</span>
													<span class="digi-calendar-week-view__date-button-date">
														{date}
													</span>
													<span
														aria-hidden="true"
														class={{
															'digi-calendar-week-view__date-button-circle':
																dateItem === this.todaysDate
														}}
													></span>
												</span>
											</digi-button>
										</li>
									);
								})}
							</ul>
						</div>
					</div>

					<digi-button
						onClick={() => this.prevWeek()}
						class={{
							'digi-calendar-week-view__week-button digi-calendar-week-view__week-button--previous':
								true,
							'digi-calendar-week-view__week-button--keep-left':
								this.currentWeek == getISOWeek(this.maxDate),
							'digi-calendar-week-view__week-button--hidden': !(
								this.currentWeek > getISOWeek(this.minDate)
							)
						}}
						af-variation="secondary"
					>
						<digi-icon slot="icon" afName={`chevron-left`}></digi-icon>
						{this.prevWeekText}
					</digi-button>

					<digi-button
						onClick={() => this.nextWeek()}
						class={{
							'digi-calendar-week-view__week-button digi-calendar-week-view__week-button--next':
								true,
							'digi-calendar-week-view__week-button--keep-right':
								this.currentWeek == getISOWeek(this.minDate),
							'digi-calendar-week-view__week-button--hidden': !(
								this.currentWeek < getISOWeek(this.maxDate)
							)
						}}
						af-variation="secondary"
					>
						<digi-icon slot="icon-secondary" afName={`chevron-right`}></digi-icon>
						{this.nextWeekText}
					</digi-button>
				</nav>
				<div
					role="status"
					aria-live="assertive"
					class="digi-calendar-week-view__result"
					id={`${this.afId}-result`}
				>
					<digi-typography>
						<slot></slot>
					</digi-typography>
				</div>
			</div>
		);
	}
}
