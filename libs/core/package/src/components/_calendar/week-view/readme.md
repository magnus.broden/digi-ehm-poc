# digi-calendar-week-view

A calendar component that shows a weekly overview.

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                           | Type                                                                                                                                                                                                             | Default                                        |
| ---------------- | ------------------ | --------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| `afDates`        | `af-dates`         | Lista över valbara datum                                              | `string`                                                                                                                                                                                                         | `''`                                           |
| `afHeadingLevel` | `af-heading-level` | Sätt rubrikens vikt. 'h2' är förvalt.                                 | `CalendarWeekViewHeadingLevel.H1 \| CalendarWeekViewHeadingLevel.H2 \| CalendarWeekViewHeadingLevel.H3 \| CalendarWeekViewHeadingLevel.H4 \| CalendarWeekViewHeadingLevel.H5 \| CalendarWeekViewHeadingLevel.H6` | `CalendarWeekViewHeadingLevel.H2`              |
| `afId`           | `af-id`            | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string`                                                                                                                                                                                                         | `randomIdGenerator('digi-calendar-week-view')` |
| `afMaxWeek`      | `af-max-week`      |                                                                       | `number`                                                                                                                                                                                                         | `undefined`                                    |
| `afMinWeek`      | `af-min-week`      |                                                                       | `number`                                                                                                                                                                                                         | `undefined`                                    |


## Events

| Event            | Description                                                                     | Type                  |
| ---------------- | ------------------------------------------------------------------------------- | --------------------- |
| `afOnDateChange` | Vid byte av dag                                                                 | `CustomEvent<string>` |
| `afOnReady`      | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>`    |
| `afOnWeekChange` | Vid byte av vecka                                                               | `CustomEvent<string>` |


## Methods

### `afMSetActiveDate(activeDate: string) => Promise<void>`

Kan användas för att manuellt sätta om det aktiva datumet. Formatet måste vara 'yyyy-MM-dd'.

#### Returns

Type: `Promise<void>`




## CSS Custom Properties

| Name                                                                       | Description                                                                            |
| -------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| `--digi--calendar-week-view--button--date--background`                     | var(--digi--color--background--primary);                                               |
| `--digi--calendar-week-view--button--date--background--active`             | var(--digi--color--background--complementary-1);                                       |
| `--digi--calendar-week-view--button--date--background--no-value`           | var(--digi--color--background--neutral-6);                                             |
| `--digi--calendar-week-view--button--date--border-color`                   | var(--digi--color--border--secondary);                                                 |
| `--digi--calendar-week-view--button--date--border-color--active`           | var(--digi--color--border--secondary);                                                 |
| `--digi--calendar-week-view--button--date--border-color--no-value`         | var(--digi--color--border--neutral-3);                                                 |
| `--digi--calendar-week-view--button--date--border-radius`                  | 0;                                                                                     |
| `--digi--calendar-week-view--button--date--box-shadow--active`             | inset 0 0 0 0.1875rem var(--digi--color--background--primary);                         |
| `--digi--calendar-week-view--button--date--color`                          | var(--digi--color--text--primary);                                                     |
| `--digi--calendar-week-view--button--date--color--active`                  | var(--digi--color--text--inverted);                                                    |
| `--digi--calendar-week-view--button--date--color--no-value`                | var(--digi--color--text--primary);                                                     |
| `--digi--calendar-week-view--button--date--font-size`                      | var(--digi--button--font-size--small);                                                 |
| `--digi--calendar-week-view--button--date--padding`                        | 16px 10px 25px 10px;                                                                   |
| `--digi--calendar-week-view--button--date--width`                          | 3.75rem;                                                                               |
| `--digi--calendar-week-view--button--date--width--small`                   | 3rem;                                                                                  |
| `--digi--calendar-week-view--button--week--background`                     | var(--digi--color--background--primary);                                               |
| `--digi--calendar-week-view--button--week--border-color`                   | var(--digi--color--border--secondary);                                                 |
| `--digi--calendar-week-view--button--week--display`                        | flex;                                                                                  |
| `--digi--calendar-week-view--button--week--font-size`                      | var(--digi--button--font-size--small);                                                 |
| `--digi--calendar-week-view--button--week--icon--width`                    | var(--digi--gutter--medium);                                                           |
| `--digi--calendar-week-view--button--week--justify-content`                | center;                                                                                |
| `--digi--calendar-week-view--button--week--keep-right--margin`             | 10rem;                                                                                 |
| `--digi--calendar-week-view--button--week--keep-right--margin--small`      | 8.1rem;                                                                                |
| `--digi--calendar-week-view--button--week--margin`                         | 0.125rem;                                                                              |
| `--digi--calendar-week-view--button--week--next-button--border-radius`     | 0 var(--digi--global--border-radius--base) var(--digi--global--border-radius--base) 0; |
| `--digi--calendar-week-view--button--week--padding`                        | calc(var(--digi--gutter--small) + 1px) var(--digi--gutter--largest-2);                 |
| `--digi--calendar-week-view--button--week--previous-button--border-radius` | var(--digi--global--border-radius--base) 0 0 var(--digi--global--border-radius--base); |
| `--digi--calendar-week-view--button--week--width`                          | 9.75rem;                                                                               |
| `--digi--calendar-week-view--button--week--width--small`                   | 7.85rem;                                                                               |
| `--digi--calendar-week-view--dates--gap`                                   | var(--digi--gutter--smaller);                                                          |
| `--digi--calendar-week-view--dates--margin`                                | 0 0 var(--digi--gutter--smaller) 0;                                                    |
| `--digi--calendar-week-view--today-circle--background`                     | var(--digi--color--background--complementary-1);                                       |
| `--digi--calendar-week-view--today-circle--background--active`             | var(--digi--color--text--inverted);                                                    |
| `--digi--calendar-week-view--today-circle--background--no-value`           | var(--digi--color--text--primary);                                                     |
| `--digi--calendar-week-view--today-circle--size`                           | 0.3125rem;                                                                             |


## Dependencies

### Depends on

- [digi-typography](../../_typography/typography)
- [digi-button](../../_button/button)
- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-calendar-week-view --> digi-typography
  digi-calendar-week-view --> digi-button
  digi-calendar-week-view --> digi-icon
  style digi-calendar-week-view fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
