export enum CodeLanguage {
  JSON = 'json',
  CSS = 'css',
  SCSS = 'scss',
  TYPESCRIPT = 'typescript',
  JAVASCRIPT = 'javascript',
  BASH = 'bash',
  HTML = 'html',
  GIT = 'git',
}
