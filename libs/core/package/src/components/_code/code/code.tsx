import { Component, Prop, h, State, Watch } from '@stencil/core';

import { CodeVariation } from './code-variation.enum';
import { CodeLanguage } from './code-language.enum';
import Prism from 'prismjs';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-git';

/**
 * @enums CodeLanguage - code-language.enum.ts
 * @enums CodeVariation - code-variation.enum.ts
 * @swedishName Kod
 */
@Component({
  tag: 'digi-code',
  styleUrls: ['code.scss']
})
export class Code {
  @State() highlightedCode;

  /**
   * En sträng med den kod som du vill visa upp.
   * @en A string of code to use in the code block.
   */
  @Prop() afCode: string;

  /**
   * Sätt färgtemat. Kan vara ljust eller mörkt.
   * @en Set code block variation. This defines the syntax highlighting and can be either light or dark.
   */
  @Prop() afVariation: CodeVariation = CodeVariation.LIGHT;

  /**
   * Sätt programmeringsspråk. 'html' är förvalt.
   * @en Set code language. Defaults to html.
   */
  @Prop() afLanguage: CodeLanguage = CodeLanguage.HTML;

  /**
   * Sätt språk inuti code-elementet. 'en' (engelska) är förvalt.
   * @en Set language inside of code element. Defaults to 'en' (english).
   */
  @Prop() afLang: string = 'en';

  componentWillLoad() {
    Prism.manual = true;
    this.formatCode();
  }

  @Watch('afCode')
  formatCode() {
    if (this.afCode && this.afLanguage) {
      this.highlightedCode = Prism.highlight(
        this.afCode,
        Prism.languages[this.afLanguage],
        this.afLanguage
      );
    }
  }

  get cssModifiers() {
    return {
      'digi-code--light': this.afVariation === CodeVariation.LIGHT,
      'digi-code--dark': this.afVariation === CodeVariation.DARK,
    };
  }

  render() {
    return (
      <span
        class={{
          'digi-code': true,
          ...this.cssModifiers,
        }}
      >
        <code
          class={`digi-code__code language-${this.afLanguage}`}
          lang={this.afLang}
          innerHTML={this.highlightedCode}
        ></code>
      </span>
    );
  }
}
