export enum CodeVariation {
  LIGHT = 'light',
  DARK = 'dark',
}
