export enum CodeExampleVariation {
  LIGHT = 'light',
  DARK = 'dark',
}
