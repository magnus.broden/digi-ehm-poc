# digi-code-example

This component is used to show example usage of code, where the output is visible and the source code is toggleable in a syntax highlighted code block element. It supports dark- and light theme and a number of different languages. The component has a toolbar with buttons for copying the code directly to the clipboard.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { CodeExampleVariation, CodeExampleLanguage } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                 | Description                                                             | Type                                                                                                                                                                                                                                                                                                 | Default                      |
| ---------------------- | ------------------------- | ----------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------- |
| `afCode`               | `af-code`                 | En sträng, eller objekt med strängar, med den kod som du vill visa upp. | `string \| { JSON: string; CSS: string; SCSS: string; Typescript: string; Javascript: string; Bash: string; HTML: string; Git: string; Angular: string; React: string; }`                                                                                                                            | `undefined`                  |
| `afCodeBlockVariation` | `af-code-block-variation` | Sätt färgtemat på kodblocket. Kan vara ljust eller mörkt.               | `CodeBlockVariation.DARK \| CodeBlockVariation.LIGHT`                                                                                                                                                                                                                                                | `CodeBlockVariation.DARK`    |
| `afExampleVariation`   | `af-example-variation`    | Sätt färgtemat på exemplets bakgrund. Kan vara ljust eller mörkt.       | `CodeExampleVariation.DARK \| CodeExampleVariation.LIGHT`                                                                                                                                                                                                                                            | `CodeExampleVariation.LIGHT` |
| `afHideCode`           | `af-hide-code`            |                                                                         | `boolean`                                                                                                                                                                                                                                                                                            | `undefined`                  |
| `afHideControls`       | `af-hide-controls`        |                                                                         | `boolean`                                                                                                                                                                                                                                                                                            | `undefined`                  |
| `afLanguage`           | `af-language`             | Sätt programmeringsspråk. 'html' är förvalt.                            | `CodeExampleLanguage.ANGULAR \| CodeExampleLanguage.BASH \| CodeExampleLanguage.CSS \| CodeExampleLanguage.GIT \| CodeExampleLanguage.HTML \| CodeExampleLanguage.JAVASCRIPT \| CodeExampleLanguage.JSON \| CodeExampleLanguage.REACT \| CodeExampleLanguage.SCSS \| CodeExampleLanguage.TYPESCRIPT` | `CodeExampleLanguage.HTML`   |


## CSS Custom Properties

| Name                               | Description                              |
| ---------------------------------- | ---------------------------------------- |
| `--digi--code-example--background` | var(--digi--color--background--primary); |


## Dependencies

### Depends on

- [digi-icon](../../_icon/icon)
- [digi-code-block](../code-block)
- [digi-form-fieldset](../../_form/form-fieldset)
- [digi-form-radiobutton](../../_form/form-radiobutton)
- [digi-button](../../_button/button)

### Graph
```mermaid
graph TD;
  digi-code-example --> digi-icon
  digi-code-example --> digi-code-block
  digi-code-example --> digi-form-fieldset
  digi-code-example --> digi-form-radiobutton
  digi-code-example --> digi-button
  digi-code-block --> digi-button
  digi-code-block --> digi-icon
  style digi-code-example fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
