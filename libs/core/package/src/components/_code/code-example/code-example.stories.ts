import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { CodeExampleVariation } from './code-example-variation.enum';
import { CodeExampleLanguage } from './code-example-language.enum';
import { CodeBlockVariation } from '../code-block/code-block-variation.enum';

export default {
	title: 'code/digi-code-example',
	argTypes: {
		'af-code-block-variation': enumSelect(CodeBlockVariation),
		'af-example-variation': enumSelect(CodeExampleVariation),
		'af-language': enumSelect(CodeExampleLanguage)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-code-example',
	'af-code': `<digi-button 
 af-variation="primary"
>I am a primary button</digi-button>`,
	'af-code-block-variation': CodeBlockVariation.DARK,
	'af-example-variation': CodeExampleVariation.LIGHT,
	'af-language': CodeExampleLanguage.HTML,
	'af-hide-controls': true,
	children: `
	<div slot='controls' class="slot__controls">
		<digi-form-fieldset af-name='Variations' af-legend='Variations'>
			<digi-form-radiobutton af-checked af-label='Primary' af-name='Variations' af-value='primary'></digi-form-radiobutton>
			<digi-form-radiobutton af-label='Secondary' af-name='Variations' af-value='secondary'></digi-form-radiobutton>
		</digi-form-fieldset>
	</div>
	<div><digi-button>I am a primary button</digi-button></div>
	`
};
