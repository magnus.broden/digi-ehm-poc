export enum CodeBlockVariation {
  LIGHT = 'light',
  DARK = 'dark',
}
