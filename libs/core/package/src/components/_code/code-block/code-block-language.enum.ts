export enum CodeBlockLanguage {
  JSON = 'json',
  CSS = 'css',
  SCSS = 'scss',
  TYPESCRIPT = 'typescript',
  JAVASCRIPT = 'javascript',
  BASH = 'bash',
  HTML = 'html',
  GIT = 'git',
  JSX = 'jsx',
  TSX = 'tsx'
}
