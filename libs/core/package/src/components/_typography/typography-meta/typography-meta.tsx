import { Component, h, Prop } from '@stencil/core';
import { TypographyMetaVariation } from './typography-meta-variation.enum';

/**
 * @slot default - Kan användas för olika element, ett text element är föredraget.
 * @slot secondary - Kan användas för olika element, ett text element är föredraget.
 *
 * @enums TypographyMetaVariation - typography-meta-variation.enum.ts
 * @swedishName Metatext
 */
@Component({
	tag: 'digi-typography-meta',
	styleUrls: ['typography-meta.scss'],
	scoped: true
})
export class TypographyMeta {
	/**
	 * Sätter variant. Kan vara primär eller sekundär.
	 * @en Set variation. Can be primary or secondary.
	 */
	@Prop() afVariation: `${TypographyMetaVariation}` =
		TypographyMetaVariation.PRIMARY;

	get cssModifiers() {
		return {
			[`digi-typography-meta--variation-${this.afVariation}`]: !!this.afVariation
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-typography-meta': true,
					...this.cssModifiers
				}}
			>
				<div class="digi-typography-meta__meta">
					<slot></slot>
				</div>
				<div class="digi-typography-meta__meta-secondary">
					<slot name="secondary"></slot>
				</div>
			</div>
		);
	}
}
