import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { TypographyMetaVariation } from './typography-meta-variation.enum';

export default {
	title: 'typography/digi-typography-meta',
	argTypes: {
		'af-variation': enumSelect(TypographyMetaVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-typography-meta',
	'af-variation': TypographyMetaVariation.PRIMARY,
	children: `
    <p>I am the label text</p>
    <p slot="secondary">Det här är en sekundär text</p>`
};
