export enum TypographyMetaVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
