import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'typography/digi-typography-preamble'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-typography-preamble',
	children:
		'I am a preamble. Use me right after headings. Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
};
