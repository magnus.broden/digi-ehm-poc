# digi-typography-preamble

This creates a preamble. It's as basic as it gets (it doesn't have any props at all). Even though it is possible to use any element types as children, it is much preferred to use only a text-node, since the component itself wraps everything in a `<p>`-tag.

<!-- Auto Generated Below -->


## Slots

| Slot        | Description           |
| ----------- | --------------------- |
| `"default"` | Should be a text node |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
