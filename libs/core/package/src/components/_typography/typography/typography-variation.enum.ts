export enum TypographyVariation {
  SMALL = 'small',
  LARGE = 'large'
}
