import { Component, Prop, h } from '@stencil/core';
import { TypographyVariation } from './typography-variation.enum';

/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums TypographyVariation - typography-variation.enum.ts
 * @swedishName Typografi
 */
@Component({
  tag: 'digi-typography',
  styleUrls: ['typography.scss'],
})
export class Typography {
  /**
   * Sätter variant. Kan vara 'small' eller 'large'
   * @en Set variation. Can be 'small' or 'large'
   */
  @Prop() afVariation: TypographyVariation = TypographyVariation.SMALL;

  get cssModifiers() {
    return {
      'digi-typography--large': this.afVariation === TypographyVariation.LARGE,
      'digi-typography--small': this.afVariation === TypographyVariation.SMALL,
    };
  }

  render() {
    return (
      <div
        class={{
          'digi-typography': true,
          ...this.cssModifiers,
        }}
      >
        <slot></slot>
      </div>
    );
  }
}
