# digi-typography

This is a general typography component. It can be small (default) or large. Use this to make sure your articles etc maintain a consistent typography.

## Enums

```ts
import { TypographyVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                    | Type                                                     | Default                     |
| ------------- | -------------- | ---------------------------------------------- | -------------------------------------------------------- | --------------------------- |
| `afVariation` | `af-variation` | Sätter variant. Kan vara 'small' eller 'large' | `TypographyVariation.LARGE \| TypographyVariation.SMALL` | `TypographyVariation.SMALL` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## CSS Custom Properties

| Name                                           | Description                        |
| ---------------------------------------------- | ---------------------------------- |
| `--digi--typography--color--text`              | var(--digi--color--text--primary); |
| `--digi--typography--h1--margin--large`        | var(--digi--margin--h1-large);     |
| `--digi--typography--h1--margin--medium`       | var(--digi--margin--h1-medium);    |
| `--digi--typography--h1--margin--small`        | var(--digi--margin--h1-small);     |
| `--digi--typography--h2--margin--large`        | var(--digi--margin--h2-large);     |
| `--digi--typography--h2--margin--small`        | var(--digi--margin--h2-small);     |
| `--digi--typography--h3--margin--large`        | var(--digi--margin--h3-large);     |
| `--digi--typography--h3--margin--small`        | var(--digi--margin--h3-small);     |
| `--digi--typography--h4--margin--large`        | var(--digi--margin--h4-h6-large);  |
| `--digi--typography--h4--margin--small`        | var(--digi--margin--h4-h6-small);  |
| `--digi--typography--h5--margin--small`        | var(--digi--margin--h4-h6-small);  |
| `--digi--typography--h6--margin--large`        | var(--digi--margin--h4-h6-large);  |
| `--digi--typography--paragraph--margin--large` | var(--digi--margin--text-large);   |
| `--digi--typography--paragraph--margin--small` | var(--digi--margin--text-small);   |
| `--digi--typography--paragraph--margin-top`    | initial;                           |


## Dependencies

### Used by

 - [digi-bar-chart](../../_chart/bar-chart)
 - [digi-calendar-week-view](../../_calendar/week-view)
 - [digi-chart-line](../../_chart/chart-line)
 - [digi-expandable-accordion](../../_expandable/expandable-accordion)
 - [digi-progress-step](../../_progress/progress-step)
 - [digi-progress-steps](../../_progress/progress-steps)

### Graph
```mermaid
graph TD;
  digi-bar-chart --> digi-typography
  digi-calendar-week-view --> digi-typography
  digi-chart-line --> digi-typography
  digi-expandable-accordion --> digi-typography
  digi-progress-step --> digi-typography
  digi-progress-steps --> digi-typography
  style digi-typography fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
