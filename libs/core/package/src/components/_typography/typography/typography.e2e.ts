import { newE2EPage } from '@stencil/core/testing';

describe('digi-typography', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-typography></digi-typography>');

    const element = await page.find('digi-typography');
    expect(element).toHaveClass('hydrated');
  });
});
