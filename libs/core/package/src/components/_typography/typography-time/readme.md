# digi-typography-time

This creates a formated time and includes three different time variation formats. Head over to the Knobs-tab to see them in action.

This is a basic component and does not include any styling.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { TypographyTimeVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                  | Attribute      | Description                                                                                       | Type                                  | Default                           |
| ------------------------- | -------------- | ------------------------------------------------------------------------------------------------- | ------------------------------------- | --------------------------------- |
| `afDateTime` _(required)_ | `af-date-time` | Ska vara ett datumobjekt, datumsträng eller ett datumformatterat nummer                           | `Date \| number \| string`            | `undefined`                       |
| `afVariation`             | `af-variation` | Sätter variant. Bestämmer hur det ska formatteras. Kan vara 'primary', 'pretty' eller 'distance'. | `"distance" \| "pretty" \| "primary"` | `TypographyTimeVariation.PRIMARY` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
