import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { TypographyTimeVariation } from './typography-time-variation.enum';

export default {
	title: 'typography/digi-typography-time',
	argTypes: {
		'af-variation': enumSelect(TypographyTimeVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-typography-time',
	'af-date-time': null,
	'af-variation': TypographyTimeVariation.PRIMARY,
	children: ''
};
