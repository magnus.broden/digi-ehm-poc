import { newE2EPage } from '@stencil/core/testing';

describe('digi-typography-time', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-typography-time></digi-typography-time>');

    const element = await page.find('digi-typography-time');
    expect(element).toHaveClass('hydrated');
  });
});
