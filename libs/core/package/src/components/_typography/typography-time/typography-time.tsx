import { _t } from '@digi/shared/text';
import { Component, h, Prop, State, Watch } from '@stencil/core';
import { TypographyTimeVariation } from './typography-time-variation.enum';

/**
 * @enums TypographyTimeVariation - typography-time-variation.enum.ts
 *@swedishName Datum
 */
@Component({
	tag: 'digi-typography-time',
	scoped: true
})
export class TypographyTime {
	@State() formatedDate: string;
	@State() datetime: string;

	/**
	 * Ska vara ett datumobjekt, datumsträng eller ett datumformatterat nummer
	 * @en Set the date time to a Date object, date string or a valid date formatted number.
	 */
	@Prop() afDateTime!: number | Date | string;

	/**
	 * Sätter variant. Bestämmer hur det ska formatteras. Kan vara 'primary', 'pretty' eller 'distance'.
	 * @en Set variation. Can be 'primary', 'pretty' or 'distance'.
	 */
	@Prop() afVariation: `${TypographyTimeVariation}` =
		TypographyTimeVariation.PRIMARY;

	primaryFormat(date: Date) {
		return date.toLocaleString('default', {
			year: 'numeric',
			month: '2-digit',
			day: '2-digit'
		});
	}

	prettyFormat(date: Date) {
		return date.toLocaleString('default', {
			day: 'numeric',
			month: 'long',
			year: 'numeric'
		});
	}

	distanceFormat(date: Date) {
		const oneDay = 1000 * 3600 * 24;
		const distance = Math.floor(Date.now() - date.getTime());

		let days = Math.floor(distance / oneDay);
		days = Math.abs(days);

		if (distance > oneDay) {
			return _t.time.days_ago(days);
		}

		if (distance < oneDay) {
			return days === 0 ? _t.today : _t.time.in_days(days);
		}
	}

	@Watch('afDateTime')
	formatDate(dateToFormat: number | Date | string) {
		const date = new Date(dateToFormat);

		switch (this.afVariation) {
			case TypographyTimeVariation.PRIMARY:
				this.formatedDate = this.primaryFormat(date);
				break;
			case TypographyTimeVariation.PRETTY:
				this.formatedDate = this.prettyFormat(date);
				break;
			case TypographyTimeVariation.DISTANCE:
				this.formatedDate = this.distanceFormat(date);
				break;
		}
		this.datetime = this.primaryFormat(date);
	}

	componentWillLoad() {
		this.formatDate(this.afDateTime);
	}

	render() {
		return (
			<time dateTime={this.datetime} class="digi-typography-time">
				{this.formatedDate}
			</time>
		);
	}
}
