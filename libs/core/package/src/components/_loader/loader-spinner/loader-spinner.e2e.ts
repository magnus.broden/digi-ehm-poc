import { newE2EPage } from '@stencil/core/testing';

describe('loader-spinner', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<loader-spinner></loader-spinner>');
		const element = await page.find('loader-spinner');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<loader-spinner></loader-spinner>');
		const component = await page.find('loader-spinner');
		const element = await page.find('loader-spinner >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
