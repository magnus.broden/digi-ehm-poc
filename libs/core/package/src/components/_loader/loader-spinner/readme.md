# digi-loader-spinner

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                    | Type                                                                             | Default                    |
| -------- | --------- | ---------------------------------------------- | -------------------------------------------------------------------------------- | -------------------------- |
| `afSize` | `af-size` | Sätter spinnerns storlek. 'medium' är förvalt. | `LoaderSpinnerSize.LARGE \| LoaderSpinnerSize.MEDIUM \| LoaderSpinnerSize.SMALL` | `LoaderSpinnerSize.MEDIUM` |
| `afText` | `af-text` | Sätter spinnerns text.                         | `string`                                                                         | `undefined`                |


## CSS Custom Properties

| Name                                         | Description                                                    |
| -------------------------------------------- | -------------------------------------------------------------- |
| `--digi--loader-spinner--icon--size--large`  | 5rem;                                                          |
| `--digi--loader-spinner--icon--size--medium` | 2rem;                                                          |
| `--digi--loader-spinner--icon--size--small`  | 1rem;                                                          |
| `--digi-loader-spinner--font--size--large`   | var(--digi--typography--heading-4--font-size--desktop-large);  |
| `--digi-loader-spinner--font--size--medium`  | var(--digi--typography--heading-4--font-size--desktop);        |
| `--digi-loader-spinner--font--size--small`   | var(--digi--typography--heading-4--font-size--desktop-xsmall); |


## Dependencies

### Depends on

- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-loader-spinner --> digi-icon
  style digi-loader-spinner fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
