import { newSpecPage } from '@stencil/core/testing';
import { ChartLine } from './chart-line';

describe('chart-line', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [ChartLine],
			html: '<chart-line></chart-line>'
		});
		expect(root).toEqualHtml(`
      <chart-line>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </chart-line>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [ChartLine],
			html: `<chart-line first="Stencil" last="'Don't call me a framework' JS"></chart-line>`
		});
		expect(root).toEqualHtml(`
      <chart-line first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </chart-line>
    `);
	});
});
