export interface ChartLineSeries {
	yValues: number[];
	title: string;
	colorToken?;
}