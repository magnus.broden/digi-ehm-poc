import { _t } from '@digi/shared/text';
import { Element, Component, h, Prop, Watch } from '@stencil/core';
import * as d3 from 'd3';
import { FormatLocaleDefinition } from 'd3';
import { randomIdGenerator } from 'libs/core/package/src/global/utils/randomIdGenerator';
import { ChartLineData } from './chart-line-data.interface';
import { ChartLineSeries } from './chart-line-series.interface';

/**
 * @slot mySlot - Slot description, if any
 *
 * @enums myEnum - chart-line-myEnum.enum.ts
 *
 * @swedishName Linjediagram
 */
@Component({
	tag: 'digi-chart-line',
	styleUrl: 'chart-line.scss',
	scoped: true
})
export class ChartLine {
	@Element() element: HTMLElement;

	// Handles to html components
	private _legendDiv: HTMLDivElement;
	private _chartDiv: any;
	private _host: any;

	// Chart specific
	private _yAxis: any;
	private _xAxis: any;
	private _lineChartContainer: any;
	private _maxVal: number;
	private _lineTypes: string[] = [null, "1.0, 7.0", "9.0, 7.0"];

	// Tooltip
	private _tooltip: any;
	private _Tipbox: any;
	private _TooltipLine: any;
	private _Circles: any;

	// Positioning of the tooltip
	private _hoverPoint: number;
	private _tooltipPoint: number;
	private _toggled: boolean = false;

	// Color function
	private _Color: Function;

	// Aux
	private _chartData: ChartLineData;
	@Prop() afHeadingLevel: string;
	@Prop() afId: string = randomIdGenerator('tooltip');
	@Prop({ mutable: true }) afChartData: string | ChartLineData;
	private _secondaryLine: d3.Selection<SVGLineElement, unknown, null, undefined>;
	private _svg: d3.Selection<SVGSVGElement, unknown, null, undefined>;
	private _xAxisHandle: any;
	private _yAxisHandle: any;
	private _lineScale: d3.ScaleLinear<number, number, never>;
	private _tooltipDataIndex: number;
	private _tooltipHeading: string;

	private _tickSize: number = 0;
	private _maxHeight: number;

	// Selection list from legend chips
	private _selectedLines: any[] = [];
	// Contains all original series that are sent to this component
	private _coreSeries: ChartLineSeries[];

	// Dragging
	private _dragStart: number = 0;
	private _dragDir: number = 0;
	private _scrollLeft: number;
	private _tableDiv: HTMLDivElement;
	private _prevHeight: string;
	private _LineFunc: d3.Line<[number, number]>;
	private _loading: boolean = false;
	private _titleDiv: HTMLDivElement;
	private _status: HTMLDivElement;


	@Watch('afChartData')
	afChartDataUpdate() {
		let data = this.afChartData;
		if (typeof data === 'object') {
			this._chartData = { ...data };
			return;
		}

		this._chartData = JSON.parse(data);
		// Making deep copy of this
		if (this._chartData.data)
			this._coreSeries = (JSON.parse(JSON.stringify(this._chartData.data.series)));
	}

	// Dimensions, margins and font sizes
	private dims: DOMRect;
	margin = {
		bottom: 0,
		top: 3,
		left: 5,
		right: 15
	};
	fontSize: string = '0.875rem';
	xPadding: number = 5;
	yPadding: number = 8;
	xTickSize: number = 5;
	// How many line types that exists currently
	private lineTypes = 3;

	// Formats
	private formatStyle = ',d';
	axisNumberFormat: FormatLocaleDefinition = {
		decimal: ',',
		thousands: ' ',
		grouping: [3],
		currency: ['', 'kr']
	};

	// Observer for watching size changes when in line chart mode
	lineChartObserver = new ResizeObserver(entries => {
		entries.forEach(() => {
			this.resizeSvg();
			this.resizeLines();
			this.repositionTooltip(); // Called on both div.resize, and window.resize
			d3.select(this._legendDiv).style('width', (this.dims.width) + 'px')
		});
	});

	// Observer for watching size changes when in table mode
	tableObserver = new ResizeObserver(entries => {
		entries.forEach(() => {
			d3.select(this._legendDiv).style('width', (this.dims.width) + 'px')
			this.resizeTable();
		});
	});

	initSvg() {
		this.margin.bottom = (Number(window.getComputedStyle(document.body).getPropertyValue('font-size').match(/\d+/)[0]) * Number(this.fontSize.replace(/[^\d.]|\.(?=.*\.)/g, ''))) + this.xPadding + this.xTickSize;
		this.margin.top = ((Number(window.getComputedStyle(document.body).getPropertyValue('font-size').match(/\d+/)[0]) * Number(this.fontSize.replace(/[^\d.]|\.(?=.*\.)/g, '')))) / 2 + this.margin.top;

		this.lineChartObserver.observe(this._chartDiv);

		d3.select(this._chartDiv).select('svg').remove();
		// Initialize an empty div, that we can place info in for tooltip
		// Styling for tooltip is in .scss file
		this._tooltip = d3
			.select("#" + this.afId);

		this._tooltip.select('.close').on("click", this.closeTooltip)

		this._tooltipHeading = 'h' + (parseInt(this.afHeadingLevel.replace(/\D/g, "")) + 1);
		// Add heading placeholder for heading
		this._tooltip.selectAll(".tooltipBody")
			.append(this._tooltipHeading)	// increment heading level by one
			.style('margin', '0 0.75rem')
			.style('font-weight', '500');

		this._maxHeight = d3.select(this._host).node().parentNode.getBoundingClientRect().height;

		this.setDims();
		this.setTickSize();
		this.checkPercentage();

		d3.select(this._chartDiv).attr("height", this.dims.height + 'px');

		// Appending svg
		this._svg = d3.select(this._chartDiv).append('svg')
			.attr('aria-hidden', true)
			.attr('role', 'img')
			.attr('aria-label', this._chartData && this._chartData.title ? 'Linjediagram om ' + this._chartData.title.toLowerCase() : 'Linjediagram');

		this._svg.attr('width', this.dims.width).attr('height', this.dims.height);

		this.mapColorToLine();

		this._TooltipLine = this._svg
			.append('line')
			.attr('id', 'toolTipLine' + this.afId);

		this._secondaryLine = this._svg
			.append('line')
			.attr('id', 'secondaryLine' + this.afId);

		this.computeMaxVal();

		this.initYAxis();

		this.initXAxis();

		this._Tipbox = this._svg
			.append('rect')
			.attr('class', 'svgRect')
			.attr('width', this.dims.width - (this.margin.left / 2))
			.attr('height', this.dims.height - this.margin.top - this.margin.bottom)
			.attr('opacity', 0)
			.attr('transform', `translate(${this.margin.left / 2},0)`)

		this.addListenersToChart();

		this._Circles = this._svg.append('g').attr('class', 'circleGroup').on('mousemove', this.drawTooltipLine);

		this._Circles
			.selectAll('circle')
			.data(this._chartData.data.series)
			.join('circle')
			.attr('stroke', (d: any) => this.colorFunction(d))
			.attr('fill', 'white')
			.attr('stroke-width', '1px')
			.attr('r', 7)
			.attr('display', 'none');

		d3.select(window).on("resize." + this.afId, this.repositionTooltip)

	}

	// Initializing chart with empty dataset
	initEmpty() {
		this.margin.bottom = (Number(window.getComputedStyle(document.body).getPropertyValue('font-size').match(/\d+/)[0]) * Number(this.fontSize.replace(/[^\d.]|\.(?=.*\.)/g, ''))) + this.xPadding + this.xTickSize;
		this.margin.top = ((Number(window.getComputedStyle(document.body).getPropertyValue('font-size').match(/\d+/)[0]) * Number(this.fontSize.replace(/[^\d.]|\.(?=.*\.)/g, '')))) / 2 + this.margin.top;

		d3.select(this._chartDiv).select('svg').remove();
		// Initialize an empty div, that we can place info in for tooltip
		// Styling for tooltip is in .scss file
		this._tooltip = d3
			.select("#" + this.afId);

		d3.select('digi-icon-x').on("click", this.closeTooltip)

		this._tooltipHeading = 'h' + (parseInt(this.afHeadingLevel.replace(/\D/g, "")) + 1);
		// Add heading placeholder for heading
		this._tooltip.selectAll(".tooltipBody")
			.append(this._tooltipHeading)	// increment heading level by one
			.style('margin', '0 0.75rem')
			.style('font-weight', '500');

		this._maxHeight = d3.select(this._host).node().parentNode.getBoundingClientRect().height;

		this.setDims();

		d3.select(this._chartDiv).attr("height", this.dims.height + 'px');

		this._svg = d3.select(this._chartDiv).append('svg').attr('aria-hidden', true);
		this._svg.attr('width', this.dims.width).attr('height', this.dims.height);

		this._TooltipLine = this._svg
			.append('line')
			.attr('id', 'toolTipLine' + this.afId);

		this._secondaryLine = this._svg
			.append('line')
			.attr('id', 'secondaryLine' + this.afId);

		this.initYAxis(true);
		this.initXAxis(true);

		this._Tipbox = this._svg
			.append('rect')
			.attr('class', 'svgRect')
			.attr('width', this.dims.width - this.margin.left / 2)
			.attr('height', this.dims.height - this.margin.top - this.margin.bottom)
			.attr('opacity', 0)
			.attr('transform', `translate(${this.margin.left / 2},0)`)

		this._Circles = this._svg.append('g').attr('class', 'circleGroup').on('mousemove', this.drawTooltipLine);

		this.defineLineAndScale();

		this.lineChartObserver.observe(this._chartDiv);

		this.loadingState(true);
	}

	loadingState(empty?: boolean) {

		this._loading = true;
		d3.select('.tableButton').style('display', 'none');
		const duration: number = 400;
		const LineFunc = d3.line()
			.defined((d: any) => d[1] != null)
			.x((d: any) => this._lineScale(d[0]))
			.y((d: any) => this._yAxis(d[1]))
			.curve(d3.curveBasis);

		// close potentially open tooltip
		this.closeTooltip();

		// Remove hover listeners from chart
		this._Tipbox.style('cursor', 'wait')
			.on('mousemove', null)
			.on('touchmove', null)
			.on('mouseup', null)
			.on('mouseleave', null);

		d3.select(this._titleDiv).select(this.afHeadingLevel).html(`${_t.loading}...`);

		const height = d3.select(this._titleDiv).style('height');
		d3.select(this._titleDiv)
			.style('height', this._prevHeight)
			.transition('adjust')
			.duration(400)
			.style('height', height)
			.on('end', () => d3.select(this._titleDiv).style('height', null))

		d3.select(this._legendDiv).style('display', 'none')

		let animateRandomLine = () => {
			let firstVal: number = this._xAxis.domain()[0];
			const randomData: number[][] = [...Array(this._xAxis.domain()[1])].map(() => {
				const dataPoint = [firstVal, Math.round((Math.random() * this._yAxis.domain()[1]) * 100) / 100] // This assumes domain is from 0 to maxVal
				firstVal++;
				return dataPoint;
			})

			this._lineChartContainer.selectAll('.linePath').remove();
			this._lineChartContainer.selectAll('linePath')
				.data([randomData])
				.join('path')
				.attr('class', 'linePath')
				.attr('stroke', 'lightgray')
				.attr("stroke-linecap", "round")
				.attr('stroke-width', 4)
				.attr('fill', 'none')
				.attr('d', (d: any) => LineFunc(d))
				.transition()
				.duration(duration * 3)
				.ease(d3.easeQuadInOut)
				.attrTween("stroke-dasharray", function () {
					const length = this.getTotalLength();
					return d3.interpolate(`0,${length}`, `${length},${length}`);
				})
				.on('end', () => {
					this._lineChartContainer.selectAll('.linePath').attr("stroke-dasharray", function () {
						const length = this.getTotalLength();
						return length + " " + length;
					})
						.attr("stroke-dashoffset", 0)
						.transition().duration(duration * 3).ease(d3.easeQuadInOut)
						.attr("stroke-dashoffset", function () {
							return -this.getTotalLength();
						})
						.on('end', () => animateRandomLine())
				})
		}

		let flattenLines = () => {
			const data: any = this._lineChartContainer.selectAll('.linePath').data().map((elem: any) => {
				elem.line = elem.line.map((point: any) => {
					return point[1] ? [point[0], this._yAxis.domain()[0]] : [point[0], undefined];
				})
				return elem;
			});

			this._lineChartContainer.selectAll('.linePath')
				.data(data)
				.transition()
				.duration(duration)
				.ease(d3.easePolyInOut)
				.attr('d', (d: any) => this._LineFunc(d.line))
				.on('end', (...[, i]: any) => i === 0 ? animateRandomLine() : null)

			this._lineChartContainer.selectAll('.symbolGroup')
				.selectAll('.symbol')
				.transition().duration(duration).ease(d3.easeLinear)
				.attr('transform', (d: any) => d ? `translate(${this._lineScale(d.x)},${this._yAxis(this._yAxis.domain()[0])})` : null)
				.attr('opacity', 0);
		}
		// Start loadingstate, either from empty set or last set
		if (empty)
			animateRandomLine();
		else
			flattenLines();
	}

	updateSvg() {
		// displaying legend again
		if (this._loading) {
			d3.select('.tableButton').style('display', null);
			d3.select(this._legendDiv).style('display', null);
			d3.select(this._titleDiv).style('height', null);
		}

		if (this._chartData.data.xValueNames)
			this.margin.right = (this.getTextSize(this._chartData.data.xValueNames[this._chartData.data.xValueNames.length - 1]) / 2) + 1;
		else
			this.margin.right = this.getTextSize(d3.max(this._chartData.data.xValues).toString()) / 2 + 1;

		this.checkPercentage();
		this.setDims(true);
		this.mapColorToLine();
		this.computeMaxVal();
		this.setTickSize();

		d3.select(this._chartDiv).attr("height", this.dims.height + 'px');

		// Updating Svg attributes
		this._svg.attr('width', this.dims.width + 'px')
			.attr('height', this.dims.height + 'px')
			.attr('aria-label', this._chartData && this._chartData.title ? 'Linjediagram om ' + this._chartData.title.toLowerCase() : 'Linjediagram');

		// Y Axis
		this._yAxis = d3
			.scaleLinear()
			.domain([0, this._maxVal])
			.nice(3)
			.range([this.dims.height - this.margin.bottom, this.margin.top]);

		// Updating margins
		this.margin.left = this.getTextSize(d3.formatLocale(this.axisNumberFormat).format(this.formatStyle)(this._yAxis.domain()[1])) + this.yPadding + 3;


		/****  Update xAxis ****/
		const oldDomain = this._xAxis.domain();

		this.initXAxis();

		this._lineChartContainer
			.transition('position')
			.duration(400)
			.ease(d3.easePolyInOut)
			.attr('width', this.dims.width).attr('height', this.dims.height)
			.attr('transform', `translate(${this.margin.left},0)`);

		this.setYTicks();

		this.defineLineAndScale();

		//this.leftSetAxis();

		this.addSymbols(true);

		if (this._loading) {

			d3.select('.tableButton').style('display', null);
			this.addListenersToChart();

			const height = d3.select(this._titleDiv).style('height');
			d3.select(this._titleDiv)
				.style('height', this._prevHeight)
				.transition('adjust')
				.duration(400)
				.style('height', height)
				.on('end', () => d3.select(this._titleDiv).style('height', null))

			d3.select(this._legendDiv).style('opacity', 0)
				.transition()
				.duration(400)
				.ease(d3.easePolyInOut)
				.style('opacity', 1);

			this._lineChartContainer
				.selectAll('.linePath').on('end', null);

			// Setting starting state for symbols
			this._lineChartContainer.selectAll('.symbolGroup')
				.selectAll('.symbol')
				.attr('opacity', 0)
				.attr('transform', (d: any) => d ? `translate(${this._lineScale(d.x)},${this._yAxis(this._yAxis.domain()[0])})` : null)

			this._lineChartContainer
				.selectAll('.linePath')
				.transition().duration(400).ease(d3.easeQuadInOut)
				.attr("stroke-dashoffset", function () {
					return -this.getTotalLength();
				})
				.on('end', () => {

					this._lineChartContainer
						.selectAll('.linePath')
						.data(this.reshapeData(this._chartData.data.series))
						.join('path')
						.attr('class', 'linePath')
						.attr('stroke', (d: any) => this.colorFunction(d))
						.attr("stroke-linecap", "round")
						.attr('stroke-width', 4)
						.attr('fill', 'none')
						.attr('stroke-dasharray', this.dashFunction)
						.attr("stroke-dashoffset", null)
						.attr('d', (d: any) => this._LineFunc(d.line.map((pair: [number, number]) => [pair[0], this._yAxis.domain()[0]])))

					// Transitioning in lines
					this._lineChartContainer
						.selectAll('.linePath').transition()
						.duration(400)
						.ease(d3.easePolyInOut)
						.attr('d', (d: any) => this._LineFunc(d.line));

					// Transitioning in symbols
					this._lineChartContainer.selectAll('.symbolGroup')
						.selectAll('.symbol')
						.transition().duration(400).ease(d3.easePolyInOut)
						.attr('transform', (d: any) => d ? `translate(${this._lineScale(d.x)},${this._yAxis(d.val)})` : null)
						.attr('opacity', 1)

					this._loading = false;
				})

		}
		else {
			this._lineChartContainer
				.selectAll('.linePath')
				.data(this.reshapeData(this._chartData.data.series))
				.join('path')
				.transition()
				.duration(400)
				.ease(d3.easePolyInOut)
				.attr('class', 'linePath')
				.attr('stroke', (d: any) => this.colorFunction(d))
				.attr("stroke-linecap", "round")
				.attr('stroke-width', (d: any) => d.length > 1 ? 4 : 6)
				.attr('fill', 'none')
				.attr('stroke-dasharray', (d: any, i: number) => d.length > 1 ? this.dashFunction(d, i) : null)
				.attr('d', (d: any) => this._LineFunc(d.line));
		}

		// If we have the exact same domain, the tooltip may continue to be active
		if (this._toggled && oldDomain === this._xAxis.domain()) {
			const stringPoint = this._chartData.data.xValues[this._tooltipDataIndex].toString();
			this._Circles
				.selectAll('circle')
				.data(this._chartData.data.series)
				.join('circle')
				.transition('New circles')
				.duration(400)
				.ease(d3.easePolyInOut)
				.attr('stroke', 'white')
				.attr('fill', (d: any) => this.colorFunction(d))
				.attr('stroke-width', '1px')
				.attr('r', 7)

			this.positionCircles(stringPoint, 400);

			const oldPos = this._TooltipLine.attr('y1');

			this._TooltipLine
				.transition()
				.duration(400)
				.ease(d3.easePolyInOut)
				.attr('x1', this._xAxis(stringPoint) + this.margin.left)
				.attr('x2', this._xAxis(stringPoint) + this.margin.left)
				.attr('y1', this._yAxis(this._maxVal))
				.attr('y2', this.dims.height - this.margin.bottom);

			let tooltipBody = this._tooltip.select(".tooltipBody");
			tooltipBody.select(this._tooltipHeading)
				.style('margin', '0 0.75rem')
				.html(() => {
					if (this._chartData.data.xValueNames)
						return this._chartData.data.xValueNames[this._tooltipPoint - 1];
					return this._tooltipPoint;
				});

			// Body
			tooltipBody
				.selectAll('div')
				.data(this._chartData.data.series)
				.join('div')
				.style("display", "flex")
				.style("flex-direction", "row")
				.style("flex-wrap", "nowrap")
				.style("white-space", "nowrap")
				.style("justify-content", "space-between")
				.html((d: any) => {
					return '<span>' + d.title + ':&nbsp</span><span class="tooltipVal">' + d.yValues[this._tooltipDataIndex] + '</span>';
				});

			this.positionTooltipWithinBounds(this._xAxis(stringPoint) + this.margin.left, 400);

			this._tooltip
				.style('transform', () => {
					// We reset the position of the tooltip, and adjust for the maximum value, -1px for it to align perfectly
					return `translateY(${oldPos - this._tooltip.node().getBoundingClientRect().height - 9}px)`
				})
				.transition()
				.duration(400)
				.ease(d3.easePolyInOut)
				.style('transform', () => {
					// We reset the position of the tooltip, and adjust for the maximum value, -1px for it to align perfectly
					return `translateY(${this._yAxis(this._maxVal) - this._tooltip.node().getBoundingClientRect().height - 9}px)`
				})

			this._svg.select('.tooltipPointer')
				.transition()
				.duration(400)
				.ease(d3.easePolyInOut)
				.attr("transform", `translate(${this._xAxis(stringPoint) + this.margin.left},${this._yAxis(this._maxVal) - 7}) rotate(180) scale(1.5,1.0)`);
		}
		else {
			this._Circles
				.selectAll('circle')
				.data(this._chartData.data.series)
				.join('circle')
				.attr('stroke', (d: any) => this.colorFunction(d))
				.attr('fill', 'white')
				.attr('stroke-width', '1px')
				.attr('r', 7)
				.attr('display', 'none');

			this.closeTooltip();
		}

	}

	private dragStart = (e: any) => {
		const container = d3.select(this._legendDiv).node();
		this._dragStart = e.x - container.offsetLeft;
		this._scrollLeft = container.scrollLeft;
	}

	private drag = (e: any) => {
		const container = d3.select(this._legendDiv);
		this._dragDir = e.x;
		const delta = this._dragDir - container.node().offsetLeft;
		const walk = (delta - this._dragStart) * 1; //scroll-fast
		container.property("scrollLeft", this._scrollLeft - walk)
	}

	// Set yTicks
	private setYTicks = (init = false) => {

		// Fixed number of y-axis lines
		var step =
			this._chartData.meta && this._chartData.meta.numberOfReferenceLines
				? this._chartData.meta.numberOfReferenceLines
				: 5,
			min = this._yAxis.domain()[0],
			max = this._yAxis.domain()[1],
			stepValue = (max - min) / (step - 1),
			tickValues = d3.range(min, max + stepValue, stepValue);

		if (!init) {
			this._yAxisHandle
				.style('font-size', this.fontSize)
				.transition()
				.duration(400)
				.call(
					d3
						.axisLeft(this._yAxis)
						.ticks(step)
						.tickPadding(this.yPadding)
						.tickValues(tickValues)
						.tickSize(-(this.dims.width - this.margin.left))
						.tickFormat((d: any) =>
							d3.formatLocale(this.axisNumberFormat).format(this.formatStyle)(d)
						)
				).on('end', () => this.leftSetAxis())
		}
		else {
			this._yAxisHandle
				.style('font-size', this.fontSize)
				.call(
					d3
						.axisLeft(this._yAxis)
						.ticks(step)
						.tickPadding(this.yPadding)
						.tickValues(tickValues)
						.tickSize(-(this.dims.width - this.margin.left))
						.tickFormat((d: any) =>
							d3.formatLocale(this.axisNumberFormat).format(this.formatStyle)(d)
						)
				)
		}


		this._yAxisHandle
			.selectAll('line')
			.attr(
				'stroke',
				getComputedStyle(document.documentElement)
					.getPropertyValue('--digi--global--color--neutral--grayscale--darker')
					.trim()
					.toLocaleLowerCase()
			);
		this._yAxisHandle.select('.domain').attr('display', 'none');

		if (init)
			this.leftSetAxis(init)
	}

	// Adding event listeners to the chart
	private addListenersToChart = () => {
		this._Tipbox.style('cursor', 'pointer')
			.on('mousemove', this.drawTooltipLine)
			.on('touchmove', 'ontouchstart' in window || window.navigator.maxTouchPoints ? this.drawTooltip : null)
			.on('mouseup', this.drawTooltip)
			.on('mouseleave', () => {
				this._secondaryLine.attr('display', 'none');
				if (!this._toggled) {
					this._TooltipLine.attr('display', 'none');
					this._Circles.selectAll('circle').attr('display', 'none');
				}
			});
	}

	// Initialize legend
	private initLegend = () => {
		const container = d3.select(this._legendDiv);

		d3.select(this._legendDiv).style('width', (this.dims.width) + 'px')

		container.selectAll('.chip').remove();
		container.call(d3.drag().on("start", this.dragStart)
			.on("drag", this.drag));

		container.selectAll('legendChips')
			.data(this._chartData.data.series)
			.join('button')
			.attr('class', 'chip')
			.attr('type', 'button')
			.attr('aria-pressed', false)
			.style('padding', '0.15rem 0.75rem')
			.style('border', '2px solid white')
			.style('border-radius', '6px')
			.style('display', 'flex')
			.style('flex-direction', 'row')
			.style('gap', '0.5rem')
			.style('align-items', 'center')
			.style('flex-wrap', 'nowrap')
			.style('min-width', 'fit-content')
			.style('height', 'calc(2rem - 2px)')
			.style('font-size', this.fontSize)
			.style('white-space', 'nowrap')
			.style('box-sizing', 'border-box')
			.style('background-color', 'white')
			.style('cursor', 'pointer')
			.html((d: any) => {
				return '<span style="pointer-events:none;">' + d.title + '</span>';
			})
			.on('click', this.togglePath)
			.on('mouseover', this.highlightPath)
			.on('mouseleave', this.colorLines)
			.datum((d: any) => d)
			.insert('svg', 'span')
			.attr('class', 'legendSvg')
			.attr('pointer-events', 'none')
			.attr('width', 32)
			.attr('height', 28)
			.append('line')
			.attr('stroke', (d: any) => this.colorFunction(d))
			.attr("stroke-linecap", "round")
			.attr('stroke-width', 6)
			.attr('stroke-dasharray', (d: any, i: number) => this.dashFunction(d, i))
			.attr('x1', 3)
			.attr('x2', 29)
			.attr('y1', '50%')
			.attr('y2', '50%')

		// Only if data contains many lines
		if (this._chartData.data.series.length <= this.lineTypes)
			return;
		container.selectAll('.legendSvg')
			.filter((...[, i]: any[]): boolean => {
				return i > 2;
			})
			.append('path')
			.attr("d", (...[, i]: any[]) => this.symbolType(i, 110))
			.attr('fill', (d: any) => this.colorFunction(d))
			.attr('transform', 'translate(16,14)');
	}

	// Highlights a line
	private highlightPath = (event: any, element: any): void => {
		if (event.target.className != 'chip')
			return;
		const target = d3.select(event.target);
		target.style('border', (d: any) => '2px solid ' + this.colorFunction(d))

		this._lineChartContainer
			.selectAll('.linePath')
			.transition('highlight')
			.duration(150)
			.attr('stroke', (d: any, i: number, nodelist: any) => {
				if (d.title === element.title) {
					d3.select(nodelist[i]).raise();
					return this.colorFunction(d);
				}
				else
					return '#E3E3E3';
			})

		this._lineChartContainer.selectAll('.symbolGroup')
			.attr('fill', (d: any, i: number, nodelist: any) => {
				if (d.title === element.title) {
					d3.select(nodelist[i]).raise();
				}
			})

		this._lineChartContainer.selectAll('.symbolGroup')
			.selectAll('.symbol')
			.transition('highlight')
			.duration(150)
			.attr('fill', (d: any) => {
				if (!d)
					return;
				if (d.title === element.title) {
					return this.colorFunction(d);
				}
				else
					return '#E3E3E3';
			})
	}

	// Selecting a path to view sollely from legend
	private togglePath = (event: any): void => {
		if (event.target.className != 'chip')
			return;

		const target = d3.select(event.target);
		// Using color of text to identify what state the legend chip is in, so resetting this the last thing we do
		const swap: boolean = 'white' === target.style('color');

		target.style('border', (d: any) => '2px solid ' + this.colorFunction(d))
			.style('background-color', (d: any) => swap ? 'white' : this.colorFunction(d))
			.style('color', () => swap ? 'black' : 'white')
			.attr('aria-pressed', target.attr('aria-pressed') == 'false' ? true : false)
			.select('line')
			.attr('stroke', (d: any) => swap ? this.colorFunction(d) : 'white');

		target.select('path').attr('fill', (d: any) => swap ? this.colorFunction(d) : 'white');

		if (swap) {
			this._selectedLines = this._selectedLines.filter(elem => {
				return elem.title === target.datum()['title'] ? false : true;
			})
		}
		else {
			this._selectedLines.push(this._coreSeries.filter(elem => elem.title === target.datum()['title'])[0]);
		}

		if (this._selectedLines.length < 1)
			this._chartData.data.series = JSON.parse(JSON.stringify(this._coreSeries));
		else {
			this._chartData.data.series.map((serie: ChartLineSeries) => {
				const index = this._selectedLines.findIndex((selected: ChartLineSeries) => {
					return serie.title === selected.title;
				})
				if (index != -1) {
					serie.yValues = this._selectedLines[index].yValues;
				}
				else {
					serie.yValues = [];
				}
			})
		}

		this.updateSvg();
	}

	// Coloring all lines
	private colorLines = (event: any): void => {
		const target = d3.select(event.target);
		target.style('border', '2px solid white');

		this._lineChartContainer
			.selectAll('.linePath')
			.transition('highlight')
			.duration(150)
			.attr('stroke', (d: any) => this.colorFunction(d))

		this._lineChartContainer.selectAll('.symbolGroup')
			.selectAll('.symbol')
			.transition('highlight')
			.duration(150)
			.attr('fill', (d: any) => d ? this.colorFunction(d) : null)
	}

	// Returns a line type given a integer value
	private dashFunction = (...[, index]: number[]): any => {
		return this._lineTypes[index % this._lineTypes.length];
	}

	// Initialize the drawing of the lines
	private initLines = () => {
		this.defineLineAndScale();

		// TODO: do this data appending somewhere else
		this._lineChartContainer
			.selectAll('linePath')
			.data(this.reshapeData(this._chartData.data.series))
			.join('path')
			.attr('class', 'linePath')
			.attr('stroke', (d: any) => this.colorFunction(d))
			.attr("stroke-linecap", "round")
			.attr('stroke-width', (d: any) => d.length > 1 ? 4 : 6)
			.attr('stroke-dasharray', (d: any, i: number) => d.length > 1 ? this.dashFunction(d, i) : null)
			.attr('fill', 'none')
			.attr('d', (d: any) => this._LineFunc(d.line));

		this.addSymbols();
	}

	private defineLineAndScale = () => {
		this._LineFunc = d3
			.line()
			.defined((d: any) => d[1] != null && d[1] != undefined && !isNaN(d[1]))
			.x((d: any) => this._lineScale(d[0]))
			.y((d: any) => this._yAxis(d[1]));
	}

	// Modifies data object adds a line member to object, line is an array of pairs of number: [number, number][]
	// Line is used to feed into _LineFunc
	private reshapeData = (series: ChartLineSeries[]): any => {
		return series.map((obj: any) => {
			obj.line = this._chartData.data.xValues.map((elem: number, ind: number) => {
				return [elem, obj.yValues[ind]];
			});
			obj.length = obj.yValues.filter((e: number) => e != null && e != undefined).length;
			return obj;
		})
	}

	// Adding symbols to lines where needed
	private addSymbols = (transition: boolean = false): void => {
		// Remove all old symbols
		this._lineChartContainer.selectAll('.symbolGroup')
			.data(this.reshapeData(this._chartData.data.series.filter((...[, index]: any[]): boolean => index > 2)))
			.join(
				enter => {
					if (!transition) {
						enter.append('g')
							.attr('class', 'symbolGroup')
							.selectAll('symbols')
							.data((d: any, i: number) => {
								return d.line.map((num: number[]) => {
									if (num[1] != null)
										return { val: num[1], x: num[0], title: d.title, colorToken: d.colorToken, index: i }
									else
										return null;
								});
							})
							.join('path')
							.attr('class', 'symbol')
							.attr("d", (d: any) => d ? this.symbolType(d.index) : null)
							.attr('fill', (d: any) => d ? this.colorFunction(d) : null)
							.attr('transform', (d: any) => d ? `translate(${this._lineScale(d.x)},${this._yAxis(d.val)})` : null);
					}
					else {
						enter.append('g')
							.attr('class', 'symbolGroup')
							.selectAll('symbol')
							.data((d: any, i: number) => {
								return d.line.map((num: number[]) => {
									if (num[1] != null)
										return { val: num[1], x: num[0], title: d.title, colorToken: d.colorToken, index: i }
									else
										return null;
								});
							})
							.join('path')
							.attr('class', 'symbol')
							.attr("d", (d: any) => d ? this.symbolType(d.index) : null)
							.attr('fill', (d: any) => d ? this.colorFunction(d) : null)
							.attr('transform', (d: any) => d ? `translate(${this._lineScale(d.x)},${this._yAxis(d.val)})` : null)
					}
				},
				update => {
					if (!transition) {
						update.selectAll('.symbol')
							.data((d: any, i: number) => {
								return d.line.map((num: number[]) => {
									if (num[1] != null)
										return { val: num[1], x: num[0], title: d.title, colorToken: d.colorToken, index: i }
									else
										return null;
								});
							})
							.join('path')
							.attr('class', 'symbol')
							.attr("d", (d: any) => d ? this.symbolType(d.index) : null)
							.attr('fill', (d: any) => d ? this.colorFunction(d) : null)
							.attr('transform', (d: any) => d ? `translate(${this._lineScale(d.x)},${this._yAxis(d.val)})` : null);
					}
					else {
						update.selectAll('path')
							.data((d: any, i: number) => {
								const toReturn = d.line.map((num: number[]) => {
									if (num[1] != null)
										return { val: num[1], x: num[0], title: d.title, colorToken: d.colorToken, index: i }
									else
										return null;
								});
								return toReturn.filter(e => e);
							})
							.join(
								enter => {
									enter.append('path').attr('class', 'symbol')
										.attr("d", (d: any) => d ? this.symbolType(d.index) : null)
										.attr('fill', (d: any) => d ? this.colorFunction(d) : null)
										.attr('transform', (d: any) => d ? `translate(${this._lineScale(d.x)},${this._yAxis(d.val)})` : null)
										.attr('opacity', 0)
										.transition()
										.duration(400)
										.ease(d3.easePolyInOut)
										.attr('opacity', 1)
								},
								update => {
									update
										.transition()
										.duration(400)
										.ease(d3.easePolyInOut)
										.attr("d", (d: any) => d ? this.symbolType(d.index) : null)
										.attr('fill', (d: any) => d ? this.colorFunction(d) : null)
										.attr('transform', (d: any) => d ? `translate(${this._lineScale(d.x)},${this._yAxis(d.val)})` : null);
								},
								exit => exit.remove()
							)

					}
				},
				exit => exit.remove()
			)
	}

	// Returns a symbol type given a index, currently we have support for 10 lines
	private symbolType = (index: number, size?: number): any => {
		if (index < this.lineTypes)
			return d3.symbol().type(d3.symbolCircle).size(size ? size : 90)();
		else if (index >= this.lineTypes && index < (this.lineTypes * 2))
			return d3.symbol().type(d3.symbolDiamond).size(size ? size : 90)();
		else
			return d3.symbol().type(d3.symbolStar).size(size ? size : 90)();
	}

	// Setting dimension variable
	private setDims = (onlyHeight = false) => {
		// Get title dimensions
		const tempTitle = d3.select(this._titleDiv).node().getBoundingClientRect(),
			tempLegend = d3.select(this._legendDiv).node().getBoundingClientRect();
		const rem = parseFloat(getComputedStyle(document.documentElement).fontSize);

		if (!onlyHeight)
			this.dims = d3.select(this._chartDiv).node().getBoundingClientRect();
		this.dims.height = this._maxHeight - tempLegend.height - tempTitle.height - (1.75 * rem);
	}

	// Resizing svg element and its children
	private resizeSvg = () => {

		if (this._loading)
			return this.loadingResize();

		this.setDims();
		this._svg.attr('width', this.dims.width).attr('height', this.dims.height);
		/****  Update yAxis ****/
		this._yAxis.range([this.dims.height - this.margin.bottom, this.margin.top]);
		// Fixed number of y-axis lines
		this.setYTicks(true);

		/****  Update xAxis ****/
		this._xAxis.range([0, this.dims.width - this.margin.left - this.margin.right])

		const ticks = Math.min(Math.max(Math.floor((this.dims.width - this.margin.left - this.margin.right) / this._tickSize) - 2, 1), this._chartData.data.xValues.length);
		this._xAxisHandle
			.attr('transform', 'translate(0,' + (this.dims.height - this.margin.bottom) + ')')
			.call(
				d3.axisBottom(this._xAxis)
					.ticks(ticks)
					.tickPadding(this.xPadding)
					.tickFormat((d: any) => {
						if (this._chartData.data.xValueNames)
							return this._chartData.data.xValueNames[this._chartData.data.xValues.indexOf(d)]
						else
							return d;
					})
			);

		this._Tipbox.attr('width', this.dims.width - this.margin.left / 2)
			.attr('height', this.dims.height - this.margin.top - this.margin.bottom)
			.attr('transform', `translate(${this.margin.left / 2},${this.margin.top})`)
	}

	// resizing chart while loading
	private loadingResize = () => {
		this.setDims();
		this._svg.attr('width', this.dims.width);
		this.setYTicks();
		this._xAxis.range([0, this.dims.width - this.margin.left - this.margin.right]);

		const ticks = Math.min(Math.floor((this.dims.width - this.margin.left - this.margin.right) / this._tickSize) - 2, this._xAxis.domain()[1]);
		this._xAxisHandle
			.call(
				d3.axisBottom(this._xAxis)
					.ticks(ticks)
					.tickPadding(this.xPadding)
					.tickFormat((d: any) => {
						if (this._chartData.data && this._chartData.data.xValueNames)
							return this._chartData.data.xValueNames[this._chartData.data.xValues.indexOf(d)]
						else
							return d;
					})
			);

		this._lineScale.range([0, this.dims.width - this.margin.left - this.margin.right]);

		const LineFunc = d3.line()
			.defined((d: any) => d[1] != null)
			.x((d: any) => this._lineScale(d[0]))
			.y((d: any) => this._yAxis(d[1]))
			.curve(d3.curveBasis);

		this._lineChartContainer.selectAll('.linePath')
			.attr('d', (d: any) => LineFunc(d))

	}

	private resizeLines = () => {
		if (this._loading)
			return

		this._lineScale.range([0, this.dims.width - this.margin.left - this.margin.right]);

		this.defineLineAndScale();

		this._lineChartContainer
			.selectAll('.linePath')
			.attr('stroke', (d: any) => this.colorFunction(d))
			.attr('d', (d: any) => this._LineFunc(d.line));

		this._lineChartContainer.selectAll('.symbolGroup')
			.selectAll('.symbol')
			.attr('transform', (d: any, i: any) => d ? `translate(${this._lineScale(this._chartData.data.xValues[i])},${this._yAxis(d.val)})` : null);

	}

	componentWillLoad() {
		this.afChartDataUpdate();
	}

	componentDidLoad() {
		if (Object.keys(this._chartData).length === 0)
			return this.initEmpty();

		// clearing selection
		this._selectedLines = [];
		this.initSvg();
		this.initLegend();
		this.initLines();

		document.fonts.onloadingdone = () => {
			this.setTickSize();
			this.resizeSvg();
		};
	}

	componentWillUpdate() {
		this._prevHeight = d3.select(this._titleDiv).style('height');
	}

	componentDidUpdate() {
		if (Object.keys(this._chartData).length === 0)
			return this.loadingState();

		// clearing selection
		this._selectedLines = [];
		this.updateSvg();
		this.initLegend();
		this.updateTableContent();

	}

	// Returns pixelwidth of text given the current fontSize (costly function using .each)
	private getTextSize(text: string): number {
		var textWidth: number[] = [];
		let svg = d3.select(this._chartDiv).append('svg');
		svg
			.selectAll('dummyText')
			.data([text])
			.join('text')
			.attr('font-size', this.fontSize)
			.text((d: any) => d)
			.each((...[, i, nodelist]: any[]) => {
				var thisWidth = nodelist[i].getComputedTextLength();
				textWidth.push(thisWidth);
				nodelist[i].remove();
			});

		svg.remove().exit();

		return textWidth[0];
	}

	// Goes through all tick values and checks the biggest in size, this one will decide the default tick-size
	private setTickSize(): void {
		if (this._chartData.data.xValueNames) {
			for (const elem of this._chartData.data.xValueNames) {
				const size = this.getTextSize(elem);
				if (size > this._tickSize)
					this._tickSize = size;
			}
		} else {
			for (const elem of this._chartData.data.xValues) {
				const size = this.getTextSize(elem.toString());
				if (size > this._tickSize)
					this._tickSize = size;
			}
		}
	}

	private colorFunction = (properties: any) => {
		return properties.colorToken
			? getComputedStyle(document.documentElement)
				.getPropertyValue(properties.colorToken)
				.trim()
				.toLocaleLowerCase()
			: this._Color(properties.title);
	}

	private drawTooltipLine = (event: any) => {

		if (this._toggled)
			return this.drawDottedLine(event)

		this.drawMainLineAndCircles(event);
	};

	private closeTooltip = () => {
		this._toggled = false;
		this._tooltip.style('display', 'none');
		this._secondaryLine.attr('display', 'none')
		this._TooltipLine.attr('display', 'none');
		this._Circles.selectAll('circle').attr('display', 'none');
		this._Circles
			.selectAll('circle')
			.attr('fill', 'white')
			.attr('stroke', (d: any) => this.colorFunction(d))
		this._svg.select('.tooltipPointer').remove();
	}

	private drawDottedLine = (event: any) => {
		this._hoverPoint = this.closest(Math.round(
			this._lineScale.invert(d3.pointer(event, this._Tipbox.node())[0] - (this.margin.left / 2))
		));

		if (this._hoverPoint === this._tooltipPoint)
			return this._secondaryLine.attr("display", "none")

		this._secondaryLine
			.attr("display", null)
			.attr('stroke', 'gray')
			.attr("stroke-dasharray", 4)
			.attr('x1', this._xAxis(this._hoverPoint.toString()) + this.margin.left)
			.attr('x2', this._xAxis(this._hoverPoint.toString()) + this.margin.left)
			.attr('y1', this._yAxis(this._maxVal))
			.attr('y2', this.dims.height - this.margin.bottom)
			.style('pointer-events', 'none');
	}

	private drawMainLineAndCircles = (event: any) => {
		this._hoverPoint = this.touchPosition(event);

		const stringPoint: string = this._hoverPoint.toString();

		this._TooltipLine
			.attr('display', null)
			.attr('stroke', 'gray')
			.attr('x1', this._xAxis(stringPoint) + this.margin.left)
			.attr('x2', this._xAxis(stringPoint) + this.margin.left)
			.attr('y1', this._yAxis(this._maxVal))
			.attr('y2', this.dims.height - this.margin.bottom)
			.style('pointer-events', 'none');

		this._Circles
			.selectAll('circle')
			.attr('display', null)
			.attr('pointer-events', 'none');
		this.positionCircles(stringPoint);
	}

	// Returns the position/index to draw tooltip/tooltip line
	private touchPosition = (event: any) => {
		if (event.type === 'touchmove') {
			event.preventDefault();
			return this.closest(Math.round(
				this._lineScale.invert(d3.pointer(event.touches[0], this._Tipbox.node())[0] - (this.margin.left / 2))
			));
		}
		else {
			return this.closest(Math.round(
				this._lineScale.invert(d3.pointer(event, this._Tipbox.node())[0] - (this.margin.left / 2))
			));
		}
	}

	// Function for drawing tooltip
	private drawTooltip = (event: any) => {

		this.drawMainLineAndCircles(event);

		this._toggled = true;

		this._tooltipPoint = this.touchPosition(event);

		this._tooltipDataIndex = this._chartData.data.xValues.indexOf(this._tooltipPoint);

		// Heading
		this._tooltip.style('display', 'flex');
		let tooltipBody = this._tooltip.select(".tooltipBody");
		tooltipBody.select(this._tooltipHeading)
			.style('margin', '0 0.75rem 0 0 ')
			.html(() => {
				if (this._chartData.data.xValueNames)
					return this._chartData.data.xValueNames[this._chartData.data.xValues.indexOf(this._tooltipPoint)];
				return this._tooltipPoint;
			});

		// Body
		tooltipBody
			.selectAll('div')
			.data(this._chartData.data.series)
			.join('div')
			.style("display", "flex")
			.style("flex-direction", "row")
			.style("flex-wrap", "nowrap")
			.style("white-space", "nowrap")
			.style("justify-content", "space-between")
			.html((d: any) => {
				return d.yValues[this._tooltipDataIndex] != null ? '<span>' + d.title + '</span><span style="margin-left:0.75rem;">' + d3.formatLocale(this.axisNumberFormat).format(this.formatStyle)(d.yValues[this._tooltipDataIndex]) + '</span>' : null;
			});

		this.positionTooltipWithinBounds(this._xAxis(this._tooltipPoint.toString()) + this.margin.left);

		this._Circles
			.selectAll('circle')
			.attr('fill', (d: any) => this.colorFunction(d))
			.attr('stroke', 'white');

		this._svg.select('.tooltipPointer').remove();
		const sym =
			d3.symbol().type(d3.symbolTriangle).size(60);

		this._svg.append("path")
			.attr("d", sym)
			.attr('class', 'tooltipPointer')
			.attr("fill", "rgba(10, 10, 10, 0.90)")
			.style('filter', 'drop-shadow(gray 0px -1px 1px)')
			.attr("transform", `translate(${this._xAxis(this._tooltipPoint.toString()) + this.margin.left},${this._yAxis(this._maxVal) - 7}) rotate(180) scale(1.5,1.0)`);

	};

	private initYAxis = (empty?: boolean): void => {
		// Y Axis
		this._yAxis = d3
			.scaleLinear()
			.domain([0, empty ? 10 : this._maxVal])
			.nice(3)
			.range([this.dims.height - this.margin.bottom, this.margin.top]);

		// The magic number is just needed in initialization, because getting the exact size is impossible during the initialization
		this.margin.left = this.getTextSize(d3.formatLocale(this.axisNumberFormat).format(this.formatStyle)(this._yAxis.domain()[1])) + this.yPadding + 3;

		this._lineChartContainer = this._svg
			.append('g')
			.attr('transform', `translate(${this.margin.left},0)`);

		this._yAxisHandle = this._lineChartContainer
			.append('g')
			.attr('class', 'yAxis')

		this.setYTicks(true);
	}

	// Left-orients Y-axis text
	private leftSetAxis(init = false): void {

		let shiftArray: number[] = [];

		if (!init) {
			this._yAxisHandle.selectAll("text")
				.transition('shuffle text')
				.duration(400)
				.attr("transform", (...[, i, nodelist]: any[]) => {
					const size: number = nodelist[i].getComputedTextLength();
					shiftArray.push(size);
					return `translate(${-1 * ((this.margin.left) - (size + this.yPadding))},0)`;
				})

			this._yAxisHandle.selectAll("line")
				.transition('shuffle line')
				.duration(400)
				.attr("x1", (...[, i]: any[]) => {
					return -1 * ((this.margin.left) - (shiftArray[i] + this.yPadding));
				})
		}
		else {
			this._yAxisHandle.selectAll("text")
				.attr("transform", (...[, i, nodelist]: any[]) => {
					const size: number = nodelist[i].getComputedTextLength();
					shiftArray.push(size);
					return `translate(${-1 * ((this.margin.left) - (size + this.yPadding))},0)`;
				})

			this._yAxisHandle.selectAll("line")
				.attr("x1", (...[, i]: any[]) => {
					return -1 * ((this.margin.left) - (shiftArray[i] + this.yPadding));
				})
		}

	}

	private initXAxis = (empty?: boolean): void => {

		this._lineChartContainer.select('.xAxis').remove();
		// X axis
		if (!empty) {
			if (this._chartData.data.xValueNames)
				this.margin.right = (this.getTextSize(this._chartData.data.xValueNames[this._chartData.data.xValueNames.length - 1]) / 2) + 1;
			else
				this.margin.right = this.getTextSize(d3.max(this._chartData.data.xValues).toString()) / 2 + 1;
		}

		this._xAxis = d3
			.scaleLinear()
			.domain([
				empty ? 1 : d3.min(this._chartData.data.xValues),
				empty ? 10 : d3.max(this._chartData.data.xValues)
			]) // This is the min and the max of the data
			.range([0, this.dims.width - this.margin.left - this.margin.right]);

		this._lineScale = d3
			.scaleLinear()
			.domain([
				empty ? 1 : d3.min(this._chartData.data.xValues),
				empty ? 10 : d3.max(this._chartData.data.xValues)
			]) // This is the min and the max of the data
			.range([0, this.dims.width - this.margin.left - this.margin.right]);
		const ticks = Math.min(Math.max(Math.floor((this.dims.width - this.margin.left - this.margin.right) / this._tickSize) - 2, 2), empty ? 10 : this._chartData.data.xValues.length);
		this._xAxisHandle = this._lineChartContainer
			.append('g')
			.attr('transform', 'translate(0,' + (this.dims.height - this.margin.bottom) + ')')
			.attr('class', 'xAxis')
			.style('font-size', this.fontSize)
			.call(
				d3.axisBottom(this._xAxis)
					.ticks(ticks)
					.tickSize(this.xTickSize)
					.tickPadding(this.xPadding)
					.tickFormat((d: any) => {
						if (!empty && this._chartData.data.xValueNames)
							return this._chartData.data.xValueNames[this._chartData.data.xValues.indexOf(d)]
						else
							return d;
					})
			);

		// Removing the domain
		this._xAxisHandle.select('.domain').attr("display", "none");
		this._xAxisHandle
			.selectAll('line')
			.attr("stroke-linecap", "round")
			.attr(
				'stroke',
				getComputedStyle(document.documentElement)
					.getPropertyValue('--digi--global--color--neutral--grayscale--darkest-3')
					.trim()
					.toLocaleLowerCase()
			);
	}

	private positionCircles = (point?: string, transition?: number): void => {

		if (transition) {
			this._Circles.selectAll('circle')
				.transition('positioncircles')
				.duration(transition)
				.attr('cx', () => {
					return this._xAxis(point) + this.margin.left;
				})
				.attr('cy', (d: any, i: number, circleList: any) => {
					let selData = d.yValues[this._chartData.data.xValues.indexOf(parseInt(point))];
					if (this._yAxis(selData)) return this._yAxis(selData);
					else {
						d3.select(circleList[i]).attr('display', 'none');
						return null;
					}
				});
		}
		else {
			this._Circles.selectAll('circle')
				.attr('cx', () => {
					return this._xAxis(point) + this.margin.left;
				})
				.attr('cy', (d: any, i: number, circleList: any) => {
					let selData = d.yValues[this._chartData.data.xValues.indexOf(parseInt(point))];
					if (this._yAxis(selData)) return this._yAxis(selData);
					else {
						d3.select(circleList[i]).attr('display', 'none');
						return null;
					}
				});
		}

	}

	private repositionTooltip = (): void => {

		if (!this._toggled)
			return

		const stringPoint = this._chartData.data.xValues[this._tooltipDataIndex].toString();

		this._TooltipLine.attr('stroke', 'gray')
			.attr('x1', this._xAxis(stringPoint) + this.margin.left)
			.attr('x2', this._xAxis(stringPoint) + this.margin.left)
			.attr('y1', this._yAxis(this._maxVal))
			.attr('y2', this.dims.height - this.margin.bottom);

		this.positionTooltipWithinBounds(this._xAxis(stringPoint) + this.margin.left);
		this._svg.select('.tooltipPointer').attr("transform", `translate(${this._xAxis(this._tooltipPoint.toString()) + this.margin.left},${this._yAxis(this._maxVal) - 7}) rotate(180) scale(1.5,1.0)`);

		this.positionCircles(stringPoint);
	}

	// Checks if tooltip bounding box is outside svg width, if so, moves tooltip within svg bounds
	private positionTooltipWithinBounds = (pos: any, transitionTimer?: number): void => {
		this._tooltip
			.style('transform', () => {
				// We reset the position of the tooltip, and adjust for the maximum value, -1px for it to align perfectly
				return `translateY(${this._yAxis(this._maxVal) - this._tooltip.node().getBoundingClientRect().height - 9}px)`
			})
		const rect = this._chartDiv.getBoundingClientRect();
		const comparativePosition = pos + rect.left;
		const tooltipWidth = (this._tooltip.node().getBoundingClientRect().width) / 2;

		if (rect.left < comparativePosition - tooltipWidth && rect.right > comparativePosition + tooltipWidth) {
			this._tooltip.style('left', pos - tooltipWidth + 'px');
		}
		else if (rect.right < comparativePosition + tooltipWidth) {
			this._tooltip.style("left", pos - this._tooltip.node().getBoundingClientRect().width + Math.max(this.margin.right, 10) + 'px');
		}
		else if (rect.left > comparativePosition - tooltipWidth) {
			if (transitionTimer)
				this._tooltip.transition('moveTooltip').duration(transitionTimer).ease(d3.easePolyInOut).style("left", pos - this.margin.left + 'px');
			else
				this._tooltip.style("left", pos - this.margin.left + 'px');
		}
		else {
			this._tooltip.style('left', pos - (document.getElementById(this.afId).getBoundingClientRect().width / 2) + 'px');
		}
	}

	private mapColorToLine = (): void => {
		// list of group names
		this._Color = d3
			.scaleOrdinal()
			.domain(this._chartData.data.series.map(function (d: any) {
				return d.title;
			}))
			.range([
				'#00005a',
				'#D43372',
				'#058470',
				'#0058A3',
				'#556D22',
				'#1616B2',
				'#AA295B',
				'#035548'
			]);
	}

	private computeMaxVal = (): void => {
		// Compute max value from datasets
		this._maxVal = parseFloat(
			d3.max(this._chartData.data.series, (d: any) => d3.max(d['yValues']))
		);
	}
	// Updates number format if specified to be in percentage
	private checkPercentage = (): void => {
		if (this._chartData.meta && this._chartData.meta.percentage)
			this.formatStyle = '.1%';
		else
			this.formatStyle = ',d';
	}


	private closest = (target: number) => {
		return this._chartData.data.xValues.reduce(function (prev, curr) {
			return (Math.abs(curr - target) < Math.abs(prev - target) ? curr : prev);
		});
	}

	// Removes resize observer for chart and inits table
	private activateTable = (event: any) => {
		this.closeTooltip();
		this.lineChartObserver.disconnect();
		this.tableObserver.observe(this._chartDiv);

		this._status.innerHTML = '<p>Visas som tabell</p>';
		d3.select(this._host).select('.tableButton').style('display', 'none');
		d3.select(this._host).select('.chartButton').style('display', null);
		event.target.nextSibling.lastChild.focus();
		const duration = 150;
		this._svg
			.attr('opacity', 1)
			.transition('fadeout')
			.duration(duration)
			.attr('opacity', 0)
			.on('end', () => {
				this._svg.remove()
				this._legendDiv.style['display'] = 'none';
				this._tableDiv.style['display'] = 'block';
				this.setTableContent();
				this.setTableDimensions();
				d3.select(this._tableDiv).style('opacity', 0)
					.transition('fadein')
					.duration(duration)
					.style('opacity', 1)
			});
		d3.select(this._legendDiv).style('opacity', 1)
			.transition('fadeout')
			.duration(duration)
			.style('opacity', 0)

	}

	// Updating data if content changes
	private updateTableContent() {
		if (this._tableDiv.style['display'] === '' || this._tableDiv.style['display'] === 'none')
			return;
		this.setTableContent();
		this.animateTableDimensions();
	}

	// Setting table content from this._chartData
	private setTableContent(): void {
		this._tableDiv.innerHTML = `<digi-table af-size="small" af-variation="primary"> <table><caption>${this._chartData.title}</caption><thead><tr>${this.headings()}</tr></thead><tbody>${this.tableBody()}</tbody></table></digi-table>`;
	}

	private setTableDimensions(): void {
		const table = d3.select(this._tableDiv);
		table
			// The last value is for compensating the padding set on the table element, changing padding in css, will need change in these two lines
			.style('height', (this._maxHeight - d3.select(this._titleDiv).node().getBoundingClientRect().height - (parseFloat(getComputedStyle(document.documentElement).fontSize) * 0.65)) + 'px');
		table.style('width', (this.dims.width - parseFloat(getComputedStyle(document.documentElement).fontSize) * 0.65) + 'px');
	}

	private animateTableDimensions(): void {
		const height = d3.select(this._titleDiv).style('height');
		d3.select(this._titleDiv)
			.style('height', this._prevHeight)
			.transition('adjust')
			.duration(400)
			.style('height', height)
			.on('end', () => d3.select(this._titleDiv).style('height', null))

		const table = d3.select(this._tableDiv);
		table
			.transition('adjustHeight')
			.duration(400)
			// The last value is for compensating the padding set on the table element, changing padding in css, will need change in these two lines
			.style('height', (this._maxHeight - (parseFloat(height) + (parseFloat(getComputedStyle(document.documentElement).fontSize) * 1.25)) - (parseFloat(getComputedStyle(document.documentElement).fontSize) * 0.65)) + 'px');
		table.style('width', (this.dims.width - parseFloat(getComputedStyle(document.documentElement).fontSize) * 0.65) + 'px');
	}

	// Resize table
	private resizeTable = () => {
		this.setDims();
		this.setTableDimensions();
	}

	// Extracting all headings from data and returning a string with <th>-elements with headings
	private headings(): string {
		let headingsString: string = `<th scope="col">${this._chartData.x}</th>`;
		this._coreSeries.forEach((elem: ChartLineSeries) => {
			headingsString = headingsString.concat(`<th scope="col">${elem.title}</th>`)
		})
		return headingsString;
	}

	// Building the table body 
	private tableBody(): string {
		let body: string = '';
		const loopable = this._chartData.data.xValueNames ? this._chartData.data.xValueNames : this._chartData.data.xValues;

		loopable.forEach((elem: any, index: number) => {
			let cells: string = '';
			this._coreSeries.forEach((serie: ChartLineSeries) => {
				cells = cells.concat(`<td>${serie.yValues[index] != null ? d3.formatLocale(this.axisNumberFormat).format(this.formatStyle)(serie.yValues[index]) : '-'}</td>`)
			})
			body = body.concat(`<tr><th scope='row'>${elem}${cells}</th></tr>`)
		})

		return body;
	}

	// Removes observer and calls init functions for line chart
	private reInitChart = (event: any) => {
		this.tableObserver.disconnect();
		this._status.innerHTML = '<p>Visas som diagram</p>';
		d3.select(this._host).select('.tableButton').style('display', null);
		d3.select(this._host).select('.chartButton').style('display', 'none');
		event.target.previousSibling.lastChild.focus();
		const table = d3.select(this._tableDiv)
		table.style('opacity', 1)
			.transition()
			.duration(150)
			.style('opacity', 0)
			.on('end', () => {
				table.style('display', 'none');
				this.initSvg();
				this.initLines();
				this._svg
					.attr('opacity', 0)
					.transition()
					.duration(150)
					.attr('opacity', 1)
				d3.select(this._legendDiv)
					.style('display', null)
					.style('opacity', 0)
					.transition()
					.duration(150)
					.style('opacity', 1);
			})
	}


	render() {
		let heading: string = '';
		if (!this._chartData.subTitle)
			heading = `<${this.afHeadingLevel}>${this._chartData.title}</${this.afHeadingLevel}>`;
		else
			heading = `<${this.afHeadingLevel}>${this._chartData.title}</${this.afHeadingLevel}><p>${this._chartData.subTitle}</p>`;

		return (
			<host ref={(el: any) => (this._host = el)}>
				<div class="topWrapper" ref={(el) => (this._titleDiv = el)}>
					<digi-typography class="chartTitle" innerHTML={heading} >
					</digi-typography>
					<digi-button class="buttonWrapper tableButton"
						af-size="medium"
						af-variation="function"
						af-full-width="false"
						onAfOnClick={this.activateTable}>
						<digi-icon-table slot="icon" />
						Visa tabell
					</digi-button>
					<digi-button style={{ 'display': 'none' }} class="buttonWrapper chartButton"
						af-size="medium"
						af-variation="function"
						af-full-width="false"
						onAfOnClick={this.reInitChart}>
						<digi-icon-chart slot="icon" />
						Visa diagram
					</digi-button>

					<div class="scChartStatus" role="status" ref={(el) => (this._status = el)}></div>
				</div>
				<div class="chartTooltip" id={this.afId}><div class="tooltipBody"></div><digi-icon-x class="close"></digi-icon-x> </div>
				<div class="chart" ref={(el) => (this._chartDiv = el)}></div>
				<div class="legend" ref={(el) => (this._legendDiv = el)}></div>
				<div class="table" ref={(el) => (this._tableDiv = el)}> </div>
			</host>
		);
	}
}
