import { newE2EPage } from '@stencil/core/testing';

describe('digi-tag', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-tag></digi-tag>');

    const element = await page.find('digi-tag');
    expect(element).toHaveClass('hydrated');
  });
});
