import { Component, Prop, Event, EventEmitter, h } from '@stencil/core';
import { TagSize } from './tag-size.enum';

/**
 * @swedishName Tagg
 */

@Component({
	tag: 'digi-tag',
	styleUrls: ['tag.scss'],
	scoped: true
})
export class Tag {
	/**
	 * Sätter taggens text.
	 * @en Set the tag text.
	 */
	@Prop() afText!: string;

	/**
	 * Tar bort taggens ikon. Falskt som förvalt.
	 * @en Removes the tag icon. Defaults to false.
	 */
	@Prop() afNoIcon: boolean = false;

	/**
	* Sätt attributet 'aria-label'.
 	* @en Set button `aria-label` attribute.
	*/
	@Prop() afAriaLabel: string;

	/**
	 * Sätter taggens storlek.
	 * @en Sets tag size.
	 */
	@Prop() afSize: TagSize = TagSize.SMALL;

	/**
	 * Taggelementets 'onclick'-event.
	 * @en The tag elements 'onclick' event.
	 */
	@Event() afOnClick: EventEmitter;

	clickHandler(e: any) {
		this.afOnClick.emit(e);
	}

	render() {
		return (
			<digi-button
				onAfOnClick={(e) => this.clickHandler(e)}
				af-variation="secondary"
				af-size={this.afSize}
				class="digi-tag"
				af-aria-label={this.afAriaLabel}
			>
				{this.afText}
				{!this.afNoIcon && <digi-icon afName={`x`} slot="icon-secondary" />}
			</digi-button>
		);
	}
}
