import { Component, Prop, h } from '@stencil/core';
import { LayoutColumnsElement } from './layout-columns-element.enum';
import { LayoutColumnsVariation } from './layout-columns-variation.enum';

/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums LayoutColumnsVariation - layout-columns-variation.enum.ts
 * @enums LayoutColumnsElement - layout-columns-element.enum.ts
 * @swedishName Kolumner
 */
@Component({
  tag: 'digi-layout-columns',
  styleUrls: ['layout-columns.scss'],
  scoped: true,
})
export class LayoutColumns {
  /**
   * Sätter elementtypen på det omslutande elementet. Kan vara 'div', 'ul' eller 'ol'.
   * @en Set wrapper element type. can be 'div', 'ul' or 'ol'.
   */
  @Prop() afElement: LayoutColumnsElement = LayoutColumnsElement.DIV;

  /**
   * Sätter förvalda kolumnvarianter. Kan vara 'two' eller 'three'.
   * @en Set preset column variations. Can be 'two' or 'three'.
   */
  @Prop() afVariation: LayoutColumnsVariation;

  get cssModifiers() {
    return {
      'digi-layout-columns--one':
        this.afVariation === LayoutColumnsVariation.ONE,
      'digi-layout-columns--two':
        this.afVariation === LayoutColumnsVariation.TWO,
      'digi-layout-columns--three':
        this.afVariation === LayoutColumnsVariation.THREE,
    };
  }

  render() {
    return (
      <this.afElement
        class={{
          'digi-layout-columns': true,
          ...this.cssModifiers,
        }}
      >
        <slot></slot>
      </this.afElement>
    );
  }
}
