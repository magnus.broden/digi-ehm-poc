export enum LayoutColumnsVariation {
  THREE = 'three',
  TWO = 'two',
  ONE = 'one'
}
