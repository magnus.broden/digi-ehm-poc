import { newE2EPage } from '@stencil/core/testing';

describe('digi-layout-columns', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-layout-columns></digi-layout-columns>');

    const element = await page.find('digi-layout-columns');
    expect(element).toHaveClass('hydrated');
  });
});
