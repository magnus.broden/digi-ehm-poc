import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { LayoutMediaObjectAlignment } from './layout-media-object-alignment.enum';

export default {
	title: 'layout/digi-layout-media-object',
	argTypes: {
		'af-alignment': enumSelect(LayoutMediaObjectAlignment)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-layout-media-object',
	'af-alignment': LayoutMediaObjectAlignment.START,
	/* html */
	children: `
    <img
        slot="media"
        src="https://picsum.photos/id/237/100/100"
    />
    <digi-typography>
        <h3>I am a media object</h3>
        <p>The media object is a very common ux layout pattern. Read more about it <a href="https://css-tricks.com/media-object-bunch-ways/">here</a>.</p>
    </digi-typography>`
};
