export enum LayoutMediaObjectAlignment {
  CENTER = 'center',
  START = 'start',
  END = 'end',
  STRETCH = 'stretch'
}
