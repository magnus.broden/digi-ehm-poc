import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { LayoutBlockVariation } from './layout-block-variation.enum';
import { LayoutBlockContainer } from './layout-block-container.enum';

export default {
	title: 'layout/digi-layout-block',
	argTypes: {
		'af-variation': enumSelect(LayoutBlockVariation),
		'af-container': enumSelect(LayoutBlockContainer)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-layout-block',
	'af-variation': LayoutBlockVariation.PRIMARY,
	'af-container': LayoutBlockContainer.STATIC,
	'af-vertical-padding': false,
	'af-margin-top': false,
	'af-margin-bottom': false,
	/* html */
	children: `
    <digi-typography>
        <h2>I am a standard layout block.</h2>
        <digi-media-image
            af-src="https://picsum.photos/id/237/1280/500"
            af-alt="En bild"
            af-width="1280"
            af-height="500"
            style="margin-bottom: var(--digi--margin--medium); display: block;"
            ></digi-media-image>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est. Pellentesque in magna molestie, vulputate nibh non, ullamcorper magna. Donec ullamcorper vehicula orci, sit amet ultrices tellus finibus non. Curabitur varius consequat est, ut eleifend urna ornare vel. Aliquam efficitur dictum consectetur. Suspendisse vehicula, velit fringilla faucibus euismod, nisl dolor rutrum nisi, ut consequat neque magna sit amet nunc. Donec porttitor gravida diam ut iaculis. Proin cursus elementum faucibus. Ut eu tincidunt sem. Donec velit massa, bibendum in turpis eu, ornare consectetur risus. Vestibulum in ipsum nibh. </p>
    </digi-typography>`
};
