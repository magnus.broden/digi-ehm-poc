import { Component, Prop, h, State, Watch, Fragment } from '@stencil/core';
import { LayoutBlockVariation } from './layout-block-variation.enum';
import { LayoutBlockContainer } from './layout-block-container.enum';

/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums LayoutBlockContainer - layout-block-container.enum.ts
 * @enums LayoutBlockVariation - layout-block-variation.enum.ts
 *@swedishName Block
 */
@Component({
	tag: 'digi-layout-block',
	styleUrls: ['layout-block.scss'],
	scoped: true
})
export class LayoutBlock {
	@State() _container: LayoutBlockContainer = LayoutBlockContainer.STATIC;

	/**
	 * Sätter variant. Kontrollerar bakgrundsfärgen.
	 * @en Set variation. Controls background color.
	 */
	@Prop() afVariation: LayoutBlockVariation = LayoutBlockVariation.PRIMARY;

	/**
	 * Sätter den inre containervarianten, eller tar bort den helt. Kan vara 'static', 'fluid' eller 'none'.
	 * @en Set inner container variation, or remove it completely. Can be 'static', 'fluid' or 'none'.
	 */
	@Prop() afContainer: LayoutBlockContainer = LayoutBlockContainer.STATIC;

	@Watch('afContainer')
	containerChangeHandler() {
		this._container = this.afContainer;
	}

	componentWillLoad() {
		this.containerChangeHandler();
	}
	/**
	 * Lägger till vertikal padding inuti containern
	 * @en Adds vertical padding inside the container
	 */
	@Prop() afVerticalPadding: boolean;

	/**
	 * Lägger till marginal i toppen på containern
	 * @en Adds margin on top of the container
	 */
	@Prop() afMarginTop: boolean;

	/**
	 * Lägger till marginal i botten på containern
	 * @en Adds margin at bottom of the container
	 */
	@Prop() afMarginBottom: boolean;

	get cssModifiers() {
		return {
			'digi-layout-block--transparent':
				this.afVariation === LayoutBlockVariation.TRANSPARENT,
			'digi-layout-block--primary':
				this.afVariation === LayoutBlockVariation.PRIMARY,
			'digi-layout-block--secondary':
				this.afVariation === LayoutBlockVariation.SECONDARY,
			'digi-layout-block--tertiary':
				this.afVariation === LayoutBlockVariation.TERTIARY,
			'digi-layout-block--symbol':
				this.afVariation === LayoutBlockVariation.SYMBOL,
			'digi-layout-block--profile':
				this.afVariation === LayoutBlockVariation.PROFILE
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-layout-block': true,
					...this.cssModifiers
				}}
			>
				{this._container === LayoutBlockContainer.NONE ? (
					<Fragment>
						<slot></slot>
					</Fragment>
				) : (
					<digi-layout-container
						afVariation={this._container as any}
						af-vertical-padding={this.afVerticalPadding}
						af-margin-top={this.afMarginTop}
						af-margin-bottom={this.afMarginBottom}
					>
						<slot></slot>
					</digi-layout-container>
				)
				}
			</div>
		);
	}
}
