# digi-layout-container

This is a basic container component. It functions as an exact replacement for Bootstrap's `.container`-class, and even accepts the same variations (`static` or `fluid`).

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { LayoutContainerVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description                                                 | Type                                                                                                 | Default                           |
| ------------------- | --------------------- | ----------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | --------------------------------- |
| `afMarginBottom`    | `af-margin-bottom`    | Lägger till marginal i botten på containern                 | `boolean`                                                                                            | `undefined`                       |
| `afMarginTop`       | `af-margin-top`       | Lägger till marginal i toppen på containern                 | `boolean`                                                                                            | `undefined`                       |
| `afNoGutter`        | `af-no-gutter`        | Tar bort marginalen från sidorna av containern              | `boolean`                                                                                            | `undefined`                       |
| `afVariation`       | `af-variation`        | Sätter containervarianten. Kan vara 'static' eller 'fluid'. | `LayoutContainerVariation.FLUID \| LayoutContainerVariation.NONE \| LayoutContainerVariation.STATIC` | `LayoutContainerVariation.STATIC` |
| `afVerticalPadding` | `af-vertical-padding` | Lägger till vertikal padding inuti containern               | `boolean`                                                                                            | `undefined`                       |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


## CSS Custom Properties

| Name                                         | Description                            |
| -------------------------------------------- | -------------------------------------- |
| `--digi--layout-container--gutter`           | var(--digi--container-gutter--base);   |
| `--digi--layout-container--margin-bottom`    | 0;                                     |
| `--digi--layout-container--margin-top`       | 0;                                     |
| `--digi--layout-container--vertical-padding` | 0;                                     |
| `--digi--layout-container--width`            | var(--digi--container-width--smaller); |


## Dependencies

### Used by

 - [digi-layout-block](../layout-block)

### Graph
```mermaid
graph TD;
  digi-layout-block --> digi-layout-container
  style digi-layout-container fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
