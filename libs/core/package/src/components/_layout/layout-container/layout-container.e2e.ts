import { newE2EPage } from '@stencil/core/testing';

describe('digi-layout-container', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-layout-container></digi-layout-container>');

    const element = await page.find('digi-layout-container');
    expect(element).toHaveClass('hydrated');
  });
});
