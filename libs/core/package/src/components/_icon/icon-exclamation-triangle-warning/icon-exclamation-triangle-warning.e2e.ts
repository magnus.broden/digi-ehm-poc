import { newE2EPage } from '@stencil/core/testing';

describe('icon-exclamation-triangle-warning', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<icon-exclamation-triangle-warning></icon-exclamation-triangle-warning>');
    const element = await page.find('icon-exclamation-triangle-warning');
    expect(element).toHaveClass('hydrated');
  });

  it('renders changes to the name data', async () => {
    const page = await newE2EPage();

    await page.setContent('<icon-exclamation-triangle-warning></icon-exclamation-triangle-warning>');
    const component = await page.find('icon-exclamation-triangle-warning');
    const element = await page.find('icon-exclamation-triangle-warning >>> div');
    expect(element.textContent).toEqual(`Hello, World! I'm `);

    component.setProperty('first', 'James');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James`);

    component.setProperty('last', 'Quincy');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

    component.setProperty('middle', 'Earl');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
  });
});
