import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-bars',
	styleUrls: ['icon-bars.scss'],
	scoped: true
})
export class IconBars {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-bars"
				width="22"
				height="26"
				viewBox="0 0 22 26"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-bars__shape"
					d="M0.8,6.9h20.4c0.4,0,0.8-0.4,0.8-0.8v-2c0-0.4-0.4-0.8-0.8-0.8H0.8C0.4,3.4,0,3.7,0,4.2v2C0,6.6,0.4,6.9,0.8,6.9z M0.8,14.8 h20.4c0.4,0,0.8-0.4,0.8-0.8v-2c0-0.4-0.4-0.8-0.8-0.8H0.8C0.4,11.2,0,11.6,0,12v2C0,14.4,0.4,14.8,0.8,14.8z M0.8,22.6h20.4 c0.4,0,0.8-0.4,0.8-0.8v-2c0-0.4-0.4-0.8-0.8-0.8H0.8c-0.4,0-0.8,0.4-0.8,0.8v2C0,22.3,0.4,22.6,0.8,22.6z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
