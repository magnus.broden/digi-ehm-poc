import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-paperclip',
	styleUrls: ['icon-paperclip.scss'],
	scoped: true
})
export class IconPaperclip {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-paperclip"
				width="23"
				height="26"
				viewBox="0 0 23 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-paperclip__shape"
					d="M2.157 23.79c-2.876-2.947-2.876-7.715 0-10.662L13.31 1.7a5.586 5.586 0 018.026 0c2.218 2.273 2.219 5.951 0 8.224l-9.391 9.623a3.93 3.93 0 01-5.647 0 4.16 4.16 0 010-5.787l7.954-8.15a.585.585 0 01.84 0l.842.863a.62.62 0 010 .861l-7.954 8.15a1.682 1.682 0 000 2.339c.63.645 1.653.645 2.283 0L19.654 8.2a3.434 3.434 0 000-4.776 3.244 3.244 0 00-4.662 0L3.84 14.852c-1.946 1.993-1.946 5.22 0 7.214a4.895 4.895 0 007.047-.002c3.236-3.318 6.472-6.636 9.71-9.953a.585.585 0 01.84 0l.842.862a.62.62 0 010 .861 37929.73 37929.73 0 00-9.71 9.953 7.233 7.233 0 01-10.411.002z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
