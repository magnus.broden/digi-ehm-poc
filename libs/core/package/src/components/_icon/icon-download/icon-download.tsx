import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-download',
	styleUrls: ['icon-download.scss'],
	scoped: true
})
export class IconDownload {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-download"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-download__shape"
					d="M20.25,0 L27.75,0 C28.996875,0 30,1.003125 30,2.25 L30,18 L38.221875,18 C39.890625,18 40.725,20.015625 39.54375,21.196875 L25.284375,35.465625 C24.58125,36.16875 23.428125,36.16875 22.725,35.465625 L8.446875,21.196875 C7.265625,20.015625 8.1,18 9.76875,18 L18,18 L18,2.25 C18,1.003125 19.003125,0 20.25,0 Z M48,35.25 L48,45.75 C48,46.996875 46.996875,48 45.75,48 L2.25,48 C1.003125,48 0,46.996875 0,45.75 L0,35.25 C0,34.003125 1.003125,33 2.25,33 L16.003125,33 L20.596875,37.59375 C22.48125,39.478125 25.51875,39.478125 27.403125,37.59375 L31.996875,33 L45.75,33 C46.996875,33 48,34.003125 48,35.25 Z M36.375,43.5 C36.375,42.46875 35.53125,41.625 34.5,41.625 C33.46875,41.625 32.625,42.46875 32.625,43.5 C32.625,44.53125 33.46875,45.375 34.5,45.375 C35.53125,45.375 36.375,44.53125 36.375,43.5 Z M42.375,43.5 C42.375,42.46875 41.53125,41.625 40.5,41.625 C39.46875,41.625 38.625,42.46875 38.625,43.5 C38.625,44.53125 39.46875,45.375 40.5,45.375 C41.53125,45.375 42.375,44.53125 42.375,43.5 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
