# digi-icon

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute                | Description                                                                   | Type                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Default     |
| --------------------- | ------------------------ | ----------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------- |
| `afDesc`              | `af-desc`                | Lägger till ett descelement i svg:n                                           | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | `undefined` |
| `afName`              | `af-name`                |                                                                               | `"search" \| "chevron-left" \| "bars" \| "check-circle-reg-alt" \| "check" \| "chevron-down" \| "chevron-right" \| "chevron-up" \| "copy" \| "danger-outline" \| "download" \| "exclamation-circle-filled" \| "exclamation-triangle-warning" \| "exclamation-triangle" \| "external-link-alt" \| "input-select-marker" \| "minus" \| "notification-error" \| "notification-info" \| "notification-succes" \| "notification-warning" \| "paperclip" \| "plus" \| "spinner" \| "trash" \| "validation-error" \| "validation-success" \| "validation-warning" \| "x" \| "calendar-alt"` | `undefined` |
| `afSvgAriaHidden`     | `af-svg-aria-hidden`     | För att dölja ikonen för skärmläsare. Default är satt till true.              | `boolean`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | `true`      |
| `afSvgAriaLabelledby` | `af-svg-aria-labelledby` | Referera till andra element på sidan för att definiera ett tillgängligt namn. | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | `undefined` |
| `afTitle`             | `af-title`               | Lägger till ett titleelement i svg:n                                          | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | `undefined` |


## CSS Custom Properties

| Name                   | Description                         |
| ---------------------- | ----------------------------------- |
| `--digi--icon--color`  | var(--digi--color--icons--primary); |
| `--digi--icon--height` | auto;                               |
| `--digi--icon--width`  | initial;                            |


## Dependencies

### Used by

 - [digi-calendar](../../_calendar/calendar)
 - [digi-calendar-week-view](../../_calendar/week-view)
 - [digi-code-block](../../_code/code-block)
 - [digi-code-example](../../_code/code-example)
 - [digi-expandable-accordion](../../_expandable/expandable-accordion)
 - [digi-form-filter](../../_form/form-filter)
 - [digi-form-input-search](../../_form/form-input-search)
 - [digi-form-select](../../_form/form-select)
 - [digi-form-validation-message](../../_form/form-validation-message)
 - [digi-link-external](../../_link/link-external)
 - [digi-link-internal](../../_link/link-internal)
 - [digi-loader-spinner](../../_loader/loader-spinner)
 - [digi-navigation-context-menu](../../_navigation/navigation-context-menu)
 - [digi-navigation-sidebar](../../_navigation/navigation-sidebar)
 - [digi-navigation-sidebar-button](../../_navigation/navigation-sidebar-button)
 - [digi-navigation-vertical-menu-item](../../_navigation/navigation-vertical-menu-item)
 - [digi-tag](../../_tag/tag)

### Graph
```mermaid
graph TD;
  digi-calendar --> digi-icon
  digi-calendar-week-view --> digi-icon
  digi-code-block --> digi-icon
  digi-code-example --> digi-icon
  digi-expandable-accordion --> digi-icon
  digi-form-filter --> digi-icon
  digi-form-input-search --> digi-icon
  digi-form-select --> digi-icon
  digi-form-validation-message --> digi-icon
  digi-link-external --> digi-icon
  digi-link-internal --> digi-icon
  digi-loader-spinner --> digi-icon
  digi-navigation-context-menu --> digi-icon
  digi-navigation-sidebar --> digi-icon
  digi-navigation-sidebar-button --> digi-icon
  digi-navigation-vertical-menu-item --> digi-icon
  digi-tag --> digi-icon
  style digi-icon fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
