import { Component, h, Prop, State, Watch } from '@stencil/core';
import { icon, IconName } from '../../../global/icon/icon';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

/**
 *
 * @swedishName Ikon
 */
@Component({
	tag: 'digi-icon',
	styleUrl: 'icon.scss',
	scoped: false
})
export class Icon {
	@State() currentIcon;

	@Prop() afName: IconName;
	@Watch('afName')
	setIcon() {
		const { IconComponent } = icon[this.afName];
		this.currentIcon = IconComponent;
	}
	@State() labelledby: string = undefined;
	setAriaLabelledby() {
		if (this.afTitle && this.afTitle !== "") {
			if(this.afSvgAriaLabelledby && this.afSvgAriaLabelledby !== ""){
				this.labelledby = this.afSvgAriaLabelledby;
			}else{
				this.labelledby = this.titleId;
			}
		}
	}

	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
	@Prop() afSvgAriaLabelledby: string;
	
	@State() titleId: string = randomIdGenerator('icontitle');


	componentWillLoad() {
		this.setIcon();
		this.setAriaLabelledby();
	}

	render() {
		return this.currentIcon ? (
			<this.currentIcon afSvgAriaHidden={this.afSvgAriaHidden} afSvgAriaLabelledby={this.labelledby}>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
			</this.currentIcon>
		) : null;
	}
}
