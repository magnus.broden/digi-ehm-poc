import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-danger-outline',
	styleUrl: 'icon-danger-outline.scss',
	scoped: true
})
export class IconDangerOutline {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-danger-outline"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-danger-outline__shape"
					d="M16.3083126,13.5575621 C17.212521,12.6758465 18.6746453,12.6758465 19.4922806,13.5575621 L24.0133226,18.0230248 L28.6209379,13.5575621 C29.5251463,12.6758465 30.9872705,12.6758465 31.8049058,13.5575621 C32.7860681,14.4487585 32.7860681,15.889842 31.8049058,16.6957111 L27.3608176,21.151693 L31.8049058,25.6930023 C32.7860681,26.5841986 32.7860681,28.0252822 31.8049058,28.8311512 C30.9872705,29.7981941 29.5251463,29.7981941 28.6209379,28.8311512 L24.0133226,24.4510158 L19.4922806,28.8311512 C18.6746453,29.7981941 17.212521,29.7981941 16.3083126,28.8311512 C15.4137234,28.0252822 15.4137234,26.5841986 16.3083126,25.6930023 L20.8389739,21.151693 L16.3083126,16.6957111 C15.4137234,15.889842 15.4137234,14.4487585 16.3083126,13.5575621 L16.3083126,13.5575621 Z M9.87119353,3.24 C11.1166068,1.228125 13.3583507,0 15.6767354,0 L32.240732,0 C34.6453376,0 36.8870815,1.228125 38.1324948,3.24 L46.9940893,17.49375 C48.3353036,19.65 48.3353036,22.35 46.9940893,24.50625 L38.1324948,38.75625 C36.8870815,40.771875 34.6453376,42 32.240732,42 L15.6767354,42 C13.3583507,42 11.1166068,40.771875 9.87119353,38.75625 L1.00576702,24.50625 C-0.335255674,22.35 -0.335255674,19.65 1.00576702,17.49375 L9.87119353,3.24 Z M4.95376353,20.0519187 C4.50454509,20.7819413 4.50454509,21.6920993 4.95376353,22.4221219 L13.8554068,36.8329571 C14.2690341,37.5060948 15.0193347,37.9232506 15.7407776,37.9232506 L32.3724409,37.9232506 C33.1804569,37.9232506 33.9307575,37.5060948 34.3443848,36.8329571 L43.2421804,22.4221219 C43.6942846,21.6920993 43.6942846,20.7819413 43.2421804,20.0519187 L34.3443848,5.64297968 C33.9307575,4.96510158 33.1804569,4.55079007 32.3724409,4.55079007 L15.7407776,4.55079007 C15.0193347,4.55079007 14.2690341,4.96510158 13.8554068,5.64297968 L4.95376353,20.0519187 Z M38.2882725,3.27656885 L34.3443848,5.64297968 L38.2882725,3.27656885 Z M1.00987575,24.7828442 L4.95376353,22.4221219 L1.00987575,24.7828442 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
