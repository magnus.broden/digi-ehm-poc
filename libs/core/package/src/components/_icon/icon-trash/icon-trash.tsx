import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-trash',
	styleUrls: ['icon-trash.scss'],
	scoped: true
})
export class Icontrash {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-trash"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-trash__shape"
					d="M28.3065836,39.0000003 L30.4710557,39.0000003 C31.0687581,39.0000003 31.5532917,38.4963197 31.5532917,37.8749994 L31.5532917,17.6250003 C31.5532917,17.00368 31.0687581,16.5000003 30.4710557,16.5000003 L28.3065836,16.5000003 C27.7088812,16.5000003 27.2243476,17.00368 27.2243476,17.6250003 L27.2243476,37.8749994 C27.2243476,38.4963197 27.7088812,39.0000003 28.3065836,39.0000003 Z M43.0971425,7.50000051 L35.6648867,7.50000051 L32.5985513,2.18437593 C31.8157803,0.828519874 30.4058526,-0.000752023508 28.884678,5.11738477e-07 L19.7920918,5.11738477e-07 C18.2715602,5.11738477e-07 16.8624625,0.829093174 16.0800223,2.18437593 L13.0118832,7.50000051 L5.57962735,7.50000051 C4.78269076,7.50000051 4.136646,8.17157355 4.136646,9.00000051 L4.136646,10.5000005 C4.136646,11.3284277 4.78269076,12.0000005 5.57962735,12.0000005 L7.0226087,12.0000005 L7.0226087,43.5000005 C7.0226087,45.9852804 8.96074297,48.0000005 11.3515528,48.0000005 L37.3252171,48.0000005 C39.7160269,48.0000005 41.6541611,45.9852804 41.6541611,43.5000005 L41.6541611,12.0000005 L43.0971425,12.0000005 C43.8940791,12.0000005 44.5401238,11.3284277 44.5401238,10.5000005 L44.5401238,9.00000051 C44.5401238,8.17157355 43.8940791,7.50000051 43.0971425,7.50000051 Z M19.6342657,4.77281332 C19.7323744,4.60306234 19.9091243,4.49944487 20.0996272,4.49999861 L28.5771426,4.49999861 C28.7673243,4.49977491 28.9436588,4.60334956 29.0416023,4.77281332 L30.6162557,7.49999861 L18.0605142,7.49999861 L19.6342657,4.77281332 Z M37.3252171,43.5000005 L11.3515528,43.5000005 L11.3515528,12.0000005 L37.3252171,12.0000005 L37.3252171,43.5000005 Z M18.2057142,39.0000003 L20.3701862,39.0000003 C20.9678886,39.0000003 21.4524222,38.4963197 21.4524222,37.8749994 L21.4524222,17.6250003 C21.4524222,17.00368 20.9678886,16.5000003 20.3701862,16.5000003 L18.2057142,16.5000003 C17.6080117,16.5000003 17.1234782,17.00368 17.1234782,17.6250003 L17.1234782,37.8749994 C17.1234782,38.4963197 17.6080117,39.0000003 18.2057142,39.0000003 Z"
					fill="currentColor"
					fill-rule="nonzero"
				//transform="translate(-12.000000, -8.000000)"
				/>
			</svg>
		);
	}
}
