export enum ProgressStepVariation {
    PRIMARY = "primary",
    SECONDARY = "secondary"
}