export enum ProgressStepStatus {
    CURRENT= "current",
     UPCOMING= "upcoming",
     DONE= "done"
    }