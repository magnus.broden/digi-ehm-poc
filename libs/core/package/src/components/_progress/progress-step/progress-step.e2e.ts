import { newE2EPage } from '@stencil/core/testing';

describe('digi-progress-steps', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-progress-steps></digi-progress-steps>');

    const element = await page.find('digi-progress-steps');
    expect(element).toHaveClass('hydrated');
  });
});
