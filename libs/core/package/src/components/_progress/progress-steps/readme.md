# digi-progress-steps



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                              | Type                                                                                                                                                                                           | Default                          |
| ---------------- | ------------------ | -------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| `afCurrentStep`  | `af-current-step`  | Sätter nuvarande steg                                    | `number`                                                                                                                                                                                       | `1`                              |
| `afHeadingLevel` | `af-heading-level` | Sätter rubriknivå på varje förloppsteg. 'h2' är förvalt. | `ProgressStepsHeadingLevel.H1 \| ProgressStepsHeadingLevel.H2 \| ProgressStepsHeadingLevel.H3 \| ProgressStepsHeadingLevel.H4 \| ProgressStepsHeadingLevel.H5 \| ProgressStepsHeadingLevel.H6` | `ProgressStepsHeadingLevel.H2`   |
| `afVariation`    | `af-variation`     | Sätter variant. Kan vara 'Primary' eller 'Secondary'     | `ProgressStepsVariation.PRIMARY \| ProgressStepsVariation.SECONDARY`                                                                                                                           | `ProgressStepsVariation.PRIMARY` |


## Events

| Event       | Description                                                                     | Type               |
| ----------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnReady` | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |


## Methods

### `afMNext() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `afMPrevious() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [digi-typography](../../_typography/typography)

### Graph
```mermaid
graph TD;
  digi-progress-steps --> digi-typography
  style digi-progress-steps fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
