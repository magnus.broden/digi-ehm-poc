import {
  Component, h, Prop, Element, State, Method, Event,
  EventEmitter,
} from '@stencil/core';
import { ProgressStepsVariation } from './progress-steps-variation.enum';
import { ProgressStepsHeadingLevel } from './progress-steps-heading-level.enum';
import { ProgressStepsStatus } from './progress-steps-status.enum';
import { HTMLStencilElement } from '@stencil/core/internal';
import { logger } from '../../../global/utils/logger';

/**
 * @enums ProgressStepsVariation -progress-steps-variation.enum
 * @enums  ProgressStepsHeadingLevel - progress-steps-heading-level.enum
 *  @enums  ProgressStepsStatus - progress-steps-status.enum
 * @swedishName Förloppssteg
 */
@Component({
  tag: 'digi-progress-steps',
  styleUrl: 'progress-steps.scss',
  scoped: true
})
export class ProgressSteps {
  @Element() hostElement: HTMLStencilElement;


  /**
   * Sätter rubriknivå på varje förloppsteg. 'h2' är förvalt. 
   * @en Set heading level. Default is 'h2'
   */

  @Prop() afHeadingLevel: ProgressStepsHeadingLevel = ProgressStepsHeadingLevel.H2;

  /**
   * Sätter variant. Kan vara 'Primary' eller 'Secondary'
   * @en Set variation. Can be 'Primary' or 'Secondary'
   */
  @Prop() afVariation: ProgressStepsVariation = ProgressStepsVariation.PRIMARY;

  /**
   * Sätter nuvarande steg
   * @en Set current steps
   */
  @Prop() afCurrentStep: number = 1;
  @State() steps = [];

  @Method()
  async afMNext() {
    this.afCurrentStep += 1;
  }

  @Method()
  async afMPrevious() {
    this.afCurrentStep -= 1;
  }

  /**
  * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
  * @en When the component and slots are loaded and initialized this event will trigger.
  */
  @Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

  componentWillLoad() {
    this.getSteps();
    this.setStepProps();
  }

  componentWillUpdate() {
    this.getSteps();
    this.setStepProps();
  }

  componentDidLoad() {
    this.afOnReady.emit(); 
  }

  getSteps() {
    let steps = this.hostElement.querySelectorAll('digi-progress-step');

    if (!steps || steps.length <= 0) {
      logger.warn(`The slot contains no children elements.`, this.hostElement);
      return;
    }

    this.steps = [...Array.from(steps)].map((step) => {
      return {
        outerHTML: step.outerHTML,
        ref: step
      };
    });
  }

  setStepProps() {
    if (this.steps && this.afCurrentStep < 1) {
      logger.warn(
        `Current step is set to ${this.afCurrentStep} which is not allowed.`,
        this.hostElement
      );
      this.afCurrentStep = 1;
      return;
    }

    if (this.steps && this.afCurrentStep > this.steps.length + 1) {
      logger.warn(
        `Current step is set to ${this.afCurrentStep} which is more than the amount of available steps (${this.steps.length}).`,
        this.hostElement
      );
      this.afCurrentStep = this.steps.length + 1;
      return;
    }

    this.steps &&
      this.steps.forEach((step, index) => {
        step.ref.afVariation = this.afVariation;

        try {
          const topLevel = Number.parseInt(this.afHeadingLevel.split('h')[1]);
          step.ref.afHeadingLevel = 'h' + (topLevel);

          const isLast = index == this.steps.length - 1;
          step.ref.afIsLast = isLast;

          let status: ProgressStepsStatus;
          if (index < this.afCurrentStep - 1) {
            status = ProgressStepsStatus.DONE;
          } else if (index == this.afCurrentStep - 1) {
            status = ProgressStepsStatus.CURRENT;
          } else {
            status = ProgressStepsStatus.UPCOMING;
          }
          step.ref.afStepStatus = status;
        } catch (e) {
          // Nothing
        }
      });
  }

  render() {
    return (
      <div role="list">
        <digi-typography>

          <slot></slot>
        </digi-typography>
      </div>
    );
  }
}
