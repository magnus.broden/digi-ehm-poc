# digi-progressbar



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                                                                                                                                                                           | Type                                                             | Default                                        |
| ------------------ | -------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- | ---------------------------------------------- |
| `afCompletedSteps` | `af-completed-steps` | Sätter antal färdiga steg                                                                                                                                                                             | `number`                                                         | `undefined`                                    |
| `afId`             | `af-id`              | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                                                                                                 | `string`                                                         | `randomIdGenerator('digi-progressbar')`        |
| `afRole`           | `af-role`            | Sätter attributet 'role'. Förvalt är 'status'. Använder du komponenten både före och efter ett formulär så ange bara den ena som 'status' för att undvika att skärmläsare läser upp texten två gånger | `ProgressbarRole.NONE \| ProgressbarRole.STATUS`                 | `ProgressbarRole.STATUS`                       |
| `afStepsLabel`     | `af-steps-label`     | Beskrivande text efter siffrorna.                                                                                                                                                                     | `string`                                                         | `_t.progress.questions_answered.toLowerCase()` |
| `afStepsSeparator` | `af-steps-separator` | Text emellan antal färdiga och totala antalet steg. Som standard används "av", men kan ändras för exempelvis annat språk.                                                                             | `string`                                                         | `_t.of`                                        |
| `afTotalSteps`     | `af-total-steps`     | Sätter totala antalet steg                                                                                                                                                                            | `number`                                                         | `undefined`                                    |
| `afVariation`      | `af-variation`       | Sätter variant. Kan vara 'primary' och 'secondary'.                                                                                                                                                   | `ProgressbarVariation.PRIMARY \| ProgressbarVariation.SECONDARY` | `ProgressbarVariation.PRIMARY`                 |


## CSS Custom Properties

| Name                                                        | Description                                         |
| ----------------------------------------------------------- | --------------------------------------------------- |
| `--digi--progressbar--border-color`                         | var(--digi--color--border--primary);                |
| `--digi--progressbar--border-color--active`                 | var(--digi--color--border--success);                |
| `--digi--progressbar--border-color--secondary--active`      | var(--digi--color--border--secondary);              |
| `--digi--progressbar--border-width`                         | var(--digi--border-width--primary);                 |
| `--digi--progressbar--gap`                                  | var(--digi--padding--largest);                      |
| `--digi--progressbar--gap--large`                           | var(--digi--padding--largest-2);                    |
| `--digi--progressbar--gap--medium`                          | var(--digi--padding--largest);                      |
| `--digi--progressbar--gap--x-large`                         | var(--digi--padding--largest-2);                    |
| `--digi--progressbar--label--font-size`                     | var(--digi--typography--label--font-size--desktop); |
| `--digi--progressbar--label--margin`                        | var(--digi--gutter--medium) 0;                      |
| `--digi--progressbar--margin`                               | 0 0 var(--digi--margin--medium);                    |
| `--digi--progressbar--step--background`                     | var(--digi--color--background--primary);            |
| `--digi--progressbar--step--background--active`             | var(--digi--color--background--complementary-1);    |
| `--digi--progressbar--step--background--secondary--active`  | var(--digi--color--background--inverted-1);         |
| `--digi--progressbar--step--line--color`                    | var(--digi--color--border--primary);                |
| `--digi--progressbar--step--line--color--active`            | var(--digi--color--border--success);                |
| `--digi--progressbar--step--line--color--current`           | var(--digi--color--border--primary);                |
| `--digi--progressbar--step--line--color--secondary--active` | var(--digi--color--border--secondary);              |
| `--digi--progressbar--step--line--width`                    | var(--digi--border-width--primary);                 |
| `--digi--progressbar--step--size`                           | var(--digi--global--spacing--base);                 |
| `--digi--progressbar--step--size--active`                   | var(--digi--global--spacing--larger);               |
| `--digi--progressbar--step--size--active--large`            | var(--digi--global--spacing--largest);              |
| `--digi--progressbar--step--size--large`                    | var(--digi--global--spacing--large);                |


## Dependencies

### Depends on

- [digi-util-mutation-observer](../../_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-progressbar --> digi-util-mutation-observer
  style digi-progressbar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
