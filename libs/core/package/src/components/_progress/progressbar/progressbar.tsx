import { _t } from '@digi/shared/text';
import { Component, Element, Prop, State, h, Watch } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ProgressbarRole } from './progressbar-role.enum';
import { ProgressbarVariation } from './progressbar-variation.enum';

/**
 * @enums ProgressbarRole - progressbar-role.enum.ts
 * @enums ProgressbarVariation - progressbar-variation.enum.ts
 * @swedishName Förloppsmätare
 */
@Component({
	tag: 'digi-progressbar',
	styleUrls: ['progressbar.scss'],
	scoped: true
})
export class Progressbar {
	@Element() hostElement: HTMLStencilElement;

	private _observer;

	@State() completedSteps: number;
	@State() totalSteps = [];
	@State() listItems: any[];
	@State() hasSlottedItems: boolean;

	/**
	 * Sätter antal färdiga steg
	 * @en Set finished amount of steps
	 */
	@Prop() afCompletedSteps: number;

	/**
	 * Sätter totala antalet steg
	 * @en Set amount of total steps
	 */
	@Prop() afTotalSteps: number;

	/**
	 * Text emellan antal färdiga och totala antalet steg.
	 * Som standard används "av", men kan ändras för exempelvis annat språk.
	 * @en Separator text between current steps and total steps.
	 * Default is "av", but can be changed e.g. for different language.
	 */
	@Prop() afStepsSeparator = _t.of;

	/**
	 * Beskrivande text efter siffrorna.
	 * @en Descriptive text after step numbers.
	 */
	@Prop() afStepsLabel = _t.progress.questions_answered.toLowerCase();

	/**
	 * Sätter attributet 'role'. Förvalt är 'status'.
	 * Använder du komponenten både före och efter ett formulär så
	 * ange bara den ena som 'status' för att undvika att skärmläsare läser upp texten två gånger
	 * @en Set role attribute. Defaults to 'status'.
	 * If you apply the progressbar both before and after a form,
	 * set only role 'status' on one of them to avoid screen readers to read the text twice
	 */
	@Prop() afRole: ProgressbarRole = ProgressbarRole.STATUS;

	/**
	 * Sätter variant. Kan vara 'primary' och 'secondary'.
	 * @en Set variation of progressbar. Can be 'primary' and 'secondary'.
	 */
	@Prop() afVariation: ProgressbarVariation = ProgressbarVariation.PRIMARY;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-progressbar');

	componentWillLoad() {
		this.changeTotalStepsHandler(this.afTotalSteps);
		this.getItems();
	}

	componentDidLoad() {
		this.getItems();
	}

	@Watch('afTotalSteps')
	changeTotalStepsHandler(value: number) {
		let arr = [];
		let n = 0;
		while (n < value) {
			n++;
			arr.push(n);
		}
		this.totalSteps = arr;
	}

	@Watch('afCompletedSteps')
	changeCompletedStepsHandler(value: number) {
		this.completedSteps = value;
	}

	getItems(update?: boolean) {
		let items: any;

		if (update) {
			items = this._observer.children;
		} else {
			items = this.hostElement.querySelectorAll('.digi-progressbar__slot li');
		}

		if (items.length > 0) {
			this.hasSlottedItems = true;

			this.listItems = [...items]
				.filter((element) => element.tagName.toLowerCase() === 'li')
				.map((element) => {
					const el = element as HTMLElement;
					const checked = el.matches('.completed');

					return {
						text: element.textContent,
						checked: checked
					};
				});
		}
	}

	get cssModifiers() {
		return {
			'digi-progressbar': true,
			'digi-progressbar--primary':
				this.afVariation === ProgressbarVariation.PRIMARY,
			'digi-progressbar--secondary':
				this.afVariation === ProgressbarVariation.SECONDARY,
			'digi-progressbar--linear': !this.hasSlottedItems
		};
	}

	get slottedContent() {
		return;
	}

	render() {
		return (
			<div
				class={this.cssModifiers}
				role={
					this.afRole === ProgressbarRole.STATUS ? ProgressbarRole.STATUS : null
				}
			>
				<p class="digi-progressbar__label">
					{this.afCompletedSteps} {this.afStepsSeparator} {this.afTotalSteps}{' '}
					{this.afStepsLabel}
				</p>

				{!this.hasSlottedItems ? (
					<ol class="digi-progressbar__list" aria-hidden="true">
						{this.totalSteps.map((item, i) => {
							return (
								<li
									class={{
										'digi-progressbar__step': true,
										'digi-progressbar__step--active': this.afCompletedSteps > i,
										'digi-progressbar__step--current': this.afCompletedSteps === i + 1
									}}
								>
									<span class="digi-progressbar__step-indicator">
										<span class="digi-progressbar__step-number">{item}</span>
									</span>
								</li>
							);
						})}
					</ol>
				) : (
					<ol class="digi-progressbar__list" aria-hidden="true">
						{this.listItems.map((item, index) => {
							return (
								<li
									class={{
										'digi-progressbar__step': true,
										'digi-progressbar__step--active': item.checked
									}}
								>
									<span class="digi-progressbar__step-indicator">
										<span class="digi-progressbar__step-number">{index + 1}</span>
									</span>
								</li>
							);
						})}
					</ol>
				)}

				<div hidden class="digi-progressbar__slot">
					<digi-util-mutation-observer
						ref={(el) => (this._observer = el)}
						onAfOnChange={() => this.getItems()}
					>
						<slot></slot>
					</digi-util-mutation-observer>
				</div>
			</div>
		);
	}
}
