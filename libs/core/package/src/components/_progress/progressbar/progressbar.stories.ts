import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ProgressbarRole } from './progressbar-role.enum';
import { ProgressbarVariation } from './progressbar-variation.enum';

export default {
	title: 'progress/digi-progressbar',
	argTypes: {
		'af-role': enumSelect(ProgressbarRole),
		'af-variation': enumSelect(ProgressbarVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-progressbar',
	'af-completed-steps': null,
	'af-total-steps': null,
	'af-steps-separator': 'av',
	'af-steps-label': 'frågor besvarade',
	'af-role': ProgressbarRole.STATUS,
	'af-variation': ProgressbarVariation.PRIMARY,
	'af-id': null,
	children: ''
};
