import {
	Component,
	Element,
	Event,
	EventEmitter,
	Host,
	Method,
	Prop,
	h
} from '@stencil/core';
import { HTMLStencilElement, State, Watch } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ButtonSize } from './button-size.enum';
import { ButtonType } from './button-type.enum';
import { ButtonVariation } from './button-variation.enum';

/**
 * @slot default - Ska vara en textnod
 * @slot icon - Ska vara en ikon (eller en ikonlik) komponent
 * @slot icon-secondary - Ska vara en ikon (eller en ikonlik) komponent
 *
 * @enums ButtonSize - button-size.enum.ts
 * @enums ButtonType - button-type.enum.ts
 * @enums ButtonVariation - button-variation.enum.ts
 * @swedishName Knapp
 */
@Component({
	tag: 'digi-button',
	styleUrls: ['button.scss'],
	scoped: true
})
export class Button {
	@Element() hostElement: HTMLStencilElement;

	private _button?: HTMLButtonElement;

	@State() hasIcon: boolean;
	@State() hasIconSecondary: boolean;
	@State() isFullSize: boolean;

	/**
	 * Sätter knappens storlek. 'medium' är förvalt.
	 * @en Set button size. Defaults to medium size.
	 */
	@Prop() afSize: `${ButtonSize}` = ButtonSize.MEDIUM;

	/**
	 * Sätter attributet 'tabindex'.
	 * @en Sets a tabindex.
	 */
	@Prop() afTabindex: string;

	/**
	 * Sätt variant. Om varianten är 'tertiary' så krävs även en ikon.
	 * @en Set button variation. If set to FUNCTION, the button also needs an icon.
	 */
	@Prop() afVariation: `${ButtonVariation}` = ButtonVariation.PRIMARY;

	/**
	 * Sätt attributet 'type'.
	 * @en Set button `type` attribute.
	 */
	@Prop() afType: `${ButtonType}` = ButtonType.BUTTON;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-button');

	/**
	 * Sätt attributet 'aria-label'.
	 * @en Set button `aria-label` attribute.
	 */
	@Prop() afAriaLabel: string;

	/**
	 * Sätt attributet 'aria-labelledby'.
	 * @en Set button `aria-labelledby` attribute.
	 */
	@Prop() afAriaLabelledby: string;

	/**
	 * Sätter attributet 'aria-controls'.
	 * @en Set button `aria-controls` attribute.
	 */
	@Prop() afAriaControls: string;



	/**
	 * Sätter attributet 'form'.
	 * @en Set button `form` attribute.
	 */
	@Prop() afForm: string;

	/**
	 * Sätter attributet 'aria-pressed'.
	 * @en Set button `aria-pressed` attribute.
	 */
	@Prop({ mutable: true }) afAriaPressed: boolean;

	/**
	 * Sätter attributet 'aria-expanded'.
	 * @en Set button `aria-expanded` attribute.
	 */
	@Prop({ mutable: true }) afAriaExpanded: boolean;

	/**
	 * Sätter attributet 'aria-haspopup'.
	 * @en Set button `aria-haspopup` attribute.
	 */
	@Prop({ mutable: true }) afAriaHaspopup: boolean;

	/**
 * Sätter attributet 'aria-current'.
 * @en Set button `aria-current` attribute.
 */
	@Prop({ mutable: true }) afAriaCurrent: string;

	/**
* Sätter attributet 'role'. 
* @en Set button `role` attribute.
*/
	@Prop({ mutable: true }) afRole: string;
	/**
 * Sätter attributet 'aria-checked'. Använd till exempel tillsammans med role="menuitemradio" eller role="menuitemcheckbox"
 * @en Set button `aria-checked` attribute. For example, use this togheter with role="menuitemradio" or role="menuitemcheckbox"
 */
	@Prop({ mutable: true }) afAriaChecked: boolean;

	/**
	 * Sätter attributet 'lang'.
	 * @en Set button `lang` attribute.
	 */
	@Prop() afLang: string;

	/**
	 * Sätter attributet 'dir'.
	 * @en Set button `dir` attribute.
	 */
	@Prop() afDir: string;

	/**
	 * Sätter knappens bredd till 100%.
	 * @en Set button width to 100%.
	 */
	@Prop() afFullWidth: boolean = false;
	@Watch('afFullWidth')
	fullWidthHandler() {
		this.isFullSize = this.afFullWidth;
	}

	/**
	 * Sätter attributet 'autofocus'.
	 * @en Input autofocus attribute
	 */
	@Prop() afAutofocus: boolean;

	/**
	 * Buttonelementets 'onclick'-event.
	 * @en The button element's 'onclick' event.
	 */
	@Event() afOnClick: EventEmitter<MouseEvent>;

	/**
	 * Buttonelementets 'onfocus'-event.
	 * @en The button element's 'onfocus' event.
	 */
	@Event() afOnFocus: EventEmitter<FocusEvent>;

	/**
	 * Buttonelementets 'onblur'-event.
	 * @en The button element's 'onblur' event.
	 */
	@Event() afOnBlur: EventEmitter;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	/**
	 * Hämta en referens till buttonelementet. Bra för att t.ex. sätta fokus programmatiskt.
	 * @en Returns a reference to the button element. Handy for setting focus programmatically.
	 */
	@Method()
	async afMGetButtonElement(): Promise<HTMLButtonElement> {
		return this._button;
	}

	componentWillLoad() {
		this.setHasIcon();
		this.fullWidthHandler();
	}

	componentWillUpdate() {
		this.setHasIcon();
	}
	
	componentDidLoad(){
		this.afOnReady.emit();  
	}

	setHasIcon() {
		const icon = !!this.hostElement.querySelector('[slot="icon"]');
		const iconSecondary = !!this.hostElement.querySelector(
			'[slot="icon-secondary"]'
		);

		if (icon) {
			this.hasIcon = icon;
		}
		if (iconSecondary) {
			this.hasIconSecondary = iconSecondary;
		}
	}

	get cssModifiers() {
		return {
			[`digi-button--variation-${this.afVariation}`]: !!this.afVariation,
			[`digi-button--size-${this.afSize}`]: !!this.afSize,
			'digi-button--icon': this.hasIcon,
			'digi-button--icon-secondary': this.hasIconSecondary,
			'digi-button--full-width': this.isFullSize
		};
	}

	get pressedState() {
		if (this.afAriaPressed === null || this.afAriaPressed === undefined) {
			return;
		}
		return this.afAriaPressed ? 'true' : 'false';
	}

	get expandedState() {
		if (this.afAriaExpanded === null || this.afAriaExpanded === undefined) {
			return;
		}
		return this.afAriaExpanded ? 'true' : 'false';
	}

	get hasPopup() {
		if (this.afAriaHaspopup === null || this.afAriaHaspopup === undefined) {
			return;
		}
		return this.afAriaHaspopup ? 'true' : 'false';
	}

	clickHandler(e: MouseEvent) {
		this.afOnClick.emit(e);
	}

	focusHandler(e: FocusEvent) {
		this.afOnFocus.emit(e);
	}

	blurHandler(e) {
		this.afOnBlur.emit(e);
	}

	render() {
		return (
			<Host>
				<button
					class={{
						'digi-button': true,
						...this.cssModifiers
					}}
					type={this.afType}
					role={this.afRole}
					ref={(el) => {
						this._button = el as HTMLButtonElement;
						// Stencil has a bug wiith form attribute so we need to set it like this
						// https://github.com/ionic-team/stencil/issues/2703
						this.afForm
							? this._button?.setAttribute('form', this.afForm)
							: this._button?.removeAttribute('form');
					}}
					id={this.afId}
					tabindex={this.afTabindex}
					aria-label={this.afAriaLabel}
					aria-labelledby={this.afAriaLabelledby}
					aria-controls={this.afAriaControls}
					aria-pressed={this.pressedState}
					aria-expanded={this.expandedState}
					aria-haspopup={this.hasPopup}
					aria-current={this.afAriaCurrent}
					aria-checked={this.afAriaChecked}
					onClick={(e) => this.clickHandler(e)}
					onFocus={(e) => this.focusHandler(e)}
					onBlur={(e) => this.blurHandler(e)}
					autoFocus={this.afAutofocus ? this.afAutofocus : null}
					form={this.afForm}
					lang={this.afLang}
					dir={this.afDir}
				>
					{this.hasIcon && (
						<span class="digi-button__icon">
							<slot name="icon"></slot>
						</span>
					)}
					<span class="digi-button__text">
						<slot></slot>
					</span>

					{this.hasIconSecondary && (
						<span class="digi-button__icon digi-button__icon--secondary">
							<slot name="icon-secondary"></slot>
						</span>
					)}
				</button>
			</Host>
		);
	}
}
