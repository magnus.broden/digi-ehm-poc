export enum ButtonVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  FUNCTION = 'function'
}
