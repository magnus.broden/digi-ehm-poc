# digi-navigation-context-menu

A context menu component.

<!-- Auto Generated Below -->


## Properties

| Property                       | Attribute                          | Description                                                           | Type                                     | Default                                             |
| ------------------------------ | ---------------------------------- | --------------------------------------------------------------------- | ---------------------------------------- | --------------------------------------------------- |
| `afIcon`                       | `af-icon`                          | Sätt namn på ikonen att ladda in. (<digi-icon-VÄRDE>)                 | `string`                                 | `undefined`                                         |
| `afId`                         | `af-id`                            | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string`                                 | `randomIdGenerator('digi-navigation-context-menu')` |
| `afNavigationContextMenuItems` | `af-navigation-context-menu-items` | Sätt array istället för att använda children-slot.                    | `INavigationContextMenuItem[] \| string` | `undefined`                                         |
| `afStartSelected`              | `af-start-selected`                | Sätter det initialt valda värdet                                      | `number`                                 | `1`                                                 |
| `afText` _(required)_          | `af-text`                          | Sätter texten för toggleknappen.                                      | `string`                                 | `undefined`                                         |


## Events

| Event          | Description                                | Type               |
| -------------- | ------------------------------------------ | ------------------ |
| `afOnActive`   | När komponenten öppnas                     | `CustomEvent<any>` |
| `afOnBlur`     | När fokus sätts utanför komponenten        | `CustomEvent<any>` |
| `afOnChange`   | Vid navigering till nytt listobjekt        | `CustomEvent<any>` |
| `afOnInactive` | När komponenten stängs                     | `CustomEvent<any>` |
| `afOnSelect`   | 'onclick'-event på knappelementen i listan | `CustomEvent<any>` |
| `afOnToggle`   | Toggleknappens 'onclick'-event             | `CustomEvent<any>` |


## Slots

| Slot        | Description                                                |
| ----------- | ---------------------------------------------------------- |
| `"default"` | Ska innehålla en eller flera navigation-context-menu-item. |


## CSS Custom Properties

| Name                                                                       | Description                                                                                                                |
| -------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| `--digi--navigation-context-menu--button--background`                      | transparent;                                                                                                               |
| `--digi--navigation-context-menu--button--background--hover`               | transparent;                                                                                                               |
| `--digi--navigation-context-menu--button--border`                          | 0;                                                                                                                         |
| `--digi--navigation-context-menu--button--bordered--padding`               | var(--digi--gutter--large) var(--digi--gutter--large) var(--digi--gutter--large) var(--digi--gutter--largest-3);           |
| `--digi--navigation-context-menu--button--color`                           | var(--digi--color--text--primary);                                                                                         |
| `--digi--navigation-context-menu--button--display`                         | flex;                                                                                                                      |
| `--digi--navigation-context-menu--button--font-size`                       | var(--digi--typography--body--font-size--desktop);                                                                         |
| `--digi--navigation-context-menu--button--font-size--desktop`              | var(--digi--typography--body--font-size--desktop-large);                                                                   |
| `--digi--navigation-context-menu--button--font-weight`                     | normal;                                                                                                                    |
| `--digi--navigation-context-menu--button--outline--focus`                  | none;                                                                                                                      |
| `--digi--navigation-context-menu--button--padding`                         | var(--digi--gutter--smallest-2) var(--digi--gutter--large) var(--digi--gutter--smallest-2) var(--digi--gutter--largest-3); |
| `--digi--navigation-context-menu--button--text-align`                      | left;                                                                                                                      |
| `--digi--navigation-context-menu--button--width`                           | 100%;                                                                                                                      |
| `--digi--navigation-context-menu--button-icon--display`                    | inline-flex;                                                                                                               |
| `--digi--navigation-context-menu--button-icon--margin-left`                | 0.25rem;                                                                                                                   |
| `--digi--navigation-context-menu--button-icon--margin-right`               | -1.125rem;                                                                                                                 |
| `--digi--navigation-context-menu--button-icon--width`                      | 0.875rem;                                                                                                                  |
| `--digi--navigation-context-menu--button-trigger--background-color`        | transparent;                                                                                                               |
| `--digi--navigation-context-menu--button-trigger--background-color--hover` | transparent;                                                                                                               |
| `--digi--navigation-context-menu--button-trigger--border-radius`           | initial;                                                                                                                   |
| `--digi--navigation-context-menu--button-trigger--border-radius--focus`    | initial;                                                                                                                   |
| `--digi--navigation-context-menu--button-trigger--border-radius--hover`    | initial;                                                                                                                   |
| `--digi--navigation-context-menu--button-trigger--font-size`               | var(--digi--typography--body--font-size--desktop);                                                                         |
| `--digi--navigation-context-menu--button-trigger--font-size--desktop`      | var(--digi--typography--body--font-size--desktop-large);                                                                   |
| `--digi--navigation-context-menu--button-trigger--padding`                 | 0;                                                                                                                         |
| `--digi--navigation-context-menu--content--background-color`               | var(--digi--color--background--primary);                                                                                   |
| `--digi--navigation-context-menu--content--border-radius`                  | var(--digi--border-radius--secondary);                                                                                     |
| `--digi--navigation-context-menu--content--box-shadow`                     | 0 0.125rem 0.375rem 0 rgba(0, 0, 0, 0.7);                                                                                  |
| `--digi--navigation-context-menu--content--position`                       | absolute;                                                                                                                  |
| `--digi--navigation-context-menu--item--background-color--active`          | var(--digi--color--background--neutral-6);                                                                                 |
| `--digi--navigation-context-menu--item--border-top`                        | var(--digi--border-width--primary) solid var(--digi--color--border--neutral-4);                                            |
| `--digi--navigation-context-menu--item--display`                           | flex;                                                                                                                      |
| `--digi--navigation-context-menu--item--font-family`                       | var(--digi--global--typography--font-family--default);                                                                     |
| `--digi--navigation-context-menu--item--font-size`                         | var(--digi--typography--body--font-size--desktop);                                                                         |
| `--digi--navigation-context-menu--item--font-size--desktop`                | var(--digi--typography--body--font-size--desktop-large);                                                                   |
| `--digi--navigation-context-menu--item--font-wight--selected`              | 600;                                                                                                                       |
| `--digi--navigation-context-menu--items--margin`                           | 0;                                                                                                                         |
| `--digi--navigation-context-menu--items--padding`                          | var(--digi--gutter--large) 0;                                                                                              |
| `--digi--navigation-context-menu--link--bordered--padding`                 | var(--digi--gutter--large);                                                                                                |
| `--digi--navigation-context-menu--link--color`                             | var(--digi--color--text--primary);                                                                                         |
| `--digi--navigation-context-menu--link--display`                           | inline-block;                                                                                                              |
| `--digi--navigation-context-menu--link--focus--outline`                    | none;                                                                                                                      |
| `--digi--navigation-context-menu--link--font-family`                       | var(--digi--global--typography--font-family--default);                                                                     |
| `--digi--navigation-context-menu--link--font-size`                         | var(--digi--typography--body--font-size--desktop);                                                                         |
| `--digi--navigation-context-menu--link--font-size--desktop`                | var(--digi--typography--body--font-size--desktop-large);                                                                   |
| `--digi--navigation-context-menu--link--padding`                           | var(--digi--gutter--smallest-2) var(--digi--gutter--large);                                                                |
| `--digi--navigation-context-menu--link--text-decoration`                   | none;                                                                                                                      |
| `--digi--navigation-context-menu--link--width`                             | 100%;                                                                                                                      |
| `--digi--navigation-context-menu--toggle-icon--transform`                  | rotate(-180deg);                                                                                                           |
| `--digi--navigation-context-menu--toggle-icon--transition`                 | ease-in-out 0.2s all;                                                                                                      |


## Dependencies

### Depends on

- [digi-util-keydown-handler](../../_util/util-keydown-handler)
- [digi-button](../../_button/button)
- [digi-icon](../../_icon/icon)
- [digi-util-mutation-observer](../../_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-navigation-context-menu --> digi-util-keydown-handler
  digi-navigation-context-menu --> digi-button
  digi-navigation-context-menu --> digi-icon
  digi-navigation-context-menu --> digi-util-mutation-observer
  style digi-navigation-context-menu fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
