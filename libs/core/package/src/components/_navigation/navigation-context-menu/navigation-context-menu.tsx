import {
	Component,
	Element,
	State,
	Event,
	EventEmitter,
	Listen,
	Prop,
	h
} from '@stencil/core';
import { HTMLStencilElement, Watch } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { NavigationContextMenuItemType } from '../navigation-context-menu-item/navigation-context-menu-item-type.enum';
import { INavigationContextMenuItem } from './navigation-context-menu.interface';

/**
 * @slot default - Ska innehålla en eller flera navigation-context-menu-item.
 * @swedishName Rullgardinsmeny
 */
@Component({
	tag: 'digi-navigation-context-menu',
	styleUrls: ['navigation-context-menu.scss'],
	scoped: true
})
export class NavigationContextMenu {
	@Element() hostElement: HTMLStencilElement;

	@State() selectedListItemIndex: number = 0;
	@State() isActive: boolean = false;
	@State() activeListItemIndex: number = 1;

	/**
	 * Sätter texten för toggleknappen.
	 * @en Set text for toggle button
	 */
	@Prop() afText!: string;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-context-menu');

	/**
	 * Sätter det initialt valda värdet
	 * @en Sets initial selected value
	 */
	@Prop() afStartSelected: number = 1;

	/**
	 * Sätt array istället för att använda children-slot.
	 * @en Set array instead of using children slot.
	 */
	@Prop() afNavigationContextMenuItems: string | INavigationContextMenuItem[];
	@State() _afNavigationContextMenuItems: string | INavigationContextMenuItem[];

	/**
	 * Sätt namn på ikonen att ladda in. (<digi-icon-VÄRDE>)
	 * @en Set name on icon (<digi-icon-Value)
	 */
	@Prop() afIcon: string;

	/**
	 * När komponenten stängs
	 * @en When component gets inactive
	 */
	@Event() afOnInactive: EventEmitter;

	/**
	 * När komponenten öppnas
	 * @en When component gets active
	 */
	@Event() afOnActive: EventEmitter;

	/**
	 * När fokus sätts utanför komponenten
	 * @en When focus is move outside of component
	 */
	@Event() afOnBlur: EventEmitter;

	/**
	 * Vid navigering till nytt listobjekt
	 * @en When navigating to a new list item
	 */
	@Event() afOnChange: EventEmitter;

	/**
	 * Toggleknappens 'onclick'-event
	 * @en The toggle button's 'onclick'-event
	 */
	@Event() afOnToggle: EventEmitter;

	/**
	 * 'onclick'-event på knappelementen i listan
	 * @en List item buttons' 'onclick'-event
	 */
	@Event() afOnSelect: EventEmitter;

	public listItems: INavigationContextMenuItem[] = [];
	public digiIcon: string;

	private _toggleButton: any;
	private _observer;

	@Listen('click', { target: 'window' })
	clickHandler(e: MouseEvent): void {
		const target = e.target as HTMLElement;
		const selector = `#${this.afId}-identifier`;

		if (detectClickOutside(target, selector) && this.isActive) {
			this.setInactive();
		}
	}

	@Listen('focusin', { target: 'document' })
	focusoutHandler(e: FocusEvent): void {
		const target = e.target as HTMLElement;
		const selector = `#${this.afId}-identifier`;

		if (detectFocusOutside(target, selector) && this.isActive) {
			this.setInactive();
			this.afOnBlur.emit(e);
		}
	}

	@Watch('afIcon')
	setIcon() {
		this.digiIcon =
			this.afIcon !== undefined ? 'digi-icon-' + this.afIcon : undefined;
	}

	@Watch('afNavigationContextMenuItems')
	setComponentTag() {
		this._afNavigationContextMenuItems = this.afNavigationContextMenuItems;
		this.generateListItems(this.hostElement.children);
	}

	generateListItems(collection: HTMLCollection) {
		this.listItems = [];
		let slotChildren = Array.from(collection).filter(
			(item) =>
				item.getAttribute('slot') === null &&
				!item.classList.contains('digi-navigation-context-menu')
		);

		slotChildren.map((item, i) => {
			this.listItems.push({
				index: i + 1,
				type: item['afType'],
				text: item['afText'],
				value: item.getAttribute('data-value'),
				href: item['afHref'],
				dir: item['afDir'],
				lang: item['afLang']
			});
			item.outerHTML = '';
		});

		if (this.listItems.length === 0) {
			if (this._afNavigationContextMenuItems) {
				try {
					if (this._afNavigationContextMenuItems.constructor === Array) {
						this.listItems = this._afNavigationContextMenuItems;
					} else if (typeof this._afNavigationContextMenuItems === 'string') {
						this.listItems = JSON.parse(this._afNavigationContextMenuItems);
					} else {
						throw `Invalid type in "navigation-context-menu-items" attribute`;
					}

					/**
					 * Quick helper to set correct type depending on if value or href is used.
					 */
					const getType = (item: INavigationContextMenuItem) => {
						if (item.type) return item.type;
						if (item.href) return NavigationContextMenuItemType.LINK;
						if (item.value) return NavigationContextMenuItemType.BUTTON;
					};
					this.listItems.map((item, i) => {
						item.index = i + 1;
						item.type = getType(item);
					});
				} catch (e) {
					logger.warn(
						`Invalid JSON in "navigation-context-menu-items" attribute`,
						this.hostElement,
						e
					);
					return;
				}
			}
		}
		if (this.listItems.length === 0) {
			logger.warn(`The slot contains no items or array items.`, this.hostElement);
			return;
		}
		this.setActiveListItemIndex();

		this.selectedListItemIndex = this.afStartSelected
			? this.afStartSelected
			: this.activeListItemIndex;
	}

	componentWillLoad() {
		this._afNavigationContextMenuItems = this.afNavigationContextMenuItems;
		this.setIcon();
		this.generateListItems(this.hostElement.children);
	}

	debounce(func, timeout = 300) {
		let timer;
		return (...args) => {
			clearTimeout(timer);
			timer = setTimeout(() => {
				func.apply(this, args);
			}, timeout);
		};
	}

	setInactive(focusTrigger = false) {
		this.isActive = false;
		this._toggleButton.querySelector('button').setAttribute('tabindex', null);

		//this.setActiveListItemIndex();
		this.afOnInactive.emit();

		if (focusTrigger) {
			this._toggleButton.querySelector('button').focus();
		}
	}

	setActiveListItemIndex() {
		if (this.afStartSelected) {
			this.activeListItemIndex = this.afStartSelected;
		} else {
			this.activeListItemIndex = this.activeListItemIndex;
		}
	}

	setActive() {
		this.isActive = true;
		this._toggleButton.querySelector('button').setAttribute('tabindex', -1);

		setTimeout(() => {
			this.focusActiveItem();
		}, 100);

		this.afOnActive.emit();
	}

	toggleMenu(e) {
		const toggle = this.isActive ? this.setInactive() : this.setActive();
		this.afOnToggle.emit(e);
		return toggle;
	}

	focusActiveItem() {
		const li = document.querySelector(
			`.digi-navigation-context-menu__item:nth-child(${this.activeListItemIndex})`
		);

		const findElement = Array.from(li.children).find((el) =>
			el.getAttribute('data-type')
		);
		const elementType = findElement.getAttribute('data-type');

		if (findElement) {
			let el: HTMLElement =
				elementType === 'button'
					? findElement.querySelector(elementType)
					: (findElement as HTMLElement);

			el.focus();
			this.afOnChange.emit(findElement);
		}
	}

	homeHandler(e: any) {
		e.detail.preventDefault();
		this.activeListItemIndex = 1;
		this.focusActiveItem();
	}

	endHandler(e: any) {
		e.detail.preventDefault();
		this.activeListItemIndex = this.listItems.length;
		this.focusActiveItem();
	}

	decrementActiveItem() {
		if (this.activeListItemIndex > 1) {
			this.activeListItemIndex = this.activeListItemIndex - 1;
			this.focusActiveItem();
		}
	}

	incrementActiveItem() {
		if (this.activeListItemIndex <= this.listItems.length - 1) {
			this.activeListItemIndex = Number(this.activeListItemIndex) + 1;
			this.focusActiveItem();
		}
	}

	upHandler(e: any) {
		e.detail.preventDefault();
		this.decrementActiveItem();
	}

	downHandler(e: any) {
		e.detail.preventDefault();
		this.incrementActiveItem();
	}

	itemClickHandler(e: any, select: boolean = false) {
		if (select) {
			this.selectedListItemIndex = e.target
				.closest('.digi-navigation-context-menu__item')
				.getAttribute('data-key');
			let buttonValue = e.target
				.closest('.digi-navigation-context-menu__button')
				.getAttribute('data-value');
			this.activeListItemIndex = this.selectedListItemIndex;

			let eventvalue = buttonValue ? buttonValue : e.target.textContent;
			this.handleSelect(eventvalue);
		}

		this.focusActiveItem();
		this.setInactive(true);
	}

	handleSelect(value: string) {
		this.afOnSelect.emit(value);
	}

	showCheckIcon(i: number, type: string) {
		return i === Number(this.selectedListItemIndex) && type === 'button'
			? true
			: false;
	}

	get cssModifiers() {
		return {
			'digi-navigation-context-menu--active': this.isActive
		};
	}

	render() {
		return (
			<div class="digi-navigation-context-menu" id={`${this.afId}-identifier`}>
				<digi-util-keydown-handler onAfOnEsc={() => this.setInactive(true)}>
					<digi-button
						ref={(el) => (this._toggleButton = el)}
						onClick={(e) => this.toggleMenu(e)}
						id={`${this.afId}-trigger`}
						af-variation="function"
						af-aria-controls={this.afId}
						af-aria-expanded={this.isActive || null}
						

						class={{
							'digi-navigation-context-menu__trigger-button': true,
							...this.cssModifiers
						}}
					>
						<this.digiIcon slot="icon"></this.digiIcon>
						{this.afText}
						<digi-icon
							class="digi-navigation-context-menu__toggle-icon"
							slot="icon-secondary"
							afName={`chevron-down`}
						></digi-icon>
					</digi-button>
					<div class="digi-navigation-context-menu__content" hidden={!this.isActive}>
						<digi-util-keydown-handler
							onAfOnHome={(e) => this.homeHandler(e)}
							onAfOnEnd={(e) => this.endHandler(e)}
							onAfOnUp={(e) => this.upHandler(e)}
							onAfOnDown={(e) => this.downHandler(e)}
							onAfOnTab={() => this.setInactive()}
							onAfOnShiftTab={() => this.setInactive()}
						>
							<ul
								id={this.afId}
								class={{
									'digi-navigation-context-menu__items': true,
									'digi-navigation-context-menu__items--bordered':
										this.listItems.length > 4
								}}
								role="menu"
								aria-describedby={`${this.afId}-trigger`}
							>
								{this.listItems.map((item) => {
									return (
										<li
											tabindex="-1"
											class={{
												'digi-navigation-context-menu__item': true,
												'digi-navigation-context-menu__item--active':
													Number(item.index) === Number(this.activeListItemIndex),
												'digi-navigation-context-menu__item--selected':
													Number(item.index) === Number(this.selectedListItemIndex)
											}}
											key={item.index}
											data-key={item.index}
											dir={item.dir}
										>
											{this.showCheckIcon(item.index, item.type) && (
												<digi-icon
													class="digi-navigation-context-menu__button-icon"
													slot="icon"
													afName={`check`}
												></digi-icon>
											)}
											{item.type === 'button' && (
												<digi-button
													af-tabindex="-1"
													afRole="menuitemradio"
													data-type={item.type}
													data-value={item.value}
													class="digi-navigation-context-menu__button"
													afDir={item.dir}
													afLang={item.lang}
													onClick={(e) => this.itemClickHandler(e, true)}
													afAriaChecked={this.showCheckIcon(item.index, item.type)}
												>
													{item.text}
												</digi-button>
											)}
											{item.type === 'link' && (
												<a
													tabindex="-1"
													role="menuitem"
													class="digi-navigation-context-menu__link"
													href={item.href}
													data-type={item.type}
													onClick={(e) => this.itemClickHandler(e)}
													dir={item.dir}
													lang={item.lang}
												>
													{item.text}
												</a>
											)}
										</li>
									);
								})}
							</ul>
						</digi-util-keydown-handler>
					</div>
				</digi-util-keydown-handler>
				<digi-util-mutation-observer
					hidden
					ref={(el) => (this._observer = el)}
					onAfOnChange={this.debounce(() => {
						this.generateListItems(this._observer.children);
					})}
				>
					<slot></slot>
				</digi-util-mutation-observer>
			</div>
		);
	}
}
