export interface INavigationContextMenuItem {
	index?: number;
	text: string; //Namn på  texten som visas i dropdownen.
	type?: string; // "Button"|"Link"
	value?: string; // value sets type Button
	href?: string; //Url sets type Link
	lang: string; // Vilket språk är ovan text (2-letter isokod)
	dir: string; // "LTR" | "RTL"
}