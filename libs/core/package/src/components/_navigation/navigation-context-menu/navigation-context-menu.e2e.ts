import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-context-menu', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-context-menu></digi-navigation-context-menu>');

    const element = await page.find('digi-navigation-context-menu');
    expect(element).toHaveClass('hydrated');
  });
});
