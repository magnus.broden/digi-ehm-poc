import { Component, Prop, h, Event, EventEmitter, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
/**
 * @swedishName Sidofältsknapp
 */

@Component({
	tag: 'digi-navigation-sidebar-button',
	styleUrls: ['navigation-sidebar-button.scss'],
	scoped: true
})
export class NavigationSidebarButton {
	@State() isActive: boolean = false;

	/**
	 * Sätter texten för toggleknappen.
	 * @en Set text for toggle button
	 */
	@Prop() afText: string;

	/**
	 * Sätter attributet 'aria-labelledby'.
	 * @en Input aria-labelledby attribute
	 */
	@Prop() afAriaLabel: string;

	/**
	 * Sätter attributet 'id' på det omslutande elementet. Ges inget värde så genereras ett slumpmässigt.
	 * @en Label id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-sidebar-button');

	/**
	 * Toggleknappens 'onclick'-event
	 * @en The toggle button's 'onclick'-event
	 */
	@Event() afOnToggle: EventEmitter;

	toggleHandler() {
		this.isActive = !this.isActive;
		this.afOnToggle.emit(this.isActive);
	}

	render() {
		return (
			<digi-button
				id={this.afId}
				class="digi-navigation-sidebar-button"
				af-variation="function"
				af-aria-label={this.afAriaLabel}
				onClick={() => this.toggleHandler()}
			>
				{this.afText}
				<digi-icon slot="icon" afName={`bars`}></digi-icon>
			</digi-button>
		);
	}
}
