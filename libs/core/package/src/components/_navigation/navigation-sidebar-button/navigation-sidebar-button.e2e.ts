import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-sidebar-button', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-sidebar-button></digi-navigation-sidebar-button>');

    const element = await page.find('digi-navigation-sidebar-button');
    expect(element).toHaveClass('hydrated');
  });
});
