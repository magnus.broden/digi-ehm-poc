import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-sidebar', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-sidebar></digi-navigation-sidebar>');

    const element = await page.find('digi-navigation-sidebar');
    expect(element).toHaveClass('hydrated');
  });
});
