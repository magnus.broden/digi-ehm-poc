# digi-navigation-sidebar

A layout component specifically tailored for the vertical navigation component.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { NavigationSidebarVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                  | Attribute                    | Description                                                                                      | Type                                                                                                                                                                                                                   | Default                                             |
| ------------------------- | ---------------------------- | ------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------- |
| `afActive`                | `af-active`                  | Ändrar aktiv status                                                                              | `boolean`                                                                                                                                                                                                              | `false`                                             |
| `afBackdrop`              | `af-backdrop`                | Lägger en skugga bakom menyn som täcker sidinnehållet när menyn är aktiv                         | `boolean`                                                                                                                                                                                                              | `true`                                              |
| `afCloseButtonAriaLabel`  | `af-close-button-aria-label` | Sätt attributet 'aria-label' på stäng-knappen                                                    | `string`                                                                                                                                                                                                               | `undefined`                                         |
| `afCloseButtonPosition`   | `af-close-button-position`   | Justerar stäng-knappens position                                                                 | `NavigationSidebarCloseButtonPosition.END \| NavigationSidebarCloseButtonPosition.START`                                                                                                                               | `NavigationSidebarCloseButtonPosition.START`        |
| `afCloseButtonText`       | `af-close-button-text`       | Stäng-knappens text                                                                              | `string`                                                                                                                                                                                                               | `undefined`                                         |
| `afCloseFocusableElement` | `af-close-focusable-element` | Element som ska få fokus när menyn stängs. Sätts ingen så får knappen som öppnade menyn fokus    | `string`                                                                                                                                                                                                               | `undefined`                                         |
| `afFocusableElement`      | `af-focusable-element`       | Element som ska få fokus när menyn öppnas. Sätts ingen så får rubriken eller stäng-knappen fokus | `string`                                                                                                                                                                                                               | `undefined`                                         |
| `afHeading`               | `af-heading`                 | Rubrikens text                                                                                   | `string`                                                                                                                                                                                                               | `undefined`                                         |
| `afHeadingLevel`          | `af-heading-level`           | Sätt rubrikens vikt. 'h2' är förvalt.                                                            | `NavigationSidebarHeadingLevel.H1 \| NavigationSidebarHeadingLevel.H2 \| NavigationSidebarHeadingLevel.H3 \| NavigationSidebarHeadingLevel.H4 \| NavigationSidebarHeadingLevel.H5 \| NavigationSidebarHeadingLevel.H6` | `NavigationSidebarHeadingLevel.H2`                  |
| `afHideHeader`            | `af-hide-header`             | Dölj rubrikdelen i sidofältet                                                                    | `boolean`                                                                                                                                                                                                              | `undefined`                                         |
| `afId`                    | `af-id`                      | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                            | `string`                                                                                                                                                                                                               | `randomIdGenerator('digi-navigation-context-menu')` |
| `afMobilePosition`        | `af-mobile-position`         | Positionerar menyn i mobilen. Sätter du inget blir det samma som för större skärmar.             | `NavigationSidebarMobilePosition.END \| NavigationSidebarMobilePosition.START`                                                                                                                                         | `undefined`                                         |
| `afMobileVariation`       | `af-mobile-variation`        | Sätt variant för mobilen.                                                                        | `NavigationSidebarMobileVariation.DEFAULT \| NavigationSidebarMobileVariation.FULLWIDTH`                                                                                                                               | `NavigationSidebarMobileVariation.DEFAULT`          |
| `afPosition`              | `af-position`                | Positionerar menyn                                                                               | `NavigationSidebarPosition.END \| NavigationSidebarPosition.START`                                                                                                                                                     | `NavigationSidebarPosition.START`                   |
| `afStickyHeader`          | `af-sticky-header`           | Låser rubrikdelen mot toppen                                                                     | `boolean`                                                                                                                                                                                                              | `undefined`                                         |
| `afVariation`             | `af-variation`               | Sätt variant                                                                                     | `NavigationSidebarVariation.OVER \| NavigationSidebarVariation.PUSH \| NavigationSidebarVariation.STATIC`                                                                                                              | `NavigationSidebarVariation.OVER`                   |


## Events

| Event               | Description                                            | Type               |
| ------------------- | ------------------------------------------------------ | ------------------ |
| `afOnBackdropClick` | Stängning av sidofält med klick på skuggan bakom menyn | `CustomEvent<any>` |
| `afOnClose`         | Stängknappens 'onclick'-event                          | `CustomEvent<any>` |
| `afOnEsc`           | Stängning av sidofält med esc-tangenten                | `CustomEvent<any>` |
| `afOnToggle`        | Vid öppning/stängning av sidofält                      | `CustomEvent<any>` |


## Slots

| Slot        | Description                        |
| ----------- | ---------------------------------- |
| `"default"` | Ska innehålla navigationskomponent |
| `"footer"`  | Kan innehålla vad som helst        |


## CSS Custom Properties

| Name                                                           | Description                                                                     |
| -------------------------------------------------------------- | ------------------------------------------------------------------------------- |
| `--digi--navigation-sidebar--backdrop--background`             | rgba(0, 0, 0, 0.7);                                                             |
| `--digi--navigation-sidebar--backdrop--transition`             | all var(--digi--animation--duration--base);                                     |
| `--digi--navigation-sidebar--backdrop--z-index`                | 1998;                                                                           |
| `--digi--navigation-sidebar--close-button--padding`            | 0;                                                                              |
| `--digi--navigation-sidebar--header--border`                   | solid var(--digi--border-width--primary) var(--digi--color--border--neutral-4); |
| `--digi--navigation-sidebar--header--padding`                  | var(--digi--padding--medium);                                                   |
| `--digi--navigation-sidebar--heading--color`                   | var(--digi--color--text--primary);                                              |
| `--digi--navigation-sidebar--heading--font-family`             | var(--digi--global--typography--font-family--default);                          |
| `--digi--navigation-sidebar--heading--font-size`               | var(--digi--typography--heading-3--font-size--desktop);                         |
| `--digi--navigation-sidebar--heading--font-size--desktop`      | var(--digi--typography--heading-3--font-size--desktop-large);                   |
| `--digi--navigation-sidebar--heading--font-weight`             | var(--digi--typography--heading-3--font-weight--desktop);                       |
| `--digi--navigation-sidebar--wrapper--background-color`        | var(--digi--color--background--primary);                                        |
| `--digi--navigation-sidebar--wrapper--border`                  | solid var(--digi--border-width--primary) var(--digi--color--border--primary);   |
| `--digi--navigation-sidebar--wrapper--box-shadow`              | 0 0.125rem 0.375rem 0 rgba(0, 0, 0, 0.7);                                       |
| `--digi--navigation-sidebar--wrapper--box-shadow--backdrop`    | 0 0 3.125rem rgba(0, 0, 0, 0.35);                                               |
| `--digi--navigation-sidebar--wrapper--margin`                  | 0 0 0 -21.875rem;                                                               |
| `--digi--navigation-sidebar--wrapper--padding`                 | 0;                                                                              |
| `--digi--navigation-sidebar--wrapper--position`                | fixed;                                                                          |
| `--digi--navigation-sidebar--wrapper--transition`              | all var(--digi--animation--duration--base);                                     |
| `--digi--navigation-sidebar--wrapper--width`                   | 21.875rem;                                                                      |
| `--digi--navigation-sidebar--wrapper--width--small`            | 90vw;                                                                           |
| `--digi--navigation-sidebar--wrapper--width--small--fullwidth` | 100%;                                                                           |
| `--digi--navigation-sidebar--wrapper--z-index`                 | 1999;                                                                           |


## Dependencies

### Depends on

- [digi-util-breakpoint-observer](../../_util/util-breakpoint-observer)
- [digi-util-keydown-handler](../../_util/util-keydown-handler)
- [digi-button](../../_button/button)
- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-navigation-sidebar --> digi-util-breakpoint-observer
  digi-navigation-sidebar --> digi-util-keydown-handler
  digi-navigation-sidebar --> digi-button
  digi-navigation-sidebar --> digi-icon
  style digi-navigation-sidebar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
