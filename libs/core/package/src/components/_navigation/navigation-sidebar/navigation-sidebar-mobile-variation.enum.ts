export enum NavigationSidebarMobileVariation {
    DEFAULT = 'default',
    FULLWIDTH = 'fullwidth'
}