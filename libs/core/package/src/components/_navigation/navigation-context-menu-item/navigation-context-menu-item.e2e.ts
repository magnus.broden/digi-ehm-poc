import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-context-menu-item', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-context-menu-item></digi-navigation-context-menu-item>');

    const element = await page.find('digi-navigation-context-menu-item');
    expect(element).toHaveClass('hydrated');
  });
});
