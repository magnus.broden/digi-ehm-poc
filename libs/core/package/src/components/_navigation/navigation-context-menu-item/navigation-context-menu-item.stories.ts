import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NavigationContextMenuItemType } from './navigation-context-menu-item-type.enum';

export default {
	title: 'navigation/digi-navigation-context-menu-item',
	argTypes: {
		'af-type': enumSelect(NavigationContextMenuItemType)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-context-menu-item',
	'af-text': 'A text is required',
	'af-href': '',
	'af-type': null,
	children: ''
};
