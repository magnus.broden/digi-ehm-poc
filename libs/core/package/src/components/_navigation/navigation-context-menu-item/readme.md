# digi-navigation-context-menu-item

This component is used by `digi-navigation-context-menu` for creating supported elements such as buttons or links.

It needs a afType value of 'button' or 'link' in order to work.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { NavigationContextMenuItemType } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute | Description                                 | Type                                                                         | Default     |
| --------------------- | --------- | ------------------------------------------- | ---------------------------------------------------------------------------- | ----------- |
| `afDir`               | `af-dir`  | Sätter attributet 'dir'.                    | `string`                                                                     | `undefined` |
| `afHref`              | `af-href` | Sätter attributet 'href'.                   | `string`                                                                     | `undefined` |
| `afLang`              | `af-lang` | Sätter attributet 'lang'.                   | `string`                                                                     | `undefined` |
| `afText` _(required)_ | `af-text` | Sätter texten i komponenten                 | `string`                                                                     | `undefined` |
| `afType` _(required)_ | `af-type` | Sätter typ. Kan vara 'link' eller 'button'. | `NavigationContextMenuItemType.BUTTON \| NavigationContextMenuItemType.LINK` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
