import {
	Component,
	Element,
	Event,
	EventEmitter,
	Prop,
	h,
	State,
	Watch
} from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

/**
 * @slot - Expects navigation-vertical-menu-item
 * @swedishName Vertikalt menyval
 */
@Component({
	tag: 'digi-navigation-vertical-menu-item',
	styleUrls: ['navigation-vertical-menu-item.scss'],
	scoped: true
})
export class NavigationVerticalMenuItem {
	@Element() hostElement: HTMLStencilElement;

	@State() hasSubnav: boolean = false;
	@State() hasIcon: boolean = false;
	@State() isActive: boolean;
	@State() activeSubnav: boolean;

	@State() listItems = [];

	private $parent: HTMLElement;
	private $subnav: HTMLElement;

	/**
	 * Sätter texten i komponenten
	 * @en Sets the item text
	 */
	@Prop() afText: string;

	/**
	 * Sätter attributet 'href'.
	 * @en Set href attribute
	 */
	@Prop() afHref: string;

	/**
	 * Sätter attributet 'aria-current'.
	 * @en Set aria-current attribute
	 */
	@Prop() afAriaCurrent: string;

	/**
	 * Ändrar aktiv status på undermenyn. Sätt som 'true' för att ha öppen från början
	 * @en Toggles subnavigation. Pass in true for default active
	 */
	@Prop() afActiveSubnav: boolean;

	/**
	 * Ändrar aktiv status
	 * @en Sets active item. Pass in true for default active
	 */
	@Prop() afActive: boolean = false;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Set id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-vertical-menu-item');

	/**
	 * Kringgår länkens vanliga beteende.
	 * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
	 * @en Override default anchor link behavior and sets focus programmatically. Should only be used if default anchor link behaviour is a problem with e.g. routing
	 */
	@Prop() afOverrideLink: boolean = false;

	/**
	 *
	 */
	@Prop() afHasSubnav: boolean = false;

	/**
	 * Länken och toggle-knappens 'onclick'-event
	 * @en Link and toggle buttons 'onclick' event
	 */
	@Event() afOnClick: EventEmitter;

	componentWillLoad() {
		this.activeSubnav = this.afActiveSubnav;
	}

	componentDidLoad() {
		this.setSubnav();
		this.setHasIcon();
		this.setHasSubnav();
		this.setIsActive();
	}

	componentWillUpdate() {
		this.setSubnav();
		this.setHasIcon();
		this.setHasSubnav();
	}

	@Watch('afActiveSubnav')
	setHasSubnav() {
		this.activeSubnav = this.activeSubnav;
		this.hasSubnav = this.activeSubnav !== undefined;

		if (this.hasSubnav) {
			if (!!this.$subnav) {
				this.$subnav.hidden = !this.activeSubnav;
			}

			this.activeSubnav
				? this.$parent?.classList.add('digi-navigation-vertical-menu__item--active')
				: this.$parent?.classList.remove(
					'digi-navigation-vertical-menu__item--active'
				);
		}
	}

	@Watch('afActive')
	setIsActive() {
		this.isActive = this.afActive;
		this.isActive
			? this.hostElement.parentElement.classList.add('active')
			: this.hostElement.parentElement.classList.remove('active');
	}

	setSubnav() {
		if (this.hasSubnav) {
			this.$subnav = this.hostElement.parentElement.querySelector('ul');
			this.$parent = this.hostElement.parentElement;
			// Check if UL-element exist or is rendered later dynamically
			if (this.$subnav) {
				this.$subnav.setAttribute('id', `${this.afId}-collapse-menu`);
			}
		}
	}

	setHasIcon() {
		const icon = !!this.hostElement.querySelector('[slot="icon"]');
		this.hasIcon = icon;
	}

	clickHandler(e: MouseEvent) {
		const el = e.target as HTMLElement;

		if (el.tagName.toLowerCase() === 'a') {
			if (this.afOverrideLink) {
				e.preventDefault();
			}
			if (
				el.classList.contains('digi-navigation-vertical-menu-item__has-subnav')
			) {
				this.activeSubnav = true;
			}
		} else {
			this.activeSubnav = !this.activeSubnav;
		}

		this.afOnClick.emit(e);
	}

	get toggleButtonText() {
		return this.activeSubnav ? 'Stäng meny' : 'Öppna meny';
	}

	get cssModifiers() {
		return {
			'digi-navigation-vertical-menu-item': true,
			'digi-navigation-vertical-menu-item--active': this.isActive,
			'digi-navigation-vertical-menu-item--active-subnav': this.activeSubnav,
			'digi-navigation-vertical-menu-item--icon': this.hasIcon
		};
	}

	render() {
		return (
			<host
				class={{
					'digi-navigation-vertical-menu-item--has-subnav': this.hasSubnav,
					'digi-navigation-vertical-menu-item--active-subnav': this.activeSubnav
				}}
			>
				<div
					class={{
						'digi-navigation-vertical-menu-item': true,
						...this.cssModifiers
					}}
				>
					{this.hasSubnav && this.afHref && (
						<div class="digi-navigation-vertical-menu-item__inner digi-navigation-vertical-menu-item__inner--combined">
							<a
								id={`${this.afId}-link`}
								href={this.afHref}
								class={{
									'digi-navigation-vertical-menu-item__link': true,
									'digi-navigation-vertical-menu-item__has-subnav': true,
									'digi-navigation-vertical-menu-item__link--active': this.isActive,
									'digi-navigation-vertical-menu-item__link--expanded': this.activeSubnav
								}}
								onClick={(e) => this.clickHandler(e)}
							>
								<div
									class="digi-navigation-vertical-menu-item__icon"
									aria-hidden="true"
									hidden={!this.hasIcon}
								>
									<slot name="icon"></slot>
								</div>
								<span class="digi-navigation-vertical-menu-item__text">
									{this.afText}
								</span>
							</a>
							<button
								type="button"
								id={`${this.afId}-toggle-button`}
								class={{
									'digi-navigation-vertical-menu-item__button digi-navigation-vertical-menu-item__button--toggle':
										true,
									'digi-navigation-vertical-menu-item__button--active': this.isActive
								}}
								onClick={(e) => this.clickHandler(e)}
								aria-controls={`${this.afId}-collapse-menu`}
								aria-expanded={this.activeSubnav ? 'true' : 'false'}
								aria-label={`${this.toggleButtonText} ${this.afText}`}
							>
								<span class="digi-navigation-vertical-menu-item__toggle-icon">
									<span>
										<digi-icon afName={this.activeSubnav ? `minus` : `plus`} />
									</span>
								</span>
							</button>
						</div>
					)}
					{this.hasSubnav && !this.afHref && (
						<div class="digi-navigation-vertical-menu-item__inner">
							<button
								id={`${this.afId}-toggle-button`}
								class="digi-navigation-vertical-menu-item__button digi-navigation-vertical-menu-item__button--only-toggle"
								onClick={(e) => this.clickHandler(e)}
								aria-controls={`${this.afId}-collapse-menu`}
								aria-expanded={this.activeSubnav ? 'true' : 'false'}
								type="button"
							>
								<span
									class="digi-navigation-vertical-menu-item__icon"
									aria-hidden="true"
									hidden={!this.hasIcon}
								>
									<slot name="icon"></slot>
								</span>
								<span class="digi-navigation-vertical-menu-item__text">
									{this.afText}
								</span>
								<span class="digi-navigation-vertical-menu-item__toggle-icon">
									<span>
										<digi-icon afName={this.activeSubnav ? `minus` : `plus`} />
									</span>
								</span>
							</button>
						</div>
					)}
					{!this.hasSubnav && (
						<div class="digi-navigation-vertical-menu-item__inner">
							{!this.isActive && (
								<a
									id={`${this.afId}-link`}
									href={this.afHref}
									class={{
										'digi-navigation-vertical-menu-item__link': true,
										'digi-navigation-vertical-menu-item__link--active': this.isActive
									}}
									onClick={(e) => this.clickHandler(e)}
								>
									<div
										class="digi-navigation-vertical-menu-item__icon"
										aria-hidden="true"
										hidden={!this.hasIcon}
									>
										<slot name="icon"></slot>
									</div>
									<span class="digi-navigation-vertical-menu-item__text">
										{this.afText}
									</span>
								</a>
							)}
							{this.isActive && (
								<a
									id={`${this.afId}-link`}
									href={this.afHref}
									aria-current={this.afAriaCurrent}
									class={{
										'digi-navigation-vertical-menu-item__link': true,
										'digi-navigation-vertical-menu-item__link--active': this.isActive
									}}
									onClick={(e) => this.clickHandler(e)}
								>
									<div
										class="digi-navigation-vertical-menu-item__icon"
										aria-hidden="true"
										hidden={!this.hasIcon}
									>
										<slot name="icon"></slot>
									</div>
									<span class="digi-navigation-vertical-menu-item__text">
										{this.afText}
									</span>
								</a>
							)}
						</div>
					)}
				</div>
			</host>
		);
	}
}
