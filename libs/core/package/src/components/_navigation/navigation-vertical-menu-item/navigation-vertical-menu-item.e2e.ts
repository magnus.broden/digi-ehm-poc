import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-vertical-menu-item', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-vertical-menu-item></digi-navigation-vertical-menu-item>');

    const element = await page.find('digi-navigation-vertical-menu-item');
    expect(element).toHaveClass('hydrated');
  });
});
