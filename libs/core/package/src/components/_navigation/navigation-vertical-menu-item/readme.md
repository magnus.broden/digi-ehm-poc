# digi-navigation-vertical-menu-item

This is a menu item that can be used in combination with digi-navigation-vertical-menu.

## CSS

If you for some reason only need the component specific CSS custom properties you can import them like this:

```scss
@use ~@af /
  digi-core/src/components/_navigation/navigation-vertical-menu-item/styles/navigation-vertical-menu-item.variables  as *;
```
This component should only be used inside of `digi-navigation-vertical-menu`. It creates a row inside of the navigation, and can either be a link, a toggle for a sub navigation or both.

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                                                                                           | Type      | Default                                                   |
| ---------------- | ------------------ | ------------------------------------------------------------------------------------------------------------------------------------- | --------- | --------------------------------------------------------- |
| `afActive`       | `af-active`        | Ändrar aktiv status                                                                                                                   | `boolean` | `false`                                                   |
| `afActiveSubnav` | `af-active-subnav` | Ändrar aktiv status på undermenyn. Sätt som 'true' för att ha öppen från början                                                       | `boolean` | `undefined`                                               |
| `afAriaCurrent`  | `af-aria-current`  | Sätter attributet 'aria-current'.                                                                                                     | `string`  | `undefined`                                               |
| `afHasSubnav`    | `af-has-subnav`    |                                                                                                                                       | `boolean` | `false`                                                   |
| `afHref`         | `af-href`          | Sätter attributet 'href'.                                                                                                             | `string`  | `undefined`                                               |
| `afId`           | `af-id`            | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                                 | `string`  | `randomIdGenerator('digi-navigation-vertical-menu-item')` |
| `afOverrideLink` | `af-override-link` | Kringgår länkens vanliga beteende. Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande. | `boolean` | `false`                                                   |
| `afText`         | `af-text`          | Sätter texten i komponenten                                                                                                           | `string`  | `undefined`                                               |


## Events

| Event       | Description                                | Type               |
| ----------- | ------------------------------------------ | ------------------ |
| `afOnClick` | Länken och toggle-knappens 'onclick'-event | `CustomEvent<any>` |


## Slots

| Slot | Description                           |
| ---- | ------------------------------------- |
|      | Expects navigation-vertical-menu-item |


## CSS Custom Properties

| Name                                                                       | Description                                                                   |
| -------------------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| `--digi--navigation-vertical-menu-item--active-indicator-background-color` | var(--digi--color--border--tertiary);                                         |
| `--digi--navigation-vertical-menu-item--active-indicator-left`             | 0;                                                                            |
| `--digi--navigation-vertical-menu-item--active-indicator-width`            | 4px;                                                                          |
| `--digi--navigation-vertical-menu-item--background`                        | transparent;                                                                  |
| `--digi--navigation-vertical-menu-item--background--active`                | var(--digi--color--background--neutral-5);                                    |
| `--digi--navigation-vertical-menu-item--background--hover`                 | transparent;                                                                  |
| `--digi--navigation-vertical-menu-item--border--color`                     | var(--digi--color--border--primary);                                          |
| `--digi--navigation-vertical-menu-item--border--width`                     | var(--digi--border-width--primary);                                           |
| `--digi--navigation-vertical-menu-item--color`                             | var(--digi--color--text--primary);                                            |
| `--digi--navigation-vertical-menu-item--color--active`                     | var(--digi--color--text--primary);                                            |
| `--digi--navigation-vertical-menu-item--color--hover`                      | var(--digi--color--text--link);                                               |
| `--digi--navigation-vertical-menu-item--divider--border`                   | solid var(--digi--border-width--primary) var(--digi--color--border--primary); |
| `--digi--navigation-vertical-menu-item--divider--margin`                   | var(--digi--margin--small) var(--digi--margin--medium);                       |
| `--digi--navigation-vertical-menu-item--font-family`                       | var(--digi--global--typography--font-family--default);                        |
| `--digi--navigation-vertical-menu-item--font-size`                         | var(--digi--typography--vertical-menu-level-2--font-size--desktop);           |
| `--digi--navigation-vertical-menu-item--font-weight`                       | var(--digi--typography--vertical-menu-level-2--font-weight--desktop);         |
| `--digi--navigation-vertical-menu-item--font-weight--active`               | var(--digi--typography--vertical-menu-level-1--font-weight--desktop);         |
| `--digi--navigation-vertical-menu-item--icon--color`                       | var(--digi--color--border--primary);                                          |
| `--digi--navigation-vertical-menu-item--icon--height`                      | 1.5rem;                                                                       |
| `--digi--navigation-vertical-menu-item--icon--margin`                      | 0 var(--digi--margin--smaller) 0 0;                                           |
| `--digi--navigation-vertical-menu-item--icon--width`                       | 1.5rem;                                                                       |
| `--digi--navigation-vertical-menu-item--icon--wrapper--width`              | 1.5rem;                                                                       |
| `--digi--navigation-vertical-menu-item--outline`                           | initial;                                                                      |
| `--digi--navigation-vertical-menu-item--outline--focus`                    | solid 2px var(--digi--color--border--secondary);                              |
| `--digi--navigation-vertical-menu-item--outline--focus--offset`            | -2px;                                                                         |
| `--digi--navigation-vertical-menu-item--padding`                           | var(--digi--padding--small) var(--digi--padding--medium);                     |
| `--digi--navigation-vertical-menu-item--padding--toggle`                   | var(--digi--padding--small) 0;                                                |
| `--digi--navigation-vertical-menu-item--toggle-icon--background`           | var(--digi--color--background--neutral-6);                                    |
| `--digi--navigation-vertical-menu-item--toggle-icon--background--active`   | var(--digi--color--background--neutral-7);                                    |
| `--digi--navigation-vertical-menu-item--toggle-icon--background--hover`    | var(--digi--color--background--inverted-2);                                   |
| `--digi--navigation-vertical-menu-item--toggle-icon--character`            | '+';                                                                          |
| `--digi--navigation-vertical-menu-item--toggle-icon--character--active`    | '-';                                                                          |
| `--digi--navigation-vertical-menu-item--toggle-icon--color`                | var(--digi--color--icons--complementary-1);                                   |
| `--digi--navigation-vertical-menu-item--toggle-icon--color--active`        | var(--digi--color--icons--complementary-1);                                   |
| `--digi--navigation-vertical-menu-item--toggle-icon--color--hover`         | var(--digi--color--icons--inverted);                                          |
| `--digi--navigation-vertical-menu-item--toggle-icon--height`               | 1.5rem;                                                                       |
| `--digi--navigation-vertical-menu-item--toggle-icon--size`                 | var(--digi--typography--tag--font-size--desktop);                             |
| `--digi--navigation-vertical-menu-item--toggle-icon--width`                | 1.5rem;                                                                       |
| `--digi--navigation-vertical-menu-item--width`                             | 100%;                                                                         |


## Dependencies

### Depends on

- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-navigation-vertical-menu-item --> digi-icon
  style digi-navigation-vertical-menu-item fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
