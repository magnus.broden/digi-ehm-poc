export enum NavigationVerticalMenuVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
