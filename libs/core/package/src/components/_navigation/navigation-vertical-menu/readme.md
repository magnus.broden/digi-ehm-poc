# digi-navigation-vertical-menu

This is the base component for building up a vertical navigation. Build up the navigation items with digi-navigation-vertical-menu-item components.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { NavigationVerticalMenuVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                | Attribute                  | Description                                                           | Type                                                                                                       | Default                                              |
| ----------------------- | -------------------------- | --------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------- | ---------------------------------------------------- |
| `afActiveIndicatorSize` | `af-active-indicator-size` | Sätter variant. Kan vara 'primary' eller 'secondary'.                 | `NavigationVerticalMenuActiveIndicatorSize.PRIMARY \| NavigationVerticalMenuActiveIndicatorSize.SECONDARY` | `NavigationVerticalMenuActiveIndicatorSize.PRIMARY`  |
| `afAriaLabel`           | `af-aria-label`            | Sätt attributet 'aria-label'.                                         | `string`                                                                                                   | `undefined`                                          |
| `afId`                  | `af-id`                    | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string`                                                                                                   | `randomIdGenerator('digi-navigation-vertical-menu')` |
| `afVariation`           | `af-variation`             | Sätter variant. Kan vara 'primary' eller 'secondary'.                 | `NavigationVerticalMenuVariation.PRIMARY \| NavigationVerticalMenuVariation.SECONDARY`                     | `NavigationVerticalMenuVariation.PRIMARY`            |


## Events

| Event       | Description                                                                     | Type               |
| ----------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnReady` | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |


## Methods

### `afMSetCurrentActiveLevel(e: any) => Promise<void>`



#### Returns

Type: `Promise<void>`




## Slots

| Slot        | Description                                                       |
| ----------- | ----------------------------------------------------------------- |
| `"default"` | Måste innehålla en eller flera digi-navigation-vertical-menu-item |


## CSS Custom Properties

| Name                                                 | Description                                                                     |
| ---------------------------------------------------- | ------------------------------------------------------------------------------- |
| `--digi--navigation-vertical-menu--background`       | transparent;                                                                    |
| `--digi--navigation-vertical-menu--item--border-top` | solid var(--digi--border-width--primary) var(--digi--color--border--neutral-4); |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
