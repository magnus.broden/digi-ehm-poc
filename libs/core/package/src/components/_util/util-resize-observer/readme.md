# digi-util-resize-observer

This is a component wrapper around the Mutation Observer API. It is very useful for detecting changes to the document inside of it (when nodes are added or deletect, attributes change etc).

<!-- Auto Generated Below -->


## Events

| Event        | Description                    | Type                               |
| ------------ | ------------------------------ | ---------------------------------- |
| `afOnChange` | När komponenten ändrar storlek | `CustomEvent<ResizeObserverEntry>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
