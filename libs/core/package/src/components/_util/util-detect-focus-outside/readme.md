# digi-util-detect-focus-outside

This monitors focus events and emits events when focus is outside or inside.

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                                                                                                                              | Type     | Default                                                         |
| ------------------ | -------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | --------------------------------------------------------------- |
| `afDataIdentifier` | `af-data-identifier` | Sätter en unik identifierare som behövs för att kontrollera fokuseventen. Måste vara prefixad med 'data-'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator( 		'data-digi-util-detect-focus-outside' 	)` |


## Events

| Event              | Description                   | Type                      |
| ------------------ | ----------------------------- | ------------------------- |
| `afOnFocusInside`  | Vid fokus inuti komponenten   | `CustomEvent<FocusEvent>` |
| `afOnFocusOutside` | Vid fokus utanför komponenten | `CustomEvent<FocusEvent>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
