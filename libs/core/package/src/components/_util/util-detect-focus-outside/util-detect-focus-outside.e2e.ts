import { newE2EPage } from '@stencil/core/testing';

describe('digi-util-detect-focus-outside', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-util-detect-focus-outside></digi-util-detect-focus-outside>');

    const element = await page.find('digi-util-detect-focus-outside');
    expect(element).toHaveClass('hydrated');
  });
});
