import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'util/digi-util-detect-focus-outside',
	parameters: {
		actions: {
			handles: ['afOnFocusOutside', 'afOnFocusInside']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-util-detect-focus-outside',
	'af-data-identifier': null,
	/* html */
	children: '<digi-button>I am inside the component</digi-button>'
};
