import { Component, Event, EventEmitter, Listen, h } from '@stencil/core';
import {
	KEY_CODE,
	keyboardHandler
} from '../../../global/utils/keyboardHandler';

/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Keydown Handler
 */
@Component({
	tag: 'digi-util-keydown-handler',
	scoped: true
})
export class UtilKeydownHandler {
	/**
	 * Vid keydown på escape
	 * @en At keydown on escape key
	 */
	@Event() afOnEsc: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på enter
	 * @en At keydown on enter key
	 */
	@Event() afOnEnter: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på tab
	 * @en At keydown on tab key
	 */
	@Event() afOnTab: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på space
	 * @en At keydown on space key
	 */
	@Event() afOnSpace: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på skift och tabb
	 * @en At keydown on shift and tab key
	 */
	@Event() afOnShiftTab: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på pil upp
	 * @en At keydown on up arrow key
	 */
	@Event() afOnUp: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på pil ner
	 * @en At keydown on down arrow key
	 */
	@Event() afOnDown: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på pil vänster
	 * @en At keydown on left arrow key
	 */
	@Event() afOnLeft: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på pil höger
	 * @en At keydown on right arrow key
	 */
	@Event() afOnRight: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på home
	 * @en At keydown on home key
	 */
	@Event() afOnHome: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på end
	 * @en At keydown on end key
	 */
	@Event() afOnEnd: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keydown på alla tangenter
	 * @en At keydown on any key
	 */
	@Event() afOnKeyDown: EventEmitter<KeyboardEvent>;

	@Listen('keydown')
	keydownHandler(e: KeyboardEvent): void {
		const key = keyboardHandler(e);

		switch (key) {
			case KEY_CODE.SHIFT_TAB:
				this.afOnShiftTab.emit(e);
			case KEY_CODE.DOWN_ARROW:
				this.afOnDown.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.UP_ARROW:
				this.afOnUp.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.LEFT_ARROW:
				this.afOnLeft.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.RIGHT_ARROW:
				this.afOnRight.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.ENTER:
				this.afOnEnter.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.ESCAPE:
				this.afOnEsc.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.TAB:
				this.afOnTab.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.SPACE:
				this.afOnSpace.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.HOME:
				this.afOnHome.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.END:
				this.afOnEnd.emit(e);
				this.afOnKeyDown.emit(e);
				break;
			case KEY_CODE.ANY:
				this.afOnKeyDown.emit(e);
				break;
			default:
				this.afOnKeyDown.emit(e);
				return;
		}
	}

	render() {
		return <slot></slot>;
	}
}
