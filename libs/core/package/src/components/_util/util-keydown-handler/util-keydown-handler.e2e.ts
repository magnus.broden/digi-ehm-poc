import { newE2EPage } from '@stencil/core/testing';

describe('digi-util-keydown-handler', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-util-keydown-handler></digi-util-keydown-handler>');

    const element = await page.find('digi-util-keydown-handler');
    expect(element).toHaveClass('hydrated');
  });
});
