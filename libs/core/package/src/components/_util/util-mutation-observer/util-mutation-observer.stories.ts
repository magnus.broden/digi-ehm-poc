import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'util/digi-util-mutation-observer',
	parameters: {
		actions: {
			handles: ['afOnChange']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-util-mutation-observer',
	'af-id': null,
	'af-options': null,
	children: ''
};
