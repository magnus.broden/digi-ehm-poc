# digi-util-mutation-observer

This is a component wrapper around the Mutation Observer API. It is very useful for detecting changes to the document inside of it (when nodes are added or deletect, attributes change etc).

<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                                                                                 | Type                             | Default                                                                                  |
| ----------- | ------------ | ----------------------------------------------------------------------------------------------------------- | -------------------------------- | ---------------------------------------------------------------------------------------- |
| `afId`      | `af-id`      | Sätter attributet 'id' på det omslutande elementet. Ges inget värde så genereras ett slumpmässigt.          | `string`                         | `randomIdGenerator('digi-util-mutation-observer')`                                       |
| `afOptions` | `af-options` | Skicka options till komponentens interna Mutation Observer. T.ex. för att kontrollera förändring i en slot. | `MutationObserverInit \| string` | `{ 		attributes: false, 		childList: true, 		subtree: false, 		characterData: false, 	}` |


## Events

| Event        | Description                                                         | Type               |
| ------------ | ------------------------------------------------------------------- | ------------------ |
| `afOnChange` | När DOM-element läggs till eller tas bort inuti Mutation Observer:n | `CustomEvent<any>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-form-filter](../../_form/form-filter)
 - [digi-form-radiogroup](../../_form/form-radiogroup)
 - [digi-form-select](../../_form/form-select)
 - [digi-navigation-context-menu](../../_navigation/navigation-context-menu)
 - [digi-progressbar](../../_progress/progressbar)

### Graph
```mermaid
graph TD;
  digi-form-filter --> digi-util-mutation-observer
  digi-form-radiogroup --> digi-util-mutation-observer
  digi-form-select --> digi-util-mutation-observer
  digi-navigation-context-menu --> digi-util-mutation-observer
  digi-progressbar --> digi-util-mutation-observer
  style digi-util-mutation-observer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
