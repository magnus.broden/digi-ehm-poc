import { Component, Event, EventEmitter, Listen, h } from '@stencil/core';
import {
	KEY_CODE,
	keyboardHandler
} from '../../../global/utils/keyboardHandler';

/**
 * @slot default - Can be anything
 * @swedishName Keyup Handler
 */
@Component({
	tag: 'digi-util-keyup-handler',
	scoped: true
})
export class UtilKeyupHandler {
	/**
	 * Vid keyup på escape
	 * @en At keyup on escape key
	 */
	@Event() afOnEsc: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på enter
	 * @en At keyup on enter key
	 */
	@Event() afOnEnter: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på tab
	 * @en At keyup on tab key
	 */
	@Event() afOnTab: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på space
	 * @en At keyup on space key
	 */
	@Event() afOnSpace: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på skift och tabb
	 * @en At keyup on shift and tab key
	 */
	@Event() afOnShiftTab: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på pil upp
	 * @en At keyup on up arrow key
	 */
	@Event() afOnUp: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på pil ner
	 * @en At keyup on down arrow key
	 */
	@Event() afOnDown: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på pil vänster
	 * @en At keyup on left arrow key
	 */
	@Event() afOnLeft: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på pil höger
	 * @en At keyup on right arrow key
	 */
	@Event() afOnRight: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på home
	 * @en At keyup on home key
	 */
	@Event() afOnHome: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på end
	 * @en At keyup on end key
	 */
	@Event() afOnEnd: EventEmitter<KeyboardEvent>;

	/**
	 * Vid keyup på alla tangenter
	 * @en At keyup on any key
	 */
	@Event() afOnKeyUp: EventEmitter<KeyboardEvent>;

	@Listen('keyup')
	keyupHandler(e: KeyboardEvent): void {
		const key = keyboardHandler(e);

		switch (key) {
			case KEY_CODE.SHIFT_TAB:
				this.afOnShiftTab.emit(e);
			case KEY_CODE.DOWN_ARROW:
				this.afOnDown.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.UP_ARROW:
				this.afOnUp.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.LEFT_ARROW:
				this.afOnLeft.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.RIGHT_ARROW:
				this.afOnRight.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.ENTER:
				this.afOnEnter.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.ESCAPE:
				this.afOnEsc.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.TAB:
				this.afOnTab.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.SPACE:
				this.afOnSpace.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.HOME:
				this.afOnHome.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.END:
				this.afOnEnd.emit(e);
				this.afOnKeyUp.emit(e);
				break;
			case KEY_CODE.ANY:
				this.afOnKeyUp.emit(e);
				break;
			default:
				this.afOnKeyUp.emit(e);
				return;
		}
	}

	render() {
		return <slot></slot>;
	}
}
