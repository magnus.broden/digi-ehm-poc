import { newE2EPage } from '@stencil/core/testing';

describe('digi-util-keyup-handler', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-util-keyup-handler></digi-util-keyup-handler>');

    const element = await page.find('digi-util-keyup-handler');
    expect(element).toHaveClass('hydrated');
  });
});
