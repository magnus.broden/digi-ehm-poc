import {
	Component,
	Element,
	Event,
	EventEmitter,
	Prop,
	Watch,
	h
} from '@stencil/core';

/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Intersection Observer
 */
@Component({
	tag: 'digi-util-intersection-observer',
	scoped: true
})
export class UtilIntersectionObserver {
	private _observer: IntersectionObserver = null;
	private _hasIntersected = false;
	private _isObserving = false;
	private _afOptions: object;

	@Element() $el: HTMLElement;

	/**
	 * Stäng av observern efter första intersection
	 * @en Disconnect the observer after first intersection.
	 */
	@Prop() afOnce = false;

	/**
	 * Skicka options till komponentens interna Intersection Observer. T.ex. för att kontrollera när bilden lazy loadas.
	 * @en Accepts an Intersection Observer options object. Controls when the image should lazy-load.
	 */
	@Prop() afOptions: object | string = {
		rootMargin: '0px',
		threshold: 0
	};

	@Watch('afOptions')
	afOptionsWatcher(newValue: object | string) {
		this._afOptions =
			typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
	}

	/**
	 * Vid statusförändring mellan 'unintersected' och 'intersected'
	 * @en On change between unintersected and intersected
	 */
	@Event() afOnChange: EventEmitter<boolean>;

	/**
	 * Vid statusförändring till 'intersected'
	 * @en On every change to intersected.
	 */
	@Event() afOnIntersect: EventEmitter;

	/**
	 * Vid statusförändring till 'unintersected'
	 * On every change to unintersected
	 */
	@Event() afOnUnintersect: EventEmitter;

	componentWillLoad() {
		this.afOptionsWatcher(this.afOptions);
		this.initObserver();
	}

	disconnectedCallback() {
		if (this._isObserving) {
			this.removeObserver();
		}
	}

	initObserver() {
		this._observer = new IntersectionObserver(([entry]) => {
			if (entry && entry.isIntersecting) {
				this.intersectionHandler();
				if (this.afOnce) {
					this.removeObserver();
				}
			} else if (entry && this._hasIntersected) {
				this.unintersectionHandler();
			}
		}, this._afOptions);

		this._observer.observe(this.$el);
		this._isObserving = true;
	}

	removeObserver() {
		this._observer.disconnect();
		this._isObserving = false;
	}

	intersectionHandler() {
		this.afOnChange.emit(true);
		this.afOnIntersect.emit();
		this._hasIntersected = true;
	}

	unintersectionHandler() {
		this.afOnChange.emit(false);
		this.afOnUnintersect.emit();
		this._hasIntersected = false;
	}

	render() {
		return <slot></slot>;
	}
}
