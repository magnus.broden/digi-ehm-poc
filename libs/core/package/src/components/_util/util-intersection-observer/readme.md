# digi-util-intersection-observer

This is a component wrapper around the Intersection Observer API. It emits events when it matches the observer options (default is when it enters and leaves the viewport).

<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                                                                                       | Type               | Default                                    |
| ----------- | ------------ | ----------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------------------------------ |
| `afOnce`    | `af-once`    | Stäng av observern efter första intersection                                                                      | `boolean`          | `false`                                    |
| `afOptions` | `af-options` | Skicka options till komponentens interna Intersection Observer. T.ex. för att kontrollera när bilden lazy loadas. | `object \| string` | `{ 		rootMargin: '0px', 		threshold: 0 	}` |


## Events

| Event             | Description                                                                | Type                   |
| ----------------- | -------------------------------------------------------------------------- | ---------------------- |
| `afOnChange`      | Vid statusförändring mellan 'unintersected' och 'intersected'              | `CustomEvent<boolean>` |
| `afOnIntersect`   | Vid statusförändring till 'intersected'                                    | `CustomEvent<any>`     |
| `afOnUnintersect` | Vid statusförändring till 'unintersected' On every change to unintersected | `CustomEvent<any>`     |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-media-image](../../_media/media-image)

### Graph
```mermaid
graph TD;
  digi-media-image --> digi-util-intersection-observer
  style digi-util-intersection-observer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
