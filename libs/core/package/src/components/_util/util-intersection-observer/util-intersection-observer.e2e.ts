import { newE2EPage } from '@stencil/core/testing';

describe('digi-util-intersection-observer', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-util-intersection-observer></digi-util-intersection-observer>');

    const element = await page.find('digi-util-intersection-observer');
    expect(element).toHaveClass('hydrated');
  });
});
