import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'util/digi-util-intersection-observer',
	parameters: {
		actions: {
			handles: ['afOnChange', 'afOnIntersect', 'afOnUnintersect']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-util-intersection-observer',
	'af-once': false,
	'af-options': null,
	children: `
    <digi-typography>
        <p>I'm inside the observer!</p>
    </digi-typography>`
};
