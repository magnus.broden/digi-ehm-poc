import {
	Component,
	Element,
	Event,
	EventEmitter,
	Listen,
	Prop,
	h
} from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';

/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Detect Click Outside
 */
@Component({
	tag: 'digi-util-detect-click-outside',
	scoped: true
})
export class UtilDetectClickOutside {
	@Element() $el: HTMLElement;

	/**
	 * Sätter en unik identifierare som behövs för att kontrollera klickeventen. Måste vara preficad med 'data-'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Set unique identifier needed for click control. Defaults to random string. Should be prefixed with 'data-'
	 */
	@Prop() afDataIdentifier: string = randomIdGenerator(
		'data-digi-util-detect-click-outside'
	);

	/**
	 * Vid klick utanför komponenten
	 * @en When click detected outside of component
	 */
	@Event() afOnClickOutside: EventEmitter<MouseEvent>;

	/**
	 * Vid klick inuti komponenten
	 * @en When click detected inside of component
	 */
	@Event() afOnClickInside: EventEmitter<MouseEvent>;

	constructor() {
		this.$el.setAttribute(this.afDataIdentifier, '');
	}

	@Listen('click', { target: 'window' })
	clickHandler(e: MouseEvent): void {
		const target = e.target as HTMLElement;
		const selector = `[${this.afDataIdentifier}]`;
		if (detectClickOutside(target, selector)) {
			this.afOnClickOutside.emit(e);
		} else {
			this.afOnClickInside.emit(e);
		}
	}

	render() {
		return <slot></slot>;
	}
}
