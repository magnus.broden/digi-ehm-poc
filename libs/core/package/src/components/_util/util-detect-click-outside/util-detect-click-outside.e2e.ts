import { newE2EPage } from '@stencil/core/testing';

describe('digi-util-detect-click-outside', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-util-detect-click-outside></digi-util-detect-click-outside>');

    const element = await page.find('digi-util-detect-click-outside');
    expect(element).toHaveClass('hydrated');
  });
});
