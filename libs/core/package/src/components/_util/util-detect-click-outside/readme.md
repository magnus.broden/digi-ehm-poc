# digi-util-detect-click-outside

This monitors click events and emits events when clicks are outside or inside.

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                                                                                                                              | Type     | Default                                                         |
| ------------------ | -------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | --------------------------------------------------------------- |
| `afDataIdentifier` | `af-data-identifier` | Sätter en unik identifierare som behövs för att kontrollera klickeventen. Måste vara preficad med 'data-'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator( 		'data-digi-util-detect-click-outside' 	)` |


## Events

| Event              | Description                   | Type                      |
| ------------------ | ----------------------------- | ------------------------- |
| `afOnClickInside`  | Vid klick inuti komponenten   | `CustomEvent<MouseEvent>` |
| `afOnClickOutside` | Vid klick utanför komponenten | `CustomEvent<MouseEvent>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
