import { newE2EPage } from '@stencil/core/testing';

describe('digi-util-breakpoint-observer', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-util-breakpoint-observer></digi-util-breakpoint-observer>');

    const element = await page.find('digi-util-breakpoint-observer');
    expect(element).toHaveClass('hydrated');
  });
});
