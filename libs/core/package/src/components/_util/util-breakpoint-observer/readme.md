# digi-util-breakpoint-observer

This observes changes in fixed viewport widths, and emits events when the breakpoint changes. Useful when you need to change properties, values in javascript etc based on different fixed viewport sizes.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { UtilBreakpointObserverBreakpoints } from '@digi/core';
```

<!-- Auto Generated Below -->


## Events

| Event        | Description                                             | Type               |
| ------------ | ------------------------------------------------------- | ------------------ |
| `afOnChange` | Vid inläsning av sida samt vid uppdatering av brytpunkt | `CustomEvent<any>` |
| `afOnLarge`  | Vid brytpunkt 'large'                                   | `CustomEvent<any>` |
| `afOnMedium` | Vid brytpunkt 'medium'                                  | `CustomEvent<any>` |
| `afOnSmall`  | Vid brytpunkt 'small'                                   | `CustomEvent<any>` |
| `afOnXLarge` | Vid brytpunkt 'xlarge'                                  | `CustomEvent<any>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-navigation-sidebar](../../_navigation/navigation-sidebar)

### Graph
```mermaid
graph TD;
  digi-navigation-sidebar --> digi-util-breakpoint-observer
  style digi-util-breakpoint-observer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
