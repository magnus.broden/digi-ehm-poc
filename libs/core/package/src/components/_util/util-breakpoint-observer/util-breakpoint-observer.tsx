import { Component, Element, Event, EventEmitter, h } from '@stencil/core';
import { Host, HTMLStencilElement } from '@stencil/core/internal';
import { UtilBreakpointObserverBreakpoints } from './util-breakpoint-observer-breakpoints.enum';

/**
 * @slot default - Kan innehålla vad som helst
 * @enums UtilBreakpointObserverBreakpoints - util-breakpoint-observer-breakpoints.enum.ts
 * @swedishName Breakpoint Observer
 *
 */
@Component({
	tag: 'digi-util-breakpoint-observer',
	styleUrls: ['util-breakpoint-observer.scss'],
	scoped: true
})
export class UtilBreakpointObserver {
	@Element() hostElement: HTMLStencilElement;

	private _observer: HTMLElement;

	componentDidLoad() {
		this.handleBreakpoint();
	}

	/**
	 * Vid inläsning av sida samt vid uppdatering av brytpunkt
	 * @en At page load and at change of breakpoint on page
	 */
	@Event() afOnChange: EventEmitter;

	/**
	 * Vid brytpunkt 'small'
	 * @en At breakpoint 'small'
	 */
	@Event() afOnSmall: EventEmitter;

	/**
	 * Vid brytpunkt 'medium'
	 * @en At breakpoint 'medium'
	 */
	@Event() afOnMedium: EventEmitter;

	/**
	 * Vid brytpunkt 'large'
	 * @en At breakpoint 'large'
	 */
	@Event() afOnLarge: EventEmitter;

	/**
	 * Vid brytpunkt 'xlarge'
	 * @en At breakpoint 'xlarge'
	 */
	@Event() afOnXLarge: EventEmitter;

	handleBreakpoint(e?: Event) {
		const observerCSSVariables: CSSStyleDeclaration = window.getComputedStyle(
			this._observer
		);
		const value = observerCSSVariables.getPropertyValue(
			'--digi--util-breakpoint-observer--breakpoint'
		);

		this.afOnChange.emit({ value, e });

		switch (value) {
			case UtilBreakpointObserverBreakpoints.SMALL:
				this.afOnSmall.emit({ value, e });
				break;
			case UtilBreakpointObserverBreakpoints.MEDIUM:
				this.afOnMedium.emit({ value, e });
				break;
			case UtilBreakpointObserverBreakpoints.LARGE:
				this.afOnLarge.emit({ value, e });
				break;
			case UtilBreakpointObserverBreakpoints.XLARGE:
				this.afOnXLarge.emit({ value, e });
		}
	}

	render() {
		return (
			<Host>
				<div class="digi-util-breakpoint-observer">
					<div
						ref={(el) => (this._observer = el)}
						onTransitionEnd={(e) => this.handleBreakpoint(e)}
						class="digi-util-breakpoint-observer__observer"
					></div>
					<slot></slot>
				</div>
			</Host>
		);
	}
}
