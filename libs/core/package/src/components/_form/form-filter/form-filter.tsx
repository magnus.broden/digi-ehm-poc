import {
	Component,
	Element,
	Event,
	EventEmitter,
	Prop,
	State,
	Listen,
	h
} from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { _t } from '@digi/shared/text';

/**
 * @slot default - Ska vara formulärselement. Till exempel digi-form-checkbox.
 * @swedishName Multifilter
 */
@Component({
	tag: 'digi-form-filter',
	styleUrls: ['form-filter.scss'],
	scoped: true
})
export class formFilter {
	@Element() hostElement: HTMLStencilElement;

	@State() isActive: boolean = false;
	@State() activeListItemIndex: number = 0;
	@State() hasActiveItems: boolean = false;
	@State() focusInList: boolean = false;

	private _toggleButton;
	private _submitButton;
	private _observer;
	private _updateActiveItems = false;

	@State() listItems = [];
	public activeList = [];

	/**
	 * Prefixet för alla komponentens olika interna 'id'-attribut på dropdowns, filterlistor etc. Ex. 'my-cool-id' genererar 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' och så vidare. Ges inget värde så genereras ett slumpmässigt.
	 * @en Prefix for all id attributes inside the component. Example: 'my-cool-id' generates 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' and so on. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-filter');

	/**
	 * Texten i filterknappen
	 * @en Set the filter button text
	 */
	@Prop() afFilterButtonText!: string;

	/**
	 * Texten i submitknappen
	 * @en Set the submit button text
	 */
	@Prop() afSubmitButtonText!: string;

	/**
	 * Skärmläsartext för submitknappen
	 * @en Set the submit button aria-label text
	 */
	 @Prop() afSubmitButtonTextAriaLabel: string;

	/**
	 * Texten i rensaknappen
	 * @en Set the reset button text
	 */
	 @Prop() afResetButtonText: string = _t.clear;

	 /**
	 * Skärmläsartext för resetknappen
	 * @en Set the reset button aria-label text
	 */
		@Prop() afResetButtonTextAriaLabel: string;

	 /**
	 * Döljer rensaknappen när värdet sätts till true
	 * @en Hides the reset button when set to true
	 */
		@Prop() afHideResetButton: boolean = false;

	/**
	 * Justera dropdown från höger till vänster
	 * @en Align dropdown from right to left
	 */
	@Prop() afAlignRight: boolean;

	/**
	 * Namnet på filterlistans valda filter vid submit. Används även av legendelementet inne i filterlistan.
	 * @en Binding name of filterlist selected filters on submit. Also used by the legend element.
	 */
	@Prop() afName: string;

	/**
	 * Sätter attributet 'aria-label' på filterknappen
	 * @en Input aria-label attribute on filter button
	 */
	 @Prop() afFilterButtonAriaLabel: string;

	/**
	 * Sätter attributet 'aria-describedby' på filterknappen
	 * @en Input aria-describedby attribute on filter button
	 */
	@Prop() afFilterButtonAriaDescribedby: string;

	/**
	 * Sätter attributet 'autofocus'.
	 * @en Input autofocus attribute
	 */
	@Prop() afAutofocus: boolean;

	@Event() afOnFocusout: EventEmitter;

	/**
	 * Vid uppdatering av filtret
	 * @en At filter submit
	 */
	@Event() afOnSubmitFilters: EventEmitter;

	/**
	 * När filtret stängs utan att valda alternativ bekräftats
	 * @en When the filter is closed without confirming the selected options
	 */
	 @Event() afOnFilterClosed: EventEmitter;

	/**
	 * Vid reset av filtret
	 * @en At filter reset
	 */
	@Event() afOnResetFilters: EventEmitter;

	/**
	 * När ett filter ändras
	 * @en When a filter is changed
	 */
	@Event() afOnChange: EventEmitter;

	@Listen('click', { target: 'window' })
	clickHandler(e: MouseEvent): void {
		const target = e.target as HTMLElement;
		const selector = `#${this.afId}-identifier`;

		if (detectClickOutside(target, selector) && this.isActive) {
			this.emitFilterClosedEvent();
		}
	}

	@Listen('focusin', { target: 'document' })
	focusoutHandler(e: FocusEvent): void {
		const target = e.target as HTMLElement;
		const selector = `#${this.afId}-identifier`;
		const focusOutsideFilter = detectFocusOutside(target, selector);

		if (focusOutsideFilter && this.isActive) {
			this.emitFilterClosedEvent();
		} else {
			this.focusInList = !detectFocusOutside(target, `#${this.afId}-filter-list`);

			//Update tabindex on focused input element
			if (this.isActive && this.focusInList && target.tagName === 'INPUT') {
				this.setTabIndex();
				this.updateTabIndex(target);
			}
			this.afOnFocusout.emit(e);
		}
	}

	@Listen('change')
	changeHandler(e) {
		if (e.target.tagName === 'INPUT') {
			this.setSelectedItems(e.target);
			this.hasActiveItems = this.activeList.length > 0 ? true : false;

			const dataKey = e.target.closest('.digi-form-filter__item')
				? e.target.closest('.digi-form-filter__item').getAttribute('data-key')
				: this.activeListItemIndex;

			this.activeListItemIndex = Number(dataKey);
			this.afOnChange.emit(e);
		}
	}

	debounce(func, timeout = 300) {
		let timer;
		return (...args) => {
			clearTimeout(timer);
			timer = setTimeout(() => {
				func.apply(this, args);
			}, timeout);
		};
	}

	setSelectedItems(item) {
		this.activeList.some((el) => el.id === item.id)
			? (this.activeList = this.activeList.filter((f) => f.id !== item.id))
			: this.activeList.push(item);
	}

	setInactive(focusToggleButton = false) {
		this.isActive = false;

		if (focusToggleButton) {
			document.getElementById(`${this.afId}-toggle-button`).focus();
		}

		document.querySelector(
			`#${this.afId}-dropdown-menu .digi-form-filter__scroll`
		).scrollTop = 0;
	}

	setActive() {
		this.isActive = true;
		this.activeListItemIndex = 0;
		setTimeout(() => {
			this.focusActiveItem();
		}, 200);
	}

	toggleDropdown() {
		return this.isActive ? this.setInactive() : this.setActive();
	}

	inputItem(identifier, index) {
		const item: HTMLInputElement = document.querySelector(
			`#${identifier} .digi-form-filter__item:nth-child(${index}) input`
		);
		return item;
	}

	getFilterData() {
		return {
			filter: this.afName,
			items: this.activeList.map((item) => {
				return {
					id: item.id,
					text: item.labels[0].htmlFor === item.id ? item.labels[0].innerText : ''
				};
			})
		};
	}

	emitFilterClosedEvent() {
		this.setInactive(true);

		this.afOnFilterClosed.emit(this.getFilterData());
	}

	submitFilters(setInactive = true) {
		if (setInactive) {
			this.setInactive(true);
		}

		this.afOnSubmitFilters.emit(this.getFilterData());
	}

	resetFilters() {
		this.listItems.map((_, i) => {
			this.inputItem(`${this.afId}-filter-list`, i + 1).checked = false;
		});
		this.activeList = [];
		this.hasActiveItems = false;

		this.afOnResetFilters.emit(this.afName);
	}

	setupListElements(collection: HTMLCollection) {
		if (!collection || collection.length <= 0) {
			logger.warn(`The slot contains no filter elements.`, this.hostElement);
			return;
		}

		this.listItems = Array.from(collection).map((element, i) => ({
				index: i,
				outerHTML: (element.cloneNode() as Element).outerHTML
			}));
	}

	componentWillLoad() {
		this.setupListElements(this.hostElement.children);
	}

	setupActiveItems() {
		this.setTabIndex();

		setTimeout(() => {
			const rootElement = document.getElementById(`${this.afId}-identifier`);
			let activeInputs = Array.from(
				rootElement?.querySelectorAll('.digi-form-filter__list input:checked')
			);

			this.activeList = [];

			activeInputs.forEach((inputEl) => {
				this.setSelectedItems(inputEl);
			});

			this.hasActiveItems = this.activeList.length > 0 ? true : false;
		});
	}

	componentDidLoad() {
		this.setupActiveItems();
	}

	componentDidUpdate() {
		if (!this._updateActiveItems) {
			return;
		}

		this._updateActiveItems = false;

		this.setupActiveItems();
	}

	focusActiveItem() {
		const element = this.inputItem(
			`${this.afId}-filter-list`,
			this.activeListItemIndex + 1
		);
		element.focus();
	}

	incrementActiveItem() {
		if (this.activeListItemIndex < this.listItems.length - 1) {
			this.activeListItemIndex = this.activeListItemIndex + 1;
			this.focusActiveItem();
			return;
		}

		this.tabHandler();
	}

	setTabIndex() {
		const tabindexes = document.querySelectorAll(`#${this.afId}-dropdown-menu`);

		if (!tabindexes) {
			logger.warn(
				`No input elements was found to set tabindex on.`,
				this.hostElement
			);
			return;
		}

		Array.from(tabindexes).map((tabindex) => {
			tabindex.setAttribute('tabindex', '-1');
		});
	}

	updateTabIndex(element: HTMLElement) {
		if (!element) {
			logger.warn(
				`No input element was found to update tabindex on.`,
				this.hostElement
			);
			return;
		}
		element.setAttribute('tabindex', '0');
	}

	decrementActiveItem() {
		if (this.activeListItemIndex === 0) {
			this._toggleButton.querySelector('button').focus();
			return;
		}

		if (this.activeListItemIndex > 0) {
			this.activeListItemIndex = this.activeListItemIndex - 1;
		}
		this.focusActiveItem();
	}

	downHandler(e: any) {
    e.detail.preventDefault();
		this.incrementActiveItem();
	}

	upHandler(e: any) {
    e.detail.preventDefault();
		this.decrementActiveItem();
	}

	homeHandler(e: any) {
    e.detail.preventDefault();
		this.activeListItemIndex = 0;
		this.focusActiveItem();
	}

	endHandler(e: any) {
    e.detail.preventDefault();
		this.activeListItemIndex = this.listItems.length - 1;
		this.focusActiveItem();
	}

	enterHandler(e) {
		e.detail.target.click();
	}

	escHandler() {
		if (this.isActive) {
			this.setInactive(true);
		}
	}

	tabHandler() {
		if (this.isActive && this.focusInList) {
			setTimeout(() => {
				this._submitButton.querySelector('button').focus();
			}, 10);
		}
	}

	shiftHandler() {
		if (this.isActive && this.focusInList) {
			this._toggleButton.querySelector('button').focus();
			this.activeListItemIndex =
				this.activeListItemIndex >= 0
					? this.activeListItemIndex - 1
					: this.activeListItemIndex;
		}
	}

	get cssModifiers() {
		return {
			'digi-form-filter--open': this.isActive
		};
	}

	get cssModifiersToggleButtonIndicator() {
		return {
			'digi-form-filter__toggle-button-indicator--active': this.hasActiveItems
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-form-filter': true,
					...this.cssModifiers
				}}
				role="application"
				id={`${this.afId}-identifier`}
			>
				<digi-util-keydown-handler onAfOnEsc={() => this.escHandler()}>
					<digi-button
						id={`${this.afId}-toggle-button`}
						class="digi-form-filter__toggle-button"
						onAfOnClick={() => this.toggleDropdown()}
						ref={(el) => (this._toggleButton = el)}
						af-aria-haspopup="true"
						af-variation="secondary"
						af-aria-label={this.afFilterButtonAriaLabel}
						af-aria-labelledby={this.afFilterButtonAriaDescribedby}
						af-aria-controls={`${this.afId}-dropdown-menu`}
						af-aria-expanded={this.isActive}
						af-autofocus={this.afAutofocus ? this.afAutofocus : null}
					>
							<i
								class={{
									'digi-form-filter__toggle-button-indicator': true,
								...this.cssModifiersToggleButtonIndicator
								}}
							></i>
							<span class="digi-form-filter__toggle-button-text">
								{this.afFilterButtonText}
							</span>
						<digi-icon
								class="digi-form-filter__toggle-icon"
								slot="icon-secondary"
							afName={`chevron-down`}
						></digi-icon>
					</digi-button>
					
					<div
						hidden={!this.isActive}
						id={`${this.afId}-dropdown-menu`}
						class={{
							'digi-form-filter__dropdown': true,
							'digi-form-filter__dropdown--right': this.afAlignRight
						}}
					>
						<div class="digi-form-filter__scroll">
							<fieldset>
								<legend class="digi-form-filter__legend">{this.afName}</legend>
								<digi-util-keydown-handler
									onAfOnDown={(e) => this.downHandler(e)}
									onAfOnUp={(e) => this.upHandler(e)}
									onAfOnHome={(e) => this.homeHandler(e)}
									onAfOnEnd={(e) => this.endHandler(e)}
									onAfOnEnter={(e) => this.enterHandler(e)}
									onAfOnTab={() => this.tabHandler()}
									onAfOnShiftTab={() => this.shiftHandler()}
								>
									<ul
										class="digi-form-filter__list"
										aria-labelledby={`${this.afId}-toggle-button`}
										id={`${this.afId}-filter-list`}
									>
										{this.listItems.map((item) => {
											return (
												<li
													class="digi-form-filter__item"
													key={item.index}
													data-key={item.index}
													innerHTML={item.outerHTML}
												></li>
											);
										})}
									</ul>
								</digi-util-keydown-handler>
								<digi-util-mutation-observer
									hidden
									ref={(el) => (this._observer = el)}
									onAfOnChange={this.debounce(() => {
										this.setupListElements(this._observer.children);
										this._updateActiveItems = true;
									})}
								>
									<slot></slot>
								</digi-util-mutation-observer>
							</fieldset>
						</div>
						<div class="digi-form-filter__footer">
							<digi-util-keydown-handler onAfOnUp={() => this.focusActiveItem()}>
								<digi-button
									ref={(el) => (this._submitButton = el)}
									af-variation="primary"
									af-aria-label={this.afSubmitButtonTextAriaLabel}
									onClick={() => {
										this.submitFilters();
										this._toggleButton.querySelector('button').focus();
									}}
									class="digi-form-filter__submit-button"
								>
									{this.afSubmitButtonText}
								</digi-button>
							</digi-util-keydown-handler>
							{!this.afHideResetButton && (
									<digi-button
										af-variation="secondary"
										af-aria-label={this.afResetButtonTextAriaLabel}
										onClick={() => this.resetFilters()}
										class="digi-form-filter__reset-button"
									>
										{this.afResetButtonText}
									</digi-button>
								)}
						</div>
					</div>
				</digi-util-keydown-handler>
			</div>
		);
	}
}
