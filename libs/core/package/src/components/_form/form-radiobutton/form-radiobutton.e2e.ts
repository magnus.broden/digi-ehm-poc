import { newE2EPage } from '@stencil/core/testing';

describe('digi-radiobutton', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-radiobutton></digi-form-radiobutton>');

    const element = await page.find('digi-form-radiobutton');
    expect(element).toHaveClass('hydrated');
  });
});
