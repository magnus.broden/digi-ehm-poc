import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormRadiobuttonVariation } from './form-radiobutton-variation.enum';
import { FormRadiobuttonValidation } from './form-radiobutton-validation.enum';
import { FormRadiobuttonLayout } from './form-radiobutton-layout.enum';

export default {
	title: 'form/digi-form-radiobutton',
	parameters: {
		actions: {
			handles: [
				'afOnChange',
				'afOnBlur',
				'afOnFocus',
				'afOnFocusout',
				'afOnInput',
				'afOnDirty',
				'afOnTouched'
			]
		}
	},
	argTypes: {
		'af-variation': enumSelect(FormRadiobuttonVariation),
		'af-validation': enumSelect(FormRadiobuttonValidation),
		'af-layout': enumSelect(FormRadiobuttonLayout)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-radiobutton',
	'af-label': 'A label is required',
	'af-variation': FormRadiobuttonVariation.PRIMARY,
	'af-layout': FormRadiobuttonLayout.INLINE,
	'af-validation': null,
	'af-name': '',
	'af-id': '',
	'af-required': false,
	value: '',
	'af-value': '',
	'af-checked': false,
	'af-aria-labelledby': '',
	'af-aria-describedby': ''
};
