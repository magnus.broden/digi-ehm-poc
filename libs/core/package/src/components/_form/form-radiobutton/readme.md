# digi-radiobutton

This is a radiobutton component.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormRadiobuttonVariation, FormRadiobuttonValidation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property               | Attribute             | Description                                                           | Type                                                                     | Default                                      |
| ---------------------- | --------------------- | --------------------------------------------------------------------- | ------------------------------------------------------------------------ | -------------------------------------------- |
| `afAriaDescribedby`    | `af-aria-describedby` | Sätter attributet 'aria-describedby'                                  | `string`                                                                 | `undefined`                                  |
| `afAriaLabelledby`     | `af-aria-labelledby`  | Sätter attributet 'aria-labelledby'                                   | `string`                                                                 | `undefined`                                  |
| `afAutofocus`          | `af-autofocus`        | Sätter attributet 'autofocus'.                                        | `boolean`                                                                | `undefined`                                  |
| `afChecked`            | `af-checked`          | Sätter om radioknappen ska vara ikryssad.                             | `boolean`                                                                | `undefined`                                  |
| `afId`                 | `af-id`               | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string`                                                                 | `randomIdGenerator('digi-form-radiobutton')` |
| `afLabel` _(required)_ | `af-label`            | Texten till labelelementet                                            | `string`                                                                 | `undefined`                                  |
| `afLayout`             | `af-layout`           | Sätter layouten.                                                      | `FormRadiobuttonLayout.BLOCK \| FormRadiobuttonLayout.INLINE`            | `FormRadiobuttonLayout.INLINE`               |
| `afName`               | `af-name`             | Sätter attributet 'name'.                                             | `string`                                                                 | `undefined`                                  |
| `afRequired`           | `af-required`         | Sätter attributet 'required'                                          | `boolean`                                                                | `undefined`                                  |
| `afValidation`         | `af-validation`       | Sätter valideringsstatus. Kan vara 'error' eller ingenting.           | `FormRadiobuttonValidation`                                              | `undefined`                                  |
| `afValue`              | `af-value`            | Sätter initiala attributet 'value'                                    | `string`                                                                 | `undefined`                                  |
| `afVariation`          | `af-variation`        | Sätter variant. Kan vara 'primary' eller 'secondary'                  | `FormRadiobuttonVariation.PRIMARY \| FormRadiobuttonVariation.SECONDARY` | `FormRadiobuttonVariation.PRIMARY`           |
| `checked`              | `checked`             | Sätter om radioknappen ska vara ikryssad.                             | `boolean`                                                                | `undefined`                                  |
| `value`                | `value`               | Sätter attributet 'value'                                             | `string`                                                                 | `undefined`                                  |


## Events

| Event          | Description                                                                     | Type               |
| -------------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnBlur`     | Inputelementets 'onblur'-event.                                                 | `CustomEvent<any>` |
| `afOnChange`   | Inputelementets 'onchange'-event.                                               | `CustomEvent<any>` |
| `afOnDirty`    | Sker vid inputfältets första 'oninput'                                          | `CustomEvent<any>` |
| `afOnFocus`    | Inputelementets 'onfocus'-event.                                                | `CustomEvent<any>` |
| `afOnFocusout` | Inputelementets 'onfocusout'-event.                                             | `CustomEvent<any>` |
| `afOnInput`    | Inputelementets 'oninput'-event.                                                | `CustomEvent<any>` |
| `afOnReady`    | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |
| `afOnTouched`  | Sker vid inputfältets första 'onblur'                                           | `CustomEvent<any>` |


## Methods

### `afMGetFormControlElement() => Promise<HTMLInputElement>`

Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.

#### Returns

Type: `Promise<HTMLInputElement>`




## CSS Custom Properties

| Name                                                       | Description                                                           |
| ---------------------------------------------------------- | --------------------------------------------------------------------- |
| `--digi--form-radiobutton--background-color`               | var(--digi--color--background--primary);                              |
| `--digi--form-radiobutton--background-color--error`        | var(--digi--color--background--danger-2);                             |
| `--digi--form-radiobutton--background-color--primary`      | var(--digi--color--background--radio-primary);                        |
| `--digi--form-radiobutton--background-color--secondary`    | var(--digi--color--background--radio-secondary);                      |
| `--digi--form-radiobutton--border-color`                   | var(--digi--color--border--primary);                                  |
| `--digi--form-radiobutton--border-color--error`            | var(--digi--color--border--danger);                                   |
| `--digi--form-radiobutton--border-color--primary`          | var(--digi--color--border--radio-primary);                            |
| `--digi--form-radiobutton--border-color--secondary`        | var(--digi--color--border--radio-secondary);                          |
| `--digi--form-radiobutton--border-radius`                  | var(--digi--border-radius--primary);                                  |
| `--digi--form-radiobutton--border-width`                   | var(--digi--border-width--primary);                                   |
| `--digi--form-radiobutton--box-shadow--error`              | var(--digi--color--border--danger);                                   |
| `--digi--form-radiobutton--color--marker`                  | var(--digi--color--background--radio-marker);                         |
| `--digi--form-radiobutton--height`                         | var(--digi--input-height--large);                                     |
| `--digi--form-radiobutton--input--focus--shadow`           | var(--digi--shadow--input-focus);                                     |
| `--digi--form-radiobutton--label--background-color--hover` | var(--digi--color--background--input-empty);                          |
| `--digi--form-radiobutton--marker--size`                   | calc(var(--digi--typography--input--font-size--desktop-large) * 0.4); |
| `--digi--form-radiobutton--padding`                        | var(--digi--gutter--icon);                                            |
| `--digi--form-radiobutton--padding--block`                 | var(--digi--gutter--small);                                           |
| `--digi--form-radiobutton--padding--inline`                | var(--digi--gutter--icon);                                            |
| `--digi--form-radiobutton--size`                           | 1.25rem;                                                              |


## Dependencies

### Used by

 - [digi-code-example](../../_code/code-example)

### Graph
```mermaid
graph TD;
  digi-code-example --> digi-form-radiobutton
  style digi-form-radiobutton fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
