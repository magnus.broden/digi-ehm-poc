# digi-form-validation-message

This is a component for showing validation messages. It is used internally in many form-control components, but this lets you use it standalone. Note: It does not provide its own live-region type accessibility features. These are expected to be provided by your application logic (i.e. use `aria-atomic` and `role="alert"` on its parent element).

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormValidationMessageVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                            | Type                                                                                                                       | Default                                  |
| ------------- | -------------- | ---------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- |
| `afVariation` | `af-variation` | Sätter valideringsstatus. Kan vara 'success', 'error' eller 'warning'. | `FormValidationMessageVariation.ERROR \| FormValidationMessageVariation.SUCCESS \| FormValidationMessageVariation.WARNING` | `FormValidationMessageVariation.SUCCESS` |


## Slots

| Slot        | Description                                                                  |
| ----------- | ---------------------------------------------------------------------------- |
| `"default"` | Ska vara en textnod eller liknande element. Renderas inne i ett spanelement. |


## CSS Custom Properties

| Name                                                   | Description                         |
| ------------------------------------------------------ | ----------------------------------- |
| `--digi--form-validation-message--icon-color--error`   | var(--digi--color--icons--danger);  |
| `--digi--form-validation-message--icon-color--success` | var(--digi--color--icons--success); |
| `--digi--form-validation-message--icon-color--warning` | var(--digi--color--icons--warning); |


## Dependencies

### Used by

 - [digi-form-input](../form-input)
 - [digi-form-select](../form-select)
 - [digi-form-textarea](../form-textarea)

### Depends on

- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-form-validation-message --> digi-icon
  digi-form-input --> digi-form-validation-message
  digi-form-select --> digi-form-validation-message
  digi-form-textarea --> digi-form-validation-message
  style digi-form-validation-message fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
