import { Component, Element, Prop, h } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { IconName } from '../../../global/icon/icon';
import { FormValidationMessageVariation } from './form-validation-message-variation.enum';

/**
 * @slot default - Ska vara en textnod eller liknande element. Renderas inne i ett spanelement.
 *
 * @enums FormValidationMessageVariation - form-validation-message-variation.enum.ts
 * @swedishName Valideringsmeddelande
 */
@Component({
	tag: 'digi-form-validation-message',
	styleUrls: ['form-validation-message.scss'],
	scoped: true
})
export class FormValidationMessage {
	@Element() hostElement: HTMLStencilElement;

	/**
	 * Sätter valideringsstatus. Kan vara 'success', 'error' eller 'warning'.
	 * @en Set validation status
	 */
	@Prop() afVariation: FormValidationMessageVariation =
		FormValidationMessageVariation.SUCCESS;

	get cssModifiers() {
		return {
			'digi-form-validation-message--success':
				this.afVariation === FormValidationMessageVariation.SUCCESS,
			'digi-form-validation-message--error':
				this.afVariation === FormValidationMessageVariation.ERROR,
			'digi-form-validation-message--warning':
				this.afVariation === FormValidationMessageVariation.WARNING
		};
	}

	get icon(): IconName {
		switch (this.afVariation) {
			case FormValidationMessageVariation.SUCCESS:
				return 'validation-success';
			case FormValidationMessageVariation.ERROR:
				return 'validation-error';
			case FormValidationMessageVariation.WARNING:
				return 'validation-warning';
		}
	}

	render() {
		return (
			<div
				class={{
					'digi-form-validation-message': true,
					...this.cssModifiers
				}}
			>
				<div class="digi-form-validation-message__icon" aria-hidden="true">
					<digi-icon afName={this.icon} />
				</div>
				<span class="digi-form-validation-message__text">
					<slot></slot>
				</span>
			</div>
		);
	}
}
