import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-validation-message', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-validation-message></digi-form-validation-message>');

    const element = await page.find('digi-form-validation-message');
    expect(element).toHaveClass('hydrated');
  });
});
