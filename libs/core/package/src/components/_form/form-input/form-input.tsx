import {
	Component,
	Element,
	Event,
	EventEmitter,
	Method,
	Prop,
	State,
	h
} from '@stencil/core';
import { HTMLStencilElement, Watch } from '@stencil/core/internal';

import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormInputButtonVariation } from './form-input-button-variation.enum';
import { FormInputType } from './form-input-type.enum';
import { FormInputMode } from './form-input-mode.enum';
import { FormInputValidation } from './form-input-validation.enum';
import { FormInputVariation } from './form-input-variation.enum';

/**
 * @enums FormInputType - form-input-type.enum.ts
 * @enums FormInputMode - form-input-mode.enum.ts
 * @enums FormInputValidation - form-input-validation.enum.ts
 * @enums FormInputVariation - form-input-variation.enum.ts
 * @swedishName Inmatningsfält
 */

@Component({
	tag: 'digi-form-input',
	styleUrls: ['form-input.scss'],
	scoped: true
})
export class FormInput {
	@Element() hostElement: HTMLStencilElement;

	private _input?: HTMLInputElement;

	@State() hasActiveValidationMessage = false;
	@State() hasButton: boolean;
	@State() dirty = false;
	@State() touched = false;

	/**
	 * Texten till labelelementet
	 * @en The label text
	 */
	@Prop() afLabel!: string;

	/**
	 * Valfri beskrivande text
	 * @en A description text
	 */
	@Prop() afLabelDescription: string;

	/**
	 * Sätter attributet 'type'.
	 * @en Input type attribute
	 */
	@Prop() afType: FormInputType = FormInputType.TEXT;

		/**
	 * Sätter attributet 'inputmode'.
	 * @en Inputmode  type attribute
	 */
		@Prop() afInputmode: FormInputMode = FormInputMode.DEFAULT;

	/**
	 * Layout för hur inmatningsfältet ska visas då det finns en knapp
	 * @en Input layout when there is a button
	 */
	@Prop() afButtonVariation: FormInputButtonVariation =
		FormInputButtonVariation.PRIMARY;

	/**
	 * Sätter attributet 'autofocus'.
	 * @en Input autofocus attribute
	 */
	@Prop() afAutofocus: boolean;

	/**
	 * Sätter variant. Kan vara 'small', 'medium' eller 'large'
	 * @en Set input variation.
	 */
	@Prop() afVariation: FormInputVariation = FormInputVariation.MEDIUM;

	/**
	 * Sätter attributet 'name'.
	 * @en Input name attribute
	 */
	@Prop() afName: string;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-input');

	/**
	 * Sätter attributet 'maxlength'.
	 * @en Input maxlength attribute
	 */
	@Prop() afMaxlength: number;

	/**
	 * Sätter attributet 'minlength'.
	 * @en Input minlength attribute
	 */
	@Prop() afMinlength: number;

	/**
	 * Sätter attributet 'required'.
	 * @en Input required attribute
	 */
	@Prop() afRequired: boolean;

	/**
	 * Sätter text för afRequired.
	 * @en Set text for afRequired.
	 */

	@Prop() afRequiredText: string;

	/**
	 * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
	 * @en Set this to true if the form contains more required fields than optional fields.
	 */
	@Prop() afAnnounceIfOptional = false;

	/**
	 * Sätter text för afAnnounceIfOptional.
	 * @en Set text for afAnnounceIfOptional.
	 */
	@Prop() afAnnounceIfOptionalText: string;

	/**
	 * Sätter attributet 'value'.
	 * @en Input value attribute
	 * @ignore
	 */
	@Prop() value: string | number;
	@Watch('value') onValueChanged(value) {
		this.afValue = value;
	}

	/**
	 * Sätter attributet 'value'.
	 * @en Input value attribute
	 */
	@Prop() afValue: string | number;
	@Watch('afValue') onAfValueChanged(value) {
		this.value = value;
	}

	/**
	 * Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting.
	 * @en Set input validation status
	 */
	@Prop() afValidation: FormInputValidation = FormInputValidation.NEUTRAL;

	/**
	 * Sätter valideringsmeddelandet
	 * @en Set input validation message
	 */
	@Prop() afValidationText: string;

	/**
	 * Sätter attributet 'role'.
	 * @en Input role attribute
	 */
	@Prop() afRole: string;

	/**
	 * Sätter attributet 'autocomplete'.
	 * @en Input autocomplete attribute
	 */
	@Prop() afAutocomplete: string;

	/**
	 * Sätter attributet 'min'.
	 * @en Input min attribute
	 */
	@Prop() afMin: string;

	/**
	 * Sätter attributet 'max'.
	 * @en Input max attribute
	 */
	@Prop() afMax: string;

	/**
	 * Sätter attributet 'list'.
	 * @en Input list attribute
	 */
	@Prop() afList: string;

	/**
	 * Sätter attributet 'step'.
	 * @en Input step attribute
	 */
	@Prop() afStep: string;

	/**
	 * Sätter attributet 'dirname'.
	 * @en Input dirname attribute
	 */
	@Prop() afDirname: string;

	/**
	 * Sätter attributet 'aria-autocomplete'.
	 * @en Input aria-autocomplete attribute
	 */
	@Prop() afAriaAutocomplete: string;

	/**
	 * Sätter attributet 'aria-activedescendant'.
	 * @en Input aria-activedescendant attribute
	 */
	@Prop() afAriaActivedescendant: string;

	/**
	 * Sätter attributet 'aria-labelledby'.
	 * @en Input aria-labelledby attribute
	 */
	@Prop() afAriaLabelledby: string;

	/**
	 * Sätter attributet 'aria-describedby'.
	 * @en Input aria-describedby attribute
	 */
	@Prop() afAriaDescribedby: string;

	/**
	 * Inputelementets 'onchange'-event.
	 * @en The input element's 'onchange' event.
	 */
	@Event() afOnChange: EventEmitter;

	/**
	 * Inputelementets 'onblur'-event.
	 * @en The input element's 'onblur' event.
	 */
	@Event() afOnBlur: EventEmitter;

	/**
	 * Inputelementets 'onkeyup'-event.
	 * @en The input element's 'onkeyup' event.
	 */
	@Event() afOnKeyup: EventEmitter;

	/**
	 * Inputelementets 'onfocus'-event.
	 * @en The input element's 'onfocus' event.
	 */
	@Event() afOnFocus: EventEmitter;

	/**
	 * Inputelementets 'onfocusout'-event.
	 * @en The input element's 'onfocusout' event.
	 */
	@Event() afOnFocusout: EventEmitter;

	/**
	 * Inputelementets 'oninput'-event.
	 * @en The input element's 'oninput' event.
	 */
	@Event() afOnInput: EventEmitter;

	/**
	 * Sker vid inputfältets första 'oninput'
	 * @en First time the input element receives an input
	 */
	@Event() afOnDirty: EventEmitter;

	/**
	 * Sker vid inputfältets första 'onblur'
	 * @en First time the input element is blurred
	 */
	@Event() afOnTouched: EventEmitter;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	@Watch('afValidationText')
	afValidationTextWatch() {
		this.setActiveValidationMessage();
	}

	/**
	 * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
	 * @en Returns a reference to the input element. Handy for setting focus programmatically.
	 */
	@Method()
	async afMGetFormControlElement() {
		return this._input;
	}

	componentWillLoad() {
		this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
		this.setActiveValidationMessage();
		this.setHasButton();
	}

	componentWillUpdate() {
		this.setHasButton();
	}

	componentDidLoad(){
		this.afOnReady.emit(); 
	}

	setActiveValidationMessage() {
		this.hasActiveValidationMessage = !!this.afValidationText;
	}

	get cssModifiers() {
		return {
			'digi-form-input--small': this.afVariation === FormInputVariation.SMALL,
			'digi-form-input--medium': this.afVariation === FormInputVariation.MEDIUM,
			'digi-form-input--large': this.afVariation === FormInputVariation.LARGE,
			'digi-form-input--neutral':
				this.afValidation === FormInputValidation.NEUTRAL,
			'digi-form-input--success':
				this.afValidation === FormInputValidation.SUCCESS,
			'digi-form-input--error': this.afValidation === FormInputValidation.ERROR,
			'digi-form-input--warning':
				this.afValidation === FormInputValidation.WARNING,
			[`digi-form-input--button-variation-${this.afButtonVariation}`]:
				!!this.afButtonVariation,
			[`digi-form-input--has-button-${this.hasButton}`]: true,
			'digi-form-input--empty':
				!this.afValue &&
				(!this.afValidation || this.afValidation === FormInputValidation.NEUTRAL)
		};
	}

	blurHandler(e) {
		if (!this.touched) {
			this.afOnTouched.emit(e);
			this.touched = true;
		}
		this.afOnBlur.emit(e);
	}

	changeHandler(e) {
		this.setActiveValidationMessage();
		this.afOnChange.emit(e);
	}

	focusHandler(e) {
		this.afOnFocus.emit(e);
	}

	focusoutHandler(e) {
		this.afOnFocusout.emit(e);
	}

	keyupHandler(e) {
		this.afOnKeyup.emit(e);
	}

	inputHandler(e) {
		this.afValue = this.value =
			this.afType === FormInputType.NUMBER
				? parseFloat(e.target.value)
				: e.target.value;
		if (!this.dirty) {
			this.afOnDirty.emit(e);
			this.dirty = true;
		}
		this.setActiveValidationMessage();
		this.afOnInput.emit(e);
	}

	setHasButton() {
		this.hasButton = !!this.hostElement.querySelector('[slot="button"]');
	}

	render() {
		return (
			<div
				class={{
					'digi-form-input': true,
					...this.cssModifiers
				}}
			>
				<digi-form-label
					class="digi-form-input__label"
					afFor={this.afId}
					afLabel={this.afLabel}
					afDescription={this.afLabelDescription}
					afRequired={this.afRequired}
					afAnnounceIfOptional={this.afAnnounceIfOptional}
					afRequiredText={this.afRequiredText}
					afAnnounceIfOptionalText={this.afAnnounceIfOptionalText}
				></digi-form-label>
				<div class="digi-form-input__input-wrapper">
					<input
						class="digi-form-input__input"
						ref={(el) => (this._input = el as HTMLInputElement)}
						onBlur={(e) => this.blurHandler(e)}
						onChange={(e) => this.changeHandler(e)}
						onFocus={(e) => this.focusHandler(e)}
						onFocusout={(e) => this.focusoutHandler(e)}
						onKeyUp={(e) => this.keyupHandler(e)}
						onInput={(e) => this.inputHandler(e)}
						aria-activedescendant={this.afAriaActivedescendant}
						aria-describedby={this.afAriaDescribedby}
						aria-labelledby={this.afAriaLabelledby}
						aria-autocomplete={this.afAriaAutocomplete}
						aria-invalid={this.afValidation != FormInputValidation.NEUTRAL ? 'true' : 'false'}
						autocomplete={this.afAutocomplete}
						autofocus={this.afAutofocus ? this.afAutofocus : null}
						maxLength={this.afMaxlength}
						minLength={this.afMinlength}
						max={this.afMax}
						min={this.afMin}
						step={this.afStep}
						list={this.afList}
						role={this.afRole}
						dirName={this.afDirname}
						required={this.afRequired}
						id={this.afId}
						name={this.afName}
						type={this.afType}
						value={this.afValue}
						{...(this.afInputmode == FormInputMode.DEFAULT ? {} : { inputmode: this.afInputmode })}
					/>
					<slot name="button"></slot>
				</div>
				<div class="digi-form-input__footer">
					<div
						class="digi-form-input__validation"
						aria-atomic="true"
						role="alert"
						id={`${this.afId}--validation-message`}
					>
						{this.hasActiveValidationMessage &&
							this.afValidation != FormInputValidation.NEUTRAL && (
								<digi-form-validation-message
									class="digi-form-input__validation-message"
									af-variation={this.afValidation}
								>
									{this.afValidationText}
								</digi-form-validation-message>
							)}
					</div>
				</div>
			</div>
		);
	}
}
