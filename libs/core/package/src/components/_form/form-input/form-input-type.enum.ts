export enum FormInputType {
  COLOR = 'color',
  DATE = 'date',
  DATETIME_LOCAL = 'datetime-local',
  EMAIL = 'email',
  HIDDEN = 'hidden',
  MONTH = 'month',
  NUMBER = 'number',
  PASSWORD = 'password',
  SEARCH = 'search',
  TEL = 'tel',
  TEXT = 'text',
  TIME = 'time',
  URL = 'url',
  WEEK = 'week'
}
