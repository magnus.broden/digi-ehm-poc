import { Component, Element, Prop, Watch, h, State } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { _t } from '@digi/shared/text';

/**
 * @slot actions - Förväntar sig interaktiva och ikonstora komponenter
 * @swedishName Etikett
 */
@Component({
	tag: 'digi-form-label',
	styleUrls: ['form-label.scss'],
	scoped: true
})
export class FormLabel {
	@Element() hostElement: HTMLStencilElement;

	@State() labelText: string;

	@State() hasActionSlot: boolean;

	/**
	 * Texten till labelelementet
	 * @en The label text
	 */
	@Prop() afLabel!: string;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Set id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-label');

	/**
	 * Sätter attributet 'for'. Ska vara identiskt med attributet 'id' på inputelementet
	 * @en Set for attribute. Should be identical with the id attribute of the associated input element.
	 */
	@Prop() afFor!: string;

	/**
	 * Valfri beskrivande text
	 * @en A description text
	 */
	@Prop() afDescription: string;

	/**
	 * Aktivera om det tillhörande inputelementet är obligatoriskt
	 * @en Set to true if the associated input element is required
	 */
	@Prop() afRequired: boolean;

	/**
	 * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
	 * @en Set this to true if the form contains more required fields than optional fields.
	 */
	@Prop() afAnnounceIfOptional = false;

	/**
	 * Sätter text för afRequired.
	 * @en Set text for afRequired.
	 */
	@Prop() afRequiredText = _t.required.toLowerCase();

	/**
	 * Sätter text för afAnnounceIfOptional.
	 * @en Set text for afAnnounceIfOptional.
	 */
	@Prop() afAnnounceIfOptionalText = _t.optional.toLowerCase();

	componentWillLoad() {
		this.setLabelText();
		this.validateLabel();
		this.validateFor();
		this.handleSlotVisibility();
	}

	@Watch('afLabel')
	validateLabel() {}

	@Watch('afFor')
	validateFor() {
		if (this.afFor) return;

		logger.warn(
			`digi-form-label must have a for attribute. Please add a for attribute using af-for`,
			this.hostElement
		);
	}

	@Watch('afLabel')
	@Watch('afRequired')
	@Watch('afAnnounceIfOptional')
	setLabelText() {
		this.labelText = `${this.afLabel} ${this.requiredText}`;

		if (!this.afLabel) {
			logger.warn(
				`digi-form-label must have a label. Please add a label using af-label`,
				this.hostElement
			);
		}
	}

	handleSlotVisibility() {
		this.hasActionSlot = !!this.hostElement.querySelector('[slot="actions"]');
	}

	get requiredText() {
		return this.afRequired && !this.afAnnounceIfOptional
			? ` (${this.afRequiredText})`
			: !this.afRequired && this.afAnnounceIfOptional
			? ` (${this.afAnnounceIfOptionalText})`
			: '';
	}

	render() {
		return (
			<div class="digi-form-label">
				<div class="digi-form-label__label-group">
					{this.afFor && this.afLabel && (
						<label class="digi-form-label__label" htmlFor={this.afFor} id={this.afId}>
							<span class="digi-form-label__label--label">{this.labelText}</span>
							{this.afDescription &&
								<span class="digi-form-label__label--description">{this.afDescription}</span>
							}
						</label>
					)}
					{this.hasActionSlot && (
						<div class="digi-form-label__actions">
							<slot name="actions"></slot>
						</div>
					)}
				</div>
			</div>
		);
	}
}
