# digi-form-label

This is a label component for use with all kinds of form fields. It has two required props `af-label` (the label text), and `af-for` (an id of the associated form field). It can also have an optional description text and a slot for actions (mainly meant for popovers and other icon-based components).

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute                      | Description                                                                       | Type      | Default                                |
| -------------------------- | ------------------------------ | --------------------------------------------------------------------------------- | --------- | -------------------------------------- |
| `afAnnounceIfOptional`     | `af-announce-if-optional`      | Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria. | `boolean` | `false`                                |
| `afAnnounceIfOptionalText` | `af-announce-if-optional-text` | Sätter text för afAnnounceIfOptional.                                             | `string`  | `_t.optional.toLowerCase()`            |
| `afDescription`            | `af-description`               | Valfri beskrivande text                                                           | `string`  | `undefined`                            |
| `afFor` _(required)_       | `af-for`                       | Sätter attributet 'for'. Ska vara identiskt med attributet 'id' på inputelementet | `string`  | `undefined`                            |
| `afId`                     | `af-id`                        | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.             | `string`  | `randomIdGenerator('digi-form-label')` |
| `afLabel` _(required)_     | `af-label`                     | Texten till labelelementet                                                        | `string`  | `undefined`                            |
| `afRequired`               | `af-required`                  | Aktivera om det tillhörande inputelementet är obligatoriskt                       | `boolean` | `undefined`                            |
| `afRequiredText`           | `af-required-text`             | Sätter text för afRequired.                                                       | `string`  | `_t.required.toLowerCase()`            |


## Slots

| Slot        | Description                                         |
| ----------- | --------------------------------------------------- |
| `"actions"` | Förväntar sig interaktiva och ikonstora komponenter |


## Dependencies

### Used by

 - [digi-form-input](../form-input)
 - [digi-form-select](../form-select)
 - [digi-form-textarea](../form-textarea)

### Graph
```mermaid
graph TD;
  digi-form-input --> digi-form-label
  digi-form-select --> digi-form-label
  digi-form-textarea --> digi-form-label
  style digi-form-label fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
