# digi-form-select

This is a select component. The component includes a label, description, support for initial selected value, validation and variations.

Validation: The component exist in three different validations (success, error, warning).

Variations: The component exist in three different variations (S, M, L).

If a placeholder text is used it will be the selected value on load, otherwise the first option will get selected or if a initial value is set that option will get selected regardless if a placeholder is used or not.

Head over to the `Knobs` tab to test it out!

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormSelectValidation, FormSelectVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute                      | Description                                                                                                                                                                                                     | Type                                                                                                                         | Default                                 |
| -------------------------- | ------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- | --------------------------------------- |
| `afAnnounceIfOptional`     | `af-announce-if-optional`      | Normalt betonas ett obligatoriskt fält visuellt och semantiskt. Detta vänder på det och betonar fältet om det inte krävs. Ställ in detta till sant om ditt formulär innehåller fler obligatoriska fält än inte. | `boolean`                                                                                                                    | `false`                                 |
| `afAnnounceIfOptionalText` | `af-announce-if-optional-text` | Sätter text för afAnnounceIfOptional.                                                                                                                                                                           | `string`                                                                                                                     | `undefined`                             |
| `afAutofocus`              | `af-autofocus`                 | Sätter attributet 'autofocus'.                                                                                                                                                                                  | `boolean`                                                                                                                    | `undefined`                             |
| `afDescription`            | `af-description`               | En valfri beskrivningstext.                                                                                                                                                                                     | `string`                                                                                                                     | `undefined`                             |
| `afDisableValidation`      | `af-disable-validation`        | Inaktiverar valideringen                                                                                                                                                                                        | `boolean`                                                                                                                    | `undefined`                             |
| `afId`                     | `af-id`                        | Sätter ett id attribut.                                                                                                                                                                                         | `string`                                                                                                                     | `randomIdGenerator('digi-form-select')` |
| `afLabel` _(required)_     | `af-label`                     | Sätter en text för select.                                                                                                                                                                                      | `string`                                                                                                                     | `undefined`                             |
| `afName`                   | `af-name`                      | Sätter väljarens namn.                                                                                                                                                                                          | `string`                                                                                                                     | `undefined`                             |
| `afPlaceholder`            | `af-placeholder`               | En platshållare för väljaren                                                                                                                                                                                    | `string`                                                                                                                     | `undefined`                             |
| `afRequired`               | `af-required`                  | Är det ihopkopplade fältet tvingat?                                                                                                                                                                             | `boolean`                                                                                                                    | `undefined`                             |
| `afRequiredText`           | `af-required-text`             | Sätter text för afRequired.                                                                                                                                                                                     | `string`                                                                                                                     | `undefined`                             |
| `afStartSelected`          | `af-start-selected`            | <span style="color:red">**[DEPRECATED]**</span> Använd afValue istället<br/><br/>Sätter ett förvalt värde.                                                                                                      | `number`                                                                                                                     | `undefined`                             |
| `afValidation`             | `af-validation`                | Sätter väljarens validering                                                                                                                                                                                     | `FormSelectValidation.ERROR \| FormSelectValidation.NEUTRAL \| FormSelectValidation.SUCCESS \| FormSelectValidation.WARNING` | `FormSelectValidation.NEUTRAL`          |
| `afValidationText`         | `af-validation-text`           | Sätter valideringstexten.                                                                                                                                                                                       | `string`                                                                                                                     | `undefined`                             |
| `afValue`                  | `af-value`                     | Sätter attributet 'value'.                                                                                                                                                                                      | `string`                                                                                                                     | `undefined`                             |
| `afVariation`              | `af-variation`                 | Sätter väljarens storlek.                                                                                                                                                                                       | `FormSelectVariation.LARGE \| FormSelectVariation.MEDIUM \| FormSelectVariation.SMALL`                                       | `FormSelectVariation.MEDIUM`            |
| `value`                    | `value`                        | Sätter attributet 'value'                                                                                                                                                                                       | `string`                                                                                                                     | `undefined`                             |


## Events

| Event          | Description                                                                     | Type               |
| -------------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnBlur`     |                                                                                 | `CustomEvent<any>` |
| `afOnChange`   |                                                                                 | `CustomEvent<any>` |
| `afOnDirty`    | Sker första gången väljarelementet ändras.                                      | `CustomEvent<any>` |
| `afOnFocus`    |                                                                                 | `CustomEvent<any>` |
| `afOnFocusout` |                                                                                 | `CustomEvent<any>` |
| `afOnReady`    | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |
| `afOnSelect`   |                                                                                 | `CustomEvent<any>` |


## Methods

### `afMGetFormControlElement() => Promise<HTMLSelectElement>`

Returnerar en referens till formulärkontrollelementet.

#### Returns

Type: `Promise<HTMLSelectElement>`




## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Should be an option element |


## CSS Custom Properties

| Name                                                 | Description                                        |
| ---------------------------------------------------- | -------------------------------------------------- |
| `--digi--form-select--background--empty`             | var(--digi--color--background--input-empty);       |
| `--digi--form-select--background--error`             | var(--digi--color--background--danger-2);          |
| `--digi--form-select--background--neutral`           | var(--digi--color--background--input);             |
| `--digi--form-select--background--success`           | var(--digi--color--background--success-2);         |
| `--digi--form-select--background--warning`           | var(--digi--color--background--warning-2);         |
| `--digi--form-select--border-color--error`           | var(--digi--color--border--danger);                |
| `--digi--form-select--border-color--neutral`         | var(--digi--color--border--neutral-3);             |
| `--digi--form-select--border-color--success`         | var(--digi--color--border--success);               |
| `--digi--form-select--border-color--warning`         | var(--digi--color--border--neutral-3);             |
| `--digi--form-select--footer--margin-top`            | var(--digi--gutter--medium);                       |
| `--digi--form-select--icon--color`                   | var(--digi--color--icons--primary);                |
| `--digi--form-select--icon--inline-end`              | 0.75rem;                                           |
| `--digi--form-select--icon--top`                     | 50%;                                               |
| `--digi--form-select--icon--transform`               | translateY(-50%);                                  |
| `--digi--form-select--label--margin-bottom`          | var(--digi--gutter--medium);                       |
| `--digi--form-select--select--border-radius`         | var(--digi--border-radius--input);                 |
| `--digi--form-select--select--border-width--error`   | var(--digi--border-width--input-validation);       |
| `--digi--form-select--select--border-width--neutral` | var(--digi--border-width--input-regular);          |
| `--digi--form-select--select--border-width--success` | var(--digi--border-width--input-validation);       |
| `--digi--form-select--select--border-width--warning` | var(--digi--border-width--input-validation);       |
| `--digi--form-select--select--font-size`             | var(--digi--typography--body--font-size--desktop); |
| `--digi--form-select--select--height--large`         | var(--digi--input-height--large);                  |
| `--digi--form-select--select--height--medium`        | var(--digi--input-height--medium);                 |
| `--digi--form-select--select--height--small`         | var(--digi--input-height--small);                  |
| `--digi--form-select--select--padding`               | 0 var(--digi--gutter--medium);                     |
| `--digi--form-select--select--padding--inline-end`   | var(--digi--padding--large);                       |
| `--digi--form-select--select--width`                 | 100%;                                              |
| `--digi--form-select--select-option--font-size`      | var(--digi--typography--body--font-size--desktop); |
| `--digi--form-select--select-wrapper--position`      | relative;                                          |


## Dependencies

### Depends on

- [digi-form-label](../form-label)
- [digi-icon](../../_icon/icon)
- [digi-util-mutation-observer](../../_util/util-mutation-observer)
- [digi-form-validation-message](../form-validation-message)

### Graph
```mermaid
graph TD;
  digi-form-select --> digi-form-label
  digi-form-select --> digi-icon
  digi-form-select --> digi-util-mutation-observer
  digi-form-select --> digi-form-validation-message
  digi-form-validation-message --> digi-icon
  style digi-form-select fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
