import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormSelectValidation } from './form-select-validation.enum';
import { FormSelectVariation } from './form-select-variation.enum';

export default {
	title: 'form/digi-form-select',
	parameters: {
		actions: {
			handles: ['afOnChange', 'afOnFocus', 'afOnBlur', 'afOnFocusout', 'afOnDirty']
		}
	},
	argTypes: {
		'af-variation': enumSelect(FormSelectVariation),
		'af-validation': enumSelect(FormSelectValidation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-select',
	'af-label': 'A label is required',
	'af-required': false,
	'af-placeholder': '',
	'af-announce-if-optional': false,
	'af-id': null,
	'af-name': '',
	'af-description': '',
	value: '',
	'af-value': '',
	'af-disable-validation': false,
	'af-variation': FormSelectVariation.MEDIUM,
	'af-validation': FormSelectValidation.NEUTRAL,
	'af-validation-text': '',
	'af-start-selected': null,
	/* html */
	children: `
    <option name="yrken" value="yrken">Yrken</option>
    <option name="utbildning" value="utbildning">Utbildning</option>`
};
