import {
	Component,
	Element,
	Event,
	Method,
	EventEmitter,
	Prop,
	h
} from '@stencil/core';
import { HTMLStencilElement, State, Watch } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { FormSelectValidation } from './form-select-validation.enum';
import { FormSelectVariation } from './form-select-variation.enum';

/**
 * @slot default - Should be an option element
 *
 * @enums FormSelectValidation - form-select-validation.enum
 * @enums FormSelectVariation - form-select-variation.enum
 * @swedishName Väljare
 */
@Component({
	tag: 'digi-form-select',
	styleUrls: ['form-select.scss'],
	scoped: true
})
export class FormSelect {
	@Element() hostElement: HTMLStencilElement;

	private _select?: HTMLSelectElement;
	private _observer;

	@State() optionItems = [];
	@State() dirty = false;
	@State() touched = false;
	@State() hasPlaceholder = false;

	/**
	 * Sätter en text för select.
	 * @en Sets a label for select.
	 */
	@Prop() afLabel!: string;

	/**
	 * Är det ihopkopplade fältet tvingat?
	 * @en Is the associated field required?
	 */
	@Prop() afRequired: boolean;

	/**
	 * Sätter text för afRequired.
	 * @en Set text for afRequired.
	 */

	@Prop() afRequiredText: string;

	/**
	 * En platshållare för väljaren
	 * @en A placeholder for the select
	 */
	@Prop() afPlaceholder: string;

	/**
	 * Normalt betonas ett obligatoriskt fält visuellt och semantiskt.
	 * Detta vänder på det och betonar fältet om det inte krävs.
	 * Ställ in detta till sant om ditt formulär innehåller fler obligatoriska fält än inte.
	 * @en Normally a required field is visually and semantically emphasized.
	 * This flips that and emphasizes the field if it is not required.
	 * Set this to true if your form contains more required fields than not.
	 */
	@Prop() afAnnounceIfOptional: boolean = false;

	/**
	 * Sätter text för afAnnounceIfOptional.
	 * @en Set text for afAnnounceIfOptional.
	 */
	@Prop() afAnnounceIfOptionalText: string;

	/**
	 * Sätter ett id attribut.
	 * @en Sets an id attribute. Standard är slumpmässig sträng.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-select');

	/**
	 * Sätter väljarens namn.
	 * @en Sets the select name.
	 */
	@Prop() afName: string;

	/**
	 * En valfri beskrivningstext.
	 * @en An optional description text.
	 */
	@Prop() afDescription: string;

	/**
	 * Sätter attributet 'value'
	 * @en Input value attribute
	 * @ignore
	 */
	@Prop() value: string;
	@Watch('value') onValueChanged(value) {
		this.afValue = value;
	}

	/**
	 * Sätter attributet 'value'.
	 * @en Input value attribute
	 */
	@Prop() afValue: string;
	@Watch('afValue') onAfValueChanged(value) {
		this.value = value;
	}

	/**
	 * Inaktiverar valideringen
	 * @en Disables the validation.
	 */
	@Prop() afDisableValidation: boolean;

	/**
	 * Sätter väljarens storlek.
	 * @en Set select input size.
	 */
	@Prop() afVariation: FormSelectVariation = FormSelectVariation.MEDIUM;

	/**
	 * Sätter väljarens validering
	 * @en Set select validation
	 */
	@Prop() afValidation: FormSelectValidation = FormSelectValidation.NEUTRAL;

	/**
	 * Sätter valideringstexten.
	 * @en Sets the validation text.
	 */
	@Prop() afValidationText: string;

	/**
	 * Sätter ett förvalt värde.
	 * @deprecated Använd afValue istället
	 * @en Sets initial selected value.
	 */
	@Prop() afStartSelected: number;

	/**
	 * Sätter attributet 'autofocus'.
	 * @en Input autofocus attribute
	 */
	@Prop() afAutofocus: boolean;

	@Event() afOnChange: EventEmitter;
	@Event() afOnSelect: EventEmitter;
	@Event() afOnFocus: EventEmitter;
	@Event() afOnFocusout: EventEmitter;
	@Event() afOnBlur: EventEmitter;

	/**
	 * Sker första gången väljarelementet ändras.
	 * @en First time the select element is changed.
	 */
	@Event() afOnDirty: EventEmitter;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	getOptions() {
		this.checkIfPlaceholder();

		let options: HTMLFormElement = this._observer.children;

		if (!options || options.length <= 0) {
			logger.warn(`The slot contains no option elements.`, this.hostElement);
			return;
		}

		this.optionItems = [...Array.from(options)]
			.filter((el) => el.tagName.toLowerCase() === 'option')
			.map((el) => {
				return {
					value: el['value'],
					text: el['text']
				};
			});
	}

	checkIfPlaceholder() {
		this.hasPlaceholder = !!this.afPlaceholder;
	}

	componentWillLoad() {
		this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
	}

	componentDidLoad() {
		this.getOptions();
		this.afStartSelected &&
			(this.afValue = this.optionItems[this.afStartSelected].value);
		this.afOnReady.emit(); 
	}

	/**
	 * Returnerar en referens till formulärkontrollelementet.
	 * @en Returns a reference to the form control element.
	 */
	@Method()
	async afMGetFormControlElement() {
		return this._select;
	}

	get cssModifiers() {
		return {
			'digi-form-select--small': this.afVariation === FormSelectVariation.SMALL,
			'digi-form-select--medium': this.afVariation === FormSelectVariation.MEDIUM,
			'digi-form-select--large': this.afVariation === FormSelectVariation.LARGE,
			'digi-form-select--neutral':
				this.afValidation === FormSelectValidation.NEUTRAL,
			'digi-form-select--success':
				this.afValidation === FormSelectValidation.SUCCESS,
			'digi-form-select--warning':
				this.afValidation === FormSelectValidation.WARNING,
			'digi-form-select--error': this.afValidation === FormSelectValidation.ERROR,
			'digi-form-select--empty':
				!this.afValue &&
				(!this.afValidation || this.afValidation === FormSelectValidation.NEUTRAL)
		};
	}

	changeHandler(e) {
		if (!this.dirty) {
			this.afOnDirty.emit(e);
			this.dirty = true;
		}
		this.value = this.afValue = e.target.value;
		this.afOnChange.emit(e);
	}
	selectHandler(e) {
		this.value = this.afValue = e.target.value;
		this.afOnSelect.emit(e);
	}

	focusHandler(e) {
		this.afOnFocus.emit(e);
	}

	focusoutHandler(e) {
		this.afOnFocusout.emit(e);
	}

	blurHandler(e) {
		this.afOnBlur.emit(e);
	}

	getSelectedItem(option) {
		return this.afValue == option.value;
	}

	showValidation() {
		return !this.afDisableValidation &&
			this.afValidation !== FormSelectValidation.NEUTRAL &&
			this.afValidationText
			? true
			: false;
	}

	render() {
		return (
			<div
				class={{
					'digi-form-select': true,
					...this.cssModifiers
				}}
			>
				<div class="digi-form-select__label-group">
					<digi-form-label
						afFor={this.afId}
						afLabel={this.afLabel}
						afId={`${this.afId}-label`}
						afDescription={this.afDescription}
						afRequired={this.afRequired}
						afAnnounceIfOptional={this.afAnnounceIfOptional}
						afRequiredText={this.afRequiredText}
						afAnnounceIfOptionalText={this.afAnnounceIfOptionalText}
					></digi-form-label>
				</div>
				<div class="digi-form-select__select-wrapper">
					<select
						class="digi-form-select__select"
						name={this.afName}
						id={this.afId}
						ref={(el) => (this._select = el as HTMLSelectElement)}
						required={this.afRequired ? this.afRequired : null}
						onFocus={(e) => this.focusHandler(e)}
						onFocusout={(e) => this.focusoutHandler(e)}
						onBlur={(e) => this.blurHandler(e)}
						onChange={(e) => this.changeHandler(e)}
						onInput={(e) => this.selectHandler(e)}
						autoFocus={this.afAutofocus ? this.afAutofocus : null}
					>
						{this.hasPlaceholder && (
							<option
								class={{
									'digi-form-select__select-option': true,
									'digi-form-select__select-option--initial-value': true
								}}
								disabled
								selected={!this.afValue}
								value=""
							>
								{this.afPlaceholder}
							</option>
						)}
						{this.optionItems.map((option, index) => {
							return (
								<option
									key={index}
									class="digi-form-select__select-option"
									value={option.value}
									selected={this.getSelectedItem(option)}
								>
									{option.text}
								</option>
							);
						})}
					</select>
					<digi-icon
						class="digi-form-select__icon"
						slot="icon"
						afName={`input-select-marker`}
					></digi-icon>
				</div>

				<digi-util-mutation-observer
					hidden
					ref={(el) => (this._observer = el)}
					onAfOnChange={() => this.getOptions()}
				>
					<slot></slot>
				</digi-util-mutation-observer>

				<div class="digi-form-select__footer">
					<div
						aria-atomic="true"
						role="alert"
						id={`${this.afId}--validation-message`}
					>
						{this.showValidation() && (
							<digi-form-validation-message af-variation={this.afValidation}>
								{this.afValidationText}
							</digi-form-validation-message>
						)}
					</div>
				</div>
			</div>
		);
	}
}
