import {
	Component,
	Element,
	Event,
	EventEmitter,
	Method,
	Prop,
	State,
	h
} from '@stencil/core';
import { HTMLStencilElement, Watch } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormCheckboxVariation } from './form-checkbox-variation.enum';
import { FormCheckboxValidation } from './form-checkbox-validation.enum';
import { FormCheckboxLayout } from './form-checkbox-layout.enum';

/**
 * @enums FormCheckboxValidation - form-checkbox-validation.enum.ts
 * @enums FormCheckboxVariation - form-checkbox-variation.enum.ts
 * @swedishName Kryssruta
 */
@Component({
	tag: 'digi-form-checkbox',
	styleUrls: ['form-checkbox.scss'],
	scoped: true
})
export class FormCheckbox {
	@Element() hostElement: HTMLStencilElement;

	private _input?: HTMLInputElement;

	@State() dirty = false;
	@State() touched = false;

	/**
	 * Texten till labelelementet
	 * @en The label text
	 */
	@Prop() afLabel!: string;

	/**
	 * Sätter variant. Kan vara 'primary' eller 'secondary'
	 * @en Set checkbox variation.
	 */
	@Prop() afVariation: FormCheckboxVariation = FormCheckboxVariation.PRIMARY;

	/**
	 * Sätter layouten.
	 * @en Set checkbox layout.
	 */
	@Prop() afLayout: FormCheckboxLayout = FormCheckboxLayout.INLINE;

	/**
	 * Sätter property 'indeterminate' på inputelementet.
	 * @en Set indeterminate property.
	 */
	@Prop() afIndeterminate: boolean = false;

	/**
	 * Sätter attributet 'name'.
	 * @en Input name attribute
	 */
	@Prop() afName: string;

	/**
	 * Sätter attributet 'checked'.
	 * @en Input checked attribute
	 * @ignore
	 */
	@Prop() checked: boolean;
	@Watch('checked') onCheckedChanged(checked) {
		this.afChecked = checked;
	}

	/**
	 * Sätter attributet 'checked'.
	 * @en Input checked attribute
	 */
	@Prop() afChecked: boolean;
	@Watch('checked') onAfCheckedChanged(checked) {
		this.checked = checked;
	}

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-checkbox');

	/**
	 * Sätter attributet 'required'
	 * @en Input required attribute
	 */
	@Prop() afRequired: boolean;

	/**
	 * Sätter attributet 'value'
	 * @en Input value attribute
	 * @ignore
	 */
	@Prop() value: string;
	@Watch('value') onValueChanged(value) {
		this.afValue = value;
	}

	/**
	 * Sätter attributet 'value'.
	 * @en Input value attribute
	 */
	@Prop() afValue: string;
	@Watch('afValue') onAfValueChanged(value) {
		this.value = value;
	}

	/**
	 * Sätter valideringsstatus. Kan vara 'error', 'varning', eller ingenting.
	 * @en Set input validation. Can be 'error', 'warning', or nothing.
	 */
	@Prop() afValidation: FormCheckboxValidation;

	/**
	 * Sätter attributet 'aria-labelledby'
	 * @en Input aria-labelledby attribute
	 */
	@Prop() afAriaLabelledby: string;

	/**
	 * Sätter attributet 'aria-label'
	 * @en Input aria-label attribute
	 */
	@Prop() afAriaLabel: string;

	/**
	 * Ytterligare beskrivande text
	 * @en Additional description text
	 */
	@Prop() afDescription: string;

	/**
	 * Sätter attributet 'aria-describedby'
	 * @en Input aria-describedby attribute
	 */
	@Prop() afAriaDescribedby: string;

	/**
	 * Sätter attributet 'autofocus'.
	 * @en Input autofocus attribute
	 */
	@Prop() afAutofocus: boolean;

	/**
	 * Inputelementets 'onchange'-event.
	 * @en The input element's 'onchange' event.
	 */
	@Event() afOnChange: EventEmitter;

	/**
	 * Inputelementets 'onblur'-event.
	 * @en The input element's 'onblur' event.
	 */
	@Event() afOnBlur: EventEmitter;

	/**
	 * Inputelementets 'onfocus'-event.
	 * @en The input element's 'onfocus' event.
	 */
	@Event() afOnFocus: EventEmitter;

	/**
	 * Inputelementets 'onfocusout'-event.
	 * @en The input element's 'onfocusout' event.
	 */
	@Event() afOnFocusout: EventEmitter;

	/**
	 * Inputelementets 'oninput'-event.
	 * @en The input element's 'oninput' event.
	 */
	@Event() afOnInput: EventEmitter;

	/**
	 * Sker vid inputfältets första 'oninput'
	 * @en First time the input element receives an input
	 */
	@Event() afOnDirty: EventEmitter;

	/**
	 * Sker vid inputfältets första 'onblur'
	 * @en First time the input element is blurred
	 */
	@Event() afOnTouched: EventEmitter;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	/**
	 * Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
	 * @en Returns a reference to the input element. Handy for setting focus programmatically.
	 */
	@Method()
	async afMGetFormControlElement() {
		return this._input;
	}

	componentWillLoad() {
		this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
		this.afChecked
			? (this.checked = this.afChecked)
			: (this.afChecked = this.checked);
	}
	componentDidLoad(){
		this.afOnReady.emit(); 
	}

	get cssModifiers() {
		return {
			'digi-form-checkbox--error':
				this.afValidation === FormCheckboxValidation.ERROR,
			'digi-form-checkbox--warning':
				this.afValidation === FormCheckboxValidation.WARNING,
			'digi-form-checkbox--primary':
				this.afVariation === FormCheckboxVariation.PRIMARY,
			'digi-form-checkbox--secondary':
				this.afVariation === FormCheckboxVariation.SECONDARY,
			'digi-form-checkbox--indeterminate': this.afIndeterminate,
			[`digi-form-checkbox--layout-${this.afLayout}`]: !!this.afLayout
		};
	}

	blurHandler(e) {
		if (!this.touched) {
			this.afOnTouched.emit(e);
			this.touched = true;
		}
		this.afOnBlur.emit(e);
	}

	changeHandler(e) {
		this.checked = this.afChecked = e.target.checked;
		this.afValue = this.value = e.target.value;
		this.afOnChange.emit(e);
	}

	focusHandler(e) {
		this.afOnFocus.emit(e);
	}

	focusoutHandler(e) {
		this.afOnFocusout.emit(e);
	}

	inputHandler(e) {
		if (!this.dirty) {
			this.afOnDirty.emit(e);
			this.dirty = true;
		}

		this.afOnInput.emit(e);
	}

	render() {
		return (
			<div
				class={{
					'digi-form-checkbox': true,
					...this.cssModifiers
				}}
			>
				<input
					class="digi-form-checkbox__input"
					ref={(el) => (this._input = el as HTMLInputElement)}
					onInput={(e) => this.inputHandler(e)}
					onBlur={(e) => this.blurHandler(e)}
					onChange={(e) => this.changeHandler(e)}
					onFocus={(e) => this.focusHandler(e)}
					onFocusout={(e) => this.focusoutHandler(e)}
					aria-describedby={
						!this.afAriaDescribedby && !this.afDescription
							? null
							: `${!!this.afAriaDescribedby ? `${this.afAriaDescribedby} ` : ''}${!!this.afDescription ? `${this.afId}-description` : ''
							}`
					}
					aria-labelledby={this.afAriaLabelledby}
					aria-label={this.afAriaLabel}
					aria-invalid={
						this.afValidation === FormCheckboxValidation.ERROR ? 'true' : 'false'
					}
					required={this.afRequired}
					id={this.afId}
					name={this.afName}
					type="checkbox"
					checked={this.afChecked}
					value={this.afValue}
					indeterminate={this.afIndeterminate}
					autofocus={this.afAutofocus ? this.afAutofocus : null}
				/>
				<label htmlFor={this.afId} class="digi-form-checkbox__label">
					<span class="digi-form-checkbox__box"></span>
					{this.afLabel}
				</label>
				{!!this.afDescription && (
					<p id={`${this.afId}-description`} class="digi-form-checkbox__description">
						{this.afDescription}
					</p>
				)}
			</div>
		);
	}
}
