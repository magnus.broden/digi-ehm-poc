export enum FormCheckboxLayout {
	INLINE = 'inline',
	BLOCK = 'block'
}
