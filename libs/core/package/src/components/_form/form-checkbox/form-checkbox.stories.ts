import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormCheckboxVariation } from './form-checkbox-variation.enum';
import { FormCheckboxValidation } from './form-checkbox-validation.enum';
import { FormCheckboxLayout } from './form-checkbox-layout.enum';

export default {
	title: 'form/digi-form-checkbox',
	parameters: {
		actions: {
			handles: [
				'afOnChange',
				'afOnBlur',
				'afOnFocus',
				'afOnFocusout',
				'afOnInput',
				'afOnDirty',
				'afOnTouched'
			]
		}
	},
	argTypes: {
		'af-variation': enumSelect(FormCheckboxVariation),
		'af-validation': enumSelect(FormCheckboxValidation),
		'af-layout': enumSelect(FormCheckboxLayout)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-checkbox',
	'af-label': 'A required label',
	'af-variation': FormCheckboxVariation.PRIMARY,
	'af-layout': FormCheckboxLayout.INLINE,
	'af-indeterminate': false,
	'af-name': '',
	checked: false,
	'af-checked': false,
	'af-id': null,
	'af-required': false,
	value: '',
	'af-value': '',
	'af-validation': null,
	'af-aria-labelledby': '',
	'af-aria-describedby': '',
	children: ''
};
