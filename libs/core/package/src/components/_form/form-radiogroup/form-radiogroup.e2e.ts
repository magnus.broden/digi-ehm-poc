import { newE2EPage } from '@stencil/core/testing';

describe('form-radiogroup', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<form-radiogroup></form-radiogroup>');
		const element = await page.find('form-radiogroup');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<form-radiogroup></form-radiogroup>');
		const component = await page.find('form-radiogroup');
		const element = await page.find('form-radiogroup >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
