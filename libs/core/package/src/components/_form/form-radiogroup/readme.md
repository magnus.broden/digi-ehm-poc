# digi-form-radiogroup

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description            | Type     | Default                                     |
| --------- | ---------- | ---------------------- | -------- | ------------------------------------------- |
| `afName`  | `af-name`  | Namnet på radiogruppen | `string` | `randomIdGenerator('digi-form-radiogroup')` |
| `afValue` | `af-value` | Värdet på radiogruppen | `string` | `undefined`                                 |
| `value`   | `value`    | Värdet på radiogruppen | `string` | `undefined`                                 |


## Events

| Event             | Description             | Type                  |
| ----------------- | ----------------------- | --------------------- |
| `afOnGroupChange` | Event när värdet ändras | `CustomEvent<string>` |


## Slots

| Slot             | Description |
| ---------------- | ----------- |
| `"radiobuttons"` |             |


## Dependencies

### Depends on

- [digi-util-mutation-observer](../../_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-form-radiogroup --> digi-util-mutation-observer
  style digi-form-radiogroup fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
