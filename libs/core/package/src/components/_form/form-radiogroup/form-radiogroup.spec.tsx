import { newSpecPage } from '@stencil/core/testing';
import { FormRadiogroup } from './form-radiogroup';

describe('form-radiogroup', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [FormRadiogroup],
			html: '<form-radiogroup></form-radiogroup>'
		});
		expect(root).toEqualHtml(`
      <form-radiogroup>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </form-radiogroup>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [FormRadiogroup],
			html: `<form-radiogroup first="Stencil" last="'Don't call me a framework' JS"></form-radiogroup>`
		});
		expect(root).toEqualHtml(`
      <form-radiogroup first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </form-radiogroup>
    `);
	});
});
