import {
	Component,
	h,
	Host,
	Prop,
	State,
	Element,
	EventEmitter,
	Event,
	Watch
} from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator/randomIdGenerator.util';

/**
 * @slot radiobuttons
 *
 * @swedishName Radiogrupp
 */
@Component({
	tag: 'digi-form-radiogroup',
	styleUrl: 'form-radiogroup.scss',
	scoped: true
})
export class FormRadiogroup {
	@Element() hostElement: HTMLStencilElement;

	@State() radiobuttons: NodeListOf<HTMLDigiFormRadiobuttonElement>;

	/**
	 * Värdet på radiogruppen
	 * @en Radiogroup value
	 */
	@Prop({mutable: true}) afValue?: string;
	@Watch('afValue')
	OnAfValueChange(value) {
		this.value = value;
	}

	/**
	 * Värdet på radiogruppen
	 * @en Radiogroup value
	 * @ignore
	 */
	 @Prop({mutable: true}) value?: string;
	 @Watch('value')
	 OnValueChange(value) {
		 this.afValue = value;
		 this.setRadiobuttons(value);
	 }

	/**
	 * Namnet på radiogruppen
	 * @en Radiogroup name
	 */
	 @Prop() afName: string = randomIdGenerator('digi-form-radiogroup');

	/**
	 * Event när värdet ändras
	 * @en Event when value changes
	 */
	@Event() afOnGroupChange: EventEmitter<string>;

	radiobuttonChangeListener(e) {
		if (Array.from(this.radiobuttons).includes(e.target)) {
			e.preventDefault();
			this.afValue = e.detail.target.value;
			this.afOnGroupChange.emit(e);
		}
	}

	componentWillLoad() {}
	componentDidLoad() {
		this.setRadiobuttons(this.value || this.afValue);
		if (this.value) this.afValue = this.value;
	}
	componentWillUpdate() {}

	setRadiobuttons(value?) {
		this.radiobuttons = this.hostElement.querySelectorAll('digi-form-radiobutton');
		this.radiobuttons.forEach(r => {
			r.afName = this.afName;
			if (value && r.afValue == value) {
        r.setAttribute('af-checked', 'true')
			}
		});

	}

	get cssModifiers() {
		return {};
	}

	render() {
		return (
			<Host>
				<div
					class={{
						'digi-form-radiogroup': true,
						...this.cssModifiers
					}}
				>
					<digi-util-mutation-observer 
						onAfOnChange={(e) => this.radiobuttonChangeListener(e)}>
						<slot></slot>
					</digi-util-mutation-observer>
				</div>
			</Host>
		);
	}
}
