import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-input-search', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-input-search></digi-form-input-search>');

    const element = await page.find('digi-form-input-search');
    expect(element).toHaveClass('hydrated');
  });
});
