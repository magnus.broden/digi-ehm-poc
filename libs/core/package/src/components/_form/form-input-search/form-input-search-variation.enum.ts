export enum FormInputSearchVariation {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large'
}
