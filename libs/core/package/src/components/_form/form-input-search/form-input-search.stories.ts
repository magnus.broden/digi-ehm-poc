import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormInputSearchVariation } from './form-input-search-variation.enum';
import { FormInputType } from '../form-input/form-input-type.enum';
import { ButtonType } from '../../_button/button/button-type.enum';
import { FormInputButtonVariation } from '../form-input/form-input-button-variation.enum';

export default {
	title: 'form/digi-form-input-search',
	parameters: {
		actions: {
			handles: [
				'afOnFocusOutside',
				'afOnFocusInside',
				'afOnClickOutside',
				'afOnClickInside',
				'afOnChange',
				'afOnBlur',
				'afOnKeyup',
				'afOnFocus',
				'afOnFocusout',
				'afOnInput',
				'afOnClick'
			]
		}
	},
	argTypes: {
		'af-type': enumSelect(FormInputType),
		'af-variation': enumSelect(FormInputSearchVariation),
		'af-button-variation': enumSelect(FormInputButtonVariation),
		'af-button-type': enumSelect(ButtonType)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-input-search',
	'af-label': 'A label is required',
	'af-label-description': '',
	'af-type': FormInputType.SEARCH,
	'af-name': '',
	'af-id': null,
	value: '',
	'af-value': '',
	'af-autocomplete': '',
	'af-aria-autocomplete': '',
	'af-aria-activedescendant': '',
	'af-aria-labelledby': '',
	'af-aria-describedby': '',
	'af-variation': FormInputSearchVariation.MEDIUM,
	'af-button-variation': FormInputButtonVariation.PRIMARY,
	'af-hide-button': false,
	'af-button-type': ButtonType.SUBMIT,
	'af-button-text': 'Sök',
	'af-button-aria-label': '',
	'af-button-aria-labelledby': '',
	children: ''
};
