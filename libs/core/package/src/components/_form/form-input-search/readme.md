# digi-form-input-search

This is a search input, with a button inline. Use this for all your search and filter needs.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormInputSearchVariation, FormInputSearchType, FormInputSearchValidation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                 | Attribute                   | Description                                                              | Type                                                                                                                                                                                                                                                                                                                                    | Default                                       |
| ------------------------ | --------------------------- | ------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------- |
| `afAriaActivedescendant` | `af-aria-activedescendant`  | Sätter attributet 'aria-activedescendant'.                               | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afAriaAutocomplete`     | `af-aria-autocomplete`      | Sätter attributet 'aria-autocomplete'.                                   | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afAriaDescribedby`      | `af-aria-describedby`       | Sätter attributet 'aria-describedby'.                                    | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afAriaLabelledby`       | `af-aria-labelledby`        | Sätter attributet 'aria-labelledby'.                                     | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afAutocomplete`         | `af-autocomplete`           | Sätter attributet 'autocomplete'.                                        | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afAutofocus`            | `af-autofocus`              | Sätter attributet 'autofocus'.                                           | `boolean`                                                                                                                                                                                                                                                                                                                               | `undefined`                                   |
| `afButtonAriaLabel`      | `af-button-aria-label`      | Knappens aria label text                                                 | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afButtonAriaLabelledby` | `af-button-aria-labelledby` | Button aria-labelledby attribute                                         | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afButtonText`           | `af-button-text`            | Knappens text                                                            | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afButtonType`           | `af-button-type`            | Sätter knappelementets attribut 'type'. Bör vara 'submit' eller 'button' | `ButtonType.BUTTON \| ButtonType.RESET \| ButtonType.SUBMIT`                                                                                                                                                                                                                                                                            | `ButtonType.SUBMIT`                           |
| `afButtonVariation`      | `af-button-variation`       | Layout för hur inmatningsfältet ska visas då det finns en knapp          | `FormInputButtonVariation.PRIMARY \| FormInputButtonVariation.SECONDARY`                                                                                                                                                                                                                                                                | `FormInputButtonVariation.PRIMARY`            |
| `afHideButton`           | `af-hide-button`            | Dölj knappen                                                             | `boolean`                                                                                                                                                                                                                                                                                                                               | `undefined`                                   |
| `afId`                   | `af-id`                     | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.    | `string`                                                                                                                                                                                                                                                                                                                                | `randomIdGenerator('digi-form-input-search')` |
| `afLabel` _(required)_   | `af-label`                  | Texten till labelelementet                                               | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afLabelDescription`     | `af-label-description`      | Valfri beskrivande text                                                  | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afName`                 | `af-name`                   | Sätter attributet 'name'.                                                | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afType`                 | `af-type`                   | Sätter attributet 'type'.                                                | `FormInputType.COLOR \| FormInputType.DATE \| FormInputType.DATETIME_LOCAL \| FormInputType.EMAIL \| FormInputType.HIDDEN \| FormInputType.MONTH \| FormInputType.NUMBER \| FormInputType.PASSWORD \| FormInputType.SEARCH \| FormInputType.TEL \| FormInputType.TEXT \| FormInputType.TIME \| FormInputType.URL \| FormInputType.WEEK` | `FormInputType.SEARCH`                        |
| `afValue`                | `af-value`                  | Sätter attributet 'value'.                                               | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |
| `afVariation`            | `af-variation`              | Sätter variant. Kan vara 'small', 'medium' eller 'large'                 | `FormInputSearchVariation.LARGE \| FormInputSearchVariation.MEDIUM \| FormInputSearchVariation.SMALL`                                                                                                                                                                                                                                   | `FormInputSearchVariation.MEDIUM`             |
| `value`                  | `value`                     | Sätter attributet 'value'.                                               | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                                   |


## Events

| Event              | Description                                                                     | Type               |
| ------------------ | ------------------------------------------------------------------------------- | ------------------ |
| `afOnBlur`         | Inputelementets 'onblur'-event.                                                 | `CustomEvent<any>` |
| `afOnChange`       | Inputelementets 'onchange'-event.                                               | `CustomEvent<any>` |
| `afOnClick`        | Knappelementets 'onclick'-event.                                                | `CustomEvent<any>` |
| `afOnClickInside`  | Vid klick inuti komponenten.                                                    | `CustomEvent<any>` |
| `afOnClickOutside` | Vid klick utanför komponenten.                                                  | `CustomEvent<any>` |
| `afOnFocus`        | Inputelementets 'onfocus'-event.                                                | `CustomEvent<any>` |
| `afOnFocusInside`  | Vid fokus inuti komponenten.                                                    | `CustomEvent<any>` |
| `afOnFocusout`     | Inputelementets 'onfocusout'-event.                                             | `CustomEvent<any>` |
| `afOnFocusOutside` | Vid fokus utanför komponenten.                                                  | `CustomEvent<any>` |
| `afOnInput`        | Inputelementets 'oninput'-event.                                                | `CustomEvent<any>` |
| `afOnKeyup`        | Inputelementets 'onkeyup'-event.                                                | `CustomEvent<any>` |
| `afOnReady`        | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |


## Methods

### `afMGetFormControlElement() => Promise<any>`

Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.

#### Returns

Type: `Promise<any>`




## CSS Custom Properties

| Name                                                       | Description                               |
| ---------------------------------------------------------- | ----------------------------------------- |
| `--digi--form-input-search--input--border-radius--default` | var(--digi--border-radius--input-search); |


## Dependencies

### Depends on

- [digi-form-input](../form-input)
- [digi-button](../../_button/button)
- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-form-input-search --> digi-form-input
  digi-form-input-search --> digi-button
  digi-form-input-search --> digi-icon
  digi-form-input --> digi-form-label
  digi-form-input --> digi-form-validation-message
  digi-form-validation-message --> digi-icon
  style digi-form-input-search fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
