export enum FormTextareaVariation {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
  AUTO = 'auto'
}
