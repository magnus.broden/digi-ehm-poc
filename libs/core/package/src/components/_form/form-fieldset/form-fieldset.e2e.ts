import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-fieldset', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-fieldset></digi-form-fieldset>');

    const element = await page.find('digi-form-fieldset');
    expect(element).toHaveClass('hydrated');
  });
});
