import { Component, Prop, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

/**
 * @slot default - Ska vara formulärselement. Till exempel digi-form-input.
 * @swedishName Fältgruppering
 */
@Component({
  tag: 'digi-form-fieldset',
  styleUrls: ['form-fieldset.scss'],
  scoped: true,
})
export class FormFieldset {
  /**
   * Legend-elementets text.
   * @en The legend text.
   */
  @Prop() afLegend: string;

  /**
   * Fieldset-elementets namn attribut.
   * @en The fieldset name attribute.
   */
   @Prop() afName: string;

  /**
   * Fieldset-elementets form attribut.
   * @en The fieldset form attribute.
   */
  @Prop() afForm: string;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
   @Prop() afId: string = randomIdGenerator('digi-form-fieldset');

  render() {
    return (
      <fieldset class="digi-form-fieldset" name={this.afName} form={this.afForm} id={this.afId}>
        {this.afLegend && (
          <legend class="digi-form-fieldset__legend">{this.afLegend}</legend>
        )}
        <slot/>
      </fieldset>
    );
  }
}
