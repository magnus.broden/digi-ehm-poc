import { sortStories } from './util/story-helpers';
import { addParameters } from '@storybook/html';
import '@storybook/addon-console';
import coreCss from '!!style-loader?injectType=lazyStyleTag!css-loader!../../../../dist/libs/core/dist/digi-core/digi-core.css';
import skolverketCss from '!!style-loader?injectType=lazyStyleTag!css-loader!../../../../dist/libs/skolverket/dist/digi-skolverket/digi-skolverket.css';
import arbetsformedlingenCss from '!!style-loader?injectType=lazyStyleTag!css-loader!../../../../dist/libs/arbetsformedlingen/dist/digi-arbetsformedlingen/digi-arbetsformedlingen.css';
import cssVariablesTheme from '@etchteam/storybook-addon-css-variables-theme';

import { setStencilDocJson } from '@pxtrn/storybook-addon-docs-stencil';
import docJson from '../../../../dist/libs/core/docs.json';

// @ts-ignore
if (docJson) setStencilDocJson(docJson);

export const decorators = [cssVariablesTheme];

export const parameters = {
	controls: { hideNoControlsWarning: true },
	cssVariables: {
		files: {
			core: coreCss,
			skolverket: skolverketCss,
			arbetsformedlingen: arbetsformedlingenCss
		},
		defaultTheme: 'Core'
	}
};

const SORT_ORDER = {
	Documentation: ['Welcome']
};

addParameters({
	options: {
		storySort: sortStories(SORT_ORDER)
	}
});
