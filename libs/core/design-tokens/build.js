const StyleDictionaryPackage = require('style-dictionary');
const { format } = require('date-fns');
const { sv } = require('date-fns/locale');

// HAVE THE STYLE DICTIONARY CONFIG DYNAMICALLY GENERATED

const BASE_PX = 16;

const baseBuildPath = '../package/src/design-tokens';

function getStyleDictionaryConfig(platform) {
	return {
		source: ['src/styles/**/*.json'],
		platforms: {
			json: {
				transformGroup: 'tokens-json',
				buildPath: `${baseBuildPath}/web/`,
				prefix: '--digi',
				files: [
					{
						destination: 'tokens.json',
						format: 'json/flat'
					}
				]
			},
			css: {
				transformGroup: 'css',
				buildPath: `${baseBuildPath}/css/`,
				prefix: '--digi',
				files: [
					{
						destination: 'variables.css',
						format: 'css/variables',
						options: {
							outputReferences: true,
              fileHeader: 'myCustomHeader'
						}
					}
				]
			},
			scss: {
				transformGroup: 'scss',
				buildPath: `${baseBuildPath}/scss/`,
				prefix: '--digi',
				files: [
					{
						destination: 'variables.scss',
						format: 'scss/variables',
            options: {
              fileHeader: 'myCustomHeaderSCSS'
            }
					}
				]
			},
			styleguide: {
				transformGroup: 'styleguide',
				buildPath: `${baseBuildPath}/styleguide/`,
				prefix: '--digi',
				files: [
					{
						destination: 'web.json',
						format: 'json/flat'
					},
					{
						destination: 'web.scss',
						format: 'scss/variables',
            options: {
              fileHeader: 'myCustomHeaderSCSS'
            }
					}
				]
			},
			'web/js': {
				transformGroup: 'tokens-js',
				buildPath: `${baseBuildPath}/web/`,
				prefix: 'digi',
				files: [
					// uncomment these ones if you need to generate more formats
					// {
					//     destination: "tokens.module.js",
					//     format: "javascript/module"
					// },
					// {
					//     destination: "tokens.object.js",
					//     format: "javascript/object"
					// },
					{
						destination: 'tokens.es6.js',
						format: 'javascript/es6',
            options: {
              fileHeader: 'myCustomHeaderSCSS'
            }
					}
				]
			},
			'qlikSense/json': {
				transformGroup: 'qlikSense-json',
				buildPath: `${baseBuildPath}/web/`,
				prefix: '--digi',
				files: [
					{
						destination: 'qlikSense-variables.json',
						format: 'qlikSense/json'
					}
				]
			},
			'sketch/txt': {
				transformGroup: 'sketch-txt',
				buildPath: `${baseBuildPath}/sketch/`,
				files: [
					{
						destination: 'sketch-colors.txt',
						format: 'sketch/txt',
						filter: function (token) {
							return token.filePath.includes('src/styles/color/color.global');
						}
					}
				]
			}
		}
	};
}

function getStyleDictionaryPrintConfig(platform) {
	return {
		source: ['src/print/*.json', 'src/styles/**/*.json'],
		platforms: {
			print: {
				transformGroup: 'print',
				buildPath: `${baseBuildPath}/print/`,
				files: [
					{
						destination: 'print.json',
						format: 'json/flat'
					}
				],
				source: ['src/print/**/*.json']
			}
		}
	};
}

// REGISTER CUSTOM FORMATS + TEMPLATES + TRANSFORMS + TRANSFORM GROUPS
// if you want to see the available pre-defined formats, transforms and transform groups uncomment this
// console.log(StyleDictionaryPackage);

// ----- Formats
StyleDictionaryPackage.registerFormat({
	name: 'json/flat',
	formatter: function (dictionary) {
		return JSON.stringify(dictionary.allProperties, null, 2);
	}
});

StyleDictionaryPackage.registerFormat({
	name: 'qlikSense/json',
	formatter: function ({ dictionary, platform, options, file }) {
		const qsVars = {};
		dictionary.allProperties.forEach(
			(token) => (qsVars[token.name] = token.value)
		);
		return JSON.stringify(qsVars, null, 1);
	}
});

StyleDictionaryPackage.registerFormat({
	name: 'sketch/txt',
	formatter: function ({ dictionary, platform, options, file }) {
		let sketchVars = '';
		dictionary.allProperties.forEach(
			(token) => (sketchVars += `${token.name}: ${token.value}\n`)
		);
		return sketchVars;
	}
});

// ----- Transforms
StyleDictionaryPackage.registerTransform({
	name: 'size/pxToPt',
	type: 'value',
	matcher: function (prop) {
		return prop.value.match(/^[\d.]+px$/);
	},
	transformer: function (prop) {
		return prop.value.replace(/px$/, 'pt');
	}
});

StyleDictionaryPackage.registerTransform({
	name: 'size/pxToRem',
	type: 'value',
	matcher: function (prop) {
		return prop.value.match(/^[\d.]+px$/);
	},
	transformer: function (prop) {
		const pxNumber = Number.parseInt(prop.value.replace(/px$/, ''));
		return `${pxNumber / BASE_PX}rem`;
	}
});

StyleDictionaryPackage.registerTransform({
	name: 'name/digiName/css',
	type: 'name',
	matcher: function (token) {
		return token.filePath.includes('src/styles/');
	},
	transformer: function (token) {
		let digiName = 'digi--' + token.path.join('--');
		return digiName;
	}
});

StyleDictionaryPackage.registerTransform({
	name: 'name/digiName/json',
	type: 'name',
	matcher: function (token) {
		return token.filePath.includes('src/styles/');
	},
	transformer: function (token) {
		let digiName = token.path.join('.');
		return digiName;
	}
});

StyleDictionaryPackage.registerTransform({
	name: 'name/digiName/js',
	type: 'name',
	matcher: function (token) {
		return token.filePath.includes('src/styles/');
	},
	transformer: function (token) {
		let digiName = token.path.join('_').replace(/-/g, '_').toUpperCase();
		return digiName;
	}
});

StyleDictionaryPackage.registerTransform({
	name: 'name/qlikSense/json',
	type: 'name',
	matcher: function (token) {
		return token.filePath.includes('src/styles/');
	},
	transformer: function (token) {
		let qsName = `@${token.path.join('-')}`;
		return qsName;
	}
});

StyleDictionaryPackage.registerTransform({
	name: 'name/sketch/txt',
	type: 'name',
	matcher: function (token) {
		return token.filePath.includes('src/styles');
	},
	transformer: function (token) {
		let sketchName = `${token.path.join('/').replace('global/color/', '')}`;
		return sketchName;
	}
});

// ----- TransformGroups
StyleDictionaryPackage.registerTransformGroup({
	name: 'styleguide',
	transforms: ['name/digiName/css', 'time/seconds', 'color/css', 'size/pxToRem']
});

StyleDictionaryPackage.registerTransformGroup({
	name: 'tokens-js',
	transforms: ['name/digiName/js', 'size/px', 'color/hex']
});

StyleDictionaryPackage.registerTransformGroup({
	name: 'styleguide',
	transforms: [
		'attribute/cti',
		'name/digiName/css',
		'time/seconds',
		'color/css',
		'size/pxToRem'
	]
});

StyleDictionaryPackage.registerTransformGroup({
	name: 'tokens-json',
	transforms: ['name/digiName/json', 'time/seconds', 'color/css', 'size/pxToRem']
});

StyleDictionaryPackage.registerTransformGroup({
	name: 'qlikSense-json',
	transforms: [
		'name/qlikSense/json',
		'time/seconds',
		'color/css',
		'size/pxToRem'
	]
});

StyleDictionaryPackage.registerTransformGroup({
	name: 'sketch-txt',
	transforms: ['name/sketch/txt', 'time/seconds', 'color/css', 'size/pxToRem']
});

StyleDictionaryPackage.registerTransformGroup({
	name: 'css',
	// to see the pre-defined "css" transformation use: console.log(StyleDictionaryPackage.transformGroup['css']);
	transforms: ['name/digiName/css', 'time/seconds', 'color/css', 'size/pxToRem']
});

StyleDictionaryPackage.registerTransformGroup({
	name: 'scss',
	// to see the pre-defined "scss" transformation use: console.log(StyleDictionaryPackage.transformGroup['scss']);
	transforms: ['name/digiName/css', 'time/seconds', 'color/css', 'size/pxToRem']
});

StyleDictionaryPackage.registerTransformGroup({
	name: 'print',
	transforms: ['attribute/cti', 'name/digiName/css', 'time/seconds', 'color/css']
});

StyleDictionaryPackage.registerFileHeader({
  name: 'myCustomHeader',
  fileHeader: function() {
    // console.log(defaultMessage);
    const date = format(new Date(), 'yyyy-MM-dd', { locale: sv});
    return [
      `Do not edit directly
 * Autogenerated`
    ];
  }
});

StyleDictionaryPackage.registerFileHeader({
  name: 'myCustomHeaderSCSS',
  fileHeader: function() {
    // console.log(defaultMessage);
    const date = format(new Date(), 'yyyy-MM-dd', { locale: sv});
    return [
      `Do not edit directly
// Autogenerated`
    ];
  }
});

console.log('Build started...');

console.log('\n==============================================');

const StyleDictionary = StyleDictionaryPackage.extend(
	getStyleDictionaryConfig()
);

const StyleDictionaryPrint = StyleDictionaryPackage.extend(
	getStyleDictionaryPrintConfig()
);

StyleDictionary.buildPlatform('json');
StyleDictionary.buildPlatform('web/js');
StyleDictionary.buildPlatform('css');
StyleDictionary.buildPlatform('scss');
StyleDictionary.buildPlatform('styleguide');
StyleDictionary.buildPlatform('qlikSense/json');
StyleDictionary.buildPlatform('sketch/txt');

StyleDictionaryPrint.buildPlatform('print');

console.log('\nEnd processing');

console.log('\n==============================================');
console.log('\nBuild completed!');
