import { _t as sharedText } from '@digi/shared/text';

const notification = {
	cookie_banner_text:
		'Vi använder kakor (cookies) för att webbplatsen ska fungera på ett så bra sätt som möjligt. Här kan du välja vilka cookies du vill godkänna.',
	cookie_banner_heading: 'Vi använder kakor',
	cookie_modal_heading: 'Anpassa inställningar för kakor (cookies)',
	cookie_required:
		'Nödvändiga kakor gör att våra tjänster är säkra och fungerar som de ska. Därför går de inte att inaktivera.',
	cookie_accept_all: 'Godkänn alla kakor',
	cookie_save_and_approve: 'Spara och godkänn'
};

export const _t = {
	...sharedText,
	notification
};
