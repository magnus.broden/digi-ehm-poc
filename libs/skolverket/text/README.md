# skolverket-text

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build skolverket-text` to build the library.

## Running unit tests

Run `nx test skolverket-text` to execute the unit tests via [Jest](https://jestjs.io).
