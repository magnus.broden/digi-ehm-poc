/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';
import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import type { Components } from '@digi/skolverket/components';

import { defineCustomElement as defineDigiBarChart } from '@digi/skolverket/components/digi-bar-chart.js';
import { defineCustomElement as defineDigiButton } from '@digi/skolverket/components/digi-button.js';
import { defineCustomElement as defineDigiCalendar } from '@digi/skolverket/components/digi-calendar.js';
import { defineCustomElement as defineDigiCalendarWeekView } from '@digi/skolverket/components/digi-calendar-week-view.js';
import { defineCustomElement as defineDigiCardBox } from '@digi/skolverket/components/digi-card-box.js';
import { defineCustomElement as defineDigiCardLink } from '@digi/skolverket/components/digi-card-link.js';
import { defineCustomElement as defineDigiChartLine } from '@digi/skolverket/components/digi-chart-line.js';
import { defineCustomElement as defineDigiCode } from '@digi/skolverket/components/digi-code.js';
import { defineCustomElement as defineDigiCodeBlock } from '@digi/skolverket/components/digi-code-block.js';
import { defineCustomElement as defineDigiCodeExample } from '@digi/skolverket/components/digi-code-example.js';
import { defineCustomElement as defineDigiDialog } from '@digi/skolverket/components/digi-dialog.js';
import { defineCustomElement as defineDigiExpandableAccordion } from '@digi/skolverket/components/digi-expandable-accordion.js';
import { defineCustomElement as defineDigiFormCheckbox } from '@digi/skolverket/components/digi-form-checkbox.js';
import { defineCustomElement as defineDigiFormFieldset } from '@digi/skolverket/components/digi-form-fieldset.js';
import { defineCustomElement as defineDigiFormFileUpload } from '@digi/skolverket/components/digi-form-file-upload.js';
import { defineCustomElement as defineDigiFormFilter } from '@digi/skolverket/components/digi-form-filter.js';
import { defineCustomElement as defineDigiFormInput } from '@digi/skolverket/components/digi-form-input.js';
import { defineCustomElement as defineDigiFormInputSearch } from '@digi/skolverket/components/digi-form-input-search.js';
import { defineCustomElement as defineDigiFormLabel } from '@digi/skolverket/components/digi-form-label.js';
import { defineCustomElement as defineDigiFormProcessStep } from '@digi/skolverket/components/digi-form-process-step.js';
import { defineCustomElement as defineDigiFormProcessSteps } from '@digi/skolverket/components/digi-form-process-steps.js';
import { defineCustomElement as defineDigiFormRadiobutton } from '@digi/skolverket/components/digi-form-radiobutton.js';
import { defineCustomElement as defineDigiFormRadiogroup } from '@digi/skolverket/components/digi-form-radiogroup.js';
import { defineCustomElement as defineDigiFormSelect } from '@digi/skolverket/components/digi-form-select.js';
import { defineCustomElement as defineDigiFormTextarea } from '@digi/skolverket/components/digi-form-textarea.js';
import { defineCustomElement as defineDigiFormValidationMessage } from '@digi/skolverket/components/digi-form-validation-message.js';
import { defineCustomElement as defineDigiIcon } from '@digi/skolverket/components/digi-icon.js';
import { defineCustomElement as defineDigiIconBars } from '@digi/skolverket/components/digi-icon-bars.js';
import { defineCustomElement as defineDigiIconCheck } from '@digi/skolverket/components/digi-icon-check.js';
import { defineCustomElement as defineDigiIconCheckCircleRegAlt } from '@digi/skolverket/components/digi-icon-check-circle-reg-alt.js';
import { defineCustomElement as defineDigiIconChevronDown } from '@digi/skolverket/components/digi-icon-chevron-down.js';
import { defineCustomElement as defineDigiIconChevronLeft } from '@digi/skolverket/components/digi-icon-chevron-left.js';
import { defineCustomElement as defineDigiIconChevronRight } from '@digi/skolverket/components/digi-icon-chevron-right.js';
import { defineCustomElement as defineDigiIconChevronUp } from '@digi/skolverket/components/digi-icon-chevron-up.js';
import { defineCustomElement as defineDigiIconCopy } from '@digi/skolverket/components/digi-icon-copy.js';
import { defineCustomElement as defineDigiIconDangerOutline } from '@digi/skolverket/components/digi-icon-danger-outline.js';
import { defineCustomElement as defineDigiIconDownload } from '@digi/skolverket/components/digi-icon-download.js';
import { defineCustomElement as defineDigiIconExclamationCircleFilled } from '@digi/skolverket/components/digi-icon-exclamation-circle-filled.js';
import { defineCustomElement as defineDigiIconExclamationTriangle } from '@digi/skolverket/components/digi-icon-exclamation-triangle.js';
import { defineCustomElement as defineDigiIconExclamationTriangleWarning } from '@digi/skolverket/components/digi-icon-exclamation-triangle-warning.js';
import { defineCustomElement as defineDigiIconExternalLinkAlt } from '@digi/skolverket/components/digi-icon-external-link-alt.js';
import { defineCustomElement as defineDigiIconMinus } from '@digi/skolverket/components/digi-icon-minus.js';
import { defineCustomElement as defineDigiIconNotificationSucces } from '@digi/skolverket/components/digi-icon-notification-succes.js';
import { defineCustomElement as defineDigiIconNotificationWarning } from '@digi/skolverket/components/digi-icon-notification-warning.js';
import { defineCustomElement as defineDigiIconPaperclip } from '@digi/skolverket/components/digi-icon-paperclip.js';
import { defineCustomElement as defineDigiIconPlus } from '@digi/skolverket/components/digi-icon-plus.js';
import { defineCustomElement as defineDigiIconSearch } from '@digi/skolverket/components/digi-icon-search.js';
import { defineCustomElement as defineDigiIconSpinner } from '@digi/skolverket/components/digi-icon-spinner.js';
import { defineCustomElement as defineDigiIconTrash } from '@digi/skolverket/components/digi-icon-trash.js';
import { defineCustomElement as defineDigiIconValidationSuccess } from '@digi/skolverket/components/digi-icon-validation-success.js';
import { defineCustomElement as defineDigiIconX } from '@digi/skolverket/components/digi-icon-x.js';
import { defineCustomElement as defineDigiLayoutBlock } from '@digi/skolverket/components/digi-layout-block.js';
import { defineCustomElement as defineDigiLayoutColumns } from '@digi/skolverket/components/digi-layout-columns.js';
import { defineCustomElement as defineDigiLayoutContainer } from '@digi/skolverket/components/digi-layout-container.js';
import { defineCustomElement as defineDigiLayoutGrid } from '@digi/skolverket/components/digi-layout-grid.js';
import { defineCustomElement as defineDigiLayoutMediaObject } from '@digi/skolverket/components/digi-layout-media-object.js';
import { defineCustomElement as defineDigiLayoutRows } from '@digi/skolverket/components/digi-layout-rows.js';
import { defineCustomElement as defineDigiLayoutStackedBlocks } from '@digi/skolverket/components/digi-layout-stacked-blocks.js';
import { defineCustomElement as defineDigiLink } from '@digi/skolverket/components/digi-link.js';
import { defineCustomElement as defineDigiLinkExternal } from '@digi/skolverket/components/digi-link-external.js';
import { defineCustomElement as defineDigiLinkIcon } from '@digi/skolverket/components/digi-link-icon.js';
import { defineCustomElement as defineDigiLinkInternal } from '@digi/skolverket/components/digi-link-internal.js';
import { defineCustomElement as defineDigiListLink } from '@digi/skolverket/components/digi-list-link.js';
import { defineCustomElement as defineDigiLoaderSpinner } from '@digi/skolverket/components/digi-loader-spinner.js';
import { defineCustomElement as defineDigiLogo } from '@digi/skolverket/components/digi-logo.js';
import { defineCustomElement as defineDigiLogoService } from '@digi/skolverket/components/digi-logo-service.js';
import { defineCustomElement as defineDigiLogoSister } from '@digi/skolverket/components/digi-logo-sister.js';
import { defineCustomElement as defineDigiMediaFigure } from '@digi/skolverket/components/digi-media-figure.js';
import { defineCustomElement as defineDigiMediaImage } from '@digi/skolverket/components/digi-media-image.js';
import { defineCustomElement as defineDigiNavigationBreadcrumbs } from '@digi/skolverket/components/digi-navigation-breadcrumbs.js';
import { defineCustomElement as defineDigiNavigationContextMenu } from '@digi/skolverket/components/digi-navigation-context-menu.js';
import { defineCustomElement as defineDigiNavigationContextMenuItem } from '@digi/skolverket/components/digi-navigation-context-menu-item.js';
import { defineCustomElement as defineDigiNavigationMainMenu } from '@digi/skolverket/components/digi-navigation-main-menu.js';
import { defineCustomElement as defineDigiNavigationMainMenuPanel } from '@digi/skolverket/components/digi-navigation-main-menu-panel.js';
import { defineCustomElement as defineDigiNavigationPagination } from '@digi/skolverket/components/digi-navigation-pagination.js';
import { defineCustomElement as defineDigiNavigationSidebar } from '@digi/skolverket/components/digi-navigation-sidebar.js';
import { defineCustomElement as defineDigiNavigationSidebarButton } from '@digi/skolverket/components/digi-navigation-sidebar-button.js';
import { defineCustomElement as defineDigiNavigationTab } from '@digi/skolverket/components/digi-navigation-tab.js';
import { defineCustomElement as defineDigiNavigationTabInABox } from '@digi/skolverket/components/digi-navigation-tab-in-a-box.js';
import { defineCustomElement as defineDigiNavigationTabs } from '@digi/skolverket/components/digi-navigation-tabs.js';
import { defineCustomElement as defineDigiNavigationToc } from '@digi/skolverket/components/digi-navigation-toc.js';
import { defineCustomElement as defineDigiNavigationVerticalMenu } from '@digi/skolverket/components/digi-navigation-vertical-menu.js';
import { defineCustomElement as defineDigiNavigationVerticalMenuItem } from '@digi/skolverket/components/digi-navigation-vertical-menu-item.js';
import { defineCustomElement as defineDigiNotificationAlert } from '@digi/skolverket/components/digi-notification-alert.js';
import { defineCustomElement as defineDigiNotificationCookie } from '@digi/skolverket/components/digi-notification-cookie.js';
import { defineCustomElement as defineDigiNotificationDetail } from '@digi/skolverket/components/digi-notification-detail.js';
import { defineCustomElement as defineDigiPage } from '@digi/skolverket/components/digi-page.js';
import { defineCustomElement as defineDigiPageBlockCards } from '@digi/skolverket/components/digi-page-block-cards.js';
import { defineCustomElement as defineDigiPageBlockHero } from '@digi/skolverket/components/digi-page-block-hero.js';
import { defineCustomElement as defineDigiPageBlockLists } from '@digi/skolverket/components/digi-page-block-lists.js';
import { defineCustomElement as defineDigiPageBlockSidebar } from '@digi/skolverket/components/digi-page-block-sidebar.js';
import { defineCustomElement as defineDigiPageFooter } from '@digi/skolverket/components/digi-page-footer.js';
import { defineCustomElement as defineDigiPageHeader } from '@digi/skolverket/components/digi-page-header.js';
import { defineCustomElement as defineDigiProgressStep } from '@digi/skolverket/components/digi-progress-step.js';
import { defineCustomElement as defineDigiProgressSteps } from '@digi/skolverket/components/digi-progress-steps.js';
import { defineCustomElement as defineDigiProgressbar } from '@digi/skolverket/components/digi-progressbar.js';
import { defineCustomElement as defineDigiTable } from '@digi/skolverket/components/digi-table.js';
import { defineCustomElement as defineDigiTag } from '@digi/skolverket/components/digi-tag.js';
import { defineCustomElement as defineDigiTypography } from '@digi/skolverket/components/digi-typography.js';
import { defineCustomElement as defineDigiTypographyHeadingSection } from '@digi/skolverket/components/digi-typography-heading-section.js';
import { defineCustomElement as defineDigiTypographyMeta } from '@digi/skolverket/components/digi-typography-meta.js';
import { defineCustomElement as defineDigiTypographyPreamble } from '@digi/skolverket/components/digi-typography-preamble.js';
import { defineCustomElement as defineDigiTypographyTime } from '@digi/skolverket/components/digi-typography-time.js';
import { defineCustomElement as defineDigiUtilBreakpointObserver } from '@digi/skolverket/components/digi-util-breakpoint-observer.js';
import { defineCustomElement as defineDigiUtilDetectClickOutside } from '@digi/skolverket/components/digi-util-detect-click-outside.js';
import { defineCustomElement as defineDigiUtilDetectFocusOutside } from '@digi/skolverket/components/digi-util-detect-focus-outside.js';
import { defineCustomElement as defineDigiUtilIntersectionObserver } from '@digi/skolverket/components/digi-util-intersection-observer.js';
import { defineCustomElement as defineDigiUtilKeydownHandler } from '@digi/skolverket/components/digi-util-keydown-handler.js';
import { defineCustomElement as defineDigiUtilKeyupHandler } from '@digi/skolverket/components/digi-util-keyup-handler.js';
import { defineCustomElement as defineDigiUtilMutationObserver } from '@digi/skolverket/components/digi-util-mutation-observer.js';
import { defineCustomElement as defineDigiUtilResizeObserver } from '@digi/skolverket/components/digi-util-resize-observer.js';


export declare interface DigiBarChart extends Components.DigiBarChart {}

@ProxyCmp({
  defineCustomElementFn: defineDigiBarChart,
  inputs: ['afChartData', 'afHeadingLevel', 'afId']
})
@Component({
  selector: 'digi-bar-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afChartData', 'afHeadingLevel', 'afId']
})
export class DigiBarChart {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiButton extends Components.DigiButton {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Buttonelementets 'onfocus'-event. @en The button element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * Buttonelementets 'onblur'-event. @en The button element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiButton,
  inputs: ['afAriaChecked', 'afAriaControls', 'afAriaCurrent', 'afAriaExpanded', 'afAriaHaspopup', 'afAriaLabel', 'afAriaLabelledby', 'afAriaPressed', 'afAutofocus', 'afDir', 'afForm', 'afFullWidth', 'afId', 'afLang', 'afRole', 'afSize', 'afTabindex', 'afType', 'afVariation'],
  methods: ['afMGetButtonElement']
})
@Component({
  selector: 'digi-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaChecked', 'afAriaControls', 'afAriaCurrent', 'afAriaExpanded', 'afAriaHaspopup', 'afAriaLabel', 'afAriaLabelledby', 'afAriaPressed', 'afAutofocus', 'afDir', 'afForm', 'afFullWidth', 'afId', 'afLang', 'afRole', 'afSize', 'afTabindex', 'afType', 'afVariation']
})
export class DigiButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick', 'afOnFocus', 'afOnBlur', 'afOnReady']);
  }
}


export declare interface DigiCalendar extends Components.DigiCalendar {
  /**
   * Sker när ett datum har valts eller avvalts. Returnerar datumen i en array. @en When a date is selected. Return the dates in an array.
   */
  afOnDateSelectedChange: EventEmitter<CustomEvent<any>>;
  /**
   * Sker när fokus flyttas utanför kalendern @en When focus moves out from the calendar
   */
  afOnFocusOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid klick utanför kalendern @en When click outside the calendar
   */
  afOnClickOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Sker när kalenderdatumen har rörts första gången, när värdet på focusedDate har ändrats första gången. @en When the calendar dates are touched the first time, when focusedDate is changed the first time.
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiCalendar,
  inputs: ['afActive', 'afId', 'afInitSelectedDate', 'afInitSelectedMonth', 'afMaxDate', 'afMinDate', 'afMultipleDates', 'afSelectedDate', 'afSelectedDates', 'afShowWeekNumber']
})
@Component({
  selector: 'digi-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afId', 'afInitSelectedDate', 'afInitSelectedMonth', 'afMaxDate', 'afMinDate', 'afMultipleDates', 'afSelectedDate', 'afSelectedDates', 'afShowWeekNumber']
})
export class DigiCalendar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnDateSelectedChange', 'afOnFocusOutside', 'afOnClickOutside', 'afOnDirty']);
  }
}


export declare interface DigiCalendarWeekView extends Components.DigiCalendarWeekView {
  /**
   * Vid byte av vecka @en When week changes
   */
  afOnWeekChange: EventEmitter<CustomEvent<string>>;
  /**
   * Vid byte av dag @en When day changes
   */
  afOnDateChange: EventEmitter<CustomEvent<string>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiCalendarWeekView,
  inputs: ['afDates', 'afHeadingLevel', 'afId', 'afMaxWeek', 'afMinWeek'],
  methods: ['afMSetActiveDate']
})
@Component({
  selector: 'digi-calendar-week-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDates', 'afHeadingLevel', 'afId', 'afMaxWeek', 'afMinWeek']
})
export class DigiCalendarWeekView {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnWeekChange', 'afOnDateChange', 'afOnReady']);
  }
}


export declare interface DigiCardBox extends Components.DigiCardBox {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCardBox,
  inputs: ['afGutter', 'afWidth']
})
@Component({
  selector: 'digi-card-box',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afGutter', 'afWidth']
})
export class DigiCardBox {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCardLink extends Components.DigiCardLink {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCardLink
})
@Component({
  selector: 'digi-card-link',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiCardLink {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiChartLine extends Components.DigiChartLine {}

@ProxyCmp({
  defineCustomElementFn: defineDigiChartLine,
  inputs: ['afChartData', 'afHeadingLevel', 'afId']
})
@Component({
  selector: 'digi-chart-line',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afChartData', 'afHeadingLevel', 'afId']
})
export class DigiChartLine {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCode extends Components.DigiCode {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCode,
  inputs: ['afCode', 'afLang', 'afLanguage', 'afVariation']
})
@Component({
  selector: 'digi-code',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCode', 'afLang', 'afLanguage', 'afVariation']
})
export class DigiCode {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCodeBlock extends Components.DigiCodeBlock {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCodeBlock,
  inputs: ['afCode', 'afHideToolbar', 'afLang', 'afLanguage', 'afVariation']
})
@Component({
  selector: 'digi-code-block',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCode', 'afHideToolbar', 'afLang', 'afLanguage', 'afVariation']
})
export class DigiCodeBlock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCodeExample extends Components.DigiCodeExample {}

@ProxyCmp({
  defineCustomElementFn: defineDigiCodeExample,
  inputs: ['afCode', 'afCodeBlockVariation', 'afExampleVariation', 'afHideCode', 'afHideControls', 'afLanguage']
})
@Component({
  selector: 'digi-code-example',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCode', 'afCodeBlockVariation', 'afExampleVariation', 'afHideCode', 'afHideControls', 'afLanguage']
})
export class DigiCodeExample {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiDialog extends Components.DigiDialog {
  /**
   *  
   */
  afOnOpen: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnClose: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiDialog,
  inputs: ['afHideCloseButton', 'afOpen'],
  methods: ['showModal', 'close']
})
@Component({
  selector: 'digi-dialog',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHideCloseButton', 'afOpen']
})
export class DigiDialog {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnOpen', 'afOnClose']);
  }
}


export declare interface DigiExpandableAccordion extends Components.DigiExpandableAccordion {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiExpandableAccordion,
  inputs: ['afAnimation', 'afExpanded', 'afHeading', 'afHeadingLevel', 'afId', 'afVariation']
})
@Component({
  selector: 'digi-expandable-accordion',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnimation', 'afExpanded', 'afHeading', 'afHeadingLevel', 'afId', 'afVariation']
})
export class DigiExpandableAccordion {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiFormCheckbox extends Components.DigiFormCheckbox {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormCheckbox,
  inputs: ['afAriaDescribedby', 'afAriaLabel', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afDescription', 'afId', 'afIndeterminate', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-checkbox',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaDescribedby', 'afAriaLabel', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afDescription', 'afId', 'afIndeterminate', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value']
})
export class DigiFormCheckbox {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormFieldset extends Components.DigiFormFieldset {}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormFieldset,
  inputs: ['afForm', 'afId', 'afLegend', 'afName']
})
@Component({
  selector: 'digi-form-fieldset',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afForm', 'afId', 'afLegend', 'afName']
})
export class DigiFormFieldset {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormFileUpload extends Components.DigiFormFileUpload {
  /**
   * Sänder ut fil vid uppladdning @en Emits file on upload
   */
  afOnUploadFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som tagits bort @en Emits which file that was deleted.
   */
  afOnRemoveFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som har avbrutis uppladdning @en Emits which file that was canceled
   */
  afOnCancelFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som försöker laddas upp igen @en Emits which file is trying to retry its upload
   */
  afOnRetryFile: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormFileUpload,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afFileMaxSize', 'afFileTypes', 'afHeadingFiles', 'afHeadingLevel', 'afId', 'afLabel', 'afLabelDescription', 'afMaxFiles', 'afName', 'afRequired', 'afRequiredText', 'afUploadBtnText'],
  methods: ['afMGetAllFiles', 'afMImportFiles', 'afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-file-upload',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afFileMaxSize', 'afFileTypes', 'afHeadingFiles', 'afHeadingLevel', 'afId', 'afLabel', 'afLabelDescription', 'afMaxFiles', 'afName', 'afRequired', 'afRequiredText', 'afUploadBtnText']
})
export class DigiFormFileUpload {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnUploadFile', 'afOnRemoveFile', 'afOnCancelFile', 'afOnRetryFile']);
  }
}


export declare interface DigiFormFilter extends Components.DigiFormFilter {
  /**
   *  
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Vid uppdatering av filtret @en At filter submit
   */
  afOnSubmitFilters: EventEmitter<CustomEvent<any>>;
  /**
   * När filtret stängs utan att valda alternativ bekräftats @en When the filter is closed without confirming the selected options
   */
  afOnFilterClosed: EventEmitter<CustomEvent<any>>;
  /**
   * Vid reset av filtret @en At filter reset
   */
  afOnResetFilters: EventEmitter<CustomEvent<any>>;
  /**
   * När ett filter ändras @en When a filter is changed
   */
  afOnChange: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormFilter,
  inputs: ['afAlignRight', 'afAutofocus', 'afFilterButtonAriaDescribedby', 'afFilterButtonAriaLabel', 'afFilterButtonText', 'afHideResetButton', 'afId', 'afName', 'afResetButtonText', 'afResetButtonTextAriaLabel', 'afSubmitButtonText', 'afSubmitButtonTextAriaLabel']
})
@Component({
  selector: 'digi-form-filter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlignRight', 'afAutofocus', 'afFilterButtonAriaDescribedby', 'afFilterButtonAriaLabel', 'afFilterButtonText', 'afHideResetButton', 'afId', 'afName', 'afResetButtonText', 'afResetButtonTextAriaLabel', 'afSubmitButtonText', 'afSubmitButtonTextAriaLabel']
})
export class DigiFormFilter {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusout', 'afOnSubmitFilters', 'afOnFilterClosed', 'afOnResetFilters', 'afOnChange']);
  }
}


export declare interface DigiFormInput extends Components.DigiFormInput {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onkeyup'-event. @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormInput,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonVariation', 'afDirname', 'afId', 'afInputmode', 'afLabel', 'afLabelDescription', 'afList', 'afMax', 'afMaxlength', 'afMin', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afStep', 'afType', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-input',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonVariation', 'afDirname', 'afId', 'afInputmode', 'afLabel', 'afLabelDescription', 'afList', 'afMax', 'afMaxlength', 'afMin', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afStep', 'afType', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value']
})
export class DigiFormInput {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormInputSearch extends Components.DigiFormInputSearch {
  /**
   * Vid fokus utanför komponenten. @en When focus is outside component.
   */
  afOnFocusOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid fokus inuti komponenten. @en When focus is inside component.
   */
  afOnFocusInside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid klick utanför komponenten. @en When click outside component.
   */
  afOnClickOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid klick inuti komponenten. @en When click inside component.
   */
  afOnClickInside: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onkeyup'-event. @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Knappelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormInputSearch,
  inputs: ['afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonAriaLabel', 'afButtonAriaLabelledby', 'afButtonText', 'afButtonType', 'afButtonVariation', 'afHideButton', 'afId', 'afLabel', 'afLabelDescription', 'afName', 'afType', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-input-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonAriaLabel', 'afButtonAriaLabelledby', 'afButtonText', 'afButtonType', 'afButtonVariation', 'afHideButton', 'afId', 'afLabel', 'afLabelDescription', 'afName', 'afType', 'afValue', 'afVariation', 'value']
})
export class DigiFormInputSearch {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusOutside', 'afOnFocusInside', 'afOnClickOutside', 'afOnClickInside', 'afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnClick', 'afOnReady']);
  }
}


export declare interface DigiFormLabel extends Components.DigiFormLabel {}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormLabel,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afDescription', 'afFor', 'afId', 'afLabel', 'afRequired', 'afRequiredText']
})
@Component({
  selector: 'digi-form-label',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afDescription', 'afFor', 'afId', 'afLabel', 'afRequired', 'afRequiredText']
})
export class DigiFormLabel {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormProcessStep extends Components.DigiFormProcessStep {
  /**
   *  
   */
  afClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormProcessStep,
  inputs: ['afContext', 'afHref', 'afLabel', 'afType']
})
@Component({
  selector: 'digi-form-process-step',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afContext', 'afHref', 'afLabel', 'afType']
})
export class DigiFormProcessStep {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afClick']);
  }
}


export declare interface DigiFormProcessSteps extends Components.DigiFormProcessSteps {}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormProcessSteps,
  inputs: ['afCurrentStep', 'afId']
})
@Component({
  selector: 'digi-form-process-steps',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCurrentStep', 'afId']
})
export class DigiFormProcessSteps {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormRadiobutton extends Components.DigiFormRadiobutton {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormRadiobutton,
  inputs: ['afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afId', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-radiobutton',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afId', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value']
})
export class DigiFormRadiobutton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormRadiogroup extends Components.DigiFormRadiogroup {
  /**
   * Event när värdet ändras @en Event when value changes
   */
  afOnGroupChange: EventEmitter<CustomEvent<string>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormRadiogroup,
  inputs: ['afName', 'afValue', 'value']
})
@Component({
  selector: 'digi-form-radiogroup',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afName', 'afValue', 'value']
})
export class DigiFormRadiogroup {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnGroupChange']);
  }
}


export declare interface DigiFormSelect extends Components.DigiFormSelect {
  /**
   *  
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnSelect: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Sker första gången väljarelementet ändras. @en First time the select element is changed.
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormSelect,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAutofocus', 'afDescription', 'afDisableValidation', 'afId', 'afLabel', 'afName', 'afPlaceholder', 'afRequired', 'afRequiredText', 'afStartSelected', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-select',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAutofocus', 'afDescription', 'afDisableValidation', 'afId', 'afLabel', 'afName', 'afPlaceholder', 'afRequired', 'afRequiredText', 'afStartSelected', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value']
})
export class DigiFormSelect {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnSelect', 'afOnFocus', 'afOnFocusout', 'afOnBlur', 'afOnDirty', 'afOnReady']);
  }
}


export declare interface DigiFormTextarea extends Components.DigiFormTextarea {
  /**
   * Textareatelementets 'onchange'-event. @en The textarea element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onblur'-event. @en The textarea element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onkeyup'-event. @en The textarea element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onfocus'-event. @en The textarea element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onfocusout'-event. @en The textarea element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'oninput'-event. @en The textarea element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid textareaelementets första 'oninput' @en First time the textarea element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid textareaelementets första 'onblur' @en First time the textelement is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormTextarea,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afId', 'afLabel', 'afLabelDescription', 'afMaxlength', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-textarea',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afId', 'afLabel', 'afLabelDescription', 'afMaxlength', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value']
})
export class DigiFormTextarea {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormValidationMessage extends Components.DigiFormValidationMessage {}

@ProxyCmp({
  defineCustomElementFn: defineDigiFormValidationMessage,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-form-validation-message',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiFormValidationMessage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIcon extends Components.DigiIcon {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIcon,
  inputs: ['afDesc', 'afName', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afName', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIcon {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBars extends Components.DigiIconBars {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconBars,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-bars',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconBars {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCheck extends Components.DigiIconCheck {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCheck,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-check',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCheck {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCheckCircleRegAlt extends Components.DigiIconCheckCircleRegAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCheckCircleRegAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-check-circle-reg-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCheckCircleRegAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronDown extends Components.DigiIconChevronDown {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronDown,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-down',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChevronDown {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronLeft extends Components.DigiIconChevronLeft {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronLeft,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-left',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChevronLeft {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronRight extends Components.DigiIconChevronRight {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronRight,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-right',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChevronRight {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronUp extends Components.DigiIconChevronUp {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronUp,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-up',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconChevronUp {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCopy extends Components.DigiIconCopy {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconCopy,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-copy',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconCopy {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconDangerOutline extends Components.DigiIconDangerOutline {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconDangerOutline,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-danger-outline',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconDangerOutline {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconDownload extends Components.DigiIconDownload {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconDownload,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-download',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconDownload {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationCircleFilled extends Components.DigiIconExclamationCircleFilled {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationCircleFilled,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-circle-filled',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExclamationCircleFilled {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationTriangle extends Components.DigiIconExclamationTriangle {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationTriangle,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-triangle',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExclamationTriangle {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationTriangleWarning extends Components.DigiIconExclamationTriangleWarning {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationTriangleWarning,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-triangle-warning',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExclamationTriangleWarning {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExternalLinkAlt extends Components.DigiIconExternalLinkAlt {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconExternalLinkAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-external-link-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconExternalLinkAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMinus extends Components.DigiIconMinus {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconMinus,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-minus',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconMinus {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNotificationSucces extends Components.DigiIconNotificationSucces {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconNotificationSucces,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-notification-succes',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconNotificationSucces {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNotificationWarning extends Components.DigiIconNotificationWarning {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconNotificationWarning,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-notification-warning',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconNotificationWarning {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPaperclip extends Components.DigiIconPaperclip {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconPaperclip,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-paperclip',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconPaperclip {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPlus extends Components.DigiIconPlus {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconPlus,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-plus',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconPlus {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSearch extends Components.DigiIconSearch {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSearch,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSearch {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSpinner extends Components.DigiIconSpinner {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconSpinner,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-spinner',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconSpinner {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconTrash extends Components.DigiIconTrash {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconTrash,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-trash',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconTrash {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconValidationSuccess extends Components.DigiIconValidationSuccess {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconValidationSuccess,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-validation-success',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconValidationSuccess {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconX extends Components.DigiIconX {}

@ProxyCmp({
  defineCustomElementFn: defineDigiIconX,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-x',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
export class DigiIconX {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutBlock extends Components.DigiLayoutBlock {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutBlock,
  inputs: ['afContainer', 'afMarginBottom', 'afMarginTop', 'afVariation', 'afVerticalPadding']
})
@Component({
  selector: 'digi-layout-block',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afContainer', 'afMarginBottom', 'afMarginTop', 'afVariation', 'afVerticalPadding']
})
export class DigiLayoutBlock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutColumns extends Components.DigiLayoutColumns {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutColumns,
  inputs: ['afElement', 'afVariation']
})
@Component({
  selector: 'digi-layout-columns',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afElement', 'afVariation']
})
export class DigiLayoutColumns {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutContainer extends Components.DigiLayoutContainer {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutContainer,
  inputs: ['afMarginBottom', 'afMarginTop', 'afNoGutter', 'afVariation', 'afVerticalPadding']
})
@Component({
  selector: 'digi-layout-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afMarginBottom', 'afMarginTop', 'afNoGutter', 'afVariation', 'afVerticalPadding']
})
export class DigiLayoutContainer {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutGrid extends Components.DigiLayoutGrid {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutGrid,
  inputs: ['afVerticalSpacing']
})
@Component({
  selector: 'digi-layout-grid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVerticalSpacing']
})
export class DigiLayoutGrid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutMediaObject extends Components.DigiLayoutMediaObject {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutMediaObject,
  inputs: ['afAlignment']
})
@Component({
  selector: 'digi-layout-media-object',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlignment']
})
export class DigiLayoutMediaObject {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutRows extends Components.DigiLayoutRows {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutRows
})
@Component({
  selector: 'digi-layout-rows',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiLayoutRows {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutStackedBlocks extends Components.DigiLayoutStackedBlocks {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutStackedBlocks,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-layout-stacked-blocks',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiLayoutStackedBlocks {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLink extends Components.DigiLink {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiLink,
  inputs: ['afHref', 'afLinkContainer', 'afOverrideLink', 'afTarget', 'afVariation'],
  methods: ['afMGetLinkElement']
})
@Component({
  selector: 'digi-link',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHref', 'afLinkContainer', 'afOverrideLink', 'afTarget', 'afVariation']
})
export class DigiLink {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick', 'afOnReady']);
  }
}


export declare interface DigiLinkExternal extends Components.DigiLinkExternal {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiLinkExternal,
  inputs: ['afHref', 'afLinkContainer', 'afOverrideLink', 'afTarget', 'afVariation']
})
@Component({
  selector: 'digi-link-external',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHref', 'afLinkContainer', 'afOverrideLink', 'afTarget', 'afVariation']
})
export class DigiLinkExternal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiLinkIcon extends Components.DigiLinkIcon {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLinkIcon
})
@Component({
  selector: 'digi-link-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiLinkIcon {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLinkInternal extends Components.DigiLinkInternal {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiLinkInternal,
  inputs: ['afHref', 'afLinkContainer', 'afOverrideLink', 'afVariation']
})
@Component({
  selector: 'digi-link-internal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHref', 'afLinkContainer', 'afOverrideLink', 'afVariation']
})
export class DigiLinkInternal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiListLink extends Components.DigiListLink {}

@ProxyCmp({
  defineCustomElementFn: defineDigiListLink,
  inputs: ['afLayout', 'afType', 'afVariation']
})
@Component({
  selector: 'digi-list-link',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afLayout', 'afType', 'afVariation']
})
export class DigiListLink {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLoaderSpinner extends Components.DigiLoaderSpinner {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLoaderSpinner,
  inputs: ['afSize', 'afText']
})
@Component({
  selector: 'digi-loader-spinner',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afSize', 'afText']
})
export class DigiLoaderSpinner {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLogo extends Components.DigiLogo {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLogo,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afTitle', 'afTitleId']
})
@Component({
  selector: 'digi-logo',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDesc', 'afSvgAriaHidden', 'afTitle', 'afTitleId']
})
export class DigiLogo {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLogoService extends Components.DigiLogoService {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLogoService,
  inputs: ['afDescription', 'afName', 'afNameId']
})
@Component({
  selector: 'digi-logo-service',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDescription', 'afName', 'afNameId']
})
export class DigiLogoService {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLogoSister extends Components.DigiLogoSister {}

@ProxyCmp({
  defineCustomElementFn: defineDigiLogoSister,
  inputs: ['afName', 'afNameId']
})
@Component({
  selector: 'digi-logo-sister',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afName', 'afNameId']
})
export class DigiLogoSister {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiMediaFigure extends Components.DigiMediaFigure {}

@ProxyCmp({
  defineCustomElementFn: defineDigiMediaFigure,
  inputs: ['afAlignment', 'afFigcaption']
})
@Component({
  selector: 'digi-media-figure',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlignment', 'afFigcaption']
})
export class DigiMediaFigure {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiMediaImage extends Components.DigiMediaImage {
  /**
   * Bildlementets 'onload'-event. @en The image element's 'onload' event.
   */
  afOnLoad: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiMediaImage,
  inputs: ['afAlt', 'afAriaLabel', 'afFullwidth', 'afHeight', 'afObserverOptions', 'afSrc', 'afSrcset', 'afTitle', 'afUnlazy', 'afWidth']
})
@Component({
  selector: 'digi-media-image',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAlt', 'afAriaLabel', 'afFullwidth', 'afHeight', 'afObserverOptions', 'afSrc', 'afSrcset', 'afTitle', 'afUnlazy', 'afWidth']
})
export class DigiMediaImage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnLoad']);
  }
}


export declare interface DigiNavigationBreadcrumbs extends Components.DigiNavigationBreadcrumbs {}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationBreadcrumbs,
  inputs: ['afAriaLabel', 'afCurrentPage', 'afVariation']
})
@Component({
  selector: 'digi-navigation-breadcrumbs',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaLabel', 'afCurrentPage', 'afVariation']
})
export class DigiNavigationBreadcrumbs {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiNavigationContextMenu extends Components.DigiNavigationContextMenu {
  /**
   * När komponenten stängs @en When component gets inactive
   */
  afOnInactive: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten öppnas @en When component gets active
   */
  afOnActive: EventEmitter<CustomEvent<any>>;
  /**
   * När fokus sätts utanför komponenten @en When focus is move outside of component
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Vid navigering till nytt listobjekt @en When navigating to a new list item
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
  /**
   * 'onclick'-event på knappelementen i listan @en List item buttons' 'onclick'-event
   */
  afOnSelect: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationContextMenu,
  inputs: ['afIcon', 'afId', 'afNavigationContextMenuItems', 'afStartSelected', 'afText']
})
@Component({
  selector: 'digi-navigation-context-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afIcon', 'afId', 'afNavigationContextMenuItems', 'afStartSelected', 'afText']
})
export class DigiNavigationContextMenu {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnInactive', 'afOnActive', 'afOnBlur', 'afOnChange', 'afOnToggle', 'afOnSelect']);
  }
}


export declare interface DigiNavigationContextMenuItem extends Components.DigiNavigationContextMenuItem {}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationContextMenuItem,
  inputs: ['afDir', 'afHref', 'afLang', 'afText', 'afType']
})
@Component({
  selector: 'digi-navigation-context-menu-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDir', 'afHref', 'afLang', 'afText', 'afType']
})
export class DigiNavigationContextMenuItem {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiNavigationMainMenu extends Components.DigiNavigationMainMenu {
  /**
   *  
   */
  afOnRoute: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationMainMenu,
  inputs: ['afId']
})
@Component({
  selector: 'digi-navigation-main-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afId']
})
export class DigiNavigationMainMenu {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnRoute']);
  }
}


export declare interface DigiNavigationMainMenuPanel extends Components.DigiNavigationMainMenuPanel {
  /**
   * När komponenten ändrar storlek @en When the component changes size
   */
  afOnResize: EventEmitter<CustomEvent<ResizeObserverEntry>>;
  /**
   * När komponenten stängs @en When the component changes size
   */
  afOnClose: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationMainMenuPanel
})
@Component({
  selector: 'digi-navigation-main-menu-panel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiNavigationMainMenuPanel {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnResize', 'afOnClose']);
  }
}


export declare interface DigiNavigationPagination extends Components.DigiNavigationPagination {
  /**
   * Vid byte av sida @en When page changes
   */
  afOnPageChange: EventEmitter<CustomEvent<number>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationPagination,
  inputs: ['afCurrentResultEnd', 'afCurrentResultStart', 'afId', 'afInitActivePage', 'afResultName', 'afTotalPages', 'afTotalResults'],
  methods: ['afMSetCurrentPage']
})
@Component({
  selector: 'digi-navigation-pagination',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCurrentResultEnd', 'afCurrentResultStart', 'afId', 'afInitActivePage', 'afResultName', 'afTotalPages', 'afTotalResults']
})
export class DigiNavigationPagination {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnPageChange']);
  }
}


export declare interface DigiNavigationSidebar extends Components.DigiNavigationSidebar {
  /**
   * Stängknappens 'onclick'-event @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter<CustomEvent<any>>;
  /**
   * Stängning av sidofält med esc-tangenten @en At close/open of sidebar using esc-key
   */
  afOnEsc: EventEmitter<CustomEvent<any>>;
  /**
   * Stängning av sidofält med klick på skuggan bakom menyn @en At close of sidebar when clicking on backdrop
   */
  afOnBackdropClick: EventEmitter<CustomEvent<any>>;
  /**
   * Vid öppning/stängning av sidofält @en At close/open of sidebar
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationSidebar,
  inputs: ['afActive', 'afBackdrop', 'afCloseButtonAriaLabel', 'afCloseButtonPosition', 'afCloseButtonText', 'afCloseFocusableElement', 'afFocusableElement', 'afHeading', 'afHeadingLevel', 'afHideHeader', 'afId', 'afMobilePosition', 'afMobileVariation', 'afPosition', 'afStickyHeader', 'afVariation']
})
@Component({
  selector: 'digi-navigation-sidebar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afBackdrop', 'afCloseButtonAriaLabel', 'afCloseButtonPosition', 'afCloseButtonText', 'afCloseFocusableElement', 'afFocusableElement', 'afHeading', 'afHeadingLevel', 'afHideHeader', 'afId', 'afMobilePosition', 'afMobileVariation', 'afPosition', 'afStickyHeader', 'afVariation']
})
export class DigiNavigationSidebar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClose', 'afOnEsc', 'afOnBackdropClick', 'afOnToggle']);
  }
}


export declare interface DigiNavigationSidebarButton extends Components.DigiNavigationSidebarButton {
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationSidebarButton,
  inputs: ['afAriaLabel', 'afId', 'afText']
})
@Component({
  selector: 'digi-navigation-sidebar-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaLabel', 'afId', 'afText']
})
export class DigiNavigationSidebarButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnToggle']);
  }
}


export declare interface DigiNavigationTab extends Components.DigiNavigationTab {
  /**
   * När tabben växlar mellan aktiv och inaktiv @en When the tab toggles between active and inactive
   */
  afOnToggle: EventEmitter<CustomEvent<boolean>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationTab,
  inputs: ['afActive', 'afAriaLabel', 'afId']
})
@Component({
  selector: 'digi-navigation-tab',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afAriaLabel', 'afId']
})
export class DigiNavigationTab {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnToggle']);
  }
}


export declare interface DigiNavigationTabInABox extends Components.DigiNavigationTabInABox {
  /**
   * När tabben växlar mellan aktiv och inaktiv @en When the tab toggles between active and inactive
   */
  afOnToggle: EventEmitter<CustomEvent<boolean>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationTabInABox,
  inputs: ['afActive', 'afAriaLabel', 'afId']
})
@Component({
  selector: 'digi-navigation-tab-in-a-box',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afAriaLabel', 'afId']
})
export class DigiNavigationTabInABox {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnToggle']);
  }
}


export declare interface DigiNavigationTabs extends Components.DigiNavigationTabs {
  /**
   *  
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   *  
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   *  
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationTabs,
  inputs: ['afAriaLabel', 'afId', 'afInitActiveTab'],
  methods: ['afMSetActiveTab']
})
@Component({
  selector: 'digi-navigation-tabs',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaLabel', 'afId', 'afInitActiveTab']
})
export class DigiNavigationTabs {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnClick', 'afOnFocus']);
  }
}


export declare interface DigiNavigationToc extends Components.DigiNavigationToc {}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationToc
})
@Component({
  selector: 'digi-navigation-toc',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiNavigationToc {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiNavigationVerticalMenu extends Components.DigiNavigationVerticalMenu {
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationVerticalMenu,
  inputs: ['afActiveIndicatorSize', 'afAriaLabel', 'afId', 'afVariation'],
  methods: ['afMSetCurrentActiveLevel']
})
@Component({
  selector: 'digi-navigation-vertical-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActiveIndicatorSize', 'afAriaLabel', 'afId', 'afVariation']
})
export class DigiNavigationVerticalMenu {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnReady']);
  }
}


export declare interface DigiNavigationVerticalMenuItem extends Components.DigiNavigationVerticalMenuItem {
  /**
   * Länken och toggle-knappens 'onclick'-event @en Link and toggle buttons 'onclick' event
   */
  afOnClick: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationVerticalMenuItem,
  inputs: ['afActive', 'afActiveSubnav', 'afAriaCurrent', 'afHasSubnav', 'afHref', 'afId', 'afOverrideLink', 'afText']
})
@Component({
  selector: 'digi-navigation-vertical-menu-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afActive', 'afActiveSubnav', 'afAriaCurrent', 'afHasSubnav', 'afHref', 'afId', 'afOverrideLink', 'afText']
})
export class DigiNavigationVerticalMenuItem {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiNotificationAlert extends Components.DigiNotificationAlert {
  /**
   * Stängknappens 'onclick'-event @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNotificationAlert,
  inputs: ['afCloseable', 'afVariation']
})
@Component({
  selector: 'digi-notification-alert',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCloseable', 'afVariation']
})
export class DigiNotificationAlert {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClose']);
  }
}


export declare interface DigiNotificationCookie extends Components.DigiNotificationCookie {
  /**
   * När användaren godkänner alla kakor @en When the user accepts all cookies
   */
  afOnAcceptAllCookies: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * När användaren godkänner alla kakor @en When the user accepts all cookies
   */
  afOnSubmitSettings: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiNotificationCookie,
  inputs: ['afBannerHeadingText', 'afBannerText', 'afId', 'afModalHeadingText', 'afRequiredCookiesText']
})
@Component({
  selector: 'digi-notification-cookie',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afBannerHeadingText', 'afBannerText', 'afId', 'afModalHeadingText', 'afRequiredCookiesText']
})
export class DigiNotificationCookie {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnAcceptAllCookies', 'afOnSubmitSettings']);
  }
}


export declare interface DigiNotificationDetail extends Components.DigiNotificationDetail {}

@ProxyCmp({
  defineCustomElementFn: defineDigiNotificationDetail,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-notification-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiNotificationDetail {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiPage extends Components.DigiPage {}

@ProxyCmp({
  defineCustomElementFn: defineDigiPage,
  inputs: ['afBackground']
})
@Component({
  selector: 'digi-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afBackground']
})
export class DigiPage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiPageBlockCards extends Components.DigiPageBlockCards {}

@ProxyCmp({
  defineCustomElementFn: defineDigiPageBlockCards,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-page-block-cards',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiPageBlockCards {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiPageBlockHero extends Components.DigiPageBlockHero {}

@ProxyCmp({
  defineCustomElementFn: defineDigiPageBlockHero,
  inputs: ['afBackground', 'afBackgroundImage', 'afVariation']
})
@Component({
  selector: 'digi-page-block-hero',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afBackground', 'afBackgroundImage', 'afVariation']
})
export class DigiPageBlockHero {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiPageBlockLists extends Components.DigiPageBlockLists {}

@ProxyCmp({
  defineCustomElementFn: defineDigiPageBlockLists,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-page-block-lists',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiPageBlockLists {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiPageBlockSidebar extends Components.DigiPageBlockSidebar {}

@ProxyCmp({
  defineCustomElementFn: defineDigiPageBlockSidebar,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-page-block-sidebar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiPageBlockSidebar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiPageFooter extends Components.DigiPageFooter {}

@ProxyCmp({
  defineCustomElementFn: defineDigiPageFooter,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-page-footer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiPageFooter {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiPageHeader extends Components.DigiPageHeader {}

@ProxyCmp({
  defineCustomElementFn: defineDigiPageHeader,
  inputs: ['afId']
})
@Component({
  selector: 'digi-page-header',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afId']
})
export class DigiPageHeader {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiProgressStep extends Components.DigiProgressStep {}

@ProxyCmp({
  defineCustomElementFn: defineDigiProgressStep,
  inputs: ['afHeading', 'afHeadingLevel', 'afId', 'afIsLast', 'afStepStatus', 'afVariation']
})
@Component({
  selector: 'digi-progress-step',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afHeading', 'afHeadingLevel', 'afId', 'afIsLast', 'afStepStatus', 'afVariation']
})
export class DigiProgressStep {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiProgressSteps extends Components.DigiProgressSteps {
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiProgressSteps,
  inputs: ['afCurrentStep', 'afHeadingLevel', 'afVariation'],
  methods: ['afMNext', 'afMPrevious']
})
@Component({
  selector: 'digi-progress-steps',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCurrentStep', 'afHeadingLevel', 'afVariation']
})
export class DigiProgressSteps {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnReady']);
  }
}


export declare interface DigiProgressbar extends Components.DigiProgressbar {}

@ProxyCmp({
  defineCustomElementFn: defineDigiProgressbar,
  inputs: ['afCompletedSteps', 'afId', 'afRole', 'afStepsLabel', 'afStepsSeparator', 'afTotalSteps', 'afVariation']
})
@Component({
  selector: 'digi-progressbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afCompletedSteps', 'afId', 'afRole', 'afStepsLabel', 'afStepsSeparator', 'afTotalSteps', 'afVariation']
})
export class DigiProgressbar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTable extends Components.DigiTable {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTable,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-table',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiTable {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTag extends Components.DigiTag {
  /**
   * Taggelementets 'onclick'-event. @en The tag elements 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiTag,
  inputs: ['afAriaLabel', 'afNoIcon', 'afSize', 'afText']
})
@Component({
  selector: 'digi-tag',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afAriaLabel', 'afNoIcon', 'afSize', 'afText']
})
export class DigiTag {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiTypography extends Components.DigiTypography {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypography,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-typography',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiTypography {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyHeadingSection extends Components.DigiTypographyHeadingSection {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyHeadingSection
})
@Component({
  selector: 'digi-typography-heading-section',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiTypographyHeadingSection {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyMeta extends Components.DigiTypographyMeta {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyMeta,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-typography-meta',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afVariation']
})
export class DigiTypographyMeta {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyPreamble extends Components.DigiTypographyPreamble {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyPreamble
})
@Component({
  selector: 'digi-typography-preamble',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiTypographyPreamble {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyTime extends Components.DigiTypographyTime {}

@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyTime,
  inputs: ['afDateTime', 'afVariation']
})
@Component({
  selector: 'digi-typography-time',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDateTime', 'afVariation']
})
export class DigiTypographyTime {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiUtilBreakpointObserver extends Components.DigiUtilBreakpointObserver {
  /**
   * Vid inläsning av sida samt vid uppdatering av brytpunkt @en At page load and at change of breakpoint on page
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'small' @en At breakpoint 'small'
   */
  afOnSmall: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'medium' @en At breakpoint 'medium'
   */
  afOnMedium: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'large' @en At breakpoint 'large'
   */
  afOnLarge: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'xlarge' @en At breakpoint 'xlarge'
   */
  afOnXLarge: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilBreakpointObserver
})
@Component({
  selector: 'digi-util-breakpoint-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiUtilBreakpointObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnSmall', 'afOnMedium', 'afOnLarge', 'afOnXLarge']);
  }
}


export declare interface DigiUtilDetectClickOutside extends Components.DigiUtilDetectClickOutside {
  /**
   * Vid klick utanför komponenten @en When click detected outside of component
   */
  afOnClickOutside: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Vid klick inuti komponenten @en When click detected inside of component
   */
  afOnClickInside: EventEmitter<CustomEvent<MouseEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilDetectClickOutside,
  inputs: ['afDataIdentifier']
})
@Component({
  selector: 'digi-util-detect-click-outside',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDataIdentifier']
})
export class DigiUtilDetectClickOutside {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClickOutside', 'afOnClickInside']);
  }
}


export declare interface DigiUtilDetectFocusOutside extends Components.DigiUtilDetectFocusOutside {
  /**
   * Vid fokus utanför komponenten @en When focus detected outside of component
   */
  afOnFocusOutside: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * Vid fokus inuti komponenten @en When focus detected inside of component
   */
  afOnFocusInside: EventEmitter<CustomEvent<FocusEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilDetectFocusOutside,
  inputs: ['afDataIdentifier']
})
@Component({
  selector: 'digi-util-detect-focus-outside',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afDataIdentifier']
})
export class DigiUtilDetectFocusOutside {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusOutside', 'afOnFocusInside']);
  }
}


export declare interface DigiUtilIntersectionObserver extends Components.DigiUtilIntersectionObserver {
  /**
   * Vid statusförändring mellan 'unintersected' och 'intersected' @en On change between unintersected and intersected
   */
  afOnChange: EventEmitter<CustomEvent<boolean>>;
  /**
   * Vid statusförändring till 'intersected' @en On every change to intersected.
   */
  afOnIntersect: EventEmitter<CustomEvent<any>>;
  /**
   * Vid statusförändring till 'unintersected'
On every change to unintersected 
   */
  afOnUnintersect: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilIntersectionObserver,
  inputs: ['afOnce', 'afOptions']
})
@Component({
  selector: 'digi-util-intersection-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afOnce', 'afOptions']
})
export class DigiUtilIntersectionObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnIntersect', 'afOnUnintersect']);
  }
}


export declare interface DigiUtilKeydownHandler extends Components.DigiUtilKeydownHandler {
  /**
   * Vid keydown på escape @en At keydown on escape key
   */
  afOnEsc: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på enter @en At keydown on enter key
   */
  afOnEnter: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på tab @en At keydown on tab key
   */
  afOnTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på space @en At keydown on space key
   */
  afOnSpace: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på skift och tabb @en At keydown on shift and tab key
   */
  afOnShiftTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil upp @en At keydown on up arrow key
   */
  afOnUp: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil ner @en At keydown on down arrow key
   */
  afOnDown: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil vänster @en At keydown on left arrow key
   */
  afOnLeft: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil höger @en At keydown on right arrow key
   */
  afOnRight: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på home @en At keydown on home key
   */
  afOnHome: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på end @en At keydown on end key
   */
  afOnEnd: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på alla tangenter @en At keydown on any key
   */
  afOnKeyDown: EventEmitter<CustomEvent<KeyboardEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilKeydownHandler
})
@Component({
  selector: 'digi-util-keydown-handler',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiUtilKeydownHandler {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnEsc', 'afOnEnter', 'afOnTab', 'afOnSpace', 'afOnShiftTab', 'afOnUp', 'afOnDown', 'afOnLeft', 'afOnRight', 'afOnHome', 'afOnEnd', 'afOnKeyDown']);
  }
}


export declare interface DigiUtilKeyupHandler extends Components.DigiUtilKeyupHandler {
  /**
   * Vid keyup på escape @en At keyup on escape key
   */
  afOnEsc: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på enter @en At keyup on enter key
   */
  afOnEnter: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på tab @en At keyup on tab key
   */
  afOnTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på space @en At keyup on space key
   */
  afOnSpace: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på skift och tabb @en At keyup on shift and tab key
   */
  afOnShiftTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil upp @en At keyup on up arrow key
   */
  afOnUp: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil ner @en At keyup on down arrow key
   */
  afOnDown: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil vänster @en At keyup on left arrow key
   */
  afOnLeft: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil höger @en At keyup on right arrow key
   */
  afOnRight: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på home @en At keyup on home key
   */
  afOnHome: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på end @en At keyup on end key
   */
  afOnEnd: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på alla tangenter @en At keyup on any key
   */
  afOnKeyUp: EventEmitter<CustomEvent<KeyboardEvent>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilKeyupHandler
})
@Component({
  selector: 'digi-util-keyup-handler',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiUtilKeyupHandler {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnEsc', 'afOnEnter', 'afOnTab', 'afOnSpace', 'afOnShiftTab', 'afOnUp', 'afOnDown', 'afOnLeft', 'afOnRight', 'afOnHome', 'afOnEnd', 'afOnKeyUp']);
  }
}


export declare interface DigiUtilMutationObserver extends Components.DigiUtilMutationObserver {
  /**
   * När DOM-element läggs till eller tas bort inuti Mutation Observer:n @en When DOM-elements is added or removed inside the Mutation Observer
   */
  afOnChange: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilMutationObserver,
  inputs: ['afId', 'afOptions']
})
@Component({
  selector: 'digi-util-mutation-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['afId', 'afOptions']
})
export class DigiUtilMutationObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange']);
  }
}


export declare interface DigiUtilResizeObserver extends Components.DigiUtilResizeObserver {
  /**
   * När komponenten ändrar storlek @en When the component changes size
   */
  afOnChange: EventEmitter<CustomEvent<ResizeObserverEntry>>;

}

@ProxyCmp({
  defineCustomElementFn: defineDigiUtilResizeObserver
})
@Component({
  selector: 'digi-util-resize-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>'
})
export class DigiUtilResizeObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange']);
  }
}
