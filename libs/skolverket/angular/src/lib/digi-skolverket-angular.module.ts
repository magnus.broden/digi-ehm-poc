import { NgModule } from '@angular/core';

import { BooleanValueAccessor } from './stencil-generated/boolean-value-accessor';
import { NumericValueAccessor } from './stencil-generated/number-value-accessor';
import { RadioValueAccessor } from './stencil-generated/radio-value-accessor';
import { SelectValueAccessor } from './stencil-generated/select-value-accessor';
import { TextValueAccessor } from './stencil-generated/text-value-accessor';
import * as Components from './stencil-generated/components';

/**
 * COMPONENTS list autogenerated on prepare.
 */

const COMPONENTS = [
 Components.DigiButton,
 Components.DigiCalendar,
 Components.DigiCalendarWeekView,
 Components.DigiCardBox,
 Components.DigiCardLink,
 Components.DigiChartLine,
 Components.DigiCode,
 Components.DigiCodeBlock,
 Components.DigiCodeExample,
 Components.DigiDialog,
 Components.DigiExpandableAccordion,
 Components.DigiFormCheckbox,
 Components.DigiFormFieldset,
 Components.DigiFormFileUpload,
 Components.DigiFormFilter,
 Components.DigiFormInput,
 Components.DigiFormInputSearch,
 Components.DigiFormLabel,
 Components.DigiFormProcessStep,
 Components.DigiFormProcessSteps,
 Components.DigiFormRadiobutton,
 Components.DigiFormRadiogroup,
 Components.DigiFormSelect,
 Components.DigiFormTextarea,
 Components.DigiFormValidationMessage,
 Components.DigiIcon,
 Components.DigiIconBars,
 Components.DigiIconCheck,
 Components.DigiIconCheckCircleRegAlt,
 Components.DigiIconChevronDown,
 Components.DigiIconChevronLeft,
 Components.DigiIconChevronRight,
 Components.DigiIconChevronUp,
 Components.DigiIconCopy,
 Components.DigiIconDangerOutline,
 Components.DigiIconDownload,
 Components.DigiIconExclamationCircleFilled,
 Components.DigiIconExclamationTriangle,
 Components.DigiIconExclamationTriangleWarning,
 Components.DigiIconExternalLinkAlt,
 Components.DigiIconMinus,
 Components.DigiIconNotificationSucces,
 Components.DigiIconNotificationWarning,
 Components.DigiIconPaperclip,
 Components.DigiIconPlus,
 Components.DigiIconSearch,
 Components.DigiIconSpinner,
 Components.DigiIconTrash,
 Components.DigiIconValidationSuccess,
 Components.DigiIconX,
 Components.DigiLayoutBlock,
 Components.DigiLayoutColumns,
 Components.DigiLayoutContainer,
 Components.DigiLayoutGrid,
 Components.DigiLayoutMediaObject,
 Components.DigiLayoutRows,
 Components.DigiLayoutStackedBlocks,
 Components.DigiLink,
 Components.DigiLinkExternal,
 Components.DigiLinkIcon,
 Components.DigiLinkInternal,
 Components.DigiListLink,
 Components.DigiLoaderSpinner,
 Components.DigiLogo,
 Components.DigiLogoService,
 Components.DigiLogoSister,
 Components.DigiMediaFigure,
 Components.DigiMediaImage,
 Components.DigiNavigationBreadcrumbs,
 Components.DigiNavigationContextMenu,
 Components.DigiNavigationContextMenuItem,
 Components.DigiNavigationMainMenu,
 Components.DigiNavigationMainMenuPanel,
 Components.DigiNavigationPagination,
 Components.DigiNavigationSidebar,
 Components.DigiNavigationSidebarButton,
 Components.DigiNavigationTab,
 Components.DigiNavigationTabInABox,
 Components.DigiNavigationTabs,
 Components.DigiNavigationToc,
 Components.DigiNavigationVerticalMenu,
 Components.DigiNavigationVerticalMenuItem,
 Components.DigiNotificationAlert,
 Components.DigiNotificationCookie,
 Components.DigiNotificationDetail,
 Components.DigiPage,
 Components.DigiPageBlockCards,
 Components.DigiPageBlockHero,
 Components.DigiPageBlockLists,
 Components.DigiPageBlockSidebar,
 Components.DigiPageFooter,
 Components.DigiPageHeader,
 Components.DigiProgressStep,
 Components.DigiProgressSteps,
 Components.DigiProgressbar,
 Components.DigiTable,
 Components.DigiTag,
 Components.DigiTypography,
 Components.DigiTypographyHeadingSection,
 Components.DigiTypographyMeta,
 Components.DigiTypographyPreamble,
 Components.DigiTypographyTime,
 Components.DigiUtilBreakpointObserver,
 Components.DigiUtilDetectClickOutside,
 Components.DigiUtilDetectFocusOutside,
 Components.DigiUtilIntersectionObserver,
 Components.DigiUtilKeydownHandler,
 Components.DigiUtilKeyupHandler,
 Components.DigiUtilMutationObserver,
 Components.DigiUtilResizeObserver
];

const VALUEACCESSORS = [
	BooleanValueAccessor,
	NumericValueAccessor,
	RadioValueAccessor,
	SelectValueAccessor,
	TextValueAccessor
];

@NgModule({
	declarations: [COMPONENTS, VALUEACCESSORS],
	exports: [COMPONENTS, VALUEACCESSORS]
})
export class DigiSkolverketAngularModule {}
