import styles from './skolverket-react.module.scss';

/* eslint-disable-next-line */
export interface SkolverketReactProps {}

export function SkolverketReact(props: SkolverketReactProps) {
	return (
		<div className={styles['container']}>
			<h1>Welcome to SkolverketReact!</h1>
		</div>
	);
}

export default SkolverketReact;
