# Skolverkets designsystem

## Publicera paket

- Uppdatera versionsnummer `npm run version-skolverket`. Accepterar [samma argument som npm version gör](https://docs.npmjs.com/cli/v9/commands/npm-version). Detta uppdaterar versionerna i [libs/skolverket/package](package/package.json) och [libs/skolverket/angular](angular/package.json).
- Uppdatera peer dependencyversionen av `@digi/skolverket` i [libs/skolverket/angular](angular/package.json) till samma som den senaste versionen.
- Säkerställ att [libs/skolverket/package/CHANGELOG.md](package/CHANGELOG.md), [libs/skolverket/angular/CHANGELOG.md](angular/CHANGELOG.md) och [apps/skolverket/docs/src/pages/kom-igang/release-notes.tsx](../../apps/skolverket/docs/src/pages/kom-igang/release-notes.tsx) är uppdaterade.
- Konfigurera din lokala `.npmrc` (inte i det här repot, utan förmodligen i din användarmapp) med behörigheter till Nexus.
- Publicera med `npm run release-skolverket`

## Publicera dokumentationswebb

(Än så länge finns ingen pipeline för det här, så tills vidare får vi leva med nedanstående högst temporära process.)

- Bygg dokumentationswebben `npm run build skolverket-docs`
- Packa ihop innehållet i `dist/apps/skolverket/docs/www/` till en zip.
- Maila zipfilen till joakim.ekman@skolverket.se
