## [17.5.0]

### Ändrat

- `digi-page-header`
  - Fixat bugg där `slot="top"` syntes även om den inte hade något innehåll.
- `digi-typography`
  - Standardstorleken på länkar är nu `1em`

## [17.4.3] - 2023-02-21

### Ändrat

- `digi-navigation-main-menu`/`digi-navigation-main-menu-panel`
  - Tagit bort felaktig vertikal yttre padding på små skärmar.
  - Korrigerat bugg där tredje nivåns höjd fortfarande kunde överflöda panelens höjd.

## [17.4.2] - 2023-02-20

### Ändrat

- `digi-navigation-main-menu`/`digi-navigation-main-menu-panel`
  - Länkar i huvudmenyn fick samma färg som bakgrunden om de var ':visited'. Så inte längre.
  - Rehydrering av slottad html vid dynamisk uppdatering av aktiv sida (med hjälp av 'aria-current="page"'). Dvs, nu uppdateras attribut och annat som behövs för att markera vilken sida som är aktiv vid dynamisk routing.
  - Menypanelens höjd hängde inte med om nivå tre var högre än nivå två. Nu ska det fungera.

## [17.4.1] - 2023-02-20

### Ändrat

- Lagt till `d3` som peer dependency

## [17.4.0] - 2023-02-17

### Nytt

- `digi-form-file-upload`
  - Lagt till stöd för att ladda upp flera filer på en gång

### Ändrat

- `digi-form-file-upload`
  - Små visuella justeringar
- `digi-icon`
  - Ändrat ikon för `validation-error`

## [17.3.0] - 2023-02-16

### Nytt

- `digi-form-file-upload`
  - Ny Skolverketversion av filuppladdaren (den tidigare var hårt knuten till Arbetsförmedlingens interna API:er). Denna har endast en variant och ingen inbygd virusskyddsfunktionalitet.

## [17.2.*] - 2023-02-02

### Ändrat

- `digi-icon`
  - Flera ikoner har bytt namn från hur de ser ut, till hur de används. Dessa har ändrats:
    - check-circle ---> validation-success
    - check-circle-reg ---> notification-success
    - exclamation-circle ---> notification-warning
    - hexagon-cross ---> notification-error
    - hexagon-cross-filled --> validation-error
    - exclamation-circle-colored ---> validation-warning
    - info-circle-reg ---> notification-info

### Ändrat

- `digi-navigation-tabs`
  - Fixat bugg där `digi-navigation-tab-in-a-box` inte fungerade som den skulle i Angular
-

## [17.1.1] - 2023-01-30

### Ändrat

- `digi-navigation-tabs`
  - Fixat bugg där `digi-navigation-tab-in-a-box` inte fungerade som den skulle i Angular

## [17.1.0] - 2023-01-27

### Nytt

- `digi-navigation-main-menu`
  - Nytt event `afOnRoute` emittas vid klick på länk.

### Ändrat

- `digi-page`, `digi-page-header`, `digi-navigation-main-menu`, `digi-navigation-main-menu-panel`
  - Layoutkorrigeringar för att undvika horisontell scrollbar
- `digi-page-header`
  - Huvudnavigationen stängs nu vid klick på länk i mobilläge
- `digi-navigation-pagination`
  - Fixat bugg där 'nästa'-knappen inte försvann om man gick direkt till sista sidan i väljaren
- `digi-form-input-search`
  - Stoppat propagering av klick- och fokusevents från den nestade knappen så det inte blir dubblerade event
