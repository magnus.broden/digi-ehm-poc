const path = require('path');

module.exports = {
	stories: ['../src/**/*.stories.@(ts|tsx)'],
	addons: [
		{
			name: '@storybook/addon-essentials'
		},
		'@storybook/addon-knobs/dist/register',
		'@pxtrn/storybook-addon-docs-stencil'
	],
	core: { builder: 'webpack5' },
	webpackFinal: async (config) => {
		config.module.rules.push({
			test: /\.scss$/,
			use: ['style-loader', 'css-loader', 'sass-loader'],
			include: path.resolve(__dirname, '../')
		});
		return config;
	},
  managerWebpack: (config, options) => {
    options.cache.set = () => Promise.resolve();
    return config;
  }
};
