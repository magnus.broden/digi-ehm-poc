export default {
	title: 'mockups and tests/layouts'
};

const placeholderBlock = (
	text = ''
) => `<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)">
    <digi-typography af-variation="small">
      <p>${text}</p>
    </digi-typography>
    </div>
    `;

export const PageLayoutTest = () =>
	`<digi-layout-rows>
      <div style="background: var(--digi--color--background--tertiary); min-height: 33vh; display: flex; align-items: center;">
        <digi-layout-container style="flex: 1;">
          <digi-typography style="padding: var(--digi--gutter--medium);">
            <h1>Test page</h1>
            <p>Klaatu verada niktu</p>
          </digi-typography>
        </digi-layout-container>  
      </div>
      <digi-layout-container>
        <digi-card-box af-width="full">
          <digi-layout-stacked-blocks>
            ${placeholderBlock('Block 1')}
            ${placeholderBlock('Block 2')}
            ${placeholderBlock('Block 3')}
            ${placeholderBlock('Block 4')}
            ${placeholderBlock('Block 5')}
            ${placeholderBlock('Block 6')}
          </digi-layout-stacked-blocks>
        </digi-card-box>
      </digi-layout-container>
  </digi-layout-rows>`;

export const TabsAndThings = () => /* html */ `
  
    <digi-layout-rows>
      <digi-layout-block af-vertical-padding af-variation="secondary">
        <digi-typography>
          <h1>Välkommen till testsidan!</h1>
          <digi-typography-preamble>Detta är en sida som är tänkt att användas för att testa olika layouter och komponenter.</digi-typography-preamble>
          <digi-button af-size="large">Ladda hem rapporten</digi-button>
          </digi-typography> 
      </digi-layout-block>
      <digi-layout-block af-vertical-padding>
        <digi-navigation-tabs>
          <digi-navigation-tab-in-a-box af-aria-label="Översikt">
            <digi-layout-rows>
            <h2>Här kan du se allt du kan göra</h2>
            <digi-layout-stacked-blocks>
              <digi-card-link>
              <img slot="image" src="https://www.skolverket.se/images/18.3770ea921807432e6c726e7/1656400441366/s%C3%A4kerhet-kris-bild-webb.png" width="392" height="220" style="width: 100%; height: auto; display: block;" />
              <h2 slot="link-heading"><a href="#">Beredskap vid ett förändrat omvärldsläge</a></h2>
          <p>Förskola och skola är samhällsviktig verksamhet som behöver fungera även vid olika typer av kriser. Här finns stöd för hur du kan förbereda din verksamhet vid olika händelser av kris.</p>
            </digi-card-link>
            <digi-card-link>
              <img slot="image" src="https://www.skolverket.se/images/18.3e89579d17f69780fbcceb/1648714549765/Mottagandet%20fr%C3%A5n%20Ukraina%20-puffbild.png" width="392" height="220" style="width: 100%; height: auto; display: block;" />
              <h2 slot="link-heading"><a href="#" slot="link-heading">Nyanlända barn och elever från Ukraina</a></h2>
              <p>Här hittar du som tar emot barn och elever från Ukraina information vad som gäller, det stöd vi erbjuder för mottagande och kartläggning och om skolsystemet och de olika skolformerna på engelska.</p>
            </digi-card-link>
            <digi-card-link>
              <h2 slot="link-heading"><a href="#">Ett kort utan bild</a></h2>
              <p>Etiam porta sem malesuada magna mollis euismod.</p>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
            </digi-card-link>
            <digi-card-link>
              <img slot="image" src="https://www.skolverket.se/images/18.d9bdf0a17bea20b43b101f/1633508909105/andringar-kursplaner-IG_355x200p.jpg" width="392" height="220" style="width: 100%; height: auto; display: block;" />
              <h2 slot="link-heading"><a href="#">Och så en länkad rubrik</a></h2>
            </digi-card-link>
            <digi-card-link>
              <div slot="image"></div>
              <h2 slot="link-heading"><a href="#">Och så en länkad rubrik</a></h2>
              <p>Lägg in nåt tomt element i slot="image" så får du en automatisk placeholder</p>
            </digi-card-link>
            </digi-layout-stacked-blocks>
            </digi-layout-rows>
          </digi-navigation-tab-in-a-box>
          <digi-navigation-tab-in-a-box af-aria-label="Undersikt">
          <digi-layout-rows>
            <h2>Här kan du se allt du inte kan göra</h2>
            <form>
              <digi-form-fieldset>
              <digi-layout-columns>
                <digi-form-input-skv af-label="Förnamn"></digi-form-input-skv>
                <digi-form-input-skv af-label="Mellannamn"></digi-form-input-skv>
                <digi-form-input-skv af-label="Efternamn"></digi-form-input-skv>
              </digi-layout-columns>
              </digi-form-fieldset>
            </form>
            </digi-layout-rows>
          </digi-navigation-tab-in-a-box>
          <digi-navigation-tab-in-a-box af-aria-label="Insikt">
          <digi-layout-rows>
          <h2>Här kan du se allt du borde göra</h2>
          <digi-table-skv>
          <table>
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Mass (10<sup>24</sup>kg)</th>
              <th scope="col">Diameter (km)</th>
              <th scope="col">Density (kg/m<sup>3</sup>)</th>
              <th scope="col">Gravity (m/s<sup>2</sup>)</th>
              <th scope="col">Length of day (hours)</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <th scope="row">Mercury</th>
            <td>0.330</td>
            <td>4,879</td>
            <td>5427</td>
            <td>3.7</td>
            <td>4222.6</td>
          </tr>
          <tr>
            <th scope="row">Active row</th>
            <td>4.87</td>
            <td>12,104</td>
            <td>5243</td>
            <td>8.9</td>
            <td>2802.0</td>
          </tr>
          <tr>
            <th scope="row">Earth</th>
            <td>5.97</td>
            <td>12,756</td>
            <td>5514</td>
            <td>9.8</td>
            <td>24.0</td>
          </tr>
          <tr>
            <th scope="row">Mars</th>
            <td>Active cell</td>
            <td>6,792</td>
            <td>3933</td>
            <td>3.7</td>
            <td>24.7</td>
          </tr>
          </tbody>
        </table>
          </digi-table-skv>
          </digi-layout-rows>
          </digi-navigation-tab-in-a-box>
        </digi-navigation-tabs> 
      </digi-layout-block>
      <digi-page-footer>
      <digi-list-link af-variation="compact" slot="top-first">
      <h3 slot="heading">Kontakt</h3>
      <li><a href="#">Alla kontaktuppgifter</a></li>
      <li><a href="#">Lämna en synpunkt</a></li>
      <li><a href="#">Skolverket på sociala medier</a></li>
    </digi-list-link>
      <digi-list-link af-variation="compact" slot="top">
        <h3 slot="heading">Om oss</h3>
        <li><a href="#">Om webbplatsen</a></li>
        <li><a href="#">Vår verksamhet</a></li>
        <li><a href="#">Organisation</a></li>
        <li><a href="#">Jobba hos oss</a></li>
      </digi-list-link>
      <digi-list-link af-variation="compact" slot="top">
        <h3 slot="heading">Nyheter och media</h3>
        <li><a href="#">Publikationer & nyhetsbrev</a></li>
        <li><a href="#">Press</a></li>
        <li><a href="#">Kalender</a></li>
        <li><a href="#">Öppna data</a></li>
        <li><a href="#">Jobba hos oss</a></li>
      </digi-list-link>
      <digi-list-link af-variation="compact" af-layout="inline" slot="bottom">
        <li><a href="#">Behandling av personuppgifter</a></li>
        <li><a href="#">Kakor</a></li>
        <li><a href="#">Tillgänglighet</a></li>
      </digi-list-link>
      </digi-page-footer>
    </digi-layout-rows>`;
