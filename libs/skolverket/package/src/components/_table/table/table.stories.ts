import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { TableVariation } from './table-variation.enum';

export default {
	title: 'table/digi-table',
	argTypes: {
		'af-variation': enumSelect(TableVariation)
	}
};

const tableMarkup = /*html*/ `
<table>
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Mass (10<sup>24</sup>kg)</th>
      <th scope="col">Diameter (km)</th>
      <th scope="col">Density (kg/m<sup>3</sup>)</th>
      <th scope="col">Gravity (m/s<sup>2</sup>)</th>
      <th scope="col">Length of day (hours)</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <th scope="row">Mercury</th>
    <td>0.330</td>
    <td>4,879</td>
    <td>5427</td>
    <td>3.7</td>
    <td>4222.6</td>
  </tr>
  <tr>
    <th scope="row">Active row</th>
    <td>4.87</td>
    <td>12,104</td>
    <td>5243</td>
    <td>8.9</td>
    <td>2802.0</td>
  </tr>
  <tr>
    <th scope="row">Earth</th>
    <td>5.97</td>
    <td>12,756</td>
    <td>5514</td>
    <td>9.8</td>
    <td>24.0</td>
  </tr>
  <tr>
    <th scope="row">Mars</th>
    <td>Active cell</td>
    <td>6,792</td>
    <td>3933</td>
    <td>3.7</td>
    <td>24.7</td>
  </tr>
  </tbody>
</table>`;

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-table',
	'af-variation': TableVariation.PRIMARY,
	children: tableMarkup
};

export const WithActiveRowAndCell = Template.bind({});
WithActiveRowAndCell.args = {
	component: 'digi-table',
	/* html */
	children: `
    <table>
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Mass (10<sup>24</sup>kg)</th>
          <th scope="col">Diameter (km)</th>
          <th scope="col">Density (kg/m<sup>3</sup>)</th>
          <th scope="col">Gravity (m/s<sup>2</sup>)</th>
          <th scope="col">Length of day (hours)</th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <th scope="row">Mercury</th>
        <td>0.330</td>
        <td>4,879</td>
        <td>5427</td>
        <td>3.7</td>
        <td>4222.6</td>
      </tr>
      <tr data-af-table-row="active">
        <th scope="row">Active row</th>
        <td>4.87</td>
        <td>12,104</td>
        <td>5243</td>
        <td>8.9</td>
        <td>2802.0</td>
      </tr>
      <tr>
        <th scope="row">Earth</th>
        <td>5.97</td>
        <td>12,756</td>
        <td>5514</td>
        <td>9.8</td>
        <td>24.0</td>
      </tr>
      <tr>
        <th scope="row">Mars</th>
        <td data-af-table-cell="active">Active cell</td>
        <td>6,792</td>
        <td>3933</td>
        <td>3.7</td>
        <td>24.7</td>
      </tr>
      </tbody>
    </table>`
};
