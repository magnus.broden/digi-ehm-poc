import { newE2EPage } from '@stencil/core/testing';

describe('digi-table', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-table></digi-table>');

		const element = await page.find('digi-table');
		expect(element).toHaveClass('hydrated');
	});
});
