import { Component, h, Prop } from '@stencil/core';
import { CardBoxGutter } from '../../_card/card-box/card-box-gutter.enum';
import { TableVariation } from './table-variation.enum';

/**
 * @slot default - Ska innehålla ett table-element
 *
 * @swedishName Tabell
 */
@Component({
	tag: 'digi-table',
	styleUrls: ['table.scss'],
	scoped: true
})
export class Table {
	@Prop() afVariation: TableVariation = TableVariation.PRIMARY;

	get cssModifiers() {
		return {
			[`digi-table--variation-${this.afVariation}`]: !!this.afVariation
		};
	}

	render() {
		return this.afVariation === TableVariation.SECONDARY ? (
			<digi-card-box afGutter={CardBoxGutter.NONE}>
				<div
					class={{
						'digi-table': true,
						...this.cssModifiers
					}}
				>
					<slot></slot>
				</div>
			</digi-card-box>
		) : (
			<div
				class={{
					'digi-table': true,
					...this.cssModifiers
				}}
			>
				<slot></slot>
			</div>
		);
	}
}
