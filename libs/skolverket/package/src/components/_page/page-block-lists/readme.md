# digi-page-block-lists

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                                                      | Default     |
| ------------- | -------------- | ----------- | --------------------------------------------------------- | ----------- |
| `afVariation` | `af-variation` |             | `"article" \| "process" \| "section" \| "start" \| "sub"` | `undefined` |


## Slots

| Slot        | Description |
| ----------- | ----------- |
| `"default"` |             |
| `"heading"` | Rubrik      |


## Dependencies

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)
- [digi-card-box](../../_card/card-box)
- [digi-typography-heading-section](../../_typography/typography-heading-section)
- [digi-layout-stacked-blocks](../../_layout/layout-stacked-blocks)

### Graph
```mermaid
graph TD;
  digi-page-block-lists --> digi-layout-container
  digi-page-block-lists --> digi-card-box
  digi-page-block-lists --> digi-typography-heading-section
  digi-page-block-lists --> digi-layout-stacked-blocks
  style digi-page-block-lists fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
