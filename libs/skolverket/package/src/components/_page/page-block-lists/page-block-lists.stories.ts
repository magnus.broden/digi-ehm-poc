import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { PageBlockListsVariation } from './page-block-lists-variation.enum';

export default {
	title: 'page/digi-page-block-lists',
	argTypes: {
		'af-variation': enumSelect(PageBlockListsVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-page-block-lists',
	'af-variation': 'start',
	/* html */
	children: `
    <h2 slot="heading">Avsnittsrubrik</h2>
    <digi-list-link>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
    </digi-list-link>
    <digi-list-link>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
    </digi-list-link>
  `
};
