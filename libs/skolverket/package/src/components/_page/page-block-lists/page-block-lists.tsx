import { Component, h, Prop } from '@stencil/core';
import { CardBoxWidth } from '../../../enums-skolverket';
import { PageBlockListsVariation } from './page-block-lists-variation.enum';

/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med listor
 */
@Component({
	tag: 'digi-page-block-lists',
	styleUrls: ['page-block-lists.scss'],
	scoped: true,
	assetsDirs: ['public']
})
export class PageBlockLists {
	@Prop() afVariation: `${PageBlockListsVariation}`;

	get cssModifiers() {
		return {
			[`digi-page-block-lists--variation-${this.afVariation}`]: !!this.afVariation
		};
	}

	render() {
		return (
			<digi-layout-container
				class={{ 'digi-page-block-lists': true, ...this.cssModifiers }}
			>
				<digi-card-box afWidth={CardBoxWidth.FULL}>
					<div class="digi-page-block-lists__inner">
						<digi-typography-heading-section>
							<slot name="heading"></slot>
						</digi-typography-heading-section>
						<digi-layout-stacked-blocks>
							<slot></slot>
						</digi-layout-stacked-blocks>
					</div>
				</digi-card-box>
			</digi-layout-container>
		);
	}
}
