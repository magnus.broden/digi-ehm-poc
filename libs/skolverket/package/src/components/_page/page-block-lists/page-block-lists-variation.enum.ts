export enum PageBlockListsVariation {
	START = 'start',
	SUB = 'sub',
	SECTION = 'section',
	PROCESS = 'process',
	ARTICLE = 'article'
}
