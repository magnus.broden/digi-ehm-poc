import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'page/digi-page-header'
	// argTypes: {
	// 	'af-width': enumSelect(PageHeaderVariation)
	// }
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'div',
	/* html */
	children: `
  <digi-page-header>
    <span slot="top">
    Till <a href="https://www.ffddfdfdfdf.se/">skolverket.se</a>
    </span>
      <a href="#" slot="logo"><digi-logo></digi-logo></a>
      <ul slot="eyebrow">
        <li><digi-link-icon><a href="#"><digi-icon af-name="search" aria-hidden="true"></digi-icon>Sök</a></digi-link-icon></li>
        <li><digi-link-icon><a href="#"><digi-icon af-name="accessibility-deaf" aria-hidden="true"></digi-icon>Lyssna</a></digi-link-icon></li>
        <li><digi-link-icon><a href="#"><digi-icon af-name="lattlast" aria-hidden="true"></digi-icon>Lättläst</a></digi-link-icon></li>
        <li><digi-link-icon><a href="#"><digi-icon af-name="globe" aria-hidden="true"></digi-icon>Languages</a></digi-link-icon></li>
      </ul>
      <nav slot="nav">
      <ul>
        <li>
          <button>Huvudingång 1</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 1</a>
            <ul>
              <li>
                <a href="#">Ingång 1 nivå 2.1</a>
              </li>
              <li>
                <a href="#">Ingång 1 nivå 2.2</a>
                <ul>
                  <li>
                    <a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
                  </li>
                  <li>
                    <a href="#">Ingång 1 nivå 2.2 nivå 3.2</a>
                    <ul>
                      <li>
                        <a href="#">Ingång 1 nivå 3.2 nivå 4.1</a>
                      </li>
                      <li>
                        <a href="#" aria-current="page">Ingång 1 nivå 3.2 nivå 4.2</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#">Ingång 1 nivå 2.3</a>
              </li>
              <li>
                <a href="#">Ingång 1 nivå 2.4</a>
                <ul>
                  <li>
                    <a href="#">Ingång 1 nivå 2.4 nivå 3.1</a>
                    <ul>
                      <li>
                        <a href="#">Ingång 1 nivå 2.4 nivå 4.1</a>
                      </li>
                      <li>
                        <a href="#">Ingång 1 nivå 2.4 nivå 4.2</a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Ingång 1 nivå 2.4 nivå 3.2</a>
                  </li>
                </ul>
              </li>
            </ul>
          </digi-navigation-main-menu-panel>
        </li>
        <li>
          <button>Huvudingång 2</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 2</a>
            <ul>
              <li>
                <a href="#">Ingång 2 nivå 2.1</a>
              </li>
              <li>
                <a href="#">Ingång 2 nivå 2.2</a>
                <ul>
                  <li>
                    <a href="#">Ingång 2 nivå 2.2 nivå 3.1</a>
                  </li>
                  <li>
                    <a href="#">Ingång 2 nivå 2.2 nivå 3.2</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#">Ingång 2 nivå 2.3</a>
              </li>
            </ul>
          </digi-navigation-main-menu-panel>
        </li>
        <li>
          <button>Huvudingång 3</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 3</a>
            <ul>
              <li>
                <a href="#">Ingång 3 nivå 2.1</a>
              </li>
              <li>
                <a href="#">Ingång 3 nivå 2.2</a>
                <ul>
                  <li>
                    <a href="#">Ingång 3 nivå 2.2 nivå 3.1</a>
                  </li>
                  <li>
                    <a href="#">Ingång 3 nivå 2.2 nivå 3.2</a>
                  </li>
                </ul>
              </li>
            </ul>
          </digi-navigation-main-menu-panel>
        </li>
        <li><a href="#">Huvudingång 4</a></li>
        <li>
          <button>Huvudingång 5</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 5</a>
            <ul>
              <li>
                <a href="#">Ingång 5 nivå 2.1</a>
              </li>
            </ul>
          </digi-navigation-main-menu-panel>
        </li>
      </ul>
    </nav>
      
      </digi-page-header>
      <p>Jag är under</p>
    `
};

// export const EService = Template.bind({});
// EService.args = {
// 	component: 'digi-page-header',
// 	/* html */
// 	children: `
//       <p slot="top">Top</p>
//       <a href="#" slot="logo"><digi-logo></digi-logo></a>
//       <form slot="eyebrow"><digi-form-input-search-skv af-label="Sök" af-hide-button af-hide-label></digi-form-input-search-skv></form>
//       <ul class="digi-u--cols" slot="eyebrow">
//         <li><digi-link-icon><a href="#"><digi-icon af-name="accessibility-deaf" aria-hidden="true"></digi-icon>Lyssna</a></digi-link-icon></li>
//         <li><digi-link-icon><a href="#"><digi-icon af-name="globe" aria-hidden="true"></digi-icon>Languages</a></digi-link-icon></li>
//       </ul>
//       <p slot="nav">Nav</p>
//     `
// };
