import { Component, h, Element, Listen, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { BREAKPOINT_LARGE } from '../../../design-tokens/web/tokens.es6';
import { HTMLStencilElement } from '@stencil/core/internal';
import { _t } from '@digi/skolverket/text';

/**
 * @slot default - kan innehålla vad som helst
 * @slot top - länk till mina sidor, tillbaka till skolvetket.se etc
 * @slot logo - ska innehålla en logotyp
 * @slot nav - ska innehålla en navigation
 *
 * @swedishName Sidhuvud
 */
@Component({
	tag: 'digi-page-header',
	styleUrls: ['page-header.scss'],
	scoped: true
})
export class PageHeader {
	private toggleButton?: HTMLButtonElement;

	@Element() hostElement: HTMLStencilElement;
	@State() isExpanded = false;
	@State() hasTopContent: boolean;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-page-header');

	componentWillLoad() {
		this.setHasTopContent();
	}

	componentWillUpdate() {
		this.setHasTopContent();
	}

	setHasTopContent() {
		this.hasTopContent = !!this.hostElement.querySelector('[slot="top"]');
	}

	get cssModifiers() {
		return {
			[`digi-page-header--expanded-${this.isExpanded}`]: true,
			[`digi-page-header--top`]: this.hasTopContent
		};
	}

	@Listen('keyup')
	keyUpHandler(e: KeyboardEvent) {
		if (
			e.key !== 'Escape' ||
			window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches ||
			!!this.hostElement.querySelector('.digi-navigation-main-menu--active-true')
		)
			return;

		this.isExpanded = false;
		this.toggleButton?.focus();
	}

	routeHandler() {
		if (window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches) {
			return;
		}

		this.isExpanded = false;
	}

	render() {
		return (
			<div
				class={{
					'digi-page-header': true,
					...this.cssModifiers
				}}
			>
				{this.hasTopContent && (
					<digi-layout-container
						class="digi-page-header__top"
						id={`${this.afId}-top`}
					>
						<div>
							<slot name="top"></slot>
						</div>
					</digi-layout-container>
				)}
				<div class="digi-page-header__main">
					<div class="digi-page-header__logo">
						<slot name="logo"></slot>
					</div>
					<div class="digi-page-header__toggle-button">
						<button
							aria-controls={`${this.afId}-top ${this.afId}-eyebrow ${this.afId}-nav`}
							aria-expanded={`${this.isExpanded}`}
							onClick={() => (this.isExpanded = !this.isExpanded)}
							ref={(el) => (this.toggleButton = el)}
						>
							<digi-icon afName={this.isExpanded ? 'x' : 'bars'}></digi-icon>
							<span>{this.isExpanded ? _t.close : _t.menu}</span>
						</button>
					</div>
					<div class="digi-page-header__eyebrow" id={`${this.afId}-eyebrow`}>
						<slot name="eyebrow"></slot>
					</div>
				</div>
				<div class="digi-page-header__nav" id={`${this.afId}-nav`}>
					<digi-navigation-main-menu onAfOnRoute={() => this.routeHandler()}>
						<slot name="nav"></slot>
					</digi-navigation-main-menu>
				</div>
			</div>
		);
	}
}
