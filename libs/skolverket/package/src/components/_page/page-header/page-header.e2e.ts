import { newE2EPage } from '@stencil/core/testing';

describe('digi-page-header', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-page-header></digi-page-header>');

		const element = await page.find('digi-page-header');
		expect(element).toHaveClass('hydrated');
	});
});
