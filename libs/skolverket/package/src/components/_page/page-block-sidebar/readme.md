# digi-page-block-sidebar

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                                                      | Default     |
| ------------- | -------------- | ----------- | --------------------------------------------------------- | ----------- |
| `afVariation` | `af-variation` |             | `"article" \| "process" \| "section" \| "start" \| "sub"` | `undefined` |


## Slots

| Slot        | Description |
| ----------- | ----------- |
| `"default"` |             |
| `"sidebar"` |             |


## Dependencies

### Depends on

- [digi-layout-grid](../../_layout/layout-grid)

### Graph
```mermaid
graph TD;
  digi-page-block-sidebar --> digi-layout-grid
  digi-layout-grid --> digi-layout-container
  style digi-page-block-sidebar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
