import { newE2EPage } from '@stencil/core/testing';

describe('digi-page-block-cards', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-page-block-cards></digi-page-block-cards>');

		const element = await page.find('digi-page-block-cards');
		expect(element).toHaveClass('hydrated');
	});
});
