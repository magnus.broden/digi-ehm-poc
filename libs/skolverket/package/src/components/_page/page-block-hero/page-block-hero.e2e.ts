import { newE2EPage } from '@stencil/core/testing';

describe('digi-page-block-hero', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-page-block-hero></digi-page-block-hero>');

		const element = await page.find('digi-page-block-hero');
		expect(element).toHaveClass('hydrated');
	});
});
