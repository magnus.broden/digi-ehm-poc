# digi-page

<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description | Type                                                                                                                            | Default     |
| -------------- | --------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `afBackground` | `af-background` |             | `"dotted_1" \| "dotted_2" \| "dotted_3" \| "square_1" \| "square_2" \| "square_3" \| "striped_1" \| "striped_2" \| "striped_3"` | `undefined` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |
| `"footer"`  | Sidfot                      |
| `"header"`  | Sidhuvud                    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
