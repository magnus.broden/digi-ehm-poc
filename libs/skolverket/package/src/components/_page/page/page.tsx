import { Component, getAssetPath, h, Prop } from '@stencil/core';
import { PageBackground } from './page-background.enum';

/**
 * @slot default - kan innehålla vad som helst
 * @slot header - Sidhuvud
 * @slot footer - Sidfot
 *
 * @swedishName Sidmall
 */
@Component({
	tag: 'digi-page',
	styleUrls: ['page.scss'],
	scoped: true,
	assetsDirs: ['public']
})
export class Page {
	@Prop() afBackground: `${PageBackground}`;

	render() {
		return (
			<div
				class="digi-page"
				style={{
					'--digi--page--background-image': this.afBackground
						? `url('${getAssetPath(`./public/images/${this.afBackground}.svg`)}')`
						: null
				}}
			>
				<div class="digi-page__header">
					<slot name="header"></slot>
				</div>
				<div class="digi-page__content">
					<slot></slot>
				</div>
				<div class="digi-page__footer">
					<slot name="footer"></slot>
				</div>
			</div>
		);
	}
}
