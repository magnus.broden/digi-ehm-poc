import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { PageBackground } from './page-background.enum';
import { pageFooterExampleMarkup } from '../page-footer/page-footer-example-markup';

export default {
	title: 'page/digi-page',
	argTypes: {
		'af-background': enumSelect(PageBackground)
	}
};

const cardMarkup = /* html */ `
<digi-card-link>
<img slot="image" src="https://www.skolverket.se/images/18.3770ea921807432e6c726e7/1656400441366/s%C3%A4kerhet-kris-bild-webb.png" width="392" height="220" style="width: 100%; height: auto; display: block;" />
<h2 slot="link-heading"><a href="#">Beredskap vid ett förändrat omvärldsläge</a></h2>
<p>Förskola och skola är samhällsviktig verksamhet som behöver fungera även vid olika typer av kriser. Här finns stöd för hur du kan förbereda din verksamhet vid olika händelser av kris.</p>
</digi-card-link>
<digi-card-link>
<img slot="image" src="https://www.skolverket.se/images/18.3e89579d17f69780fbcceb/1648714549765/Mottagandet%20fr%C3%A5n%20Ukraina%20-puffbild.png" width="392" height="220" style="width: 100%; height: auto; display: block;" />
<h2 slot="link-heading"><a href="#" slot="link-heading">Nyanlända barn och elever från Ukraina</a></h2>
<p>Här hittar du som tar emot barn och elever från Ukraina information vad som gäller, det stöd vi erbjuder för mottagande och kartläggning och om skolsystemet och de olika skolformerna på engelska.</p>
</digi-card-link>
<digi-card-link>
  <img slot="image" src="https://www.skolverket.se/images/18.d9bdf0a17bea20b43b101f/1633508909105/andringar-kursplaner-IG_355x200p.jpg" width="392" height="220" style="width: 100%; height: auto; display: block;" />
  <h2 slot="link-heading"><a href="#">Och så en länkad rubrik</a></h2>
  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
</digi-card-link>
`;

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-page',
	'af-background': 'striped_1',
	/* html */
	children: `
    <header slot="header" style="background: var(--digi--color--background--inverted-1); height: 80px;"></header>
    <digi-page-block-hero af-variation="start" af-background="transparent">
      <h1 slot="heading">Välkommen till Skolverket</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl vitae augue.</p>
    </digi-page-block-hero>
    <p>Här kan man lägga grejer. Hest olika typer av sidblock. Se den andra storyn för exempel.</p>
    <footer slot="footer"><digi-page-footer>${pageFooterExampleMarkup}</digi-page-footer></footer>
  `
};

const listMarkup = /* html */ `
<digi-list-link>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
    </digi-list-link>
    <digi-list-link>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
      <li><a href="#">Undersida</a></li>
    </digi-list-link>`;

export const WithPageBlocks = Template.bind({});
WithPageBlocks.args = {
	component: 'digi-page',
	'af-background': 'striped_1',
	/* html */
	children: `
    <header slot="header">
      <digi-page-header>
      <p slot="top">Top</p>
      <a href="#" slot="logo"><digi-logo></digi-logo></a>
      <ul class="digi-u--cols" slot="eyebrow">
        <li><digi-link-icon><a href="#"><digi-icon af-name="search" aria-hidden="true"></digi-icon>Sök</a></digi-link-icon></li>
        <li><digi-link-icon><a href="#"><digi-icon af-name="accessibility-deaf" aria-hidden="true"></digi-icon>Lyssna</a></digi-link-icon></li>
        <li><digi-link-icon><a href="#"><digi-icon af-name="lattlast" aria-hidden="true"></digi-icon>Lättläst</a></digi-link-icon></li>
        <li><digi-link-icon><a href="#"><digi-icon af-name="globe" aria-hidden="true"></digi-icon>Languages</a></digi-link-icon></li>
      </ul>
      <p slot="nav">Nav</p>
      </digi-page-header>
    </header>
    <digi-page-block-hero af-variation="start">
      <h1 slot="heading">Välkommen till Skolverket</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl vitae augue.</p>
    </digi-page-block-hero>
    <digi-page-block-cards af-variation="start">
      <h2 slot="heading">Undervisning</h2>
      ${cardMarkup}
    </digi-page-block-cards>
    <digi-page-block-lists af-variation="start">
      <h2 slot="heading">Vidare läsning</h2>
      ${listMarkup}
    </digi-page-block-lists>
    <digi-page-block-cards af-variation="start">
      <h2 slot="heading">Skolutveckling</h2>
      ${cardMarkup}
    </digi-page-block-cards>
    <footer slot="footer"><digi-page-footer>${pageFooterExampleMarkup}</digi-page-footer></footer>
  `
};
