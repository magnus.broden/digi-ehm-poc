export enum PageBackground {
	STRIPED_1 = 'striped_1',
	STRIPED_2 = 'striped_2',
	STRIPED_3 = 'striped_3',
	SQUARE_1 = 'square_1',
	SQUARE_2 = 'square_2',
	SQUARE_3 = 'square_3',
	DOTTED_1 = 'dotted_1',
	DOTTED_2 = 'dotted_2',
	DOTTED_3 = 'dotted_3'
}
