export const pageFooterExampleMarkup = /*html*/ `
<digi-list-link af-variation="compact" slot="top-first">
  <h3 slot="heading">Kontakt</h3>
  <li><a href="#">Alla kontaktuppgifter</a></li>
  <li><a href="#">Lämna en synpunkt</a></li>
  <li><a href="#">Skolverket på sociala medier</a></li>
</digi-list-link>
<digi-list-link af-variation="compact" slot="top">
  <h3 slot="heading">Om oss</h3>
  <li><a href="#">Om webbplatsen</a></li>
  <li><a href="#">Vår verksamhet</a></li>
  <li><a href="#">Organisation</a></li>
  <li><a href="#">Jobba hos oss</a></li>
</digi-list-link>
<digi-list-link af-variation="compact" slot="top">
  <h3 slot="heading">Nyheter och media</h3>
  <li><a href="#">Publikationer & nyhetsbrev</a></li>
  <li><a href="#">Press</a></li>
  <li><a href="#">Kalender</a></li>
  <li><a href="#">Öppna data</a></li>
  <li><a href="#">Jobba hos oss</a></li>
</digi-list-link>
<digi-list-link af-variation="compact" af-layout="inline" slot="bottom">
  <li><a href="#">Behandling av personuppgifter</a></li>
  <li><a href="#">Kakor</a></li>
  <li><a href="#">Tillgänglighet</a></li>
</digi-list-link>`;
