import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { PageFooterVariation } from './page-footer-variation.enum';
import { pageFooterExampleMarkup } from './page-footer-example-markup';

export default {
	title: 'page/digi-page-footer',
	argTypes: {
		'af-width': enumSelect(PageFooterVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-page-footer',
	'af-variation': PageFooterVariation.PRIMARY,
	/* html */
	children: pageFooterExampleMarkup
};
