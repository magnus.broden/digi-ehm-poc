import { Component, h, Prop } from '@stencil/core';
import { PageFooterVariation } from './page-footer-variation.enum';

/**
 * @slot default - kan innehålla vad som helst
 * @slot top-first
 * @slot top
 * @slot bottom
 *
 * @swedishName Sidfot
 */
@Component({
	tag: 'digi-page-footer',
	styleUrls: ['page-footer.scss'],
	scoped: true
})
export class PageFooter {
	@Prop() afVariation: PageFooterVariation = PageFooterVariation.PRIMARY;
	get cssModifiers() {
		return {
			[`digi-page-footer--variation-${this.afVariation}`]: !!this.afVariation
		};
	}

	render() {
		return (
			<digi-typography
				class={{
					'digi-page-footer': true,
					...this.cssModifiers
				}}
			>
				<digi-layout-grid>
					<div class="digi-page-footer__section digi-page-footer__section--top">
						<div class="digi-page-footer__top-first">
							<slot name="top-first"></slot>
						</div>
						<slot name="top"></slot>
					</div>
					<div class="digi-page-footer__section digi-page-footer__section--bottom">
						<div class="digi-page-footer__logo">
							<digi-logo af-title="Skolverket" af-desc="Skolverket logo"></digi-logo>
						</div>
						<div class="digi-page-footer__bottom-end">
							<slot name="bottom"></slot>
						</div>
					</div>
				</digi-layout-grid>
			</digi-typography>
		);
	}
}
