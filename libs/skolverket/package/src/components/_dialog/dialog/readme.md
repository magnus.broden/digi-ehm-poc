# digi-dialog

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute              | Description | Type      | Default |
| ------------------- | ---------------------- | ----------- | --------- | ------- |
| `afHideCloseButton` | `af-hide-close-button` |             | `boolean` | `false` |
| `afOpen`            | `af-open`              |             | `boolean` | `false` |


## Events

| Event       | Description | Type               |
| ----------- | ----------- | ------------------ |
| `afOnClose` |             | `CustomEvent<any>` |
| `afOnOpen`  |             | `CustomEvent<any>` |


## Methods

### `close() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `showModal() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"actions"` | Knappar                     |
| `"default"` | Kan innehålla vad som helst |
| `"heading"` | Rubrik                      |


## Dependencies

### Used by

 - [digi-notification-cookie](../../_notification/notification-cookie)

### Depends on

- [digi-util-detect-click-outside](../../../__core/_util/util-detect-click-outside)
- [digi-card-box](../../_card/card-box)
- [digi-typography](../../../__core/_typography/typography)
- [digi-icon](../../../__core/_icon/icon)

### Graph
```mermaid
graph TD;
  digi-dialog --> digi-util-detect-click-outside
  digi-dialog --> digi-card-box
  digi-dialog --> digi-typography
  digi-dialog --> digi-icon
  digi-notification-cookie --> digi-dialog
  style digi-dialog fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
