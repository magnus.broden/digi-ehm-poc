export enum ListLinkType {
	UNORDERED = 'ul',
	ORDERED = 'ol'
}
