export enum ListLinkLayout {
	BLOCK = 'block',
	INLINE = 'inline'
}
