# digi-list-link

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                                                     | Default                     |
| ------------- | -------------- | ----------- | -------------------------------------------------------- | --------------------------- |
| `afLayout`    | `af-layout`    |             | `ListLinkLayout.BLOCK \| ListLinkLayout.INLINE`          | `ListLinkLayout.BLOCK`      |
| `afType`      | `af-type`      |             | `ListLinkType.ORDERED \| ListLinkType.UNORDERED`         | `ListLinkType.UNORDERED`    |
| `afVariation` | `af-variation` |             | `ListLinkVariation.COMPACT \| ListLinkVariation.REGULAR` | `ListLinkVariation.REGULAR` |


## Slots

| Slot        | Description                                   |
| ----------- | --------------------------------------------- |
| `"default"` | ska innehålla li-element med a-element inuti. |
| `"heading"` | Rubrik                                        |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-list-link --> digi-typography
  style digi-list-link fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
