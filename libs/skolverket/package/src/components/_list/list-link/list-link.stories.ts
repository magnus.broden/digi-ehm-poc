import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ListLinkType } from './list-link-type.enum';
import { ListLinkLayout } from './list-link-layout.enum';
import { ListLinkVariation } from './list-link-variation.enum';

export default {
	title: 'list/digi-list-link',
	argTypes: {
		'af-type': enumSelect(ListLinkType),
		'af-layout': enumSelect(ListLinkLayout),
		'af-variation': enumSelect(ListLinkVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-list-link',
	'af-type': ListLinkType.UNORDERED,
	'af-layout': ListLinkLayout.BLOCK,
	'af-variation': ListLinkVariation.REGULAR,
	/* html */
	children: `
				<h3 slot="heading">En rubrik</h3>
        <li><a href="#">Om webbplatsen</a></li>
        <li><a href="#">Vår verksamhet</a></li>
        <li><a href="#">Organisation</a></li>
        <li><a href="#">Jobba hos oss</a></li>
    `
};
