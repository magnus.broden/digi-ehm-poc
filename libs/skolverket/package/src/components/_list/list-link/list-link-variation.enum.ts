export enum ListLinkVariation {
	REGULAR = 'regular',
	COMPACT = 'compact'
}
