import { Component, h, Prop } from '@stencil/core';
import { CardBoxWidth } from './card-box-width.enum';
import { CardBoxGutter } from './card-box-gutter.enum';

/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Skuggad box
 */
@Component({
	tag: 'digi-card-box',
	styleUrls: ['card-box.scss'],
	scoped: true
})
export class CardBox {
	@Prop() afWidth: CardBoxWidth = CardBoxWidth.REGULAR;
	@Prop() afGutter: CardBoxGutter = CardBoxGutter.REGULAR;

	get cssModifiers() {
		return {
			[`digi-card-box--width-${this.afWidth}`]: !!this.afWidth,
			[`digi-card-box--gutter-${this.afGutter}`]: !!this.afGutter
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-card-box': true,
					...this.cssModifiers
				}}
			>
				<slot></slot>
			</div>
		);
	}
}
