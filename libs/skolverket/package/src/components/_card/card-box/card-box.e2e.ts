import { newE2EPage } from '@stencil/core/testing';

describe('digi-card-box', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-card-box></digi-card-box>');

		const element = await page.find('digi-card-box');
		expect(element).toHaveClass('hydrated');
	});
});
