import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { CardBoxWidth } from './card-box-width.enum';
import { CardBoxGutter } from './card-box-gutter.enum';

export default {
	title: 'card/digi-card-box',
	argTypes: {
		'af-width': enumSelect(CardBoxWidth),
		'af-gutter': enumSelect(CardBoxGutter)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-card-box',
	'af-width': CardBoxWidth.REGULAR,
	'af-gutter': CardBoxGutter.REGULAR,
	/* html */
	children: `
        <digi-typography><p>hej hej</p></digi-typography>
    `
};

export const InsideAContainer = () =>
	`<digi-layout-container><digi-card-box><digi-typography><p>hej hej</p></digi-typography></digi-card-box></digi-layout-container>`;

export const InsideAGrid = () =>
	`<style>digi-card-box { grid-column: span calc(var(--digi--layout-grid--columns) / 2) }</style><digi-layout-grid><digi-card-box><digi-typography><p>hej hej</p></digi-typography></digi-card-box><digi-card-box><digi-typography><p>hej hej</p></digi-typography></digi-card-box></digi-layout-grid>`;

export const DeepNestingInsideAGrid = () =>
	`<digi-layout-grid><div style="grid-column: span 3;"><digi-card-box><digi-typography><p>hej hej</p></digi-typography></digi-card-box></div></digi-layout-grid>`;

const placeholderBlock = (
	text = ''
) => `<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)">
<digi-typography>
  <p>${text}</p>
</digi-typography>
</div>
`;
export const WithStackedBlocksInside = () =>
	`<digi-layout-rows>
    <digi-layout-container>
      <digi-card-box af-width="full">
        <digi-layout-stacked-blocks>
          ${placeholderBlock('Block 1')}
          ${placeholderBlock('Block 2')}
          ${placeholderBlock('Block 3')}
          ${placeholderBlock('Block 4')}
          ${placeholderBlock('Block 5')}
          ${placeholderBlock('Block 6')}
        </digi-layout-stacked-blocks>
      </digi-card-box>
    </digi-layout-container>
    <digi-layout-grid>
      <div style="grid-column: span 4;">${placeholderBlock('Block 1')}</div>
      <div style="grid-column: span 4;">${placeholderBlock('Block 2')}</div>
      <div style="grid-column: span 4;">${placeholderBlock('Block 3')}</div>
      <div style="grid-column: span 3;">${placeholderBlock('Block 4')}</div>
      <div style="grid-column: span 3;">${placeholderBlock('Block 5')}</div>
      <div style="grid-column: span 3;">${placeholderBlock('Block 6')}</div>
      <div style="grid-column: span 3;">${placeholderBlock('Block 7')}</div>
      <div style="grid-column: span 3;">${placeholderBlock('Block 8')}</div>
      <div style="grid-column: span 3;">${placeholderBlock('Block 9')}</div>
      <div style="grid-column: span 3;">${placeholderBlock('Block 10')}</div>
    </digi-layout-grid>
  </digi-layout-rows>`;

export const FullWidthInsideAContainer = () =>
	`<digi-layout-container><digi-card-box af-width="full"><digi-typography><p>hej hej</p></digi-typography></digi-card-box></digi-layout-container>`;
