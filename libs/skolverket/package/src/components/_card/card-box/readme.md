# digi-card-box

<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type                                          | Default                 |
| ---------- | ----------- | ----------- | --------------------------------------------- | ----------------------- |
| `afGutter` | `af-gutter` |             | `CardBoxGutter.NONE \| CardBoxGutter.REGULAR` | `CardBoxGutter.REGULAR` |
| `afWidth`  | `af-width`  |             | `CardBoxWidth.FULL \| CardBoxWidth.REGULAR`   | `CardBoxWidth.REGULAR`  |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-dialog](../../_dialog/dialog)
 - [digi-navigation-tab-in-a-box](../../_navigation/navigation-tab-in-a-box)
 - [digi-page-block-cards](../../_page/page-block-cards)
 - [digi-page-block-lists](../../_page/page-block-lists)
 - [digi-table](../../_table/table)

### Graph
```mermaid
graph TD;
  digi-dialog --> digi-card-box
  digi-navigation-tab-in-a-box --> digi-card-box
  digi-page-block-cards --> digi-card-box
  digi-page-block-lists --> digi-card-box
  digi-table --> digi-card-box
  style digi-card-box fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
