# digi-card-link

<!-- Auto Generated Below -->


## Slots

| Slot             | Description                            |
| ---------------- | -------------------------------------- |
| `"default"`      | kan innehålla vad som helst            |
| `"image"`        | Bild                                   |
| `"link-heading"` | Ska innehålla en rubrik med länk inuti |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-card-link --> digi-typography
  style digi-card-link fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
