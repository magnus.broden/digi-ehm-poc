import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'card/digi-card-link'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-card-link',
	/* html */
	children: `
  <img slot="image" src="https://picsum.photos/id/237/392/220" width="392" height="220" />
    <h2 slot="link-heading"><a href="#">Beredskap vid ett förändrat omvärldsläge</a></h2>
    <p>Förskola och skola är samhällsviktig verksamhet som behöver fungera även vid olika typer av kriser. Här finns stöd för hur du kan förbereda din verksamhet vid olika händelser av kris.</p>
    `
};

export const InALayout = () => /* html */ `
  <digi-layout-container>
    <digi-layout-stacked-blocks>
      <digi-card-link>
        <img slot="image" src="https://picsum.photos/id/237/392/220" width="392" height="220" />
        <h2 slot="link-heading"><a href="#">Beredskap vid ett förändrat omvärldsläge</a></h2>
    <p>Förskola och skola är samhällsviktig verksamhet som behöver fungera även vid olika typer av kriser. Här finns stöd för hur du kan förbereda din verksamhet vid olika händelser av kris.</p>
      </digi-card-link>
      <digi-card-link>
        <img slot="image" src="https://picsum.photos/id/1025/392/220" width="392" height="220" />
        <h2 slot="link-heading"><a href="#" slot="link-heading">Nyanlända barn och elever från Ukraina</a></h2>
        <p>Här hittar du som tar emot barn och elever från Ukraina information vad som gäller, det stöd vi erbjuder för mottagande och kartläggning och om skolsystemet och de olika skolformerna på engelska.</p>
      </digi-card-link>
      <digi-card-link>
        <h2 slot="link-heading"><a href="#">Ett kort utan bild</a></h2>
        <p>Etiam porta sem malesuada magna mollis euismod.</p>
        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
      </digi-card-link>
      <digi-card-link>
        <img slot="image" src="https://picsum.photos/id/1062/392/220" width="392" height="220" />
        <h2 slot="link-heading"><a href="#">Och så en länkad rubrik</a></h2>
      </digi-card-link>
      <digi-card-link>
        <div slot="image"></div>
        <h2 slot="link-heading"><a href="#">Och så en länkad rubrik</a></h2>
        <p>Lägg in nåt tomt element i slot="image" så får du en automatisk placeholder</p>
      </digi-card-link>
    </digi-layout-stacked-blocks>
  </digi-layout-container>
`;
