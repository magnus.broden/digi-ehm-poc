import { Component, Event, EventEmitter, Prop, h } from '@stencil/core';
import { NotificationAlertVariation } from './notification-alert-variation.enum';
import { ButtonVariation, TypographyVariation } from '../../../enums-core';
import { _t } from '@digi/skolverket/text';

/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Rubrik
 * @slot link - Länk
 * @slot actions - Knappar
 *
 * @swedishName Informationsmeddelande
 */
@Component({
	tag: 'digi-notification-alert',
	styleUrls: ['notification-alert.scss'],
	scoped: true
})
export class NotificationAlert {
	/**
	 * Sätter variant.
	 * @en Set variation of alert notification. Can be 'info', 'success', 'warning' or 'danger'.
	 */
	@Prop() afVariation: `${NotificationAlertVariation}` =
		NotificationAlertVariation.INFO;

	/**
	 * Gör det möjligt att stänga meddelandet med en stängknapp
	 * @en If true, the close button is added to the component
	 */
	@Prop() afCloseable: boolean = false;

	/**
	 * Stängknappens 'onclick'-event
	 * @en Close button's 'onclick' event
	 */
	@Event() afOnClose: EventEmitter<MouseEvent>;

	clickHandler(e) {
		this.afOnClose.emit(e);
	}

	get cssModifiers() {
		return {
			[`digi-notification-alert--variation-${this.afVariation}`]:
				!!this.afVariation,
			'digi-notification-alert--closeable': this.afCloseable
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-notification-alert': true,
					...this.cssModifiers
				}}
			>
				<div class="digi-notification-alert__inner">
					<div class="digi-notification-alert__icon">
						<digi-icon
							afName={`notification-${this.afVariation}`}
							aria-hidden="true"
						/>
					</div>
					{this.afCloseable && (
						<digi-button
							af-variation={ButtonVariation.FUNCTION}
							onAfOnClick={(e) => this.clickHandler(e)}
							class="digi-notification-alert__close-button"
						>
							<span class="digi-notification-alert__close-button__text">
								{_t.close}
							</span>
							<digi-icon
								afName={`close`}
								slot="icon-secondary"
								aria-hidden="true"
							></digi-icon>
						</digi-button>
					)}
					<div class="digi-notification-alert__content">
						<slot name="heading" />
						<digi-typography
							class="digi-notification-alert__text"
							af-variation={TypographyVariation.SMALL}
						>
							<slot />
						</digi-typography>
						<slot name="link" />
					</div>
					<div class="digi-notification-alert__actions">
						<slot name="actions" />
					</div>
				</div>
			</div>
		);
	}
}
