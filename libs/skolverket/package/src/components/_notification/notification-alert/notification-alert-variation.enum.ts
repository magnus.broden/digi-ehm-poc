export enum NotificationAlertVariation {
	INFO = 'info',
	WARNING = 'warning',
	DANGER = 'danger'
}
