import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'notification/digi-notification-cookie',
	parameters: {
		actions: {
			handles: ['afOnAcceptAllCookies', 'afOnSubmitSettings']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-notification-cookie',
	/* html */
	children: `
		<a href="#" slot="link">Läs mer i vår cookie policy</a>
		<digi-form-checkbox
			slot="settings"
			af-label="Analytiska cookies"
			af-description="Ger oss information om hur vår webbplats används som gör att vi kan underhålla, driva och förbättra användarupplevelsen."
			af-layout="block"
			af-name="cookie"
			af-value="analytical"
		></digi-form-checkbox>
		<digi-form-checkbox
			slot="settings"
			af-label="Ännu fler cookies"
			af-description="Ger oss information om hur vår webbplats används som gör att vi kan underhålla, driva och förbättra användarupplevelsen."
			af-layout="block"
			af-name="cookie"
			af-value="more"
		></digi-form-checkbox>`
};
