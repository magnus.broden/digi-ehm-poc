# digi-notification-cookie

<!-- Auto Generated Below -->


## Properties

| Property                | Attribute                  | Description                                                           | Type     | Default                                         |
| ----------------------- | -------------------------- | --------------------------------------------------------------------- | -------- | ----------------------------------------------- |
| `afBannerHeadingText`   | `af-banner-heading-text`   |                                                                       | `string` | `_t.notification.cookie_banner_heading`         |
| `afBannerText`          | `af-banner-text`           |                                                                       | `string` | `_t.notification.cookie_banner_text`            |
| `afId`                  | `af-id`                    | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-notification-cookie')` |
| `afModalHeadingText`    | `af-modal-heading-text`    |                                                                       | `string` | `_t.notification.cookie_modal_heading`          |
| `afRequiredCookiesText` | `af-required-cookies-text` |                                                                       | `string` | `_t.notification.cookie_modal_heading`          |


## Events

| Event                  | Description                         | Type                      |
| ---------------------- | ----------------------------------- | ------------------------- |
| `afOnAcceptAllCookies` | När användaren godkänner alla kakor | `CustomEvent<MouseEvent>` |
| `afOnSubmitSettings`   | När användaren godkänner alla kakor | `CustomEvent<any>`        |


## Slots

| Slot         | Description                                      |
| ------------ | ------------------------------------------------ |
| `"default"`  | Kan innehålla vad som helst                      |
| `"link"`     | Länk tiill exempel till läs-mer-om-cookies-sidan |
| `"settings"` | Formulärselement för olika cookieinställningar   |


## CSS Custom Properties

| Name                                                     | Description                                           |
| -------------------------------------------------------- | ----------------------------------------------------- |
| `--digi--notification-cookie--background-color--danger`  | var(--digi--color--background--notification-danger);  |
| `--digi--notification-cookie--background-color--info`    | var(--digi--color--background--notification-info);    |
| `--digi--notification-cookie--background-color--warning` | var(--digi--color--background--notification-warning); |
| `--digi--notification-cookie--border-color--danger`      | var(--digi--color--border--danger);                   |
| `--digi--notification-cookie--border-color--info`        | var(--digi--color--border--informative);              |
| `--digi--notification-cookie--border-color--warning`     | var(--digi--color--border--warning);                  |


## Dependencies

### Depends on

- [digi-notification-alert](../notification-alert)
- [digi-button](../../../__core/_button/button)
- [digi-dialog](../../_dialog/dialog)
- [digi-form-fieldset](../../../__core/_form/form-fieldset)

### Graph
```mermaid
graph TD;
  digi-notification-cookie --> digi-notification-alert
  digi-notification-cookie --> digi-button
  digi-notification-cookie --> digi-dialog
  digi-notification-cookie --> digi-form-fieldset
  digi-notification-alert --> digi-icon
  digi-notification-alert --> digi-button
  digi-notification-alert --> digi-typography
  digi-dialog --> digi-util-detect-click-outside
  digi-dialog --> digi-card-box
  digi-dialog --> digi-typography
  digi-dialog --> digi-icon
  style digi-notification-cookie fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
