import { Component, Prop, h } from '@stencil/core';
import { NotificationDetailVariation } from './notification-detail-variation.enum';
import { TypographyVariation } from '../../../enums-core';

/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Ska innehålla en rubrik
 *
 * @swedishName Detaljmeddelande
 */
@Component({
	tag: 'digi-notification-detail',
	styleUrls: ['notification-detail.scss'],
	scoped: true
})
export class NotificationDetail {
	/**
	 * Sätter variant.
	 * @en Set the variation.
	 */
	@Prop() afVariation: `${NotificationDetailVariation}` =
		NotificationDetailVariation.INFO;

	get cssModifiers() {
		return {
			[`digi-notification-detail--variation-${this.afVariation}`]:
				!!this.afVariation
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-notification-detail': true,
					...this.cssModifiers
				}}
			>
				<div class="digi-notification-detail__inner">
					<div class="digi-notification-detail__icon">
						<digi-icon
							afName={`notification-${this.afVariation}`}
							aria-hidden="true"
						/>
					</div>
					<div class="digi-notification-detail__content">
						<slot name="heading" />
						<digi-typography
							class="digi-notification-detail__text"
							af-variation={TypographyVariation.SMALL}
						>
							<slot />
						</digi-typography>
					</div>
				</div>
			</div>
		);
	}
}
