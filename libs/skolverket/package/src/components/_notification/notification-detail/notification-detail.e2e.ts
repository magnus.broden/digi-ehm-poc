import { newE2EPage } from '@stencil/core/testing';

describe('digi-notification-detail', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent(
			'<digi-notification-detail></digi-notification-detail>'
		);

		const element = await page.find('digi-notification-detail');
		expect(element).toHaveClass('hydrated');
	});
});
