import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'link/digi-link-icon'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-link-icon',
	children: /*html */ `<a href="#"><digi-icon af-name="search" aria-hidden="true"></digi-icon>Sök</a>`
};
