# digi-link-icon

<!-- Auto Generated Below -->


## Slots

| Slot        | Description                                 |
| ----------- | ------------------------------------------- |
| `"default"` | Ska vara en länk med en ikon och text inuti |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
