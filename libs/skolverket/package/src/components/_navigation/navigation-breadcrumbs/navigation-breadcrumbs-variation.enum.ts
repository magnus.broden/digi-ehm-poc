export enum NavigationBreadcrumbsVariation {
	REGULAR = 'regular',
	COMPRESSED = 'compressed'
}
