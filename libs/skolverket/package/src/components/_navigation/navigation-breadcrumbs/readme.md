# digi-navigation-breadcrumbs

This is a navigational breadcrumb component receiving links through a slot. The slot expects a-elements with a href attribute and text-content.

The component will manipulate and rerender the sloted elements wrapping them within a li-element. The `af-current-page` prop will be displayed as the current page.

When the breadcrumbs-list state changes the list will get updated through componentWillUpdate.

## INFO

In order to create a custom routing it is up to the application to prevent the click through the `afOnClick` event.

<!-- Auto Generated Below -->


## Properties

| Property                     | Attribute         | Description                                     | Type                                                                                  | Default                                  |
| ---------------------------- | ----------------- | ----------------------------------------------- | ------------------------------------------------------------------------------------- | ---------------------------------------- |
| `afAriaLabel`                | `af-aria-label`   | Sätter attributet 'aria-label' på navelementet. | `string`                                                                              | `_t.breadcrumbs`                         |
| `afCurrentPage` _(required)_ | `af-current-page` | Sätter texten för den aktiva sidan              | `string`                                                                              | `undefined`                              |
| `afVariation`                | `af-variation`    | Sätter texten för den aktiva sidan              | `NavigationBreadcrumbsVariation.COMPRESSED \| NavigationBreadcrumbsVariation.REGULAR` | `NavigationBreadcrumbsVariation.REGULAR` |


## Slots

| Slot        | Description                                  |
| ----------- | -------------------------------------------- |
| `"default"` | Ska inehålla li-element med a-element inuti. |


## CSS Custom Properties

| Name                                                                  | Description                                                     |
| --------------------------------------------------------------------- | --------------------------------------------------------------- |
| `--digi--navigation-breadcrumbs--divider--padding`                    | var(--digi--gutter--icon);                                      |
| `--digi--navigation-breadcrumbs--item--color`                         | var(--digi--color--text--primary);                              |
| `--digi--navigation-breadcrumbs--item--font-family`                   | var(--digi--global--typography--font-family--default);          |
| `--digi--navigation-breadcrumbs--item--font-size`                     | var(--digi--typography--breadcrumbs--font-size--desktop);       |
| `--digi--navigation-breadcrumbs--item--text-decoration`               | var(--digi--typography--breadcrumbs--text-decoration--desktop); |
| `--digi--navigation-breadcrumbs--items--background`                   | var(--digi--color--background--primary);                        |
| `--digi--navigation-breadcrumbs--items--display`                      | inline-flex;                                                    |
| `--digi--navigation-breadcrumbs--items--flex-wrap`                    | wrap;                                                           |
| `--digi--navigation-breadcrumbs--items--margin`                       | 0;                                                              |
| `--digi--navigation-breadcrumbs--items--padding`                      | var(--digi--gutter--icon);                                      |
| `--digi--navigation-breadcrumbs--link--color`                         | var(--digi--color--text--link);                                 |
| `--digi--navigation-breadcrumbs--link--hover--focus--color`           | var(--digi--color--text--link-hover);                           |
| `--digi--navigation-breadcrumbs--link--hover--focus--text-decoration` | underline;                                                      |
| `--digi--navigation-breadcrumbs--link--visited--color`                | var(--digi--color--text--link-visited);                         |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
