# digi-navigation-tabs

<!-- Auto Generated Below -->


## Properties

| Property          | Attribute            | Description                                         | Type     | Default                                     |
| ----------------- | -------------------- | --------------------------------------------------- | -------- | ------------------------------------------- |
| `afAriaLabel`     | `af-aria-label`      | Sätter attributet 'aria-label' på tablistelementet. | `string` | `''`                                        |
| `afId`            | `af-id`              | Input id attribute. Defaults to random string.      | `string` | `randomIdGenerator('digi-navigation-tabs')` |
| `afInitActiveTab` | `af-init-active-tab` | Sätter initial aktiv tabb                           | `number` | `0`                                         |


## Events

| Event        | Description | Type                      |
| ------------ | ----------- | ------------------------- |
| `afOnChange` |             | `CustomEvent<any>`        |
| `afOnClick`  |             | `CustomEvent<MouseEvent>` |
| `afOnFocus`  |             | `CustomEvent<any>`        |


## Methods

### `afMSetActiveTab(tabIndex: number) => Promise<void>`

Sätter om aktiv flik.

#### Returns

Type: `Promise<void>`




## Slots

| Slot        | Description                               |
| ----------- | ----------------------------------------- |
| `"default"` | Ska innehålla flera digi-navigation-tab-* |


## Dependencies

### Depends on

- [digi-util-keydown-handler](../../../__core/_util/util-keydown-handler)
- [digi-util-mutation-observer](../../../__core/_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-navigation-tabs --> digi-util-keydown-handler
  digi-navigation-tabs --> digi-util-mutation-observer
  style digi-navigation-tabs fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
