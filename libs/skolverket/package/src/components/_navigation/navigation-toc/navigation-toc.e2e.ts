import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-toc', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-navigation-toc></digi-navigation-toc>');

		const element = await page.find('digi-navigation-toc');
		expect(element).toHaveClass('hydrated');
	});
});
