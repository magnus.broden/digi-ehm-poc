# digi-navigation-toc

<!-- Auto Generated Below -->


## Slots

| Slot        | Description                                  |
| ----------- | -------------------------------------------- |
| `"default"` | Ska inehålla li-element med a-element inuti. |
| `"heading"` | Rubrik                                       |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-navigation-toc --> digi-typography
  style digi-navigation-toc fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
