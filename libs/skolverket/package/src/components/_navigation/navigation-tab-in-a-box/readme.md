# digi-navigation-tab-in-a-box

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute       | Description                                                                                      | Type      | Default     |
| -------------------------- | --------------- | ------------------------------------------------------------------------------------------------ | --------- | ----------- |
| `afActive`                 | `af-active`     | Sätter aktiv tabb. Detta sköts av digi-navigation-tab-in-a-boxs som ska omsluta denna komponent. | `boolean` | `undefined` |
| `afAriaLabel` _(required)_ | `af-aria-label` | Sätter attributet 'aria-label'                                                                   | `string`  | `undefined` |
| `afId`                     | `af-id`         | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                            | `string`  | `undefined` |


## Events

| Event        | Description                                | Type                   |
| ------------ | ------------------------------------------ | ---------------------- |
| `afOnToggle` | När tabben växlar mellan aktiv och inaktiv | `CustomEvent<boolean>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## Dependencies

### Depends on

- [digi-navigation-tab](../navigation-tab)
- [digi-card-box](../../_card/card-box)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-navigation-tab-in-a-box --> digi-navigation-tab
  digi-navigation-tab-in-a-box --> digi-card-box
  digi-navigation-tab-in-a-box --> digi-typography
  style digi-navigation-tab-in-a-box fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
