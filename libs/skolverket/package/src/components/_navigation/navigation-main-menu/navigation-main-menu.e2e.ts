import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-main-menu', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent(
			'<digi-navigation-main-menu></digi-navigation-main-menu>'
		);

		const element = await page.find('digi-navigation-main-menu');
		expect(element).toHaveClass('hydrated');
	});
});
