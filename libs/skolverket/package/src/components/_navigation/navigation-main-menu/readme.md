# digi-navigation-main-menu

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                           | Type     | Default                                          |
| -------- | --------- | --------------------------------------------------------------------- | -------- | ------------------------------------------------ |
| `afId`   | `af-id`   | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-navigation-main-menu')` |


## Events

| Event       | Description | Type               |
| ----------- | ----------- | ------------------ |
| `afOnRoute` |             | `CustomEvent<any>` |


## Slots

| Slot        | Description                      |
| ----------- | -------------------------------- |
| `"default"` | Se översikten för exakt innehåll |


## Dependencies

### Used by

 - [digi-page-header](../../_page/page-header)

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)

### Graph
```mermaid
graph TD;
  digi-navigation-main-menu --> digi-layout-container
  digi-page-header --> digi-navigation-main-menu
  style digi-navigation-main-menu fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
