import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NavigationMainMenuVariation } from './navigation-main-menu-variation.enum';
import { NavigationMainMenuActiveIndicatorSize } from './navigation-main-menu-active-indicator-size.enum';

export default {
	title: 'navigation/digi-navigation-main-menu',
	argTypes: {
		'af-variation': enumSelect(NavigationMainMenuVariation),
		'af-active-indicator-size': enumSelect(NavigationMainMenuActiveIndicatorSize)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'div',
	'af-id': null,
	/* html */
	children: `
  <digi-navigation-main-menu id="the-main-menu">
    <nav>
      <ul>
        <li>
          <button>Huvudingång 1</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 1</a>
            <ul>
              <li>
                <a href="#">Ingång 1 nivå 2.1</a>
              </li>
              <li>
                <a href="#">Ingång 1 nivå 2.2</a>
                <ul>
                  <li>
                    <a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
                  </li>
                  <li>
                    <a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
                  </li>
                  <li>
                    <a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
                  </li>
                </ul>
              </li>
            </ul>
          </digi-navigation-main-menu-panel>
        </li>
        <li>
          <button>Huvudingång 2</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 2</a>
            <ul>
              <li>
                <a href="#">Ingång 2 nivå 2.1</a>
              </li>
              <li>
                <a href="#">Ingång 2 nivå 2.2</a>
                <ul>
                  <li>
                    <a href="#">Ingång 2 nivå 2.2 nivå 3.1</a>
                  </li>
                  <li>
                    <a href="#">Ingång 2 nivå 2.2 nivå 3.2</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#">Ingång 2 nivå 2.3</a>
              </li>
            </ul>
          </digi-navigation-main-menu-panel>
        </li>
        <li>
          <button>Huvudingång 3</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 3</a>
            <ul>
              <li>
                <a href="#">Ingång 3 nivå 2.1</a>
              </li>
              <li>
                <a href="#">Ingång 3 nivå 2.2</a>
                <ul>
                  <li>
                    <a href="#">Ingång 3 nivå 2.2 nivå 3.1</a>
                  </li>
                  <li>
                    <a href="#">Ingång 3 nivå 2.2 nivå 3.2</a>
                  </li>
                </ul>
              </li>
            </ul>
          </digi-navigation-main-menu-panel>
        </li>
        <li><a href="#">Huvudingång 4</a></li>
        <li>
          <button>Huvudingång 5</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 5</a>
            <ul>
              <li>
                <a href="#">Ingång 5 nivå 2.1</a>
              </li>
            </ul>
          </digi-navigation-main-menu-panel>
        </li>
      </ul>
    </nav>
  </digi-navigation-main-menu>
  <p>Jag är under</p>
  <script>
    const links = document.querySelectorAll('digi-navigation-main-menu-panel a');
    links.forEach(link => link.addEventListener('click', (e) => {
      e.preventDefault();
      const currentPage = document.querySelector('digi-navigation-main-menu-panel a[aria-current="page"]');
      if (currentPage) {
        currentPage.removeAttribute('aria-current');
      }
      link.setAttribute('aria-current', 'page');
    }));
  </script>
    `
};
