export enum NavigationMainMenuActiveIndicatorSize {
	PRIMARY = 'primary',
	SECONDARY = 'secondary'
}
