# digi-navigation-pagination

<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                 | Description                                                                                                                                      | Type     | Default                                           |
| ---------------------- | ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | -------- | ------------------------------------------------- |
| `afCurrentResultEnd`   | `af-current-result-end`   | Sätter slutvärdet för nuvarande resultat                                                                                                         | `number` | `undefined`                                       |
| `afCurrentResultStart` | `af-current-result-start` | Sätter startvärdet för nuvarande resultat                                                                                                        | `number` | `undefined`                                       |
| `afId`                 | `af-id`                   | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                                            | `string` | `randomIdGenerator('digi-navigation-pagination')` |
| `afInitActivePage`     | `af-init-active-page`     | Sätter initiala aktiva sidan                                                                                                                     | `number` | `1`                                               |
| `afResultName`         | `af-result-name`          | Sätter ett resultatnamn. Det används för att kommunicera vad som listas. T.ex. om värdet är 'användare' så kan det stå 'Visar 23-138 användare'. | `string` | `_t.hits.toLowerCase()`                           |
| `afTotalPages`         | `af-total-pages`          | Sätter totala mängden sidor                                                                                                                      | `number` | `undefined`                                       |
| `afTotalResults`       | `af-total-results`        | Sätter totala mängden resultat                                                                                                                   | `number` | `0`                                               |


## Events

| Event            | Description      | Type                  |
| ---------------- | ---------------- | --------------------- |
| `afOnPageChange` | Vid byte av sida | `CustomEvent<number>` |


## Methods

### `afMSetCurrentPage(pageNumber: number) => Promise<void>`

Kan användas för att manuellt sätta om den aktiva sidan.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)
- [digi-form-select](../../../__core/_form/form-select)
- [digi-button](../../../__core/_button/button)
- [digi-icon](../../../__core/_icon/icon)

### Graph
```mermaid
graph TD;
  digi-navigation-pagination --> digi-typography
  digi-navigation-pagination --> digi-form-select
  digi-navigation-pagination --> digi-button
  digi-navigation-pagination --> digi-icon
  digi-form-select --> digi-form-label
  digi-form-select --> digi-icon
  digi-form-select --> digi-util-mutation-observer
  digi-form-select --> digi-form-validation-message
  digi-form-validation-message --> digi-icon
  style digi-navigation-pagination fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
