import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-main-menu-panel'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-typography',
	'af-id': null,
	/* html */
	children: `
    <p>Jag är en undermeny för digi-navigation-main-menu. Se där för implementationsdetaljer</p>
    `
};
