import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'typography/digi-typography-heading-section'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-typography-heading-section',
	'af-variation': 'start',
	/* html */
	children: `
    <h2>En sektionsrubrik</h2>
  `
};
