# digi-form-file-upload

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute                      | Description                                                                       | Type                                                                                                                                                                                                 | Default                                      |
| -------------------------- | ------------------------------ | --------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| `afAnnounceIfOptional`     | `af-announce-if-optional`      | Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria. | `boolean`                                                                                                                                                                                            | `false`                                      |
| `afAnnounceIfOptionalText` | `af-announce-if-optional-text` | Sätter text för afAnnounceIfOptional.                                             | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileMaxSize`            | `af-file-max-size`             | Sätter en maximal filstorlek i MB, 10MB är standard.                              | `number`                                                                                                                                                                                             | `10`                                         |
| `afFileTypes` _(required)_ | `af-file-types`                | Sätter attributet 'accept'. Använd för att limitera accepterade filtyper          | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afHeadingFiles`           | `af-heading-files`             | Sätt rubrik för uppladdade filer.                                                 | `string`                                                                                                                                                                                             | `_t.form.file_uploaded_files`                |
| `afHeadingLevel`           | `af-heading-level`             | Sätt rubrikens vikt. 'h2' är förvalt.                                             | `FormFileUploadHeadingLevel.H1 \| FormFileUploadHeadingLevel.H2 \| FormFileUploadHeadingLevel.H3 \| FormFileUploadHeadingLevel.H4 \| FormFileUploadHeadingLevel.H5 \| FormFileUploadHeadingLevel.H6` | `FormFileUploadHeadingLevel.H2`              |
| `afId`                     | `af-id`                        | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.             | `string`                                                                                                                                                                                             | `randomIdGenerator('digi-form-file-upload')` |
| `afLabel`                  | `af-label`                     | Texten till labelelementet                                                        | `string`                                                                                                                                                                                             | `_t.form.file_upload`                        |
| `afLabelDescription`       | `af-label-description`         | Valfri beskrivande text                                                           | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afMaxFiles`               | `af-max-files`                 | Sätter maximalt antal filer man kan ladda upp                                     | `number`                                                                                                                                                                                             | `undefined`                                  |
| `afName`                   | `af-name`                      | Sätter attributet 'name'.                                                         | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afRequired`               | `af-required`                  | Sätter attributet 'required'.                                                     | `boolean`                                                                                                                                                                                            | `undefined`                                  |
| `afRequiredText`           | `af-required-text`             | Sätter text för afRequired.                                                       | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afUploadBtnText`          | `af-upload-btn-text`           | Sätter ladda upp knappens text.                                                   | `string`                                                                                                                                                                                             | `_t.form.file_choose`                        |


## Events

| Event            | Description                                       | Type               |
| ---------------- | ------------------------------------------------- | ------------------ |
| `afOnCancelFile` | Sänder ut vilken fil som har avbrutis uppladdning | `CustomEvent<any>` |
| `afOnRemoveFile` | Sänder ut vilken fil som tagits bort              | `CustomEvent<any>` |
| `afOnRetryFile`  | Sänder ut vilken fil som försöker laddas upp igen | `CustomEvent<any>` |
| `afOnUploadFile` | Sänder ut fil vid uppladdning                     | `CustomEvent<any>` |


## Methods

### `afMGetAllFiles() => Promise<File[]>`

Få ut alla uppladdade filer

#### Returns

Type: `Promise<File[]>`



### `afMGetFormControlElement() => Promise<HTMLInputElement>`



#### Returns

Type: `Promise<HTMLInputElement>`



### `afMImportFiles(files: File[]) => Promise<void>`

Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt måste innehålla id, status och filen själv.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [digi-form-label](../../../__core/_form/form-label)
- [digi-icon](../../../__core/_icon/icon)
- [digi-button](../../../__core/_button/button)
- [digi-typography](../../../__core/_typography/typography)
- [digi-form-validation-message](../../../__core/_form/form-validation-message)
- [digi-icon-spinner](../../../__core/_icon/icon-spinner)
- [digi-icon-danger-outline](../../../__core/_icon/icon-danger-outline)

### Graph
```mermaid
graph TD;
  digi-form-file-upload --> digi-form-label
  digi-form-file-upload --> digi-icon
  digi-form-file-upload --> digi-button
  digi-form-file-upload --> digi-typography
  digi-form-file-upload --> digi-form-validation-message
  digi-form-file-upload --> digi-icon-spinner
  digi-form-file-upload --> digi-icon-danger-outline
  digi-form-validation-message --> digi-icon
  style digi-form-file-upload fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
