import { newSpecPage } from '@stencil/core/testing';
import { FormFileUpload } from './form-file-upload';

describe('form-file-upload', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [FormFileUpload],
			html: '<form-file-upload></form-file-upload>'
		});
		expect(root).toEqualHtml(`
      <form-file-upload>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </form-file-upload>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [FormFileUpload],
			html: `<form-file-upload first="Stencil" last="'Don't call me a framework' JS"></form-file-upload>`
		});
		expect(root).toEqualHtml(`
      <form-file-upload first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </form-file-upload>
    `);
	});
});
