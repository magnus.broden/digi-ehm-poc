import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'form/digi-form-process-steps',
	argTypes: {
		// 'af-vertical-spacing': enumSelect(FormProcessStepsVerticalSpacing)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-process-steps',
	'af-current-step': 3,
	/* html */
	children: `
    <li><digi-form-process-step af-label="Tillfällig lagring"></digi-form-process-step></li>
    <li><digi-form-process-step af-href="#" af-label="Användare"></digi-form-process-step></li>
    <li><digi-form-process-step af-label="Kontaktuppgifter"></digi-form-process-step></li>
    <li><digi-form-process-step af-label="Hantera ärende"></digi-form-process-step></li>
    <li><digi-form-process-step af-label="Granska och skicka"></digi-form-process-step></li>
  `
};
