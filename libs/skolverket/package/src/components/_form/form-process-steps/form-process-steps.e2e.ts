import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-process-steps', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-form-process-steps></digi-form-process-steps>');

		const element = await page.find('digi-form-process-steps');
		expect(element).toHaveClass('hydrated');
	});
});
