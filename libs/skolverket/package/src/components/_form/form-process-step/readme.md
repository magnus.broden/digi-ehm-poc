# digi-form-process-step

<!-- Auto Generated Below -->


## Properties

| Property               | Attribute    | Description | Type                                     | Default      |
| ---------------------- | ------------ | ----------- | ---------------------------------------- | ------------ |
| `afContext`            | `af-context` |             | `"fallback" \| "regular"`                | `'regular'`  |
| `afHref`               | `af-href`    |             | `string`                                 | `undefined`  |
| `afLabel` _(required)_ | `af-label`   |             | `string`                                 | `undefined`  |
| `afType`               | `af-type`    |             | `"completed" \| "current" \| "upcoming"` | `'upcoming'` |


## Events

| Event     | Description | Type                      |
| --------- | ----------- | ------------------------- |
| `afClick` |             | `CustomEvent<MouseEvent>` |


## Slots

| Slot        | Description                             |
| ----------- | --------------------------------------- |
| `"default"` | Ska innehålla flera <form-process-step> |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
