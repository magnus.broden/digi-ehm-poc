# digi-logo-service

<!-- Auto Generated Below -->


## Properties

| Property                     | Attribute        | Description                                                                                  | Type     | Default                                         |
| ---------------------------- | ---------------- | -------------------------------------------------------------------------------------------- | -------- | ----------------------------------------------- |
| `afDescription` _(required)_ | `af-description` | Tjänsten eller verktygets beskrivning                                                        | `string` | `undefined`                                     |
| `afName` _(required)_        | `af-name`        | Tjänsten eller verktygets namn                                                               | `string` | `undefined`                                     |
| `afNameId`                   | `af-name-id`     | Sätter attributet 'id' på namntextens element. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-logo-service__title')` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
