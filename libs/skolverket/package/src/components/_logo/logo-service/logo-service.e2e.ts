import { newE2EPage } from '@stencil/core/testing';

describe('digi-logo-service', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-logo-service></digi-logo-service>');

		const element = await page.find('digi-logo-service');
		expect(element).toHaveClass('hydrated');
	});
});
