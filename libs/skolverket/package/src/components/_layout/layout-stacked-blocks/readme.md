# digi-layout-stacked-blocks

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                      | Default                                |
| ------------- | -------------- | ----------- | ------------------------- | -------------------------------------- |
| `afVariation` | `af-variation` |             | `"enhanced" \| "regular"` | `LayoutStackedBlocksVariation.REGULAR` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-page-block-cards](../../_page/page-block-cards)
 - [digi-page-block-lists](../../_page/page-block-lists)

### Graph
```mermaid
graph TD;
  digi-page-block-cards --> digi-layout-stacked-blocks
  digi-page-block-lists --> digi-layout-stacked-blocks
  style digi-layout-stacked-blocks fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
