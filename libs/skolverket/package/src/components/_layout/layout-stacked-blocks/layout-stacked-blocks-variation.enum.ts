export enum LayoutStackedBlocksVariation {
	REGULAR = 'regular',
	ENHANCED = 'enhanced'
}
