import {
	enumSelect,
	parseArgs,
	Template
} from '../../../../../../shared/utils/src';
import { LayoutStackedBlocksVariation } from './layout-stacked-blocks-variation.enum';

export default {
	title: 'layout/digi-layout-stacked-blocks',
	argTypes: {
		'af-variation': enumSelect(LayoutStackedBlocksVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-layout-stacked-blocks',
	'af-variation': LayoutStackedBlocksVariation.REGULAR,
	/* html */
	children: `
    <div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>Layoutkomponent som placerar ut sina barn i jämna kolumner med en gutter mellan.</p></digi-typography></div>
    <div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>Vid stora skärmar blir det tre kolumner, lite mindre två kolumner och vid små en kolumn.</p></div>
    <div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>Ändrar man varianten till "ennhanced", så kommer första barnet att ta upp hela första raden.</p></div>
    <div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>Alla barnen på samma rad kommer stretchas ut så att de får samma höjd.</p></div>
  `
};

export const WithContainerWrapper = ({ children, ...args }) => `
    <digi-layout-container>
      <digi-layout-stacked-blocks ${parseArgs(args)}>
        ${children}
      </digi-layout-stacked-blocks>
    </digi-layout-container>`;

WithContainerWrapper.args = {
	'af-variation': LayoutStackedBlocksVariation.REGULAR,
	/* html */
	children: `
    <div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>Här är allt wrappat i komponenten "digi-layout-container" för att hamna fint positionerat på sidan.</p></digi-typography></div>
    <div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p></div>
    <div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus..</p></div>
    <div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus..</p></div>
  `
};
