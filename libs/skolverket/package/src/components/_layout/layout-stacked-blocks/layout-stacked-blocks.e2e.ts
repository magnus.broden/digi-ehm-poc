import { newE2EPage } from '@stencil/core/testing';

describe('digi-layout-stacked-blocks', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent(
			'<digi-layout-stacked-blocks></digi-layout-stacked-blocks>'
		);

		const element = await page.find('digi-layout-stacked-blocks');
		expect(element).toHaveClass('hydrated');
	});
});
