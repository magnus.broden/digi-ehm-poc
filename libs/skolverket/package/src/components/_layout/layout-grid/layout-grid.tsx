import { Component, h, Prop } from '@stencil/core';
import { LayoutGridVerticalSpacing } from './layout-grid-vertical-spacing.enum';

/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Basgrid
 */
@Component({
	tag: 'digi-layout-grid',
	styleUrls: ['layout-grid.scss'],
	scoped: true
})
export class LayoutGrid {
	@Prop() afVerticalSpacing: `${LayoutGridVerticalSpacing}` =
		LayoutGridVerticalSpacing.REGULAR;

	get cssModifiers() {
		return {
			[`digi-layout-grid--vertical-spacing-${this.afVerticalSpacing}`]:
				!!this.afVerticalSpacing
		};
	}

	render() {
		return (
			<digi-layout-container>
				<div
					class={{
						'digi-layout-grid': true,
						...this.cssModifiers
					}}
				>
					<slot></slot>
				</div>
			</digi-layout-container>
		);
	}
}
