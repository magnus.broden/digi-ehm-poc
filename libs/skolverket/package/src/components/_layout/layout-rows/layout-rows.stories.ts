import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'layout/digi-layout-rows'
};

const rows = `
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>1</p></digi-typography></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>2</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>3</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>4</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>5</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>6</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>7</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>8</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>9</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>10</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>11</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>12</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>13</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>14</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>15</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>16</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>17</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>18</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>19</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>20</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>21</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>22</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>23</p></div>
<div style="background: var(--digi--color--background--tertiary); padding: var(--digi--gutter--medium)"><digi-typography><p>24</p></div>
`;

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-layout-rows',
	children: rows
};

export const RowsInAGrid = () =>
	`<digi-layout-grid><digi-layout-rows style="grid-column: span calc(var(--digi--layout-grid--columns) / 2);">${rows}</digi-layout-rows><digi-layout-rows style="grid-column: span calc(var(--digi--layout-grid--columns) / 2);">${rows}</digi-layout-rows></digi-layout-grid>`;
