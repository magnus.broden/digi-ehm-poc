# digi-layout-rows

<!-- Auto Generated Below -->


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
