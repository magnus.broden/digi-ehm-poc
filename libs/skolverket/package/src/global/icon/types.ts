import { namesAsArray as coreNamesAsArray } from '../../../../../core/package/src/global/icon/types';

/**
 * TODO: Super-temporary solution. Replace with codegen from .svg files
 */
export const namesAsArray = [
	...new Set([
		...coreNamesAsArray,
		'accessibility-deaf',
		'bars',
		'notification-success',
		'check',
		'chevron-down',
		'chevron-left',
		'chevron-right',
		'chevron-up',
		'dislike',
		'notification-warning',
		'exclamation-triangle-warning',
		'globe',
		'notification-info',
		'lattlast',
		'like',
		'minus',
		'plus',
		'search',
		'sort',
		'x',
		'input-select-marker',
		'notification-warning',
		'notification-danger',
		'notification-info',
		'close',
		'notification-error',
		'validation-error',
		'validation-warning',
		'upload',
    'work',
    'info',
    'grading'
	])
];
