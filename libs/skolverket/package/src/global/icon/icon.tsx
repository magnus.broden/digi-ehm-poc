import { h } from '@stencil/core';

const coreIcon = {
	bars: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="22"
					height="26"
					viewBox="0 0 22 26"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M0.8,6.9h20.4c0.4,0,0.8-0.4,0.8-0.8v-2c0-0.4-0.4-0.8-0.8-0.8H0.8C0.4,3.4,0,3.7,0,4.2v2C0,6.6,0.4,6.9,0.8,6.9z M0.8,14.8 h20.4c0.4,0,0.8-0.4,0.8-0.8v-2c0-0.4-0.4-0.8-0.8-0.8H0.8C0.4,11.2,0,11.6,0,12v2C0,14.4,0.4,14.8,0.8,14.8z M0.8,22.6h20.4 c0.4,0,0.8-0.4,0.8-0.8v-2c0-0.4-0.4-0.8-0.8-0.8H0.8c-0.4,0-0.8,0.4-0.8,0.8v2C0,22.3,0.4,22.6,0.8,22.6z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	check: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="23"
					height="18"
					viewBox="0 0 23 18"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M20.547.18L7.315 13.627 2.453 8.686a.597.597 0 00-.853 0L.177 10.132a.62.62 0 000 .868l6.711 6.82c.236.24.618.24.854 0L22.823 2.493a.62.62 0 000-.867L21.4.18a.597.597 0 00-.853 0z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'validation-success': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="25"
					height="25"
					viewBox="0 0 25 25"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					xmlns="http://www.w3.org/2000/svg"
				>
					{children}
					<path
						d="M25 12.5C25 19.404 19.404 25 12.5 25S0 19.404 0 12.5 5.596 0 12.5 0 25 5.596 25 12.5zm-13.946 6.619l9.274-9.275a.806.806 0 000-1.14l-1.14-1.14a.806.806 0 00-1.14 0l-7.564 7.563-3.531-3.531a.807.807 0 00-1.14 0l-1.141 1.14a.806.806 0 000 1.14l5.242 5.243a.806.806 0 001.14 0z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'notification-success': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="25"
					height="25"
					viewBox="0 0 25 25"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M12.5 0C5.596 0 0 5.596 0 12.5S5.596 25 12.5 25 25 19.404 25 12.5 19.404 0 12.5 0zm0 2.42c5.571 0 10.08 4.508 10.08 10.08 0 5.571-4.508 10.08-10.08 10.08A10.075 10.075 0 012.42 12.5c0-5.571 4.508-10.08 10.08-10.08zm7.067 6.565L18.43 7.84a.605.605 0 00-.855-.003l-7.125 7.067-3.014-3.038a.605.605 0 00-.855-.004l-1.145 1.136a.605.605 0 00-.004.856l4.576 4.612a.605.605 0 00.855.004l8.7-8.63a.605.605 0 00.003-.855z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'check-circle-reg-alt': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="42"
					height="42"
					viewBox="0 0 42 42"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor" fill-rule="nonzero">
						<path d="M21 39c9.941 0 18-8.059 18-18S30.941 3 21 3 3 11.059 3 21s8.059 18 18 18zm0 3C9.402 42 0 32.598 0 21S9.402 0 21 0s21 9.402 21 21-9.402 21-21 21z" />
						<path d="M17.72 28.415a1.98 1.98 0 002.817-.007l8.905-9.09a1.945 1.945 0 00-.039-2.764 1.98 1.98 0 00-2.785.038l-7.41 7.765-3.833-3.889a1.98 1.98 0 00-2.786-.023 1.945 1.945 0 00-.024 2.765l5.156 5.205z" />
					</g>
				</svg>
			);
		}
	},
	'chevron-down': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="14"
					height="8"
					viewBox="0 0 14 8"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M1 1l6.074 6L13 1.147"
						stroke="currentColor"
						stroke-width="2"
						fill="none"
						fill-rule="evenodd"
					/>
				</svg>
			);
		}
	},
	'chevron-left': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="9"
					height="14"
					viewBox="0 0 9 14"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M8 13L2 6.926 7.853 1"
						stroke="currentColor"
						stroke-width="2"
						fill="none"
						fill-rule="evenodd"
					/>
				</svg>
			);
		}
	},
	'chevron-right': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="9"
					height="14"
					viewBox="0 0 9 14"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M1 13l6-6.074L1.147 1"
						stroke="currentColor"
						stroke-width="2"
						fill="none"
						fill-rule="evenodd"
					/>
				</svg>
			);
		}
	},
	'chevron-up': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="14"
					height="9"
					viewBox="0 0 14 9"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M1 8l6.074-6L13 7.853"
						stroke="currentColor"
						stroke-width="2"
						fill="none"
						fill-rule="evenodd"
					/>
				</svg>
			);
		}
	},
	copy: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="22"
					height="26"
					viewBox="0 0 22 26"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M15.714 22.75v2.031c0 .673-.527 1.219-1.178 1.219H1.179C.528 26 0 25.454 0 24.781V6.094c0-.673.528-1.219 1.179-1.219h3.535v15.031c0 1.568 1.234 2.844 2.75 2.844h8.25zm0-17.469V0h-8.25c-.65 0-1.178.546-1.178 1.219v18.687c0 .673.527 1.219 1.178 1.219h13.357c.651 0 1.179-.546 1.179-1.219V6.5h-5.107c-.648 0-1.179-.548-1.179-1.219zm5.94-1.575L18.418.356A1.16 1.16 0 0017.583 0h-.297v4.875H22v-.308a1.24 1.24 0 00-.345-.861z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'danger-outline': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="48"
					height="48"
					viewBox="0 0 48 48"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M16.3083126,13.5575621 C17.212521,12.6758465 18.6746453,12.6758465 19.4922806,13.5575621 L24.0133226,18.0230248 L28.6209379,13.5575621 C29.5251463,12.6758465 30.9872705,12.6758465 31.8049058,13.5575621 C32.7860681,14.4487585 32.7860681,15.889842 31.8049058,16.6957111 L27.3608176,21.151693 L31.8049058,25.6930023 C32.7860681,26.5841986 32.7860681,28.0252822 31.8049058,28.8311512 C30.9872705,29.7981941 29.5251463,29.7981941 28.6209379,28.8311512 L24.0133226,24.4510158 L19.4922806,28.8311512 C18.6746453,29.7981941 17.212521,29.7981941 16.3083126,28.8311512 C15.4137234,28.0252822 15.4137234,26.5841986 16.3083126,25.6930023 L20.8389739,21.151693 L16.3083126,16.6957111 C15.4137234,15.889842 15.4137234,14.4487585 16.3083126,13.5575621 L16.3083126,13.5575621 Z M9.87119353,3.24 C11.1166068,1.228125 13.3583507,0 15.6767354,0 L32.240732,0 C34.6453376,0 36.8870815,1.228125 38.1324948,3.24 L46.9940893,17.49375 C48.3353036,19.65 48.3353036,22.35 46.9940893,24.50625 L38.1324948,38.75625 C36.8870815,40.771875 34.6453376,42 32.240732,42 L15.6767354,42 C13.3583507,42 11.1166068,40.771875 9.87119353,38.75625 L1.00576702,24.50625 C-0.335255674,22.35 -0.335255674,19.65 1.00576702,17.49375 L9.87119353,3.24 Z M4.95376353,20.0519187 C4.50454509,20.7819413 4.50454509,21.6920993 4.95376353,22.4221219 L13.8554068,36.8329571 C14.2690341,37.5060948 15.0193347,37.9232506 15.7407776,37.9232506 L32.3724409,37.9232506 C33.1804569,37.9232506 33.9307575,37.5060948 34.3443848,36.8329571 L43.2421804,22.4221219 C43.6942846,21.6920993 43.6942846,20.7819413 43.2421804,20.0519187 L34.3443848,5.64297968 C33.9307575,4.96510158 33.1804569,4.55079007 32.3724409,4.55079007 L15.7407776,4.55079007 C15.0193347,4.55079007 14.2690341,4.96510158 13.8554068,5.64297968 L4.95376353,20.0519187 Z M38.2882725,3.27656885 L34.3443848,5.64297968 L38.2882725,3.27656885 Z M1.00987575,24.7828442 L4.95376353,22.4221219 L1.00987575,24.7828442 Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	download: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="48"
					height="48"
					viewBox="0 0 48 48"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M20.25,0 L27.75,0 C28.996875,0 30,1.003125 30,2.25 L30,18 L38.221875,18 C39.890625,18 40.725,20.015625 39.54375,21.196875 L25.284375,35.465625 C24.58125,36.16875 23.428125,36.16875 22.725,35.465625 L8.446875,21.196875 C7.265625,20.015625 8.1,18 9.76875,18 L18,18 L18,2.25 C18,1.003125 19.003125,0 20.25,0 Z M48,35.25 L48,45.75 C48,46.996875 46.996875,48 45.75,48 L2.25,48 C1.003125,48 0,46.996875 0,45.75 L0,35.25 C0,34.003125 1.003125,33 2.25,33 L16.003125,33 L20.596875,37.59375 C22.48125,39.478125 25.51875,39.478125 27.403125,37.59375 L31.996875,33 L45.75,33 C46.996875,33 48,34.003125 48,35.25 Z M36.375,43.5 C36.375,42.46875 35.53125,41.625 34.5,41.625 C33.46875,41.625 32.625,42.46875 32.625,43.5 C32.625,44.53125 33.46875,45.375 34.5,45.375 C35.53125,45.375 36.375,44.53125 36.375,43.5 Z M42.375,43.5 C42.375,42.46875 41.53125,41.625 40.5,41.625 C39.46875,41.625 38.625,42.46875 38.625,43.5 C38.625,44.53125 39.46875,45.375 40.5,45.375 C41.53125,45.375 42.375,44.53125 42.375,43.5 Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'notification-warning': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="42"
					height="42"
					viewBox="0 0 42 42"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M21 3C11.059 3 3 11.059 3 21s8.059 18 18 18 18-8.059 18-18S30.941 3 21 3zm0-3c11.598 0 21 9.402 21 21s-9.402 21-21 21S0 32.598 0 21 9.402 0 21 0z"
							fill-rule="nonzero"
						/>
						<path d="M25 10l-.875 11h-5.25L18 10zM21.5 31c-2.178 0-3.5-1.58-3.5-3.5s1.322-3.5 3.5-3.5 3.5 1.58 3.5 3.5-1.322 3.5-3.5 3.5z" />
					</g>
				</svg>
			);
		}
	},
	'exclamation-circle-filled': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="25"
					height="25"
					viewBox="0 0 25 25"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M25 12.5C25 19.405 19.403 25 12.5 25S0 19.405 0 12.5C0 5.599 5.597 0 12.5 0S25 5.599 25 12.5zm-12.5 2.52a2.319 2.319 0 100 4.637 2.319 2.319 0 000-4.637zm-2.201-8.334l.374 6.855c.017.32.282.572.604.572h2.446a.605.605 0 00.604-.572l.374-6.855a.605.605 0 00-.604-.638h-3.194a.605.605 0 00-.604.638z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'exclamation-triangle': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="44"
					height="38"
					viewBox="0 0 44 38"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M20.417 3.941L3.35 31.235s-.787 1.777 0 2.771S4.985 35 4.985 35h33.93s.846 0 1.718-.994c.826-.941 0-2.77 0-2.77L23.583 3.94C23.27 3.418 22.792 3 22 3c-.792 0-1.27.418-1.583.941zM22 0c1.853 0 3.283.943 4.128 2.352l17.156 27.465.083.184c.097.214.212.513.323.881A6.84 6.84 0 0144 32.92c-.01 1.113-.333 2.178-1.113 3.065C41.62 37.43 40.151 38 38.914 38H4.977c-.17-.005-.17-.005-.243-.01a4.78 4.78 0 01-1.237-.273 5.57 5.57 0 01-2.201-1.499 6.453 6.453 0 01-.298-.35c-.982-1.24-1.154-2.662-.9-4.132a7.384 7.384 0 01.509-1.716l.086-.195L17.861 2.37C18.735.93 20.159 0 22 0z"
							fill-rule="nonzero"
						/>
						<path d="M19 13l.75 11h4.5L25 13zM25 28c0-1.645-1.133-3-3-3s-3 3-3 3 1.133 3 3 3 3-1.355 3-3z" />
					</g>
				</svg>
			);
		}
	},
	'exclamation-triangle-warning': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="22"
					height="20"
					viewBox="0 0 22 20"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<defs>
						<path
							d="M9.5 8.38l.252 4.207a.455.455 0 00.458.424h1.58a.455.455 0 00.458-.424l.251-4.207a.454.454 0 00-.457-.477H9.958a.454.454 0 00-.457.477zm3.104 6.734c0 .872-.718 1.578-1.604 1.578-.886 0-1.604-.706-1.604-1.578 0-.87.718-1.577 1.604-1.577.886 0 1.604.706 1.604 1.577zm-.016-13.522c-.704-1.2-2.47-1.202-3.176 0L.247 17.218c-.703 1.2.178 2.704 1.588 2.704h18.33c1.407 0 2.292-1.502 1.587-2.704L12.588 1.592zM2.032 17.782l8.77-14.95a.231.231 0 01.397 0l8.77 14.95a.225.225 0 01-.2.337H2.23a.225.225 0 01-.198-.338z"
							id="iconExclamationTriangelWarningPath"
						/>
					</defs>
					<g fill="none" fill-rule="nonzero">
						<path
							d="M11.874 2.573l8.3 14.941A1 1 0 0119.3 19H2.7a1 1 0 01-.875-1.486l8.3-14.94a1 1 0 011.75 0z"
							fill="var(--digi--icon-exclamation-triangle-warning--background)"
						/>
						<g>
							<mask id="iconExclamationTriangelWarningPathB" fill="#FFF">
								<use href="#iconExclamationTriangelWarningPath" />
							</mask>
							<use
								fill="none"
								fill-rule="nonzero"
								href="#iconExclamationTriangelWarningPath"
							/>
							<g
								mask="url(#iconExclamationTriangelWarningPathB)"
								fill="var(--digi--icon-exclamation-triangle-warning--outline)"
							>
								<path d="M-1.571-2.308h25.143v24.615H-1.571z" />
							</g>
						</g>
					</g>
				</svg>
			);
		}
	},
	'external-link-alt': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="29"
					height="26"
					viewBox="0 0 29 26"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M22.556 12.28v11.283A2.427 2.427 0 0120.139 26H2.417A2.427 2.427 0 010 23.562V5.688A2.427 2.427 0 012.417 3.25h17.117c.538 0 .807.656.427 1.04L18.752 5.51a.602.602 0 01-.427.178H2.72a.303.303 0 00-.302.305v17.266c0 .168.135.305.302.305h17.118a.303.303 0 00.302-.305v-9.76c0-.16.064-.316.177-.43l1.208-1.219a.604.604 0 011.032.431zM28.396 0h-6.847c-.536 0-.807.657-.427 1.04l2.426 2.448L9.844 17.311a.613.613 0 000 .862l1.139 1.149a.6.6 0 00.854 0L25.542 5.499l2.427 2.447A.604.604 0 0029 7.516V.608C29 .273 28.73 0 28.396 0z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	minus: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="22"
					height="26"
					viewBox="0 0 22 26"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					xmlns="http://www.w3.org/2000/svg"
				>
					{children}
					<path
						d="M21.2,13c0,0.7-0.5,1.2-1.2,1.2H2c-0.7,0-1.2-0.5-1.2-1.2c0-0.6,0.5-1.2,1.2-1.2H20C20.7,11.8,21.2,12.4,21.2,13z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	paperclip: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="23"
					height="26"
					viewBox="0 0 23 26"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					xmlns="http://www.w3.org/2000/svg"
				>
					{children}
					<path
						d="M2.157 23.79c-2.876-2.947-2.876-7.715 0-10.662L13.31 1.7a5.586 5.586 0 018.026 0c2.218 2.273 2.219 5.951 0 8.224l-9.391 9.623a3.93 3.93 0 01-5.647 0 4.16 4.16 0 010-5.787l7.954-8.15a.585.585 0 01.84 0l.842.863a.62.62 0 010 .861l-7.954 8.15a1.682 1.682 0 000 2.339c.63.645 1.653.645 2.283 0L19.654 8.2a3.434 3.434 0 000-4.776 3.244 3.244 0 00-4.662 0L3.84 14.852c-1.946 1.993-1.946 5.22 0 7.214a4.895 4.895 0 007.047-.002c3.236-3.318 6.472-6.636 9.71-9.953a.585.585 0 01.84 0l.842.862a.62.62 0 010 .861 37929.73 37929.73 0 00-9.71 9.953 7.233 7.233 0 01-10.411.002z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	plus: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="22"
					height="26"
					viewBox="0 0 22 26"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					xmlns="http://www.w3.org/2000/svg"
				>
					{children}
					<path
						d="M21.6,13c0,0.7-0.5,1.2-1.2,1.2h-7.9V22c0,0.7-0.5,1.2-1.2,1.2s-1.2-0.5-1.2-1.2v-7.9H2.3c-0.7,0-1.2-0.5-1.2-1.2 c0-0.6,0.5-1.2,1.2-1.2h7.9V4c0-0.7,0.5-1.2,1.2-1.2s1.2,0.5,1.2,1.2v7.9h7.9C21.1,11.8,21.6,12.4,21.6,13z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	search: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="26"
					height="26"
					viewBox="0 0 26 26"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					xmlns="http://www.w3.org/2000/svg"
				>
					{children}
					<path
						d="M25.645 22.437l-5.063-5.053a1.22 1.22 0 00-.864-.355h-.827a10.477 10.477 0 002.234-6.487C21.125 4.719 16.397 0 10.562 0 4.729 0 0 4.719 0 10.542c0 5.823 4.728 10.542 10.563 10.542 2.452 0 4.707-.831 6.5-2.23v.826c0 .324.126.633.355.862l5.063 5.053c.477.476 1.25.476 1.721 0l1.437-1.435a1.22 1.22 0 00.006-1.723zm-15.082-5.408c-3.59 0-6.5-2.899-6.5-6.487a6.49 6.49 0 016.5-6.487c3.59 0 6.5 2.899 6.5 6.487a6.49 6.49 0 01-6.5 6.487z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	spinner: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					height="50"
					viewBox="0 0 50 50"
					width="50"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					xmlns="http://www.w3.org/2000/svg"
				>
					{children}
					<g fill="none" fill-rule="evenodd">
						<mask fill="#fff" id="iconSpinnerPath">
							<path d="M25 42c9.389 0 17-7.611 17-17S34.389 8 25 8 8 15.611 8 25s7.611 17 17 17zm0 8C11.193 50 0 38.807 0 25S11.193 0 25 0s25 11.193 25 25-11.193 25-25 25z" />
						</mask>
						<path
							d="M25 42c9.389 0 17-7.611 17-17S34.389 8 25 8 8 15.611 8 25s7.611 17 17 17zm0 8C11.193 50 0 38.807 0 25S11.193 0 25 0s25 11.193 25 25-11.193 25-25 25z"
							fill="#D0CFE1"
							fill-rule="nonzero"
						/>
						<path
							d="M19 26L54-5v62z"
							fill="currentColor"
							mask="url(#iconSpinnerPath)"
						/>
					</g>
				</svg>
			);
		}
	},
	trash: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="48"
					height="48"
					viewBox="0 0 48 48"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					xmlns="http://www.w3.org/2000/svg"
				>
					{children}
					<path
						d="M28.3065836,39.0000003 L30.4710557,39.0000003 C31.0687581,39.0000003 31.5532917,38.4963197 31.5532917,37.8749994 L31.5532917,17.6250003 C31.5532917,17.00368 31.0687581,16.5000003 30.4710557,16.5000003 L28.3065836,16.5000003 C27.7088812,16.5000003 27.2243476,17.00368 27.2243476,17.6250003 L27.2243476,37.8749994 C27.2243476,38.4963197 27.7088812,39.0000003 28.3065836,39.0000003 Z M43.0971425,7.50000051 L35.6648867,7.50000051 L32.5985513,2.18437593 C31.8157803,0.828519874 30.4058526,-0.000752023508 28.884678,5.11738477e-07 L19.7920918,5.11738477e-07 C18.2715602,5.11738477e-07 16.8624625,0.829093174 16.0800223,2.18437593 L13.0118832,7.50000051 L5.57962735,7.50000051 C4.78269076,7.50000051 4.136646,8.17157355 4.136646,9.00000051 L4.136646,10.5000005 C4.136646,11.3284277 4.78269076,12.0000005 5.57962735,12.0000005 L7.0226087,12.0000005 L7.0226087,43.5000005 C7.0226087,45.9852804 8.96074297,48.0000005 11.3515528,48.0000005 L37.3252171,48.0000005 C39.7160269,48.0000005 41.6541611,45.9852804 41.6541611,43.5000005 L41.6541611,12.0000005 L43.0971425,12.0000005 C43.8940791,12.0000005 44.5401238,11.3284277 44.5401238,10.5000005 L44.5401238,9.00000051 C44.5401238,8.17157355 43.8940791,7.50000051 43.0971425,7.50000051 Z M19.6342657,4.77281332 C19.7323744,4.60306234 19.9091243,4.49944487 20.0996272,4.49999861 L28.5771426,4.49999861 C28.7673243,4.49977491 28.9436588,4.60334956 29.0416023,4.77281332 L30.6162557,7.49999861 L18.0605142,7.49999861 L19.6342657,4.77281332 Z M37.3252171,43.5000005 L11.3515528,43.5000005 L11.3515528,12.0000005 L37.3252171,12.0000005 L37.3252171,43.5000005 Z M18.2057142,39.0000003 L20.3701862,39.0000003 C20.9678886,39.0000003 21.4524222,38.4963197 21.4524222,37.8749994 L21.4524222,17.6250003 C21.4524222,17.00368 20.9678886,16.5000003 20.3701862,16.5000003 L18.2057142,16.5000003 C17.6080117,16.5000003 17.1234782,17.00368 17.1234782,17.6250003 L17.1234782,37.8749994 C17.1234782,38.4963197 17.6080117,39.0000003 18.2057142,39.0000003 Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	x: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="12"
					height="12"
					viewBox="0 0 12 12"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					xmlns="http://www.w3.org/2000/svg"
				>
					{children}
					<g
						stroke="currentColor"
						stroke-width="2"
						fill="none"
						fill-rule="evenodd"
						stroke-linecap="square"
					>
						<path d="M2 2l7.986 7.986M9.986 2L2 9.986" />
					</g>
				</svg>
			);
		}
	},
	'input-select-marker': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="14"
					height="8"
					viewBox="0 0 14 8"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M1 1l6.074 6L13 1.147"
						stroke="currentColor"
						stroke-width="2"
						fill="none"
						fill-rule="evenodd"
					/>
				</svg>
			);
		}
	}
} as const;

export const icon = {
	...coreIcon,
	'accessibility-deaf': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M17,20a1.9,1.9,0,0,1-.76-.15,3.475,3.475,0,0,1-1.71-2.38,5.892,5.892,0,0,0-2.39-3,7.6,7.6,0,0,1-2.32-2.53A6.264,6.264,0,0,1,9,9,5,5,0,0,1,19,9h2a6.92,6.92,0,0,0-7-7A6.92,6.92,0,0,0,7,9a8.278,8.278,0,0,0,1.07,3.9,9.533,9.533,0,0,0,2.85,3.15,4.028,4.028,0,0,1,1.71,2.05,5.443,5.443,0,0,0,2.73,3.55A4,4,0,0,0,21,18H19a2.006,2.006,0,0,1-2,2ZM7.64,2.64,6.22,1.22a11.009,11.009,0,0,0,0,15.56l1.41-1.41A9.005,9.005,0,0,1,7.64,2.64ZM11.5,9A2.5,2.5,0,1,0,14,6.5,2.5,2.5,0,0,0,11.5,9Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	bars: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M3,18H21V16H3Zm0-5H21V11H3ZM3,6V8H21V6Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'notification-succes': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8.011,8.011,0,0,1,12,20ZM16.59,7.58,10,14.17,7.41,11.59,6,13l4,4,8-8Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	check: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M9,16.2,4.8,12,3.4,13.4,9,19,21,7,19.6,5.6Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'chevron-down': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					transform="rotate(90)"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M288.917,415.517,287.5,414.1l3.96-3.96-3.96-3.96,1.414-1.414,5.373,5.374Z"
						transform="translate(-278.17 -398.103)"
						fill="currentColor"
					/>
				</svg>
			);
		}
	},
	'chevron-left': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					transform="rotate(180)"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M288.917,415.517,287.5,414.1l3.96-3.96-3.96-3.96,1.414-1.414,5.373,5.374Z"
						transform="translate(-278.17 -398.103)"
						fill="currentColor"
					/>
				</svg>
			);
		}
	},
	'chevron-right': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M288.917,415.517,287.5,414.1l3.96-3.96-3.96-3.96,1.414-1.414,5.373,5.374Z"
						transform="translate(-278.17 -398.103)"
						fill="currentColor"
					/>
				</svg>
			);
		}
	},
	'chevron-up': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					transform="rotate(-90)"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M288.917,415.517,287.5,414.1l3.96-3.96-3.96-3.96,1.414-1.414,5.373,5.374Z"
						transform="translate(-278.17 -398.103)"
						fill="currentColor"
					/>
				</svg>
			);
		}
	},
	dislike: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					transform="rotate(180)"
				>
					{children}
					<path d="M0,0H24V24H0ZM0,0H24V24H0Z" fill="none" />
					<g fill="currentColor">
						<path d="M9,21h9a1.987,1.987,0,0,0,1.84-1.22l3.02-7.05A1.976,1.976,0,0,0,23,12V10a2.006,2.006,0,0,0-2-2H14.69l.95-4.57.03-.32a1.505,1.505,0,0,0-.44-1.06L14.17,1,7.58,7.59A1.987,1.987,0,0,0,7,9V19A2.006,2.006,0,0,0,9,21ZM9,9l4.34-4.34L12,10h9v2l-3,7H9ZM1,9H5V21H1Z" />
					</g>
				</svg>
			);
		}
	},
	'exclamation-triangle-warning': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M12,5.99,19.53,19H4.47L12,5.99M12,2,1,21H23Zm1,14H11v2h2Zm0-6H11v4h2Z"
							fill-rule="nonzero"
						/>
					</g>
				</svg>
			);
		}
	},
	globe: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	lattlast: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M-12518.443-17740.133v11.785h7.209"
						transform="translate(12525 17747)"
						fill="none"
						stroke="currentColor"
						stroke-width="2"
					/>
					<path
						d="M-12518.443-17740.133v11.785h7.209"
						transform="translate(12528.604 17743)"
						fill="none"
						stroke="currentColor"
						stroke-width="2"
					/>
				</svg>
			);
		}
	},
	like: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0ZM0,0H24V24H0Z" fill="none" />
					<g fill="currentColor">
						<path d="M9,21h9a1.987,1.987,0,0,0,1.84-1.22l3.02-7.05A1.976,1.976,0,0,0,23,12V10a2.006,2.006,0,0,0-2-2H14.69l.95-4.57.03-.32a1.505,1.505,0,0,0-.44-1.06L14.17,1,7.58,7.59A1.987,1.987,0,0,0,7,9V19A2.006,2.006,0,0,0,9,21ZM9,9l4.34-4.34L12,10h9v2l-3,7H9ZM1,9H5V21H1Z" />
					</g>
				</svg>
			);
		}
	},
	minus: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path d="M19,13H5V11H19Z" fill="currentColor" fill-rule="nonzero" />
				</svg>
			);
		}
	},
	plus: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M19,13H13v6H11V13H5V11h6V5h2v6h6Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	search: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M15.5,14h-.79l-.28-.27a6.51,6.51,0,1,0-.7.7l.27.28v.79l5,4.99L20.49,19Zm-6,0A4.5,4.5,0,1,1,14,9.5,4.494,4.494,0,0,1,9.5,14Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	sort: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M10,18h4V16H10ZM3,6V8H21V6Zm3,7H18V11H6Z"
						fill="currentColor"
						fill-rule="evenodd"
					/>
				</svg>
			);
		}
	},
	x: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<g fill="currentColor">
						<path d="M19,6.41,17.59,5,12,10.59,6.41,5,5,6.41,10.59,12,5,17.59,6.41,19,12,13.41,17.59,19,19,17.59,13.41,12Z" />
					</g>
				</svg>
			);
		}
	},
	'input-select-marker': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					xmlns="http://www.w3.org/2000/svg"
					width="24"
					height="24"
					viewBox="0 0 24 24"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="m7 10 5 5 5-5Z" fill="currentColor" />
				</svg>
			);
		}
	},
	'notification-danger': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M12,5.99,19.53,19H4.47L12,5.99M12,2,1,21H23Zm1,14H11v2h2Zm0-6H11v4h2Z"
							fill-rule="nonzero"
						/>
					</g>
				</svg>
			);
		}
	},
	'notification-warning': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M11,15h2v2H11Zm0-8h2v6H11Zm.99-5A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2ZM12,20a8,8,0,1,1,8-8A8,8,0,0,1,12,20Z"
							fill-rule="nonzero"
						/>
					</g>
				</svg>
			);
		}
	},
	'notification-info': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M11,15h2v2H11Zm0-8h2v6H11Zm.99-5A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2ZM12,20a8,8,0,1,1,8-8A8,8,0,0,1,12,20Z"
							fill-rule="nonzero"
						/>
					</g>
				</svg>
			);
		}
	},
	close: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<g fill="currentColor">
						<path d="M19,6.41,17.59,5,12,10.59,6.41,5,5,6.41,10.59,12,5,17.59,6.41,19,12,13.41,17.59,19,19,17.59,13.41,12Z" />
					</g>
				</svg>
			);
		}
	},
	'notification-error': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="48"
					height="48"
					viewBox="0 0 48 48"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M16.3083126,13.5575621 C17.212521,12.6758465 18.6746453,12.6758465 19.4922806,13.5575621 L24.0133226,18.0230248 L28.6209379,13.5575621 C29.5251463,12.6758465 30.9872705,12.6758465 31.8049058,13.5575621 C32.7860681,14.4487585 32.7860681,15.889842 31.8049058,16.6957111 L27.3608176,21.151693 L31.8049058,25.6930023 C32.7860681,26.5841986 32.7860681,28.0252822 31.8049058,28.8311512 C30.9872705,29.7981941 29.5251463,29.7981941 28.6209379,28.8311512 L24.0133226,24.4510158 L19.4922806,28.8311512 C18.6746453,29.7981941 17.212521,29.7981941 16.3083126,28.8311512 C15.4137234,28.0252822 15.4137234,26.5841986 16.3083126,25.6930023 L20.8389739,21.151693 L16.3083126,16.6957111 C15.4137234,15.889842 15.4137234,14.4487585 16.3083126,13.5575621 L16.3083126,13.5575621 Z M9.87119353,3.24 C11.1166068,1.228125 13.3583507,0 15.6767354,0 L32.240732,0 C34.6453376,0 36.8870815,1.228125 38.1324948,3.24 L46.9940893,17.49375 C48.3353036,19.65 48.3353036,22.35 46.9940893,24.50625 L38.1324948,38.75625 C36.8870815,40.771875 34.6453376,42 32.240732,42 L15.6767354,42 C13.3583507,42 11.1166068,40.771875 9.87119353,38.75625 L1.00576702,24.50625 C-0.335255674,22.35 -0.335255674,19.65 1.00576702,17.49375 L9.87119353,3.24 Z M4.95376353,20.0519187 C4.50454509,20.7819413 4.50454509,21.6920993 4.95376353,22.4221219 L13.8554068,36.8329571 C14.2690341,37.5060948 15.0193347,37.9232506 15.7407776,37.9232506 L32.3724409,37.9232506 C33.1804569,37.9232506 33.9307575,37.5060948 34.3443848,36.8329571 L43.2421804,22.4221219 C43.6942846,21.6920993 43.6942846,20.7819413 43.2421804,20.0519187 L34.3443848,5.64297968 C33.9307575,4.96510158 33.1804569,4.55079007 32.3724409,4.55079007 L15.7407776,4.55079007 C15.0193347,4.55079007 14.2690341,4.96510158 13.8554068,5.64297968 L4.95376353,20.0519187 Z M38.2882725,3.27656885 L34.3443848,5.64297968 L38.2882725,3.27656885 Z M1.00987575,24.7828442 L4.95376353,22.4221219 L1.00987575,24.7828442 Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'validation-error': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="18"
					height="18"
					viewBox="0 0 18 18"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="none" fill-rule="evenodd">
						<circle cx="9" cy="9" r="6.75" fill="white" />
						<path
							d="m18 9c0 4.9717-4.0298 9-9 9-4.9702 0-9-4.0283-9-9 0-4.9688 4.0298-9 9-9 4.9702 0 9 4.0312 9 9zm-9 1.8145c-0.92196 0-1.6694 0.7474-1.6694 1.6694 0 0.92196 0.7474 1.6694 1.6694 1.6694s1.6694-0.7474 1.6694-1.6694c0-0.92196-0.7474-1.6694-1.6694-1.6694zm-1.3157-1.065c0.012593 0.23095 0.20355 0.41175 0.43483 0.41175h1.7618c0.23128 0 0.42224-0.1808 0.43483-0.41175l0.2692-4.9355c0.013609-0.24946-0.18501-0.45922-0.43483-0.45922h-2.3002c-0.24982 0-0.4484 0.20976-0.43479 0.45922l0.2692 4.9355z"
							fill="var(--digi--color--icons--danger)"
						/>
					</g>
				</svg>
			);
		}
	},
	'validation-warning': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="18"
					height="18"
					viewBox="0 0 18 18"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="none" fill-rule="evenodd">
						<circle
							cx="9"
							cy="9"
							r="6.75"
							fill="var(--digi--color--icons--informative)"
						/>
						<path
							d="m18 9c0 4.9717-4.0298 9-9 9-4.9702 0-9-4.0283-9-9 0-4.9688 4.0298-9 9-9 4.9702 0 9 4.0312 9 9zm-9 1.8145c-0.92196 0-1.6694 0.7474-1.6694 1.6694 0 0.92196 0.7474 1.6694 1.6694 1.6694s1.6694-0.7474 1.6694-1.6694c0-0.92196-0.7474-1.6694-1.6694-1.6694zm-1.3157-1.065c0.012593 0.23095 0.20355 0.41175 0.43483 0.41175h1.7618c0.23128 0 0.42224-0.1808 0.43483-0.41175l0.2692-4.9355c0.013609-0.24946-0.18501-0.45922-0.43483-0.45922h-2.3002c-0.24982 0-0.4484 0.20976-0.43479 0.45922l0.2692 4.9355z"
							fill="var(--digi--color--icons--warning)"
						/>
					</g>
				</svg>
			);
		}
	},
  upload: {
    IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
      return (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
          aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
        >
          {children}
          <path
            d="M2.25 24A2.307 2.307 0 0 1 0 21.75v-5.362h2.25v5.362h19.5v-5.362H24v5.362A2.307 2.307 0 0 1 21.75 24Zm8.625-5.738V4.35l-4.5 4.5-1.612-1.612L12 0l7.238 7.237-1.613 1.613-4.5-4.5v13.912Z"
            fill="currentColor"
          />
        </svg>
      );
    }
  },
  work: {
    IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
      return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
        >
          {children}
          <path d="M160-120q-33 0-56.5-23.5T80-200v-440q0-33 23.5-56.5T160-720h160v-80q0-33 23.5-56.5T400-880h160q33 0 56.5 23.5T640-800v80h160q33 0 56.5 23.5T880-640v440q0 33-23.5 56.5T800-120H160Zm240-600h160v-80H400v80Z"
          fill="currentColor"
          />
          </svg>
      );
    }
  },
  grading: {
    IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
      return (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
          aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
        >
          {children}
          <path d="M657-121 544-234l56-56 57 57 127-127 56 56-183 183Zm-537 1v-80h360v80H120Zm0-160v-80h360v80H120Zm0-160v-80h720v80H120Zm0-160v-80h720v80H120Zm0-160v-80h720v80H120Z"
            fill="currentColor"
          />
        </svg>
      );
    }
  },
  info: {
    IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
      return (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
          aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
        >
          {children}
          <path d="M440-280h80v-240h-80v240Zm40-320q17 0 28.5-11.5T520-640q0-17-11.5-28.5T480-680q-17 0-28.5 11.5T440-640q0 17 11.5 28.5T480-600Zm0 520q-83 0-156-31.5T197-197q-54-54-85.5-127T80-480q0-83 31.5-156T197-763q54-54 127-85.5T480-880q83 0 156 31.5T763-763q54 54 85.5 127T880-480q0 83-31.5 156T763-197q-54 54-127 85.5T480-80Zm0-80q134 0 227-93t93-227q0-134-93-227t-227-93q-134 0-227 93t-93 227q0 134 93 227t227 93Zm0-320Z"
            fill="currentColor"
          />
        </svg>
      );
    }
  }
} as const;

export type IconName = keyof typeof icon;
export const iconNames = Object.keys(icon) as IconName[];
