import { randomIdGenerator } from './randomIdGenerator.util';

describe('randomIdGenerator', () => {
  it('should not return same id', () => {
    const id1 = randomIdGenerator();
    const id2 = randomIdGenerator();
    expect(id1).not.toEqual(id2);
  });

  it('should begin with default prefix', () => {
    expect(randomIdGenerator()).toMatch(/^digi/);
  });

  it('should begin with custom prefix', () => {
    expect(randomIdGenerator('test')).toMatch(/^test/);
  });
});
