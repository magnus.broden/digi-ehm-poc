import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';
import { postcss } from '@stencil/postcss';
import autoprefixer from 'autoprefixer';

import {
	angularOutputTarget,
	ValueAccessorConfig
} from '@stencil/angular-output-target';
import { reactOutputTarget } from '@stencil/react-output-target';
import { FormInputType } from './src/enums-core';

console.log('busting cache 2303281');

// This is used primarily by Angular Reactive Forms
const angularValueAccessorBindings: ValueAccessorConfig[] = [
	{
		elementSelectors: [
			'digi-form-input',
			`digi-form-input[afType=${FormInputType.TEXT}]`,
			`digi-form-input[afType=${FormInputType.EMAIL}]`,
			`digi-form-input[afType=${FormInputType.COLOR}]`,
			`digi-form-input[afType=${FormInputType.DATE}]`,
			`digi-form-input[afType=${FormInputType.DATETIME_LOCAL}]`,
			`digi-form-input[afType=${FormInputType.MONTH}]`,
			`digi-form-input[afType=${FormInputType.PASSWORD}]`,
			`digi-form-input[afType=${FormInputType.SEARCH}]`,
			`digi-form-input[afType=${FormInputType.TEL}]`,
			`digi-form-input[afType=${FormInputType.TIME}]`,
			`digi-form-input[afType=${FormInputType.URL}]`,
			`digi-form-input[afType=${FormInputType.WEEK}]`,
			'digi-form-textarea',
			'digi-form-input-search'
		],
		event: 'afOnInput',
		targetAttr: 'value',
		type: 'text'
	},
	{
		elementSelectors: [`digi-form-input[afType=${FormInputType.NUMBER}]`],
		event: 'afOnInput',
		targetAttr: 'value',
		type: 'number'
	},
	{
		elementSelectors: ['digi-calendar'],
		event: 'afOnDateSelectedChange',
		targetAttr: 'afSelectedDate',
		type: 'text'
	},
	{
		elementSelectors: ['digi-form-checkbox'],
		event: 'afOnChange',
		targetAttr: 'checked',
		type: 'boolean'
	},
	{
		elementSelectors: ['digi-form-radiogroup'],
		event: 'afOnGroupChange',
		targetAttr: 'value',
		type: 'text'
	},
	{
		elementSelectors: ['digi-form-radiobutton'],
		event: 'afOnChange',
		targetAttr: 'value',
		type: 'radio'
	},
	{
		elementSelectors: ['digi-form-select'],
		event: 'afOnChange',
		targetAttr: 'value',
		type: 'select'
	}
];

export const config: Config = {
	namespace: 'digi-skolverket',
	taskQueue: 'async',
	plugins: [
		sass(),
		postcss({
			plugins: [autoprefixer()]
		})
	],
	outputTargets: [
		angularOutputTarget({
			componentCorePackage: '@digi/skolverket',
			directivesProxyFile:
				'../../../libs/skolverket/angular/src/lib/stencil-generated/components.ts',
			valueAccessorConfigs: angularValueAccessorBindings,
			includeImportCustomElements: true
		}),
		reactOutputTarget({
			componentCorePackage: '@digi/skolverket',
			proxiesFile:
				'../../../libs/skolverket/react/src/lib/stencil-generated/components.ts',
			includeDefineCustomElements: true
		}),
		{
			type: 'dist',
			esmLoaderPath: '../loader',
			copy: [
				{ src: 'design-tokens', dest: 'design-tokens' },
				{ src: 'public', dest: 'public' },
				{ src: '../../fonts', dest: 'fonts' },
				{
					src: 'components/**/styles/*.variables.scss',
					dest: 'design-tokens/components',
					warn: true
				},
				{
					src: '__core/**/styles/*.variables.scss',
					dest: 'design-tokens/components',
					warn: true
				}
			]
		},
		{
			type: 'dist-custom-elements',
			dir: 'components',
			customElementsExportBehavior: 'single-export-module',
			copy: [
				{ src: 'design-tokens', dest: 'design-tokens' },
				{
					src: 'components/**/styles/*.variables.scss',
					dest: 'design-tokens/components',
					warn: true
				},
				{ src: '../../fonts', dest: 'fonts' },
				{
					src: '__core/**/styles/*.variables.scss',
					dest: 'design-tokens/components',
					warn: true
				}
			]
		},
		{
			type: 'docs-readme'
		},
		{
			type: 'docs-json',
			file: './docs.json'
		},
		{
			type: 'www',
			serviceWorker: null
		},
		{
			type: 'docs-vscode',
			file: './custom-elements.json'
		},
		{
			type: 'dist-hydrate-script'
		}
	],
	globalStyle: './src/global/styles/index.scss'
};
