## [unreleased]
### retry!
### Nytt

- `@digi/skolverket`
  - `digi-form-process-steps`, `digi-form-process-step`
    - Nya komponenter!
  - `digi-navigation-toc`
    - Ny komponent!
  - `digi-notification-cookie`
    - Ny komponent!
  - `digi-notification-detail`
    - Ny komponent!
  - `digi-notification-alert`
    - Nygammal komponent i ny skrud
- `@digi/core`
  - `digi-icon`
    - Ny komponent som ersätter tidigare specifika ikonkomponenter. Istället för detta `<digi-icon-search>` så gör man nu såhär `<digi-icon af-name="search">`

### Ändrat

- `@digi/styles`
  - Lagt till `li` i `_reset.scss`
- `@digi/core`
  - `digi-form-checkbox`
    - Ny property `afDescription`
  - `digi-button`
    - Fix så att `afForm` fungerar (mappas nu mot `form`-attributet i `ref` pga bugg i Stencil)
  - `digi-notification-alert`, `digi-form-error-list`
    - Flyttade från Core till Arbetsförmedlingen
  - `digi-expandable-accordion`
    - Ändrat några tokens för att täcka upp Skolverkets behov.
    - Ny property `afVariation`. `'primary'` är Arbetsförmedlingens utseende och `'secondary'` är Skolverkets utseende
  - `digi-expandable-faq`, `digi-expandable-faq-item`
    - Flyttade från Core till Arbetsförmedlingen
  - `digi-form-checkbox`, `digi-form-radiobutton`
    - Ny property `afLayout`. `'block'` är Skolverkets standardversion
  - `digi-form-input-search`, `digi-form-textarea`, `digi-form-label`, `digi-form-select`, `digi-form-input`
    - Refaktorerade för att täcka upp Skolverkets behov
  - `digi-button`
    - Småfixar för att täcka upp Skolverkets behov
  - `digi-icon-*`
    - Alla ikoner som inte täcks upp av `digi-icon` är nu flyttade från Core till Arbetsförmedlingen
- `@digi/skolverket`
  - `digi-notification-alert`, `digi-notification-detail`
    - Korrekt bakgrundsfärg
  - `digi-notification-alert-skv`
    - Bortplockad helt
  - `digi-expandable-accordion-skv`
    - Bortplockad helt
  - `digi-form-input-search-skv`, `digi-form-textarea-skv`, `digi-form-label-skv`, `digi-form-select-skv`, `digi-form-input-skv`
    - Bortplockade helt

## [16.1.2] - 2022-05-18

### Ändrat

- `Dokumentation`
  - Färgsidan under grafisk profil är uppdaterad enligt varumärket

## [16.1.1] - 2022-05-17

### Ändrat

- `Dokumentation`
  - Uppdaterade kodexempel
    - Typografikomponenter
    - Mediakomponenter
    - Tabellkomponent
  - Tillgänglighetsförbättringar på logotyplänk
  - Tillgänglighetslistan har fått funktionalitet för att importera csv-fil

## [16.1.0] - 2022-05-03

### Ändrat

- `@digi/core`
  - Live-exempel täcker nu fullbredd
- `Dokumentation`
  - Uppdaterade kodexempel
    - Formulärkomponenter
    - Förloppsmätarkomponenter
    - Kalenderkomponenten
    - Kodkomponenter
    - Kortkomponenter
    - Logotypkomponenten
    - Länkkomponenter
    - Taggkomponenten
    - Utfällbartkomponenter
  - Title-texter i webbläsaren sätts nu korrekt

## [16.0.1] - 2022-04-29

### Ändrat

- `@digi/core`
  - `digi-expandable-accordion`, `digi-expandable-faq-item`; Lagt in en knapp i rubriken för att interaktion med skärmläsare ska fungera korrekt.
  - `digi-button`; Lagt till attributen `afAriaControls`, `afAriaPressed` och `afAriaExpanded`.

## [16.0.0] - 2022-04-26

### Nytt

- `@digi/design-tokens`;<br />_Designtokens används för att style:a applikationer enligt gemensam nomenklatur och struktur._

  - Library som innehåller designtokens i jsonformat och förmåga att exportera dessa i olika format (e.g. CSS, SCSS, JSON, JS, m.fl.).
  - Designtokens finns i 3 nivåer; (Component-tokens finns bara i `@digi/core` och `@digi/styles`, `@digi/design-tokens` innehåller bara global och brand)
    - **global** - skalor av färger, avstånd m.m.,
    - **brand** - global tokens applicerade i olika kontexter, t.ex. color text primary, padding medium,
    - **component** - komponenters specifika applicering av brand-tokens, t.ex. button color background primary.
  - Alla designtokens hanteras i grunden här och inte längre i `@digi/styles` (som nu endast konsumerar designtokens i css/scss-format och exporterar vidare).
  - Olika applikationer kan nyttja designtokens i olika format, dessa format går att konfigurera direkt i `@digi/design-tokens` för att kunna exporteras vid behov - synka med designsystemet för att få till er applikations format!

- `@digi/core`

  - Ny struktur på komponenter som bättre följer en gemensam struktur och arkitektur. i Styles-mappen i komponentens mapp finns två filer främst; `<komponent>.variables.scss` (innehåller designtokens för komponenten) och `<komponent>.variations.scss` (innehåller mixins som applicerar designtokens beroende på komponentens aktiva variation).
  - <breaking/> Designtokens för komponenter skrivs och hanteras nu direkt i `@digi/core` och inte längre som SCSS-filer i styles. Dessa variabler/tokens exporteras dock till `@digi/styles` under Components.
  - Nytt verktyg för att skapa komponenter, kör kommandot `npm run generate-component:core` och följ dialogen för att skapa en ny komponent i `@digi/core` enligt den nya strukturen, med exempelfiler.

- `@digi/styles`
  - <breaking/> Tagit bort filerna `_entry.scss` och `digi.scss`, och ersatt dessa med `digi-styles.custom-properties.scss` (för alla designtokens i css/scss-format), `digi-styles.utilities.scss` (för alla functions och mixins, e.g. a11y--sr-only) samt `digi-styles.scss`(för allt från `@digi/styles` samtidigt, ersätter i princip `digi.scss`).
  - CSS-fil som innehåller alla designtokens/custom-properties (global, brand och component) samt CSS-fil som innehåller alla utility-classes finns under dist-mappen.

### Ändrat

- `@digi/core`

  - <breaking/> Alla enums för storlekar (S, M och L) har nu döpts om till sina fullständiga namn (SMALL, MEDIUM och LARGE).<br />**Se till att uppdatera era applikationer där ni använder dessa!**
  - <breaking/> Alla komponenters custom properties/designtokens har fått uppdaterade namn. Dessa kan ni se under respektive komponent på dokumentations-sajten.<br />**Se till att uppdatera era applikationer där ni använder dessa!**
  - Använder nu nya filer i `@digi/styles` för variables och utilities. Laddar in css-variabler i roten/global.
  - <breaking/> Ikoner har fått uppdaterad struktur för sina designtokens. Nu behöver du override:a variabeln direkt på komponenten, precis som för alla andra komponenter. Om du har en dynamisk ikon så kan du hitta den i css via t.ex. `[slot^='icon']` (för både icon och icon-secondary).<br />**Se till att uppdatera era applikationer där ni använder dessa!**
  - <breaking/> Följande komponenter har fått uppdaterade enums: `digi-button`, `digi-calendar`, `digi-form-input`, `digi-form-input-search`, `digi-form-select`, `digi-form-textarea`, `digi-link`, `digi-link-button`, `digi-link-internal`, `digi-link-external`, `digi-logo`, `digi-notification-alert`, `digi-table`, `digi-tag`, `digi-typography`, `digi-breakpoint-observer`.

- `@digi/styles`

  - <breaking/> Innehåller numera inga SCSS-variabler för komponenterna utan dessa är ersatta med customproperties (css-variabler). Källan till dessa är flyttade till respektive komponent i `@digi/core` och exporteras sedan tillbaka till `@digi/styles`.
  - Värdet på flera variabler har justerats, t.ex. avstånd och vissa färger.

- `Dokumentationen`
  - Använder nya designtokens och uppdatera komponenter.

## [15.0.0] - 2022-04-12

### Nytt

- `@digi/core`
  - Nya ikoner!
- `Dokumentation`
  - Lagt till en sida för release notes.
  - Förbättrade kontaktuppgifter enligt tillgänglighetskrav.
  - Ny startsida och sida för introduktion av designsystemet.

### Ändrat

- `@digi/core`
  - `digi-icon-arrow-<up|down|left|right>`; ikonerna för `arrow` har bytt namn till `digi-icon-chevron-<up|down|left|right>` för att bättre följa namnpraxis. Nya ikoner för arrow införda i dess ställe.
    **Glöm inte att byta namn på ikonerna om du lagt till dom manuellt i din applikation.**
  - `digi-media-image`; Fixat fel som uppstod om man använder komponenten som `af-unlazy` utan att ange specifik höjd och bredd på bilden.
  - `digi-icon-spinner`; problem med ikonen fixat.
- `Dokumentation`
  - Förbättrat live-exempel för informationsmeddelanden.
  - Fixat problem med laddning av bilder på dokumentations-sajten.

## [14.0.1] - 2022-04-11

### Ändrat

- `@digi/core-angular`
  - `digi-progress-steps` och `digi-progress-step` saknades i core-angular. Problemet är patchat.

## [14.0.0] - 2022-03-29

### Nytt

- `@digi/core`
  - `digi-progress-steps`; Ny komponent för att visualisera ett användarflöde.
  - `digi-progress-step`; Ny komponent som används tillsammans med `digi-progress-steps`. Detta är varje steg i flödet.

### Ändrat

- `Dokumentation`

  - Rättat länk till mediabanken.
  - Navigationen på dokumentationssidan använder sig av uppdateringarna i `digi-navigation-vertical-menu` och `digi-navigation-vertical-menu-item`.
  - Tagit bort dubbla landmärken runt navigationen.
  - Fixat valideringsfel på navigationen.
  - Uppdaterat tillgänglighetsredogörelsen.

- `@digi/core`
  - `digi-navigation-vertical-menu`; Layout är uppdaterad.
  - `digi-navigation-vertical-menu-item`; Layout är uppdaterad.

## [13.2.3] - 2022-03-25

- `@digi/core-angular`
  - `digi-icon-accessibility-universal`, `digi-icon-download`, `digi-icon-redo`, `digi-icon-trash` saknades i core-angular. Problemet är patchat.

## [13.2.2] - 2022-03-24

- `@digi/core`
  - `Code` typen för Code exporterades inte korrekt och skapade problem vid användning av Core i vissa sammanhang. Problemet är patchat.

## [13.2.1] - 2022-03-23

- `@digi/core-angular`
  - Tillåter alla peer dependency versioner av angular-paketen för version 12 och 13 och undviker därmed Conflicting Peer Dependency-felet när du gör npm install.

## [13.2.0] - 2022-03-22

### Ändrat

- `Dokumentationen`

  - Ändrat till Stencil Router V2 för att lösa buggar med bl.a. lazy-loadade bilder i dokumentationsapplikationen. (Problem med bildladdning kvarstår för vissa användare och kommer åtgärdas till nästa release.)
  - Komponentdokumentationen för `digi-link-button`, `digi-button` och `digi-info-card` är uppdaterad med den nya versionen av kodexemepelkomponenten

- `@digi/core`

  - `digi-code-example`; förbättrad funktionalitet och möjlighet att kunna växla mellan olika varianter av exempelkomponenten i demoytan.
  - `digi-code`; ändrat bakgrundsfärg från gråsvart till mörkblå, samt gjort den ljusa varianten till standard.
  - `digi-code-block`; ändrat bakgrundsfärg från gråsvart till mörkblå.

- `@digi/core-angular`

  - n/a

- `@digi/styles`

  - n/a

- `@digi/core-fonts`
  - n/a

## [13.1.2] - 2022-03-18

### Ändrat

- `@digi/core`

  - n/a

- `@digi/core-angular`

  - La till komponenter under Utfällbart (expandable).

- `@digi/styles`

  - n/a

- `@digi/core-fonts`
  - n/a

## [13.1.1] - 2022-03-16

### Ändrat

- `@digi/core`

  - `digi-navigation-pagination`; aktiv sida markerades inte korrekt om man har väldigt många sidor.

- `@digi/core-angular`

  - n/a

- `@digi/styles`

  - n/a

- `@digi/core-fonts`
  - n/a

## [13.1.0] - 2022-03-10

### Nytt

- `Changelog`

  - Ny gemensam changelog för hela monorepot. All information om driftsättningar kommer skrivas i samma dokument för att hålla ihop versionsnummer och gemensamma ändringar.

- `Dokumentationen`

  - Testmetoder; ny sida under Tillgänglighet och design.
  - Ny förbättrad caching av sidan. Laddar resurser i bakgrunden och tillåter bl.a. offline-läge och snabbare sidladdning.

- `@digi/core`

  - `digi-icon-trash`; ny ikon som illustrerar en soptunna.
  - `digi-icon-accessibility-universal`; ny ikon som illustrerar tillgänglighet.

- `@digi/core-angular`

  - n/a

- `@digi/styles`

  - n/a

- `@digi/core-fonts`
  - n/a

### Ändrat

- `Dokumentationen`

  - Tillgänglighetslistan; ny förbättrad funktionalitet för att kunna följa upp status och kommentarer på olika krav samt bättre filtrering. Även ökad tillgänglighet.

- `@digi/core`

  - n/a

- `@digi/core-angular`

  - n/a

- `@digi/styles`

  - n/a

- `@digi/core-fonts`
  - n/a

## [13.0.0] - 2022-03-08

### Nytt

- `@digi/core-angular`

  - Nu helt releasad och har gått ur beta.

- `@digi/core-fonts`
  - Nytt bibliotek för hantering av Arbetsförmedlingens typsnitt.

### Ändrat

- `digi-button`
  - Lagt till saknade design tokens samt dokumenterat tokens.

## [12.6.1] - 2022-02-18

- `digi-form-select`
  - `afStartSelected` är deprecated. Använd `afValue` istället.

## [12.6.0] - 2022-02-22

### Nytt

- `digi-expandable-accordion`

  - Ny komponent

- `digi-expandable-faq`

  - Ny komponent

- `digi-expandable-faq-item`
  - Ny komponent

## [12.5.0] - 2022-02-22

### Ändrat

- `digi-link-(internal|external)`
  - Justerat så att digi-link-internal och digi-link-external använder sig av digi-link för att de ska ärva regler och fungera lika.

## [12.4.0] - 2022-02-15

- `digi-typography`
  - Lagt till maxbredd för rubriker och länkar.
  - Lagt en l-version för maxbredd.

## [12.3.1] - 2022-02-09

- `digi-button, digi-link-(internal|external)`
  - Justerat positionering av ikoner och dess storlek för knappar och länkar.

## [12.3.0] - 2022-02-09

- `digi-navigation-tabs`
  - Lagt till en publik metod för att ändra aktiv flik.

## [12.1.1-beta.5] - 2022-01-20

- `value`
  - Value och afValue sätts vid afOnInput så attributet korrekt speglas vid target.

## [11.2.1-beta.0] - 2022-01-19

- `digi-form-radiobutton`
  - Fixat bugg där radiogroup inte stöds. Fungerar nu på det sätt du önskar!

## [10.2.1] - 2022-01-19

### Ändrat

- `digi-media-image`
  - Fixat bugg med hur platshållaren fungerar
  - Ändrat bakgrunsfärg på platshållaren
  - Lagt till så attributet loading används på bilden. Sätts till `lazy` eller `eager` beroende på om man använder `afUnLazy` eller inte
  - Bilden går att ändra dynamiskt

## [12.0.0] - 2022-01-18

- `version`
  - Ökade versionsnumret till 12 för att undvika problem med paket i npm.

## [10.2.0] - 2022-01-18

- `digi-layout-block`

  - Lade till attributet af-vertical-padding för att kunna addera padding inuti container-elementet.
  - Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på container-elementet.

- `digi-layout-container`
  - Lade till attributet af-vertical-padding för att kunna addera padding inuti elementet.
  - Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på elementet.

## [11.1.0] - 2022-01-10

- `enums`

  - Enums finns numera under `@digi/core` och fungerar med bl.a. Angular.

- `types`

  - Ändrat types från `components.d.ts` till `index.d.ts` som både exporterar components samt enums. På components finns namespace Components som kan användas t.ex. som `Components.DigiButton`. Förberett för interfaces om/när det ska användas och exponeras.

- `output-target`
  - Använder nu `dist-custom-elements` istället för `dist-custom-elements-bundle` som blivit deprecated.

## [10.1.3] - 2022-01-17

### Ändrat

- `digi-form-input`, `digi-form-textarea`
  - Tagit bort kontroll av 'dirty' och 'touched' av formulärelement vilket gör det mer flexibelt att välja när felmeddelanden ska visas.
- `digi-form-select`
  - Använder sig av `digi-util-mutation-observer` för att se om options-lista ändras programmatiskt.
- `digi-form-fieldset`
  - Lagt in möjlighet att sätta id på komponenten. Väljer man inget sätts en slumpmässig id.
- `digi-media-image`
  - Löst bugg som gjorde att komponenten inte använde den bredd och höjd man angett.

## [11.0.0] - 2021-12-23

- `digi-form-*`

  - Justerat formulärkomponenterna så att de kan använda `value` istället för `afValue` (som nu är markerat deprecated). Även `checked` ersätter `afChecked`. Detta är gjort för att bättre fungera med Angular samt det nya biblioteket Digi Core Angular som är påväg ut.

- `digi-form-select`

  - Ny logik för att hitta valt värde och skicka rätt event enligt samma practice som de andra formulärelementen.

- `digi-form-radiobutton`

  - Tog bort `afChecked` och använder nu `afValue`för att initialt sätta ett värde. När `value` är samma som `afValue` så är radioknappen icheckad (följer bättre radiogroup).

- `övrigt`
  - Digi Core är nu förberett för att kunna exportera filer via output-target till Angular. Ett nytt bibliotek `@digi/core-angular` kommer snart som är till för angular-appar. Det här biblioteket wrappar Digi Core och gör det enklare att använda t.ex. Reactive Forms.

## [10.1.2] - 2021-12-15

### Ändrat

- `digi-typography`
  - Lagt till så ul- och dl-listor får samma textstorlek som stycken och även samma hantering för att inte få för långa textrader
- `digi-navigation-pagination`
  - Justerat så att primär och sekundär variant på knapparna används korrekt. För att ändra utseende på knapparna så behöver knapparnas original-variabler ändras direkt, t.ex. `--digi-button--background`, dock finns variabler för t.ex. width, padding, m.m.

## [10.1.1] - 2021-12-10

### Ändrat

- `digi-form-filter`
  - Löst en bugg där komponenten tidigare fungerade felaktigt om någon kryssruta var aktiv vid sidladdning.
- `digi-navigation-tabs`
  - Lagt in stöd så komponenten känner av om man lägger till eller tar bort `digi-navigation-tab`-komponenter för att kunna dynamiskt ändra antal flikar.
- `digi-info-card`
  - Tagit bort felaktig marginal på rubriken.
- `digi-navigation-tab`
  - Ändrat så fokusram på en panel endast markeras när man navigerar dit med skärmläsare.
- `digi-form-filter`
  - Fixat så att komponenten fungerar korrekt även om man använder den inuti `digi-navigation-tab`. Tidigare stängdes listan när man försökte klicka på en kryssruta.

## [10.1.0] - 2021-11-23

### Nytt

- `digi-icon-exclamation-triangle-warning`
  - Lagt till en ny ikon. Denna ikon används bl.a. för varningsmeddelanden i formulär. Tidigare behövdes den ikonen läsas in som en asset i projektet, nu kommer inte detta behövas.

### Ändrat

- `digi-link-button`
  - Uppdaterat komponenten så man kan välja mellan olika storlekar.
  - Uppdaterat så alla layouter som finns i UI-kit också går att använda i kod.
- `digi-form-input`, `digi-form-select`, `digi-form-textarea`
  - Korrigerat så layout av formulärelement när de indikerar "fel", "varning" och "korrekt" följer UI-kit.
- `digi-icon-arrow-down`
  - Korrigerat layout så den följer UI-kit
- `digi-navigation-breadcrumbs`
  - Ändrat uppdelaren mellan länkar till '/', istället för '>'.

## [10.0.0] - 2021-10-19

### Ändrat

- `digi-navigation-menu`
  - Allt innehåll i Navigation Menu ska ligga i en `<ul>`-tagg och listas med respektive `<li>`-taggar.
- `digi-navigation-menu-item`
  - En Navigation Menu Item som ska agera som expanderbart menyalternativ ska följas av en `<ul>`-tagg, t.ex.
  ```
    <digi-navigation-menu-item></digi-navigation-menu-item>
    <ul>...</ul>
  ```
  För att göra raden expanderbar måste attributet `af-active-subnav` användas med antingen `true` för för-expanderad eller `false` för stängd.
- `digi-layout-media`
  - Lagt till token `--digi-layout-media-object--flex-wrap`
- `digi-progressbar`
  - Lagt till möjlighet att tända valfria steg i komponenten, som följer ordningen på vilka formulär-element som är gjorda.
- `digi-navigation-pagination`
  - Lagt till en publik metod för att ändra aktivt steg i komponenten.
- `digi-form-error-list`
  - Lade till default värde på linkItems för att undvika JS fel vid tom lista

## [9.4.6] - 2021-09-30

### Ändrat

- `digi-code-example`
  - Ändrat bakgrundsfärg på vertygslistan

## [9.4.5] - 2021-09-28

### Ändrat

- `digi-form-input-search`
  - Lagt till möjlighet att dölja knappen
  - Buggfix, knappens text gick inte att ändra

## [9.4.4] - 2021-09-24

### Ändrat

- `digi-form-error-list`
  - Ändrat färgen på notifikationen från "info" till "danger"
- `digi-notification-alert`
  - Justerat padding för medium storlek
- `digi-form-validation-message`
  - Ändrat default varningsmeddelande till att vara tomt

## [9.4.3] - 2021-09-16

### Ändrat

- `design-tokens`
  - Ändrat färgen `$digi--ui--color--green`
  - Ändrat färgen `$digi--ui--color--pink`

## [9.4.2] - 2021-08-27

### Ändrat

- `digi-form-filter`
  - Lagt till afOnChange EventEmitter

## [9.4.1] - 2021-08-18

### Ändrat

- Lagt till en ny outputTarget `dist-custom-elements-bundle`

## [9.4.0] - 2021-07-02

### Nytt

- `digi-progressbar`
  - Ny komponent

### Ändrat

- `digi-form-radiobutton`
  - Korrigeringar av layout

## [9.3.0] - 2021-06-18

### Ändrat

- `digi-navigation-pagination`
  - Skriv inte ut pagineringen om det bara är en sida. Däremot visas fortfarande texten "Visar 1-15 av XXX".

## [9.2.1] - 2021-06-15

### Ändrat

- Lagt till alla enums under `@digi/core/dist/enum/`
- Korrigerat felaktigheter i alla readmefiler. Främst gällande felaktiga paths till enum-importer.
- Lagt till enum-mappen till disten.

## [9.2.0] - 2021-06-15

### Nytt

- `digi-calendar`
  - Ny komponent
- `digi-typography-meta`
  - Ny komponent
- `digi-logo`
  - Ny komponent
- `digi-util-mutation-observer`
  - Ny komponent
- `digi-form-radiobutton`
  - Ny komponent

### Ändrat

- `digi-form-fieldset`
  - Ändrade padding i fieldset till 0 för att linjera med komponenter utanför fieldset
- `digi-form-error-list`
  - Lagt till mutation observer för att se ändringar i komponenten

## [9.1.0] - 2021-05-25

### Nytt

- `digi-util-breakpoint-observer`
  - Ny komponent
- `digi-form-select`
  - Ny komponent
- `digi-form-fieldset`
  - Ny komponent
- `digi-tag`
  - Ny komponent

### Ändrat

- `digi-navigation-pagination`
  - Felplacering av ikon i knapparna föregående/nästa vid navigering av siffer-knapparna.
  - Centrering av text i siffer-knapparna efter css-reset tagits bort

## [9.0.0] - 2021-05-04

### Ändrat

- `digi-core`
  - Lagt till ny docs tag `@enums` i alla komponenter
- `digi-media-figure`
  - Ändrat enum `MediaFigureAlign` till `MediaFigureAlignment`
  - Ändrat prop `afAlign` till `afAlignment`
- `digi-form-error-list`
  - Ändrat enum `ErrorListHeadingLevel` till `FormErrorListHeadingLevel`

## [8.1.0] - 2021-05-03

### Nytt

- `digi-core`
  - Lagt till alla `enums` till paketet. Dessa hittas under `@digi/core/enum/mitt-enum-namn.emum.ts`

## [8.0.0] - 2021-04-30

### Changed

- `digi-core`
  - Ändrat all dokumentation inne i komponenterna till svenska
- `digi-form-filter`
  - Ändrat event `afOnSubmittedFilter` till `afOnSubmitFilter`
- `digi-form-input-search`
  - Ändrat prop `afButtonLabel` till `afButtonText`
- `digi-navigation-tabs`
  - Ändrat prop `afTablistAriaLabel` till `afAriaLabel`
  - Ändrat prop `afInitActiveTabIndex` till `afInitActiveTab`
- `digi-navigation-pagination`
  - Ändrat prop `afStartPage` till `afInitActivePage`
- `dgi-notification-alert`
  - Ändrat event `afOnCloseAlert` till `afOnClose`

## [7.2.0] - 2021-04-23

### Added

- `digi-notification-alert`
  - Created component
- `digi-form-error-list`
  - Created component
- `digi-link`
  - Created component

### Changed

- `digi-layout-media-object`
  - Included Stretch alignment

## [7.1.0] - 2021-04-19

### Added

- `digi-typography-time`
  - Created component

### Changed

- `Navigation-pagination` `Navigation-tabs` `Form-filter` `Form-textarea` `Form-input` `Form-input-search`
  - Solved style issues after removal of base-reset

## [7.0.0] - 2021-04-19

### Added

- `digi-navigation-context-menu`
  - Created component
- `digi-navigation-breadcrumbs`
  - Created component

### Breaking Changes

- **Removed reset css from all components**
  - Previously, all components included a reset css to prevent global styles from leaking in. This created a massive duplication of css and is now removed. Every consuming app is now responsible for handling css leakage. We are continuously correcting css problems that this may have caused inside the components themselves. If you encounter any problems, please report them to us.
- **Removed `global` folder**
  - All uncompiled scss is being moved to a new library called `@digi/styles`, which will very soon be available from the package manager. In the meantime, you can use the old `@af/digi-core`-package.

### Changed

- `digi-button`
  - Included hasIconSecondary within handleErrors function
  - Added outline and text-align variables

