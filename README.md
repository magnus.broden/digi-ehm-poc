# Digi

Det här är Arbetsförmedlingens, Skolverkets och eHälsomyndighetens gemensamma designsystem. Här finns bland annat komponentbibliotek, design tokens, funktionalitet, api:er och annat som hjälper dig att bygga digitala tjänster som följer våra gemensamma riktlinjer. Repot är ett monorepo genererat med [Nx](https://nx.dev). Besök [Arbetsförmedlingens dokumentationssida](https://designsystem.arbetsformedlingen.se/) för att se vilka komponenter som ingår.

## Kom igång

### Förutsättningar

- [node (och npm)](https://nodejs.org/en/) i din utvecklingsmiljö. Aktuell nodeversion finns i `.nvmrc`

### Installera

- `nvm use` om du använder nvm. Annars får byta till rätt nodeversion på annat sätt.
- `npm i` i roten av projektet.

## Arbeta med bibliotek och applikationer

Nx gör skillnad på bibliotek (libs) och applikationer (apps). Dessa hittar du under respektive mapp i projektet. Digi innehåller en mängd olika bibliotek och applikationer, och fler tillkommer hela tiden. Se nedan för de viktigaste.

### Publicera paket

[Publicera Skolverkets paket och dokumentationswebb](/libs/skolverket)

#### Bygg alla påverkade tjänster

`npm run affected:build`
Nx har ett mycket behändigt sätt att se vilka olika tjänster som påverkas av varandra, och kan bygga alla de som påverkas av förändringar du gör. Så om `libA` är beroende av `libB` och du gör förändringar i `libB`, så kan Nx automatiskt bygga om `libA`.

#### Testa alla påverkade tjänster

`npm run affected:test`

#### Visualisera beroenden mellan olika tjänster

`npm run dep-graph`
Detta kommando ger dig en mycket överskådlig graf över vilka tjänster som är beroende av vilka.

## De olika tjänsterna

### Struktur

Varje tjänst är grupperad efter ägandeskap:

- `core` är sådant som delas av alla. Publiceras som `@digi/core`
- `arbetsformedlingen` är specifikt för Arbetsförmedlingen. Publiceras som `@digi/arbetsformedlingen`
- `skolverket` är specifikt för Skolverket. Publiceras som `@digi/skolverket`

Varje sådan grundkategori innehåller några av följande bibliotek:

- `package` - Detta är ett Stencilprojekt med komponenter. Alla komponenter från `core` kopieras in i de andras komponentbibliotek då de byggs, vilket gör att de finns tillgängliga direkt inne i respektive konsuments publicerade paket
- `design-tokens` - Design tokenskonfigurationer (använder sig av Style Dictionary) för respektive konsument
- `fonts` - Typsnittsfiler och css-definitioner
- `angular` - Angularkomponenter från Stencils Angular output target
- `react` - Reactkomponenter från Stencils React output target

Utöver allt ovan finns även `shared`, funktioner och annat som används av alla andra tjänster. I `Shared\Styles` ligger all css som genereras automatiskt vid byggen.

### Util Taxonomies

Util Taxonomies är ett hjälparbibliotel för att hantera alla olika eventuella taxonomier vi kan behöva. Här finns kategorier för komponenter, och även css-kategorier kommer tillkomma. Vid behov av andra typer av taxonomier (till exempel taggar om vi behöver flera-till-en-taxonomier), så lägger vi till dem här. Detta bibliotek används till exempel av Docs för att kategorisera komponenter i navigationen.

## Börja jobba

### Arbetsförmedlingen

Här hittar du information om hur du startar dokumentation och hur du bygger/testar komponenter för Arbetsförmedlingen.
[Arbetsförmedlingen](./apps/arbetsformedlingen/README.md)

### Skolverket

Här hittar du information om hur du startar dokumentation och hur du bygger/testar komponenter för Skolverket.
[Skolverket](./apps/Skolverket/README.md)
