"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.componentSchematic = exports.digiDocsComponentPageGenerator = void 0;
const tslib_1 = require("tslib");
const devkit_1 = require("@nrwl/devkit");
const path_1 = require("path");
function digiDocsComponentPageGenerator(host, options) {
    return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
        if (!/[-]/.test(options.name)) {
            throw new Error((0, devkit_1.stripIndents) `
      "${options.name}" tag must contain a dash (-) to work as a valid web component. Please refer to
      https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name for more info.
      `);
        }
        const componentFileName = (0, devkit_1.names)(options.name).fileName+"-details";
        const heading = (0, devkit_1.names)(options.name).className.replace(/([A-Z])/g, ' $1').trim();
        const componentName = (0, devkit_1.names)(options.name).fileName;
        const directoryName = (0, devkit_1.names)(options.directory).fileName;
        const className = (0, devkit_1.names)(options.name).className;
        const componentOptions = ({}.generators || { '@nxext/stencil:component': {}, })['@nxext/stencil:component'];
        if (!componentOptions) {
            devkit_1.logger.info((0, devkit_1.stripIndents) `
        Style options for components not set, please run "nx migrate @nxext/stencil"
      `);
        }
        options = Object.assign(Object.assign({}, options), componentOptions);
        (0, devkit_1.generateFiles)(host, (0, path_1.join)(__dirname, './files/component'), (0, devkit_1.joinPathFragments)(`apps/${directoryName}/docs/src/details/${componentName+"-details"}`), {
            componentName: componentName,
            directoryName: directoryName,
            componentFileName: componentFileName,
            heading: heading,
            className: className,
            style: options.style || "scss",
        });
        if (!options.skipFormat) {
            yield (0, devkit_1.formatFiles)(host);
        }
    });
}
exports.digiDocsComponentPageGenerator = digiDocsComponentPageGenerator;
exports.default = digiDocsComponentPageGenerator;
exports.componentSchematic = (0, devkit_1.convertNxGenerator)(digiDocsComponentPageGenerator);
//# sourceMappingURL=component.js.map