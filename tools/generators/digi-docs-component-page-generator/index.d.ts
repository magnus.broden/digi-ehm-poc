import { Tree } from '@nrwl/devkit';
export interface ComponentPageSchema {
    name: string;
    heading: string;
    style?: string;
    skipFormat?: boolean;
}
export declare function digiDocsComponentPageGenerator(host: Tree, options: ComponentPageSchema): Promise<void>;
export default digiDocsComponentPageGenerator;
export declare const componentPageSchematic: (options: ComponentPageSchema) => (tree: any, context: any) => Promise<any>;
