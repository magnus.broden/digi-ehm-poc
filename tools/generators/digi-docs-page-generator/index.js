"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.componentSchematic = exports.digiDocsPageGenerator = void 0;
const tslib_1 = require("tslib");
const devkit_1 = require("@nrwl/devkit");
const path_1 = require("path");
function digiDocsPageGenerator(host, options) {
    return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
        if (!/[-]/.test(options.name)) {
            throw new Error((0, devkit_1.stripIndents)`
      "${options.name}" tag must contain a dash (-) to work as a valid web component. Please refer to
      https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name for more info.
      `);
        }
        
        const componentFileName = "digi-docs-" + (0, devkit_1.names)(options.name).fileName;
        const heading = (0, devkit_1.names)(options.name).className.replace(/([A-Z])/g, ' $1').trim();
        const directoryName = (0, devkit_1.names)(options.directory).name;
        const pageName = (0, devkit_1.names)(options.name).fileName;
        const className = (0, devkit_1.names)(options.name).className;
        const componentOptions = ({}.generators || { '@nxext/stencil:component': {}, })['@nxext/stencil:component'];
        const createDate = new Date;

        if (!componentOptions) {
            devkit_1.logger.info((0, devkit_1.stripIndents)`
        Style options for components not set, please run "nx migrate @nxext/stencil"
      `);
        }
        options = Object.assign(Object.assign({}, options), componentOptions);
        (0, devkit_1.generateFiles)(host, (0, path_1.join)(__dirname, './files/component'), (0, devkit_1.joinPathFragments)(`apps/${directoryName}/docs/src/pages/${pageName}`), {
            pageName: pageName,
            directoryName: directoryName,
            componentFileName: componentFileName,
            heading: heading,
            className: className,
            style: options.style || "scss",
            createDate: createDate
        });
        if (!options.skipFormat) {
            yield (0, devkit_1.formatFiles)(host);
        }
    });
}
exports.digiDocsPageGenerator = digiDocsPageGenerator;
exports.default = digiDocsPageGenerator;
exports.componentSchematic = (0, devkit_1.convertNxGenerator)(digiDocsPageGenerator);
//# sourceMappingURL=component.js.map