import { Tree } from '@nrwl/devkit';
export interface PageSchema {
    name: string;
    heading: string;
    style?: string;
    skipFormat?: boolean;
}
export declare function digiDocsPageGenerator(host: Tree, options: PageSchema): Promise<void>;
export default digiDocsPageGenerator;
export declare const pageSchematic: (options: PageSchema) => (tree: any, context: any) => Promise<any>;
