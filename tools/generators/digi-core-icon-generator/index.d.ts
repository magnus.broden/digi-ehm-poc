import { Tree } from '@nrwl/devkit';
export interface IconSchema {
    name: string;
    style?: string;
    skipFormat?: boolean;
}
export declare function digiCoreIconGenerator(host: Tree, options: IconSchema): Promise<void>;
export default digiCoreIconGenerator;
export declare const componentSchematic: (options: IconSchema) => (tree: any, context: any) => Promise<any>;
