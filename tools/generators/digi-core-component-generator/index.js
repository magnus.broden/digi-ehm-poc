"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.componentSchematic = exports.digiCoreComponentGenerator = void 0;
const tslib_1 = require("tslib");
const devkit_1 = require("@nrwl/devkit");
const path_1 = require("path");
function digiCoreComponentGenerator(host, options) {
    return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
        if (!/[-]/.test(`${options.category}-${options.name}`)) {
            throw new Error((0, devkit_1.stripIndents) `
      "${options.name}" tag must contain a dash (-) to work as a valid web component. Please refer to
      https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name for more info.
      `);
        }
        const componentName = (0, devkit_1.names)(options.name).name;
        const componentCategory = (0, devkit_1.names)(options.category).name;
        const directoryName = (0, devkit_1.names)(options.directory).name;

        let variations = (0, devkit_1.names)(options.variations).name;
        variations && (variations = variations.replace(/\ /g,'').split(','));

        function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
        }

        const className = (`${capitalizeFirstLetter(componentCategory)}-${componentName}`).replace(/-./g, x=>x[1].toUpperCase());
        const componentOptions = ({}.generators || { '@nxext/stencil:component': {}, })['@nxext/stencil:component'];
        if (!componentOptions) {
            devkit_1.logger.info((0, devkit_1.stripIndents) `
        Style options for components not set, please run "nx migrate @nxext/stencil"
      `);
        }
        options = Object.assign(Object.assign({}, options), componentOptions);
        (0, devkit_1.generateFiles)(host, (0, path_1.join)(__dirname, './files/component'), (0, devkit_1.joinPathFragments)(`libs/${directoryName}/package/src/components/_${componentCategory}/${componentCategory}-${componentName}`), {
            componentName,
            componentCategory,
            variations,
            directoryName,
            className,
            style: options.style || "scss",
        });
        if (!options.skipFormat) {
            yield (0, devkit_1.formatFiles)(host);
        }
    });
}
exports.digiCoreComponentGenerator = digiCoreComponentGenerator;
exports.default = digiCoreComponentGenerator;
exports.componentSchematic = (0, devkit_1.convertNxGenerator)(digiCoreComponentGenerator);
//# sourceMappingURL=component.js.map