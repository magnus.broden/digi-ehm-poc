import { Tree } from '@nrwl/devkit';
export interface ComponentSchema {
    componentName: string;
    componentCategory: string;
    variations?: string[];
    className: string;
    style?: string;
    skipFormat?: boolean;
}
export declare function digiDocsComponentPageGenerator(host: Tree, options: ComponentSchema): Promise<void>;
export default digiDocsComponentPageGenerator;
export declare const ComponentSchematic: (options: ComponentSchema) => (tree: any, context: any) => Promise<any>;
